# jdk8-source-code

#### 介绍
在工作之余，阅读下 jdk8 中常见的源码；
同时也回顾对比秋招时候的自己，看看有没有成长，对代码的理解有没有加深。

#### 进度

lang
```markdown
Object
String
AbstractStringBuilder
StringBuffer
StringBuilder
Boolean
Byte
Double
Float
ApplicationShutdownHooks
Shutdown
Runtime
System
SecurityManager
Process
ProcessBuilder
Integer
Short
Long
Character
Thread
ThreadGroup
ThreadLocal
InheritedThreadLocal
Enum
Throwable
Exception
Error
Class
ClassLoader
Iterable
```

util
```markdown
StringJoiner
Formatter
StringTokenizer
Collection
AbstractCollection
List
AbstractList
ArrayList
AbstractSequentialList
Queue
Deque
LinkedList
ArrayDeque
Vector
Stack
AbstractQueue
Map
AbstractMap
HashMap
```

util.concurrent
```markdown

```

util.concurrent.atomic
```markdown

```
reflect
```markdown

```

annotation
```markdown

```

util.concurrent.locks
```markdown

```
io
```markdown

```

nio
```markdown

```
sql
```markdown

```
net
```markdown

```


#### 使用说明

基本把上述类都看了一遍，方法上的英文注释使用 copilot 简单翻译了一下，但不一定正确，还是要以英文注释为主；


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

