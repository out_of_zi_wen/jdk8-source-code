/*
 * Copyright (c) 1997, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

import java.util.function.Consumer;

/**
 * Doubly-linked list implementation of the {@code List} and {@code Deque}
 * interfaces.  Implements all optional list operations, and permits all
 * elements (including {@code null}).
 * <p>
 * List 和 Deque 接口的双向链表实现。
 * 实现了所有可选的 list 操作，并允许所有元素（包括null）。
 *
 * <p>All of the operations perform as could be expected for a doubly-linked
 * list.  Operations that index into the list will traverse the list from
 * the beginning or the end, whichever is closer to the specified index.
 * <p>
 * 所有操作都按照双向链表的预期执行。
 * 索引到 list 的所有操作将从开始或结束遍历 list，以靠近指定索引的那一端为准。
 *
 * <p><strong>Note that this implementation is not synchronized.</strong>
 * If multiple threads access a linked list concurrently, and at least
 * one of the threads modifies the list structurally, it <i>must</i> be
 * synchronized externally.  (A structural modification is any operation
 * that adds or deletes one or more elements; merely setting the value of
 * an element is not a structural modification.)  This is typically
 * accomplished by synchronizing on some object that naturally
 * encapsulates the list.
 * <p>
 * 注意，这个实现是不同步的。
 * 如果多个线程并发访问一个链表，并且至少有一个线程结构性地修改了 list，那么它必须在外部同步。
 * （结构性修改是指添加或删除一个或多个元素的任何操作；仅仅设置元素的值不是结构性修改。）
 * 这通常通过在自然封装 list 的某个对象上同步来完成。
 * <p>
 * If no such object exists, the list should be "wrapped" using the
 * {@link Collections#synchronizedList Collections.synchronizedList}
 * method.  This is best done at creation time, to prevent accidental
 * unsynchronized access to the list:<pre>
 *   List list = Collections.synchronizedList(new LinkedList(...));</pre>
 * <p>
 * 如果不存在这样的对象，则应使用 Collections.synchronizedList 方法“包装” list。
 * 最好在创建时完成，以防止对 list 的意外非同步访问：
 * List list = Collections.synchronizedList(new LinkedList(...));
 *
 * <p>The iterators returned by this class's {@code iterator} and
 * {@code listIterator} methods are <i>fail-fast</i>: if the list is
 * structurally modified at any time after the iterator is created, in
 * any way except through the Iterator's own {@code remove} or
 * {@code add} methods, the iterator will throw a {@link
 * ConcurrentModificationException}.  Thus, in the face of concurrent
 * modification, the iterator fails quickly and cleanly, rather than
 * risking arbitrary, non-deterministic behavior at an undetermined
 * time in the future.
 * <p>
 * 该类的 iterator 和 listIterator 方法返回的迭代器是 fail-fast 的：
 * 如果在创建迭代器之后的任何时候以任何方式（除了通过 Iterator 自己的 remove 或 add 方法之外）结构性地修改 list，
 * 迭代器都会抛出 ConcurrentModificationException。
 * 因此，在并发修改的情况下，迭代器很快就会失败，而不是在将来的某个不确定的时间冒着任意的、不确定的行为的风险。
 *
 * <p>Note that the fail-fast behavior of an iterator cannot be guaranteed
 * as it is, generally speaking, impossible to make any hard guarantees in the
 * presence of unsynchronized concurrent modification.  Fail-fast iterators
 * throw {@code ConcurrentModificationException} on a best-effort basis.
 * Therefore, it would be wrong to write a program that depended on this
 * exception for its correctness:   <i>the fail-fast behavior of iterators
 * should be used only to detect bugs.</i>
 * <p>
 * 注意，迭代器的 fail-fast 行为不能保证，因为一般来说，在存在非同步并发修改的情况下，不可能做出任何硬性保证。
 * Fail-fast 迭代器尽最大努力抛出 ConcurrentModificationException。
 * 因此，编写依赖于此异常的程序来保证其正确性是错误的：迭代器的 fail-fast 行为只能用于检测 bug。
 *
 * <p>This class is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @param <E> the type of elements held in this collection
 * @author Josh Bloch
 * @see List
 * @see ArrayList
 * @since 1.2
 */

public class LinkedList<E>
        extends AbstractSequentialList<E>
        implements List<E>, Deque<E>, Cloneable, java.io.Serializable {

    // 都使用 transient 修饰，不参与默认的序列化
    transient int size = 0;

    /**
     * Pointer to first node.
     * Invariant: (first == null && last == null) ||
     * (first.prev == null && first.item != null)
     * <p>
     * 指向首元结点
     * 不变式：(first == null && last == null) || (first.prev == null && first.item != null)
     */
    transient Node<E> first;

    /**
     * Pointer to last node.
     * Invariant: (first == null && last == null) ||
     * (last.next == null && last.item != null)
     * <p>
     * 指向尾结点
     * 不变式：(first == null && last == null) || (last.next == null && last.item != null)
     */
    transient Node<E> last;

    /**
     * Constructs an empty list.
     */
    public LinkedList() {
    }

    /**
     * Constructs a list containing the elements of the specified
     * collection, in the order they are returned by the collection's
     * iterator.
     * <p>
     * 构造一个包含指定 collection 中的元素的列表，这些元素是按照 collection 的迭代器返回的顺序排列的。
     *
     * @param c the collection whose elements are to be placed into this list
     * @throws NullPointerException if the specified collection is null
     */
    public LinkedList(Collection<? extends E> c) {
        // 调用空参构造
        this();
        // 将 c 中的元素添加到 this 中
        addAll(c);
    }

    /**
     * Links e as first element.
     * <p>
     * 将 e 作为首元结点
     */
    private void linkFirst(E e) {
        // f 指向首元结点
        final Node<E> f = first;
        // 封装为 Node，新结点将成为新的首元结点，因此它的后继结点就是f（f 将成为上一个首元结点）
        final Node<E> newNode = new Node<>(null, e, f);
        // 将 first 指向新结点，明确指定新结点为首元结点
        first = newNode;
        // 如果 f 为 null，表示此时链表为空，那么新结点也是尾结点
        if (f == null)
            last = newNode;
        else
            // 否则，将 f 的前驱结点指向新结点
            f.prev = newNode;

        // 更新 size 和 modCount
        size++;
        modCount++;
    }

    /**
     * Links e as last element.
     * <p>
     * 将 e 作为尾结点
     */
    void linkLast(E e) {
        // l 指向尾结点
        final Node<E> l = last;
        // 封装为 Node，新结点将成为新的尾结点，因此它的前驱结点就是l（l 将成为上一个尾结点）
        final Node<E> newNode = new Node<>(l, e, null);
        // 将 last 指向新结点，明确指定新结点为尾结点
        last = newNode;
        // 如果 l 为 null，表示此时链表为空，那么新结点也是首元结点
        if (l == null)
            first = newNode;
        else
            // 否则，将 l 的后继结点指向新结点
            l.next = newNode;

        // 更新 size 和 modCount
        size++;
        modCount++;
    }

    /**
     * Inserts element e before non-null Node succ.
     * <p>
     * 在非空结点 succ 之前插入元素 e
     */
    void linkBefore(E e, Node<E> succ) {
        // assert succ != null;
        // 获取 succ 的前驱结点，将在 e 和 succ 之间插入新结点
        final Node<E> pred = succ.prev;
        // 封装为 Node，新结点的前驱结点为 pred，后继结点为 succ
        final Node<E> newNode = new Node<>(pred, e, succ);
        // 将 succ 的前驱结点指向新结点
        succ.prev = newNode;
        // 如果 pred 为 null，表示 succ 为首元结点，那么新结点也是首元结点
        if (pred == null)
            first = newNode;
        else
            // 否则，将 pred 的后继结点指向新结点
            pred.next = newNode;

        // 更新 size 和 modCount
        size++;
        modCount++;
    }

    /**
     * Unlinks non-null first node f.
     * <p>
     * 删除首元结点 f，这个入参 f 一定是 first；
     * 如果 f 不是 first 将会导致 first～f 的所有结点都被 remove；
     */
    private E unlinkFirst(Node<E> f) {
        // assert f == first && f != null;
        // 先取出 f 结点代表的元素
        final E element = f.item;
        // 获取 f 的后继结点
        final Node<E> next = f.next;
        // 将 f 结点的元素置为 null，help GC
        f.item = null;
        f.next = null; // help GC
        // 将 first 指向 f 的后继结点，明确指定 f 的后继结点为首元结点
        first = next;
        // 如果 next 为 null，表示链表中只有一个结点，那么 last 也置为 null
        if (next == null)
            last = null;
        else
            // 否则，将 next 的前驱结点置为 null，help GC
            next.prev = null;

        // 更新 size 和 modCount
        size--;
        modCount++;
        return element;
    }

    /**
     * Unlinks non-null last node l.
     * <p>
     * 删除尾结点 l，这个入参 l 一定是 last；
     * 如果 l 不是 last 将会导致 l～last 的所有结点都被 remove；
     */
    private E unlinkLast(Node<E> l) {
        // assert l == last && l != null;
        // 先取出 l 结点代表的元素
        final E element = l.item;
        // 获取 l 的前驱结点
        final Node<E> prev = l.prev;
        // 将 l 结点的元素置为 null，help GC
        l.item = null;
        l.prev = null; // help GC
        // 将 last 指向 l 的前驱结点，明确指定 l 的前驱结点为尾结点
        last = prev;
        // 如果 prev 为 null，表示链表中只有一个结点，那么 first 也置为 null
        if (prev == null)
            first = null;
        else
            // 否则，将 prev 的后继结点置为 null，help GC
            prev.next = null;

        // 更新 size 和 modCount
        size--;
        modCount++;
        return element;
    }

    /**
     * Unlinks non-null node x.
     * <p>
     * 删除结点 x，不对 x 的前驱和后继结点做任何限制；
     */
    E unlink(Node<E> x) {
        // assert x != null;
        // 先取出 x 结点代表的元素
        final E element = x.item;
        // 获取 x 的后继结点
        final Node<E> next = x.next;
        final Node<E> prev = x.prev;

        // 如果 prev 为 null，表示 x 为首元结点，那么将 first 指向 x 的后继结点
        if (prev == null) {
            first = next;
        } else {
            // 否则，将 prev 的后继结点指向 x 的后继结点
            prev.next = next;
            x.prev = null;
        }

        // 如果 next 为 null，表示 x 为尾结点，那么将 last 指向 x 的前驱结点
        if (next == null) {
            last = prev;
        } else {
            // 否则，将 next 的前驱结点指向 x 的前驱结点
            next.prev = prev;
            x.next = null;
        }

        // 将 x 结点的元素置为 null，help GC
        x.item = null;

        // 更新 size 和 modCount
        size--;
        modCount++;
        return element;
    }

    /**
     * Returns the first element in this list.
     * <p>
     * 返回 list 的首元结点
     *
     * @return the first element in this list
     * @throws NoSuchElementException if this list is empty
     */
    public E getFirst() {
        // 获取首元结点
        final Node<E> f = first;
        // f == null 代表 list 为空，抛出异常
        if (f == null)
            throw new NoSuchElementException();
        return f.item;
    }

    /**
     * Returns the last element in this list.
     * <p>
     * 返回 list 的尾结点
     *
     * @return the last element in this list
     * @throws NoSuchElementException if this list is empty
     */
    public E getLast() {
        // 获取尾结点
        final Node<E> l = last;
        // l == null 代表 list 为空，抛出异常
        if (l == null)
            throw new NoSuchElementException();
        return l.item;
    }

    /**
     * Removes and returns the first element from this list.
     * <p>
     * 删除并返回 list 的首元结点
     *
     * @return the first element from this list
     * @throws NoSuchElementException if this list is empty
     */
    public E removeFirst() {
        // 获取首元结点
        final Node<E> f = first;
        // f == null 代表 list 为空，抛出异常
        if (f == null)
            throw new NoSuchElementException();
        // 删除首元结点
        return unlinkFirst(f);
    }

    /**
     * Removes and returns the last element from this list.
     * <p>
     * 删除并返回 list 的尾结点
     *
     * @return the last element from this list
     * @throws NoSuchElementException if this list is empty
     */
    public E removeLast() {
        // 获取尾结点
        final Node<E> l = last;
        // l == null 代表 list 为空，抛出异常
        if (l == null)
            throw new NoSuchElementException();
        // 删除尾结点
        return unlinkLast(l);
    }

    /**
     * Inserts the specified element at the beginning of this list.
     * <p>
     * 在 list 的首部插入指定元素
     *
     * @param e the element to add
     */
    public void addFirst(E e) {
        linkFirst(e);
    }

    /**
     * Appends the specified element to the end of this list.
     * <p>
     * 在 list 的尾部插入指定元素
     *
     * <p>This method is equivalent to {@link #add}.
     * <p>
     * 该方法等价于 add 方法
     *
     * @param e the element to add
     */
    public void addLast(E e) {
        linkLast(e);
    }

    /**
     * Returns {@code true} if this list contains the specified element.
     * More formally, returns {@code true} if and only if this list contains
     * at least one element {@code e} such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     * <p>
     * 如果 list 包含指定元素，则返回 true。
     * 更正式地讲，当且仅当 list 包含至少一个满足 (o==null ? e==null : o.equals(e)) 的元素 e 时，返回 true。
     *
     * @param o element whose presence in this list is to be tested
     * @return {@code true} if this list contains the specified element
     */
    public boolean contains(Object o) {
        // 调用 indexOf 方法，如果返回 -1，表示不存在，返回 false
        return indexOf(o) != -1;
    }

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    public int size() {
        return size;
    }

    /**
     * Appends the specified element to the end of this list.
     *
     * <p>This method is equivalent to {@link #addLast}.
     *
     * @param e element to be appended to this list
     * @return {@code true} (as specified by {@link Collection#add})
     */
    public boolean add(E e) {
        linkLast(e);
        return true;
    }

    /**
     * Removes the first occurrence of the specified element from this list,
     * if it is present.  If this list does not contain the element, it is
     * unchanged.  More formally, removes the element with the lowest index
     * {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
     * (if such an element exists).  Returns {@code true} if this list
     * contained the specified element (or equivalently, if this list
     * changed as a result of the call).
     * <p>
     * 如果 list 包含指定元素，则删除该元素的第一次出现（如果存在）。
     * 如果 list 不包含该元素，则它将不会更改。
     * 更正式地讲，删除最低索引 i 的元素，使得 (o==null ? get(i)==null : o.equals(get(i)))（如果存在这样的元素）。
     * 如果 list 包含指定元素，则返回 true（或者，如果此 list 由于调用而更改，则返回 true）。
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if this list contained the specified element
     */
    public boolean remove(Object o) {
        // 如果 o 为 null，表示删除首次出现的 null
        if (o == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null) {
                    unlink(x);
                    return true;
                }
            }
        } else {
            // 否则，删除首次出现的 o，需要调用 equals 方法
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.item)) {
                    unlink(x);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the specified
     * collection's iterator.  The behavior of this operation is undefined if
     * the specified collection is modified while the operation is in
     * progress.  (Note that this will occur if the specified collection is
     * this list, and it's nonempty.)
     * <p>
     * 将指定 collection 中的所有元素按照指定 collection 的迭代器返回的顺序添加到此列表的末尾。
     * 如果在操作进行过程中修改了指定的 collection，则此操作的行为是不确定的。
     * （注意，如果指定的 collection 是此 list，并且它是非空的，则会发生这种情况。）
     *
     * @param c collection containing elements to be added to this list
     * @return {@code true} if this list changed as a result of the call
     * @throws NullPointerException if the specified collection is null
     */
    public boolean addAll(Collection<? extends E> c) {
        // 明确指定从尾部添加
        return addAll(size, c);
    }

    /**
     * Inserts all of the elements in the specified collection into this
     * list, starting at the specified position.  Shifts the element
     * currently at that position (if any) and any subsequent elements to
     * the right (increases their indices).  The new elements will appear
     * in the list in the order that they are returned by the
     * specified collection's iterator.
     * <p>
     * 将指定 collection 中的所有元素从指定的位置开始插入此列表。
     * 将当前在该位置的元素（如果有）和任何后续元素向右移动（增加其索引）。
     * 新元素将按照它们由指定 collection 的迭代器返回的顺序出现在列表中。
     *
     * @param index index at which to insert the first element
     *              from the specified collection
     * @param c     collection containing elements to be added to this list
     * @return {@code true} if this list changed as a result of the call
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @throws NullPointerException      if the specified collection is null
     */
    public boolean addAll(int index, Collection<? extends E> c) {
        // 检查 index 是否越界
        checkPositionIndex(index);

        // 将 c 转换为数组，toArray 方法应当满足 Collection 接口的规范，即返回的数组中的元素顺序和 iterator 迭代的顺序一致
        Object[] a = c.toArray();
        // 判断是否需要添加
        int numNew = a.length;
        if (numNew == 0)
            return false;

        // 定义前驱和后继结点；pred 为前驱结点，succ 为 c 插入之后的后继结点，即当前的 index 位置的结点
        Node<E> pred, succ;
        // index == size 时，表示在尾部添加，此时 succ 为 null，pred 为 last
        if (index == size) {
            succ = null;
            pred = last;
        } else {
            // index != size 时，表示在中间添加，那么需要先找到 index 位置的结点，然后将其作为 succ，其前驱结点作为 pred
            succ = node(index);
            pred = succ.prev;
        }

        // 将数组中的元素添加到 list 中
        for (Object o : a) {
            // 封装为 Node，构造方法中已经建立好了与前驱的关系，其后继结点将在下一次循环中建立
            @SuppressWarnings("unchecked") E e = (E) o;
            Node<E> newNode = new Node<>(pred, e, null);
            // 如果 pred 为 null，表示在首部添加，此时 first 为 newNode；
            if (pred == null)
                first = newNode;
            else
                // 否则，将 pred 的后继结点指向 newNode
                pred.next = newNode;
            // 更新 pred 为 newNode，即向后移动一位
            pred = newNode;
        }

        // 将 c 中的最后一个结点与 succ 建立联系
        // 如果 succ 为 null，表示在尾部添加，此时 last 为 pred
        if (succ == null) {
            last = pred;
        } else {
            // 否则，建立起双向链表的关系
            pred.next = succ;
            succ.prev = pred;
        }

        // 更新 size 和 modCount
        size += numNew;
        modCount++;
        return true;
    }

    /**
     * Removes all of the elements from this list.
     * The list will be empty after this call returns.
     * <p>
     * 删除 list 中的所有元素。
     * 此调用返回后，list 将为空。
     */
    public void clear() {
        // Clearing all of the links between nodes is "unnecessary", but:
        // - helps a generational GC if the discarded nodes inhabit
        //   more than one generation
        // - is sure to free memory even if there is a reachable Iterator
        // 清除结点之间的所有链接是“不必要的”，但是：
        // - 如果废弃的结点占据了多个代，可以帮助分代 GC
        // - 即使有可达的迭代器，也一定会释放内存
        for (Node<E> x = first; x != null; ) {
            // 遍历 list，将每个结点的元素置为 null，help GC
            Node<E> next = x.next;
            x.item = null;
            x.next = null;
            x.prev = null;
            x = next;
        }
        // 将 first 和 last 置为 null，明确指定 list 为空
        first = last = null;
        // 更新 size 和 modCount
        size = 0;
        modCount++;
    }


    // Positional Access Operations

    /**
     * Returns the element at the specified position in this list.
     * <p>
     * 返回 list 中指定位置的元素
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public E get(int index) {
        // 检查 index 是否越界
        checkElementIndex(index);
        // 获取 index 位置的结点，然后返回其元素
        return node(index).item;
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     * <p>
     * 用指定元素替换 list 中指定位置的元素
     *
     * @param index   index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public E set(int index, E element) {
        // 检查 index 是否越界
        checkElementIndex(index);
        // 获取 index 位置的结点，然后替换其元素
        Node<E> x = node(index);
        E oldVal = x.item;
        x.item = element;
        // set 行为不会改变 list 的结构，因此不需要更新 modCount
        return oldVal;
    }

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     * <p>
     * 在 list 的指定位置插入指定元素。
     * 将当前在该位置的元素（如果有）和任何后续元素向右移动（增加其索引）。
     *
     * @param index   index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public void add(int index, E element) {
        // 检查 index 是否越界
        checkPositionIndex(index);

        // 如果 index == size，表示在尾部添加，此时 succ 为 null，pred 为 last
        if (index == size)
            linkLast(element);
        else
            // 否则，表示在中间添加，那么需要先找到 index 位置的结点，然后将其作为 succ，其前驱结点作为 pred
            linkBefore(element, node(index));
    }

    /**
     * Removes the element at the specified position in this list.  Shifts any
     * subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     * <p>
     * 删除 list 中指定位置的元素。
     * 将任何后续元素向左移动（从它们的索引中减去一个）。
     * 返回从 list 中删除的元素。
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public E remove(int index) {
        // 检查 index 是否越界
        checkElementIndex(index);
        // 删除 index 位置的结点
        return unlink(node(index));
    }

    /**
     * Tells if the argument is the index of an existing element.
     * <p>
     * 判断 index 是否为 list 中元素的索引
     */
    private boolean isElementIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Tells if the argument is the index of a valid position for an
     * iterator or an add operation.
     * <p>
     * 判断 index 是否是 iterator 或 add 操作的有效位置
     */
    private boolean isPositionIndex(int index) {
        return index >= 0 && index <= size;
    }

    /**
     * Constructs an IndexOutOfBoundsException detail message.
     * Of the many possible refactorings of the error handling code,
     * this "outlining" performs best with both server and client VMs.
     */
    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + size;
    }

    private void checkElementIndex(int index) {
        if (!isElementIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private void checkPositionIndex(int index) {
        if (!isPositionIndex(index))
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    /**
     * Returns the (non-null) Node at the specified element index.
     * <p>
     * 返回指定索引的结点
     */
    Node<E> node(int index) {
        // assert isElementIndex(index);

        // 如果 index 小于 size 的一半，从首元结点开始遍历
        if (index < (size >> 1)) {
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            // 否则，从尾结点开始遍历
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    // Search Operations

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the lowest index {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     * <p>
     * 返回 list 中指定元素的首次出现的索引，如果 list 不包含该元素，则返回 -1。
     * 更正式地讲，返回最低索引 i，使得 (o==null ? get(i)==null : o.equals(get(i)))，如果不存在这样的索引，则返回 -1。
     *
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    public int indexOf(Object o) {
        int index = 0;
        if (o == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

    /**
     * Returns the index of the last occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     * More formally, returns the highest index {@code i} such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such index.
     * <p>
     * 返回 list 中指定元素的最后一次出现的索引，如果 list 不包含该元素，则返回 -1。
     * 更正式地讲，返回最高索引 i，使得 (o==null ? get(i)==null : o.equals(get(i)))，如果不存在这样的索引，则返回 -1。
     *
     * @param o element to search for
     * @return the index of the last occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    public int lastIndexOf(Object o) {
        // 从尾结点开始遍历
        int index = size;
        if (o == null) {
            for (Node<E> x = last; x != null; x = x.prev) {
                index--;
                if (x.item == null)
                    return index;
            }
        } else {
            for (Node<E> x = last; x != null; x = x.prev) {
                index--;
                if (o.equals(x.item))
                    return index;
            }
        }
        return -1;
    }

    // Queue operations.

    /**
     * Retrieves, but does not remove, the head (first element) of this list.
     * <p>
     * 检索 list 的首元结点，但不删除它。
     *
     * @return the head of this list, or {@code null} if this list is empty
     * @since 1.5
     */
    public E peek() {
        // 获取首元结点
        final Node<E> f = first;
        // 如果 f 为 null，表示 list 为空，返回 null
        return (f == null) ? null : f.item;
    }

    /**
     * Retrieves, but does not remove, the head (first element) of this list.
     * <p>
     * 检索 list 的首元结点，但不删除它。
     *
     * @return the head of this list
     * @throws NoSuchElementException if this list is empty
     * @since 1.5
     */
    public E element() {
        return getFirst();
    }

    /**
     * Retrieves and removes the head (first element) of this list.
     * <p>
     * 检索并删除 list 的首元结点。
     *
     * @return the head of this list, or {@code null} if this list is empty
     * @since 1.5
     */
    public E poll() {
        // 获取首元结点
        final Node<E> f = first;
        // 如果 f 为 null，表示 list 为空，返回 null；否则，删除首元结点
        return (f == null) ? null : unlinkFirst(f);
    }

    /**
     * Retrieves and removes the head (first element) of this list.
     *
     * @return the head of this list
     * @throws NoSuchElementException if this list is empty
     * @since 1.5
     */
    public E remove() {
        return removeFirst();
    }

    /**
     * Adds the specified element as the tail (last element) of this list.
     * <p>
     * 将指定元素添加为 list 的尾结点。
     * LinkedList 不限制元素的个数，因此 add 操作不会失败，所以该方法总是返回 true。
     *
     * @param e the element to add
     * @return {@code true} (as specified by {@link Queue#offer})
     * @since 1.5
     */
    public boolean offer(E e) {
        return add(e);
    }

    // Deque operations

    /**
     * Inserts the specified element at the front of this list.
     *
     * @param e the element to insert
     * @return {@code true} (as specified by {@link Deque#offerFirst})
     * @since 1.6
     */
    public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }

    /**
     * Inserts the specified element at the end of this list.
     *
     * @param e the element to insert
     * @return {@code true} (as specified by {@link Deque#offerLast})
     * @since 1.6
     */
    public boolean offerLast(E e) {
        addLast(e);
        return true;
    }

    /**
     * Retrieves, but does not remove, the first element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the first element of this list, or {@code null}
     * if this list is empty
     * @since 1.6
     */
    public E peekFirst() {
        final Node<E> f = first;
        return (f == null) ? null : f.item;
    }

    /**
     * Retrieves, but does not remove, the last element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the last element of this list, or {@code null}
     * if this list is empty
     * @since 1.6
     */
    public E peekLast() {
        final Node<E> l = last;
        return (l == null) ? null : l.item;
    }

    /**
     * Retrieves and removes the first element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the first element of this list, or {@code null} if
     * this list is empty
     * @since 1.6
     */
    public E pollFirst() {
        final Node<E> f = first;
        return (f == null) ? null : unlinkFirst(f);
    }

    /**
     * Retrieves and removes the last element of this list,
     * or returns {@code null} if this list is empty.
     *
     * @return the last element of this list, or {@code null} if
     * this list is empty
     * @since 1.6
     */
    public E pollLast() {
        final Node<E> l = last;
        return (l == null) ? null : unlinkLast(l);
    }

    /**
     * Pushes an element onto the stack represented by this list.  In other
     * words, inserts the element at the front of this list.
     *
     * <p>This method is equivalent to {@link #addFirst}.
     *
     * @param e the element to push
     * @since 1.6
     */
    public void push(E e) {
        addFirst(e);
    }

    /**
     * Pops an element from the stack represented by this list.  In other
     * words, removes and returns the first element of this list.
     *
     * <p>This method is equivalent to {@link #removeFirst()}.
     *
     * @return the element at the front of this list (which is the top
     * of the stack represented by this list)
     * @throws NoSuchElementException if this list is empty
     * @since 1.6
     */
    public E pop() {
        return removeFirst();
    }

    /**
     * Removes the first occurrence of the specified element in this
     * list (when traversing the list from head to tail).  If the list
     * does not contain the element, it is unchanged.
     * <p>
     * 删除 list 中首次出现的指定元素（从头到尾遍历 list）。
     * 如果 list 不包含该元素，则不做任何改变。
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if the list contained the specified element
     * @since 1.6
     */
    public boolean removeFirstOccurrence(Object o) {
        return remove(o);
    }

    /**
     * Removes the last occurrence of the specified element in this
     * list (when traversing the list from head to tail).  If the list
     * does not contain the element, it is unchanged.
     *
     * @param o element to be removed from this list, if present
     * @return {@code true} if the list contained the specified element
     * @since 1.6
     */
    public boolean removeLastOccurrence(Object o) {
        if (o == null) {
            for (Node<E> x = last; x != null; x = x.prev) {
                if (x.item == null) {
                    unlink(x);
                    return true;
                }
            }
        } else {
            for (Node<E> x = last; x != null; x = x.prev) {
                if (o.equals(x.item)) {
                    unlink(x);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns a list-iterator of the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     * Obeys the general contract of {@code List.listIterator(int)}.
     * <p>
     * 返回 list 中指定位置的元素的列表迭代器（按适当顺序），从 list 中的指定位置开始。
     * 遵守 List.listIterator(int) 的一般契约。
     * <p>
     * The list-iterator is <i>fail-fast</i>: if the list is structurally
     * modified at any time after the Iterator is created, in any way except
     * through the list-iterator's own {@code remove} or {@code add}
     * methods, the list-iterator will throw a
     * {@code ConcurrentModificationException}.  Thus, in the face of
     * concurrent modification, the iterator fails quickly and cleanly, rather
     * than risking arbitrary, non-deterministic behavior at an undetermined
     * time in the future.
     * <p>
     *     list-iterator 是 fail-fast 的：如果 list 在迭代器创建后的任何时候以任何方式（除了通过 list-iterator 自己的 remove 或 add 方法）结构性地修改，
     *     list-iterator 都会抛出 ConcurrentModificationException。
     *     因此，在并发修改的情况下，迭代器会快速而干净地失败，而不是在将来的某个不确定的时间冒着任意的、不确定的行为的风险。
     *
     * @param index index of the first element to be returned from the
     *              list-iterator (by a call to {@code next})
     * @return a ListIterator of the elements in this list (in proper
     * sequence), starting at the specified position in the list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     * @see List#listIterator(int)
     */
    public ListIterator<E> listIterator(int index) {
        // 检查 index 是否越界
        checkPositionIndex(index);
        return new ListItr(index);
    }

    private class ListItr implements ListIterator<E> {
        // 类似 ArrayList 的实现，只是将数组换成了双向链表
        private Node<E> lastReturned;
        private Node<E> next;
        private int nextIndex;
        private int expectedModCount = modCount;

        ListItr(int index) {
            // assert isPositionIndex(index);
            next = (index == size) ? null : node(index);
            nextIndex = index;
        }

        public boolean hasNext() {
            return nextIndex < size;
        }

        public E next() {
            checkForComodification();
            if (!hasNext())
                throw new NoSuchElementException();

            lastReturned = next;
            next = next.next;
            nextIndex++;
            return lastReturned.item;
        }

        public boolean hasPrevious() {
            return nextIndex > 0;
        }

        public E previous() {
            checkForComodification();
            if (!hasPrevious())
                throw new NoSuchElementException();

            lastReturned = next = (next == null) ? last : next.prev;
            nextIndex--;
            return lastReturned.item;
        }

        public int nextIndex() {
            return nextIndex;
        }

        public int previousIndex() {
            return nextIndex - 1;
        }

        public void remove() {
            checkForComodification();
            if (lastReturned == null)
                throw new IllegalStateException();

            Node<E> lastNext = lastReturned.next;
            unlink(lastReturned);
            if (next == lastReturned)
                next = lastNext;
            else
                nextIndex--;
            lastReturned = null;
            expectedModCount++;
        }

        public void set(E e) {
            if (lastReturned == null)
                throw new IllegalStateException();
            checkForComodification();
            lastReturned.item = e;
        }

        public void add(E e) {
            checkForComodification();
            lastReturned = null;
            if (next == null)
                linkLast(e);
            else
                linkBefore(e, next);
            nextIndex++;
            expectedModCount++;
        }

        public void forEachRemaining(Consumer<? super E> action) {
            Objects.requireNonNull(action);
            while (modCount == expectedModCount && nextIndex < size) {
                action.accept(next.item);
                lastReturned = next;
                next = next.next;
                nextIndex++;
            }
            checkForComodification();
        }

        final void checkForComodification() {
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }
    }

    // 双向链表的结点实现
    private static class Node<E> {
        E item;
        Node<E> next;
        Node<E> prev;

        Node(Node<E> prev, E element, Node<E> next) {
            this.item = element;
            this.next = next;
            this.prev = prev;
        }
    }

    /**
     * @since 1.6
     */
    public Iterator<E> descendingIterator() {
        return new DescendingIterator();
    }

    /**
     * Adapter to provide descending iterators via ListItr.previous
     */
    private class DescendingIterator implements Iterator<E> {
        private final ListItr itr = new ListItr(size());

        public boolean hasNext() {
            return itr.hasPrevious();
        }

        public E next() {
            return itr.previous();
        }

        public void remove() {
            itr.remove();
        }
    }

    @SuppressWarnings("unchecked")
    private LinkedList<E> superClone() {
        try {
            return (LinkedList<E>) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError(e);
        }
    }

    /**
     * Returns a shallow copy of this {@code LinkedList}. (The elements
     * themselves are not cloned.)
     *
     * @return a shallow copy of this {@code LinkedList} instance
     */
    public Object clone() {
        LinkedList<E> clone = superClone();

        // Put clone into "virgin" state
        clone.first = clone.last = null;
        clone.size = 0;
        clone.modCount = 0;

        // Initialize clone with our elements
        for (Node<E> x = first; x != null; x = x.next)
            clone.add(x.item);

        return clone;
    }

    /**
     * Returns an array containing all of the elements in this list
     * in proper sequence (from first to last element).
     *
     * <p>The returned array will be "safe" in that no references to it are
     * maintained by this list.  (In other words, this method must allocate
     * a new array).  The caller is thus free to modify the returned array.
     *
     * <p>This method acts as bridge between array-based and collection-based
     * APIs.
     *
     * @return an array containing all of the elements in this list
     * in proper sequence
     */
    public Object[] toArray() {
        Object[] result = new Object[size];
        int i = 0;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.item;
        return result;
    }

    /**
     * Returns an array containing all of the elements in this list in
     * proper sequence (from first to last element); the runtime type of
     * the returned array is that of the specified array.  If the list fits
     * in the specified array, it is returned therein.  Otherwise, a new
     * array is allocated with the runtime type of the specified array and
     * the size of this list.
     *
     * <p>If the list fits in the specified array with room to spare (i.e.,
     * the array has more elements than the list), the element in the array
     * immediately following the end of the list is set to {@code null}.
     * (This is useful in determining the length of the list <i>only</i> if
     * the caller knows that the list does not contain any null elements.)
     *
     * <p>Like the {@link #toArray()} method, this method acts as bridge between
     * array-based and collection-based APIs.  Further, this method allows
     * precise control over the runtime type of the output array, and may,
     * under certain circumstances, be used to save allocation costs.
     *
     * <p>Suppose {@code x} is a list known to contain only strings.
     * The following code can be used to dump the list into a newly
     * allocated array of {@code String}:
     *
     * <pre>
     *     String[] y = x.toArray(new String[0]);</pre>
     * <p>
     * Note that {@code toArray(new Object[0])} is identical in function to
     * {@code toArray()}.
     *
     * @param a the array into which the elements of the list are to
     *          be stored, if it is big enough; otherwise, a new array of the
     *          same runtime type is allocated for this purpose.
     * @return an array containing the elements of the list
     * @throws ArrayStoreException  if the runtime type of the specified array
     *                              is not a supertype of the runtime type of every element in
     *                              this list
     * @throws NullPointerException if the specified array is null
     */
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a) {
        if (a.length < size)
            a = (T[]) java.lang.reflect.Array.newInstance(
                    a.getClass().getComponentType(), size);
        int i = 0;
        Object[] result = a;
        for (Node<E> x = first; x != null; x = x.next)
            result[i++] = x.item;

        if (a.length > size)
            a[size] = null;

        return a;
    }

    private static final long serialVersionUID = 876323262645176354L;

    /**
     * Saves the state of this {@code LinkedList} instance to a stream
     * (that is, serializes it).
     *
     * @serialData The size of the list (the number of elements it
     * contains) is emitted (int), followed by all of its
     * elements (each an Object) in the proper order.
     */
    private void writeObject(java.io.ObjectOutputStream s)
            throws java.io.IOException {
        // Write out any hidden serialization magic
        s.defaultWriteObject();

        // Write out size
        s.writeInt(size);

        // Write out all elements in the proper order.
        for (Node<E> x = first; x != null; x = x.next)
            s.writeObject(x.item);
    }

    /**
     * Reconstitutes this {@code LinkedList} instance from a stream
     * (that is, deserializes it).
     */
    @SuppressWarnings("unchecked")
    private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
        // Read in any hidden serialization magic
        s.defaultReadObject();

        // Read in size
        int size = s.readInt();

        // Read in all elements in the proper order.
        for (int i = 0; i < size; i++)
            linkLast((E) s.readObject());
    }

    /**
     * Creates a <em><a href="Spliterator.html#binding">late-binding</a></em>
     * and <em>fail-fast</em> {@link Spliterator} over the elements in this
     * list.
     *
     * <p>The {@code Spliterator} reports {@link Spliterator#SIZED} and
     * {@link Spliterator#ORDERED}.  Overriding implementations should document
     * the reporting of additional characteristic values.
     *
     * @return a {@code Spliterator} over the elements in this list
     * @implNote The {@code Spliterator} additionally reports {@link Spliterator#SUBSIZED}
     * and implements {@code trySplit} to permit limited parallelism..
     * @since 1.8
     */
    @Override
    public Spliterator<E> spliterator() {
        return new LLSpliterator<E>(this, -1, 0);
    }

    /**
     * A customized variant of Spliterators.IteratorSpliterator
     */
    static final class LLSpliterator<E> implements Spliterator<E> {
        static final int BATCH_UNIT = 1 << 10;  // batch array size increment
        static final int MAX_BATCH = 1 << 25;  // max batch array size;
        final LinkedList<E> list; // null OK unless traversed
        Node<E> current;      // current node; null until initialized
        int est;              // size estimate; -1 until first needed
        int expectedModCount; // initialized when est set
        int batch;            // batch size for splits

        LLSpliterator(LinkedList<E> list, int est, int expectedModCount) {
            this.list = list;
            this.est = est;
            this.expectedModCount = expectedModCount;
        }

        final int getEst() {
            int s; // force initialization
            final LinkedList<E> lst;
            if ((s = est) < 0) {
                if ((lst = list) == null)
                    s = est = 0;
                else {
                    expectedModCount = lst.modCount;
                    current = lst.first;
                    s = est = lst.size;
                }
            }
            return s;
        }

        public long estimateSize() {
            return (long) getEst();
        }

        public Spliterator<E> trySplit() {
            Node<E> p;
            int s = getEst();
            if (s > 1 && (p = current) != null) {
                int n = batch + BATCH_UNIT;
                if (n > s)
                    n = s;
                if (n > MAX_BATCH)
                    n = MAX_BATCH;
                Object[] a = new Object[n];
                int j = 0;
                do {
                    a[j++] = p.item;
                } while ((p = p.next) != null && j < n);
                current = p;
                batch = j;
                est = s - j;
                return Spliterators.spliterator(a, 0, j, Spliterator.ORDERED);
            }
            return null;
        }

        public void forEachRemaining(Consumer<? super E> action) {
            Node<E> p;
            int n;
            if (action == null) throw new NullPointerException();
            if ((n = getEst()) > 0 && (p = current) != null) {
                current = null;
                est = 0;
                do {
                    E e = p.item;
                    p = p.next;
                    action.accept(e);
                } while (p != null && --n > 0);
            }
            if (list.modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }

        public boolean tryAdvance(Consumer<? super E> action) {
            Node<E> p;
            if (action == null) throw new NullPointerException();
            if (getEst() > 0 && (p = current) != null) {
                --est;
                E e = p.item;
                current = p.next;
                action.accept(e);
                if (list.modCount != expectedModCount)
                    throw new ConcurrentModificationException();
                return true;
            }
            return false;
        }

        public int characteristics() {
            return Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SUBSIZED;
        }
    }

}
