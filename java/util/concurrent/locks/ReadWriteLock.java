/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

/**
 * A {@code ReadWriteLock} maintains a pair of associated {@link
 * Lock locks}, one for read-only operations and one for writing.
 * The {@link #readLock read lock} may be held simultaneously by
 * multiple reader threads, so long as there are no writers.  The
 * {@link #writeLock write lock} is exclusive.
 * <p>
 *     ReadWriteLock 维护了一对相关的锁，一个用于只读操作，一个用于写操作。
 *     只读锁可以被多个读线程同时持有，只要没有写线程。
 *     写锁是独占的。
 *
 * <p>All {@code ReadWriteLock} implementations must guarantee that
 * the memory synchronization effects of {@code writeLock} operations
 * (as specified in the {@link Lock} interface) also hold with respect
 * to the associated {@code readLock}. That is, a thread successfully
 * acquiring the read lock will see all updates made upon previous
 * release of the write lock.
 * <p>
 *     所有 ReadWriteLock 实现都必须保证 writeLock 操作的内存同步效果（如 Lock 接口中所指定的）也适用于相关的 readLock。
 *     也就是说，成功获取读锁的线程将看到在释放写锁之后所做的所有更新。
 *
 * <p>A read-write lock allows for a greater level of concurrency in
 * accessing shared data than that permitted by a mutual exclusion lock.
 * It exploits the fact that while only a single thread at a time (a
 * <em>writer</em> thread) can modify the shared data, in many cases any
 * number of threads can concurrently read the data (hence <em>reader</em>
 * threads).
 * In theory, the increase in concurrency permitted by the use of a read-write
 * lock will lead to performance improvements over the use of a mutual
 * exclusion lock. In practice this increase in concurrency will only be fully
 * realized on a multi-processor, and then only if the access patterns for
 * the shared data are suitable.
 * <p>
 *     读写锁允许在访问共享数据时比互斥锁允许的并发级别更高。
 *     它利用了这样一个事实：虽然一次只有一个线程（写线程）可以修改共享数据，但在许多情况下，任意数量的线程可以同时读取数据（因此读线程）。
 *     理论上，使用读写锁可以提高并发性，从而提高性能，而不是使用互斥锁。
 *     实际上，只有在多处理器上，并且只有在共享数据的访问模式适合的情况下，才能充分实现并发性的提高。
 *
 * <p>Whether or not a read-write lock will improve performance over the use
 * of a mutual exclusion lock depends on the frequency that the data is
 * read compared to being modified, the duration of the read and write
 * operations, and the contention for the data - that is, the number of
 * threads that will try to read or write the data at the same time.
 * For example, a collection that is initially populated with data and
 * thereafter infrequently modified, while being frequently searched
 * (such as a directory of some kind) is an ideal candidate for the use of
 * a read-write lock. However, if updates become frequent then the data
 * spends most of its time being exclusively locked and there is little, if any
 * increase in concurrency. Further, if the read operations are too short
 * the overhead of the read-write lock implementation (which is inherently
 * more complex than a mutual exclusion lock) can dominate the execution
 * cost, particularly as many read-write lock implementations still serialize
 * all threads through a small section of code. Ultimately, only profiling
 * and measurement will establish whether the use of a read-write lock is
 * suitable for your application.
 * <p>
 *     读写锁是否会提高性能，取决于数据被读取的频率与被修改的频率、读操作和写操作的持续时间以及对数据的争用——即同时尝试读取或写入数据的线程数。
 *     例如，最初用数据填充并且此后很少修改，同时频繁搜索（例如某种目录）的集合是使用读写锁的理想候选。
 *     但是，如果更新变得频繁，那么数据大部分时间都是被独占锁定的，并且并发性几乎没有增加。
 *     此外，如果读操作太短，读写锁实现的开销（本质上比互斥锁复杂）可能会主导执行成本，特别是因为许多读写锁实现仍然通过一小部分代码串行化所有线程。
 *     最终，只有通过分析和测量才能确定读写锁是否适合您的应用程序。
 *
 *
 * <p>Although the basic operation of a read-write lock is straight-forward,
 * there are many policy decisions that an implementation must make, which
 * may affect the effectiveness of the read-write lock in a given application.
 * Examples of these policies include:
 * <ul>
 * <li>Determining whether to grant the read lock or the write lock, when
 * both readers and writers are waiting, at the time that a writer releases
 * the write lock. Writer preference is common, as writes are expected to be
 * short and infrequent. Reader preference is less common as it can lead to
 * lengthy delays for a write if the readers are frequent and long-lived as
 * expected. Fair, or &quot;in-order&quot; implementations are also possible.
 *
 * <li>Determining whether readers that request the read lock while a
 * reader is active and a writer is waiting, are granted the read lock.
 * Preference to the reader can delay the writer indefinitely, while
 * preference to the writer can reduce the potential for concurrency.
 *
 * <li>Determining whether the locks are reentrant: can a thread with the
 * write lock reacquire it? Can it acquire a read lock while holding the
 * write lock? Is the read lock itself reentrant?
 *
 * <li>Can the write lock be downgraded to a read lock without allowing
 * an intervening writer? Can a read lock be upgraded to a write lock,
 * in preference to other waiting readers or writers?
 *
 * </ul>
 * You should consider all of these things when evaluating the suitability
 * of a given implementation for your application.
 * <p>
 *     // TODO 振聋发聩的思考，读写锁本身还是挺复杂的。
 *     虽然读写锁的基本操作很简单，但实现必须做出许多策略决策，这可能会影响读写锁在特定应用程序中的有效性。
 *     这些策略的示例包括：
 *     1. 在写线程释放写锁时，确定在读线程和写线程都在等待时是否授予读锁或写锁。写线程优先是常见的，因为写操作预计是短暂且不频繁的。
 *     读线程优先较少见，因为如果读线程频繁且长时间存在，则可能导致写操作的长时间延迟。公平或“按顺序”实现也是可能的。
 *     2. 确定在读线程活动且写线程等待时请求读锁的读线程是否被授予读锁。对读线程的偏好可能会无限期地延迟写线程，而对写线程的偏好可能会减少并发的潜力。
 *     3. 确定锁是否可重入：持有写锁的线程是否可以重新获取它？在持有写锁的同时是否可以获取读锁？读锁本身是否可重入？
 *     4. 写锁是否可以降级为读锁，而不允许介入的写线程？读锁是否可以升级为写锁，而不是优先于其他等待的读线程或写线程？
 *     在评估特定实现是否适合您的应用程序时，您应该考虑所有这些事情。
 *
 * @see ReentrantReadWriteLock
 * @see Lock
 * @see ReentrantLock
 *
 * @since 1.5
 * @author Doug Lea
 */
public interface ReadWriteLock {
    /**
     * Returns the lock used for reading.
     * <p>
     *     返回用于读取的锁。
     *
     * @return the lock used for reading
     */
    Lock readLock();

    /**
     * Returns the lock used for writing.
     * <p>
     *     返回用于写入的锁。
     *
     * @return the lock used for writing
     */
    Lock writeLock();
}
