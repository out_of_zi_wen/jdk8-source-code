/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

/**
 * A synchronizer that may be exclusively owned by a thread.  This
 * class provides a basis for creating locks and related synchronizers
 * that may entail a notion of ownership.  The
 * {@code AbstractOwnableSynchronizer} class itself does not manage or
 * use this information. However, subclasses and tools may use
 * appropriately maintained values to help control and monitor access
 * and provide diagnostics.
 * <p>
 * 可以被线程独占的同步器。
 * 这个类提供了创建锁和相关同步器的基础，这些同步器可能包含所有权的概念。
 * AbstractOwnableSynchronizer 类本身不管理或使用这些信息。
 * 然而，子类和工具可以使用适当维护的值来帮助控制和监视访问并提供诊断。
 *
 * @author Doug Lea
 * @since 1.6
 */
public abstract class AbstractOwnableSynchronizer
        implements java.io.Serializable {

    // TODO REMIND AbstractOwnableSynchronizer本身仅仅是下了一个定义，即当前类的实现对象是可以被线程独占的，即所谓的 Ownable；


    /**
     * Use serial ID even though all fields transient.
     * <p>
     * 即使所有字段（即 exclusiveOwnerThead） 都是由 transient 修饰的，但是还是使用了 serial ID；
     */
    private static final long serialVersionUID = 3737899427754241961L;

    /**
     * Empty constructor for use by subclasses.
     */
    protected AbstractOwnableSynchronizer() {
    }

    /**
     * The current owner of exclusive mode synchronization.
     * <p>
     * 记录了以独占的方式持有当前 synchronizer 的线程；
     */
    private transient Thread exclusiveOwnerThread;

    /**
     * Sets the thread that currently owns exclusive access.
     * A {@code null} argument indicates that no thread owns access.
     * This method does not otherwise impose any synchronization or
     * {@code volatile} field accesses.
     * <p>
     *     设置入参 thread 为独占当前 synchronizer 的线程；
     *     如果入参为 null，则表示没有任何线程独占当前 synchronizer，即释放独占(如果 set 方法执行前 exclusiveOwnerThread != null 的话)；
     *
     * @param thread the owner thread
     */
    protected final void setExclusiveOwnerThread(Thread thread) {
        exclusiveOwnerThread = thread;
    }

    /**
     * Returns the thread last set by {@code setExclusiveOwnerThread},
     * or {@code null} if never set.  This method does not otherwise
     * impose any synchronization or {@code volatile} field accesses.
     * <p>
     *     返回上一次通过 setExclusiveOwnerThread 方法设置的线程，或者 null（如果从未设置过）；
     *     这个方法不会强制任何同步或 volatile 字段访问；
     *
     * @return the owner thread
     */
    protected final Thread getExclusiveOwnerThread() {
        return exclusiveOwnerThread;
    }
}
