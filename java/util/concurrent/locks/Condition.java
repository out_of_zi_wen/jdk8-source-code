/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;
import java.util.concurrent.TimeUnit;
import java.util.Date;

/**
 * {@code Condition} factors out the {@code Object} monitor
 * methods ({@link Object#wait() wait}, {@link Object#notify notify}
 * and {@link Object#notifyAll notifyAll}) into distinct objects to
 * give the effect of having multiple wait-sets per object, by
 * combining them with the use of arbitrary {@link Lock} implementations.
 * Where a {@code Lock} replaces the use of {@code synchronized} methods
 * and statements, a {@code Condition} replaces the use of the Object
 * monitor methods.
 * <p>
 *     Condition 将 Object 监视器方法（wait，notify和notifyAll）分解为不同的对象，以便通过与任意 Lock 实现的结合，实现每个对象具有多个等待集的效果。
 *     在 Lock 替换 synchronized 方法和语句的使用时，Condition 替换了 Object 监视器方法的使用。
 *
 * <p>Conditions (also known as <em>condition queues</em> or
 * <em>condition variables</em>) provide a means for one thread to
 * suspend execution (to &quot;wait&quot;) until notified by another
 * thread that some state condition may now be true.  Because access
 * to this shared state information occurs in different threads, it
 * must be protected, so a lock of some form is associated with the
 * condition. The key property that waiting for a condition provides
 * is that it <em>atomically</em> releases the associated lock and
 * suspends the current thread, just like {@code Object.wait}.
 * <p>
 *     Conditions（也称为条件队列或条件变量）提供了一种方法，使一个线程暂停执行（“等待”），直到另一个线程通知某个状态条件现在可能为真。
 *     由于对此共享状态信息的访问发生在不同的线程中，因此必须受到保护，因此与条件关联的是某种形式的锁。
 *     等待条件提供的关键属性是它原子性地释放关联的锁并挂起当前线程，就像 Object.wait 一样。
 *
 * <p>A {@code Condition} instance is intrinsically bound to a lock.
 * To obtain a {@code Condition} instance for a particular {@link Lock}
 * instance use its {@link Lock#newCondition newCondition()} method.
 * <p>
 *     Condition 实例与锁内在绑定。
 *     要为特定 Lock 实例获取 Condition 实例，请使用其 newCondition() 方法。
 *
 * <p>As an example, suppose we have a bounded buffer which supports
 * {@code put} and {@code take} methods.  If a
 * {@code take} is attempted on an empty buffer, then the thread will block
 * until an item becomes available; if a {@code put} is attempted on a
 * full buffer, then the thread will block until a space becomes available.
 * We would like to keep waiting {@code put} threads and {@code take}
 * threads in separate wait-sets so that we can use the optimization of
 * only notifying a single thread at a time when items or spaces become
 * available in the buffer. This can be achieved using two
 * {@link Condition} instances.
 * <pre>
 * class BoundedBuffer {
 *   <b>final Lock lock = new ReentrantLock();</b>
 *   final Condition notFull  = <b>lock.newCondition(); </b>
 *   final Condition notEmpty = <b>lock.newCondition(); </b>
 *
 *   final Object[] items = new Object[100];
 *   int putptr, takeptr, count;
 *
 *   public void put(Object x) throws InterruptedException {
 *     <b>lock.lock();
 *     try {</b>
 *       while (count == items.length)
 *         <b>notFull.await();</b>
 *       items[putptr] = x;
 *       if (++putptr == items.length) putptr = 0;
 *       ++count;
 *       <b>notEmpty.signal();</b>
 *     <b>} finally {
 *       lock.unlock();
 *     }</b>
 *   }
 *
 *   public Object take() throws InterruptedException {
 *     <b>lock.lock();
 *     try {</b>
 *       while (count == 0)
 *         <b>notEmpty.await();</b>
 *       Object x = items[takeptr];
 *       if (++takeptr == items.length) takeptr = 0;
 *       --count;
 *       <b>notFull.signal();</b>
 *       return x;
 *     <b>} finally {
 *       lock.unlock();
 *     }</b>
 *   }
 * }
 * </pre>
 *
 * (The {@link java.util.concurrent.ArrayBlockingQueue} class provides
 * this functionality, so there is no reason to implement this
 * sample usage class.)
 * <p>
 *     举个例子，假设我们有一个支持 put 和 take 方法的有界缓冲区。
 *     如果尝试在空缓冲区上执行 take，则线程将阻塞，直到有项目可用；
 *     如果尝试在满缓冲区上执行 put，则线程将阻塞，直到有空间可用。
 *     我们希望将等待 put 线程和 take 线程保持在单独的等待集中，以便我们可以在缓冲区中的项目或空间可用时一次只通知一个线程的优化。
 *     这可以使用两个 Condition 实例来实现。
 *     class BoundedBuffer {
 *     final Lock lock = new ReentrantLock();
 *     final Condition notFull  = lock.newCondition();
 *     final Condition notEmpty = lock.newCondition();
 *     final Object[] items = new Object[100];
 *     int putptr, takeptr, count;
 *
 *     public void put(Object x) throws InterruptedException {
 *      lock.lock();
 *      try {
 *          while (count == items.length)
 *              notFull.await();
 *          items[putptr] = x;
 *          if (++putptr == items.length) putptr = 0;
 *              ++count;
 *          notEmpty.signal();
 *       } finally {
 *          lock.unlock();
 *       }
 *      }
 *
 *      public Object take() throws InterruptedException {
 *          lock.lock();
 *          try {
 *              while (count == 0)
 *                  notEmpty.await();
 *              Object x = items[takeptr];
 *              if (++takeptr == items.length) takeptr = 0;
 *                  --count;
 *              notFull.signal();
 *              return x;
 *          } finally {
 *              lock.unlock();
 *          }
 *       }
 *      }
 *
 * <p>A {@code Condition} implementation can provide behavior and semantics
 * that is
 * different from that of the {@code Object} monitor methods, such as
 * guaranteed ordering for notifications, or not requiring a lock to be held
 * when performing notifications.
 * If an implementation provides such specialized semantics then the
 * implementation must document those semantics.
 * <p>
 *     Condition 实现可以提供与 Object 监视器方法不同的行为和语义，例如通知的有序性保证，或者在执行通知时不需要持有锁。
 *     如果实现提供了这样的特殊语义，则实现必须记录这些语义。
 *     例如，一些 Condition 实现可能会对等待线程的通知顺序做出保证，或者允许在没有持有锁的情况下执行通知。
 *
 * <p>Note that {@code Condition} instances are just normal objects and can
 * themselves be used as the target in a {@code synchronized} statement,
 * and can have their own monitor {@link Object#wait wait} and
 * {@link Object#notify notification} methods invoked.
 * Acquiring the monitor lock of a {@code Condition} instance, or using its
 * monitor methods, has no specified relationship with acquiring the
 * {@link Lock} associated with that {@code Condition} or the use of its
 * {@linkplain #await waiting} and {@linkplain #signal signalling} methods.
 * It is recommended that to avoid confusion you never use {@code Condition}
 * instances in this way, except perhaps within their own implementation.
 * <p>
 *     请注意，Condition 实例只是普通对象，它们本身可以作为 synchronized 语句中的目标，并且可以调用它们自己的监视器 wait 和 notify 方法。
 *     获取 Condition 实例的监视器锁，或使用它的监视器方法，与获取与该 Condition 关联的 Lock 或使用它的等待和通知方法没有指定的关系。
 *     建议避免混淆，除非在它们自己的实现中，否则不要以这种方式使用 Condition 实例。
 *
 * <p>Except where noted, passing a {@code null} value for any parameter
 * will result in a {@link NullPointerException} being thrown.
 * <p>
 *     除非另有说明，否则为任何参数传递 null 值都将导致抛出 NullPointerException。
 *
 * <h3>Implementation Considerations</h3>
 *
 * <p>When waiting upon a {@code Condition}, a &quot;<em>spurious
 * wakeup</em>&quot; is permitted to occur, in
 * general, as a concession to the underlying platform semantics.
 * This has little practical impact on most application programs as a
 * {@code Condition} should always be waited upon in a loop, testing
 * the state predicate that is being waited for.  An implementation is
 * free to remove the possibility of spurious wakeups but it is
 * recommended that applications programmers always assume that they can
 * occur and so always wait in a loop.
 * <p>
 *     在等待 Condition 时，通常允许发生“虚假唤醒”，这是对底层平台语义的让步。
 *     这对大多数应用程序几乎没有实际影响，因为 Condition 应该总是在循环中等待，测试正在等待的状态谓词。
 *     实现可以自由地消除虚假唤醒的可能性，但建议应用程序员始终假定它们可能发生，因此始终在循环中等待。
 *
 * <p>The three forms of condition waiting
 * (interruptible, non-interruptible, and timed) may differ in their ease of
 * implementation on some platforms and in their performance characteristics.
 * In particular, it may be difficult to provide these features and maintain
 * specific semantics such as ordering guarantees.
 * Further, the ability to interrupt the actual suspension of the thread may
 * not always be feasible to implement on all platforms.
 * <p>
 *     条件等待的三种形式（可中断、不可中断和定时）在某些平台上的实现难度和性能特征可能有所不同。
 *     特别是，可能很难提供这些特性并保持特定的语义，例如顺序保证。
 *     此外，可能无法在所有平台上实现中断线程的实际挂起。
 *
 * <p>Consequently, an implementation is not required to define exactly the
 * same guarantees or semantics for all three forms of waiting, nor is it
 * required to support interruption of the actual suspension of the thread.
 * <p>
 *     因此，实现不需要为所有三种等待形式定义完全相同的保证或语义，也不需要支持中断线程的实际挂起。
 *
 * <p>An implementation is required to
 * clearly document the semantics and guarantees provided by each of the
 * waiting methods, and when an implementation does support interruption of
 * thread suspension then it must obey the interruption semantics as defined
 * in this interface.
 * <p>
 *     实现需要清楚地记录每个等待方法提供的语义和保证，当实现确实支持中断线程挂起时，它必须遵守此接口中定义的中断语义。
 *
 * <p>As interruption generally implies cancellation, and checks for
 * interruption are often infrequent, an implementation can favor responding
 * to an interrupt over normal method return. This is true even if it can be
 * shown that the interrupt occurred after another action that may have
 * unblocked the thread. An implementation should document this behavior.
 * <p>
 *     由于中断通常意味着取消，并且对中断的检查通常很少，因此实现可以优先响应中断而不是正常方法返回。
 *     即使可以证明中断发生在可能解除线程阻塞的另一个操作之后，这也是正确的。实现应记录此行为。
 *
 * @since 1.5
 * @author Doug Lea
 */
public interface Condition {

    /**
     * Causes the current thread to wait until it is signalled or
     * {@linkplain Thread#interrupt interrupted}.
     * <p>
     *     使当前线程等待，直到它被通知或中断。
     *
     * <p>The lock associated with this {@code Condition} is atomically
     * released and the current thread becomes disabled for thread scheduling
     * purposes and lies dormant until <em>one</em> of four things happens:
     * <ul>
     * <li>Some other thread invokes the {@link #signal} method for this
     * {@code Condition} and the current thread happens to be chosen as the
     * thread to be awakened; or
     * <li>Some other thread invokes the {@link #signalAll} method for this
     * {@code Condition}; or
     * <li>Some other thread {@linkplain Thread#interrupt interrupts} the
     * current thread, and interruption of thread suspension is supported; or
     * <li>A &quot;<em>spurious wakeup</em>&quot; occurs.
     * </ul>
     * <p>
     *     与此 Condition 关联的锁被原子性地释放，并且当前线程因线程调度目的而被禁用，并处于休眠状态，直到发生以下四种情况之一：
     *     1. 其他线程调用此 Condition 的 signal 方法，并且当前线程恰好被选择为要唤醒的线程；
     *     2. 其他线程调用此 Condition 的 signalAll 方法；
     *     3. 其他线程中断当前线程，并且支持线程挂起的中断；
     *     4. 发生“虚假唤醒”。
     *
     * <p>In all cases, before this method can return the current thread must
     * re-acquire the lock associated with this condition. When the
     * thread returns it is <em>guaranteed</em> to hold this lock.
     * <p>
     *     在所有情况下，在此方法返回之前，当前线程必须重新获取与此条件关联的锁。当线程返回时，它保证持有此锁。
     *
     * <p>If the current thread:
     * <ul>
     * <li>has its interrupted status set on entry to this method; or
     * <li>is {@linkplain Thread#interrupt interrupted} while waiting
     * and interruption of thread suspension is supported,
     * </ul>
     * then {@link InterruptedException} is thrown and the current thread's
     * interrupted status is cleared. It is not specified, in the first
     * case, whether or not the test for interruption occurs before the lock
     * is released.
     * <p>
     *     如果当前线程：
     *     1. 在进入此方法时设置了中断状态；或
     *     2. 在等待时被中断，并且支持线程挂起的中断，
     *     则抛出 InterruptedException，并清除当前线程的中断状态。
     *     在第一种情况下，未指定在释放锁之前是否进行中断测试。
     *
     * <p><b>Implementation Considerations</b>
     *
     * <p>The current thread is assumed to hold the lock associated with this
     * {@code Condition} when this method is called.
     * It is up to the implementation to determine if this is
     * the case and if not, how to respond. Typically, an exception will be
     * thrown (such as {@link IllegalMonitorStateException}) and the
     * implementation must document that fact.
     *
     * <p>An implementation can favor responding to an interrupt over normal
     * method return in response to a signal. In that case the implementation
     * must ensure that the signal is redirected to another waiting thread, if
     * there is one.
     *
     * @throws InterruptedException if the current thread is interrupted
     *         (and interruption of thread suspension is supported)
     */
    void await() throws InterruptedException;

    /**
     * Causes the current thread to wait until it is signalled.
     * <p>
     *     使当前线程等待，直到它被通知。
     *
     * <p>The lock associated with this condition is atomically
     * released and the current thread becomes disabled for thread scheduling
     * purposes and lies dormant until <em>one</em> of three things happens:
     * <ul>
     * <li>Some other thread invokes the {@link #signal} method for this
     * {@code Condition} and the current thread happens to be chosen as the
     * thread to be awakened; or
     * <li>Some other thread invokes the {@link #signalAll} method for this
     * {@code Condition}; or
     * <li>A &quot;<em>spurious wakeup</em>&quot; occurs.
     * </ul>
     * <p>
     *     与此 Condition 关联的锁被原子性地释放，并且当前线程因线程调度目的而被禁用，并处于休眠状态，直到发生以下三种情况之一：
     *     1. 其他线程调用此 Condition 的 signal 方法，并且当前线程恰好被选择为要唤醒的线程；
     *     2. 其他线程调用此 Condition 的 signalAll 方法；
     *     3. 发生“虚假唤醒”。
     *
     * <p>In all cases, before this method can return the current thread must
     * re-acquire the lock associated with this condition. When the
     * thread returns it is <em>guaranteed</em> to hold this lock.
     * <p>
     *     在所有情况下，在此方法返回之前，当前线程必须重新获取与此条件关联的锁。
     *     当线程返回时，它保证持有此锁。
     *
     * <p>If the current thread's interrupted status is set when it enters
     * this method, or it is {@linkplain Thread#interrupt interrupted}
     * while waiting, it will continue to wait until signalled. When it finally
     * returns from this method its interrupted status will still
     * be set.
     * <p>
     *     如果当前线程在进入此方法时设置了中断状态，或者在等待时被中断，它将继续等待直到被通知。
     *     当它最终从此方法返回时，它的中断状态仍将被设置(reinterrupt)。
     *
     * <p><b>Implementation Considerations</b>
     *
     * <p>The current thread is assumed to hold the lock associated with this
     * {@code Condition} when this method is called.
     * It is up to the implementation to determine if this is
     * the case and if not, how to respond. Typically, an exception will be
     * thrown (such as {@link IllegalMonitorStateException}) and the
     * implementation must document that fact.
     */
    void awaitUninterruptibly();

    /**
     * Causes the current thread to wait until it is signalled or interrupted,
     * or the specified waiting time elapses.
     * <p>
     *     使当前线程等待，直到它被通知或中断，或者经过指定的等待时间。
     *
     * <p>The lock associated with this condition is atomically
     * released and the current thread becomes disabled for thread scheduling
     * purposes and lies dormant until <em>one</em> of five things happens:
     * <ul>
     * <li>Some other thread invokes the {@link #signal} method for this
     * {@code Condition} and the current thread happens to be chosen as the
     * thread to be awakened; or
     * <li>Some other thread invokes the {@link #signalAll} method for this
     * {@code Condition}; or
     * <li>Some other thread {@linkplain Thread#interrupt interrupts} the
     * current thread, and interruption of thread suspension is supported; or
     * <li>The specified waiting time elapses; or
     * <li>A &quot;<em>spurious wakeup</em>&quot; occurs.
     * </ul>
     * <p>
     *     与此 Condition 关联的锁被原子性地释放，并且当前线程因线程调度目的而被禁用，并处于休眠状态，直到发生以下五种情况之一：
     *     1. 其他线程调用此 Condition 的 signal 方法，并且当前线程恰好被选择为要唤醒的线程；
     *     2. 其他线程调用此 Condition 的 signalAll 方法；
     *     3. 其他线程中断当前线程，并且支持线程挂起的中断；
     *     4. 指定的等待时间已经过；
     *     5. 发生“虚假唤醒”。
     *
     * <p>In all cases, before this method can return the current thread must
     * re-acquire the lock associated with this condition. When the
     * thread returns it is <em>guaranteed</em> to hold this lock.
     * <p>
     *     在所有情况下，在此方法返回之前，当前线程必须重新获取与此条件关联的锁。
     *     当线程返回时，它保证持有此锁。
     *
     * <p>If the current thread:
     * <ul>
     * <li>has its interrupted status set on entry to this method; or
     * <li>is {@linkplain Thread#interrupt interrupted} while waiting
     * and interruption of thread suspension is supported,
     * </ul>
     * then {@link InterruptedException} is thrown and the current thread's
     * interrupted status is cleared. It is not specified, in the first
     * case, whether or not the test for interruption occurs before the lock
     * is released.
     * <p>
     *     如果当前线程：
     *     1. 在进入此方法时设置了中断状态；或
     *     2. 在等待时被中断，并且支持线程挂起的中断，
     *     则抛出 InterruptedException，并清除当前线程的中断状态。
     *     在第一种情况下，未指定在释放锁之前是否进行中断测试。
     *
     * <p>The method returns an estimate of the number of nanoseconds
     * remaining to wait given the supplied {@code nanosTimeout}
     * value upon return, or a value less than or equal to zero if it
     * timed out. This value can be used to determine whether and how
     * long to re-wait in cases where the wait returns but an awaited
     * condition still does not hold. Typical uses of this method take
     * the following form:
     *
     *  <pre> {@code
     * boolean aMethod(long timeout, TimeUnit unit) {
     *   long nanos = unit.toNanos(timeout);
     *   lock.lock();
     *   try {
     *     while (!conditionBeingWaitedFor()) {
     *       if (nanos <= 0L)
     *         return false;
     *       nanos = theCondition.awaitNanos(nanos);
     *     }
     *     // ...
     *   } finally {
     *     lock.unlock();
     *   }
     * }}</pre>
     * <p>
     *     该方法返回在返回时给定的 nanosTimeout 值的剩余等待时间的估计值，如果超时，则返回小于或等于零的值。
     *     此值可用于确定是否以及如何重新等待，在等待返回但等待的条件仍然不成立的情况下。
     *
     * <p>Design note: This method requires a nanosecond argument so
     * as to avoid truncation errors in reporting remaining times.
     * Such precision loss would make it difficult for programmers to
     * ensure that total waiting times are not systematically shorter
     * than specified when re-waits occur.
     *
     * <p><b>Implementation Considerations</b>
     *
     * <p>The current thread is assumed to hold the lock associated with this
     * {@code Condition} when this method is called.
     * It is up to the implementation to determine if this is
     * the case and if not, how to respond. Typically, an exception will be
     * thrown (such as {@link IllegalMonitorStateException}) and the
     * implementation must document that fact.
     *
     * <p>An implementation can favor responding to an interrupt over normal
     * method return in response to a signal, or over indicating the elapse
     * of the specified waiting time. In either case the implementation
     * must ensure that the signal is redirected to another waiting thread, if
     * there is one.
     *
     * @param nanosTimeout the maximum time to wait, in nanoseconds
     * @return an estimate of the {@code nanosTimeout} value minus
     *         the time spent waiting upon return from this method.
     *         A positive value may be used as the argument to a
     *         subsequent call to this method to finish waiting out
     *         the desired time.  A value less than or equal to zero
     *         indicates that no time remains.
     * @throws InterruptedException if the current thread is interrupted
     *         (and interruption of thread suspension is supported)
     */
    long awaitNanos(long nanosTimeout) throws InterruptedException;

    /**
     * Causes the current thread to wait until it is signalled or interrupted,
     * or the specified waiting time elapses. This method is behaviorally
     * equivalent to:
     *  <pre> {@code awaitNanos(unit.toNanos(time)) > 0}</pre>
     *
     * @param time the maximum time to wait
     * @param unit the time unit of the {@code time} argument
     * @return {@code false} if the waiting time detectably elapsed
     *         before return from the method, else {@code true}
     * @throws InterruptedException if the current thread is interrupted
     *         (and interruption of thread suspension is supported)
     */
    boolean await(long time, TimeUnit unit) throws InterruptedException;

    /**
     * Causes the current thread to wait until it is signalled or interrupted,
     * or the specified deadline elapses.
     *
     * <p>The lock associated with this condition is atomically
     * released and the current thread becomes disabled for thread scheduling
     * purposes and lies dormant until <em>one</em> of five things happens:
     * <ul>
     * <li>Some other thread invokes the {@link #signal} method for this
     * {@code Condition} and the current thread happens to be chosen as the
     * thread to be awakened; or
     * <li>Some other thread invokes the {@link #signalAll} method for this
     * {@code Condition}; or
     * <li>Some other thread {@linkplain Thread#interrupt interrupts} the
     * current thread, and interruption of thread suspension is supported; or
     * <li>The specified deadline elapses; or
     * <li>A &quot;<em>spurious wakeup</em>&quot; occurs.
     * </ul>
     *
     * <p>In all cases, before this method can return the current thread must
     * re-acquire the lock associated with this condition. When the
     * thread returns it is <em>guaranteed</em> to hold this lock.
     *
     *
     * <p>If the current thread:
     * <ul>
     * <li>has its interrupted status set on entry to this method; or
     * <li>is {@linkplain Thread#interrupt interrupted} while waiting
     * and interruption of thread suspension is supported,
     * </ul>
     * then {@link InterruptedException} is thrown and the current thread's
     * interrupted status is cleared. It is not specified, in the first
     * case, whether or not the test for interruption occurs before the lock
     * is released.
     *
     *
     * <p>The return value indicates whether the deadline has elapsed,
     * which can be used as follows:
     *  <pre> {@code
     * boolean aMethod(Date deadline) {
     *   boolean stillWaiting = true;
     *   lock.lock();
     *   try {
     *     while (!conditionBeingWaitedFor()) {
     *       if (!stillWaiting)
     *         return false;
     *       stillWaiting = theCondition.awaitUntil(deadline);
     *     }
     *     // ...
     *   } finally {
     *     lock.unlock();
     *   }
     * }}</pre>
     *
     * <p><b>Implementation Considerations</b>
     *
     * <p>The current thread is assumed to hold the lock associated with this
     * {@code Condition} when this method is called.
     * It is up to the implementation to determine if this is
     * the case and if not, how to respond. Typically, an exception will be
     * thrown (such as {@link IllegalMonitorStateException}) and the
     * implementation must document that fact.
     *
     * <p>An implementation can favor responding to an interrupt over normal
     * method return in response to a signal, or over indicating the passing
     * of the specified deadline. In either case the implementation
     * must ensure that the signal is redirected to another waiting thread, if
     * there is one.
     *
     * @param deadline the absolute time to wait until
     * @return {@code false} if the deadline has elapsed upon return, else
     *         {@code true}
     * @throws InterruptedException if the current thread is interrupted
     *         (and interruption of thread suspension is supported)
     */
    boolean awaitUntil(Date deadline) throws InterruptedException;

    /**
     * Wakes up one waiting thread.
     * <p>
     *     唤醒一个等待的线程。
     *
     * <p>If any threads are waiting on this condition then one
     * is selected for waking up. That thread must then re-acquire the
     * lock before returning from {@code await}.
     * <p>
     *     如果有任何线程在此条件上等待，则选择一个线程唤醒。
     *     然后该线程必须重新获取锁，然后才能从 await 返回。
     *
     * <p><b>Implementation Considerations</b>
     *
     * <p>An implementation may (and typically does) require that the
     * current thread hold the lock associated with this {@code
     * Condition} when this method is called. Implementations must
     * document this precondition and any actions taken if the lock is
     * not held. Typically, an exception such as {@link
     * IllegalMonitorStateException} will be thrown.
     */
    void signal();

    /**
     * Wakes up all waiting threads.
     * <p>
     *     唤醒所有等待的线程。
     *
     * <p>If any threads are waiting on this condition then they are
     * all woken up. Each thread must re-acquire the lock before it can
     * return from {@code await}.
     * <p>
     *     如果有任何线程在此条件上等待，则它们都将被唤醒。
     *     每个线程必须重新获取锁，然后才能从 await 返回。
     *
     * <p><b>Implementation Considerations</b>
     *
     * <p>An implementation may (and typically does) require that the
     * current thread hold the lock associated with this {@code
     * Condition} when this method is called. Implementations must
     * document this precondition and any actions taken if the lock is
     * not held. Typically, an exception such as {@link
     * IllegalMonitorStateException} will be thrown.
     */
    void signalAll();
}
