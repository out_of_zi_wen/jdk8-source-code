/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

import sun.misc.Unsafe;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Provides a framework for implementing blocking locks and related
 * synchronizers (semaphores, events, etc) that rely on
 * first-in-first-out (FIFO) wait queues.  This class is designed to
 * be a useful basis for most kinds of synchronizers that rely on a
 * single atomic {@code int} value to represent state. Subclasses
 * must define the protected methods that change this state, and which
 * define what that state means in terms of this object being acquired
 * or released.  Given these, the other methods in this class carry
 * out all queuing and blocking mechanics. Subclasses can maintain
 * other state fields, but only the atomically updated {@code int}
 * value manipulated using methods {@link #getState}, {@link
 * #setState} and {@link #compareAndSetState} is tracked with respect
 * to synchronization.
 * <p>
 * 提供了一个实现阻塞锁和相关同步器（信号量、事件等）的框架，这些同步器依赖于先进先出（FIFO）等待队列。
 * 针对大多数依赖一个原子的 int 值表示状态的 Synchronizer 来说， 当前类是一个非常有用的基础抽象类。
 * 子类必须定义当前抽象类中所有由 protected 关键字修饰的能更新上述原子 int 状态值的方法；
 * 所谓的能修改原子 int 状态值的方法，其实就是针对 state 的 acquire 和 release 的操作。
 * 除了上述两个方法，当前类中的其他方法都是用来执行所有的排队和阻塞机制的。
 * 子类可以维护其他状态字段，但是只有使用方法 getState、setState 和 compareAndSetState 来操作的原子更新的 int 值才会被跟踪到同步。
 *
 * <p>Subclasses should be defined as non-public internal helper
 * classes that are used to implement the synchronization properties
 * of their enclosing class.  Class
 * {@code AbstractQueuedSynchronizer} does not implement any
 * synchronization interface.  Instead it defines methods such as
 * {@link #acquireInterruptibly} that can be invoked as
 * appropriate by concrete locks and related synchronizers to
 * implement their public methods.
 * <p>
 * 当前类的子类应该被定义为非 public 的内部辅助类，进而起到帮助实现其外部类的同步属性的作用。
 * 当前类 AbstractQueuedSynchronizer 并没有实现任何同步接口。
 * 相反，它定义了一些方法，比如 acquireInterruptibly，这些方法可以被具体的锁和相关的同步器适当地调用，以实现它们的公共方法。
 *
 *
 * <p>This class supports either or both a default <em>exclusive</em>
 * mode and a <em>shared</em> mode. When acquired in exclusive mode,
 * attempted acquires by other threads cannot succeed. Shared mode
 * acquires by multiple threads may (but need not) succeed. This class
 * does not &quot;understand&quot; these differences except in the
 * mechanical sense that when a shared mode acquire succeeds, the next
 * waiting thread (if one exists) must also determine whether it can
 * acquire as well. Threads waiting in the different modes share the
 * same FIFO queue. Usually, implementation subclasses support only
 * one of these modes, but both can come into play for example in a
 * {@link ReadWriteLock}. Subclasses that support only exclusive or
 * only shared modes need not define the methods supporting the unused mode.
 * <p>
 * 当前类支持默认的 exclusive 模式和 shared 模式。
 * 如果当前 synchronizer 已经被以 exclusive 模式获取了，此时其他线程尝试 acquire 将不会成功。
 * 如果当前 synchronizer 已经被以 shared 模式获取了，此时多个线程尝试 acquire 可能会成功，也可能不会成功。
 * 当前类并不会理解这些区别，除了在机械意义上，即当一个 shared 模式的 acquire 成功时，下一个等待的线程（如果存在的话）也必须确定它是否也可以 acquire。
 * 无论 synchronizer 提供的是 exclusive 模式还是 shared 模式，等待的线程都是共享同一个 FIFO 队列的，即会被存在同一个队列中。
 * 通常情况下，AQS 的子类只支持这两种模式中的一种，但是实际上这两种模式是可以同时被实现的，如 ReadWriteLock。
 * 如果子类只支持 exclusive 或者只支持 shared 模式，那么它们就不需要定义支持另一种模式的方法。
 *
 * <p>This class defines a nested {@link ConditionObject} class that
 * can be used as a {@link Condition} implementation by subclasses
 * supporting exclusive mode for which method {@link
 * #isHeldExclusively} reports whether synchronization is exclusively
 * held with respect to the current thread, method {@link #release}
 * invoked with the current {@link #getState} value fully releases
 * this object, and {@link #acquire}, given this saved state value,
 * eventually restores this object to its previous acquired state.  No
 * {@code AbstractQueuedSynchronizer} method otherwise creates such a
 * condition, so if this constraint cannot be met, do not use it.  The
 * behavior of {@link ConditionObject} depends of course on the
 * semantics of its synchronizer implementation.
 * <p>
 * AQS 定义了一个内部类 ConditionObject，它可以被用作子类的 Condition 实现，特别是对于支持 exclusive 模式的子类。
 * 对于上述这种子类，isHeldExclusively 方法明确了当前线程是否独占了同步，
 * release 方法使用当前 getState 的值完全释放了这个对象，
 * acquire 方法使用保存的状态值，最终将这个对象恢复到之前的获取状态。
 * 除了这些方法，AbstractQueuedSynchronizer 类中没有其他方法可以创建这样的条件，所以如果这个约束不能被满足，那么就不要使用它。
 * 当然，ConditionObject 的行为取决于它的同步器实现的语义。
 *
 * <p>This class provides inspection, instrumentation, and monitoring
 * methods for the internal queue, as well as similar methods for
 * condition objects. These can be exported as desired into classes
 * using an {@code AbstractQueuedSynchronizer} for their
 * synchronization mechanics.
 * <p>
 * 当前类提供了针对内部队列的检查、检测和监控方法，以及针对内部 condition 对象的类似方法。
 * 这些方法可以根据需要导出到使用 AbstractQueuedSynchronizer 的类中，以用于它们的同步机制。
 *
 * <p>Serialization of this class stores only the underlying atomic
 * integer maintaining state, so deserialized objects have empty
 * thread queues. Typical subclasses requiring serializability will
 * define a {@code readObject} method that restores this to a known
 * initial state upon deserialization.
 * <p>
 * 当前类的序列化只存储了维护状态的底层原子 int 整数，所以反序列化的对象会有空的线程队列。
 * 需要可序列化的典型子类将定义一个 readObject 方法，以便在反序列化时将其恢复到已知的初始状态。
 *
 * <h3>Usage</h3>
 *
 * <p>To use this class as the basis of a synchronizer, redefine the
 * following methods, as applicable, by inspecting and/or modifying
 * the synchronization state using {@link #getState}, {@link
 * #setState} and/or {@link #compareAndSetState}:
 *
 * <ul>
 * <li> {@link #tryAcquire}
 * <li> {@link #tryRelease}
 * <li> {@link #tryAcquireShared}
 * <li> {@link #tryReleaseShared}
 * <li> {@link #isHeldExclusively}
 * </ul>
 * <p>
 * Each of these methods by default throws {@link
 * UnsupportedOperationException}.  Implementations of these methods
 * must be internally thread-safe, and should in general be short and
 * not block. Defining these methods is the <em>only</em> supported
 * means of using this class. All other methods are declared
 * {@code final} because they cannot be independently varied.
 * <p>
 *     上述的所有方法都会默认抛出 UnsupportedOperationException 异常。
 *     子类针对这些方法的实现必须是线程安全的，通常应该是短小的，不会阻塞的。
 *     定义这些方法是使用当前类的唯一支持的方式。
 *     所有其他方法都被声明为 final，因为它们不能被独立地重写，否则会破坏整体的功能。
 *
 * <p>You may also find the inherited methods from {@link
 * AbstractOwnableSynchronizer} useful to keep track of the thread
 * owning an exclusive synchronizer.  You are encouraged to use them
 * -- this enables monitoring and diagnostic tools to assist users in
 * determining which threads hold locks.
 * <p>
 *     你可能还会发现从 AbstractOwnableSynchronizer 继承的方法对于跟踪独占同步器的线程非常有用。
 *     鼓励你使用它们，这样可以使监控和诊断工具帮助用户确定哪些线程持有锁。
 *
 * <p>Even though this class is based on an internal FIFO queue, it
 * does not automatically enforce FIFO acquisition policies.  The core
 * of exclusive synchronization takes the form:
 *
 * <pre>
 * Acquire:
 *     while (!tryAcquire(arg)) {
 *        <em>enqueue thread if it is not already queued</em>;
 *        <em>possibly block current thread</em>;
 *     }
 *
 * Release:
 *     if (tryRelease(arg))
 *        <em>unblock the first queued thread</em>;
 * </pre>
 * <p>
 * (Shared mode is similar but may involve cascading signals.)
 *
 * <p id="barging">Because checks in acquire are invoked before
 * enqueuing, a newly acquiring thread may <em>barge</em> ahead of
 * others that are blocked and queued.  However, you can, if desired,
 * define {@code tryAcquire} and/or {@code tryAcquireShared} to
 * disable barging by internally invoking one or more of the inspection
 * methods, thereby providing a <em>fair</em> FIFO acquisition order.
 * In particular, most fair synchronizers can define {@code tryAcquire}
 * to return {@code false} if {@link #hasQueuedPredecessors} (a method
 * specifically designed to be used by fair synchronizers) returns
 * {@code true}.  Other variations are possible.
 * <p>
 *     因为在 acquire 方法中的检查是在入队之前调用的，所以一个新获取的线程可能会在其他被阻塞和排队的线程之前插队。
 *     但是，如果需要的话，你可以定义 tryAcquire 和/或 tryAcquireShared 方法来禁用 barging，方法是在内部调用一个或多个检查方法，从而提供公平的 FIFO 获取顺序。
 *     特别是，大多数公平的同步器可以定义 tryAcquire 方法，如果 hasQueuedPredecessors 方法返回 true，那么 tryAcquire 就返回 false。
 *
 * <p>Throughput and scalability are generally highest for the
 * default barging (also known as <em>greedy</em>,
 * <em>renouncement</em>, and <em>convoy-avoidance</em>) strategy.
 * While this is not guaranteed to be fair or starvation-free, earlier
 * queued threads are allowed to recontend before later queued
 * threads, and each recontention has an unbiased chance to succeed
 * against incoming threads.  Also, while acquires do not
 * &quot;spin&quot; in the usual sense, they may perform multiple
 * invocations of {@code tryAcquire} interspersed with other
 * computations before blocking.  This gives most of the benefits of
 * spins when exclusive synchronization is only briefly held, without
 * most of the liabilities when it isn't. If so desired, you can
 * augment this by preceding calls to acquire methods with
 * "fast-path" checks, possibly prechecking {@link #hasContended}
 * and/or {@link #hasQueuedThreads} to only do so if the synchronizer
 * is likely not to be contended.
 * <p>
 *     默认的 barging 策略通常具有最高的吞吐量和可伸缩性（也称为贪婪、放弃和避免车队）。
 *     虽然这不能保证是公平的或者无饥饿的，但是先前排队的线程被允许在后排队的线程之前重新竞争，每次重新竞争都有一个公平的机会来成功地对抗新来的线程。
 *     此外，虽然 acquire 方法不会像通常那样“自旋”，但是它们可能会在阻塞之前执行多次 tryAcquire 方法的调用，这些调用可能会与其他计算交替进行。
 *     当独占同步只是短暂持有时，这样做可以获得自旋的大部分好处，而不会有大部分的缺点。
 *     如果需要的话，你可以在调用 acquire 方法之前增加“快速路径”检查，可能会预先检查 hasContended 和/或 hasQueuedThreads，以便在同步器可能不会被争用时才这样做。
 *
 * <p>This class provides an efficient and scalable basis for
 * synchronization in part by specializing its range of use to
 * synchronizers that can rely on {@code int} state, acquire, and
 * release parameters, and an internal FIFO wait queue. When this does
 * not suffice, you can build synchronizers from a lower level using
 * {@link java.util.concurrent.atomic atomic} classes, your own custom
 * {@link java.util.Queue} classes, and {@link LockSupport} blocking
 * support.
 * <p>
 *     当前类提供了一个高效和可伸缩的同步基础，部分原因是它专门将其使用范围专门用于可以依赖 int 状态、acquire 和 release 参数以及内部 FIFO 等待队列的同步器。
 *     当这些都不够时，你可以使用更低级别的类构建同步器，使用 atomic 类、自定义的 queue 队列类和 LockSupport 阻塞支持。
 *
 * <h3>Usage Examples</h3>
 *
 * <p>Here is a non-reentrant mutual exclusion lock class that uses
 * the value zero to represent the unlocked state, and one to
 * represent the locked state. While a non-reentrant lock
 * does not strictly require recording of the current owner
 * thread, this class does so anyway to make usage easier to monitor.
 * It also supports conditions and exposes
 * one of the instrumentation methods:
 *
 *  <pre> {@code
 * class Mutex implements Lock, java.io.Serializable {
 *
 *   // Our internal helper class
 *   // 静态内部类继承自 AQS
 *   private static class Sync extends AbstractQueuedSynchronizer {
 *     // Reports whether in locked state
 *     // 明确了当前同步器是否被独占
 *     protected boolean isHeldExclusively() {
 *       return getState() == 1;
 *     }
 *
 *     // Acquires the lock if state is zero
 *     public boolean tryAcquire(int acquires) {
 *       assert acquires == 1; // Otherwise unused
 *       // CAS 操作，如果当前 state 为 0，则设置为 1
 *       // 那么也就是说，单纯的 tryAcquire 方法是不支持重入的
 *       if (compareAndSetState(0, 1)) {
 *         setExclusiveOwnerThread(Thread.currentThread());
 *         return true;
 *       }
 *       return false;
 *     }
 *
 *     // Releases the lock by setting state to zero
 *     protected boolean tryRelease(int releases) {
 *       assert releases == 1; // Otherwise unused
 *       if (getState() == 0) throw new IllegalMonitorStateException();
 *       setExclusiveOwnerThread(null);
 *       // 同样的，这里被直接设置为了 0，而不是减 1
 *       setState(0);
 *       return true;
 *     }
 *
 *     // Provides a Condition
 *     Condition newCondition() { return new ConditionObject(); }
 *
 *     // Deserializes properly
 *     private void readObject(ObjectInputStream s)
 *         throws IOException, ClassNotFoundException {
 *       s.defaultReadObject();
 *       setState(0); // reset to unlocked state
 *     }
 *   }
 *
 *   // The sync object does all the hard work. We just forward to it.
 *   private final Sync sync = new Sync();
 *
 *   public void lock()                { sync.acquire(1); }
 *   public boolean tryLock()          { return sync.tryAcquire(1); }
 *   public void unlock()              { sync.release(1); }
 *   public Condition newCondition()   { return sync.newCondition(); }
 *   public boolean isLocked()         { return sync.isHeldExclusively(); }
 *   public boolean hasQueuedThreads() { return sync.hasQueuedThreads(); }
 *   public void lockInterruptibly() throws InterruptedException {
 *     sync.acquireInterruptibly(1);
 *   }
 *   public boolean tryLock(long timeout, TimeUnit unit)
 *       throws InterruptedException {
 *     return sync.tryAcquireNanos(1, unit.toNanos(timeout));
 *   }
 * }}</pre>
 *
 * <p>Here is a latch class that is like a
 * {@link java.util.concurrent.CountDownLatch CountDownLatch}
 * except that it only requires a single {@code signal} to
 * fire. Because a latch is non-exclusive, it uses the {@code shared}
 * acquire and release methods.
 *
 *  <pre> {@code
 * class BooleanLatch {
 *
 *   private static class Sync extends AbstractQueuedSynchronizer {
 *     boolean isSignalled() { return getState() != 0; }
 *
 *     protected int tryAcquireShared(int ignore) {
 *       return isSignalled() ? 1 : -1;
 *     }
 *
 *     protected boolean tryReleaseShared(int ignore) {
 *       setState(1);
 *       return true;
 *     }
 *   }
 *
 *   private final Sync sync = new Sync();
 *   public boolean isSignalled() { return sync.isSignalled(); }
 *   public void signal()         { sync.releaseShared(1); }
 *   public void await() throws InterruptedException {
 *     sync.acquireSharedInterruptibly(1);
 *   }
 * }}</pre>
 *
 * @author Doug Lea
 * @since 1.5
 */
public abstract class AbstractQueuedSynchronizer
        extends AbstractOwnableSynchronizer
        implements java.io.Serializable {

    // TODO REMIND AQS 本身其实仅仅提供了一些能够帮助实现线程安全的方法和属性，但是具体的同步逻辑还是需要由子类来实现的。
    // 并且一般情况下，子类也不会立即对外开放作为同步类来使用，而是再作为一个内部类来使用；
    // 即 wrapper 对外提供线程安全的功能，AQSImpl 作为 wrapper 的内部类保障线程安全；
    // ---
    // 需要整理一下方法的脉络，这样会清楚一点；名字类似的方法的调用逻辑大致是相似的

    private static final long serialVersionUID = 7373984972572414691L;

    /**
     * Creates a new {@code AbstractQueuedSynchronizer} instance
     * with initial synchronization state of zero.
     * <p>
     * 创建一个新的 AbstractQueuedSynchronizer 实例，初始的同步状态为 0。
     */
    protected AbstractQueuedSynchronizer() {
    }

    /**
     * Inserts node into queue, initializing if necessary. See picture above.
     * <p>
     * 将节点插入到 aqs 队列的队尾，必要时进行初始化。
     *
     * @param node the node to insert
     * @return node's predecessor  会返回 node 插入后的前驱结点
     */
    private Node enq(final Node node) {
        for (; ; ) {
            // 获取尾结点
            Node t = tail;
            // 如果尾结点不存在，说明是第一次插入
            if (t == null) { // Must initialize
                // cas 初始化，将 aqs 队列中的 head 和 tail 都设置为指向 new Node() 对象，其 waitStatus==0
                // 注意：之后通过 for 循环再次 enq
                if (compareAndSetHead(new Node()))
                    tail = head;
            } else {
                // 否则说明队列已经初始化过了，直接插入到队尾
                // 需要同时设置原尾结点和当前结点的信息
                node.prev = t;
                if (compareAndSetTail(t, node)) {
                    t.next = node;
                    // 这个 t 可能是 newNode（enq 的同时执行了初始化的话）
                    return t;
                }
            }
        }
    }

    /**
     * Head of the wait queue, lazily initialized.  Except for
     * initialization, it is modified only via method setHead.  Note:
     * If head exists, its waitStatus is guaranteed not to be
     * CANCELLED.
     * <p>
     * 等待队列的头部，延迟初始化。
     * 除了初始化之外，它只能通过 setHead 方法进行修改。
     * 注意：如果 head 存在（不是 null），那么它的 waitStatus 肯定不是 CANCELLED。
     */
    private transient volatile Node head;

    /**
     * Tail of the wait queue, lazily initialized.  Modified only via
     * method enq to add new wait node.
     * <p>
     * 等待队列的尾部，延迟初始化。
     * 只能通过 enq 方法添加新的等待节点来修改。
     */
    private transient volatile Node tail;

    /**
     * The synchronization state.
     * <p>
     * 同步状态。
     * 描述的是 AQS 的同步状态。
     */
    private volatile int state;

    /**
     * Returns the current value of synchronization state.
     * This operation has memory semantics of a {@code volatile} read.
     * <p>
     * 返回当前同步状态的值。
     * 此操作具有 volatile 读的内存语义。
     *
     * @return current state value
     */
    protected final int getState() {
        return state;
    }

    /**
     * Sets the value of synchronization state.
     * This operation has memory semantics of a {@code volatile} write.
     * <p>
     * 设置同步状态的值。
     * 此操作具有 volatile 写的内存语义。
     *
     * @param newState the new state value
     */
    protected final void setState(int newState) {
        state = newState;
    }

    /**
     * Atomically sets synchronization state to the given updated
     * value if the current state value equals the expected value.
     * This operation has memory semantics of a {@code volatile} read
     * and write.
     * <p>
     * 如果当前状态值等于期望值，则将 status 原子地设置为给定的更新值。
     * 此操作具有 volatile 读和写的内存语义。
     *
     * @param expect the expected value
     * @param update the new value
     * @return {@code true} if successful. False return indicates that the actual
     * value was not equal to the expected value.
     */
    protected final boolean compareAndSetState(int expect, int update) {
        // See below for intrinsics setup to support this
        return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
    }

    // Queuing utilities

    /**
     * The number of nanoseconds for which it is faster to spin
     * rather than to use timed park. A rough estimate suffices
     * to improve responsiveness with very short timeouts.
     * <p>
     * 以纳秒为单位的时间，它比使用 timed park 更快。
     * 粗略的估计足以提高对非常短超时的响应性。
     */
    static final long spinForTimeoutThreshold = 1000L;

    /**
     * Transfers a node from a condition queue onto sync queue.
     * Returns true if successful.
     * <p>
     * 将节点从条件队列转移到同步队列。
     * 如果成功返回true。
     *
     * @param node the node
     * @return true if successfully transferred (else the node was
     * cancelled before signal)
     */
    final boolean transferForSignal(Node node) {
        /*
         * If cannot change waitStatus, the node has been cancelled.
         * <p>
         *     尝试将 node 的 waitStatus 从 CONDITION 改为 0；
         *     如果失败，则说明 node 的目前并不处于 condition 状态，自然不需要 signal（可能其他线程已经把它处理了）
         */
        if (!compareAndSetWaitStatus(node, Node.CONDITION, 0))
            return false;

        /*
         * Splice onto queue and try to set waitStatus of predecessor to
         * indicate that thread is (probably) waiting. If cancelled or
         * attempt to set waitStatus fails, wake up to resync (in which
         * case the waitStatus can be transiently and harmlessly wrong).
         * <p>
         *     将 node 加入到 aqs 的等待队列中（aqs 队列而非 condition 队列）；
         *     并尝试将 node 的前驱节点的 waitStatus 设置为 SIGNAL；
         *     如果前驱节点已经取消或者设置失败，唤醒 node 的线程，重新同步；
         */
        // p 是 node 被插入后的 aqs 队列中的前驱节点
        Node p = enq(node);
        // 取到 p 的 waitStatus，即前驱结点的 waitStatus
        // ws 可能等于 0，表示 enq 的同时执行了初始化；
        int ws = p.waitStatus;
        // 如果前驱结点的 waitStatus 大于 0(p 的 waitStatus 为 CANCELLED) || 尝试将 p 的 waitStatus 设置为 SIGNAL 失败，则唤醒 node 的线程
        // 思考为什么需要唤醒？思考 signal 的语义即表示 -> 当一个节点的状态被设置为 SIGNAL 时，意味着后继节点（如果存在）的线程需要被唤醒。
        // 也就是说，此时前驱结点可能被 cancel 了，或者在 cas 期间，waitStatus 改变了，那么此时我们就可以立即唤醒 node 的线程，让它尝试获取锁。
        // 所以可以把它看作一种错误恢复机制
        if (ws > 0 || !compareAndSetWaitStatus(p, ws, Node.SIGNAL))
            LockSupport.unpark(node.thread);
        return true;
    }

    /**
     * Creates and enqueues node for current thread and given mode.
     * <p>
     * 为当前线程和给定模式创建结点并排队节点。
     *
     * @param mode Node.EXCLUSIVE for exclusive, Node.SHARED for shared 入参代表模式
     * @return the new node
     */
    private Node addWaiter(Node mode) {
        // 将当前线程封装成一个 Node
        Node node = new Node(Thread.currentThread(), mode);
        // Try the fast path of enq; backup to full enq on failure
        // 使用一种快速的方式将当前节点插入到队列中，如果失败则使用完整的 enq 方法
        Node pred = tail;
        if (pred != null) {
            node.prev = pred;
            // cas 设置 tail 指向新的 node
            if (compareAndSetTail(pred, node)) {
                // 设置原尾结点的 next 指向新的 node
                pred.next = node;
                return node;
            }
        }
        // 否则，使用 enq 方法进队；
        // 有哪些可能会使用 enq 方法进队呢？
        // 1. tail 为 null；即队列未初始化；
        // 2. cas 设置 tail 失败；需要通过 enq 的循环 cas 实现进队；
        enq(node);
        return node;
    }

    /**
     * Sets head of queue to be node, thus dequeuing. Called only by
     * acquire methods.  Also nulls out unused fields for sake of GC
     * and to suppress unnecessary signals and traversals.
     * <p>
     * 将队列的头部设置为 node，因此出队。
     * 仅由 acquire 方法调用。
     * 为了 GC 和抑制不必要的信号和遍历，还会将未使用的字段置空。
     *
     * @param node the node
     */
    private void setHead(Node node) {
        // 直接将 head 指向 node，并且情况 node 的一些字段；
        // 一般在 acquire 成功之后调用；
        head = node;
        node.thread = null;
        node.prev = null;
    }

    /**
     * Wakes up node's successor, if one exists.
     * <p>
     * 针对共享模式中传入的结点（一般为头结点），唤醒其后序结点
     *
     * @param node the node 传入的结点，目前 node 已苏醒，预期通过调用当前方法，唤醒 node 的后继结点
     */
    private void unparkSuccessor(Node node) {
        /*
         * If status is negative (i.e., possibly needing signal) try
         * to clear in anticipation of signalling.  It is OK if this
         * fails or if status is changed by waiting thread.
         * <p>
         *     如果状态是负数（即，可能需要信号），则尝试清除以预期信号。
         *    如果这失败了，或者状态被等待线程改变了，那也没关系。
         */
        // step1 cas 更新 node 的 ws，即使更新失败也无碍
        int ws = node.waitStatus;
        // 尝试通过一次 cas 将 node 的 waitStatus 设置为 0；
        // waitStatus >= 0 意味着结点对应的线程不会被阻塞；
        // waitStatus == -1 的时候，结点对应的线程会阻塞；
        if (ws < 0)
            compareAndSetWaitStatus(node, ws, 0);

        /*
         * Thread to unpark is held in successor, which is normally
         * just the next node.  But if cancelled or apparently null,
         * traverse backwards from tail to find the actual
         * non-cancelled successor.
         * <p>
         *     要唤醒的线程保存在后继节点中，通常只是下一个节点。
         *     但是如果后继结点被取消或者为 null，那么从尾部向前遍历，找到实际的未被取消的后继节点。
         */
        // step2 取到 node 的有效后继结点；
        // 可能最终 s 会再指向 node
        Node s = node.next;
        // 如果下一个结点为 null 或者下一个结点的 waitStatus > 0（被 cancel 了）
        if (s == null || s.waitStatus > 0) {
            s = null;
            // 从 tail 向前找；直到找到一个 null 的后继结点
            for (Node t = tail; t != null && t != node; t = t.prev)
                // 仅当遍历期间的 node 的 waitStatus <= 0 时（处于阻塞状态）才会赋值给 s
                if (t.waitStatus <= 0)
                    s = t;
        }
        // step3 唤醒 node 的有效后继结点
        // s != null 说明找到了一个未被 cancel 的后继结点，需要被唤醒
        if (s != null)
            LockSupport.unpark(s.thread);
    }

    /**
     * Release action for shared mode -- signals successor and ensures
     * propagation. (Note: For exclusive mode, release just amounts
     * to calling unparkSuccessor of head if it needs signal.)
     * <p>
     * 共享模式的释放操作——信号后继节点并确保传播。
     * （注意：对于独占模式，释放只是调用 head 的 unparkSuccessor 方法，如果它需要信号。）
     */
    private void doReleaseShared() {
        /*
         * Ensure that a release propagates, even if there are other
         * in-progress acquires/releases.  This proceeds in the usual
         * way of trying to unparkSuccessor of head if it needs
         * signal. But if it does not, status is set to PROPAGATE to
         * ensure that upon release, propagation continues.
         * Additionally, we must loop in case a new node is added
         * while we are doing this. Also, unlike other uses of
         * unparkSuccessor, we need to know if CAS to reset status
         * fails, if so rechecking.
         * <p>
         *     确保释放的行为能被传播，即使有其他正在进行的获取/释放操作。
         *     这是通过尝试 unparkSuccessor 方法来实现的，如果 head 需要信号的话。
         *     但是如果它不需要，状态将被设置为 PROPAGATE，以确保在释放时传播继续。
         *     此外，我们必须循环，以防我们在执行此操作时添加了一个新节点。
         *     此外，与 unparkSuccessor 的其他用途不同，我们需要知道 CAS 重置状态是否失败，如果失败则重新检查。
         */
        for (; ; ) {
            // 取到头结点，其实是首元结点
            Node h = head;
            // 如果队列中有结点
            if (h != null && h != tail) {
                // 获取头结点的状态
                int ws = h.waitStatus;
                // 如果头结点的状态为 SIGNAL，说明后继结点需要被唤醒，则 cas 将头结点的状态设置为 0
                if (ws == Node.SIGNAL) {
                    if (!compareAndSetWaitStatus(h, Node.SIGNAL, 0))
                        continue;            // loop to recheck cases
                    // 唤醒后继结点
                    unparkSuccessor(h);

                    // 否则如果头结点的状态为 0，那么就 cas 将头结点的状态设置为 PROPAGATE
                } else if (ws == 0 &&
                        !compareAndSetWaitStatus(h, 0, Node.PROPAGATE))
                    continue;                // loop on failed CAS
            }
            // 如果头结点已经变了，那么就继续循环；否则退出循环
            if (h == head)                   // loop if head changed
                break;
        }
    }

    /**
     * Sets head of queue, and checks if successor may be waiting
     * in shared mode, if so propagating if either propagate > 0 or
     * PROPAGATE status was set.
     * <p>
     * 设置队列的头结点，并检查后继节点是否可能以共享模式等待。
     * 如果是，则传播，如果 propagate > 0 或者设置了 PROPAGATE 状态。
     *
     * @param node      the node
     * @param propagate the return value from a tryAcquireShared tryAcquireShared 方法的返回值，也就是说这个方法只有在尝试以共享模式获取锁时才会被调用
     */
    private void setHeadAndPropagate(Node node, int propagate) {
        // 取出旧的头结点
        Node h = head; // Record old head for check below
        // cas 将新的结点设置为头结点
        setHead(node);
        /*
         * Try to signal next queued node if:
         *   Propagation was indicated by caller,
         *     or was recorded (as h.waitStatus either before
         *     or after setHead) by a previous operation
         *     (note: this uses sign-check of waitStatus because
         *      PROPAGATE status may transition to SIGNAL.)
         * and
         *   The next node is waiting in shared mode,
         *     or we don't know, because it appears null
         * <p>
         *     尝试唤醒下一个在队里的结点，如果满足以下条件：
         *     1. 调用者指示了传播，
         *        或者被前一个操作记录了（如 h.waitStatus 在 setHead 之前或之后），
         *        （注意：这里使用 waitStatus 的符号检查，因为 PROPAGATE 状态可能转换为 SIGNAL。）
         *     并且
         *     2. 下一个节点正在以共享模式等待，
         *        或者我们不知道，因为它看起来是 null
         *
         *
         * The conservatism in both of these checks may cause
         * unnecessary wake-ups, but only when there are multiple
         * racing acquires/releases, so most need signals now or soon
         * anyway.
         * <p>
         *     这两个检查中的保守性可能会导致不必要的唤醒，但是只有在有多个竞争的获取/释放时才会发生，因此大多数现在或很快需要信号。
         */
        if (propagate > 0 || h == null || h.waitStatus < 0 ||
                (h = head) == null || h.waitStatus < 0) {
            Node s = node.next;
            if (s == null || s.isShared())
                // 如果不存在后继结点，那么直接 release；
                // 如果存在后继结点，并且也是尝试获取共享锁，那么就让当前结点 release，让后继结点来 acquire
                doReleaseShared();
        }
    }

    // Utilities for various versions of acquire

    /**
     * Cancels an ongoing attempt to acquire.
     * <p>
     * 取消正在进行的获取尝试。
     *
     * @param node the node
     */
    private void cancelAcquire(Node node) {
        // Ignore if node doesn't exist
        if (node == null)
            return;

        // 将 node 的线程置空
        node.thread = null;

        // Skip cancelled predecessors
        // 找到 node 的前驱结点，将它们的 waitStatus 设置为 cancel；
        // 会一直向前找，直到找到一个 waitStatus <= 0 的结点 && 因为头结点的 waitStatus 一定 <=0 ?，所以这里不需要判空
        Node pred = node.prev;
        while (pred.waitStatus > 0)
            node.prev = pred = pred.prev;

        // predNext is the apparent node to unsplice. CASes below will
        // fail if not, in which case, we lost race vs another cancel
        // or signal, so no further action is necessary.
        // predNext 是要取消链接的结点。
        // 如果不是，下面的 CAS 将失败，如果失败，我们就会丢失与另一个取消或信号的竞争，因此不需要进一步的操作。
        Node predNext = pred.next;

        // Can use unconditional write instead of CAS here.
        // After this atomic step, other Nodes can skip past us.
        // Before, we are free of interference from other threads.
        // 可以在这里使用无条件写入，而不是 CAS。
        // 在这个原子步骤之后，其他节点可以跳过我们。
        // 在此之前，我们不受其他线程的干扰。
        node.waitStatus = Node.CANCELLED;

        // If we are the tail, remove ourselves.
        // 如果我们是尾结点，那么移除我们自己，即将 pred 设置为 tail，并将
        if (node == tail && compareAndSetTail(node, pred)) {
            compareAndSetNext(pred, predNext, null);
        } else {
            // If successor needs signal, try to set pred's next-link
            // so it will get one. Otherwise wake it up to propagate.
            // 如果后继节点需要信号，尝试设置 pred 的 next-link，以便它会得到一个。
            // 否则唤醒它来传播。
            int ws;
            if (pred != head &&
                    ((ws = pred.waitStatus) == Node.SIGNAL ||
                            (ws <= 0 && compareAndSetWaitStatus(pred, ws, Node.SIGNAL))) &&
                    pred.thread != null) {
                Node next = node.next;
                if (next != null && next.waitStatus <= 0)
                    compareAndSetNext(pred, predNext, next);
            } else {
                unparkSuccessor(node);
            }

            node.next = node; // help GC
        }
    }

    /**
     * Checks and updates status for a node that failed to acquire.
     * Returns true if thread should block. This is the main signal
     * control in all acquire loops.  Requires that pred == node.prev.
     * <p>
     * 检查并更新获取失败的节点的状态。
     * 如果线程应该阻塞，则返回 true。
     * 这是所有获取循环中的主要信号控制。
     * 要求 pred == node.prev。
     *
     * @param pred node's predecessor holding status
     * @param node the node
     * @return {@code true} if thread should block
     */
    private static boolean shouldParkAfterFailedAcquire(Node pred, Node node) {
        // 取到前驱结点的状态
        int ws = pred.waitStatus;
        // 如果前驱结点的状态为 SIGNAL，说明后继结点需要被唤醒
        if (ws == Node.SIGNAL)
            /*
             * This node has already set status asking a release
             * to signal it, so it can safely park.
             * <p>
             *     这个节点已经设置了状态，要求释放来信号它，所以它可以安全地 park。
             */
            return true;

        // 否则，如果前驱结点的状态 > 0，说明前驱结点已经被 cancel 了
        if (ws > 0) {
            /*
             * Predecessor was cancelled. Skip over predecessors and
             * indicate retry.
             * <p>
             *     前驱结点被取消了。跳过前驱结点并指示重试。
             */
            do {
                // 尝试持续向前找到一个 waitStatus <= 0 的结点
                // 为什么这里不需要判断 pred 是否为 null 呢？
                node.prev = pred = pred.prev;
            } while (pred.waitStatus > 0);
            pred.next = node;
        } else {
            // 否则，说明 pred 的状态是非 SIGNAL 的阻塞值（估计是 PROPAGATE），那么就将 pred 的状态设置为 SIGNAL
            /*
             * waitStatus must be 0 or PROPAGATE.  Indicate that we
             * need a signal, but don't park yet.  Caller will need to
             * retry to make sure it cannot acquire before parking.
             * <p>
             *     waitStatus 必须是 0 或 PROPAGATE。
             * 指示我们需要一个信号，但还不要 park。
             */
            compareAndSetWaitStatus(pred, ws, Node.SIGNAL);
        }
        // 返回 false，表示调用者需要重试，以确保在 park 之前不能获取
        return false;
    }

    /**
     * Convenience method to interrupt current thread.
     */
    static void selfInterrupt() {
        Thread.currentThread().interrupt();
    }

    /**
     * Convenience method to park and then check if interrupted
     *
     * @return {@code true} if interrupted
     */
    private final boolean parkAndCheckInterrupt() {
        LockSupport.park(this);
        return Thread.interrupted();
    }

    /*
     * Various flavors of acquire, varying in exclusive/shared and
     * control modes.  Each is mostly the same, but annoyingly
     * different.  Only a little bit of factoring is possible due to
     * interactions of exception mechanics (including ensuring that we
     * cancel if tryAcquire throws exception) and other control, at
     * least not without hurting performance too much.
     * <p>
     *     获取的各种方式，根据独占/共享和控制模式的不同而不同。
     *    每种方式大部分都是相同的，但是有些让人讨厌的不同。
     *   由于异常机制的交互（包括确保 tryAcquire 抛出异常时取消）和其他控制，只有一点点的因素是可能的，至少不会对性能造成太大的影响。
     */

    /**
     * Acquires in exclusive uninterruptible mode for thread already in
     * queue. Used by condition wait methods as well as acquire.
     * <p>
     * 为队列中已经存在的线程以独占模式获取锁，不可中断。
     * 由条件等待方法和获取方法使用。
     *
     * @param node the node
     * @param arg  the acquire argument
     * @return {@code true} if interrupted while waiting 如果在等待时被中断，则返回 true
     */
    final boolean acquireQueued(final Node node, int arg) {
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (; ; ) {
                // 取到 node 的前驱结点
                final Node p = node.predecessor();
                // 如果前驱结点是头结点，说明 node 是头结点的后继结点；尝试获取锁，并且成功获取锁后将 node 设置为头结点
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    // 将前驱结点的 next 设置为 null，帮助 GC
                    // 也就是说会打断 p -> node 的联系
                    p.next = null; // help GC
                    failed = false;
                    return interrupted;
                }
                // 如果 node 的前驱结点不是头结点，说明前面还有其他结点在等待
                // 或者 tryAcquire 失败说明它虽然是头结点，但是没能竞争过其他尝试获取锁的线程
                // 那么就需要进行 park
                // 如果需要 park，那么就 park 并且检查是否被中断
                if (shouldParkAfterFailedAcquire(p, node) && // 校验是否需要 park
                        parkAndCheckInterrupt()) // park 并且检查是否被中断

                    // 记录是否被中断，然后开启下一次 for 循环
                    interrupted = true;
            }
        } finally {
            // 如果执行期间抛异常了，那么就取消获取
            // 为什么会抛异常？因为 tryAcquire 可能会抛异常
            if (failed)
                cancelAcquire(node);
        }
    }

    /**
     * Acquires in exclusive interruptible mode.
     *
     * @param arg the acquire argument
     */
    private void doAcquireInterruptibly(int arg)
            throws InterruptedException {
        // 将当前线程封装成一个 Node，并且加入到 aqs 队列中
        final Node node = addWaiter(Node.EXCLUSIVE);
        boolean failed = true;
        try {
            for (; ; ) {
                final Node p = node.predecessor();
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return;
                }
                //  每次竞争锁失败后，都会 park 并且检查是否被中断
                if (shouldParkAfterFailedAcquire(p, node) &&
                        parkAndCheckInterrupt())
                    throw new InterruptedException();
            }
        } finally {
            // 如果执行期间抛异常了，那么就取消获取
            if (failed)
                cancelAcquire(node);
        }
    }

    /**
     * Acquires in exclusive timed mode.
     *
     * @param arg          the acquire argument
     * @param nanosTimeout max wait time
     * @return {@code true} if acquired
     */
    private boolean doAcquireNanos(int arg, long nanosTimeout)
            throws InterruptedException {
        if (nanosTimeout <= 0L)
            return false;
        // 算好时间
        final long deadline = System.nanoTime() + nanosTimeout;
        final Node node = addWaiter(Node.EXCLUSIVE);
        boolean failed = true;
        try {
            for (; ; ) {
                final Node p = node.predecessor();
                if (p == head && tryAcquire(arg)) {
                    setHead(node);
                    p.next = null; // help GC
                    failed = false;
                    return true;
                }
                // 每次醒过来都会重新计算剩余时间
                nanosTimeout = deadline - System.nanoTime();
                if (nanosTimeout <= 0L)
                    return false;
                if (shouldParkAfterFailedAcquire(p, node) &&
                        nanosTimeout > spinForTimeoutThreshold)
                    LockSupport.parkNanos(this, nanosTimeout);
                if (Thread.interrupted())
                    throw new InterruptedException();
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }

    /**
     * Acquires in shared uninterruptible mode.
     *
     * @param arg the acquire argument
     */
    private void doAcquireShared(int arg) {
        // 将当前线程封装成一个 Node，并且加入到 aqs 队列中
        final Node node = addWaiter(Node.SHARED);
        boolean failed = true;
        try {
            boolean interrupted = false;
            for (; ; ) {
                // 获取到前驱结点
                final Node p = node.predecessor();
                if (p == head) {
                    // 如果前驱结点是头结点，那么就尝试获取锁
                    int r = tryAcquireShared(arg);
                    // r>=0 说明获取锁成功
                    if (r >= 0) {
                        // 将 node 设置为头结点，并传播共享信息
                        setHeadAndPropagate(node, r);
                        p.next = null; // help GC
                        if (interrupted)
                            selfInterrupt();
                        failed = false;
                        return;
                    }
                }
                // 自我阻塞，并判断中断
                if (shouldParkAfterFailedAcquire(p, node) &&
                        parkAndCheckInterrupt())
                    interrupted = true;
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }

    /**
     * Acquires in shared interruptible mode.
     * <p>
     *     以共享模式获取锁，可中断。
     *
     * @param arg the acquire argument
     */
    private void doAcquireSharedInterruptibly(int arg)
            throws InterruptedException {
        // 封装当前线程为 Node，并且加入到 CLH 队列中
        final Node node = addWaiter(Node.SHARED);
        boolean failed = true;
        try {
            for (; ; ) {
                // 取到前驱结点
                final Node p = node.predecessor();
                // 如果前驱结点是头结点，那么就尝试获取锁
                if (p == head) {
                    int r = tryAcquireShared(arg);
                    if (r >= 0) {
                        setHeadAndPropagate(node, r);
                        p.next = null; // help GC
                        failed = false;
                        return;
                    }
                }
                // 每次被唤醒尝试后都会尝试检查中断
                if (shouldParkAfterFailedAcquire(p, node) &&
                        parkAndCheckInterrupt())
                    throw new InterruptedException();
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }

    /**
     * Acquires in shared timed mode.
     *
     * @param arg          the acquire argument
     * @param nanosTimeout max wait time
     * @return {@code true} if acquired
     */
    private boolean doAcquireSharedNanos(int arg, long nanosTimeout)
            throws InterruptedException {
        if (nanosTimeout <= 0L)
            return false;
        // 计算一个截止时间
        final long deadline = System.nanoTime() + nanosTimeout;
        final Node node = addWaiter(Node.SHARED);
        boolean failed = true;
        try {
            for (; ; ) {
                final Node p = node.predecessor();
                if (p == head) {
                    int r = tryAcquireShared(arg);
                    if (r >= 0) {
                        setHeadAndPropagate(node, r);
                        p.next = null; // help GC
                        failed = false;
                        return true;
                    }
                }
                // 每次加锁失败都会重新计算剩余时间
                nanosTimeout = deadline - System.nanoTime();
                // 如果剩余时间小于等于 0，那么就返回 false
                if (nanosTimeout <= 0L)
                    return false;
                // 否则根据时间来 spin 或 park
                if (shouldParkAfterFailedAcquire(p, node) &&
                        nanosTimeout > spinForTimeoutThreshold)
                    LockSupport.parkNanos(this, nanosTimeout);
                // 每次 park 后都会检查中断
                if (Thread.interrupted())
                    throw new InterruptedException();
            }
        } finally {
            if (failed)
                cancelAcquire(node);
        }
    }

    // Main exported methods

    /**
     * Attempts to acquire in exclusive mode. This method should query
     * if the state of the object permits it to be acquired in the
     * exclusive mode, and if so to acquire it.
     *
     * <p>This method is always invoked by the thread performing
     * acquire.  If this method reports failure, the acquire method
     * may queue the thread, if it is not already queued, until it is
     * signalled by a release from some other thread. This can be used
     * to implement method {@link Lock#tryLock()}.
     *
     * <p>The default
     * implementation throws {@link UnsupportedOperationException}.
     *
     * @param arg the acquire argument. This value is always the one
     *            passed to an acquire method, or is the value saved on entry
     *            to a condition wait.  The value is otherwise uninterpreted
     *            and can represent anything you like.
     * @return {@code true} if successful. Upon success, this object has
     * been acquired.
     * @throws IllegalMonitorStateException  if acquiring would place this
     *                                       synchronizer in an illegal state. This exception must be
     *                                       thrown in a consistent fashion for synchronization to work
     *                                       correctly.
     * @throws UnsupportedOperationException if exclusive mode is not supported
     */
    protected boolean tryAcquire(int arg) {
        throw new UnsupportedOperationException();
    }

    /**
     * Attempts to set the state to reflect a release in exclusive
     * mode.
     *
     * <p>This method is always invoked by the thread performing release.
     *
     * <p>The default implementation throws
     * {@link UnsupportedOperationException}.
     *
     * @param arg the release argument. This value is always the one
     *            passed to a release method, or the current state value upon
     *            entry to a condition wait.  The value is otherwise
     *            uninterpreted and can represent anything you like.
     * @return {@code true} if this object is now in a fully released
     * state, so that any waiting threads may attempt to acquire;
     * and {@code false} otherwise.
     * @throws IllegalMonitorStateException  if releasing would place this
     *                                       synchronizer in an illegal state. This exception must be
     *                                       thrown in a consistent fashion for synchronization to work
     *                                       correctly.
     * @throws UnsupportedOperationException if exclusive mode is not supported
     */
    protected boolean tryRelease(int arg) {
        throw new UnsupportedOperationException();
    }

    /**
     * Attempts to acquire in shared mode. This method should query if
     * the state of the object permits it to be acquired in the shared
     * mode, and if so to acquire it.
     *
     * <p>This method is always invoked by the thread performing
     * acquire.  If this method reports failure, the acquire method
     * may queue the thread, if it is not already queued, until it is
     * signalled by a release from some other thread.
     *
     * <p>The default implementation throws {@link
     * UnsupportedOperationException}.
     *
     * @param arg the acquire argument. This value is always the one
     *            passed to an acquire method, or is the value saved on entry
     *            to a condition wait.  The value is otherwise uninterpreted
     *            and can represent anything you like.
     * @return a negative value on failure; zero if acquisition in shared
     * mode succeeded but no subsequent shared-mode acquire can
     * succeed; and a positive value if acquisition in shared
     * mode succeeded and subsequent shared-mode acquires might
     * also succeed, in which case a subsequent waiting thread
     * must check availability. (Support for three different
     * return values enables this method to be used in contexts
     * where acquires only sometimes act exclusively.)  Upon
     * success, this object has been acquired.
     * @throws IllegalMonitorStateException  if acquiring would place this
     *                                       synchronizer in an illegal state. This exception must be
     *                                       thrown in a consistent fashion for synchronization to work
     *                                       correctly.
     * @throws UnsupportedOperationException if shared mode is not supported
     */
    protected int tryAcquireShared(int arg) {
        throw new UnsupportedOperationException();
    }

    /**
     * Attempts to set the state to reflect a release in shared mode.
     *
     * <p>This method is always invoked by the thread performing release.
     *
     * <p>The default implementation throws
     * {@link UnsupportedOperationException}.
     *
     * @param arg the release argument. This value is always the one
     *            passed to a release method, or the current state value upon
     *            entry to a condition wait.  The value is otherwise
     *            uninterpreted and can represent anything you like.
     * @return {@code true} if this release of shared mode may permit a
     * waiting acquire (shared or exclusive) to succeed; and
     * {@code false} otherwise
     * @throws IllegalMonitorStateException  if releasing would place this
     *                                       synchronizer in an illegal state. This exception must be
     *                                       thrown in a consistent fashion for synchronization to work
     *                                       correctly.
     * @throws UnsupportedOperationException if shared mode is not supported
     */
    protected boolean tryReleaseShared(int arg) {
        throw new UnsupportedOperationException();
    }

    /**
     * Returns {@code true} if synchronization is held exclusively with
     * respect to the current (calling) thread.  This method is invoked
     * upon each call to a non-waiting {@link ConditionObject} method.
     * (Waiting methods instead invoke {@link #release}.)
     *
     * <p>The default implementation throws {@link
     * UnsupportedOperationException}. This method is invoked
     * internally only within {@link ConditionObject} methods, so need
     * not be defined if conditions are not used.
     *
     * @return {@code true} if synchronization is held exclusively;
     * {@code false} otherwise
     * @throws UnsupportedOperationException if conditions are not supported
     */
    protected boolean isHeldExclusively() {
        throw new UnsupportedOperationException();
    }

    /**
     * Acquires in exclusive mode, ignoring interrupts.  Implemented
     * by invoking at least once {@link #tryAcquire},
     * returning on success.  Otherwise the thread is queued, possibly
     * repeatedly blocking and unblocking, invoking {@link
     * #tryAcquire} until success.  This method can be used
     * to implement method {@link Lock#lock}.
     * <p>
     *     以独占模式获取锁，忽略中断。
     *     通过至少一次调用 tryAcquire 来实现，成功时返回。
     *     否则，线程将被排队，可能会重复阻塞和解除阻塞，调用 tryAcquire 直到成功。
     *     这个方法可以用来实现 lock 方法。
     *
     *     也就是说，进队列之后，即使被中断了，也必须等到获取到锁之后才会返回。
     *
     * @param arg the acquire argument.  This value is conveyed to
     *            {@link #tryAcquire} but is otherwise uninterpreted and
     *            can represent anything you like.
     */
    public final void acquire(int arg) {
        // 先尝试 tryAcquire
        // 如果失败了，那么就执行 acquireQueued 方法，即先入队，然后等唤醒并竞争锁
        if (!tryAcquire(arg) &&
                acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
            // acquireQueued 也返回了 true，那么就自我中断
            selfInterrupt();
    }

    /**
     * Acquires in exclusive mode, aborting if interrupted.
     * Implemented by first checking interrupt status, then invoking
     * at least once {@link #tryAcquire}, returning on
     * success.  Otherwise the thread is queued, possibly repeatedly
     * blocking and unblocking, invoking {@link #tryAcquire}
     * until success or the thread is interrupted.  This method can be
     * used to implement method {@link Lock#lockInterruptibly}.
     * <p>
     *     以独占模式获取锁，如果被中断则中止。
     *     首先检查中断状态，然后至少一次调用 tryAcquire，成功时返回。
     *     否则，线程将被排队，可能会重复阻塞和解除阻塞，调用 tryAcquire 直到成功或线程被中断。
     *     这个方法可以用来实现 lockInterruptibly 方法。
     *
     * @param arg the acquire argument.  This value is conveyed to
     *            {@link #tryAcquire} but is otherwise uninterpreted and
     *            can represent anything you like.
     * @throws InterruptedException if the current thread is interrupted
     */
    public final void acquireInterruptibly(int arg)
            throws InterruptedException {
        // 如果当前线程被中断了，那么就抛出 InterruptedException
        if (Thread.interrupted())
            throw new InterruptedException();
        // 否则，尝试获取锁
        if (!tryAcquire(arg))
            // 如果失败了，那么就入队，但是每次被唤醒时都会检查是否被中断
            doAcquireInterruptibly(arg);
    }

    /**
     * Attempts to acquire in exclusive mode, aborting if interrupted,
     * and failing if the given timeout elapses.  Implemented by first
     * checking interrupt status, then invoking at least once {@link
     * #tryAcquire}, returning on success.  Otherwise, the thread is
     * queued, possibly repeatedly blocking and unblocking, invoking
     * {@link #tryAcquire} until success or the thread is interrupted
     * or the timeout elapses.  This method can be used to implement
     * method {@link Lock#tryLock(long, TimeUnit)}.
     * <p>
     *     尝试以独占模式获取锁，如果被中断则中止，并且如果给定的超时时间过去了则失败。
     *     首先检查中断状态，然后至少一次调用 tryAcquire，成功时返回。
     *     否则，线程将被排队，可能会重复阻塞和解除阻塞，调用 tryAcquire 直到成功或线程被中断或超时。
     *     这个方法可以用来实现 tryLock(long, TimeUnit) 方法。
     *
     * @param arg          the acquire argument.  This value is conveyed to
     *                     {@link #tryAcquire} but is otherwise uninterpreted and
     *                     can represent anything you like.
     * @param nanosTimeout the maximum number of nanoseconds to wait
     * @return {@code true} if acquired; {@code false} if timed out
     * @throws InterruptedException if the current thread is interrupted
     */
    public final boolean tryAcquireNanos(int arg, long nanosTimeout)
            throws InterruptedException {
        // 如果当前线程被中断了，那么就抛出 InterruptedException
        if (Thread.interrupted())
            throw new InterruptedException();
        // 先调用一次 tryAcquire
        // 如果失败了，那么就执行 doAcquireNanos 方法，即先入队，然后等唤醒并竞争锁
        return tryAcquire(arg) ||
                doAcquireNanos(arg, nanosTimeout);
    }

    /**
     * Releases in exclusive mode.  Implemented by unblocking one or
     * more threads if {@link #tryRelease} returns true.
     * This method can be used to implement method {@link Lock#unlock}.
     * <p>
     * 释放独占模式。
     * 如果 tryRelease 返回 true，那么就会唤醒一个或多个线程。
     * 这个方法可以用来实现 unlock 方法。
     *
     * @param arg the release argument.  This value is conveyed to
     *            {@link #tryRelease} but is otherwise uninterpreted and
     *            can represent anything you like.
     * @return the value returned from {@link #tryRelease}
     */
    public final boolean release(int arg) {
        // 尝试释放锁
        if (tryRelease(arg)) {
            // 如果释放成功，则唤醒后继结点（后继结点会和释放后请求锁的线程竞争）
            Node h = head;
            // 如果存在后继结点，并且后继结点的状态不为 0（有效结点），就唤醒它
            // 实际唤醒的是 h.next
            if (h != null && h.waitStatus != 0)
                unparkSuccessor(h);
            return true;
        }
        return false;
    }

    /**
     * Acquires in shared mode, ignoring interrupts.  Implemented by
     * first invoking at least once {@link #tryAcquireShared},
     * returning on success.  Otherwise the thread is queued, possibly
     * repeatedly blocking and unblocking, invoking {@link
     * #tryAcquireShared} until success.
     * <p>
     *     以共享模式获取锁，忽略中断。
     *     通过至少一次调用 tryAcquireShared 来实现，成功时返回。
     *     否则，线程将被排队，可能会重复阻塞和解除阻塞，调用 tryAcquireShared 直到成功。
     *
     * @param arg the acquire argument.  This value is conveyed to
     *            {@link #tryAcquireShared} but is otherwise uninterpreted
     *            and can represent anything you like.
     */
    public final void acquireShared(int arg) {
        // 即加读锁
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }

    // TODO REMIND  增加 acquireShared、 acquireSharedInterruptibly 等方法的对比

    /**
     * Acquires in shared mode, aborting if interrupted.  Implemented
     * by first checking interrupt status, then invoking at least once
     * {@link #tryAcquireShared}, returning on success.  Otherwise the
     * thread is queued, possibly repeatedly blocking and unblocking,
     * invoking {@link #tryAcquireShared} until success or the thread
     * is interrupted.
     *
     * @param arg the acquire argument.
     *            This value is conveyed to {@link #tryAcquireShared} but is
     *            otherwise uninterpreted and can represent anything
     *            you like.
     * @throws InterruptedException if the current thread is interrupted
     */
    public final void acquireSharedInterruptibly(int arg)
            throws InterruptedException {
        // 校验当前线程是否被中断
        if (Thread.interrupted())
            throw new InterruptedException();
        // 先 tryAcquireShared；如果失败了，那么就执行 doAcquireSharedInterruptibly 方法
        if (tryAcquireShared(arg) < 0)
            doAcquireSharedInterruptibly(arg);
    }

    /**
     * Attempts to acquire in shared mode, aborting if interrupted, and
     * failing if the given timeout elapses.  Implemented by first
     * checking interrupt status, then invoking at least once {@link
     * #tryAcquireShared}, returning on success.  Otherwise, the
     * thread is queued, possibly repeatedly blocking and unblocking,
     * invoking {@link #tryAcquireShared} until success or the thread
     * is interrupted or the timeout elapses.
     *
     * @param arg          the acquire argument.  This value is conveyed to
     *                     {@link #tryAcquireShared} but is otherwise uninterpreted
     *                     and can represent anything you like.
     * @param nanosTimeout the maximum number of nanoseconds to wait
     * @return {@code true} if acquired; {@code false} if timed out
     * @throws InterruptedException if the current thread is interrupted
     */
    public final boolean tryAcquireSharedNanos(int arg, long nanosTimeout)
            throws InterruptedException {
        // 如果当前线程被中断了，那么就抛出 InterruptedException
        if (Thread.interrupted())
            throw new InterruptedException();
        // 先 tryAcquireShared；如果失败了，那么就执行 doAcquireSharedNanos 方法
        return tryAcquireShared(arg) >= 0 ||
                doAcquireSharedNanos(arg, nanosTimeout);
    }

    /**
     * Releases in shared mode.  Implemented by unblocking one or more
     * threads if {@link #tryReleaseShared} returns true.
     * <p>
     *     以共享模式释放锁。
     *     如果 tryReleaseShared 返回 true，那么就会唤醒一个或多个线程。
     *
     * @param arg the release argument.  This value is conveyed to
     *            {@link #tryReleaseShared} but is otherwise uninterpreted
     *            and can represent anything you like.
     * @return the value returned from {@link #tryReleaseShared}
     */
    public final boolean releaseShared(int arg) {
        // 尝试以共享模式释放锁
        if (tryReleaseShared(arg)) {
            // 如果释放成功，则唤醒后继结点（后继结点会和释放后请求锁的线程竞争）
            doReleaseShared();
            return true;
        }
        return false;
    }

    // Queue inspection methods

    /**
     * Queries whether any threads are waiting to acquire. Note that
     * because cancellations due to interrupts and timeouts may occur
     * at any time, a {@code true} return does not guarantee that any
     * other thread will ever acquire.
     * <p>
     *     查询是否有线程正在等待获取锁。
     *     请注意，由于 interrupt 和 timeout 引起的 cancel 可能会在任何时候发生，因此返回 true 并不保证任何其他线程将获得锁。
     *
     * <p>In this implementation, this operation returns in
     * constant time.
     * <p>
     *     在这个实现中，这个操作是常数时间的。
     *
     * @return {@code true} if there may be other threads waiting to acquire
     */
    public final boolean hasQueuedThreads() {
        return head != tail;
    }

    /**
     * Queries whether any threads have ever contended to acquire this
     * synchronizer; that is if an acquire method has ever blocked.
     * <p>
     *     查询是否有任何线程曾经争夺过获取这个同步器；
     *     也就是说，是否有任何获取方法曾经被阻塞过。
     *
     * <p>In this implementation, this operation returns in
     * constant time.
     *
     * @return {@code true} if there has ever been contention
     */
    public final boolean hasContended() {
        return head != null;
    }

    /**
     * Returns the first (longest-waiting) thread in the queue, or
     * {@code null} if no threads are currently queued.
     * <p>
     *     返回队列中的第一个（等待时间最长的）线程，如果当前没有线程在队列中，则返回 null。
     *
     * <p>In this implementation, this operation normally returns in
     * constant time, but may iterate upon contention if other threads are
     * concurrently modifying the queue.
     * <p>
     *     在这个实现中，这个操作通常是常数时间的，但是如果其他线程同时修改队列，那么可能会迭代争用。
     *
     * @return the first (longest-waiting) thread in the queue, or
     * {@code null} if no threads are currently queued
     */
    public final Thread getFirstQueuedThread() {
        // handle only fast path, else relay
        return (head == tail) ? null : fullGetFirstQueuedThread();
    }

    /**
     * Version of getFirstQueuedThread called when fastpath fails
     */
    private Thread fullGetFirstQueuedThread() {
        /*
         * The first node is normally head.next. Try to get its
         * thread field, ensuring consistent reads: If thread
         * field is nulled out or s.prev is no longer head, then
         * some other thread(s) concurrently performed setHead in
         * between some of our reads. We try this twice before
         * resorting to traversal.
         * <p>
         * 第一个结点通常是 head.next。
         * 尝试获取它的线程字段，确保一致的读取：如果线程字段被置空，或者 s.prev 不再是 head，那么我们的一些读取之间可能有其他线程并发执行了 setHead。
         * 在回到遍历之前，我们尝试两次。
         */
        Node h, s;
        Thread st;
        if (((h = head) != null && (s = h.next) != null &&
                s.prev == head && (st = s.thread) != null) // 如果能直接获取到 head.next 的线程，那么就直接返回
                ||
                ((h = head) != null && (s = h.next) != null &&
                        s.prev == head && (st = s.thread) != null)) // 一模一样地再获取一次
            return st;

        /*
         * Head's next field might not have been set yet, or may have
         * been unset after setHead. So we must check to see if tail
         * is actually first node. If not, we continue on, safely
         * traversing from tail back to head to find first,
         * guaranteeing termination.
         * <p>
         * head 的下一个字段可能还没有设置，或者在 setHead 之后被取消了。
         * 因此，我们必须检查 tail 是否实际上是第一个结点。
         * 如果不是，我们继续，从 tail 安全地遍历回 head 来找到第一个结点，保证终止。
         */

        Node t = tail;
        Thread firstThread = null;
        while (t != null && t != head) {
            Thread tt = t.thread;
            if (tt != null)
                firstThread = tt;
            t = t.prev;
        }
        return firstThread;
    }

    /**
     * Returns true if the given thread is currently queued.
     * <p>
     *     如果给定的线程当前在队列中，那么就返回 true。
     *
     * <p>This implementation traverses the queue to determine
     * presence of the given thread.
     *
     * @param thread the thread
     * @return {@code true} if the given thread is on the queue
     * @throws NullPointerException if the thread is null
     */
    public final boolean isQueued(Thread thread) {
        // 判空
        if (thread == null)
            throw new NullPointerException();
        // 从尾结点开始遍历，找到了就返回 true
        for (Node p = tail; p != null; p = p.prev)
            if (p.thread == thread)
                return true;
        return false;
    }

    /**
     * Returns {@code true} if the apparent first queued thread, if one
     * exists, is waiting in exclusive mode.  If this method returns
     * {@code true}, and the current thread is attempting to acquire in
     * shared mode (that is, this method is invoked from {@link
     * #tryAcquireShared}) then it is guaranteed that the current thread
     * is not the first queued thread.  Used only as a heuristic in
     * ReentrantReadWriteLock.
     * <p>
     *     如果等待最久的线程（如果存在）正在独占模式下等待，那么就返回 true。
     *     如果这个方法返回 true，并且当前线程正在尝试以共享模式获取锁
     *     （也就是说，这个方法是从 tryAcquireShared 调用的），那么可以保证当前线程不是队列中的第一个线程。
     *     仅在 ReentrantReadWriteLock 中作为启发式方法使用。
     */
    final boolean apparentlyFirstQueuedIsExclusive() {
        Node h, s;
        // 判断等待最久的结点是否是独占模式
        return (h = head) != null &&
                (s = h.next) != null &&
                !s.isShared() &&
                s.thread != null;
    }

    /**
     * Queries whether any threads have been waiting to acquire longer
     * than the current thread.
     * <p>
     *     查询是否有任何线程等待获取的时间比当前线程长。
     *
     * <p>An invocation of this method is equivalent to (but may be
     * more efficient than):
     * <pre> {@code
     * getFirstQueuedThread() != Thread.currentThread() &&
     * hasQueuedThreads()}</pre>
     * <p>
     *     这个方法的调用等效于（但可能比以下方法更有效）：
     *     getFirstQueuedThread() != Thread.currentThread() && hasQueuedThreads()
     *
     * <p>Note that because cancellations due to interrupts and
     * timeouts may occur at any time, a {@code true} return does not
     * guarantee that some other thread will acquire before the current
     * thread.  Likewise, it is possible for another thread to win a
     * race to enqueue after this method has returned {@code false},
     * due to the queue being empty.
     * <p>
     *     请注意，由于中断和超时引起的取消可能会在任何时候发生，因此返回 true 并不保证其他线程会在当前线程之前获取。
     *      完全有可能上一秒返回 true，下一秒前驱结点就 cancel 了
     *
     * <p>This method is designed to be used by a fair synchronizer to
     * avoid <a href="AbstractQueuedSynchronizer#barging">barging</a>.
     * Such a synchronizer's {@link #tryAcquire} method should return
     * {@code false}, and its {@link #tryAcquireShared} method should
     * return a negative value, if this method returns {@code true}
     * (unless this is a reentrant acquire).  For example, the {@code
     * tryAcquire} method for a fair, reentrant, exclusive mode
     * synchronizer might look like this:
     *
     * <pre> {@code
     * protected boolean tryAcquire(int arg) {
     *   if (isHeldExclusively()) {
     *     // A reentrant acquire; increment hold count
     *     return true;
     *   } else if (hasQueuedPredecessors()) {
     *     return false;
     *   } else {
     *     // try to acquire normally
     *   }
     * }}</pre>
     * <p>
     *     这个方法设计用于公平同步器，以避免 barging。
     *     这样的同步器的 tryAcquire 方法应该返回 false，而它的 tryAcquireShared 方法应该返回负值，如果这个方法返回 true（除非这是一个可重入获取）。
     *     例如，一个公平的、可重入的、独占模式同步器的 tryAcquire 方法可能如下所示：
     *     protected boolean tryAcquire(int arg) {
     *      if (isHeldExclusively()) {
     *          // A reentrant acquire; increment hold count
     *          return true;
     *      } else if (hasQueuedPredecessors()) {
     *          return false;
     *      } else {
     *      // try to acquire normally
     *      }
     *     }
     *
     * @return {@code true} if there is a queued thread preceding the
     * current thread, and {@code false} if the current thread
     * is at the head of the queue or the queue is empty
     * @since 1.7
     */
    public final boolean hasQueuedPredecessors() {
        // The correctness of this depends on head being initialized
        // before tail and on head.next being accurate if the current
        // thread is first in queue.
        // 这个方法的正确性取决于 head 在 tail 之前初始化，并且 head.next 在当前线程是队列中的第一个时是准确的。

        Node t = tail; // Read fields in reverse initialization order 逆序初始化顺序读取字段
        Node h = head;
        Node s;
        return h != t &&
                ((s = h.next) == null || s.thread != Thread.currentThread());
    }


    // Instrumentation and monitoring methods

    /**
     * Returns an estimate of the number of threads waiting to
     * acquire.  The value is only an estimate because the number of
     * threads may change dynamically while this method traverses
     * internal data structures.  This method is designed for use in
     * monitoring system state, not for synchronization
     * control.
     * <p>
     *     返回等待获取锁的线程数的估计值。
     *     这个值只是一个估计值，因为在这个方法遍历内部数据结构时，线程的数量可能会动态变化。
     *     这个方法设计用于监视系统状态，而不是用于同步控制。
     *
     * @return the estimated number of threads waiting to acquire
     */
    public final int getQueueLength() {
        int n = 0;
        // 从 tail 遍历到 head，计算有效结点的数量
        for (Node p = tail; p != null; p = p.prev) {
            if (p.thread != null)
                ++n;
        }
        return n;
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire.  Because the actual set of threads may change
     * dynamically while constructing this result, the returned
     * collection is only a best-effort estimate.  The elements of the
     * returned collection are in no particular order.  This method is
     * designed to facilitate construction of subclasses that provide
     * more extensive monitoring facilities.
     *
     * @return the collection of threads
     */
    public final Collection<Thread> getQueuedThreads() {
        ArrayList<Thread> list = new ArrayList<Thread>();
        for (Node p = tail; p != null; p = p.prev) {
            Thread t = p.thread;
            if (t != null)
                list.add(t);
        }
        return list;
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire in exclusive mode. This has the same properties
     * as {@link #getQueuedThreads} except that it only returns
     * those threads waiting due to an exclusive acquire.
     *
     * @return the collection of threads
     */
    public final Collection<Thread> getExclusiveQueuedThreads() {
        ArrayList<Thread> list = new ArrayList<Thread>();
        for (Node p = tail; p != null; p = p.prev) {
            if (!p.isShared()) {
                Thread t = p.thread;
                if (t != null)
                    list.add(t);
            }
        }
        return list;
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire in shared mode. This has the same properties
     * as {@link #getQueuedThreads} except that it only returns
     * those threads waiting due to a shared acquire.
     * <p>
     *     返回一个包含可能正在等待以共享模式获取锁的线程的集合。
     *     它具有与 getQueuedThreads 相同的属性，只是它只返回由于共享获取而等待的那些线程。
     *
     * @return the collection of threads
     */
    public final Collection<Thread> getSharedQueuedThreads() {
        ArrayList<Thread> list = new ArrayList<Thread>();
        for (Node p = tail; p != null; p = p.prev) {
            if (p.isShared()) {
                Thread t = p.thread;
                if (t != null)
                    list.add(t);
            }
        }
        return list;
    }

    /**
     * Returns a string identifying this synchronizer, as well as its state.
     * The state, in brackets, includes the String {@code "State ="}
     * followed by the current value of {@link #getState}, and either
     * {@code "nonempty"} or {@code "empty"} depending on whether the
     * queue is empty.
     * <p>
     *     返回一个标识这个同步器的字符串，以及它的状态。
     *     状态在方括号中，包括字符串 "State =" 后跟 getState 的当前值，以及 "nonempty" 或 "empty"，具体取决于队列是否为空。
     *
     * @return a string identifying this synchronizer, as well as its state
     */
    public String toString() {
        // 获取当前同步器的状态
        int s = getState();
        // 如果队列不为空，就返回 "nonempty"，否则返回 "empty"
        String q = hasQueuedThreads() ? "non" : "";
        return super.toString() +
                "[State = " + s + ", " + q + "empty queue]";
    }


    // Internal support methods for Conditions

    /**
     * Returns true if a node, always one that was initially placed on
     * a condition queue, is now waiting to reacquire on sync queue.
     * <p>
     * 如果一个节点，总是最初放置在条件队列上，
     * 现在正在等待重新获取同步队列，则返回 true。
     *
     * @param node the node
     * @return true if is reacquiring
     */
    final boolean isOnSyncQueue(Node node) {
        // 如果 node 的 ws == Condition，说明它正处于 condition queue 中，一定不处于 sync queue；
        // 如果 node 的前驱节点为 null，说明它已经获取到了锁，一定不处于 sync queue；
        if (node.waitStatus == Node.CONDITION || node.prev == null)
            return false;

        // 如果 node 的后继节点不为 null，说明它一定处于 sync queue；
        if (node.next != null) // If has successor, it must be on queue
            return true;
        /*
         * node.prev can be non-null, but not yet on queue because
         * the CAS to place it on queue can fail. So we have to
         * traverse from tail to make sure it actually made it.  It
         * will always be near the tail in calls to this method, and
         * unless the CAS failed (which is unlikely), it will be
         * there, so we hardly ever traverse much.
         * <p>
         *     node.prev 可能不为 null，但是还没有彻底地放到队列中，因为将它放到队列中的 CAS 可能会失败。
         *    因此，我们必须从 sync queue 的尾部开始遍历，以确保该 node 确实已经放到队列中。
         *   在调用这个方法时，它总是在队列的尾部附近，除非 CAS 失败（这是不太可能的），它将会在那里，所以我们几乎不会遍历太多。
         */
        return findNodeFromTail(node);
    }

    /**
     * Returns true if node is on sync queue by searching backwards from tail.
     * Called only when needed by isOnSyncQueue.
     *
     * @return true if present
     */
    private boolean findNodeFromTail(Node node) {
        // 从尾部开始遍历
        Node t = tail;
        for (; ; ) {
            // 如果 t == node，说明 node 已经在 sync queue 中
            if (t == node)
                return true;
            // 如果 t == null，说明直到遍历结束了，node 都没有在 sync queue 中
            if (t == null)
                return false;
            t = t.prev;
        }
    }

    /**
     * Queries whether the given ConditionObject
     * uses this synchronizer as its lock.
     * <p>
     *     查询给定的 ConditionObject 是否使用这个同步器作为它的锁。
     *
     * @param condition the condition
     * @return {@code true} if owned
     * @throws NullPointerException if the condition is null
     */
    public final boolean owns(ConditionObject condition) {
        return condition.isOwnedBy(this);
    }

    /**
     * Transfers node, if necessary, to sync queue after a cancelled wait.
     * Returns true if thread was cancelled before being signalled.
     * <p>
     * 在取消等待后，如果需要，将节点转移到同步队列。
     * 如果线程在被 signal 之前被 cancel，则返回 true。
     *
     * @param node the node
     * @return true if cancelled before the node was signalled
     */
    final boolean transferAfterCancelledWait(Node node) {
        // cas 将 node 的 ws 从 CONDITION 设置为 0
        if (compareAndSetWaitStatus(node, Node.CONDITION, 0)) {
            // cas 成功之后直接 enq
            enq(node);
            return true;
        }
        /*
         * If we lost out to a signal(), then we can't proceed
         * until it finishes its enq().  Cancelling during an
         * incomplete transfer is both rare and transient, so just
         * spin.
         * <p>
         * 如果我们输给了 signal()，那么在它完成 enq() 之前我们不能继续。
         * 在不完整的传输过程中取消既罕见又短暂，所以只需自旋。
         */
        // 说明此时 node 的 ws 已经不是 CONDITION 了，说明 node 已经被 signal 了，那么就直接返回 false
        // 那么我们就需要自旋等待 signal 方法完成 enq 操作；
        while (!isOnSyncQueue(node))
            Thread.yield();
        return false;
    }

    /**
     * Invokes release with current state value; returns saved state.
     * Cancels node and throws exception on failure.
     * <p>
     * 使用当前状态值调用 release；返回保存的状态。
     * 在失败时取消节点并抛出异常。
     *
     * @param node the condition node for this wait
     * @return previous sync state
     */
    final int fullyRelease(Node node) {
        boolean failed = true;
        try {
            // 获取 aqs 的当前状态值；
            // 既然当前方法能够被调用，说明当前线程已经持有了锁，因此可以直接调用 getState 方法；
            int savedState = getState();
            // 当前线程释放 aqs 锁
            if (release(savedState)) {
                failed = false;
                return savedState;
            } else {
                throw new IllegalMonitorStateException();
            }
        } finally {
            // 如果释放锁失败，那么设置 node 的状态为 cancel
            if (failed)
                node.waitStatus = Node.CANCELLED;
        }
    }

    // Instrumentation methods for conditions

    /**
     * Queries whether any threads are waiting on the given condition
     * associated with this synchronizer. Note that because timeouts
     * and interrupts may occur at any time, a {@code true} return
     * does not guarantee that a future {@code signal} will awaken
     * any threads.  This method is designed primarily for use in
     * monitoring of the system state.
     * <p>
     *     查询是否有任何线程正在等待与此同步器关联的给定条件。
     *     请注意，因为超时和中断可能在任何时候发生，因此返回 true 并不保证将来的 signal 会唤醒任何线程。
     *     该方法主要用于监视系统状态。
     *
     * @param condition the condition
     * @return {@code true} if there are any waiting threads
     * @throws IllegalMonitorStateException if exclusive synchronization
     *                                      is not held
     * @throws IllegalArgumentException     if the given condition is
     *                                      not associated with this synchronizer
     * @throws NullPointerException         if the condition is null
     */
    public final boolean hasWaiters(ConditionObject condition) {
        // 判断当前 aqs 对象是否是 condition 的 owner
        if (!owns(condition))
            throw new IllegalArgumentException("Not owner");
        // 调用 condition 的 hasWaiters 方法
        return condition.hasWaiters();
    }

    /**
     * Returns an estimate of the number of threads waiting on the
     * given condition associated with this synchronizer. Note that
     * because timeouts and interrupts may occur at any time, the
     * estimate serves only as an upper bound on the actual number of
     * waiters.  This method is designed for use in monitoring of the
     * system state, not for synchronization control.
     * <p>
     *     返回与此同步器关联的给定条件上等待的线程数的估计值。
     *     请注意，因为超时和中断可能在任何时候发生，因此估计值仅作为实际等待者数量的上限。
     *     该方法设计用于监视系统状态，而不是用于同步控制。
     *
     * @param condition the condition
     * @return the estimated number of waiting threads
     * @throws IllegalMonitorStateException if exclusive synchronization
     *                                      is not held
     * @throws IllegalArgumentException     if the given condition is
     *                                      not associated with this synchronizer
     * @throws NullPointerException         if the condition is null
     */
    public final int getWaitQueueLength(ConditionObject condition) {
        // 判断当前 aqs 对象是否是 condition 的 owner
        if (!owns(condition))
            throw new IllegalArgumentException("Not owner");
        // 调用 condition 的 getWaitQueueLength 方法
        return condition.getWaitQueueLength();
    }

    /**
     * Returns a collection containing those threads that may be
     * waiting on the given condition associated with this
     * synchronizer.  Because the actual set of threads may change
     * dynamically while constructing this result, the returned
     * collection is only a best-effort estimate. The elements of the
     * returned collection are in no particular order.
     * <p>
     *     返回一个包含可能正在等待与此同步器关联的给定条件的线程的集合。
     *     因为在构造此结果时实际的线程集可能会动态变化，所以返回的集合只是一个尽力而为的估计。
     *     返回的集合的元素没有特定的顺序。
     *     该方法设计用于监视系统状态，而不是用于同步控制。
     *
     * @param condition the condition
     * @return the collection of threads
     * @throws IllegalMonitorStateException if exclusive synchronization
     *                                      is not held
     * @throws IllegalArgumentException     if the given condition is
     *                                      not associated with this synchronizer
     * @throws NullPointerException         if the condition is null
     */
    public final Collection<Thread> getWaitingThreads(ConditionObject condition) {
        // 判断当前 aqs 对象是否是 condition 的 owner
        if (!owns(condition))
            throw new IllegalArgumentException("Not owner");
        // 调用 condition 的 getWaitingThreads 方法
        return condition.getWaitingThreads();
    }

    /**
     * Wait queue node class.
     * <p>
     * 存储到等待队列中的节点类。
     *
     * <p>The wait queue is a variant of a "CLH" (Craig, Landin, and
     * Hagersten) lock queue. CLH locks are normally used for
     * spinlocks.  We instead use them for blocking synchronizers, but
     * use the same basic tactic of holding some of the control
     * information about a thread in the predecessor of its node.  A
     * "status" field in each node keeps track of whether a thread
     * should block.  A node is signalled when its predecessor
     * releases.  Each node of the queue otherwise serves as a
     * specific-notification-style monitor holding a single waiting
     * thread. The status field does NOT control whether threads are
     * granted locks etc though.  A thread may try to acquire if it is
     * first in the queue. But being first does not guarantee success;
     * it only gives the right to contend.  So the currently released
     * contender thread may need to rewait.
     * <p>
     * 等待队列是“CLH”（Craig, Landin, and Hagersten）锁队列的一种变体。
     * CLH 锁通常用于自旋锁。
     * 我们使用它们来阻塞同步器，但是使用了相同的基本策略，即在其节点的前驱中保存关于线程的一些控制信息。
     * 每个节点中的“状态”字段记录了线程是否应该阻塞。
     * 当前节点的前驱释放时，当前节点会被唤醒。
     * 否则，队列中的每个节点都充当特定通知样式的监视器，持有一个等待线程。
     * 状态字段并不控制线程是否被授予锁等。
     * 如果线程是队列中的第一个，那么它可能会尝试获取锁。
     * 但是，排在第一位并不能保证成功；它只是有权竞争。
     * 因此，当前释放的竞争者线程可能需要重新等待。
     *
     *
     * <p>To enqueue into a CLH lock, you atomically splice it in as new
     * tail. To dequeue, you just set the head field.
     * <pre>
     *      +------+  prev +-----+       +-----+
     * head |      | <---- |     | <---- |     |  tail
     *      +------+       +-----+       +-----+
     * </pre>
     *
     * <p>Insertion into a CLH queue requires only a single atomic
     * operation on "tail", so there is a simple atomic point of
     * demarcation from unqueued to queued. Similarly, dequeuing
     * involves only updating the "head". However, it takes a bit
     * more work for nodes to determine who their successors are,
     * in part to deal with possible cancellation due to timeouts
     * and interrupts.
     * <p>
     * 要将节点插入到 CLH 锁中，你需要将它原子地拼接为新的尾部。
     * 要将节点出队，你只需要设置头部字段。
     * 插入到 CLH 队列中只需要对“尾部”进行一次原子操作，因此从未排队到排队有一个简单的原子分界点。
     * 同样，出队只涉及更新“头部”。
     * 但是，节点需要更多的工作来确定它们的后继节点是谁，部分原因是要处理由于超时和中断可能导致的取消。
     *
     * <p>The "prev" links (not used in original CLH locks), are mainly
     * needed to handle cancellation. If a node is cancelled, its
     * successor is (normally) relinked to a non-cancelled
     * predecessor. For explanation of similar mechanics in the case
     * of spin locks, see the papers by Scott and Scherer at
     * http://www.cs.rochester.edu/u/scott/synchronization/
     * <p>
     * “prev” 链接（在原始的 CLH 锁中没有使用）主要是用来处理取消。
     * 如果一个节点被取消，它的后继节点（通常）会被重新链接到一个未被取消的前驱节点。
     * 有关自旋锁中类似机制的解释，请参见 Scott 和 Scherer 在 http://www.cs.rochester.edu/u/scott/synchronization/ 上的论文。
     *
     * <p>We also use "next" links to implement blocking mechanics.
     * The thread id for each node is kept in its own node, so a
     * predecessor signals the next node to wake up by traversing
     * next link to determine which thread it is.  Determination of
     * successor must avoid races with newly queued nodes to set
     * the "next" fields of their predecessors.  This is solved
     * when necessary by checking backwards from the atomically
     * updated "tail" when a node's successor appears to be null.
     * (Or, said differently, the next-links are an optimization
     * so that we don't usually need a backward scan.)
     * <p>
     * 我们还使用“next”链接来实现阻塞机制。
     * 每个节点的线程 id 都保存在自己的节点中，因此前驱节点通过遍历 next 链接来确定下一个要唤醒的线程。
     * 后继节点的确定必须避免与新排队的节点竞争，以设置它们的前驱节点的“next”字段。
     * 当一个节点的后继节点看起来是 null 时，这个问题会在必要时通过从原子更新的“尾部”向后检查来解决。
     *
     * <p>Cancellation introduces some conservatism to the basic
     * algorithms.  Since we must poll for cancellation of other
     * nodes, we can miss noticing whether a cancelled node is
     * ahead or behind us. This is dealt with by always unparking
     * successors upon cancellation, allowing them to stabilize on
     * a new predecessor, unless we can identify an uncancelled
     * predecessor who will carry this responsibility.
     * <p>
     * 取消排队对基本算法引入了一些保守性。
     * 由于我们必须轮询其他节点的取消，我们可能会错过注意到一个取消的节点是在我们前面还是后面。
     * 这是通过在取消时总是唤醒后继节点来处理的，除非我们能识别出一个未取消的前驱节点，它将承担这个责任。
     *
     * <p>CLH queues need a dummy header node to get started. But
     * we don't create them on construction, because it would be wasted
     * effort if there is never contention. Instead, the node
     * is constructed and head and tail pointers are set upon first
     * contention.
     * <p>
     * CLH 队列需要一个虚拟的头节点来启动。
     * 但是我们不会在构造时创建它们，因为如果没有竞争，那么这将是一种浪费。
     * 相反，节点是在第一次竞争时构造的，头部和尾部指针也在第一次竞争时被设置。
     *
     * <p>Threads waiting on Conditions use the same nodes, but
     * use an additional link. Conditions only need to link nodes
     * in simple (non-concurrent) linked queues because they are
     * only accessed when exclusively held.  Upon await, a node is
     * inserted into a condition queue.  Upon signal, the node is
     * transferred to the main queue.  A special value of status
     * field is used to mark which queue a node is on.
     * <p>
     * 等待 Condition 的线程使用相同的节点，但是使用了额外的链接。
     * Condition 只需要在简单（非并发）的链接队列中链接节点，因为它们只在独占持有时才会被访问。
     * 在 await 时，一个节点会被插入到一个 condition 队列中。
     *
     * <p>Thanks go to Dave Dice, Mark Moir, Victor Luchangco, Bill
     * Scherer and Michael Scott, along with members of JSR-166
     * expert group, for helpful ideas, discussions, and critiques
     * on the design of this class.
     */
    static final class Node {
        // --- Markers ---
        // TODO REMIND 注意，这里是两个 static 成员变量；因此是全局统一的；
        /**
         * Marker to indicate a node is waiting in shared mode
         * <p>
         * 标记，用来指示一个节点正在以 shared 模式等待。
         */
        static final Node SHARED = new Node();
        /**
         * Marker to indicate a node is waiting in exclusive mode
         * <p>
         * 标记，用来指示一个节点正在以 exclusive 模式等待。
         */
        static final Node EXCLUSIVE = null;

        // --- Wait Status Values ---
        /**
         * waitStatus value to indicate thread has cancelled
         * <p>
         * waitStatus 的值，用来指示线程已经被取消。
         * --
         * 思考，哪些情况下会被 cancel 呢？
         * 1. 线程等待时设置了等待时间，结果超时了；
         * 2. 本来在 aqs 等待队列中等待的线程被 interrupt 了，那么线程对应的 node 的 ws 状态会被设置为 CANCELLED；
         * 3. 本来在 condition 等待队列中等待的线程被 interrupt 了；
         */
        static final int CANCELLED = 1;
        /**
         * waitStatus value to indicate successor's thread needs unparking
         * <p>
         * waitStatus 的值，用来指示后继节点的线程需要被唤醒。
         */
        static final int SIGNAL = -1;
        /**
         * waitStatus value to indicate thread is waiting on condition
         * <p>
         * signal 表示当前结点 release 锁后，需要唤醒后继结点；
         */
        static final int CONDITION = -2;
        /**
         * waitStatus value to indicate the next acquireShared should
         * unconditionally propagate
         * <p>
         * waitStatus 的值，用来指示下一个 acquireShared 应该无条件地传播。
         * 用于共享锁；
         */
        static final int PROPAGATE = -3;

        /**
         * Status field, taking on only the values:
         * SIGNAL:     The successor of this node is (or will soon be)
         * blocked (via park), so the current node must
         * unpark its successor when it releases or
         * cancels. To avoid races, acquire methods must
         * first indicate they need a signal,
         * then retry the atomic acquire, and then,
         * on failure, block.
         * CANCELLED:  This node is cancelled due to timeout or interrupt.
         * Nodes never leave this state. In particular,
         * a thread with cancelled node never again blocks.
         * CONDITION:  This node is currently on a condition queue.
         * It will not be used as a sync queue node
         * until transferred, at which time the status
         * will be set to 0. (Use of this value here has
         * nothing to do with the other uses of the
         * field, but simplifies mechanics.)
         * PROPAGATE:  A releaseShared should be propagated to other
         * nodes. This is set (for head node only) in
         * doReleaseShared to ensure propagation
         * continues, even if other operations have
         * since intervened.
         * 0:          None of the above
         * <p>
         * 状态字段，只能取以下值：
         * SIGNAL -> successor node 正在（或即将）被阻塞（通过 park  方法），因此 current node 在 release 或 cancel 时必须唤醒它的 successor node。为了避免竞争，acquire 方法必须首先表明它们需要一个信号，然后重试原子获取，然后在失败时阻塞。
         * CANCELLED -> 由于超时或中断，current node 被 cancel 了。节点永远不会离开这个状态。特别是，被 cancel 的节点持有的线程永远不会再次阻塞。
         * CONDITION -> current node 当前在 condition 队列上。在被转移之前，它不会被用作同步队列节点，此时状态将被设置为 0。（这里使用这个值与字段的其他用途无关，但是简化了机制。）
         * PROPAGATE -> 一个 releaseShared 应该被传播到其他节点。这个值（仅对 head 节点）在 doReleaseShared 中设置，以确保传播继续，即使其他操作已经介入。
         *
         * <p>
         * The values are arranged numerically to simplify use.
         * Non-negative values mean that a node doesn't need to
         * signal. So, most code doesn't need to check for particular
         * values, just for sign.
         * <p>
         * 这些值是按数字顺序排列的，以简化使用。
         * 非负值表示节点不需要信号。
         * 因此，大多数代码不需要检查特定的值，只需要检查符号。
         *
         * <p>
         * The field is initialized to 0 for normal sync nodes, and
         * CONDITION for condition nodes.  It is modified using CAS
         * (or when possible, unconditional volatile writes).
         */
        // TODO REMIND watStatus >= 0 的判断就是在判断对应 node 是否被 cancel 了！
        volatile int waitStatus;

        /**
         * Link to predecessor node that current node/thread relies on
         * for checking waitStatus. Assigned during enqueuing, and nulled
         * out (for sake of GC) only upon dequeuing.  Also, upon
         * cancellation of a predecessor, we short-circuit while
         * finding a non-cancelled one, which will always exist
         * because the head node is never cancelled: A node becomes
         * head only as a result of successful acquire. A
         * cancelled thread never succeeds in acquiring, and a thread only
         * cancels itself, not any other node.
         * <p>
         * 链接到 prev node，当前节点/线程依赖它来检查 waitStatus。
         * 在 cur node 入队时分配，并且只在 cur node 出队时被置空（为了 GC），也就是说 prev node 出队时不会修改 cur node 的 prev 。
         * 此外，在 prev node 的 status 更新为 cancel 状态时，我们会短路地向后寻找，直到找到一个 status != cancel 的 node，这样的节点总是存在的，因为头节点永远不会被取消：一个节点只有在成功获取时才会成为头节点。
         * 一个 status 为 cancel 的线程永远不会成功获取锁，一个线程只会 cancel 自己，而不会 cancel 任何其他节点。
         */
        volatile Node prev;

        /**
         * Link to the successor node that the current node/thread
         * unparks upon release. Assigned during enqueuing, adjusted
         * when bypassing cancelled predecessors, and nulled out (for
         * sake of GC) when dequeued.  The enq operation does not
         * assign next field of a predecessor until after attachment,
         * so seeing a null next field does not necessarily mean that
         * node is at end of queue. However, if a next field appears
         * to be null, we can scan prev's from the tail to
         * double-check.  The next field of cancelled nodes is set to
         * point to the node itself instead of null, to make life
         * easier for isOnSyncQueue.
         * <p>
         * 链接到 succ node，当前节点/线程在释放时唤醒它。
         * 在 cur node 入队时分配，当绕过取消的前驱节点时进行调整，并且在 cur node 出队时被置空（为了 GC）。
         * 在 enqueue 操作中，直到完成追加操作之后，prev node 的 next 字段才会被赋值，因此看到一个 null 的 next 字段并不一定意味着节点在队列的末尾。
         * 但是，如果 next 字段看起来是 null，我们可以从尾部开始扫描 prev 节点来进行二次检查。
         * 取消节点的 next 字段被设置为指向节点本身，而不是 null，以使 isOnSyncQueue 方法更容易实现。
         */
        volatile Node next;

        /**
         * The thread that enqueued this node.  Initialized on
         * construction and nulled out after use.
         * <p>
         * 将当前节点入队的线程。在构造时初始化，并在使用后置空。
         * 也就是说，这个字段是用来记录当前节点是由哪个线程入队的。
         */
        volatile Thread thread;

        /**
         * Link to next node waiting on condition, or the special
         * value SHARED.  Because condition queues are accessed only
         * when holding in exclusive mode, we just need a simple
         * linked queue to hold nodes while they are waiting on
         * conditions. They are then transferred to the queue to
         * re-acquire. And because conditions can only be exclusive,
         * we save a field by using special value to indicate shared
         * mode.
         * <p>
         * 链接到下一个在 condition 上等待的节点，或者特殊值 SHARED。
         * 因为 condition 队列只在独占模式下访问，所以我们只需要一个简单的链接队列来保存节点，当它们在 condition 上等待时。
         * 然后它们被转移到队列中重新获取。
         */
        Node nextWaiter;

        // Used to establish initial head or SHARED marker
        // 仅用于建立初始的 head 或 SHARED 标记
        Node() {
        }

        // Used by addWaiter
        // 由 addWaiter 方法使用；会更新 nextWaiter -> SHARED/condition
        // TODO REMIND prev 和 next 用于 aqs 队列；nextWaiter 用于 condition 队列，是一个特殊的字段，用来标记当前节点是以 shared 模式还是 condition 模式等待；
        Node(Thread thread, Node mode) {
            this.nextWaiter = mode;
            this.thread = thread;
        }

        // Used by Condition
        // 由 Condition 使用；
        Node(Thread thread, int waitStatus) {
            this.waitStatus = waitStatus;
            this.thread = thread;
        }

        /**
         * Returns true if node is waiting in shared mode.
         * <p>
         * 如果节点正在以 shared 模式等待，则返回 true。
         */
        final boolean isShared() {
            return nextWaiter == SHARED;
        }

        /**
         * Returns previous node, or throws NullPointerException if null.
         * Use when predecessor cannot be null.  The null check could
         * be elided, but is present to help the VM.
         * <p>
         * 返回前驱节点，如果为 null，则抛出 NullPointerException。
         * 当前驱节点不能为 null 时使用。
         * null 检查可以省略，但是存在是为了帮助 VM。
         *
         * @return the predecessor of this node
         */
        final Node predecessor() throws NullPointerException {
            Node p = prev;
            if (p == null)
                throw new NullPointerException();
            else
                return p;
        }
    }

    /**
     * Condition implementation for a {@link
     * AbstractQueuedSynchronizer} serving as the basis of a {@link
     * Lock} implementation.
     * <p>
     * 用于 AbstractQueuedSynchronizer 的 Condition 实现，作为 Lock 实现的基础
     *
     * <p>Method documentation for this class describes mechanics,
     * not behavioral specifications from the point of view of Lock
     * and Condition users. Exported versions of this class will in
     * general need to be accompanied by documentation describing
     * condition semantics that rely on those of the associated
     * {@code AbstractQueuedSynchronizer}.
     * <p>
     * 该类的方法文档描述了机制，而不是从 Lock 和 Condition 用户的角度描述的行为规范。
     * 一般情况下，该类的导出版本需要附带描述依赖于关联的 AbstractQueuedSynchronizer 的条件语义的文档。
     * 也就是说，Condition 的行为规范需要依赖于 AbstractQueuedSynchronizer 的行为规范
     *
     * <p>This class is Serializable, but all fields are transient,
     * so deserialized conditions have no waiters.
     */
    public class ConditionObject implements Condition, java.io.Serializable {
        private static final long serialVersionUID = 1173984872572414699L;
        /**
         * First node of condition queue.
         * <p>
         * 条件队列的第一个节点
         */
        private transient Node firstWaiter;
        /**
         * Last node of condition queue.
         * <p>
         * 条件队列的最后一个节点
         */
        private transient Node lastWaiter;

        /**
         * Creates a new {@code ConditionObject} instance.
         * <p>
         * 创建一个新的 ConditionObject 实例
         */
        public ConditionObject() {
        }

        // Internal methods

        /**
         * Adds a new waiter to wait queue.
         *
         * @return its new wait node
         */
        private Node addConditionWaiter() {
            // 取到 condition 队列的最后一个节点
            Node t = lastWaiter;
            // If lastWaiter is cancelled, clean out.
            // 如果 lastWaiter 被取消了，发起一次整个 condition 队列的清理
            if (t != null && t.waitStatus != Node.CONDITION) {
                unlinkCancelledWaiters();
                // 重新取到 condition 队列的最后一个节点
                t = lastWaiter;
            }
            // 创建一个新的 node，状态为 CONDITION
            Node node = new Node(Thread.currentThread(), Node.CONDITION);
            // 将新 node 添加到 condition 队列的尾部
            if (t == null)
                firstWaiter = node;
            else
                t.nextWaiter = node;
            lastWaiter = node;
            return node;
        }

        /**
         * Removes and transfers nodes until hit non-cancelled one or
         * null. Split out from signal in part to encourage compilers
         * to inline the case of no waiters.
         * <p>
         * 删除并转移结点，直到遇到非取消的结点或 null
         * 从 signal 中分离出来，部分是为了鼓励编译器内联没有等待者的情况
         *
         * @param first (non-null) the first node on condition queue
         */
        private void doSignal(Node first) {
            do {
                // 此时 first 已经指向了旧的 firstWaiter
                // firstWaiter 向后移动一位；如果下一位是 null，将 lastWaiter 置为 null，表示当前 Condition 无线程等待
                if ((firstWaiter = first.nextWaiter) == null)
                    lastWaiter = null;
                // 将旧的 firstWaiter 的 nextWaiter 置为 null，表示已经从条件队列中移除
                first.nextWaiter = null;

                // 如果处理旧的 firstWaiter 失败 && firstWaiter 存在，就持续 while
            } while (!transferForSignal(first) &&
                    (first = firstWaiter) != null);
        }

        /**
         * Removes and transfers all nodes.
         *
         * @param first (non-null) the first node on condition queue
         */
        private void doSignalAll(Node first) {
            // 将 condition 队列的头尾指针清空
            lastWaiter = firstWaiter = null;

            // 遍历 condition 链表
            do {
                Node next = first.nextWaiter;
                // 打断 condition 链表的后继指针
                first.nextWaiter = null;
                // 将每个 condition 中等待的 node 都迁移到 aqs 队列中
                transferForSignal(first);
                first = next;
            } while (first != null);
        }

        /**
         * Unlinks cancelled waiter nodes from condition queue.
         * Called only while holding lock. This is called when
         * cancellation occurred during condition wait, and upon
         * insertion of a new waiter when lastWaiter is seen to have
         * been cancelled. This method is needed to avoid garbage
         * retention in the absence of signals. So even though it may
         * require a full traversal, it comes into play only when
         * timeouts or cancellations occur in the absence of
         * signals. It traverses all nodes rather than stopping at a
         * particular target to unlink all pointers to garbage nodes
         * without requiring many re-traversals during cancellation
         * storms.
         * <p>
         * 从 condition 队列中 unlink 取消的等待节点。
         * 仅在持有锁时调用。
         * 当在 condition 等待期间发生取消时，以及在看到 lastWaiter 已经被取消时插入新的等待者时，会调用此方法。
         * 在没有 signal 的情况下，需要此方法来避免垃圾保留。
         * 因此，即使它可能需要完全遍历，它只在超时或取消发生时没有信号时才起作用。
         * 它遍历所有节点，而不是在特定目标处停止，以取消所有指向垃圾节点的指针，而不需要在取消风暴期间进行多次重新遍历。
         */
        private void unlinkCancelledWaiters() {
            // 取到 condition 队列的第一个节点
            Node t = firstWaiter;
            // trail 代表 t 的有效前驱节点
            Node trail = null;
            // 通过 t 来迭代 condition 队列
            while (t != null) {
                // 取到 t 的后继节点
                Node next = t.nextWaiter;
                // 如果 t 的状态不是 CONDITION，表示 t 已经被取消了，需要将 t 从 condition 队列中 unlink
                if (t.waitStatus != Node.CONDITION) {
                    t.nextWaiter = null;
                    // 如果 trail 为 null，表示 t 是 condition 队列的第一个节点，需要更新 firstWaiter
                    if (trail == null)
                        firstWaiter = next;
                        // 否则，更新 trail 的 nextWaiter 指向 t 的 nextWaiter，表示跳过 t
                    else
                        trail.nextWaiter = next;
                    // 如果 next 为 null，表示 t 是 condition 队列的最后一个节点，需要更新 lastWaiter
                    if (next == null)
                        lastWaiter = trail;
                } else
                    // 否则说明 t 是有效节点，更新 trail 为 t
                    trail = t;
                t = next;
            }
        }

        // public methods

        /**
         * Moves the longest-waiting thread, if one exists, from the
         * wait queue for this condition to the wait queue for the
         * owning lock.
         * <p>
         * 将等待时间最长的线程（如果存在）从此条件的等待队列移动到拥有锁的等待队列
         * 所谓的等待时间最长，就是指 firstWaiter
         *
         * @throws IllegalMonitorStateException if {@link #isHeldExclusively}
         *                                      returns {@code false}
         */
        public final void signal() {
            // 如果当前线程不是独占锁的持有者，抛出 IllegalMonitorStateException 异常;
            // 也就是说，condition 只能由独占锁的持有者来调用
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            // 获取条件队列的第一个节点
            Node first = firstWaiter;
            if (first != null)
                doSignal(first);
        }

        /**
         * Moves all threads from the wait queue for this condition to
         * the wait queue for the owning lock.
         * <p>
         * 将所有线程从此条件的等待队列移动到拥有锁的等待队列
         *
         * @throws IllegalMonitorStateException if {@link #isHeldExclusively}
         *                                      returns {@code false}
         */
        public final void signalAll() {
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            Node first = firstWaiter;
            if (first != null)
                doSignalAll(first);
        }

        /**
         * Implements uninterruptible condition wait.
         * <ol>
         * <li> Save lock state returned by {@link #getState}.
         * <li> Invoke {@link #release} with saved state as argument,
         *      throwing IllegalMonitorStateException if it fails.
         * <li> Block until signalled.
         * <li> Reacquire by invoking specialized version of
         *      {@link #acquire} with saved state as argument.
         * </ol>
         * <p>
         *     实现不可中断的条件等待。
         *     1. 保存 getState 返回的锁状态。
         *     2. 使用保存的状态作为参数调用 release，如果失败则抛出 IllegalMonitorStateException。
         *     3. 阻塞直到被唤醒。
         *     4. 通过使用保存的状态作为参数调用 acquire 的专门版本来重新获取。
         */
        public final void awaitUninterruptibly() {
            // 基于当前线程，构造 node 结点，添加到 condition 队列中
            Node node = addConditionWaiter();
            // 释放锁，返回保存的状态，即重入的次数
            int savedState = fullyRelease(node);
            boolean interrupted = false;
            // isOnSyncQueue 会在 node 处于 syncQueue 时列中时返回 true，即如果 node 处于 condition queue 会返回 false
            // 进而 !isOnSyncQueue(node) 会返回 true
            // 也就是说，直到 node 被转移到 syncQueue 中，才会离开这个 while 循环，即当前仅当被 signal 时，才会离开这个 while 循环
            while (!isOnSyncQueue(node)) {
                // 阻塞当前线程，直到被唤醒
                LockSupport.park(this);
                // 如果当前线程被中断，设置 interrupted 为 true
                if (Thread.interrupted())
                    interrupted = true;
            }
            // 当代码执行到这里的时候，当前线程已经被唤醒了
            // 那么会尝试重新获取锁，如果获取失败，或者在等待过程中被中断，会抛出 InterruptedException 异常
            // 只有当真正获取到锁之后，才会从 acquireQueued 方法中返回；然后它的返回值就是是否需要中断当前线程
            if (acquireQueued(node, savedState) || interrupted)
                // 调用 currentThread 的 interrupt 方法，设置中断标志位
                selfInterrupt();
        }

        /*
         * For interruptible waits, we need to track whether to throw
         * InterruptedException, if interrupted while blocked on
         * condition, versus reinterrupt current thread, if
         * interrupted while blocked waiting to re-acquire.
         * <p>
         * 对于可中断的等待，我们需要跟踪是否在 Condition 上阻塞时被 intterrupt 而抛出 InterruptedException，
         * 还是在等待重新获取时被中断而重新中断当前线程。
         */

        /**
         * Mode meaning to reinterrupt on exit from wait
         * <p>
         * 意味着在退出等待时重新中断
         */
        private static final int REINTERRUPT = 1;
        /**
         * Mode meaning to throw InterruptedException on exit from wait
         * <p>
         * 意味着在退出等待时抛出 InterruptedException
         */
        private static final int THROW_IE = -1;

        /**
         * Checks for interrupt, returning THROW_IE if interrupted
         * before signalled, REINTERRUPT if after signalled, or
         * 0 if not interrupted.
         * <p>
         * 检查中断，
         * 如果在被 signal 之前被中断，返回 THROW_IE；
         * 如果在被 signal 之后被中断，返回 REINTERRUPT；
         * 如果没有被中断，返回 0。
         */
        private int checkInterruptWhileWaiting(Node node) {
            return Thread.interrupted() ?
                    (transferAfterCancelledWait(node) // 在被 interrupt 之后尝试将 node 从 condition 队列转移到 aqs 队列（尽管已经被 cancel）
                            ? THROW_IE : // 如果在 signal 之前被 enq 到 aqs 队列，返回 THROW_IE
                            REINTERRUPT) : // 如果在 signal 之后被 enq 到 aqs 队列，返回 REINTERRUPT
                    0; // 0 表示没有被中断
        }

        /**
         * Throws InterruptedException, reinterrupts current thread, or
         * does nothing, depending on mode.
         */
        private void reportInterruptAfterWait(int interruptMode)
                throws InterruptedException {
            if (interruptMode == THROW_IE)
                throw new InterruptedException();
            else if (interruptMode == REINTERRUPT)
                selfInterrupt();
        }

        /**
         * Implements interruptible condition wait.
         * <ol>
         * <li> If current thread is interrupted, throw InterruptedException.
         * <li> Save lock state returned by {@link #getState}.
         * <li> Invoke {@link #release} with saved state as argument,
         *      throwing IllegalMonitorStateException if it fails.
         * <li> Block until signalled or interrupted.
         * <li> Reacquire by invoking specialized version of
         *      {@link #acquire} with saved state as argument.
         * <li> If interrupted while blocked in step 4, throw InterruptedException.
         * </ol>
         * <p>
         *     实现可中断的条件等待。
         *     1. 如果当前线程被中断，抛出 InterruptedException。
         *     2. 保存 getState 返回的锁状态。
         *     3. 使用保存的状态作为参数调用 release，如果失败则抛出 IllegalMonitorStateException。
         *     4. 阻塞直到被唤醒或中断。
         *     5. 通过使用保存的状态作为参数调用 acquire 的专门版本来重新获取。
         *     6. 如果在步骤 4 中被阻塞时被中断，抛出 InterruptedException。
         */
        public final void await() throws InterruptedException {
            // 如果当前线程已经被中断，抛出 InterruptedException
            if (Thread.interrupted())
                throw new InterruptedException();
            // 基于当前线程，构造 node 结点，添加到 condition 队列中
            Node node = addConditionWaiter();
            // 释放锁，返回保存的状态，即重入的次数；释放锁之后，其他线程就会开始竞争了
            int savedState = fullyRelease(node);
            // 记录中断模式
            int interruptMode = 0;
            // 如果 node 不在 aqs 队列中，就阻塞当前线程，直到被唤醒后再次校验中断状态
            while (!isOnSyncQueue(node)) {
                // 阻塞当前线程，直到被唤醒
                LockSupport.park(this);
                // 当代码执行到这里的时候，意味着当前线程已经被唤醒了
                // 校验中断状态，即使真的被中断了，也会将当前线程的 node 转到 aqs 队列中
                if ((interruptMode = checkInterruptWhileWaiting(node)) != 0)
                    break;
            }
            // 当代码执行到这里以后，说明已经到达了 aqs 队列中，且已经被唤醒了
            // 那么会尝试已指定的状态重新获取锁，如果获取失败，或者在等待过程中被中断，会抛出 InterruptedException 异常
            if (acquireQueued(node, savedState) && interruptMode != THROW_IE)
                // 记录是否需要再次中断当前线程，即再次将 Thread 中的中断标志位设置为 true
                interruptMode = REINTERRUPT;

            // 如果 node 的 nextWaiter 不为 null，说明 node 不是通过 signal 被迁移到 aqs 队列中的；即当前节点已经被 cancel 了
            // 如果代码执行到了这里，说明 node 已经获取到锁了，或者在等待过程中遇到异常了
            if (node.nextWaiter != null) // clean up if cancelled
                unlinkCancelledWaiters();
            if (interruptMode != 0)
                reportInterruptAfterWait(interruptMode);
        }

        /**
         * Implements timed condition wait.
         * <ol>
         * <li> If current thread is interrupted, throw InterruptedException.
         * <li> Save lock state returned by {@link #getState}.
         * <li> Invoke {@link #release} with saved state as argument,
         *      throwing IllegalMonitorStateException if it fails.
         * <li> Block until signalled, interrupted, or timed out.
         * <li> Reacquire by invoking specialized version of
         *      {@link #acquire} with saved state as argument.
         * <li> If interrupted while blocked in step 4, throw InterruptedException.
         * </ol>
         * <p>
         *     实现定时条件等待。
         *     1. 如果当前线程被中断，抛出 InterruptedException。
         *     2. 保存 getState 返回的锁状态。
         *     3. 使用保存的状态作为参数调用 release，如果失败则抛出 IllegalMonitorStateException。
         *     4. 阻塞直到被唤醒、中断或超时。
         *     5. 通过使用保存的状态作为参数调用 acquire 的专门版本来重新获取。
         *     6. 如果在步骤 4 中被阻塞时被中断，抛出 InterruptedException。
         */
        public final long awaitNanos(long nanosTimeout)
                throws InterruptedException {
            // 如果当前线程已经被中断，抛出 InterruptedException
            if (Thread.interrupted())
                throw new InterruptedException();
            // 基于当前线程，构造 node 结点，添加到 condition 队列中
            Node node = addConditionWaiter();
            // 释放锁，返回保存的状态，即重入的次数；释放锁之后，其他线程就会开始竞争了
            int savedState = fullyRelease(node);
            // 计算超时时间
            final long deadline = System.nanoTime() + nanosTimeout;
            // 记录中断模式
            int interruptMode = 0;
            // 如果 node 不在 aqs 队列中，就阻塞当前线程，直到被唤醒后再次校验中断状态
            while (!isOnSyncQueue(node)) {
                // 如果 nanosTimeout <= 0L，说明已经超时了，直接将 node 从 condition 队列转移到 aqs 队列
                if (nanosTimeout <= 0L) {
                    transferAfterCancelledWait(node);
                    break;
                }
                // 如果 nanosTimeout >= spinForTimeoutThreshold，说明还有较长的等待时间，直接阻塞当前线程
                if (nanosTimeout >= spinForTimeoutThreshold)
                    LockSupport.parkNanos(this, nanosTimeout);
                // 执行到这里的时候，说明当前线程已经处于唤醒状态。
                // 校验中断状态，即使真的被中断了，也会将当前线程的 node 转到 aqs 队列中
                if ((interruptMode = checkInterruptWhileWaiting(node)) != 0)
                    break;
                // 每次被唤醒后，都需要重新计算剩余的等待时间
                nanosTimeout = deadline - System.nanoTime();
            }
            // reacquire 会尝试已指定的状态重新获取锁，如果获取失败，或者在等待过程中被中断，会抛出 InterruptedException 异常
            if (acquireQueued(node, savedState) && interruptMode != THROW_IE)
                interruptMode = REINTERRUPT;
            if (node.nextWaiter != null)
                unlinkCancelledWaiters();
            if (interruptMode != 0)
                reportInterruptAfterWait(interruptMode);
            // 返回剩余的等待时间
            return deadline - System.nanoTime();
        }

        /**
         * Implements absolute timed condition wait.
         * <ol>
         * <li> If current thread is interrupted, throw InterruptedException.
         * <li> Save lock state returned by {@link #getState}.
         * <li> Invoke {@link #release} with saved state as argument,
         *      throwing IllegalMonitorStateException if it fails.
         * <li> Block until signalled, interrupted, or timed out.
         * <li> Reacquire by invoking specialized version of
         *      {@link #acquire} with saved state as argument.
         * <li> If interrupted while blocked in step 4, throw InterruptedException.
         * <li> If timed out while blocked in step 4, return false, else true.
         * </ol>
         * <p>
         *     实现绝对定时条件等待。
         *     1. 如果当前线程被中断，抛出 InterruptedException。
         *     2. 保存 getState 返回的锁状态。
         *     3. 使用保存的状态作为参数调用 release，如果失败则抛出 IllegalMonitorStateException。
         *     4. 阻塞直到被唤醒、中断或超时。
         *     5. 通过使用保存的状态作为参数调用 acquire 的专门版本来重新获取。
         *     6. 如果在步骤 4 中被阻塞时被中断，抛出 InterruptedException。
         *     7. 如果在步骤 4 中被阻塞时超时，返回 false，否则返回 true。
         */
        public final boolean awaitUntil(Date deadline)
                throws InterruptedException {
            // 获取 deadline 的时间戳
            long abstime = deadline.getTime();
            // 如果当前线程已经被中断，抛出 InterruptedException
            if (Thread.interrupted())
                throw new InterruptedException();
            // 基于当前线程，构造 node 结点，添加到 condition 队列中
            Node node = addConditionWaiter();
            // 释放锁，返回保存的状态，即重入的次数；释放锁之后，其他线程就会开始竞争了
            int savedState = fullyRelease(node);
            // 记录是否超时
            boolean timedout = false;
            // 记录中断模式
            int interruptMode = 0;
            // 如果 node 不在 aqs 队列中，就阻塞当前线程，直到被唤醒后再次校验中断状态
            while (!isOnSyncQueue(node)) {
                // 如果当前时间已经超过了 deadline，直接将 node 从 condition 队列转移到 aqs 队列
                if (System.currentTimeMillis() > abstime) {
                    // 更新 timedout 为 true
                    timedout = transferAfterCancelledWait(node);
                    break;
                }
                // 阻塞当前线程，直到被唤醒
                LockSupport.parkUntil(this, abstime);
                // 当代码执行到这里的时候，意味着当前线程已经被唤醒了
                if ((interruptMode = checkInterruptWhileWaiting(node)) != 0)
                    break;
            }
            if (acquireQueued(node, savedState) && interruptMode != THROW_IE)
                interruptMode = REINTERRUPT;
            if (node.nextWaiter != null)
                unlinkCancelledWaiters();
            if (interruptMode != 0)
                reportInterruptAfterWait(interruptMode);
            return !timedout;
        }

        /**
         * Implements timed condition wait.
         * <ol>
         * <li> If current thread is interrupted, throw InterruptedException.
         * <li> Save lock state returned by {@link #getState}.
         * <li> Invoke {@link #release} with saved state as argument,
         *      throwing IllegalMonitorStateException if it fails.
         * <li> Block until signalled, interrupted, or timed out.
         * <li> Reacquire by invoking specialized version of
         *      {@link #acquire} with saved state as argument.
         * <li> If interrupted while blocked in step 4, throw InterruptedException.
         * <li> If timed out while blocked in step 4, return false, else true.
         * </ol>
         * <p>
         *     实现定时条件等待。
         *     1. 如果当前线程被中断，抛出 InterruptedException。
         *      2. 保存 getState 返回的锁状态。
         *      3. 使用保存的状态作为参数调用 release，如果失败则抛出 IllegalMonitorStateException。
         *      4. 阻塞直到被唤醒、中断或超时。
         *      5. 通过使用保存的状态作为参数调用 acquire 的专门版本来重新获取。
         *      6. 如果在步骤 4 中被阻塞时被中断，抛出 InterruptedException。
         *      7. 如果在步骤 4 中被阻塞时超时，返回 false，否则返回 true。
         */
        public final boolean await(long time, TimeUnit unit)
                throws InterruptedException {
            // 将 time 转换为纳秒
            long nanosTimeout = unit.toNanos(time);
            // 如果当前线程已经被中断，抛出 InterruptedException
            if (Thread.interrupted())
                throw new InterruptedException();
            // 基于当前线程，构造 node 结点，添加到 condition 队列中
            Node node = addConditionWaiter();
            // 释放锁，返回保存的状态，即重入的次数；释放锁之后，其他线程就会开始竞争了
            int savedState = fullyRelease(node);
            // 记录是否超时
            final long deadline = System.nanoTime() + nanosTimeout;
            // 记录是否超时
            boolean timedout = false;
            // 记录中断模式
            int interruptMode = 0;
            // 如果 node 不在 aqs 队列中，就阻塞当前线程，直到被唤醒后再次校验中断状态
            while (!isOnSyncQueue(node)) {
                // 如果 nanosTimeout <= 0L，说明已经超时了，直接将 node 从 condition 队列转移到 aqs 队列
                if (nanosTimeout <= 0L) {
                    timedout = transferAfterCancelledWait(node);
                    break;
                }
                // 如果 nanosTimeout >= spinForTimeoutThreshold，说明还有较长的等待时间，直接阻塞当前线程
                if (nanosTimeout >= spinForTimeoutThreshold)
                    LockSupport.parkNanos(this, nanosTimeout);
                // 执行到这里的时候，说明当前线程已经处于唤醒状态。
                if ((interruptMode = checkInterruptWhileWaiting(node)) != 0)
                    break;
                // 每次被唤醒后，都需要重新计算剩余的等待时间
                nanosTimeout = deadline - System.nanoTime();
            }

            if (acquireQueued(node, savedState) && interruptMode != THROW_IE)
                interruptMode = REINTERRUPT;
            if (node.nextWaiter != null)
                unlinkCancelledWaiters();
            if (interruptMode != 0)
                reportInterruptAfterWait(interruptMode);
            return !timedout;
        }

        //  support for instrumentation

        /**
         * Returns true if this condition was created by the given
         * synchronization object.
         * <p>
         *     如果此条件是由给定的同步对象创建的，则返回 true。
         *
         * @return {@code true} if owned
         */
        final boolean isOwnedBy(AbstractQueuedSynchronizer sync) {
            // 获取外部类的当前对象（AbstractQueuedSynchronizer.this）
            return sync == AbstractQueuedSynchronizer.this;
        }

        /**
         * Queries whether any threads are waiting on this condition.
         * Implements {@link AbstractQueuedSynchronizer#hasWaiters(ConditionObject)}.
         *
         * @return {@code true} if there are any waiting threads
         * @throws IllegalMonitorStateException if {@link #isHeldExclusively}
         *                                      returns {@code false}
         */
        protected final boolean hasWaiters() {
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            for (Node w = firstWaiter; w != null; w = w.nextWaiter) {
                if (w.waitStatus == Node.CONDITION)
                    return true;
            }
            return false;
        }

        /**
         * Returns an estimate of the number of threads waiting on
         * this condition.
         * Implements {@link AbstractQueuedSynchronizer#getWaitQueueLength(ConditionObject)}.
         *
         * @return the estimated number of waiting threads
         * @throws IllegalMonitorStateException if {@link #isHeldExclusively}
         *                                      returns {@code false}
         */
        protected final int getWaitQueueLength() {
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            int n = 0;
            for (Node w = firstWaiter; w != null; w = w.nextWaiter) {
                if (w.waitStatus == Node.CONDITION)
                    ++n;
            }
            return n;
        }

        /**
         * Returns a collection containing those threads that may be
         * waiting on this Condition.
         * Implements {@link AbstractQueuedSynchronizer#getWaitingThreads(ConditionObject)}.
         *
         * @return the collection of threads
         * @throws IllegalMonitorStateException if {@link #isHeldExclusively}
         *                                      returns {@code false}
         */
        protected final Collection<Thread> getWaitingThreads() {
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            ArrayList<Thread> list = new ArrayList<Thread>();
            for (Node w = firstWaiter; w != null; w = w.nextWaiter) {
                if (w.waitStatus == Node.CONDITION) {
                    Thread t = w.thread;
                    if (t != null)
                        list.add(t);
                }
            }
            return list;
        }
    }

    /**
     * Setup to support compareAndSet. We need to natively implement
     * this here: For the sake of permitting future enhancements, we
     * cannot explicitly subclass AtomicInteger, which would be
     * efficient and useful otherwise. So, as the lesser of evils, we
     * natively implement using hotspot intrinsics API. And while we
     * are at it, we do the same for other CASable fields (which could
     * otherwise be done with atomic field updaters).
     */
    private static final Unsafe unsafe = Unsafe.getUnsafe();
    private static final long stateOffset;
    private static final long headOffset;
    private static final long tailOffset;
    private static final long waitStatusOffset;
    private static final long nextOffset;

    static {
        try {
            stateOffset = unsafe.objectFieldOffset
                    (AbstractQueuedSynchronizer.class.getDeclaredField("state"));
            headOffset = unsafe.objectFieldOffset
                    (AbstractQueuedSynchronizer.class.getDeclaredField("head"));
            tailOffset = unsafe.objectFieldOffset
                    (AbstractQueuedSynchronizer.class.getDeclaredField("tail"));
            waitStatusOffset = unsafe.objectFieldOffset
                    (Node.class.getDeclaredField("waitStatus"));
            nextOffset = unsafe.objectFieldOffset
                    (Node.class.getDeclaredField("next"));

        } catch (Exception ex) {
            throw new Error(ex);
        }
    }

    /**
     * CAS head field. Used only by enq.
     */
    private final boolean compareAndSetHead(Node update) {
        return unsafe.compareAndSwapObject(this, headOffset, null, update);
    }

    /**
     * CAS tail field. Used only by enq.
     */
    private final boolean compareAndSetTail(Node expect, Node update) {
        return unsafe.compareAndSwapObject(this, tailOffset, expect, update);
    }

    /**
     * CAS waitStatus field of a node.
     */
    private static final boolean compareAndSetWaitStatus(Node node,
                                                         int expect,
                                                         int update) {
        return unsafe.compareAndSwapInt(node, waitStatusOffset,
                expect, update);
    }

    /**
     * CAS next field of a node.
     */
    private static final boolean compareAndSetNext(Node node,
                                                   Node expect,
                                                   Node update) {
        return unsafe.compareAndSwapObject(node, nextOffset, expect, update);
    }
}
