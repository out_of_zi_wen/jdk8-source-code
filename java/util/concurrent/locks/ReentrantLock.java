/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * A reentrant mutual exclusion {@link Lock} with the same basic
 * behavior and semantics as the implicit monitor lock accessed using
 * {@code synchronized} methods and statements, but with extended
 * capabilities.
 * <p>
 *     一个可重入的互斥锁，具有与使用 synchronized 方法和语句访问的隐式监视器锁相同的基本行为和语义，但具有扩展功能。
 *
 * <p>A {@code ReentrantLock} is <em>owned</em> by the thread last
 * successfully locking, but not yet unlocking it. A thread invoking
 * {@code lock} will return, successfully acquiring the lock, when
 * the lock is not owned by another thread. The method will return
 * immediately if the current thread already owns the lock. This can
 * be checked using methods {@link #isHeldByCurrentThread}, and {@link
 * #getHoldCount}.
 * <p>
 *     一个 ReentrantLock 是由最后一个成功锁定但尚未解锁的线程拥有的。
 *     当锁不被另一个线程拥有时，调用 lock 的线程将返回，成功获取锁。
 *     如果当前线程已经拥有锁，该方法将立即返回。
 *     可以使用 isHeldByCurrentThread 和 getHoldCount 方法进行检查。
 *
 * <p>The constructor for this class accepts an optional
 * <em>fairness</em> parameter.  When set {@code true}, under
 * contention, locks favor granting access to the longest-waiting
 * thread.  Otherwise this lock does not guarantee any particular
 * access order.  Programs using fair locks accessed by many threads
 * may display lower overall throughput (i.e., are slower; often much
 * slower) than those using the default setting, but have smaller
 * variances in times to obtain locks and guarantee lack of
 * starvation. Note however, that fairness of locks does not guarantee
 * fairness of thread scheduling. Thus, one of many threads using a
 * fair lock may obtain it multiple times in succession while other
 * active threads are not progressing and not currently holding the
 * lock.
 * Also note that the untimed {@link #tryLock()} method does not
 * honor the fairness setting. It will succeed if the lock
 * is available even if other threads are waiting.
 * <p>
 *     该类的构造函数接受一个可选的公平性参数。
 *     当设置为 true 时，在争用情况下，锁倾向于授予最长等待的线程访问。
 *     否则，此锁不保证任何特定的访问顺序。
 *     使用许多线程访问的公平锁的程序可能显示较低的总体吞吐量（即，速度较慢；通常慢得多），
 *     而不是使用默认设置的程序，但在获取锁的时间上具有较小的方差，并保证不会饿死。
 *     但是，请注意，锁的公平性不能保证线程调度的公平性。
 *     因此，使用公平锁的众多线程之一可能会连续多次获得它，而其他活动线程没有进展并且当前没有持有锁。
 *     还要注意，未定时的 tryLock() 方法不遵守公平性设置。
 *     如果锁可用，即使其他线程正在等待，它也会成功。
 *
 * <p>It is recommended practice to <em>always</em> immediately
 * follow a call to {@code lock} with a {@code try} block, most
 * typically in a before/after construction such as:
 *
 *  <pre> {@code
 * class X {
 *   private final ReentrantLock lock = new ReentrantLock();
 *   // ...
 *
 *   public void m() {
 *     lock.lock();  // block until condition holds
 *     try {
 *       // ... method body
 *     } finally {
 *       lock.unlock()
 *     }
 *   }
 * }}</pre>
 *
 * <p>In addition to implementing the {@link Lock} interface, this
 * class defines a number of {@code public} and {@code protected}
 * methods for inspecting the state of the lock.  Some of these
 * methods are only useful for instrumentation and monitoring.
 * <p>
 *     除了实现 Lock 接口之外，该类还定义了许多用于检查锁状态的 public 和 protected 方法。
 *     这些方法中的一些方法仅用于仪器和监视。
 *
 * <p>Serialization of this class behaves in the same way as built-in
 * locks: a deserialized lock is in the unlocked state, regardless of
 * its state when serialized.
 * <p>
 *     该类的序列化行为与内置锁的行为相同：反序列化的锁处于解锁状态，而不管其序列化时的状态如何。
 *
 * <p>This lock supports a maximum of 2147483647 recursive locks by
 * the same thread. Attempts to exceed this limit result in
 * {@link Error} throws from locking methods.
 * <p>
 *     该锁支持同一线程的最大 2147483647 个递归锁。
 *
 * @since 1.5
 * @author Doug Lea
 */
public class ReentrantLock implements Lock, java.io.Serializable {
    private static final long serialVersionUID = 7373984872572414699L;
    /**
     * Synchronizer providing all implementation mechanics
     * <p>
     * 提供所有实现机制的同步器
     */
    private final Sync sync;

    /**
     * Base of synchronization control for this lock. Subclassed
     * into fair and nonfair versions below. Uses AQS state to
     * represent the number of holds on the lock.
     * <p>
     *     该锁的同步控制基础。
     *     在下面的公平和非公平版本中进行子类化
     *     。使用 AQS 状态来表示锁上的保持数。
     */
    abstract static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = -5179523762034025860L;

        /**
         * Performs {@link Lock#lock}. The main reason for subclassing
         * is to allow fast path for nonfair version.
         * <p>
         *     执行 Lock.lock。
         *     子类化的主要原因是允许非公平版本的快速路径。
         */
        abstract void lock();

        /**
         * Performs non-fair tryLock.  tryAcquire is implemented in
         * subclasses, but both need nonfair try for trylock method.
         * <p>
         *     执行非公平的 tryLock。
         *     tryAcquire 在子类中实现，但是 trylock 方法都需要非公平的 try。
         */
        final boolean nonfairTryAcquire(int acquires) {
            // 获取当前线程
            final Thread current = Thread.currentThread();
            // 获取当前 state
            int c = getState();
            // 如果 state 为 0，表示当前锁没有被占用
            if (c == 0) {
                // 直接尝试抢占锁
                if (compareAndSetState(0, acquires)) {
                    // 抢占成功后，设置 exclusiveOwnerThread 为当前线程
                    setExclusiveOwnerThread(current);
                    return true;
                }

                // 判断是否是锁的重入
            } else if (current == getExclusiveOwnerThread()) {
                // 如果是锁的重入，则直接增加 state
                int nextc = c + acquires;
                if (nextc < 0) // overflow
                    throw new Error("Maximum lock count exceeded");
                // 直接 set 即可；因为只有 owner 能 set，所以不需要 cas
                setState(nextc);
                return true;
            }
            return false;
        }

        protected final boolean tryRelease(int releases) {
            // 计算 release 之后的 state
            int c = getState() - releases;
            // 如果当前线程不是持有锁的线程，则抛出异常
            if (Thread.currentThread() != getExclusiveOwnerThread())
                throw new IllegalMonitorStateException();
            // 记录是否释放锁成功
            boolean free = false;
            if (c == 0) {
                // c == 0 就意味着所有的重用次数都被 release 掉了
                // 即锁已彻底释放
                free = true;
                // 释放锁后，将 exclusiveOwnerThread 设置为 null
                setExclusiveOwnerThread(null);
            }
            // 更新 state
            setState(c);
            return free;
        }

        protected final boolean isHeldExclusively() {
            // While we must in general read state before owner,
            // we don't need to do so to check if current thread is owner
            // 判断当前线程是否持有锁
            return getExclusiveOwnerThread() == Thread.currentThread();
        }

        final ConditionObject newCondition() {
            // 基于当前 lock 构造一个 ConditionObject
            return new ConditionObject();
        }

        // Methods relayed from outer class

        final Thread getOwner() {
            // 获取锁的持有者
            return getState() == 0 ? null : getExclusiveOwnerThread();
        }

        final int getHoldCount() {
            // 获取当前线程持有锁的次数，即重入的次数；
            // 如果当前线程没有持有锁，则返回 0；
            return isHeldExclusively() ? getState() : 0;
        }

        final boolean isLocked() {
            // 判断该锁当前是否被占用
            return getState() != 0;
        }

        /**
         * Reconstitutes the instance from a stream (that is, deserializes it).
         * <p>
         *     从流中重新构造实例（即对其进行反序列化）。
         *     即每次反序列化的时候都会把 state 设置为 0，保证反序列化后的锁是解锁状态。
         */
        private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
            s.defaultReadObject();
            setState(0); // reset to unlocked state
        }
    }

    /**
     * Sync object for non-fair locks
     * <p>
     *     非公平锁的 Sync 对象
     */
    static final class NonfairSync extends Sync {
        private static final long serialVersionUID = 7316153563782823691L;

        /**
         * Performs lock.  Try immediate barge, backing up to normal
         * acquire on failure.
         * <p>
         *     执行 lock。
         *     尝试立即 barging，如果失败则回退到正常的 acquire。
         */
        final void lock() {
            // 一上来就尝试 cas 设置 state 为 1
            if (compareAndSetState(0, 1)) {
                // 成功了就设置 exclusiveOwnerThread 为当前线程
                setExclusiveOwnerThread(Thread.currentThread());

                // 否则，调用父类的 acquire 方法；
                // 会触发 tryAccquire 方法，进而调用 nonfairTryAcquire 方法；
                // 如果还是失败，则调用 acquireQueued，入队等待。
            } else {
                acquire(1);
            }
        }

        protected final boolean tryAcquire(int acquires) {
            return nonfairTryAcquire(acquires);
        }
    }

    /**
     * Sync object for fair locks
     * <p>
     *     公平锁的 Sync 对象
     */
    static final class FairSync extends Sync {
        private static final long serialVersionUID = -3000897897090466540L;

        final void lock() {
            // 上来就直接调用父类的方法；
            // 由于父类中会调用 tryAcquire，因此 FairSync 还重写了 tryAcquire 方法；
            acquire(1);
        }

        /**
         * Fair version of tryAcquire.  Don't grant access unless
         * recursive call or no waiters or is first.
         * <p>
         *     tryAcquire 的公平版本。
         *     除非是递归调用，或者没有等待者，或者是第一个，否则不授予访问权限。
         */
        protected final boolean tryAcquire(int acquires) {
            // 获取当前线程
            final Thread current = Thread.currentThread();
            // 获取 aqs 的 state（占有次数）
            int c = getState();
            // 如果此时没有线程持有锁
            if (c == 0) {
                // 如果队列中没有等待者，或者当前线程是队列中的第一个线程
                // 就立即尝试 cas；
                // 成功了就返回
                if (!hasQueuedPredecessors() &&
                    compareAndSetState(0, acquires)) {
                    setExclusiveOwnerThread(current);
                    return true;
                }

                // 否则判断是否是公平锁的重入
            } else if (current == getExclusiveOwnerThread()) {
                int nextc = c + acquires;
                if (nextc < 0)
                    throw new Error("Maximum lock count exceeded");
                setState(nextc);
                return true;
            }
            // 否则返回 false，代表 tryAcquire 失败；
            // 后续会由 acquire 方法将当前 Thread 封装为 Node 进入 CLH 队列等待；
            return false;
        }
    }

    /**
     * Creates an instance of {@code ReentrantLock}.
     * This is equivalent to using {@code ReentrantLock(false)}.
     * <p>
     *     创建一个 ReentrantLock 实例。
     *     这相当于使用 ReentrantLock(false)。
     */
    public ReentrantLock() {
        sync = new NonfairSync();
    }

    /**
     * Creates an instance of {@code ReentrantLock} with the
     * given fairness policy.
     * <p>
     *     使用给定的公平性策略创建 ReentrantLock 实例。
     *
     * @param fair {@code true} if this lock should use a fair ordering policy
     */
    public ReentrantLock(boolean fair) {
        sync = fair ? new FairSync() : new NonfairSync();
    }

    /**
     * Acquires the lock.
     * <p>
     *     获取锁。
     *
     * <p>Acquires the lock if it is not held by another thread and returns
     * immediately, setting the lock hold count to one.
     * <p>
     *     如果锁没有被其他线程持有，则立即获取锁，并将锁保持计数设置为 1。
     *
     * <p>If the current thread already holds the lock then the hold
     * count is incremented by one and the method returns immediately.
     * <p>
     *     如果当前线程已经持有锁，则保持计数将增加 1，并立即返回。
     *
     * <p>If the lock is held by another thread then the
     * current thread becomes disabled for thread scheduling
     * purposes and lies dormant until the lock has been acquired,
     * at which time the lock hold count is set to one.
     * <p>
     *     如果锁被另一个线程持有，则当前线程将被禁用以进行线程调度，并处于休眠状态，直到获取锁为止。
     */
    public void lock() {
        sync.lock();
    }

    /**
     * Acquires the lock unless the current thread is
     * {@linkplain Thread#interrupt interrupted}.
     * <p>
     *     获取锁，除非当前线程被中断。
     *
     * <p>Acquires the lock if it is not held by another thread and returns
     * immediately, setting the lock hold count to one.
     * <p>
     *     如果锁没有被其他线程持有，则立即获取锁，并将锁保持计数设置为 1。
     *
     * <p>If the current thread already holds this lock then the hold count
     * is incremented by one and the method returns immediately.
     * <p>
     *     如果当前线程已经持有此锁，则保持计数将增加 1，并立即返回。
     *
     * <p>If the lock is held by another thread then the
     * current thread becomes disabled for thread scheduling
     * purposes and lies dormant until one of two things happens:
     *
     * <ul>
     *
     * <li>The lock is acquired by the current thread; or
     *
     * <li>Some other thread {@linkplain Thread#interrupt interrupts} the
     * current thread.
     *
     * </ul>
     * <p>
     *     如果锁被另一个线程持有，则当前线程将被禁用以进行线程调度，并处于休眠状态，直到发生以下两种情况之一：
     *     1. 当前线程获取了锁；
     *     2. 其他线程中断了当前线程。
     *
     * <p>If the lock is acquired by the current thread then the lock hold
     * count is set to one.
     * <p>
     *     如果当前线程获取了锁，则锁保持计数将设置为 1。
     *
     * <p>If the current thread:
     *
     * <ul>
     *
     * <li>has its interrupted status set on entry to this method; or
     *
     * <li>is {@linkplain Thread#interrupt interrupted} while acquiring
     * the lock,
     *
     * </ul>
     *
     * then {@link InterruptedException} is thrown and the current thread's
     * interrupted status is cleared.
     * <p>
     *     如果当前线程：
     *     1. 在进入此方法时设置了中断状态；或者
     *     2. 在获取锁时被中断，
     *     则将抛出 InterruptedException，并清除当前线程的中断状态。
     *
     *
     * <p>In this implementation, as this method is an explicit
     * interruption point, preference is given to responding to the
     * interrupt over normal or reentrant acquisition of the lock.
     *
     * @throws InterruptedException if the current thread is interrupted
     */
    public void lockInterruptibly() throws InterruptedException {
        // 直接调用了 aqs 的 acquireInterruptibly 方法；
        // aqs 的 acquireInterruptibly 方法会调用 tryAcquire 方法；
        // 进而调用到 fairSync/nonFairSync 中的 tryAcquire 方法；
        sync.acquireInterruptibly(1);
    }

    /**
     * Acquires the lock only if it is not held by another thread at the time
     * of invocation.
     * <p>
     *     仅在调用时锁没有被其他线程持有时才获取锁。
     *
     * <p>Acquires the lock if it is not held by another thread and
     * returns immediately with the value {@code true}, setting the
     * lock hold count to one. Even when this lock has been set to use a
     * fair ordering policy, a call to {@code tryLock()} <em>will</em>
     * immediately acquire the lock if it is available, whether or not
     * other threads are currently waiting for the lock.
     * This &quot;barging&quot; behavior can be useful in certain
     * circumstances, even though it breaks fairness. If you want to honor
     * the fairness setting for this lock, then use
     * {@link #tryLock(long, TimeUnit) tryLock(0, TimeUnit.SECONDS) }
     * which is almost equivalent (it also detects interruption).
     * <p>
     *     如果锁没有被其他线程持有，则立即获取锁并返回 true，将锁保持计数设置为 1。
     *     即使此锁已设置为使用公平排序策略，调用 tryLock() 也会立即获取锁（如果可用），无论其他线程当前是否正在等待锁。
     *     这种“插队”行为在某些情况下可能很有用，尽管它会破坏公平性。
     *     如果要遵守此锁的公平性设置，请使用 tryLock(0, TimeUnit.SECONDS)（它还会检测中断），这几乎是等效的。
     *
     * <p>If the current thread already holds this lock then the hold
     * count is incremented by one and the method returns {@code true}.
     * <p>
     *     如果当前线程已经持有此锁，则保持计数将增加 1，并返回 true。
     *
     * <p>If the lock is held by another thread then this method will return
     * immediately with the value {@code false}.
     * <p>
     *     如果锁被另一个线程持有，则此方法将立即返回 false。
     *
     * @return {@code true} if the lock was free and was acquired by the
     *         current thread, or the lock was already held by the current
     *         thread; and {@code false} otherwise
     */
    public boolean tryLock() {
        // 这里直接调用的是 Sync 的 nonfairTryAcquire 方法；
        // 不会去考虑是否 fair 。即使设置了 fair=true，也会直接尝试获取锁；
        return sync.nonfairTryAcquire(1);
    }

    /**
     * Acquires the lock if it is not held by another thread within the given
     * waiting time and the current thread has not been
     * {@linkplain Thread#interrupt interrupted}.
     * <p>
     *     如果在给定的等待时间内锁没有被其他线程持有，并且当前线程没有被中断，则获取锁。
     *
     * <p>Acquires the lock if it is not held by another thread and returns
     * immediately with the value {@code true}, setting the lock hold count
     * to one. If this lock has been set to use a fair ordering policy then
     * an available lock <em>will not</em> be acquired if any other threads
     * are waiting for the lock. This is in contrast to the {@link #tryLock()}
     * method. If you want a timed {@code tryLock} that does permit barging on
     * a fair lock then combine the timed and un-timed forms together:
     *
     *  <pre> {@code
     * if (lock.tryLock() ||
     *     lock.tryLock(timeout, unit)) {
     *   ...
     * }}</pre>
     * <p>
     *     如果锁没有被其他线程持有，则立即获取锁并返回 true，将锁保持计数设置为 1。
     *     如果此锁已设置为使用公平排序策略，则如果其他线程正在等待锁，则不会获取可用的锁。
     *     这与 tryLock() 方法相反。
     *     如果要使用允许公平锁插队的定时 tryLock，则将定时和非定时形式结合在一起：
     *     if (lock.tryLock() || lock.tryLock(timeout, unit)) { ... }
     *
     * <p>If the current thread
     * already holds this lock then the hold count is incremented by one and
     * the method returns {@code true}.
     * <p>
     *     如果当前线程已经持有此锁，则保持计数将增加 1，并返回 true。
     *
     * <p>If the lock is held by another thread then the
     * current thread becomes disabled for thread scheduling
     * purposes and lies dormant until one of three things happens:
     *
     * <ul>
     *
     * <li>The lock is acquired by the current thread; or
     *
     * <li>Some other thread {@linkplain Thread#interrupt interrupts}
     * the current thread; or
     *
     * <li>The specified waiting time elapses
     *
     * </ul>
     * <p>
     *     如果锁被另一个线程持有，则当前线程将被禁用以进行线程调度，并处于休眠状态，直到发生以下三种情况之一：
     *     1. 当前线程获取了锁；
     *     2. 其他线程中断了当前线程；
     *     3. 指定的等待时间过去了。
     *
     * <p>If the lock is acquired then the value {@code true} is returned and
     * the lock hold count is set to one.
     * <p>
     *     如果获取了锁，则返回 true，并将锁保持计数设置为 1。
     *
     * <p>If the current thread:
     *
     * <ul>
     *
     * <li>has its interrupted status set on entry to this method; or
     *
     * <li>is {@linkplain Thread#interrupt interrupted} while
     * acquiring the lock,
     *
     * </ul>
     * then {@link InterruptedException} is thrown and the current thread's
     * interrupted status is cleared.
     * <p>
     *     如果当前线程：
     *     1. 在进入此方法时设置了中断状态；或者
     *     2. 在获取锁时被中断，
     *     则将抛出 InterruptedException，并清除当前线程的中断状态。
     *
     * <p>If the specified waiting time elapses then the value {@code false}
     * is returned.  If the time is less than or equal to zero, the method
     * will not wait at all.
     * <p>
     *     如果指定的等待时间过去了，则返回 false。
     *     如果时间小于或等于零，则该方法根本不会等待。
     *
     * <p>In this implementation, as this method is an explicit
     * interruption point, preference is given to responding to the
     * interrupt over normal or reentrant acquisition of the lock, and
     * over reporting the elapse of the waiting time.
     *
     * @param timeout the time to wait for the lock
     * @param unit the time unit of the timeout argument
     * @return {@code true} if the lock was free and was acquired by the
     *         current thread, or the lock was already held by the current
     *         thread; and {@code false} if the waiting time elapsed before
     *         the lock could be acquired
     * @throws InterruptedException if the current thread is interrupted
     * @throws NullPointerException if the time unit is null
     */
    public boolean tryLock(long timeout, TimeUnit unit)
            throws InterruptedException {
        // 直接调用了 aqs 的 tryAcquireNanos 方法；
        // 进而会调用到 fairSync/nonFairSync 中的 tryAcquire 方法；
        // 因此公平性能够保障
        return sync.tryAcquireNanos(1, unit.toNanos(timeout));
    }

    /**
     * Attempts to release this lock.
     * <p>
     *     尝试释放此锁。
     *
     * <p>If the current thread is the holder of this lock then the hold
     * count is decremented.  If the hold count is now zero then the lock
     * is released.  If the current thread is not the holder of this
     * lock then {@link IllegalMonitorStateException} is thrown.
     * <p>
     *     如果当前线程是此锁的持有者，则保持计数将递减。
     *     如果保持计数现在为零，则释放锁。
     *     如果当前线程不是此锁的持有者，则抛出 IllegalMonitorStateException。
     *
     * @throws IllegalMonitorStateException if the current thread does not
     *         hold this lock
     */
    public void unlock() {
        // 会先调用 sync 中的 tryRelease 的逻辑
        // 然后 unparkSuccessor
        sync.release(1);
    }

    /**
     * Returns a {@link Condition} instance for use with this
     * {@link Lock} instance.
     * <p>
     *     返回一个与此 Lock 实例一起使用的 Condition 实例。
     *
     * <p>The returned {@link Condition} instance supports the same
     * usages as do the {@link Object} monitor methods ({@link
     * Object#wait() wait}, {@link Object#notify notify}, and {@link
     * Object#notifyAll notifyAll}) when used with the built-in
     * monitor lock.
     *
     * <ul>
     *
     * <li>If this lock is not held when any of the {@link Condition}
     * {@linkplain Condition#await() waiting} or {@linkplain
     * Condition#signal signalling} methods are called, then an {@link
     * IllegalMonitorStateException} is thrown.
     *
     * <li>When the condition {@linkplain Condition#await() waiting}
     * methods are called the lock is released and, before they
     * return, the lock is reacquired and the lock hold count restored
     * to what it was when the method was called.
     *
     * <li>If a thread is {@linkplain Thread#interrupt interrupted}
     * while waiting then the wait will terminate, an {@link
     * InterruptedException} will be thrown, and the thread's
     * interrupted status will be cleared.
     *
     * <li> Waiting threads are signalled in FIFO order.
     *
     * <li>The ordering of lock reacquisition for threads returning
     * from waiting methods is the same as for threads initially
     * acquiring the lock, which is in the default case not specified,
     * but for <em>fair</em> locks favors those threads that have been
     * waiting the longest.
     *
     * </ul>
     * <p>
     *     当与内置 aqs 锁一起使用时，返回的 Condition 实例支持与 Object 监视器方法（wait、notify 和 notifyAll）相同的用法。
     *     1. 如果在调用任何 Condition await 或 signal 方法时没有持有此锁，则会抛出 IllegalMonitorStateException。
     *     2. 调用 Condition await 方法时，锁将被释放，并在返回之前重新获取锁，并将锁保持计数恢复为调用该方法时的值。
     *     3. 如果线程在等待时被 interrupt，则等待将终止，将抛出 InterruptedException，并清除线程的中断状态。
     *     4. 等待线程按 FIFO 顺序 signal。
     *     5. 从等待方法返回的线程重新获取锁的顺序与最初获取锁的线程的顺序相同，对于公平锁，它不会指定，但会偏向于等待时间最长的线程。
     *
     * @return the Condition object
     */
    public Condition newCondition() {
        return sync.newCondition();
    }

    /**
     * Queries the number of holds on this lock by the current thread.
     * <p>
     *     查询当前线程对此锁的保持数。
     *
     * <p>A thread has a hold on a lock for each lock action that is not
     * matched by an unlock action.
     * <p>
     *     对于每个未匹配的解锁操作，线程对锁的保持数都会增加。
     *
     * <p>The hold count information is typically only used for testing and
     * debugging purposes. For example, if a certain section of code should
     * not be entered with the lock already held then we can assert that
     * fact:
     *
     *  <pre> {@code
     * class X {
     *   ReentrantLock lock = new ReentrantLock();
     *   // ...
     *   public void m() {
     *     assert lock.getHoldCount() == 0;
     *     lock.lock();
     *     try {
     *       // ... method body
     *     } finally {
     *       lock.unlock();
     *     }
     *   }
     * }}</pre>
     *
     * @return the number of holds on this lock by the current thread,
     *         or zero if this lock is not held by the current thread
     */
    public int getHoldCount() {
        return sync.getHoldCount();
    }

    /**
     * Queries if this lock is held by the current thread.
     * <p>
     *     查询此锁是否由当前线程持有。
     *
     * <p>Analogous to the {@link Thread#holdsLock(Object)} method for
     * built-in monitor locks, this method is typically used for
     * debugging and testing. For example, a method that should only be
     * called while a lock is held can assert that this is the case:
     *
     *  <pre> {@code
     * class X {
     *   ReentrantLock lock = new ReentrantLock();
     *   // ...
     *
     *   public void m() {
     *       assert lock.isHeldByCurrentThread();
     *       // ... method body
     *   }
     * }}</pre>
     *
     * <p>It can also be used to ensure that a reentrant lock is used
     * in a non-reentrant manner, for example:
     *
     *  <pre> {@code
     * class X {
     *   ReentrantLock lock = new ReentrantLock();
     *   // ...
     *
     *   public void m() {
     *       assert !lock.isHeldByCurrentThread();
     *       lock.lock();
     *       try {
     *           // ... method body
     *       } finally {
     *           lock.unlock();
     *       }
     *   }
     * }}</pre>
     *
     * @return {@code true} if current thread holds this lock and
     *         {@code false} otherwise
     */
    public boolean isHeldByCurrentThread() {
        return sync.isHeldExclusively();
    }

    /**
     * Queries if this lock is held by any thread. This method is
     * designed for use in monitoring of the system state,
     * not for synchronization control.
     * <p>
     *     查询此锁是否由任何线程持有。
     *
     * @return {@code true} if any thread holds this lock and
     *         {@code false} otherwise
     */
    public boolean isLocked() {
        return sync.isLocked();
    }

    /**
     * Returns {@code true} if this lock has fairness set true.
     * <p>
     *     如果此锁是公平锁，则返回 true。
     *
     * @return {@code true} if this lock has fairness set true
     */
    public final boolean isFair() {
        return sync instanceof FairSync;
    }

    /**
     * Returns the thread that currently owns this lock, or
     * {@code null} if not owned. When this method is called by a
     * thread that is not the owner, the return value reflects a
     * best-effort approximation of current lock status. For example,
     * the owner may be momentarily {@code null} even if there are
     * threads trying to acquire the lock but have not yet done so.
     * This method is designed to facilitate construction of
     * subclasses that provide more extensive lock monitoring
     * facilities.
     * <p>
     *     返回当前拥有此锁的线程，如果没有拥有者，则返回 null。
     *     当此方法由不是所有者的线程调用时，返回值反映了当前锁状态的尽力近似。
     *     例如，即使有线程试图获取锁但尚未这样做，所有者可能暂时为 null。
     *     此方法旨在便于构建提供更广泛的锁监视功能的子类。
     *
     * @return the owner, or {@code null} if not owned
     */
    protected Thread getOwner() {
        return sync.getOwner();
    }

    /**
     * Queries whether any threads are waiting to acquire this lock. Note that
     * because cancellations may occur at any time, a {@code true}
     * return does not guarantee that any other thread will ever
     * acquire this lock.  This method is designed primarily for use in
     * monitoring of the system state.
     * <p>
     *     查询是否有任何线程正在等待获取此锁。
     *     请注意，由于随时可能发生取消，因此 true 返回并不保证任何其他线程将永远获取此锁。
     *     此方法主要用于监视系统状态。
     *
     * @return {@code true} if there may be other threads waiting to
     *         acquire the lock
     */
    public final boolean hasQueuedThreads() {
        return sync.hasQueuedThreads();
    }

    /**
     * Queries whether the given thread is waiting to acquire this
     * lock. Note that because cancellations may occur at any time, a
     * {@code true} return does not guarantee that this thread
     * will ever acquire this lock.  This method is designed primarily for use
     * in monitoring of the system state.
     * <p>
     *     查询给定线程是否正在等待获取此锁。
     *     请注意，由于随时可能发生取消，因此 true 返回并不保证此线程将永远获取此锁。
     *     此方法主要用于监视系统状态。
     *
     * @param thread the thread
     * @return {@code true} if the given thread is queued waiting for this lock
     * @throws NullPointerException if the thread is null
     */
    public final boolean hasQueuedThread(Thread thread) {
        return sync.isQueued(thread);
    }

    /**
     * Returns an estimate of the number of threads waiting to
     * acquire this lock.  The value is only an estimate because the number of
     * threads may change dynamically while this method traverses
     * internal data structures.  This method is designed for use in
     * monitoring of the system state, not for synchronization
     * control.
     *
     * @return the estimated number of threads waiting for this lock
     */
    public final int getQueueLength() {
        return sync.getQueueLength();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire this lock.  Because the actual set of threads may change
     * dynamically while constructing this result, the returned
     * collection is only a best-effort estimate.  The elements of the
     * returned collection are in no particular order.  This method is
     * designed to facilitate construction of subclasses that provide
     * more extensive monitoring facilities.
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedThreads() {
        return sync.getQueuedThreads();
    }

    /**
     * Queries whether any threads are waiting on the given condition
     * associated with this lock. Note that because timeouts and
     * interrupts may occur at any time, a {@code true} return does
     * not guarantee that a future {@code signal} will awaken any
     * threads.  This method is designed primarily for use in
     * monitoring of the system state.
     * <p>
     *     查询是否有任何线程正在等待与此锁关联的给定条件。
     *     请注意，由于随时可能发生超时和中断，因此 true 返回并不保证将来的 signal 将唤醒任何线程。
     *     此方法主要用于监视系统状态。
     *
     * @param condition the condition
     * @return {@code true} if there are any waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    public boolean hasWaiters(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.hasWaiters((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns an estimate of the number of threads waiting on the
     * given condition associated with this lock. Note that because
     * timeouts and interrupts may occur at any time, the estimate
     * serves only as an upper bound on the actual number of waiters.
     * This method is designed for use in monitoring of the system
     * state, not for synchronization control.
     * <p>
     *     返回与此锁关联的给定条件上等待的线程数的估计值。
     *     请注意，由于随时可能发生超时和中断，因此估计值仅用作实际等待者数量的上限。
     *     此方法主要用于监视系统状态，而不是用于同步控制。
     *
     * @param condition the condition
     * @return the estimated number of waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    public int getWaitQueueLength(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitQueueLength((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns a collection containing those threads that may be
     * waiting on the given condition associated with this lock.
     * Because the actual set of threads may change dynamically while
     * constructing this result, the returned collection is only a
     * best-effort estimate. The elements of the returned collection
     * are in no particular order.  This method is designed to
     * facilitate construction of subclasses that provide more
     * extensive condition monitoring facilities.
     *
     * @param condition the condition
     * @return the collection of threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    protected Collection<Thread> getWaitingThreads(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitingThreads((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns a string identifying this lock, as well as its lock state.
     * The state, in brackets, includes either the String {@code "Unlocked"}
     * or the String {@code "Locked by"} followed by the
     * {@linkplain Thread#getName name} of the owning thread.
     * <p>
     *     返回一个标识此锁及其锁状态的字符串。
     *     括号中的状态包括字符串 "Unlocked" 或字符串 "Locked by"，后跟拥有线程的名称。
     *
     * @return a string identifying this lock, as well as its lock state
     */
    public String toString() {
        Thread o = sync.getOwner();
        return super.toString() + ((o == null) ?
                                   "[Unlocked]" :
                                   "[Locked by thread " + o.getName() + "]");
    }
}
