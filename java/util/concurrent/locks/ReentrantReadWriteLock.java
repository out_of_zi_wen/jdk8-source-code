/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.locks;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * An implementation of {@link ReadWriteLock} supporting similar
 * semantics to {@link ReentrantLock}.
 * <p>This class has the following properties:
 *
 * <ul>
 * <li><b>Acquisition order</b>
 *
 * <p>This class does not impose a reader or writer preference
 * ordering for lock access.  However, it does support an optional
 * <em>fairness</em> policy.
 *
 * <dl>
 * <dt><b><i>Non-fair mode (default)</i></b>
 * <dd>When constructed as non-fair (the default), the order of entry
 * to the read and write lock is unspecified, subject to reentrancy
 * constraints.  A nonfair lock that is continuously contended may
 * indefinitely postpone one or more reader or writer threads, but
 * will normally have higher throughput than a fair lock.
 *
 * <dt><b><i>Fair mode</i></b>
 * <dd>When constructed as fair, threads contend for entry using an
 * approximately arrival-order policy. When the currently held lock
 * is released, either the longest-waiting single writer thread will
 * be assigned the write lock, or if there is a group of reader threads
 * waiting longer than all waiting writer threads, that group will be
 * assigned the read lock.
 *
 * <p>A thread that tries to acquire a fair read lock (non-reentrantly)
 * will block if either the write lock is held, or there is a waiting
 * writer thread. The thread will not acquire the read lock until
 * after the oldest currently waiting writer thread has acquired and
 * released the write lock. Of course, if a waiting writer abandons
 * its wait, leaving one or more reader threads as the longest waiters
 * in the queue with the write lock free, then those readers will be
 * assigned the read lock.
 *
 * <p>A thread that tries to acquire a fair write lock (non-reentrantly)
 * will block unless both the read lock and write lock are free (which
 * implies there are no waiting threads).  (Note that the non-blocking
 * {@link ReadLock#tryLock()} and {@link WriteLock#tryLock()} methods
 * do not honor this fair setting and will immediately acquire the lock
 * if it is possible, regardless of waiting threads.)
 * <p>
 * </dl>
 *
 * <li><b>Reentrancy</b>
 *
 * <p>This lock allows both readers and writers to reacquire read or
 * write locks in the style of a {@link ReentrantLock}. Non-reentrant
 * readers are not allowed until all write locks held by the writing
 * thread have been released.
 *
 * <p>Additionally, a writer can acquire the read lock, but not
 * vice-versa.  Among other applications, reentrancy can be useful
 * when write locks are held during calls or callbacks to methods that
 * perform reads under read locks.  If a reader tries to acquire the
 * write lock it will never succeed.
 *
 * <li><b>Lock downgrading</b>
 * <p>Reentrancy also allows downgrading from the write lock to a read lock,
 * by acquiring the write lock, then the read lock and then releasing the
 * write lock. However, upgrading from a read lock to the write lock is
 * <b>not</b> possible.
 *
 * <li><b>Interruption of lock acquisition</b>
 * <p>The read lock and write lock both support interruption during lock
 * acquisition.
 *
 * <li><b>{@link Condition} support</b>
 * <p>The write lock provides a {@link Condition} implementation that
 * behaves in the same way, with respect to the write lock, as the
 * {@link Condition} implementation provided by
 * {@link ReentrantLock#newCondition} does for {@link ReentrantLock}.
 * This {@link Condition} can, of course, only be used with the write lock.
 *
 * <p>The read lock does not support a {@link Condition} and
 * {@code readLock().newCondition()} throws
 * {@code UnsupportedOperationException}.
 *
 * <li><b>Instrumentation</b>
 * <p>This class supports methods to determine whether locks
 * are held or contended. These methods are designed for monitoring
 * system state, not for synchronization control.
 * </ul>
 *
 * <p>Serialization of this class behaves in the same way as built-in
 * locks: a deserialized lock is in the unlocked state, regardless of
 * its state when serialized.
 *
 * <p><b>Sample usages</b>. Here is a code sketch showing how to perform
 * lock downgrading after updating a cache (exception handling is
 * particularly tricky when handling multiple locks in a non-nested
 * fashion):
 *
 * <pre> {@code
 * class CachedData {
 *   Object data;
 *   volatile boolean cacheValid;
 *   final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
 *
 *   void processCachedData() {
 *     rwl.readLock().lock();
 *     if (!cacheValid) {
 *       // Must release read lock before acquiring write lock
 *       rwl.readLock().unlock();
 *       rwl.writeLock().lock();
 *       try {
 *         // Recheck state because another thread might have
 *         // acquired write lock and changed state before we did.
 *         if (!cacheValid) {
 *           data = ...
 *           cacheValid = true;
 *         }
 *         // Downgrade by acquiring read lock before releasing write lock
 *         rwl.readLock().lock();
 *       } finally {
 *         rwl.writeLock().unlock(); // Unlock write, still hold read
 *       }
 *     }
 *
 *     try {
 *       use(data);
 *     } finally {
 *       rwl.readLock().unlock();
 *     }
 *   }
 * }}</pre>
 *
 * ReentrantReadWriteLocks can be used to improve concurrency in some
 * uses of some kinds of Collections. This is typically worthwhile
 * only when the collections are expected to be large, accessed by
 * more reader threads than writer threads, and entail operations with
 * overhead that outweighs synchronization overhead. For example, here
 * is a class using a TreeMap that is expected to be large and
 * concurrently accessed.
 *
 *  <pre> {@code
 * class RWDictionary {
 *   private final Map<String, Data> m = new TreeMap<String, Data>();
 *   private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
 *   private final Lock r = rwl.readLock();
 *   private final Lock w = rwl.writeLock();
 *
 *   public Data get(String key) {
 *     r.lock();
 *     try { return m.get(key); }
 *     finally { r.unlock(); }
 *   }
 *   public String[] allKeys() {
 *     r.lock();
 *     try { return m.keySet().toArray(); }
 *     finally { r.unlock(); }
 *   }
 *   public Data put(String key, Data value) {
 *     w.lock();
 *     try { return m.put(key, value); }
 *     finally { w.unlock(); }
 *   }
 *   public void clear() {
 *     w.lock();
 *     try { m.clear(); }
 *     finally { w.unlock(); }
 *   }
 * }}</pre>
 *
 * <h3>Implementation Notes</h3>
 *
 * <p>This lock supports a maximum of 65535 recursive write locks
 * and 65535 read locks. Attempts to exceed these limits result in
 * {@link Error} throws from locking methods.
 *
 * @since 1.5
 * @author Doug Lea
 */
public class ReentrantReadWriteLock
        implements ReadWriteLock, java.io.Serializable {
    private static final long serialVersionUID = -6992448646407690164L;

    // TODO REMIND 所谓的「写锁降级到读锁」指：先获取写锁，然后获取读锁，然后释放写锁；这个是支持的；
    // 具体的应用场景：先加写锁更新，然后加读锁，然后释放写锁；之后当前线程就一直占有这读锁，因此一定能读到最新的数据；

    // TODO REMIND 所谓的「读锁升级到写锁」指：先获取读锁，然后获取写锁；这个是不支持的，可能导致死锁；


    /**
     * Performs all synchronization mechanics
     * <p>
     * 执行所有同步机制；
     */
    final Sync sync;
    /**
     * Inner class providing readlock
     * <p>
     * 提供读锁的内部类；
     */
    private final ReentrantReadWriteLock.ReadLock readerLock;
    /**
     * Inner class providing writelock
     * <p>
     * 提供写锁的内部类；
     */
    private final ReentrantReadWriteLock.WriteLock writerLock;

    /**
     * Creates a new {@code ReentrantReadWriteLock} with
     * default (nonfair) ordering properties.
     * <p>
     *     使用默认（非公平）排序属性创建一个新的 ReentrantReadWriteLock；
     */
    public ReentrantReadWriteLock() {
        this(false);
    }

    /**
     * Creates a new {@code ReentrantReadWriteLock} with
     * the given fairness policy.
     * <p>
     *     使用给定的公平策略创建一个新的 ReentrantReadWriteLock；
     *
     * @param fair {@code true} if this lock should use a fair ordering policy
     */
    public ReentrantReadWriteLock(boolean fair) {
        // 根据入参 fair 创建一个公平/非公平的 Sync 对象；
        sync = fair ? new FairSync() : new NonfairSync();
        // 初始化读锁和写锁，两个 lock 里面会都会记录下外部类的 sync；
        readerLock = new ReadLock(this);
        writerLock = new WriteLock(this);
    }

    // 获取读锁/写锁
    public ReentrantReadWriteLock.WriteLock writeLock() { return writerLock; }
    public ReentrantReadWriteLock.ReadLock  readLock()  { return readerLock; }

    /**
     * Synchronization implementation for ReentrantReadWriteLock.
     * Subclassed into fair and nonfair versions.
     * <p>
     *     ReentrantReadWriteLock 的同步实现；
     *     分为公平和非公平两个版本；
     */
    abstract static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 6317671515068378041L;

        /*
         * Read vs write count extraction constants and functions.
         * Lock state is logically divided into two unsigned shorts:
         * The lower one representing the exclusive (writer) lock hold count,
         * and the upper the shared (reader) hold count.
         * <p>
         *     读锁和写锁计数提取常量和函数；
         *     锁状态在逻辑上被分为两个无符号短整型：
         *     低位表示独占（写）锁持有计数，高位表示共享（读）锁持有计数；
         */

        static final int SHARED_SHIFT   = 16;
        static final int SHARED_UNIT    = (1 << SHARED_SHIFT);
        static final int MAX_COUNT      = (1 << SHARED_SHIFT) - 1;
        static final int EXCLUSIVE_MASK = (1 << SHARED_SHIFT) - 1;

        /**
         * The number of reentrant read locks held by current thread.
         * Initialized only in constructor and readObject.
         * Removed whenever a thread's read hold count drops to 0.
         * <p>
         *     当前线程持有的可重入读锁的数量；
         *     仅在构造函数和 readObject 方法中初始化；
         *     当线程的读持有计数降到 0 时移除；
         */
        private transient ThreadLocalHoldCounter readHolds;
        /**
         * The hold count of the last thread to successfully acquire
         * readLock. This saves ThreadLocal lookup in the common case
         * where the next thread to release is the last one to
         * acquire. This is non-volatile since it is just used
         * as a heuristic, and would be great for threads to cache.
         * <p>
         *     最后一个成功获取读锁的线程的持有计数；
         *     这样可以在常见情况下避免 ThreadLocal 查找，即下一个释放锁的线程是最后一个获取锁的线程；
         *     这个字段是非 volatile 的，因为它只是一个启发式的，线程可以缓存；
         *
         * <p>Can outlive the Thread for which it is caching the read
         * hold count, but avoids garbage retention by not retaining a
         * reference to the Thread.
         * <p>
         *     可以超过缓存读持有计数的线程的生命周期，但是不会保留对线程的引用，因此不会导致垃圾保留；
         *
         * <p>Accessed via a benign data race; relies on the memory
         * model's final field and out-of-thin-air guarantees.
         * <p>
         *     通过良性数据竞争访问；依赖于内存模型的 final 字段和虚拟机的内存屏障保证；
         */
        private transient HoldCounter cachedHoldCounter;
        /**
         * firstReader is the first thread to have acquired the read lock.
         * firstReaderHoldCount is firstReader's hold count.
         * <p>
         *     firstReader 是第一个获取读锁的线程；
         *     firstReaderHoldCount 是 firstReader 的持有计数；
         *
         * <p>More precisely, firstReader is the unique thread that last
         * changed the shared count from 0 to 1, and has not released the
         * read lock since then; null if there is no such thread.
         * <p>
         *     更准确地说，firstReader 是最后一个将共享计数从 0 改变为 1 的唯一线程，并且自那时以来没有释放读锁；
         *     如果没有这样的线程，则为 null；
         *
         * <p>Cannot cause garbage retention unless the thread terminated
         * without relinquishing its read locks, since tryReleaseShared
         * sets it to null.
         * <p>
         *     除非线程在没有释放读锁的情况下终止，否则不会导致垃圾保留，因为 tryReleaseShared 方法会将其设置为 null；
         *
         * <p>Accessed via a benign data race; relies on the memory
         * model's out-of-thin-air guarantees for references.
         *
         * <p>This allows tracking of read holds for uncontended read
         * locks to be very cheap.
         */
        private transient Thread firstReader = null;
        private transient int firstReaderHoldCount;

        Sync() {
            readHolds = new ThreadLocalHoldCounter();
            setState(getState()); // ensures visibility of readHolds
        }

        /**
         * Returns the number of shared holds represented in count
         * <p>
         * 返回入参 c 中表示的共享持有的计数值；
         */
        static int sharedCount(int c) {
            return c >>> SHARED_SHIFT;
        }

        /**
         * Returns the number of exclusive holds represented in count
         * <p>
         * 返回入参 c 中表示的独占持有的计数值；
         */
        static int exclusiveCount(int c) {
            return c & EXCLUSIVE_MASK;
        }

        /**
         * Returns true if the current thread, when trying to acquire
         * the read lock, and otherwise eligible to do so, should block
         * because of policy for overtaking other waiting threads.
         * <p>
         *     如果当前线程在尝试获取读锁时，应该因为超越其他等待线程的策略而阻塞，则返回 true；
         *     否则就表示有资格获取读锁；
         */
        abstract boolean readerShouldBlock();

        /**
         * Returns true if the current thread, when trying to acquire
         * the write lock, and otherwise eligible to do so, should block
         * because of policy for overtaking other waiting threads.
         * <p>
         *     如果当前线程在尝试获取写锁时，应该因为超越其他等待线程的策略而阻塞，则返回 true；
         *     否则就表示有资格获取写锁；
         */
        abstract boolean writerShouldBlock();

        /*
         * Acquires and releases use the same code for fair and
         * nonfair locks, but differ in whether/how they allow barging
         * when queues are non-empty.
         * <p>
         *     获取和释放使用相同的代码来实现公平和非公平锁，但是在队列非空时允许插队的方式上有所不同；
         */

        /**
         * 尝试释放写锁
         *
         * @param releases the release argument. This value is always the one
         *                 passed to a release method, or the current state value upon
         *                 entry to a condition wait.  The value is otherwise
         *                 uninterpreted and can represent anything you like.
         * @return
         */
        protected final boolean tryRelease(int releases) {
            // 如果当前线程不是独占锁的持有者，则抛出 IllegalMonitorStateException 异常；
            if (!isHeldExclusively())
                throw new IllegalMonitorStateException();
            // 计算更新后的状态值
            int nextc = getState() - releases;
            // 如果更新后的状态值对应的独占锁 count==0，那么就释放独占锁
            boolean free = exclusiveCount(nextc) == 0;
            if (free)
                // 取消独占锁的持有者
                setExclusiveOwnerThread(null);
            // 更新状态值
            setState(nextc);
            return free;
        }

        /**
         * 尝试获取写锁；
         *
         * @param acquires the acquire argument. This value is always the one
         *                 passed to an acquire method, or is the value saved on entry
         *                 to a condition wait.  The value is otherwise uninterpreted
         *                 and can represent anything you like.
         * @return
         */
        protected final boolean tryAcquire(int acquires) {
            /*
             * Walkthrough:
             * 1. If read count nonzero or write count nonzero
             *    and owner is a different thread, fail.
             * 2. If count would saturate, fail. (This can only
             *    happen if count is already nonzero.)
             * 3. Otherwise, this thread is eligible for lock if
             *    it is either a reentrant acquire or
             *    queue policy allows it. If so, update state
             *    and set owner.
             * <p>
             *     步骤：
             *     1. 如果读计数非零或写计数非零，并且所有者是另一个线程，则失败；
             *     2. 如果计数会饱和，则失败。（这只能发生在计数已经非零的情况下。）
             *     3. 否则，如果线程有资格获取锁，那么它要么是可重入获取，要么是队列策略允许的；
             */
            // 确定当前线程
            Thread current = Thread.currentThread();
            // 获取当前状态值
            int c = getState();
            // 计算出独占锁的 count
            int w = exclusiveCount(c);

            // 如果 c != 0，说明有线程持有锁（读锁写锁都有可能）
            if (c != 0) {
                // (Note: if c != 0 and w == 0 then shared count != 0)
                // w == 0，说明有线程持有读锁
                // 或者当前线程不是独占锁的持有者
                // 则认为 tryAcquire 失败
                if (w == 0 || current != getExclusiveOwnerThread())
                    return false;

                // 到这里，说明当前线程是独占锁的持有者；那么需要判断重入次数是否超过了最大值
                if (w + exclusiveCount(acquires) > MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                // Reentrant acquire
                // 更新值并返回，表示 acquire 成功
                setState(c + acquires);
                return true;
            }

            // 说明 c == 0，即没有线程持有锁
            // 那么判断加写锁时，是否需要阻塞；否则就 cas 更新状态值
            if (writerShouldBlock() ||
                !compareAndSetState(c, c + acquires))
                // 如果需要阻塞，或者 cas 更新状态值失败，那么就返回 false，表示 acquire 失败
                return false;
            // 否则，说明 cas 成功，设置当前线程为独占锁的持有者
            setExclusiveOwnerThread(current);
            return true;
        }

        /*
         * Note that tryRelease and tryAcquire can be called by
         * Conditions. So it is possible that their arguments contain
         * both read and write holds that are all released during a
         * condition wait and re-established in tryAcquire.
         * <p>
         *     注意，tryRelease 和 tryAcquire 可能会被 Conditions 调用；
         *    因此，它们的参数可能包含在条件等待期间全部释放的读和写持有，然后在 tryAcquire 中重新建立；
         */

        /**
         * 尝试释放读锁
         *
         * @param unused the release argument. This value is always the one
         *               passed to a release method, or the current state value upon
         *               entry to a condition wait.  The value is otherwise
         *               uninterpreted and can represent anything you like.
         * @return
         */
        protected final boolean tryReleaseShared(int unused) {
            // 获取当前线程
            Thread current = Thread.currentThread();

            // 如果 firstReader == current，说明当前线程是第一个获取读锁的线程
            if (firstReader == current) {
                // assert firstReaderHoldCount > 0;
                // 如果当前线程的重入次数为 1，那么就将 firstReaderHoldCount 置为 0，firstReader 置为 null
                if (firstReaderHoldCount == 1) {
                    firstReader = null;
                } else {

                    // 否则，将 firstReaderHoldCount 减 1
                    firstReaderHoldCount--;
                }

                // 否则，说明当前线程不是第一个获取读锁的线程
            } else {
                // 取到最近一个获取读锁的线程的重入次数
                HoldCounter rh = cachedHoldCounter;
                // 如果 rh 不是自己，那么就从 ThreadLocal 中取出自己的 HoldCounter
                if (rh == null || rh.tid != getThreadId(current))
                    rh = readHolds.get();
                // 取出自己的重入次数
                int count = rh.count;
                // 如果重入次数为 1，那么就将 rh 置为 null
                if (count <= 1) {
                    readHolds.remove();
                    if (count <= 0)
                        throw unmatchedUnlockException();
                }
                // 将自己的将重入次数减 1
                --rh.count;
            }
            for (;;) {
                // 获取 aqs 的状态值
                int c = getState();
                // 计算出新的状态值；这里可以直接减 SHARED_UNIT，因为只有读锁
                int nextc = c - SHARED_UNIT;
                // cas 更新状态值
                if (compareAndSetState(c, nextc))
                    // Releasing the read lock has no effect on readers,
                    // but it may allow waiting writers to proceed if
                    // both read and write locks are now free.

                    // 如果计算之后的结果值为 0，说明读锁都被释放了，那么就返回 true
                    return nextc == 0;
            }
        }

        /**
         * 尝试获取读锁
         *
         * @param unused the acquire argument. This value is always the one
         *               passed to an acquire method, or is the value saved on entry
         *               to a condition wait.  The value is otherwise uninterpreted
         *               and can represent anything you like.
         * @return
         */
        protected final int tryAcquireShared(int unused) {
            /*
             * Walkthrough:
             * 1. If write lock held by another thread, fail.
             * 2. Otherwise, this thread is eligible for
             *    lock wrt state, so ask if it should block
             *    because of queue policy. If not, try
             *    to grant by CASing state and updating count.
             *    Note that step does not check for reentrant
             *    acquires, which is postponed to full version
             *    to avoid having to check hold count in
             *    the more typical non-reentrant case.
             * 3. If step 2 fails either because thread
             *    apparently not eligible or CAS fails or count
             *    saturated, chain to version with full retry loop.
             */
            // 获取当前线程
            Thread current = Thread.currentThread();
            // 获取当前 aqs 状态值
            int c = getState();
            // 判断是否已经被独占锁持有
            if (exclusiveCount(c) != 0 &&
                getExclusiveOwnerThread() != current)
                return -1;
            // 计算出读锁的 count
            int r = sharedCount(c);

            // TODO 如果存在写锁等待，那么读锁就会 block！因此 ReentrantReadWriteLock 可以理解是写锁优先的！
            if (!readerShouldBlock() && // 判断加读锁是否需要阻塞
                r < MAX_COUNT && // 判断读锁的 count 是否已经饱和
                compareAndSetState(c, c + SHARED_UNIT)) { // cas 更新状态值

                // 此时意味着加读锁成功
                // 判断是否是第一个获取读锁的线程；如果是，那么就更新 firstReader 和 firstReaderHoldCount
                if (r == 0) {
                    firstReader = current;
                    firstReaderHoldCount = 1;

                    // 否则，判断是否是重入获取读锁
                } else if (firstReader == current) {
                    firstReaderHoldCount++;
                } else {
                    // 如果不是，那么就更新 cachedHoldCounter
                    HoldCounter rh = cachedHoldCounter;
                    if (rh == null || rh.tid != getThreadId(current))
                        cachedHoldCounter = rh = readHolds.get();
                    else if (rh.count == 0)
                        readHolds.set(rh);
                    // 将重入次数加 1
                    rh.count++;
                }
                return 1;
            }

            // 否则，说明加读锁失败，那么就调用 fullTryAcquireShared 方法
            return fullTryAcquireShared(current);
        }

        /**
         * A counter for per-thread read hold counts.
         * Maintained as a ThreadLocal; cached in cachedHoldCounter
         * <p>
         * 每个线程的读持有计数器；
         * 作为一个 ThreadLocal 维护；缓存在 cachedHoldCounter 中；
         */
        static final class HoldCounter {
            // Use id, not reference, to avoid garbage retention
            final long tid = getThreadId(java.lang.Thread.currentThread());
            int count = 0;
        }

        private IllegalMonitorStateException unmatchedUnlockException() {
            return new IllegalMonitorStateException(
                "attempt to unlock read lock, not locked by current thread");
        }

        /**
         * ThreadLocal subclass. Easiest to explicitly define for sake
         * of deserialization mechanics.
         * <p>
         * ThreadLocal 的子类；
         * 为了便于序列化机制的定义，最好明确定义；
         */
        static final class ThreadLocalHoldCounter
                extends ThreadLocal<HoldCounter> {
            // 重写 initialValue 方法，返回一个新的 HoldCounter 对象；
            public HoldCounter initialValue() {
                return new HoldCounter();
            }
        }

        /**
         * Full version of acquire for reads, that handles CAS misses
         * and reentrant reads not dealt with in tryAcquireShared.
         * <p>
         *     读锁的完整版本，处理了 CAS 失败和 tryAcquireShared 中未处理的可重入读；
         */
        final int fullTryAcquireShared(Thread current) {
            /*
             * This code is in part redundant with that in
             * tryAcquireShared but is simpler overall by not
             * complicating tryAcquireShared with interactions between
             * retries and lazily reading hold counts.
             */
            HoldCounter rh = null;
            for (;;) {
                int c = getState();
                // 判断是否已经被独占锁持有
                if (exclusiveCount(c) != 0) {
                    // 判断占有写锁的是不是自己
                    if (getExclusiveOwnerThread() != current)
                        return -1;
                    // else we hold the exclusive lock; blocking here
                    // would cause deadlock.

                    // 说明目前无写锁
                    // 判断是否需要阻塞，一般阻塞的原因是公平考虑、期间有写锁获取到了锁等；
                } else if (readerShouldBlock()) {
                    // Make sure we're not acquiring read lock reentrantly
                    // 判断是否是重入获取读锁
                    if (firstReader == current) {
                        // assert firstReaderHoldCount > 0;
                    } else {
                        // 阻塞完了，判断是否是重入获取读锁；
                        // 如果不是，那么 return -1?
                        if (rh == null) {
                            // 获取当前 Thread 对应的 HoldCounter
                            rh = cachedHoldCounter;
                            if (rh == null || rh.tid != getThreadId(current)) {
                                rh = readHolds.get();
                                if (rh.count == 0)
                                    readHolds.remove();
                            }
                        }
                        if (rh.count == 0)
                            return -1;
                    }
                }
                // 判断读锁的 count 是否已经饱和
                if (sharedCount(c) == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                // cas 更新状态值
                if (compareAndSetState(c, c + SHARED_UNIT)) {
                    // 更新读锁相关的字段
                    if (sharedCount(c) == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                    } else {
                        if (rh == null)
                            rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        rh.count++;
                        cachedHoldCounter = rh; // cache for release
                    }
                    return 1;
                }
            }
        }

        /**
         * Performs tryLock for write, enabling barging in both modes.
         * This is identical in effect to tryAcquire except for lack
         * of calls to writerShouldBlock.
         * <p>
         *     尝试获取写锁，允许插队；
         *     除了 writerShouldBlock 方法的调用外，效果与 tryAcquire 相同；
         */
        final boolean tryWriteLock() {
            // 获取当前线程
            Thread current = Thread.currentThread();
            // 获取 aqs 同步器的状态
            int c = getState();
            // 如果 c != 0，说明已经有线程持有锁（读锁或写锁）
            if (c != 0) {
                // 统计出独占锁的 count
                int w = exclusiveCount(c);
                // 如果 w == 0，说明有线程持有读锁；
                // 或者如果当前线程不是独占锁的持有者(不是锁重入)，那么就返回 false
                if (w == 0 || current != getExclusiveOwnerThread())
                    return false;
                // 如果重入次数超过了最大值，那么就抛出异常
                if (w == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
            }
            // 否则，说明 c == 0，即没有线程持有锁；
            // 那么就 cas 加锁
            if (!compareAndSetState(c, c + 1))
                return false;
            // 记录当前线程为独占锁的持有者
            setExclusiveOwnerThread(current);
            return true;
        }

        /**
         * Performs tryLock for read, enabling barging in both modes.
         * This is identical in effect to tryAcquireShared except for
         * lack of calls to readerShouldBlock.
         * <p>
         *     尝试获取读锁，允许插队；
         *     除了 readerShouldBlock 方法的调用外，效果与 tryAcquireShared 相同；
         */
        final boolean tryReadLock() {
            // 获取当前线程
            Thread current = Thread.currentThread();
            // 会一直尝试循环，直到成功加读锁 或 发现其他线程持有写锁
            for (;;) {
                // 获取 aqs 同步器的状态
                int c = getState();
                // 如果已经有线程持有锁（读锁或写锁）并且当前线程不是独占锁的持有者，那么就返回 false
                // 主要处理先占写锁，后占读锁的重入场景
                if (exclusiveCount(c) != 0 &&
                    getExclusiveOwnerThread() != current)
                    return false;
                // 计算出读锁的 count
                int r = sharedCount(c);
                // 如果读锁的 count 已经饱和，那么就返回 false
                if (r == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                // cas 更新状态值
                if (compareAndSetState(c, c + SHARED_UNIT)) {
                    // 如果原先没有线程持有锁，那么就记录当前线程为第一个获取读锁的线程
                    if (r == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;

                        // 否则判断是否是第一个获取读锁的线程重入
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                    } else {
                        // 否则，后面获取读所的线程，需要更新各自 ThreadLocal 中的 HoldCounter
                        HoldCounter rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            cachedHoldCounter = rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        rh.count++;
                    }
                    return true;
                }
            }
        }

        protected final boolean isHeldExclusively() {
            // While we must in general read state before owner,
            // we don't need to do so to check if current thread is owner
            return getExclusiveOwnerThread() == Thread.currentThread();
        }

        // Methods relayed to outer class

        final ConditionObject newCondition() {
            return new ConditionObject();
        }

        final Thread getOwner() {
            // Must read state before owner to ensure memory consistency
            return ((exclusiveCount(getState()) == 0) ?
                    null :
                    getExclusiveOwnerThread());
        }

        final int getReadLockCount() {
            return sharedCount(getState());
        }

        final boolean isWriteLocked() {
            return exclusiveCount(getState()) != 0;
        }

        final int getWriteHoldCount() {
            // 如果当前线程是独占锁的持有者，那么就返回独占锁的 count；否则，返回 0
            return isHeldExclusively() ? exclusiveCount(getState()) : 0;
        }

        final int getReadHoldCount() {
            // 获取读锁的加锁次数（即加了读锁的线程数）
            if (getReadLockCount() == 0)
                return 0;

            // 获取当前线程
            Thread current = Thread.currentThread();
            // 如果当前线程是第一个获取读锁的线程，那么就返回 firstReaderHoldCount
            if (firstReader == current)
                return firstReaderHoldCount;

            // 否则，获取最近获取读锁的线程的 HoldCounter
            HoldCounter rh = cachedHoldCounter;
            if (rh != null && rh.tid == getThreadId(current))
                return rh.count;

            // 从 ThreadLocal 中获取当前线程的 HoldCounter
            int count = readHolds.get().count;
            if (count == 0) readHolds.remove();
            return count;
        }

        /**
         * Reconstitutes the instance from a stream (that is, deserializes it).
         */
        private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
            s.defaultReadObject();
            readHolds = new ThreadLocalHoldCounter();
            setState(0); // reset to unlocked state
        }

        final int getCount() { return getState(); }
    }

    /**
     * Nonfair version of Sync
     * <p>
     *     非公平版本的 Sync；
     */
    static final class NonfairSync extends Sync {
        private static final long serialVersionUID = -8159625535654395037L;

        final boolean writerShouldBlock() {
            // 写锁用不阻塞，永远允许插队
            return false; // writers can always barge
        }
        final boolean readerShouldBlock() {
            /* As a heuristic to avoid indefinite writer starvation,
             * block if the thread that momentarily appears to be head
             * of queue, if one exists, is a waiting writer.  This is
             * only a probabilistic effect since a new reader will not
             * block if there is a waiting writer behind other enabled
             * readers that have not yet drained from the queue.
             * <p>
             *     为了避免写线程无限期饥饿，如果队列中有一个正在等待的写线程，那么就阻塞；
             *    这只是一个概率效果，因为如果队列中有一个等待的写线程，新的读线程不会阻塞，除非队列中有其他未从队列中排空的已启用的读线程；
             *
             */
            // 如果目前存在了一个正在长时间等待的写线程(head.next)，那么就阻塞
            return apparentlyFirstQueuedIsExclusive();
        }
    }

    /**
     * Fair version of Sync
     */
    static final class FairSync extends Sync {
        private static final long serialVersionUID = -2274990926593161451L;
        final boolean writerShouldBlock() {
            // 只要队列中有线程，就阻塞
            return hasQueuedPredecessors();
        }
        final boolean readerShouldBlock() {
            // 只要队列中有线程，就阻塞
            return hasQueuedPredecessors();
        }
    }

    /**
     * The lock returned by method {@link ReentrantReadWriteLock#readLock}.
     */
    public static class ReadLock implements Lock, java.io.Serializable {
        private static final long serialVersionUID = -5992448646407690164L;
        private final Sync sync;

        /**
         * Constructor for use by subclasses
         * <p>
         *     供子类使用的构造函数；
         *
         * @param lock the outer lock object
         * @throws NullPointerException if the lock is null
         */
        protected ReadLock(ReentrantReadWriteLock lock) {
            sync = lock.sync;
        }

        /**
         * Acquires the read lock.
         * <p>
         *     获取读锁；
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately.
         * <p>
         *     如果写锁没有被其他线程持有，那么就立即获取读锁；
         *
         * <p>If the write lock is held by another thread then
         * the current thread becomes disabled for thread scheduling
         * purposes and lies dormant until the read lock has been acquired.
         * <p>
         *     如果写锁被其他线程持有，那么当前线程会因为线程调度目的而被禁用，并且会一直休眠，直到获取到读锁；
         */
        public void lock() {
            // 调用 aqs 的 acquireShared 方法
            // 由于 reentrantReadWriteLock 中的 Sync 抽象类重写了 tryLockShared
            // 所以，在 aqs 的 acquireShared 中会调用 Sync 的 tryAcquireShared 方法
            sync.acquireShared(1);
        }

        /**
         * Acquires the read lock unless the current thread is
         * {@linkplain Thread#interrupt interrupted}.
         * <p>
         *     获取读锁，除非当前线程被中断；
         *
         * <p>Acquires the read lock if the write lock is not held
         * by another thread and returns immediately.
         * <p>
         *     如果写锁没有被其他线程持有，那么就立即获取读锁；
         *
         * <p>If the write lock is held by another thread then the
         * current thread becomes disabled for thread scheduling
         * purposes and lies dormant until one of two things happens:
         *
         * <ul>
         *
         * <li>The read lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread.
         *
         * </ul>
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method; or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the read lock,
         *
         * </ul>
         *
         * then {@link InterruptedException} is thrown and the current
         * thread's interrupted status is cleared.
         * <p>
         *      如果写锁被其他线程持有，那么当前线程会因为线程调度目的而被禁用，并且会一直休眠，直到下面两种情况发生：
         *      1. 当前线程获取到读锁；
         *      2. 其他线程中断了当前线程；
         *
         *      如果当前线程：
         *      1. 在进入此方法时设置了中断状态；或者
         *      2. 在获取读锁时被中断；
         *      那么就会抛出 InterruptedException 异常，并且清除当前线程的中断状态；
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock.
         *
         * @throws InterruptedException if the current thread is interrupted
         */
        public void lockInterruptibly() throws InterruptedException {
            // 调用 aqs 的 acquireSharedInterruptibly 方法
            // 由于 reentrantReadWriteLock 中的 Sync 抽象类重写了 tryLockShared
            // 所以，在 aqs 的 acquireSharedInterruptibly 中会调用 Sync 的 tryAcquireShared 方法
            sync.acquireSharedInterruptibly(1);
        }

        /**
         * Acquires the read lock only if the write lock is not held by
         * another thread at the time of invocation.
         * <p>
         *     只有在调用时写锁没有被其他线程持有的情况下，才获取读锁；
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately with the value
         * {@code true}. Even when this lock has been set to use a
         * fair ordering policy, a call to {@code tryLock()}
         * <em>will</em> immediately acquire the read lock if it is
         * available, whether or not other threads are currently
         * waiting for the read lock.  This &quot;barging&quot; behavior
         * can be useful in certain circumstances, even though it
         * breaks fairness. If you want to honor the fairness setting
         * for this lock, then use {@link #tryLock(long, TimeUnit)
         * tryLock(0, TimeUnit.SECONDS) } which is almost equivalent
         * (it also detects interruption).
         * <p>
         *     如果写锁没有被其他线程持有，那么就立即获取读锁，并返回 true；
         *     即使此锁已设置为使用公平排序策略，调用 tryLock() 也会立即获取读锁（如果可用），无论其他线程当前是否正在等待读锁；
         *     这种“插队”行为在某些情况下可能很有用，尽管它会破坏公平性；
         *     如果要遵守此锁的公平性设置，则使用 tryLock(long, TimeUnit) 方法，该方法几乎等效（它还会检测中断）；
         *
         * <p>If the write lock is held by another thread then
         * this method will return immediately with the value
         * {@code false}.
         * <p>
         *     如果写锁被其他线程持有，那么就立即返回 false；
         *
         * @return {@code true} if the read lock was acquired
         */
        public boolean tryLock() {
            return sync.tryReadLock();
        }

        /**
         * Acquires the read lock if the write lock is not held by
         * another thread within the given waiting time and the
         * current thread has not been {@linkplain Thread#interrupt
         * interrupted}.
         * <p>
         *     如果写锁没有被其他线程持有，并且当前线程没有被中断，那么在给定的等待时间内获取读锁；
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately with the value
         * {@code true}. If this lock has been set to use a fair
         * ordering policy then an available lock <em>will not</em> be
         * acquired if any other threads are waiting for the
         * lock. This is in contrast to the {@link #tryLock()}
         * method. If you want a timed {@code tryLock} that does
         * permit barging on a fair lock then combine the timed and
         * un-timed forms together:
         *
         *  <pre> {@code
         * if (lock.tryLock() ||
         *     lock.tryLock(timeout, unit)) {
         *   ...
         * }}</pre>
         * <p>
         *     如果写锁没有被其他线程持有，并且当前线程没有被中断，那么在给定的等待时间内获取读锁，并返回 true；
         *     如果此锁已设置为使用公平排序策略，则如果其他线程正在等待锁，则不会获取可用锁；
         *     这与 tryLock() 方法相反；
         *     如果要在公平锁上允许插队的定时 tryLock，则将定时和非定时形式结合在一起；
         *     即：if (lock.tryLock() || lock.tryLock(timeout, unit)) {...}
         *
         * <p>If the write lock is held by another thread then the
         * current thread becomes disabled for thread scheduling
         * purposes and lies dormant until one of three things happens:
         *
         * <ul>
         *
         * <li>The read lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread; or
         *
         * <li>The specified waiting time elapses.
         *
         * </ul>
         * <p>
         *     如果写锁被其他线程持有，那么当前线程会因为线程调度目的而被禁用，并且会一直休眠，直到下面三种情况发生：
         *     1. 当前线程获取到读锁；
         *     2. 其他线程中断了当前线程；
         *     3. 指定的等待时间过去了；
         *
         * <p>If the read lock is acquired then the value {@code true} is
         * returned.
         * <p>
         *     如果获取到了读锁，那么就返回 true；
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method; or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the read lock,
         *
         * </ul> then {@link InterruptedException} is thrown and the
         * current thread's interrupted status is cleared.
         * <p>
         *     如果当前线程：
         *     1. 在进入此方法时设置了中断状态；或者
         *     2. 在获取读锁时被中断；
         *     那么就会抛出 InterruptedException 异常，并且清除当前线程的中断状态；
         *
         * <p>If the specified waiting time elapses then the value
         * {@code false} is returned.  If the time is less than or
         * equal to zero, the method will not wait at all.
         * <p>
         *     如果指定的等待时间过去了，那么就返回 false；
         *     如果时间小于等于 0，那么就不会等待；
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock, and over reporting the elapse of the waiting time.
         * <p>
         *     在此实现中，由于此方法是一个显式的中断点，因此优先响应中断，而不是正常或可重入地获取锁，并且优先报告等待时间的过去；
         *
         * @param timeout the time to wait for the read lock
         * @param unit the time unit of the timeout argument
         * @return {@code true} if the read lock was acquired
         * @throws InterruptedException if the current thread is interrupted
         * @throws NullPointerException if the time unit is null
         */
        public boolean tryLock(long timeout, TimeUnit unit)
                throws InterruptedException {
            return sync.tryAcquireSharedNanos(1, unit.toNanos(timeout));
        }

        /**
         * Attempts to release this lock.
         * <p>
         *     尝试释放读锁；
         *
         * <p>If the number of readers is now zero then the lock
         * is made available for write lock attempts.
         */
        public void unlock() {
            sync.releaseShared(1);
        }

        /**
         * Throws {@code UnsupportedOperationException} because
         * {@code ReadLocks} do not support conditions.
         * <p>
         *     抛出 UnsupportedOperationException 异常，因为 ReadLocks 不支持 condition；
         *
         * @throws UnsupportedOperationException always
         */
        public Condition newCondition() {
            throw new UnsupportedOperationException();
        }

        /**
         * Returns a string identifying this lock, as well as its lock state.
         * The state, in brackets, includes the String {@code "Read locks ="}
         * followed by the number of held read locks.
         *
         * @return a string identifying this lock, as well as its lock state
         */
        public String toString() {
            int r = sync.getReadLockCount();
            return super.toString() +
                "[Read locks = " + r + "]";
        }
    }

    /**
     * The lock returned by method {@link ReentrantReadWriteLock#writeLock}.
     * <p>
     *     由 ReentrantReadWriteLock#writeLock 方法返回的锁；
     */
    public static class WriteLock implements Lock, java.io.Serializable {
        private static final long serialVersionUID = -4992448646407690164L;
        private final Sync sync;

        /**
         * Constructor for use by subclasses
         * <p>
         *     供子类使用的构造函数；
         *
         * @param lock the outer lock object
         * @throws NullPointerException if the lock is null
         */
        protected WriteLock(ReentrantReadWriteLock lock) {
            sync = lock.sync;
        }

        /**
         * Acquires the write lock.
         * <p>
         *     获取写锁；
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately, setting the write lock hold count to
         * one.
         * <p>
         *     如果读锁和写锁都没有被其他线程持有，那么就立即获取写锁，并将写锁的 count 设置为 1；
         *
         * <p>If the current thread already holds the write lock then the
         * hold count is incremented by one and the method returns
         * immediately.
         * <p>
         *     如果当前线程已经持有写锁，那么就将写锁的 count 加 1，并立即返回；
         *
         * <p>If the lock is held by another thread then the current
         * thread becomes disabled for thread scheduling purposes and
         * lies dormant until the write lock has been acquired, at which
         * time the write lock hold count is set to one.
         * <p>
         *     如果写锁被其他线程持有，那么当前线程会因为线程调度目的而被禁用，并且会一直休眠，直到获取到写锁；
         */
        public void lock() {
            // 调用 aqs 的 acquire 方法
            // 由于 reentrantReadWriteLock 中的 Sync 抽象类重写了 tryAcquire；所以，在 aqs 的 acquire 中会调用 Sync 的 tryAcquire 方法
            // 获取到锁前屏蔽中断
            sync.acquire(1);
        }

        /**
         * Acquires the write lock unless the current thread is
         * {@linkplain Thread#interrupt interrupted}.
         * <p>
         *     获取写锁，除非当前线程被中断；
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately, setting the write lock hold count to
         * one.
         * <p>
         *     如果读锁和写锁都没有被其他线程持有，那么就立即获取写锁，并将写锁的 count 设置为 1；
         *
         * <p>If the current thread already holds this lock then the
         * hold count is incremented by one and the method returns
         * immediately.
         * <p>
         *     如果当前线程已经持有写锁，那么就将写锁的 count 加 1，并立即返回；
         *
         * <p>If the lock is held by another thread then the current
         * thread becomes disabled for thread scheduling purposes and
         * lies dormant until one of two things happens:
         *
         * <ul>
         *
         * <li>The write lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread.
         *
         * </ul>
         *
         * <p>If the write lock is acquired by the current thread then the
         * lock hold count is set to one.
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method;
         * or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the write lock,
         *
         * </ul>
         *
         * then {@link InterruptedException} is thrown and the current
         * thread's interrupted status is cleared.
         * <p>
         *     如果写锁被其他线程持有，那么当前线程会因为线程调度目的而被禁用，并且会一直休眠，直到下面两种情况发生：
         *     1. 当前线程获取到写锁；
         *     2. 其他线程中断了当前线程；
         *     如果当前线程：
         *     1. 在进入此方法时设置了中断状态；或者
         *     2. 在获取写锁时被中断；
         *     那么就会抛出 InterruptedException 异常，并且清除当前线程的中断状态；
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock.
         *
         * @throws InterruptedException if the current thread is interrupted
         */
        public void lockInterruptibly() throws InterruptedException {
            // 调用 aqs 的 acquireInterruptibly 方法
            // 由于 reentrantReadWriteLock 中的 Sync 抽象类重写了 tryAcquire；所以，在 aqs 的 acquireInterruptibly 中会调用 Sync 的 tryAcquire 方法
            sync.acquireInterruptibly(1);
        }

        /**
         * Acquires the write lock only if it is not held by another thread
         * at the time of invocation.
         * <p>
         *     只有在调用时写锁没有被其他线程持有的情况下，才获取写锁；
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately with the value {@code true},
         * setting the write lock hold count to one. Even when this lock has
         * been set to use a fair ordering policy, a call to
         * {@code tryLock()} <em>will</em> immediately acquire the
         * lock if it is available, whether or not other threads are
         * currently waiting for the write lock.  This &quot;barging&quot;
         * behavior can be useful in certain circumstances, even
         * though it breaks fairness. If you want to honor the
         * fairness setting for this lock, then use {@link
         * #tryLock(long, TimeUnit) tryLock(0, TimeUnit.SECONDS) }
         * which is almost equivalent (it also detects interruption).
         * <p>
         *     如果读锁和写锁都没有被其他线程持有，那么就立即获取写锁，并返回 true；
         *     即使此锁已设置为使用公平排序策略，调用 tryLock() 也会立即获取写锁（如果可用），无论其他线程当前是否正在等待写锁；
         *     这种“插队”行为在某些情况下可能很有用，尽管它会破坏公平性；
         *     如果要遵守此锁的公平性设置，则使用 tryLock(long, TimeUnit) 方法，该方法几乎等效（它还会检测中断）；
         *
         * <p>If the current thread already holds this lock then the
         * hold count is incremented by one and the method returns
         * {@code true}.
         * <p>
         *     如果当前线程已经持有写锁，那么就将写锁的 count 加 1，并返回 true；
         *
         * <p>If the lock is held by another thread then this method
         * will return immediately with the value {@code false}.
         * <p>
         *     如果写锁被其他线程持有，那么就立即返回 false；
         *
         * @return {@code true} if the lock was free and was acquired
         * by the current thread, or the write lock was already held
         * by the current thread; and {@code false} otherwise.
         */
        public boolean tryLock( ) {
            return sync.tryWriteLock();
        }

        /**
         * Acquires the write lock if it is not held by another thread
         * within the given waiting time and the current thread has
         * not been {@linkplain Thread#interrupt interrupted}.
         *
         * <p>Acquires the write lock if neither the read nor write lock
         * are held by another thread
         * and returns immediately with the value {@code true},
         * setting the write lock hold count to one. If this lock has been
         * set to use a fair ordering policy then an available lock
         * <em>will not</em> be acquired if any other threads are
         * waiting for the write lock. This is in contrast to the {@link
         * #tryLock()} method. If you want a timed {@code tryLock}
         * that does permit barging on a fair lock then combine the
         * timed and un-timed forms together:
         *
         *  <pre> {@code
         * if (lock.tryLock() ||
         *     lock.tryLock(timeout, unit)) {
         *   ...
         * }}</pre>
         *
         * <p>If the current thread already holds this lock then the
         * hold count is incremented by one and the method returns
         * {@code true}.
         *
         * <p>If the lock is held by another thread then the current
         * thread becomes disabled for thread scheduling purposes and
         * lies dormant until one of three things happens:
         *
         * <ul>
         *
         * <li>The write lock is acquired by the current thread; or
         *
         * <li>Some other thread {@linkplain Thread#interrupt interrupts}
         * the current thread; or
         *
         * <li>The specified waiting time elapses
         *
         * </ul>
         *
         * <p>If the write lock is acquired then the value {@code true} is
         * returned and the write lock hold count is set to one.
         *
         * <p>If the current thread:
         *
         * <ul>
         *
         * <li>has its interrupted status set on entry to this method;
         * or
         *
         * <li>is {@linkplain Thread#interrupt interrupted} while
         * acquiring the write lock,
         *
         * </ul>
         *
         * then {@link InterruptedException} is thrown and the current
         * thread's interrupted status is cleared.
         *
         * <p>If the specified waiting time elapses then the value
         * {@code false} is returned.  If the time is less than or
         * equal to zero, the method will not wait at all.
         *
         * <p>In this implementation, as this method is an explicit
         * interruption point, preference is given to responding to
         * the interrupt over normal or reentrant acquisition of the
         * lock, and over reporting the elapse of the waiting time.
         *
         * @param timeout the time to wait for the write lock
         * @param unit the time unit of the timeout argument
         *
         * @return {@code true} if the lock was free and was acquired
         * by the current thread, or the write lock was already held by the
         * current thread; and {@code false} if the waiting time
         * elapsed before the lock could be acquired.
         *
         * @throws InterruptedException if the current thread is interrupted
         * @throws NullPointerException if the time unit is null
         */
        public boolean tryLock(long timeout, TimeUnit unit)
                throws InterruptedException {
            return sync.tryAcquireNanos(1, unit.toNanos(timeout));
        }

        /**
         * Attempts to release this lock.
         * <p>
         *     尝试释放写锁；
         *
         * <p>If the current thread is the holder of this lock then
         * the hold count is decremented. If the hold count is now
         * zero then the lock is released.  If the current thread is
         * not the holder of this lock then {@link
         * IllegalMonitorStateException} is thrown.
         * <p>
         *     如果当前线程是此锁的持有者，那么就将 count 减 1；
         *     如果 count 现在为 0，那么就释放锁；
         *     如果当前线程不是此锁的持有者，那么就抛出 IllegalMonitorStateException 异常；
         *
         * @throws IllegalMonitorStateException if the current thread does not
         * hold this lock
         */
        public void unlock() {
            sync.release(1);
        }

        /**
         * Returns a {@link Condition} instance for use with this
         * {@link Lock} instance.
         * <p>The returned {@link Condition} instance supports the same
         * usages as do the {@link Object} monitor methods ({@link
         * Object#wait() wait}, {@link Object#notify notify}, and {@link
         * Object#notifyAll notifyAll}) when used with the built-in
         * monitor lock.
         * <p>
         *     返回一个 Condition 实例，用于与此 Lock 实例一起使用；
         *     返回的 Condition 实例支持与内置监视器锁一起使用时相同的用法（Object 的 wait、notify 和 notifyAll 方法）；
         *
         * <ul>
         *
         * <li>If this write lock is not held when any {@link
         * Condition} method is called then an {@link
         * IllegalMonitorStateException} is thrown.  (Read locks are
         * held independently of write locks, so are not checked or
         * affected. However it is essentially always an error to
         * invoke a condition waiting method when the current thread
         * has also acquired read locks, since other threads that
         * could unblock it will not be able to acquire the write
         * lock.)
         *
         * <li>When the condition {@linkplain Condition#await() waiting}
         * methods are called the write lock is released and, before
         * they return, the write lock is reacquired and the lock hold
         * count restored to what it was when the method was called.
         *
         * <li>If a thread is {@linkplain Thread#interrupt interrupted} while
         * waiting then the wait will terminate, an {@link
         * InterruptedException} will be thrown, and the thread's
         * interrupted status will be cleared.
         *
         * <li> Waiting threads are signalled in FIFO order.
         *
         * <li>The ordering of lock reacquisition for threads returning
         * from waiting methods is the same as for threads initially
         * acquiring the lock, which is in the default case not specified,
         * but for <em>fair</em> locks favors those threads that have been
         * waiting the longest.
         *
         * </ul>
         *
         * @return the Condition object
         */
        public Condition newCondition() {
            return sync.newCondition();
        }

        /**
         * Returns a string identifying this lock, as well as its lock
         * state.  The state, in brackets includes either the String
         * {@code "Unlocked"} or the String {@code "Locked by"}
         * followed by the {@linkplain Thread#getName name} of the owning thread.
         *
         * @return a string identifying this lock, as well as its lock state
         */
        public String toString() {
            Thread o = sync.getOwner();
            return super.toString() + ((o == null) ?
                                       "[Unlocked]" :
                                       "[Locked by thread " + o.getName() + "]");
        }

        /**
         * Queries if this write lock is held by the current thread.
         * Identical in effect to {@link
         * ReentrantReadWriteLock#isWriteLockedByCurrentThread}.
         *
         * @return {@code true} if the current thread holds this lock and
         *         {@code false} otherwise
         * @since 1.6
         */
        public boolean isHeldByCurrentThread() {
            return sync.isHeldExclusively();
        }

        /**
         * Queries the number of holds on this write lock by the current
         * thread.  A thread has a hold on a lock for each lock action
         * that is not matched by an unlock action.  Identical in effect
         * to {@link ReentrantReadWriteLock#getWriteHoldCount}.
         *
         * @return the number of holds on this lock by the current thread,
         *         or zero if this lock is not held by the current thread
         * @since 1.6
         */
        public int getHoldCount() {
            return sync.getWriteHoldCount();
        }
    }

    // Instrumentation and status

    /**
     * Returns {@code true} if this lock has fairness set true.
     * <p>
     *     如果此锁设置为公平锁，那么就返回 true；
     *
     * @return {@code true} if this lock has fairness set true
     */
    public final boolean isFair() {
        return sync instanceof FairSync;
    }

    /**
     * Returns the thread that currently owns the write lock, or
     * {@code null} if not owned. When this method is called by a
     * thread that is not the owner, the return value reflects a
     * best-effort approximation of current lock status. For example,
     * the owner may be momentarily {@code null} even if there are
     * threads trying to acquire the lock but have not yet done so.
     * This method is designed to facilitate construction of
     * subclasses that provide more extensive lock monitoring
     * facilities.
     * <p>
     *     返回当前拥有写锁的线程，如果没有拥有者，那么就返回 null；
     *     当不是拥有者的线程调用此方法时，返回值反映了当前锁状态的最佳努力近似。
     *     例如，即使有线程试图获取锁但尚未这样做，拥有者可能暂时为 null。
     *     这个方法是为了方便构造提供更广泛的监控设施的子类；
     *
     * @return the owner, or {@code null} if not owned
     */
    protected Thread getOwner() {
        return sync.getOwner();
    }

    /**
     * Queries the number of read locks held for this lock. This
     * method is designed for use in monitoring system state, not for
     * synchronization control.
     * <p>
     *     查询此锁持有的读锁的数量；
     *     这个方法是为了方便构造提供更广泛的监控设施的子类；
     * @return the number of read locks held
     */
    public int getReadLockCount() {
        return sync.getReadLockCount();
    }

    /**
     * Queries if the write lock is held by any thread. This method is
     * designed for use in monitoring system state, not for
     * synchronization control.
     * <p>
     *     查询是否有线程持有写锁；
     *     这个方法是为了方便构造提供更广泛的监控设施的子类；
     *
     * @return {@code true} if any thread holds the write lock and
     *         {@code false} otherwise
     */
    public boolean isWriteLocked() {
        return sync.isWriteLocked();
    }

    /**
     * Queries if the write lock is held by the current thread.
     * <p>
     *     查询当前线程是否持有写锁；
     *
     * @return {@code true} if the current thread holds the write lock and
     *         {@code false} otherwise
     */
    public boolean isWriteLockedByCurrentThread() {
        return sync.isHeldExclusively();
    }

    /**
     * Queries the number of reentrant write holds on this lock by the
     * current thread.  A writer thread has a hold on a lock for
     * each lock action that is not matched by an unlock action.
     * <p>
     *     查询当前线程对此锁的重入写锁的数量。
     *     写线程对每个未匹配解锁操作的锁操作都持有锁；
     *
     * @return the number of holds on the write lock by the current thread,
     *         or zero if the write lock is not held by the current thread
     */
    public int getWriteHoldCount() {
        return sync.getWriteHoldCount();
    }

    /**
     * Queries the number of reentrant read holds on this lock by the
     * current thread.  A reader thread has a hold on a lock for
     * each lock action that is not matched by an unlock action.
     * <p>
     *     查询当前线程对此锁的重入读锁的数量。
     *     读线程对每个未匹配解锁操作的锁操作都持有锁；
     *
     * @return the number of holds on the read lock by the current thread,
     *         or zero if the read lock is not held by the current thread
     * @since 1.6
     */
    public int getReadHoldCount() {
        return sync.getReadHoldCount();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire the write lock.  Because the actual set of threads may
     * change dynamically while constructing this result, the returned
     * collection is only a best-effort estimate.  The elements of the
     * returned collection are in no particular order.  This method is
     * designed to facilitate construction of subclasses that provide
     * more extensive lock monitoring facilities.
     * <p>
     *     返回一个包含可能正在等待获取写锁的线程的集合。
     *     因为实际的线程集合可能在构造此结果时动态变化，所以返回的集合只是一个尽力估计。
     *     返回的集合的元素没有特定的顺序。
     *     这个方法是为了方便构造提供更广泛的监控设施的子类；
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedWriterThreads() {
        return sync.getExclusiveQueuedThreads();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire the read lock.  Because the actual set of threads may
     * change dynamically while constructing this result, the returned
     * collection is only a best-effort estimate.  The elements of the
     * returned collection are in no particular order.  This method is
     * designed to facilitate construction of subclasses that provide
     * more extensive lock monitoring facilities.
     * <p>
     *     返回一个包含可能正在等待获取读锁的线程的集合。
     *     因为实际的线程集合可能在构造此结果时动态变化，所以返回的集合只是一个尽力估计。
     *     返回的集合的元素没有特定的顺序。
     *     这个方法是为了方便构造提供更广泛的监控设施的子类；
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedReaderThreads() {
        return sync.getSharedQueuedThreads();
    }

    /**
     * Queries whether any threads are waiting to acquire the read or
     * write lock. Note that because cancellations may occur at any
     * time, a {@code true} return does not guarantee that any other
     * thread will ever acquire a lock.  This method is designed
     * primarily for use in monitoring of the system state.
     * <p>
     *     查询是否有线程正在等待获取读锁或写锁。
     *     注意，由于取消可能在任何时候发生，因此返回 true 并不保证任何其他线程将永远获取锁。
     *     这个方法是为了方便监控系统状态；
     *
     * @return {@code true} if there may be other threads waiting to
     *         acquire the lock
     */
    public final boolean hasQueuedThreads() {
        return sync.hasQueuedThreads();
    }

    /**
     * Queries whether the given thread is waiting to acquire either
     * the read or write lock. Note that because cancellations may
     * occur at any time, a {@code true} return does not guarantee
     * that this thread will ever acquire a lock.  This method is
     * designed primarily for use in monitoring of the system state.
     * <p>
     *     查询给定的线程是否正在等待获取读锁或写锁。
     *     注意，由于取消可能在任何时候发生，因此返回 true 并不保证此线程将永远获取锁。
     *     这个方法是为了方便监控系统状态；
     *
     * @param thread the thread
     * @return {@code true} if the given thread is queued waiting for this lock
     * @throws NullPointerException if the thread is null
     */
    public final boolean hasQueuedThread(Thread thread) {
        return sync.isQueued(thread);
    }

    /**
     * Returns an estimate of the number of threads waiting to acquire
     * either the read or write lock.  The value is only an estimate
     * because the number of threads may change dynamically while this
     * method traverses internal data structures.  This method is
     * designed for use in monitoring of the system state, not for
     * synchronization control.
     * <p>
     *     返回正在等待获取读锁或写锁的线程的估计数量。
     *     这个值只是一个估计，因为在这个方法遍历内部数据结构时，线程的数量可能会动态变化。
     *     这个方法是为了方便监控系统状态，而不是用于同步控制；
     *
     * @return the estimated number of threads waiting for this lock
     */
    public final int getQueueLength() {
        return sync.getQueueLength();
    }

    /**
     * Returns a collection containing threads that may be waiting to
     * acquire either the read or write lock.  Because the actual set
     * of threads may change dynamically while constructing this
     * result, the returned collection is only a best-effort estimate.
     * The elements of the returned collection are in no particular
     * order.  This method is designed to facilitate construction of
     * subclasses that provide more extensive monitoring facilities.
     * <p>
     *     返回一个包含可能正在等待获取读锁或写锁的线程的集合。
     *     因为实际的线程集合可能在构造此结果时动态变化，所以返回的集合只是一个尽力估计。
     *     返回的集合的元素没有特定的顺序。
     *     这个方法是为了方便构造提供更广泛的监控设施的子类；
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedThreads() {
        return sync.getQueuedThreads();
    }

    /**
     * Queries whether any threads are waiting on the given condition
     * associated with the write lock. Note that because timeouts and
     * interrupts may occur at any time, a {@code true} return does
     * not guarantee that a future {@code signal} will awaken any
     * threads.  This method is designed primarily for use in
     * monitoring of the system state.
     *
     * @param condition the condition
     * @return {@code true} if there are any waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    public boolean hasWaiters(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.hasWaiters((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns an estimate of the number of threads waiting on the
     * given condition associated with the write lock. Note that because
     * timeouts and interrupts may occur at any time, the estimate
     * serves only as an upper bound on the actual number of waiters.
     * This method is designed for use in monitoring of the system
     * state, not for synchronization control.
     *
     * @param condition the condition
     * @return the estimated number of waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    public int getWaitQueueLength(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitQueueLength((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns a collection containing those threads that may be
     * waiting on the given condition associated with the write lock.
     * Because the actual set of threads may change dynamically while
     * constructing this result, the returned collection is only a
     * best-effort estimate. The elements of the returned collection
     * are in no particular order.  This method is designed to
     * facilitate construction of subclasses that provide more
     * extensive condition monitoring facilities.
     *
     * @param condition the condition
     * @return the collection of threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    protected Collection<Thread> getWaitingThreads(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitingThreads((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns a string identifying this lock, as well as its lock state.
     * The state, in brackets, includes the String {@code "Write locks ="}
     * followed by the number of reentrantly held write locks, and the
     * String {@code "Read locks ="} followed by the number of held
     * read locks.
     *
     * @return a string identifying this lock, as well as its lock state
     */
    public String toString() {
        int c = sync.getCount();
        int w = Sync.exclusiveCount(c);
        int r = Sync.sharedCount(c);

        return super.toString() +
            "[Write locks = " + w + ", Read locks = " + r + "]";
    }

    /**
     * Returns the thread id for the given thread.  We must access
     * this directly rather than via method Thread.getId() because
     * getId() is not final, and has been known to be overridden in
     * ways that do not preserve unique mappings.
     */
    static final long getThreadId(Thread thread) {
        return UNSAFE.getLongVolatile(thread, TID_OFFSET);
    }

    // Unsafe mechanics
    private static final sun.misc.Unsafe UNSAFE;
    private static final long TID_OFFSET;
    static {
        try {
            UNSAFE = sun.misc.Unsafe.getUnsafe();
            Class<?> tk = Thread.class;
            TID_OFFSET = UNSAFE.objectFieldOffset
                (tk.getDeclaredField("tid"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

}
