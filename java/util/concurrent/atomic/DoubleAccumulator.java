/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent.atomic;
import java.io.Serializable;
import java.util.function.DoubleBinaryOperator;

/**
 * One or more variables that together maintain a running {@code double}
 * value updated using a supplied function.  When updates (method
 * {@link #accumulate}) are contended across threads, the set of variables
 * may grow dynamically to reduce contention.  Method {@link #get}
 * (or, equivalently, {@link #doubleValue}) returns the current value
 * across the variables maintaining updates.
 * <p>
 *     一个或多个变量，它们一起使用提供的函数维护一个运行的 double 值。
 *     当更新（方法 accumulate）在多个线程之间发生争用时，变量集合可能会动态增长以减少争用。
 *     方法 get（或等效地，doubleValue）返回维护更新的变量的当前值。
 *
 * <p>This class is usually preferable to alternatives when multiple
 * threads update a common value that is used for purposes such as
 * summary statistics that are frequently updated but less frequently
 * read.
 * <p>
 *     当多个线程更新用于诸如频繁更新但不太频繁读取的汇总统计等目的的共同值时，通常应优先使用此类而不是其他类。
 *
 * <p>The supplied accumulator function should be side-effect-free,
 * since it may be re-applied when attempted updates fail due to
 * contention among threads. The function is applied with the current
 * value as its first argument, and the given update as the second
 * argument.  For example, to maintain a running maximum value, you
 * could supply {@code Double::max} along with {@code
 * Double.NEGATIVE_INFINITY} as the identity. The order of
 * accumulation within or across threads is not guaranteed. Thus, this
 * class may not be applicable if numerical stability is required,
 * especially when combining values of substantially different orders
 * of magnitude.
 * <p>
 *     由于尝试更新由于线程之间的争用而失败时，可能会重新应用累加器函数，因此提供的累加器函数应该是无副作用的。
 *     该函数将当前值作为第一个参数应用，并将给定的更新作为第二个参数应用。
 *     例如，要维护运行的最大值，可以提供 Double::max 以及 Double.NEGATIVE_INFINITY 作为标识。
 *     线程内或线程间的累积顺序不能保证。因此，如果需要数值稳定性，特别是在合并数量级差异很大的值时，此类可能不适用。
 *
 * <p>Class {@link DoubleAdder} provides analogs of the functionality
 * of this class for the common special case of maintaining sums.  The
 * call {@code new DoubleAdder()} is equivalent to {@code new
 * DoubleAccumulator((x, y) -> x + y, 0.0)}.
 * <p>
 *     类 DoubleAdder 为此类的常见特殊情况提供了类似的功能。
 *     调用 new DoubleAdder() 等效于 new DoubleAccumulator((x, y) -> x + y, 0.0)。
 *
 * <p>This class extends {@link Number}, but does <em>not</em> define
 * methods such as {@code equals}, {@code hashCode} and {@code
 * compareTo} because instances are expected to be mutated, and so are
 * not useful as collection keys.
 * <p>
 *     此类扩展了 Number，但不定义诸如 equals、hashCode 和 compareTo 等方法，因为预计实例将被改变，因此不适用作集合键。
 *
 * @since 1.8
 * @author Doug Lea
 */
public class DoubleAccumulator extends Striped64 implements Serializable {
    private static final long serialVersionUID = 7249069246863182397L;

    private final DoubleBinaryOperator function;
    private final long identity; // use long representation

    // TODO REMIND cas 底层不支持 double，因此实际上在 Striped64 中是用 long 来表示的 double

    /**
     * Creates a new instance using the given accumulator function
     * and identity element.
     * <p>
     *     使用给定的累加器函数和标识元素创建一个新实例。
     *
     * @param accumulatorFunction a side-effect-free function of two arguments
     * @param identity identity (initial value) for the accumulator function
     */
    public DoubleAccumulator(DoubleBinaryOperator accumulatorFunction,
                             double identity) {
        this.function = accumulatorFunction;
        base = this.identity = Double.doubleToRawLongBits(identity);
    }

    /**
     * Updates with the given value.
     * <p>
     *     使用给定值更新。
     *
     * @param x the value
     */
    public void accumulate(double x) {
        // 定义一个数组
        Cell[] as; long b, v, r; int m; Cell a;

        if ((as = cells) != null || // 1. cells 数组不为空
            (r = Double.doubleToRawLongBits // 2. 在 cells 为空时，基于 function 计算 base 和 x 的值，并将结果赋值回 base
             (function.applyAsDouble
              (Double.longBitsToDouble(b = base), x))) != b  && !casBase(b, r)) {


            boolean uncontended = true;
            if (as == null || (m = as.length - 1) < 0 ||
                (a = as[getProbe() & m]) == null ||
                !(uncontended =
                  (r = Double.doubleToRawLongBits
                   (function.applyAsDouble
                    (Double.longBitsToDouble(v = a.value), x))) == v ||
                  a.cas(v, r)))
                // 调用父类 Striped64 的 doubleAccumulate 方法，将 x 添加到 cells 数组中
                doubleAccumulate(x, function, uncontended);
        }
    }

    /**
     * Returns the current value.  The returned value is <em>NOT</em>
     * an atomic snapshot; invocation in the absence of concurrent
     * updates returns an accurate result, but concurrent updates that
     * occur while the value is being calculated might not be
     * incorporated.
     * <p>
     *     返回当前值。
     *     返回的值不是原子快照；在没有并发更新的情况下调用会返回准确的结果，但在计算值时发生的并发更新可能不会被合并。
     *
     * @return the current value
     */
    public double get() {
        Cell[] as = cells; Cell a;
        // 取 base 值
        double result = Double.longBitsToDouble(base);
        if (as != null) {
            for (int i = 0; i < as.length; ++i) {
                if ((a = as[i]) != null)
                    //  遍历每个 cell 的元素值，将每个 cell 的元素值添加到 result 中
                    result = function.applyAsDouble
                        (result, Double.longBitsToDouble(a.value));
            }
        }
        return result;
    }

    /**
     * Resets variables maintaining updates to the identity value.
     * This method may be a useful alternative to creating a new
     * updater, but is only effective if there are no concurrent
     * updates.  Because this method is intrinsically racy, it should
     * only be used when it is known that no threads are concurrently
     * updating.
     * <p>
     *     重置维护更新的变量为标识值。此方法可能是创建新更新器的有用替代方法，但仅在没有并发更新时才有效。
     *     因为此方法本质上是有竞争的，所以只有在已知没有线程同时更新时才应使用此方法。
     */
    public void reset() {
        Cell[] as = cells; Cell a;
        // 将 base 设置为标识元素(reset)
        base = identity;
        if (as != null) {
            for (int i = 0; i < as.length; ++i) {
                if ((a = as[i]) != null)
                    // 将每个 cell 的元素值设置为标识元素(reset)
                    a.value = identity;
            }
        }
    }

    /**
     * Equivalent in effect to {@link #get} followed by {@link
     * #reset}. This method may apply for example during quiescent
     * points between multithreaded computations.  If there are
     * updates concurrent with this method, the returned value is
     * <em>not</em> guaranteed to be the final value occurring before
     * the reset.
     * <p>
     *     在效果上等同于 get 后面跟 reset。例如，在多线程计算之间的静止点期间，此方法可能适用。
     *     如果有与此方法并发的更新，则返回的值不能保证是重置之前发生的最终值。
     *
     * @return the value before reset
     */
    public double getThenReset() {
        // 取到 cells 数组的引用
        Cell[] as = cells; Cell a;
        // 将基数 base 添加到 result 中
        double result = Double.longBitsToDouble(base);
        // 更新 base 为标识元素(reset)
        base = identity;

        // 遍历 cells 数组
        if (as != null) {
            for (int i = 0; i < as.length; ++i) {
                if ((a = as[i]) != null) {
                    // 取出每个 cell 中的元素值
                    double v = Double.longBitsToDouble(a.value);
                    // 更新每个 cell 的元素值为标识元素(reset)
                    a.value = identity;
                    // 将每个 cell 的元素值添加到 result 中
                    result = function.applyAsDouble(result, v);
                }
            }
        }

        // 返回遍历汇总后的结果
        return result;
    }

    /**
     * Returns the String representation of the current value.
     * @return the String representation of the current value
     */
    public String toString() {
        return Double.toString(get());
    }

    /**
     * Equivalent to {@link #get}.
     *
     * @return the current value
     */
    public double doubleValue() {
        return get();
    }

    /**
     * Returns the {@linkplain #get current value} as a {@code long}
     * after a narrowing primitive conversion.
     */
    public long longValue() {
        return (long)get();
    }

    /**
     * Returns the {@linkplain #get current value} as an {@code int}
     * after a narrowing primitive conversion.
     */
    public int intValue() {
        return (int)get();
    }

    /**
     * Returns the {@linkplain #get current value} as a {@code float}
     * after a narrowing primitive conversion.
     */
    public float floatValue() {
        return (float)get();
    }

    /**
     * Serialization proxy, used to avoid reference to the non-public
     * Striped64 superclass in serialized forms.
     * <p>
     *     序列化代理，用于避免在序列化形式中引用非公共 Striped64 超类。
     *
     * @serial include
     */
    private static class SerializationProxy implements Serializable {
        private static final long serialVersionUID = 7249069246863182397L;

        /**
         * The current value returned by get().
         * @serial
         */
        private final double value;
        /**
         * The function used for updates.
         * @serial
         */
        private final DoubleBinaryOperator function;
        /**
         * The identity value
         * @serial
         */
        private final long identity;

        SerializationProxy(DoubleAccumulator a) {
            function = a.function;
            identity = a.identity;
            value = a.get();
        }

        /**
         * Returns a {@code DoubleAccumulator} object with initial state
         * held by this proxy.
         *
         * @return a {@code DoubleAccumulator} object with initial state
         * held by this proxy.
         */
        private Object readResolve() {
            double d = Double.longBitsToDouble(identity);
            DoubleAccumulator a = new DoubleAccumulator(function, d);
            a.base = Double.doubleToRawLongBits(value);
            return a;
        }
    }

    /**
     * Returns a
     * <a href="../../../../serialized-form.html#java.util.concurrent.atomic.DoubleAccumulator.SerializationProxy">
     * SerializationProxy</a>
     * representing the state of this instance.
     *
     * @return a {@link SerializationProxy}
     * representing the state of this instance
     */
    private Object writeReplace() {
        return new SerializationProxy(this);
    }

    /**
     * @param s the stream
     * @throws java.io.InvalidObjectException always
     */
    private void readObject(java.io.ObjectInputStream s)
        throws java.io.InvalidObjectException {
        throw new java.io.InvalidObjectException("Proxy required");
    }

}
