/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent;
import java.util.concurrent.locks.LockSupport;

/**
 * A cancellable asynchronous computation.  This class provides a base
 * implementation of {@link Future}, with methods to start and cancel
 * a computation, query to see if the computation is complete, and
 * retrieve the result of the computation.  The result can only be
 * retrieved when the computation has completed; the {@code get}
 * methods will block if the computation has not yet completed.  Once
 * the computation has completed, the computation cannot be restarted
 * or cancelled (unless the computation is invoked using
 * {@link #runAndReset}).
 * <p>
 *     可取消的异步计算。
 *     此类提供了 Future 的基本实现，提供了启动和取消计算、查询计算是否完成以及检索计算结果的方法。
 *     只有在计算完成时才能检索结果；如果计算尚未完成，get 方法将阻塞。
 *     一旦计算完成，计算就无法重新启动或取消（除非使用 runAndReset 调用计算）。
 *
 * <p>A {@code FutureTask} can be used to wrap a {@link Callable} or
 * {@link Runnable} object.  Because {@code FutureTask} implements
 * {@code Runnable}, a {@code FutureTask} can be submitted to an
 * {@link Executor} for execution.
 * <p>
 *     FutureTask 可用于包装 Callable 或 Runnable 对象。
 *     因为 FutureTask 实现了 Runnable，所以 FutureTask 可以提交给 Executor 执行。
 *
 * <p>In addition to serving as a standalone class, this class provides
 * {@code protected} functionality that may be useful when creating
 * customized task classes.
 * <p>
 *     除了作为独立类之外，此类还提供了可能在创建自定义任务类时有用的 protected 功能。
 *
 * @since 1.5
 * @author Doug Lea
 * @param <V> The result type returned by this FutureTask's {@code get} methods
 */
public class FutureTask<V> implements RunnableFuture<V> {
    /*
     * Revision notes: This differs from previous versions of this
     * class that relied on AbstractQueuedSynchronizer, mainly to
     * avoid surprising users about retaining interrupt status during
     * cancellation races. Sync control in the current design relies
     * on a "state" field updated via CAS to track completion, along
     * with a simple Treiber stack to hold waiting threads.
     * <p>
     *    修订说明：这与此类的先前版本不同，先前版本依赖于 AbstractQueuedSynchronizer，主要是为了避免在取消竞争中保留中断状态，以免让用户感到惊讶。
     *    当前设计中的同步控制依赖于通过 CAS 更新的“state”字段来跟踪完成状态，以及一个简单的 Treiber 栈来保存等待线程。
     *
     * Style note: As usual, we bypass overhead of using
     * AtomicXFieldUpdaters and instead directly use Unsafe intrinsics.
     * <p>
     *    样式说明：通常情况下，我们绕过使用 AtomicXFieldUpdaters 的开销，而是直接使用 Unsafe 内部函数。
     */

    // todo FutureTask 既是 Future 也是 Task，因此它既可以代表一个任务，也可以代表一个结果

    /**
     * The run state of this task, initially NEW.  The run state
     * transitions to a terminal state only in methods set,
     * setException, and cancel.  During completion, state may take on
     * transient values of COMPLETING (while outcome is being set) or
     * INTERRUPTING (only while interrupting the runner to satisfy a
     * cancel(true)). Transitions from these intermediate to final
     * states use cheaper ordered/lazy writes because values are unique
     * and cannot be further modified.
     * <p>
     *     此任务的运行状态，最初为 NEW。
     *     运行状态只在 set、setException 和 cancel 方法中转换为终端状态。
     *     在完成期间，状态可能具有 COMPLETING（在设置结果时）或 INTERRUPTING（仅在中断运行程序以满足 cancel(true) 时）的瞬时值。
     *     从这些中间状态到最终状态的转换使用更便宜的有序/延迟写入，因为值是唯一的，不能进一步修改。
     *
     * Possible state transitions:
     * NEW -> COMPLETING -> NORMAL
     * NEW -> COMPLETING -> EXCEPTIONAL
     * NEW -> CANCELLED
     * NEW -> INTERRUPTING -> INTERRUPTED
     */
    private volatile int state;
    private static final int NEW          = 0;
    private static final int COMPLETING   = 1;
    private static final int NORMAL       = 2;
    private static final int EXCEPTIONAL  = 3;
    private static final int CANCELLED    = 4;
    private static final int INTERRUPTING = 5;
    private static final int INTERRUPTED  = 6;

    /**
     * The underlying callable; nulled out after running
     * <p>
     * 底层的 callable；在运行后置空
     */
    private Callable<V> callable;

    /**
     * The result to return or exception to throw from get()
     * <p>
     * 从 get() 返回的结果或抛出的异常
     */
    private Object outcome; // non-volatile, protected by state reads/writes

    /**
     * The thread running the callable; CASed during run()
     * <p>
     *     运行 callable 的线程；在 run() 期间 CAS
     */
    private volatile Thread runner;

    /**
     * Treiber stack of waiting threads
     * <p>
     *     等待线程的 Treiber 栈
     */
    private volatile WaitNode waiters;

    /**
     * Creates a {@code FutureTask} that will, upon running, execute the
     * given {@code Callable}.
     * <p>
     *     创建一个 FutureTask，该 FutureTask 将在运行时执行给定的 Callable。
     *
     * @param  callable the callable task
     * @throws NullPointerException if the callable is null
     */
    public FutureTask(Callable<V> callable) {
        if (callable == null)
            throw new NullPointerException();
        this.callable = callable;
        this.state = NEW;       // ensure visibility of callable
    }

    /**
     * Creates a {@code FutureTask} that will, upon running, execute the
     * given {@code Runnable}, and arrange that {@code get} will return the
     * given result on successful completion.
     * <p>
     *     创建一个 FutureTask，该 FutureTask 将在运行时执行给定的 Runnable，并安排 get 在成功完成时返回给定的结果。
     *
     * @param runnable the runnable task
     * @param result the result to return on successful completion. If
     * you don't need a particular result, consider using
     * constructions of the form:
     * {@code Future<?> f = new FutureTask<Void>(runnable, null)}
     * @throws NullPointerException if the runnable is null
     */
    public FutureTask(Runnable runnable, V result) {
        this.callable = Executors.callable(runnable, result);
        this.state = NEW;       // ensure visibility of callable
    }

    /**
     * Returns result or throws exception for completed task.
     * <p>
     *     返回已完成任务的结果或抛出异常。
     *
     * @param s completed state value 已完成状态值
     */
    @SuppressWarnings("unchecked")
    private V report(int s) throws ExecutionException {
        Object x = outcome;
        // 如果状态值正常，则返回结果
        if (s == NORMAL)
            return (V)x;
        // 如果状态值异常(被取消或被中断)，则抛出异常
        if (s >= CANCELLED)
            throw new CancellationException();
        // 否则抛出异常
        throw new ExecutionException((Throwable)x);
    }

    // 判断任务是否已经被取消，只要 state >= CANCELLED 就表示任务已经被取消
    public boolean isCancelled() {
        return state >= CANCELLED;
    }

    // 判断任务是否已经完成，只要 state != NEW 就表示任务已经完成
    public boolean isDone() {
        return state != NEW;
    }

    // 尝试取消任务，如果任务已经完成或者已经被取消，则返回 false
    public boolean cancel(boolean mayInterruptIfRunning) {
        // 如果任务已经完成或者 cas 失败，则返回 false
        if (!(state == NEW &&
              UNSAFE.compareAndSwapInt(this, stateOffset, NEW,
                  mayInterruptIfRunning ? INTERRUPTING : CANCELLED)))
            return false;
        try {    // in case call to interrupt throws exception
            if (mayInterruptIfRunning) {
                try {
                    Thread t = runner;
                    if (t != null)
                        t.interrupt();
                } finally { // final state
                    UNSAFE.putOrderedInt(this, stateOffset, INTERRUPTED);
                }
            }
        } finally {
            finishCompletion();
        }
        return true;
    }

    /**
     * @throws CancellationException {@inheritDoc}
     */
    public V get() throws InterruptedException, ExecutionException {
        // 获取当前任务的状态
        int s = state;
        // 如果任务还没有完成，则阻塞当前线程
        if (s <= COMPLETING)
            s = awaitDone(false, 0L);
        // 返回结果
        return report(s);
    }

    /**
     * @throws CancellationException {@inheritDoc}
     */
    public V get(long timeout, TimeUnit unit)
        throws InterruptedException, ExecutionException, TimeoutException {
        if (unit == null)
            throw new NullPointerException();
        int s = state;
        if (s <= COMPLETING &&
            (s = awaitDone(true, unit.toNanos(timeout))) <= COMPLETING)
            throw new TimeoutException();
        return report(s);
    }

    /**
     * Protected method invoked when this task transitions to state
     * {@code isDone} (whether normally or via cancellation). The
     * default implementation does nothing.  Subclasses may override
     * this method to invoke completion callbacks or perform
     * bookkeeping. Note that you can query status inside the
     * implementation of this method to determine whether this task
     * has been cancelled.
     * <p>
     *     当此任务转换为状态 isDone（无论是正常还是通过取消）时调用的受保护方法。
     *     默认实现什么也不做。子类可以重写此方法以调用完成回调或执行簿记。
     *     请注意，您可以在此方法的实现中查询状态，以确定此任务是否已取消。
     */
    protected void done() { }

    /**
     * Sets the result of this future to the given value unless
     * this future has already been set or has been cancelled.
     * <p>
     *     将此 future 的结果设置为给定值，除非此 future 已经设置或已取消。
     *
     * <p>This method is invoked internally by the {@link #run} method
     * upon successful completion of the computation.
     * <p>
     *     在计算成功完成时，此方法由 run 方法内部调用。
     *
     * @param v the value
     */
    protected void set(V v) {
        if (UNSAFE.compareAndSwapInt(this, stateOffset, NEW, COMPLETING)) {
            outcome = v;
            UNSAFE.putOrderedInt(this, stateOffset, NORMAL); // final state
            finishCompletion();
        }
    }

    /**
     * Causes this future to report an {@link ExecutionException}
     * with the given throwable as its cause, unless this future has
     * already been set or has been cancelled.
     * <p>
     *     使此 future 报告一个 ExecutionException，其中包含给定的 throwable 作为其原因，除非此 future 已经设置或已取消。
     *
     * <p>This method is invoked internally by the {@link #run} method
     * upon failure of the computation.
     * <p>
     *     在计算失败时，此方法由 run 方法内部调用。
     *
     * @param t the cause of failure
     */
    protected void setException(Throwable t) {
        // cas 将状态从 NEW 设置为 COMPLETING
        if (UNSAFE.compareAndSwapInt(this, stateOffset, NEW, COMPLETING)) {
            // 将结果设置为 t
            outcome = t;
            // 将状态设置为 EXCEPTIONAL
            UNSAFE.putOrderedInt(this, stateOffset, EXCEPTIONAL); // final state
            // 通知所有等待线程
            finishCompletion();
        }
    }

    public void run() {
        // 如果 state 不是 NEW 或者 cas 设置执行线程失败，则直接返回
        if (state != NEW ||
            !UNSAFE.compareAndSwapObject(this, runnerOffset,
                                         null, Thread.currentThread()))
            return;
        try {
            Callable<V> c = callable;
            // 如果任务不为空，且 task 正常，则执行任务
            if (c != null && state == NEW) {
                V result;
                boolean ran;
                try {
                    // 执行任务
                    result = c.call();
                    ran = true;
                } catch (Throwable ex) {
                    result = null;
                    ran = false;
                    setException(ex);
                }
                // 将 result 设置到 outcome 中
                if (ran)
                    set(result);
            }
        } finally {
            // runner must be non-null until state is settled to
            // prevent concurrent calls to run()
            //  清理
            runner = null;
            // state must be re-read after nulling runner to prevent
            // leaked interrupts
            int s = state;
            if (s >= INTERRUPTING)
                handlePossibleCancellationInterrupt(s);
        }
    }

    /**
     * Executes the computation without setting its result, and then
     * resets this future to initial state, failing to do so if the
     * computation encounters an exception or is cancelled.  This is
     * designed for use with tasks that intrinsically execute more
     * than once.
     * <p>
     *     在不设置其结果的情况下执行计算，然后将此 future 重置为初始状态，如果计算遇到异常或被取消，则无法执行此操作。
     *     这是为了与本质上执行多次的任务一起使用而设计的。
     *
     * @return {@code true} if successfully run and reset
     */
    protected boolean runAndReset() {
        if (state != NEW ||
            !UNSAFE.compareAndSwapObject(this, runnerOffset,
                                         null, Thread.currentThread()))
            return false;
        boolean ran = false;
        int s = state;
        try {
            Callable<V> c = callable;
            if (c != null && s == NEW) {
                try {
                    c.call(); // don't set result
                    ran = true;
                } catch (Throwable ex) {
                    setException(ex);
                }
            }
        } finally {
            // runner must be non-null until state is settled to
            // prevent concurrent calls to run()
            runner = null;
            // state must be re-read after nulling runner to prevent
            // leaked interrupts
            s = state;
            if (s >= INTERRUPTING)
                handlePossibleCancellationInterrupt(s);
        }
        return ran && s == NEW;
    }

    /**
     * Ensures that any interrupt from a possible cancel(true) is only
     * delivered to a task while in run or runAndReset.
     * <p>
     *     确保只有在 run 或 runAndReset 期间才会将可能的 cancel(true) 中断传递给任务。
     */
    private void handlePossibleCancellationInterrupt(int s) {
        // It is possible for our interrupter to stall before getting a
        // chance to interrupt us.  Let's spin-wait patiently.
        if (s == INTERRUPTING)
            // 如果 state 处于 INTERRUPTING 状态，则持续地 yield
            while (state == INTERRUPTING)
                Thread.yield(); // wait out pending interrupt

        // assert state == INTERRUPTED;

        // We want to clear any interrupt we may have received from
        // cancel(true).  However, it is permissible to use interrupts
        // as an independent mechanism for a task to communicate with
        // its caller, and there is no way to clear only the
        // cancellation interrupt.
        //
        // Thread.interrupted();
    }

    /**
     * Removes and signals all waiting threads, invokes done(), and
     * nulls out callable.
     * <p>
     *     移除并通知所有等待线程，调用 done()，并将 callable 置空。
     */
    private void finishCompletion() {
        // assert state > COMPLETING;
        for (WaitNode q; (q = waiters) != null;) {
            // cas 将 waiters 置空；如果失败则重试
            if (UNSAFE.compareAndSwapObject(this, waitersOffset, q, null)) {
                for (;;) {
                    // 唤醒每个线程
                    Thread t = q.thread;
                    if (t != null) {
                        q.thread = null;
                        LockSupport.unpark(t);
                    }
                    WaitNode next = q.next;
                    if (next == null)
                        break;
                    q.next = null; // unlink to help gc
                    q = next;
                }
                break;
            }
        }

        // 模板方法
        done();

        callable = null;        // to reduce footprint
    }

    /**
     * Awaits completion or aborts on interrupt or timeout.
     * <p>
     *     等待完成或在中断或超时时中止。
     *
     * @param timed true if use timed waits
     * @param nanos time to wait, if timed
     * @return state upon completion
     */
    private int awaitDone(boolean timed, long nanos)
        throws InterruptedException {
        // 计算截止时间
        final long deadline = timed ? System.nanoTime() + nanos : 0L;
        WaitNode q = null;
        boolean queued = false;
        // 持续等待
        for (;;) {
            // 如果当前线程被中断，则移除等待线程并抛出异常
            if (Thread.interrupted()) {
                removeWaiter(q);
                throw new InterruptedException();
            }

            int s = state;
            // 如果任务已经完成，则返回状态值；同时清空 node.thread
            if (s > COMPLETING) {
                if (q != null)
                    q.thread = null;
                return s;
            }
            // 如果任务正在处理中，则持续 yield
            else if (s == COMPLETING) // cannot time out yet
                Thread.yield();
            // 第一次循环，初始化 node
            else if (q == null)
                q = new WaitNode();
            // 第二次循环，将 node 加入到等待队列中
            else if (!queued)
                queued = UNSAFE.compareAndSwapObject(this, waitersOffset,
                                                     q.next = waiters, q);
            // 如果超时了，则移除；否则自我阻塞
            else if (timed) {
                nanos = deadline - System.nanoTime();
                if (nanos <= 0L) {
                    removeWaiter(q);
                    return state;
                }
                LockSupport.parkNanos(this, nanos);
            }
            else
                LockSupport.park(this);
        }
    }

    /**
     * Simple linked list nodes to record waiting threads in a Treiber
     * stack.  See other classes such as Phaser and SynchronousQueue
     * for more detailed explanation.
     * <p>
     *     简单的链表节点，用于记录 Treiber 栈中的等待线程。
     *     有关更详细的解释，请参见其他类，如 Phaser 和 SynchronousQueue。
     */
    static final class WaitNode {
        volatile Thread thread;
        volatile WaitNode next;
        WaitNode() { thread = Thread.currentThread(); }
    }

    /**
     * Tries to unlink a timed-out or interrupted wait node to avoid
     * accumulating garbage.  Internal nodes are simply unspliced
     * without CAS since it is harmless if they are traversed anyway
     * by releasers.  To avoid effects of unsplicing from already
     * removed nodes, the list is retraversed in case of an apparent
     * race.  This is slow when there are a lot of nodes, but we don't
     * expect lists to be long enough to outweigh higher-overhead
     * schemes.
     * <p>
     *     尝试取消链接超时或中断的等待节点，以避免积累垃圾。
     *     内部节点只是简单地取消拼接，而不使用 CAS，因为如果它们被释放者遍历，这是无害的。
     *     为了避免从已经删除的节点中取消拼接的影响，在明显的竞争情况下，重新遍历列表。
     *     当节点很多时，这样做很慢，但我们不希望列表足够长以抵消更高开销的方案。
     *
     */
    private void removeWaiter(WaitNode node) {
        if (node != null) {
            node.thread = null;
            retry:
            for (;;) {          // restart on removeWaiter race
                for (WaitNode pred = null, q = waiters, s; q != null; q = s) {
                    s = q.next;
                    if (q.thread != null)
                        pred = q;
                    else if (pred != null) {
                        pred.next = s;
                        if (pred.thread == null) // check for race
                            continue retry;
                    }
                    else if (!UNSAFE.compareAndSwapObject(this, waitersOffset,
                                                          q, s))
                        continue retry;
                }
                break;
            }
        }
    }

    // Unsafe mechanics
    private static final sun.misc.Unsafe UNSAFE;
    private static final long stateOffset;
    private static final long runnerOffset;
    private static final long waitersOffset;
    static {
        try {
            UNSAFE = sun.misc.Unsafe.getUnsafe();
            Class<?> k = FutureTask.class;
            stateOffset = UNSAFE.objectFieldOffset
                (k.getDeclaredField("state"));
            runnerOffset = UNSAFE.objectFieldOffset
                (k.getDeclaredField("runner"));
            waitersOffset = UNSAFE.objectFieldOffset
                (k.getDeclaredField("waiters"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

}
