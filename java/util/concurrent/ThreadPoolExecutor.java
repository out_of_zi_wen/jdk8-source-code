/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent;

import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * An {@link ExecutorService} that executes each submitted task using
 * one of possibly several pooled threads, normally configured
 * using {@link Executors} factory methods.
 * <p>
 *     一个 ExecutorService，它使用可能的几个池线程之一执行每个提交的任务，通常使用 Executors 工厂方法进行配置。
 *
 * <p>Thread pools address two different problems: they usually
 * provide improved performance when executing large numbers of
 * asynchronous tasks, due to reduced per-task invocation overhead,
 * and they provide a means of bounding and managing the resources,
 * including threads, consumed when executing a collection of tasks.
 * Each {@code ThreadPoolExecutor} also maintains some basic
 * statistics, such as the number of completed tasks.
 * <p>
 *     线程池解决了两个不同的问题：当执行大量异步任务时，它们通常提供了更好的性能，因为减少了每个任务的调用开销，并且它们提供了一种限制和管理资源的方法，包括在执行任务集合时消耗的线程。
 *     每个 ThreadPoolExecutor 还维护一些基本统计信息，例如已完成的任务数。
 *
 * <p>To be useful across a wide range of contexts, this class
 * provides many adjustable parameters and extensibility
 * hooks. However, programmers are urged to use the more convenient
 * {@link Executors} factory methods {@link
 * Executors#newCachedThreadPool} (unbounded thread pool, with
 * automatic thread reclamation), {@link Executors#newFixedThreadPool}
 * (fixed size thread pool) and {@link
 * Executors#newSingleThreadExecutor} (single background thread), that
 * preconfigure settings for the most common usage
 * scenarios. Otherwise, use the following guide when manually
 * configuring and tuning this class:
 * <p>
 *     为了在广泛的上下文中使用，这个类提供了许多可调参数和可扩展挂钩。
 *     然而，建议程序员使用更方便的 Executors 工厂方法 Executors.newCachedThreadPool（无界线程池，具有自动线程回收）、
 *     Executors.newFixedThreadPool（固定大小线程池）和 Executors.newSingleThreadExecutor（单个后台线程），
 *     这些方法为最常见的使用场景预配置了设置。
 *     否则，当手动配置和调整这个类时，请使用以下指南：
 *
 * <dl>
 *
 * <dt>Core and maximum pool sizes</dt>
 *
 * <dd>A {@code ThreadPoolExecutor} will automatically adjust the
 * pool size (see {@link #getPoolSize})
 * according to the bounds set by
 * corePoolSize (see {@link #getCorePoolSize}) and
 * maximumPoolSize (see {@link #getMaximumPoolSize}).
 *
 * When a new task is submitted in method {@link #execute(Runnable)},
 * and fewer than corePoolSize threads are running, a new thread is
 * created to handle the request, even if other worker threads are
 * idle.  If there are more than corePoolSize but less than
 * maximumPoolSize threads running, a new thread will be created only
 * if the queue is full.  By setting corePoolSize and maximumPoolSize
 * the same, you create a fixed-size thread pool. By setting
 * maximumPoolSize to an essentially unbounded value such as {@code
 * Integer.MAX_VALUE}, you allow the pool to accommodate an arbitrary
 * number of concurrent tasks. Most typically, core and maximum pool
 * sizes are set only upon construction, but they may also be changed
 * dynamically using {@link #setCorePoolSize} and {@link
 * #setMaximumPoolSize}. </dd>
 * <p>
 *     核心和最大池大小
 *
 *     ThreadPoolExecutor 将根据 corePoolSize 和 maximumPoolSize 设置的边界自动调整池大小（请参阅 getPoolSize）。
 *     当在 execute(Runnable) 方法中提交新任务时，并且运行的线程少于 corePoolSize 时，将创建一个新线程来处理请求，即使其他工作线程处于空闲状态。
 *     如果运行的线程数大于 corePoolSize 但小于 maximumPoolSize，则只有在队列已满时才会创建新线程。
 *     通过将 corePoolSize 和 maximumPoolSize 设置为相同的值，可以创建一个固定大小的线程池。
 *     通过将 maximumPoolSize 设置为一个基本无界的值，例如 Integer.MAX_VALUE，可以允许池容纳任意数量的并发任务。
 *     通常，核心和最大池大小仅在构造时设置，但也可以使用 setCorePoolSize 和 setMaximumPoolSize 方法动态更改它们。
 *
 * <dt>On-demand construction</dt>
 *
 * <dd>By default, even core threads are initially created and
 * started only when new tasks arrive, but this can be overridden
 * dynamically using method {@link #prestartCoreThread} or {@link
 * #prestartAllCoreThreads}.  You probably want to prestart threads if
 * you construct the pool with a non-empty queue. </dd>
 * <p>
 *     按需构建
 *
 *     默认情况下，即使核心线程最初也只有在新任务到达时才会创建和启动，但可以使用 prestartCoreThread 或 prestartAllCoreThreads 方法动态覆盖这一点。
 *     如果使用非空队列构造池，则可能需要预启动线程。
 *
 * <dt>Creating new threads</dt>
 *
 * <dd>New threads are created using a {@link ThreadFactory}.  If not
 * otherwise specified, a {@link Executors#defaultThreadFactory} is
 * used, that creates threads to all be in the same {@link
 * ThreadGroup} and with the same {@code NORM_PRIORITY} priority and
 * non-daemon status. By supplying a different ThreadFactory, you can
 * alter the thread's name, thread group, priority, daemon status,
 * etc. If a {@code ThreadFactory} fails to create a thread when asked
 * by returning null from {@code newThread}, the executor will
 * continue, but might not be able to execute any tasks. Threads
 * should possess the "modifyThread" {@code RuntimePermission}. If
 * worker threads or other threads using the pool do not possess this
 * permission, service may be degraded: configuration changes may not
 * take effect in a timely manner, and a shutdown pool may remain in a
 * state in which termination is possible but not completed.</dd>
 * <p>
 *     创建新线程
 *
 *     使用 ThreadFactory 创建新线程。
 *     如果没有另外指定，将使用 Executors.defaultThreadFactory，它创建的线程都在同一个 ThreadGroup 中，并具有相同的 NORM_PRIORITY 优先级和非守护进程状态。
 *     通过提供不同的 ThreadFactory，可以更改线程的名称、线程组、优先级、守护进程状态等。
 *     如果 ThreadFactory 在被要求创建线程时返回 null 从 newThread 中失败，则执行程序将继续，但可能无法执行任何任务。
 *     线程应该拥有 "modifyThread" RuntimePermission。
 *     如果使用池的工作线程或其他线程不具备此权限，则服务可能会降级：配置更改可能不会及时生效，并且关闭池可能仍处于终止可能但未完成的状态。
 *
 * <dt>Keep-alive times</dt>
 *
 * <dd>If the pool currently has more than corePoolSize threads,
 * excess threads will be terminated if they have been idle for more
 * than the keepAliveTime (see {@link #getKeepAliveTime(TimeUnit)}).
 * This provides a means of reducing resource consumption when the
 * pool is not being actively used. If the pool becomes more active
 * later, new threads will be constructed. This parameter can also be
 * changed dynamically using method {@link #setKeepAliveTime(long,
 * TimeUnit)}.  Using a value of {@code Long.MAX_VALUE} {@link
 * TimeUnit#NANOSECONDS} effectively disables idle threads from ever
 * terminating prior to shut down. By default, the keep-alive policy
 * applies only when there are more than corePoolSize threads. But
 * method {@link #allowCoreThreadTimeOut(boolean)} can be used to
 * apply this time-out policy to core threads as well, so long as the
 * keepAliveTime value is non-zero. </dd>
 * <p>
 *     保持活动时间
 *
 *     如果池当前的线程数超过 corePoolSize，则如果它们空闲时间超过 keepAliveTime（请参阅 getKeepAliveTime(TimeUnit)），则将终止多余的线程。
 *     当池没有被积极使用时，这提供了减少资源消耗的方法。
 *     如果池稍后变得更活跃，将构建新线程。
 *     这个参数也可以使用 setKeepAliveTime(long, TimeUnit) 方法动态更改。
 *     使用 Long.MAX_VALUE TimeUnit.NANOSECONDS 的值有效地禁用了在关闭之前空闲线程的终止。
 *     默认情况下，只有当线程数超过 corePoolSize 时，保持活动策略才会应用。
 *     但是 allowCoreThreadTimeOut(boolean) 方法可以用于将此超时策略应用于核心线程，只要 keepAliveTime 值不为零。
 *
 * <dt>Queuing</dt>
 *
 * <dd>Any {@link BlockingQueue} may be used to transfer and hold
 * submitted tasks.  The use of this queue interacts with pool sizing:
 *
 * <ul>
 *
 * <li> If fewer than corePoolSize threads are running, the Executor
 * always prefers adding a new thread
 * rather than queuing.</li>
 *
 * <li> If corePoolSize or more threads are running, the Executor
 * always prefers queuing a request rather than adding a new
 * thread.</li>
 *
 * <li> If a request cannot be queued, a new thread is created unless
 * this would exceed maximumPoolSize, in which case, the task will be
 * rejected.</li>
 *
 * </ul>
 * <p>
 *     排队
 *
 *     任何 BlockingQueue 都可以用于传输和保存提交的任务。
 *     使用此队列与池大小交互：
 *     1. 如果运行的线程少于 corePoolSize，则 Executor 总是更喜欢添加一个新线程而不是排队。
 *     2. 如果运行的线程数达到或超过 corePoolSize，则 Executor 总是更喜欢排队请求而不是添加新线程。
 *     3. 如果请求无法排队，则会创建一个新线程，除非这将超过 maximumPoolSize，在这种情况下，任务将被拒绝。
 *
 * There are three general strategies for queuing:
 * <ol>
 *
 * <li> <em> Direct handoffs.</em> A good default choice for a work
 * queue is a {@link SynchronousQueue} that hands off tasks to threads
 * without otherwise holding them. Here, an attempt to queue a task
 * will fail if no threads are immediately available to run it, so a
 * new thread will be constructed. This policy avoids lockups when
 * handling sets of requests that might have internal dependencies.
 * Direct handoffs generally require unbounded maximumPoolSizes to
 * avoid rejection of new submitted tasks. This in turn admits the
 * possibility of unbounded thread growth when commands continue to
 * arrive on average faster than they can be processed.  </li>
 *
 * <li><em> Unbounded queues.</em> Using an unbounded queue (for
 * example a {@link LinkedBlockingQueue} without a predefined
 * capacity) will cause new tasks to wait in the queue when all
 * corePoolSize threads are busy. Thus, no more than corePoolSize
 * threads will ever be created. (And the value of the maximumPoolSize
 * therefore doesn't have any effect.)  This may be appropriate when
 * each task is completely independent of others, so tasks cannot
 * affect each others execution; for example, in a web page server.
 * While this style of queuing can be useful in smoothing out
 * transient bursts of requests, it admits the possibility of
 * unbounded work queue growth when commands continue to arrive on
 * average faster than they can be processed.  </li>
 *
 * <li><em>Bounded queues.</em> A bounded queue (for example, an
 * {@link ArrayBlockingQueue}) helps prevent resource exhaustion when
 * used with finite maximumPoolSizes, but can be more difficult to
 * tune and control.  Queue sizes and maximum pool sizes may be traded
 * off for each other: Using large queues and small pools minimizes
 * CPU usage, OS resources, and context-switching overhead, but can
 * lead to artificially low throughput.  If tasks frequently block (for
 * example if they are I/O bound), a system may be able to schedule
 * time for more threads than you otherwise allow. Use of small queues
 * generally requires larger pool sizes, which keeps CPUs busier but
 * may encounter unacceptable scheduling overhead, which also
 * decreases throughput.  </li>
 *
 * </ol>
 * <p>
 *     有三种一般的排队策略：
 *     1. 直接交接。
 *     一个好的默认工作队列选择是一个 SynchronousQueue，它将任务交给线程而不会保留它们。
 *     在这里，如果没有线程立即可用于运行任务，则尝试排队任务将失败，因此将构造一个新线程。
 *     当处理可能具有内部依赖关系的请求集时，此策略避免了锁定。
 *     直接交接通常需要无界的 maximumPoolSizes，以避免拒绝新提交的任务。
 *     这反过来又允许在命令继续以平均速度到达比可以处理的速度更快时出现无界线程增长的可能性。
 *     2. 无界队列。
 *     使用无界队列（例如没有预定义容量的 LinkedBlockingQueue）将导致新任务在所有 corePoolSize 线程都忙时等待在队列中。
 *     因此，最多只会创建 corePoolSize 个线程。
 *     （因此 maximumPoolSize 的值没有任何效果。）
 *     当每个任务完全独立于其他任务时，这可能是合适的，因此任务不能影响彼此的执行；例如，在 Web 页面服务器中。
 *     虽然这种排队风格可以在平滑瞬时请求突发时有用，但当命令继续以平均速度到达比可以处理的速度更快时，它允许无界工作队列增长的可能性。
 *     3. 有界队列。
 *     有界队列（例如 ArrayBlockingQueue）有助于防止在有限的 maximumPoolSizes 下使用时资源耗尽，但可能更难调整和控制。
 *     队列大小和最大池大小可以相互折衷：使用大队列和小池最小化 CPU 使用率、操作系统资源和上下文切换开销，但可能导致人为低吞吐量。
 *     如果任务经常阻塞（例如，如果它们是 I/O 绑定的），系统可能能够为比您允许的线程更多地安排时间。
 *     使用小队列通常需要更大的池大小，这使 CPU 更忙碌，但可能会遇到不可接受的调度开销，这也会降低吞吐量。
 *
 * </dd>
 *
 * <dt>Rejected tasks</dt>
 *
 * <dd>New tasks submitted in method {@link #execute(Runnable)} will be
 * <em>rejected</em> when the Executor has been shut down, and also when
 * the Executor uses finite bounds for both maximum threads and work queue
 * capacity, and is saturated.  In either case, the {@code execute} method
 * invokes the {@link
 * RejectedExecutionHandler#rejectedExecution(Runnable, ThreadPoolExecutor)}
 * method of its {@link RejectedExecutionHandler}.  Four predefined handler
 * policies are provided:
 *
 * <ol>
 *
 * <li> In the default {@link ThreadPoolExecutor.AbortPolicy}, the
 * handler throws a runtime {@link RejectedExecutionException} upon
 * rejection. </li>
 *
 * <li> In {@link ThreadPoolExecutor.CallerRunsPolicy}, the thread
 * that invokes {@code execute} itself runs the task. This provides a
 * simple feedback control mechanism that will slow down the rate that
 * new tasks are submitted. </li>
 *
 * <li> In {@link ThreadPoolExecutor.DiscardPolicy}, a task that
 * cannot be executed is simply dropped.  </li>
 *
 * <li>In {@link ThreadPoolExecutor.DiscardOldestPolicy}, if the
 * executor is not shut down, the task at the head of the work queue
 * is dropped, and then execution is retried (which can fail again,
 * causing this to be repeated.) </li>
 *
 * </ol>
 *
 * It is possible to define and use other kinds of {@link
 * RejectedExecutionHandler} classes. Doing so requires some care
 * especially when policies are designed to work only under particular
 * capacity or queuing policies. </dd>
 * <p>
 *     拒绝任务
 *
 *     在 execute(Runnable) 方法中提交的新任务将在 Executor 被关闭时被拒绝，当 Executor 对最大线程数和工作队列容量都使用有限边界并且饱和时也会被拒绝。
 *     在任一情况下，execute 方法调用其 RejectedExecutionHandler 的 rejectedExecution(Runnable, ThreadPoolExecutor) 方法。
 *     提供了四种预定义的处理程序策略：
 *     1. 在默认的 ThreadPoolExecutor.AbortPolicy 中，处理程序在拒绝时抛出运行时 RejectedExecutionException。
 *     2. 在 ThreadPoolExecutor.CallerRunsPolicy 中，调用 execute 的线程本身运行任务。这提供了一个简单的反馈控制机制，可以减慢提交新任务的速率。
 *     3. 在 ThreadPoolExecutor.DiscardPolicy 中，无法执行的任务将被简单地丢弃。
 *     4. 在 ThreadPoolExecutor.DiscardOldestPolicy 中，如果执行程序没有关闭，则工作队列头部的任务将被丢弃，然后重试执行（这可能会再次失败，导致重复）。
 *     可以定义和使用其他类型的 RejectedExecutionHandler 类。
 *     这样做需要一些小心，特别是当策略被设计为仅在特定容量或排队策略下工作时。
 *
 * <dt>Hook methods</dt>
 *
 * <dd>This class provides {@code protected} overridable
 * {@link #beforeExecute(Thread, Runnable)} and
 * {@link #afterExecute(Runnable, Throwable)} methods that are called
 * before and after execution of each task.  These can be used to
 * manipulate the execution environment; for example, reinitializing
 * ThreadLocals, gathering statistics, or adding log entries.
 * Additionally, method {@link #terminated} can be overridden to perform
 * any special processing that needs to be done once the Executor has
 * fully terminated.
 *
 * <p>If hook or callback methods throw exceptions, internal worker
 * threads may in turn fail and abruptly terminate.</dd>
 * <p>
 *     钩子方法
 *     这个类提供了 beforeExecute(Thread, Runnable) 和 afterExecute(Runnable, Throwable) 方法，这些方法在每个任务执行之前和之后调用。
 *     这些方法可以用于操作执行环境；例如，重新初始化 ThreadLocals、收集统计信息或添加日志条目。
 *     此外，可以重写 terminated 方法来执行 Executor 完全终止后需要执行的任何特殊处理。
 *     如果钩子或回调方法抛出异常，内部工作线程可能会失败并突然终止。
 *
 * <dt>Queue maintenance</dt>
 *
 * <dd>Method {@link #getQueue()} allows access to the work queue
 * for purposes of monitoring and debugging.  Use of this method for
 * any other purpose is strongly discouraged.  Two supplied methods,
 * {@link #remove(Runnable)} and {@link #purge} are available to
 * assist in storage reclamation when large numbers of queued tasks
 * become cancelled.</dd>
 * <p>
 *     队列维护
 *     getQueue() 方法允许访问工作队列，用于监视和调试。
 *     强烈不建议将此方法用于其他任何目的。
 *     两个提供的方法 remove(Runnable) 和 purge 可用于在大量排队任务被取消时协助存储回收。
 *
 * <dt>Finalization</dt>
 *
 * <dd>A pool that is no longer referenced in a program <em>AND</em>
 * has no remaining threads will be {@code shutdown} automatically. If
 * you would like to ensure that unreferenced pools are reclaimed even
 * if users forget to call {@link #shutdown}, then you must arrange
 * that unused threads eventually die, by setting appropriate
 * keep-alive times, using a lower bound of zero core threads and/or
 * setting {@link #allowCoreThreadTimeOut(boolean)}.  </dd>
 * <p>
 *     终结
 *     在程序中不再引用的池并且没有剩余线程将自动关闭。
 *     如果您希望确保即使用户忘记调用 shutdown，也会回收未引用的池，则必须安排未使用的线程最终死亡，
 *     方法是设置适当的保持活动时间，使用零核心线程的下限和/或设置 allowCoreThreadTimeOut(boolean)。
 *
 * </dl>
 *
 * <p><b>Extension example</b>. Most extensions of this class
 * override one or more of the protected hook methods. For example,
 * here is a subclass that adds a simple pause/resume feature:
 *
 *  <pre> {@code
 * class PausableThreadPoolExecutor extends ThreadPoolExecutor {
 *   private boolean isPaused;
 *   private ReentrantLock pauseLock = new ReentrantLock();
 *   private Condition unpaused = pauseLock.newCondition();
 *
 *   public PausableThreadPoolExecutor(...) { super(...); }
 *
 *   protected void beforeExecute(Thread t, Runnable r) {
 *     super.beforeExecute(t, r);
 *     pauseLock.lock();
 *     try {
 *       while (isPaused) unpaused.await();
 *     } catch (InterruptedException ie) {
 *       t.interrupt();
 *     } finally {
 *       pauseLock.unlock();
 *     }
 *   }
 *
 *   public void pause() {
 *     pauseLock.lock();
 *     try {
 *       isPaused = true;
 *     } finally {
 *       pauseLock.unlock();
 *     }
 *   }
 *
 *   public void resume() {
 *     pauseLock.lock();
 *     try {
 *       isPaused = false;
 *       unpaused.signalAll();
 *     } finally {
 *       pauseLock.unlock();
 *     }
 *   }
 * }}</pre>
 *
 * @since 1.5
 * @author Doug Lea
 */
public class ThreadPoolExecutor extends AbstractExecutorService {
    /**
     * The main pool control state, ctl, is an atomic integer packing
     * two conceptual fields
     *   workerCount, indicating the effective number of threads
     *   runState,    indicating whether running, shutting down etc
     * <p>
     *     主池控制状态 ctl 是一个原子整数，包含两个概念字段
     *     workerCount，表示有效线程数
     *     runState，表示是否正在运行、关闭等
     *
     * In order to pack them into one int, we limit workerCount to
     * (2^29)-1 (about 500 million) threads rather than (2^31)-1 (2
     * billion) otherwise representable. If this is ever an issue in
     * the future, the variable can be changed to be an AtomicLong,
     * and the shift/mask constants below adjusted. But until the need
     * arises, this code is a bit faster and simpler using an int.
     * <p>
     *     为了将它们打包成一个 int，我们将 workerCount 限制为 (2^29)-1（约 5 亿）线程，而不是 (2^31)-1（20 亿）线程，否则可以表示。
     *     如果将来出现问题，该变量可以更改为 AtomicLong，并调整下面的移位/掩码常量。
     *     但在需要之前，使用 int 的代码更快、更简单。
     *
     * The workerCount is the number of workers that have been
     * permitted to start and not permitted to stop.  The value may be
     * transiently different from the actual number of live threads,
     * for example when a ThreadFactory fails to create a thread when
     * asked, and when exiting threads are still performing
     * bookkeeping before terminating. The user-visible pool size is
     * reported as the current size of the workers set.
     * <p>
     *     workerCount 是已被允许启动但不允许停止的工作线程数。
     *     该值可能与实际活动线程数短暂不同，例如当 ThreadFactory 在被要求创建线程时失败时，以及退出线程在终止之前仍在执行簿记。
     *     用户可见的池大小报告为工作集的当前大小。
     *
     * The runState provides the main lifecycle control, taking on values:
     *
     *   RUNNING:  Accept new tasks and process queued tasks
     *   SHUTDOWN: Don't accept new tasks, but process queued tasks
     *   STOP:     Don't accept new tasks, don't process queued tasks,
     *             and interrupt in-progress tasks
     *   TIDYING:  All tasks have terminated, workerCount is zero,
     *             the thread transitioning to state TIDYING
     *             will run the terminated() hook method
     *   TERMINATED: terminated() has completed
     * <p>
     *     runState 提供了主要的生命周期控制，接受以下值：
     *     RUNNING：接受新任务并处理排队的任务
     *     SHUTDOWN：不接受新任务，但处理排队的任务
     *     STOP：不接受新任务，不处理排队的任务，并中断正在进行的任务
     *     TIDYING：所有任务已终止，workerCount 为零，线程转换到状态 TIDYING 将运行 terminated() 钩子方法
     *     TERMINATED：terminated() 已完成
     *
     * The numerical order among these values matters, to allow
     * ordered comparisons. The runState monotonically increases over
     * time, but need not hit each state. The transitions are:
     *
     * RUNNING -> SHUTDOWN
     *    On invocation of shutdown(), perhaps implicitly in finalize()
     * (RUNNING or SHUTDOWN) -> STOP
     *    On invocation of shutdownNow()
     * SHUTDOWN -> TIDYING
     *    When both queue and pool are empty
     * STOP -> TIDYING
     *    When pool is empty
     * TIDYING -> TERMINATED
     *    When the terminated() hook method has completed
     * <p>
     *     这些值之间的数字顺序很重要，以允许有序比较。
     *     runState 随时间单调递增，但不需要达到每个状态。
     *     过渡是：
     *     RUNNING -> SHUTDOWN
     *     在调用 shutdown() 时，可能隐式地在 finalize() 中
     *     (RUNNING or SHUTDOWN) -> STOP
     *     在调用 shutdownNow() 时
     *     SHUTDOWN -> TIDYING
     *     当队列和池都为空时
     *     STOP -> TIDYING
     *     当池为空时
     *     TIDYING -> TERMINATED
     *     当 terminated() 钩子方法已完成时
     *
     * Threads waiting in awaitTermination() will return when the
     * state reaches TERMINATED.
     * <p>
     *     在 awaitTermination() 中等待的线程将在状态达到 TERMINATED 时返回。
     *
     * Detecting the transition from SHUTDOWN to TIDYING is less
     * straightforward than you'd like because the queue may become
     * empty after non-empty and vice versa during SHUTDOWN state, but
     * we can only terminate if, after seeing that it is empty, we see
     * that workerCount is 0 (which sometimes entails a recheck -- see
     * below).
     * <p>
     *     检测从 SHUTDOWN 到 TIDYING 的转换不像您希望的那样直接，因为队列在 SHUTDOWN 状态期间可能在非空和空之间变为空，反之亦然，
     *     但是我们只有在看到它为空后，看到 workerCount 为 0 时才能终止（有时需要重新检查 - 请参见下文）。
     */
    private final AtomicInteger ctl = new AtomicInteger(ctlOf(RUNNING, 0));
    private static final int COUNT_BITS = Integer.SIZE - 3;
    private static final int CAPACITY   = (1 << COUNT_BITS) - 1;

    // runState is stored in the high-order bits
    private static final int RUNNING    = -1 << COUNT_BITS;
    private static final int SHUTDOWN   =  0 << COUNT_BITS;
    private static final int STOP       =  1 << COUNT_BITS;
    private static final int TIDYING    =  2 << COUNT_BITS;
    private static final int TERMINATED =  3 << COUNT_BITS;

    // Packing and unpacking ctl
    private static int runStateOf(int c)     { return c & ~CAPACITY; }
    private static int workerCountOf(int c)  { return c & CAPACITY; }
    private static int ctlOf(int rs, int wc) { return rs | wc; }

    // todo 整体流程：当 addWorker 方法调用后，会添加线程，并直接执行 Worker.run 方法，触发其中的 Thread 执行，它会循环地 take 任务(阻塞)，然后执行任务。
    // 每次执行时，会 tryLock；执行完毕后会 release；
    // 因此可以通过 lockState 判断是否正在执行任务；
    // 执行时，如果通过 submit 方法传入了 Callable，那么在 AbstractExecutorService 中会封装为 FutureTask，并把 FutureTask 传入 execute 方法中
    // 然后会调用 FutureTask 的 run 方法，它重写了 Runnable 的 run 方法，会调用 Callable 的 call 方法，并把结果记录到自己的 result 中

    /*
     * Bit field accessors that don't require unpacking ctl.
     * These depend on the bit layout and on workerCount being never negative.
     */

    private static boolean runStateLessThan(int c, int s) {
        return c < s;
    }

    private static boolean runStateAtLeast(int c, int s) {
        return c >= s;
    }

    private static boolean isRunning(int c) {
        return c < SHUTDOWN;
    }

    /**
     * Attempts to CAS-increment the workerCount field of ctl.
     */
    private boolean compareAndIncrementWorkerCount(int expect) {
        return ctl.compareAndSet(expect, expect + 1);
    }

    /**
     * Attempts to CAS-decrement the workerCount field of ctl.
     */
    private boolean compareAndDecrementWorkerCount(int expect) {
        return ctl.compareAndSet(expect, expect - 1);
    }

    /**
     * Decrements the workerCount field of ctl. This is called only on
     * abrupt termination of a thread (see processWorkerExit). Other
     * decrements are performed within getTask.
     */
    private void decrementWorkerCount() {
        do {} while (! compareAndDecrementWorkerCount(ctl.get()));
    }

    /**
     * The queue used for holding tasks and handing off to worker
     * threads.  We do not require that workQueue.poll() returning
     * null necessarily means that workQueue.isEmpty(), so rely
     * solely on isEmpty to see if the queue is empty (which we must
     * do for example when deciding whether to transition from
     * SHUTDOWN to TIDYING).  This accommodates special-purpose
     * queues such as DelayQueues for which poll() is allowed to
     * return null even if it may later return non-null when delays
     * expire.
     */
    private final BlockingQueue<Runnable> workQueue;

    /**
     * Lock held on access to workers set and related bookkeeping.
     * While we could use a concurrent set of some sort, it turns out
     * to be generally preferable to use a lock. Among the reasons is
     * that this serializes interruptIdleWorkers, which avoids
     * unnecessary interrupt storms, especially during shutdown.
     * Otherwise exiting threads would concurrently interrupt those
     * that have not yet interrupted. It also simplifies some of the
     * associated statistics bookkeeping of largestPoolSize etc. We
     * also hold mainLock on shutdown and shutdownNow, for the sake of
     * ensuring workers set is stable while separately checking
     * permission to interrupt and actually interrupting.
     */
    private final ReentrantLock mainLock = new ReentrantLock();

    /**
     * Set containing all worker threads in pool. Accessed only when
     * holding mainLock.
     */
    private final HashSet<Worker> workers = new HashSet<Worker>();

    /**
     * Wait condition to support awaitTermination
     */
    private final Condition termination = mainLock.newCondition();

    /**
     * Tracks largest attained pool size. Accessed only under
     * mainLock.
     */
    private int largestPoolSize;

    /**
     * Counter for completed tasks. Updated only on termination of
     * worker threads. Accessed only under mainLock.
     */
    private long completedTaskCount;

    /*
     * All user control parameters are declared as volatiles so that
     * ongoing actions are based on freshest values, but without need
     * for locking, since no internal invariants depend on them
     * changing synchronously with respect to other actions.
     */

    /**
     * Factory for new threads. All threads are created using this
     * factory (via method addWorker).  All callers must be prepared
     * for addWorker to fail, which may reflect a system or user's
     * policy limiting the number of threads.  Even though it is not
     * treated as an error, failure to create threads may result in
     * new tasks being rejected or existing ones remaining stuck in
     * the queue.
     * <p>
     *     用于创建新线程的工厂。
     *     所有线程都是使用此工厂创建的（通过 addWorker 方法）。
     *     所有调用者必须准备好 addWorker 失败，这可能反映了系统或用户的策略限制线程数。
     *     即使它不被视为错误，创建线程失败可能导致新任务被拒绝或现有任务仍然停留在队列中。
     *
     * We go further and preserve pool invariants even in the face of
     * errors such as OutOfMemoryError, that might be thrown while
     * trying to create threads.  Such errors are rather common due to
     * the need to allocate a native stack in Thread.start, and users
     * will want to perform clean pool shutdown to clean up.  There
     * will likely be enough memory available for the cleanup code to
     * complete without encountering yet another OutOfMemoryError.
     * <p>
     *     我们进一步保持池的不变性，即使面对诸如 OutOfMemoryError 之类的错误，这些错误可能在尝试创建线程时抛出。
     *     由于需要在 Thread.start 中分配本机堆栈，这些错误相当常见，用户将希望执行干净的池关闭以清理。
     *     可能有足够的内存可用于清理代码完成，而不会遇到另一个 OutOfMemoryError。
     */
    private volatile ThreadFactory threadFactory;

    /**
     * Handler called when saturated or shutdown in execute.
     * <p>
     *     在 execute 中饱和或关闭时调用的处理程序。
     */
    private volatile RejectedExecutionHandler handler;

    /**
     * Timeout in nanoseconds for idle threads waiting for work.
     * Threads use this timeout when there are more than corePoolSize
     * present or if allowCoreThreadTimeOut. Otherwise they wait
     * forever for new work.
     * <p>
     *     等待工作的空闲线程的超时时间（以纳秒为单位）。
     *     当存在多于 corePoolSize 或 allowCoreThreadTimeOut 时，线程使用此超时。
     *     否则，它们将永远等待新工作。
     */
    private volatile long keepAliveTime;

    /**
     * If false (default), core threads stay alive even when idle.
     * If true, core threads use keepAliveTime to time out waiting
     * for work.
     * <p>
     *     如果为 false（默认值），即使空闲，核心线程也保持活动。
     *     如果为 true，核心线程使用 keepAliveTime 超时等待工作。
     */
    private volatile boolean allowCoreThreadTimeOut;

    /**
     * Core pool size is the minimum number of workers to keep alive
     * (and not allow to time out etc) unless allowCoreThreadTimeOut
     * is set, in which case the minimum is zero.
     * <p>
     *     核心池大小是保持活动的最小工作线程数（不允许超时等等），除非设置了 allowCoreThreadTimeOut，在这种情况下，最小值为零。
     */
    private volatile int corePoolSize;

    /**
     * Maximum pool size. Note that the actual maximum is internally
     * bounded by CAPACITY.
     * <p>
     *     最大池大小。
     *     请注意，实际最大值在内部由 CAPACITY 限制。
     */
    private volatile int maximumPoolSize;

    /**
     * The default rejected execution handler
     * <p>
     *     默认的拒绝执行处理程序
     */
    private static final RejectedExecutionHandler defaultHandler =
        new AbortPolicy();

    /**
     * Permission required for callers of shutdown and shutdownNow.
     * We additionally require (see checkShutdownAccess) that callers
     * have permission to actually interrupt threads in the worker set
     * (as governed by Thread.interrupt, which relies on
     * ThreadGroup.checkAccess, which in turn relies on
     * SecurityManager.checkAccess). Shutdowns are attempted only if
     * these checks pass.
     *
     * All actual invocations of Thread.interrupt (see
     * interruptIdleWorkers and interruptWorkers) ignore
     * SecurityExceptions, meaning that the attempted interrupts
     * silently fail. In the case of shutdown, they should not fail
     * unless the SecurityManager has inconsistent policies, sometimes
     * allowing access to a thread and sometimes not. In such cases,
     * failure to actually interrupt threads may disable or delay full
     * termination. Other uses of interruptIdleWorkers are advisory,
     * and failure to actually interrupt will merely delay response to
     * configuration changes so is not handled exceptionally.
     */
    private static final RuntimePermission shutdownPerm =
        new RuntimePermission("modifyThread");

    /* The context to be used when executing the finalizer, or null. */
    private final AccessControlContext acc;

    /**
     * Class Worker mainly maintains interrupt control state for
     * threads running tasks, along with other minor bookkeeping.
     * This class opportunistically extends AbstractQueuedSynchronizer
     * to simplify acquiring and releasing a lock surrounding each
     * task execution.  This protects against interrupts that are
     * intended to wake up a worker thread waiting for a task from
     * instead interrupting a task being run.  We implement a simple
     * non-reentrant mutual exclusion lock rather than use
     * ReentrantLock because we do not want worker tasks to be able to
     * reacquire the lock when they invoke pool control methods like
     * setCorePoolSize.  Additionally, to suppress interrupts until
     * the thread actually starts running tasks, we initialize lock
     * state to a negative value, and clear it upon start (in
     * runWorker).
     * <p>
     *     Worker 类主要维护运行任务的线程的中断控制状态，以及其他次要簿记。
     *     这个类机会地扩展 AbstractQueuedSynchronizer，以简化获取和释放围绕每个任务执行的锁。
     *     这可以防止打算唤醒等待任务的工作线程的中断，而不是中断正在运行的任务。
     *     我们实现了一个简单的非可重入互斥锁，而不是使用 ReentrantLock，因为我们不希望工作任务能够在调用池控制方法（如 setCorePoolSize）时重新获取锁。
     *     此外，为了抑制中断直到线程实际开始运行任务，我们将锁状态初始化为负值，并在启动时清除它（在 runWorker 中）。
     */
    private final class Worker
        extends AbstractQueuedSynchronizer
        implements Runnable
    {
        /**
         * This class will never be serialized, but we provide a
         * serialVersionUID to suppress a javac warning.
         */
        private static final long serialVersionUID = 6138294804551838833L;

        /**
         * Thread this worker is running in.  Null if factory fails.
         * <p>
         *     此工作线程正在运行的线程。
         *     如果工厂失败，则为 null。
         */
        final Thread thread;

        /**
         * Initial task to run.  Possibly null.
         * <p>
         *     要运行的初始任务。
         *     可能为 null。
         */
        Runnable firstTask;

        /**
         * Per-thread task counter
         * <p>
         *     每个线程的任务计数器
         */
        volatile long completedTasks;

        /**
         * Creates with given first task and thread from ThreadFactory.
         * <p>
         *     使用给定的第一个任务和线程从 ThreadFactory 创建。
         *
         * @param firstTask the first task (null if none)
         */
        Worker(Runnable firstTask) {
            setState(-1); // inhibit interrupts until runWorker
            this.firstTask = firstTask;
            // 调用 getThreadFactory() 方法获取线程工厂创建线程
            this.thread = getThreadFactory().newThread(this);
        }

        /**
         * Delegates main run loop to outer runWorker
         * <p>
         *     将主运行循环委托给外部 runWorker
         */
        public void run() {
            runWorker(this);
        }

        // Lock methods
        //
        // The value 0 represents the unlocked state.
        // The value 1 represents the locked state.

        protected boolean isHeldExclusively() {
            return getState() != 0;
        }

        protected boolean tryAcquire(int unused) {
            if (compareAndSetState(0, 1)) {
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        protected boolean tryRelease(int unused) {
            setExclusiveOwnerThread(null);
            setState(0);
            return true;
        }

        public void lock()        { acquire(1); }
        public boolean tryLock()  { return tryAcquire(1); }
        public void unlock()      { release(1); }
        public boolean isLocked() { return isHeldExclusively(); }

        void interruptIfStarted() {
            Thread t;
            if (getState() >= 0 && (t = thread) != null && !t.isInterrupted()) {
                try {
                    t.interrupt();
                } catch (SecurityException ignore) {
                }
            }
        }
    }

    /*
     * Methods for setting control state
     */

    /**
     * Transitions runState to given target, or leaves it alone if
     * already at least the given target.
     *
     * @param targetState the desired state, either SHUTDOWN or STOP
     *        (but not TIDYING or TERMINATED -- use tryTerminate for that)
     */
    private void advanceRunState(int targetState) {
        for (;;) {
            int c = ctl.get();
            if (runStateAtLeast(c, targetState) ||
                ctl.compareAndSet(c, ctlOf(targetState, workerCountOf(c))))
                break;
        }
    }

    /**
     * Transitions to TERMINATED state if either (SHUTDOWN and pool
     * and queue empty) or (STOP and pool empty).  If otherwise
     * eligible to terminate but workerCount is nonzero, interrupts an
     * idle worker to ensure that shutdown signals propagate. This
     * method must be called following any action that might make
     * termination possible -- reducing worker count or removing tasks
     * from the queue during shutdown. The method is non-private to
     * allow access from ScheduledThreadPoolExecutor.
     * <p>
     *     如果（SHUTDOWN 和池和队列为空）或（STOP 和池为空），则转换为 TERMINATED 状态。
     *     如果否则有资格终止但 workerCount 不为零，则中断空闲工作线程以确保关闭信号传播。
     *     必须在可能使终止成为可能的任何操作之后调用此方法 - 在关闭期间减少工作线程数或从队列中删除任务。
     *     该方法是非私有的，以允许从 ScheduledThreadPoolExecutor 访问。
     */
    final void tryTerminate() {
        for (;;) {
            // 获取当前 ctl
            int c = ctl.get();
            // 如果 ctl 表示当前线程池处于 RUNNING 状态，
            // 或者线程池处于 TIDYING 或 TERMINATED 状态
            // 或者线程池处于 SHUTDOWN 状态并且队列不为空
            // 则直接返回，不做任何操作
            if (isRunning(c) ||
                runStateAtLeast(c, TIDYING) ||
                (runStateOf(c) == SHUTDOWN && ! workQueue.isEmpty()))
                return;
            // 如果当前线程数量不为 0
            if (workerCountOf(c) != 0) { // Eligible to terminate
                // 中断一个线程
                interruptIdleWorkers(ONLY_ONE);
                return;
            }

            // 加全局锁
            final ReentrantLock mainLock = this.mainLock;
            mainLock.lock();
            try {
                // cas 更新 ctl 为 TIDYING
                if (ctl.compareAndSet(c, ctlOf(TIDYING, 0))) {
                    try {
                        // 调用模板 terminated 方法
                        terminated();
                    } finally {
                        // cas 更新 ctl 为 TERMINATED
                        ctl.set(ctlOf(TERMINATED, 0));
                        // 唤醒所有等待线程
                        termination.signalAll();
                    }
                    return;
                }
            } finally {
                mainLock.unlock();
            }
            // else retry on failed CAS
        }
    }

    /*
     * Methods for controlling interrupts to worker threads.
     */

    /**
     * If there is a security manager, makes sure caller has
     * permission to shut down threads in general (see shutdownPerm).
     * If this passes, additionally makes sure the caller is allowed
     * to interrupt each worker thread. This might not be true even if
     * first check passed, if the SecurityManager treats some threads
     * specially.
     */
    private void checkShutdownAccess() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(shutdownPerm);
            final ReentrantLock mainLock = this.mainLock;
            mainLock.lock();
            try {
                for (Worker w : workers)
                    security.checkAccess(w.thread);
            } finally {
                mainLock.unlock();
            }
        }
    }

    /**
     * Interrupts all threads, even if active. Ignores SecurityExceptions
     * (in which case some threads may remain uninterrupted).
     * <p>
     *     中断所有线程，即使是活动的。
     *     忽略 SecurityExceptions（在这种情况下，某些线程可能保持未中断状态）。
     */
    private void interruptWorkers() {
        // 加锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 中止所有工作线程
            for (Worker w : workers)
                w.interruptIfStarted();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Interrupts threads that might be waiting for tasks (as
     * indicated by not being locked) so they can check for
     * termination or configuration changes. Ignores
     * SecurityExceptions (in which case some threads may remain
     * uninterrupted).
     * <p>
     *     中断可能正在等待任务的线程（如未锁定所示），以便它们可以检查终止或配置更改。
     *     忽略 SecurityExceptions（在这种情况下，某些线程可能保持未中断状态）。
     *
     * @param onlyOne If true, interrupt at most one worker. This is
     * called only from tryTerminate when termination is otherwise
     * enabled but there are still other workers.  In this case, at
     * most one waiting worker is interrupted to propagate shutdown
     * signals in case all threads are currently waiting.
     * Interrupting any arbitrary thread ensures that newly arriving
     * workers since shutdown began will also eventually exit.
     * To guarantee eventual termination, it suffices to always
     * interrupt only one idle worker, but shutdown() interrupts all
     * idle workers so that redundant workers exit promptly, not
     * waiting for a straggler task to finish.
     * <p>
     * 如果为 true，则最多中断一个工作线程。
     * 仅在终止否则已启用但仍有其他工作线程时，才从 tryTerminate 调用。
     * 在这种情况下，最多中断一个等待的工作线程以传播关闭信号，以防所有线程当前都在等待。
     * 中断任意线程可确保自关闭开始以来新到达的工作线程最终也会退出。
     * 为了保证最终终止，只需中断一个空闲工作线程即可，但 shutdown() 中断所有空闲工作线程，以便多余的工作线程迅速退出，而不必等待落后的任务完成。
     */
    private void interruptIdleWorkers(boolean onlyOne) {
        // 加锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 遍历所有工作线程
            for (Worker w : workers) {
                Thread t = w.thread;
                // 中断线程
                if (!t.isInterrupted() && w.tryLock()) {
                    try {
                        t.interrupt();
                    } catch (SecurityException ignore) {
                    } finally {
                        w.unlock();
                    }
                }
                // 如果 onlyOne 为 true，代表只中断一个线程
                if (onlyOne)
                    break;
            }
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Common form of interruptIdleWorkers, to avoid having to
     * remember what the boolean argument means.
     * <p>
     *     中断空闲工作线程的常见形式，以避免记住布尔参数的含义。
     */
    private void interruptIdleWorkers() {
        interruptIdleWorkers(false);
    }

    private static final boolean ONLY_ONE = true;

    /*
     * Misc utilities, most of which are also exported to
     * ScheduledThreadPoolExecutor
     */

    /**
     * Invokes the rejected execution handler for the given command.
     * Package-protected for use by ScheduledThreadPoolExecutor.
     */
    final void reject(Runnable command) {
        handler.rejectedExecution(command, this);
    }

    /**
     * Performs any further cleanup following run state transition on
     * invocation of shutdown.  A no-op here, but used by
     * ScheduledThreadPoolExecutor to cancel delayed tasks.
     * <p>
     *     在调用 shutdown 后执行运行状态转换后的任何进一步清理。
     *     这里是一个空操作，但由 ScheduledThreadPoolExecutor 用于取消延迟任务。
     */
    void onShutdown() {
    }

    /**
     * State check needed by ScheduledThreadPoolExecutor to
     * enable running tasks during shutdown.
     *
     * @param shutdownOK true if should return true if SHUTDOWN
     */
    final boolean isRunningOrShutdown(boolean shutdownOK) {
        int rs = runStateOf(ctl.get());
        return rs == RUNNING || (rs == SHUTDOWN && shutdownOK);
    }

    /**
     * Drains the task queue into a new list, normally using
     * drainTo. But if the queue is a DelayQueue or any other kind of
     * queue for which poll or drainTo may fail to remove some
     * elements, it deletes them one by one.
     */
    private List<Runnable> drainQueue() {
        BlockingQueue<Runnable> q = workQueue;
        ArrayList<Runnable> taskList = new ArrayList<Runnable>();
        q.drainTo(taskList);
        if (!q.isEmpty()) {
            for (Runnable r : q.toArray(new Runnable[0])) {
                if (q.remove(r))
                    taskList.add(r);
            }
        }
        return taskList;
    }

    /*
     * Methods for creating, running and cleaning up after workers
     */

    /**
     * Checks if a new worker can be added with respect to current
     * pool state and the given bound (either core or maximum). If so,
     * the worker count is adjusted accordingly, and, if possible, a
     * new worker is created and started, running firstTask as its
     * first task. This method returns false if the pool is stopped or
     * eligible to shut down. It also returns false if the thread
     * factory fails to create a thread when asked.  If the thread
     * creation fails, either due to the thread factory returning
     * null, or due to an exception (typically OutOfMemoryError in
     * Thread.start()), we roll back cleanly.
     * <p>
     *     检查是否可以根据当前池状态和给定边界（核心或最大）添加新工作线程。
     *     如果可以，则相应地调整工作线程计数，并且如果可能，则创建并启动新工作线程，将 firstTask 作为其第一个任务运行。
     *     如果池已停止或有资格关闭，则此方法返回 false。
     *     如果线程工厂在被要求时无法创建线程，则此方法还将返回 false。
     *     如果线程创建失败，要么是因为线程工厂返回 null，要么是因为出现异常（通常是 Thread.start() 中的 OutOfMemoryError），我们将干净地回滚。
     *
     * @param firstTask the task the new thread should run first (or
     * null if none). Workers are created with an initial first task
     * (in method execute()) to bypass queuing when there are fewer
     * than corePoolSize threads (in which case we always start one),
     * or when the queue is full (in which case we must bypass queue).
     * Initially idle threads are usually created via
     * prestartCoreThread or to replace other dying workers.
     *
     * @param core if true use corePoolSize as bound, else
     * maximumPoolSize. (A boolean indicator is used here rather than a
     * value to ensure reads of fresh values after checking other pool
     * state).
     * @return true if successful
     */
    private boolean addWorker(Runnable firstTask, boolean core) {
        // 持续的循环，判断和添加新线程
        retry:
        for (;;) {
            // 获取 ctl 并解析出当前线程池状态和线程数量
            int c = ctl.get();
            int rs = runStateOf(c);

            // Check if queue empty only if necessary.
            // 根据状态判断是否可以添加新线程
            if (rs >= SHUTDOWN &&
                ! (rs == SHUTDOWN &&
                   firstTask == null &&
                   ! workQueue.isEmpty()))
                return false;

            for (;;) {
                // 如果当前的工作线程数量已经超标，则返回 false
                int wc = workerCountOf(c);
                if (wc >= CAPACITY ||
                    wc >= (core ? corePoolSize : maximumPoolSize))
                    return false;
                // cas 更新 ctl，增加工作线程数量；如果成功了则跳出大循环
                if (compareAndIncrementWorkerCount(c))
                    break retry;
                c = ctl.get();  // Re-read ctl
                // 否则判断当前线程池状态是否发生了变化，如果发生了变化则重复大循环
                if (runStateOf(c) != rs)
                    continue retry;
                // else CAS failed due to workerCount change; retry inner loop
            }
        }
        // 记录标志位
        boolean workerStarted = false;
        boolean workerAdded = false;
        Worker w = null;
        try {
            // 封装 worker 对象
            w = new Worker(firstTask);
            final Thread t = w.thread;
            if (t != null) {
                // 加全局锁
                final ReentrantLock mainLock = this.mainLock;
                mainLock.lock();
                try {
                    // Recheck while holding lock.
                    // Back out on ThreadFactory failure or if
                    // shut down before lock acquired.
                    int rs = runStateOf(ctl.get());

                    if (rs < SHUTDOWN ||
                        (rs == SHUTDOWN && firstTask == null)) {
                        if (t.isAlive()) // precheck that t is startable
                            throw new IllegalThreadStateException();
                        // 添加到 workers 集合中
                        workers.add(w);
                        int s = workers.size();
                        if (s > largestPoolSize)
                            largestPoolSize = s;
                        workerAdded = true;
                    }
                } finally {
                    mainLock.unlock();
                }
                // 如果添加成功，则启动线程
                if (workerAdded) {
                    t.start();
                    workerStarted = true;
                }
            }
        } finally {
            if (! workerStarted)
                addWorkerFailed(w);
        }
        return workerStarted;
    }

    /**
     * Rolls back the worker thread creation.
     * - removes worker from workers, if present
     * - decrements worker count
     * - rechecks for termination, in case the existence of this
     *   worker was holding up termination
     * <p>
     *     回滚工作线程创建。
     *     - 如果存在，则从 workers 中删除工作线程
     *     - 减少工作线程计数
     */
    private void addWorkerFailed(Worker w) {
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            if (w != null)
                workers.remove(w);
            decrementWorkerCount();
            tryTerminate();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Performs cleanup and bookkeeping for a dying worker. Called
     * only from worker threads. Unless completedAbruptly is set,
     * assumes that workerCount has already been adjusted to account
     * for exit.  This method removes thread from worker set, and
     * possibly terminates the pool or replaces the worker if either
     * it exited due to user task exception or if fewer than
     * corePoolSize workers are running or queue is non-empty but
     * there are no workers.
     * <p>
     *     为垂死的工作线程执行清理和簿记。
     *     仅从工作线程调用。
     *     除非设置了 completedAbruptly，否则假定 workerCount 已经调整为考虑退出。
     *     此方法从工作线程集中删除线程，并且如果线程由于用户任务异常退出，或者运行的工作线程少于 corePoolSize 或队列非空但没有工作线程，则可能终止池或替换工作线程。
     *
     * @param w the worker
     * @param completedAbruptly if the worker died due to user exception
     */
    private void processWorkerExit(Worker w, boolean completedAbruptly) {
        // 如果是因为异常退出，则需要调整工作线程数量
        if (completedAbruptly) // If abrupt, then workerCount wasn't adjusted
            decrementWorkerCount();

        // 加全局锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 更新任务完成总数，并移除该工作线程
            completedTaskCount += w.completedTasks;
            workers.remove(w);
        } finally {
            mainLock.unlock();
        }

        tryTerminate();

        int c = ctl.get();
        // 补充工作线程
        if (runStateLessThan(c, STOP)) {
            if (!completedAbruptly) {
                int min = allowCoreThreadTimeOut ? 0 : corePoolSize;
                if (min == 0 && ! workQueue.isEmpty())
                    min = 1;
                if (workerCountOf(c) >= min)
                    return; // replacement not needed
            }
            addWorker(null, false);
        }
    }

    /**
     * Performs blocking or timed wait for a task, depending on
     * current configuration settings, or returns null if this worker
     * must exit because of any of:
     * 1. There are more than maximumPoolSize workers (due to
     *    a call to setMaximumPoolSize).
     * 2. The pool is stopped.
     * 3. The pool is shutdown and the queue is empty.
     * 4. This worker timed out waiting for a task, and timed-out
     *    workers are subject to termination (that is,
     *    {@code allowCoreThreadTimeOut || workerCount > corePoolSize})
     *    both before and after the timed wait, and if the queue is
     *    non-empty, this worker is not the last thread in the pool.
     * <p>
     *     根据当前配置设置执行阻塞或定时等待任务，或者如果工作线程必须退出，则返回 null，因为：
     *     1. 有超过 maximumPoolSize 个工作线程（由于调用 setMaximumPoolSize）。
     *     2. 池已停止。
     *     3. 池已关闭并且队列为空。
     *     4. 此工作线程等待任务超时，并且超时的工作线程有资格终止（即 allowCoreThreadTimeOut || workerCount > corePoolSize）在定时等待之前和之后，并且如果队列非空，则此工作线程不是池中的最后一个线程。
     *
     * @return task, or null if the worker must exit, in which case
     *         workerCount is decremented
     */
    private Runnable getTask() {
        boolean timedOut = false; // Did the last poll() time out?

        // 持续循环
        for (;;) {
            // 获取线程池的运行状态
            int c = ctl.get();
            int rs = runStateOf(c);

            // Check if queue empty only if necessary.
            // 如果线程池已经关闭，并且队列为空，则减少线程数并返回 null
            if (rs >= SHUTDOWN && (rs >= STOP || workQueue.isEmpty())) {
                decrementWorkerCount();
                return null;
            }

            // 统计现在的工作线程数量
            int wc = workerCountOf(c);

            // Are workers subject to culling?
            // 是否需要削减工作线程
            boolean timed = allowCoreThreadTimeOut || wc > corePoolSize;

            // 如果（工作线程数量大于最大线程数 || （超时 && 超时了））则减少线程数并返回 null
            if ((wc > maximumPoolSize || (timed && timedOut))
                && (wc > 1 || workQueue.isEmpty())) {
                if (compareAndDecrementWorkerCount(c))
                    return null;
                continue;
            }

            try {
                // 从队列中获取任务并返回
                Runnable r = timed ?
                    workQueue.poll(keepAliveTime, TimeUnit.NANOSECONDS) :
                    workQueue.take();
                if (r != null)
                    return r;
                timedOut = true;
            } catch (InterruptedException retry) {
                timedOut = false;
            }
        }
    }

    /**
     * Main worker run loop.  Repeatedly gets tasks from queue and
     * executes them, while coping with a number of issues:
     * <p>
     *     主工作线程运行循环。
     *     重复从队列中获取任务并执行它们，同时处理许多问题：
     *
     * 1. We may start out with an initial task, in which case we
     * don't need to get the first one. Otherwise, as long as pool is
     * running, we get tasks from getTask. If it returns null then the
     * worker exits due to changed pool state or configuration
     * parameters.  Other exits result from exception throws in
     * external code, in which case completedAbruptly holds, which
     * usually leads processWorkerExit to replace this thread.
     * <p>
     *     1. 我们可能从初始任务开始，在这种情况下，我们不需要获取第一个任务。
     *     否则，只要池正在运行，我们就从 getTask 获取任务。
     *     如果它返回 null，则由于更改的池状态或配置参数，工作线程退出。
     *     其他退出是由于外部代码中的异常抛出，这种情况下 completedAbruptly 保持不变，通常会导致 processWorkerExit 替换此线程。
     *
     * 2. Before running any task, the lock is acquired to prevent
     * other pool interrupts while the task is executing, and then we
     * ensure that unless pool is stopping, this thread does not have
     * its interrupt set.
     * <p>
     *     2. 在运行任何任务之前，获取锁以防止任务执行时发生其他池中断，然后确保除非池正在停止，否则此线程不会设置中断。
     *
     * 3. Each task run is preceded by a call to beforeExecute, which
     * might throw an exception, in which case we cause thread to die
     * (breaking loop with completedAbruptly true) without processing
     * the task.
     * <p>
     *     3. 每个任务运行之前都会调用 beforeExecute，这可能会抛出异常，如果是这种情况，我们会导致线程死亡（将循环中断 completedAbruptly 设置为 true），而不处理任务。
     *
     * 4. Assuming beforeExecute completes normally, we run the task,
     * gathering any of its thrown exceptions to send to afterExecute.
     * We separately handle RuntimeException, Error (both of which the
     * specs guarantee that we trap) and arbitrary Throwables.
     * Because we cannot rethrow Throwables within Runnable.run, we
     * wrap them within Errors on the way out (to the thread's
     * UncaughtExceptionHandler).  Any thrown exception also
     * conservatively causes thread to die.
     * <p>
     *     4. 假设 beforeExecute 正常完成，我们运行任务，收集任何抛出的异常以发送给 afterExecute。
     *     我们分别处理 RuntimeException、Error（规范保证我们捕获这两个异常）和任意 Throwables。
     *     因为我们无法在 Runnable.run 中重新抛出 Throwables，所以我们在退出时（到线程的 UncaughtExceptionHandler）将它们包装在 Errors 中。
     *     任何抛出的异常也会保守地导致线程死亡。
     *
     * 5. After task.run completes, we call afterExecute, which may
     * also throw an exception, which will also cause thread to
     * die. According to JLS Sec 14.20, this exception is the one that
     * will be in effect even if task.run throws.
     * <p>
     *     5. 在 task.run 完成后，我们调用 afterExecute，这也可能会抛出异常，这也会导致线程死亡。
     *     根据 JLS Sec 14.20，这个异常是即使 task.run 抛出异常也会生效的异常。
     *
     * The net effect of the exception mechanics is that afterExecute
     * and the thread's UncaughtExceptionHandler have as accurate
     * information as we can provide about any problems encountered by
     * user code.
     * <p>
     *     异常机制的净效果是，afterExecute 和线程的 UncaughtExceptionHandler 有关于用户代码遇到的任何问题的尽可能准确的信息。
     *
     * @param w the worker
     */
    final void runWorker(Worker w) {
        // 获取当前线程
        Thread wt = Thread.currentThread();
        // 获取 Worker 指定的第一个任务并清空
        Runnable task = w.firstTask;
        w.firstTask = null;
        // Worker 取消锁定状态，允许中断
        w.unlock(); // allow interrupts
        boolean completedAbruptly = true;
        try {
            // 如果 task 为 null，则从队列中获取任务
            while (task != null || (task = getTask()) != null) {
                // 获取到任务之后，worker 加锁
                w.lock();
                // If pool is stopping, ensure thread is interrupted;
                // if not, ensure thread is not interrupted.  This
                // requires a recheck in second case to deal with
                // shutdownNow race while clearing interrupt
                // 如果线程池正在停止，则确保线程被中断；
                // 如果没有，则确保线程没有被中断。
                // 在第二种情况下需要重新检查，以处理清除中断时的 shutdownNow 竞争
                if ((runStateAtLeast(ctl.get(), STOP) ||
                     (Thread.interrupted() &&
                      runStateAtLeast(ctl.get(), STOP))) &&
                    !wt.isInterrupted())
                    wt.interrupt();
                try {
                    // 执行模板方法
                    beforeExecute(wt, task);
                    // 预留异常信息位置
                    Throwable thrown = null;
                    try {
                        // 执行任务
                        task.run();

                        // 存储异常信息
                    } catch (RuntimeException x) {
                        thrown = x; throw x;
                    } catch (Error x) {
                        thrown = x; throw x;
                    } catch (Throwable x) {
                        thrown = x; throw new Error(x);
                    } finally {
                        // 执行模板方法
                        afterExecute(task, thrown);
                    }
                } finally {
                    // 任务执行完成，清空任务，worker 解锁
                    task = null;
                    w.completedTasks++;
                    w.unlock();
                }
            }
            completedAbruptly = false;
        } finally {
            processWorkerExit(w, completedAbruptly);
        }
    }

    // Public constructors and methods

    /**
     * Creates a new {@code ThreadPoolExecutor} with the given initial
     * parameters and default thread factory and rejected execution handler.
     * It may be more convenient to use one of the {@link Executors} factory
     * methods instead of this general purpose constructor.
     *
     * @param corePoolSize the number of threads to keep in the pool, even
     *        if they are idle, unless {@code allowCoreThreadTimeOut} is set
     * @param maximumPoolSize the maximum number of threads to allow in the
     *        pool
     * @param keepAliveTime when the number of threads is greater than
     *        the core, this is the maximum time that excess idle threads
     *        will wait for new tasks before terminating.
     * @param unit the time unit for the {@code keepAliveTime} argument
     * @param workQueue the queue to use for holding tasks before they are
     *        executed.  This queue will hold only the {@code Runnable}
     *        tasks submitted by the {@code execute} method.
     * @throws IllegalArgumentException if one of the following holds:<br>
     *         {@code corePoolSize < 0}<br>
     *         {@code keepAliveTime < 0}<br>
     *         {@code maximumPoolSize <= 0}<br>
     *         {@code maximumPoolSize < corePoolSize}
     * @throws NullPointerException if {@code workQueue} is null
     */
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             Executors.defaultThreadFactory(), defaultHandler);
    }

    /**
     * Creates a new {@code ThreadPoolExecutor} with the given initial
     * parameters and default rejected execution handler.
     *
     * @param corePoolSize the number of threads to keep in the pool, even
     *        if they are idle, unless {@code allowCoreThreadTimeOut} is set
     * @param maximumPoolSize the maximum number of threads to allow in the
     *        pool
     * @param keepAliveTime when the number of threads is greater than
     *        the core, this is the maximum time that excess idle threads
     *        will wait for new tasks before terminating.
     * @param unit the time unit for the {@code keepAliveTime} argument
     * @param workQueue the queue to use for holding tasks before they are
     *        executed.  This queue will hold only the {@code Runnable}
     *        tasks submitted by the {@code execute} method.
     * @param threadFactory the factory to use when the executor
     *        creates a new thread
     * @throws IllegalArgumentException if one of the following holds:<br>
     *         {@code corePoolSize < 0}<br>
     *         {@code keepAliveTime < 0}<br>
     *         {@code maximumPoolSize <= 0}<br>
     *         {@code maximumPoolSize < corePoolSize}
     * @throws NullPointerException if {@code workQueue}
     *         or {@code threadFactory} is null
     */
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             threadFactory, defaultHandler);
    }

    /**
     * Creates a new {@code ThreadPoolExecutor} with the given initial
     * parameters and default thread factory.
     *
     * @param corePoolSize the number of threads to keep in the pool, even
     *        if they are idle, unless {@code allowCoreThreadTimeOut} is set
     * @param maximumPoolSize the maximum number of threads to allow in the
     *        pool
     * @param keepAliveTime when the number of threads is greater than
     *        the core, this is the maximum time that excess idle threads
     *        will wait for new tasks before terminating.
     * @param unit the time unit for the {@code keepAliveTime} argument
     * @param workQueue the queue to use for holding tasks before they are
     *        executed.  This queue will hold only the {@code Runnable}
     *        tasks submitted by the {@code execute} method.
     * @param handler the handler to use when execution is blocked
     *        because the thread bounds and queue capacities are reached
     * @throws IllegalArgumentException if one of the following holds:<br>
     *         {@code corePoolSize < 0}<br>
     *         {@code keepAliveTime < 0}<br>
     *         {@code maximumPoolSize <= 0}<br>
     *         {@code maximumPoolSize < corePoolSize}
     * @throws NullPointerException if {@code workQueue}
     *         or {@code handler} is null
     */
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              RejectedExecutionHandler handler) {
        this(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
             Executors.defaultThreadFactory(), handler);
    }

    /**
     * Creates a new {@code ThreadPoolExecutor} with the given initial
     * parameters.
     * <p>
     *     使用给定的初始参数创建一个新的 ThreadPoolExecutor。
     *
     * @param corePoolSize the number of threads to keep in the pool, even
     *        if they are idle, unless {@code allowCoreThreadTimeOut} is set
     * @param maximumPoolSize the maximum number of threads to allow in the
     *        pool
     * @param keepAliveTime when the number of threads is greater than
     *        the core, this is the maximum time that excess idle threads
     *        will wait for new tasks before terminating.
     * @param unit the time unit for the {@code keepAliveTime} argument
     * @param workQueue the queue to use for holding tasks before they are
     *        executed.  This queue will hold only the {@code Runnable}
     *        tasks submitted by the {@code execute} method.
     * @param threadFactory the factory to use when the executor
     *        creates a new thread
     * @param handler the handler to use when execution is blocked
     *        because the thread bounds and queue capacities are reached
     * @throws IllegalArgumentException if one of the following holds:<br>
     *         {@code corePoolSize < 0}<br>
     *         {@code keepAliveTime < 0}<br>
     *         {@code maximumPoolSize <= 0}<br>
     *         {@code maximumPoolSize < corePoolSize}
     * @throws NullPointerException if {@code workQueue}
     *         or {@code threadFactory} or {@code handler} is null
     */
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) {
        // 入参校验
        if (corePoolSize < 0 ||
            maximumPoolSize <= 0 ||
            maximumPoolSize < corePoolSize ||
            keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        // 记录安全上下文
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        // 存储到成语变量中
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }

    // todo remind ThreadPoolExecutor 并没有重写 submit 方法，而是继承了 AbstractExecutorService 的 submit 方法
    // 而在 AbstractExecutorService 中的 submit 的实现就是直接调用 execute 方法

    /**
     * Executes the given task sometime in the future.  The task
     * may execute in a new thread or in an existing pooled thread.
     * <p>
     *     在将来的某个时候执行给定的任务。
     *     任务可能在新线程中执行，也可能在现有的池线程中执行。
     *
     * If the task cannot be submitted for execution, either because this
     * executor has been shutdown or because its capacity has been reached,
     * the task is handled by the current {@code RejectedExecutionHandler}.
     *
     * @param command the task to execute
     * @throws RejectedExecutionException at discretion of
     *         {@code RejectedExecutionHandler}, if the task
     *         cannot be accepted for execution
     * @throws NullPointerException if {@code command} is null
     */
    public void execute(Runnable command) {
        // 入参校验
        if (command == null)
            throw new NullPointerException();
        /*
         * Proceed in 3 steps:
         *
         * 1. If fewer than corePoolSize threads are running, try to
         * start a new thread with the given command as its first
         * task.  The call to addWorker atomically checks runState and
         * workerCount, and so prevents false alarms that would add
         * threads when it shouldn't, by returning false.
         *
         * 2. If a task can be successfully queued, then we still need
         * to double-check whether we should have added a thread
         * (because existing ones died since last checking) or that
         * the pool shut down since entry into this method. So we
         * recheck state and if necessary roll back the enqueuing if
         * stopped, or start a new thread if there are none.
         *
         * 3. If we cannot queue task, then we try to add a new
         * thread.  If it fails, we know we are shut down or saturated
         * and so reject the task.
         * <p>
         * 使用 3 个步骤继续：
         * 1. 如果运行的线程少于 corePoolSize，则尝试使用给定的命令作为其第一个任务启动一个新线程。
         *   addWorker 的调用原子地检查 runState 和 workerCount，因此通过返回 false 防止了错误警报，该警报会在不应该添加线程时添加线程。
         * 2. 如果任务可以成功排队，那么我们仍然需要再次检查是否应该添加线程（因为自上次检查以来现有线程已死亡），
         *  或者自进入此方法以来池已关闭。因此，我们重新检查状态，如果必要，如果停止，则回滚排队，或者如果没有线程，则启动一个新线程。
         * 3. 如果我们无法排队任务，那么我们尝试添加一个新线程。
         *  如果失败，我们知道我们已关闭或饱和，因此拒绝任务。
         */
        // 获取 ctl
        int c = ctl.get();
        // 如果工作线程数量小于核心线程数，则尝试添加一个工作线程；并返回
        if (workerCountOf(c) < corePoolSize) {
            if (addWorker(command, true))
                return;
            // 否则说明添加失败了
            c = ctl.get();
        }
        // 如果线程池是运行状态，则尝试将任务添加到队列中
        if (isRunning(c) && workQueue.offer(command)) {
            int recheck = ctl.get();
            // 再次检查，如果线程池不是运行状态，则移除任务；触发拒绝策略
            if (! isRunning(recheck) && remove(command))
                reject(command);
            else if (workerCountOf(recheck) == 0)
                addWorker(null, false);
        }
        // 否则，触发 addWorker，如果失败了，则触发拒绝策略
        else if (!addWorker(command, false))
            reject(command);
    }

    /**
     * Initiates an orderly shutdown in which previously submitted
     * tasks are executed, but no new tasks will be accepted.
     * Invocation has no additional effect if already shut down.
     * <p>
     *     启动一个有序关闭，其中将执行先前提交的任务，但不会接受新任务。
     *     如果已经关闭，则调用不会产生额外效果。
     *
     * <p>This method does not wait for previously submitted tasks to
     * complete execution.  Use {@link #awaitTermination awaitTermination}
     * to do that.
     * <p>
     *     此方法不会等待先前提交的任务完成执行。
     *     使用 awaitTermination 来完成。
     *
     * @throws SecurityException {@inheritDoc}
     */
    public void shutdown() {
        // 加锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 更新 ctl
            checkShutdownAccess();
            advanceRunState(SHUTDOWN);
            // 中断空闲线程
            interruptIdleWorkers();
            // 模板方法
            onShutdown(); // hook for ScheduledThreadPoolExecutor
        } finally {
            mainLock.unlock();
        }
        tryTerminate();
    }

    /**
     * Attempts to stop all actively executing tasks, halts the
     * processing of waiting tasks, and returns a list of the tasks
     * that were awaiting execution. These tasks are drained (removed)
     * from the task queue upon return from this method.
     * <p>
     *     尝试停止所有正在执行的任务，停止处理等待的任务，并返回等待执行的任务列表。
     *     从此方法返回后，这些任务将从任务队列中清除（删除）。
     *
     * <p>This method does not wait for actively executing tasks to
     * terminate.  Use {@link #awaitTermination awaitTermination} to
     * do that.
     * <p>
     *     此方法不会等待正在执行的任务终止。
     *     使用 awaitTermination 来完成。
     *
     * <p>There are no guarantees beyond best-effort attempts to stop
     * processing actively executing tasks.  This implementation
     * cancels tasks via {@link Thread#interrupt}, so any task that
     * fails to respond to interrupts may never terminate.
     * <p>
     *     除了尽力尝试停止处理正在执行的任务之外，没有其他保证。
     *     此实现通过 interrupt 取消任务，因此任何未响应中断的任务可能永远不会终止。
     *
     * @throws SecurityException {@inheritDoc}
     */
    public List<Runnable> shutdownNow() {
        // 加锁
        List<Runnable> tasks;
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 直接将 ctl 设置为 STOP
            checkShutdownAccess();
            advanceRunState(STOP);
            // 中断所有线程
            interruptWorkers();
            // 将队列中的任务取出
            tasks = drainQueue();
        } finally {
            mainLock.unlock();
        }
        tryTerminate();
        // 返回队列中的任务
        return tasks;
    }

    public boolean isShutdown() {
        // 判断 ctl 是否 > RUNNING
        return ! isRunning(ctl.get());
    }

    /**
     * Returns true if this executor is in the process of terminating
     * after {@link #shutdown} or {@link #shutdownNow} but has not
     * completely terminated.  This method may be useful for
     * debugging. A return of {@code true} reported a sufficient
     * period after shutdown may indicate that submitted tasks have
     * ignored or suppressed interruption, causing this executor not
     * to properly terminate.
     * <p>
     *     如果此执行程序正在终止（在 shutdown 或 shutdownNow 之后）但尚未完全终止，则返回 true。
     *     此方法可能对调试很有用。
     *     在 shutdown 之后的足够长时间后返回 true 可能表明提交的任务已忽略或抑制了中断，导致此执行程序无法正确终止。
     *
     * @return {@code true} if terminating but not yet terminated
     */
    public boolean isTerminating() {
        int c = ctl.get();
        return ! isRunning(c) && runStateLessThan(c, TERMINATED);
    }

    public boolean isTerminated() {
        // 判断 ctl 是否 >= TERMINATED
        return runStateAtLeast(ctl.get(), TERMINATED);
    }

    public boolean awaitTermination(long timeout, TimeUnit unit)
        throws InterruptedException {
        // 加锁
        long nanos = unit.toNanos(timeout);
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 循环直到线程池终止或超时
            for (;;) {
                // 判断 ctl 是否 >= TERMINATED
                if (runStateAtLeast(ctl.get(), TERMINATED))
                    return true;
                if (nanos <= 0)
                    return false;
                // 等待 nanos 时间
                nanos = termination.awaitNanos(nanos);
            }
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Invokes {@code shutdown} when this executor is no longer
     * referenced and it has no threads.
     * <p>
     *     当此执行程序不再引用并且没有线程时，调用 shutdown。
     */
    protected void finalize() {
        // 调用 shutdown 方法
        SecurityManager sm = System.getSecurityManager();
        if (sm == null || acc == null) {
            shutdown();
        } else {
            PrivilegedAction<Void> pa = () -> { shutdown(); return null; };
            AccessController.doPrivileged(pa, acc);
        }
    }

    /**
     * Sets the thread factory used to create new threads.
     * <p>
     *     设置用于创建新线程的线程工厂。
     *
     * @param threadFactory the new thread factory
     * @throws NullPointerException if threadFactory is null
     * @see #getThreadFactory
     */
    public void setThreadFactory(ThreadFactory threadFactory) {
        if (threadFactory == null)
            throw new NullPointerException();
        this.threadFactory = threadFactory;
    }

    /**
     * Returns the thread factory used to create new threads.
     * <p>
     *     返回用于创建新线程的线程工厂。
     *
     * @return the current thread factory
     * @see #setThreadFactory(ThreadFactory)
     */
    public ThreadFactory getThreadFactory() {
        return threadFactory;
    }

    /**
     * Sets a new handler for unexecutable tasks.
     * <p>
     *     设置一个新的拒绝策略 handler。
     *
     * @param handler the new handler
     * @throws NullPointerException if handler is null
     * @see #getRejectedExecutionHandler
     */
    public void setRejectedExecutionHandler(RejectedExecutionHandler handler) {
        if (handler == null)
            throw new NullPointerException();
        this.handler = handler;
    }

    /**
     * Returns the current handler for unexecutable tasks.
     * <p>
     *     返回当前的拒绝策略 handler。
     *
     * @return the current handler
     * @see #setRejectedExecutionHandler(RejectedExecutionHandler)
     */
    public RejectedExecutionHandler getRejectedExecutionHandler() {
        return handler;
    }

    /**
     * Returns the core number of threads.
     * <p>
     *     返回核心线程数。
     *
     * @return the core number of threads
     * @see #setCorePoolSize
     */
    public int getCorePoolSize() {
        return corePoolSize;
    }

    /**
     * Sets the core number of threads.  This overrides any value set
     * in the constructor.  If the new value is smaller than the
     * current value, excess existing threads will be terminated when
     * they next become idle.  If larger, new threads will, if needed,
     * be started to execute any queued tasks.
     * <p>
     *     设置核心线程数。
     *     这将覆盖构造函数中设置的任何值。
     *     如果新值小于当前值，则在下次空闲时终止多余的现有线程。
     *
     * @param corePoolSize the new core size
     * @throws IllegalArgumentException if {@code corePoolSize < 0}
     * @see #getCorePoolSize
     */
    public void setCorePoolSize(int corePoolSize) {
        // 参数校验
        if (corePoolSize < 0)
            throw new IllegalArgumentException();
        // 更新核心线程数量
        int delta = corePoolSize - this.corePoolSize;
        this.corePoolSize = corePoolSize;
        // 如果当前工作线程数量大于核心线程数，那么中断空闲线程
        if (workerCountOf(ctl.get()) > corePoolSize)
            interruptIdleWorkers();
            // 如果 delta > 0，说明核心线程数量增加了
            // 那么添加 delta 个新的核心线程
        else if (delta > 0) {
            // We don't really know how many new threads are "needed".
            // As a heuristic, prestart enough new workers (up to new
            // core size) to handle the current number of tasks in
            // queue, but stop if queue becomes empty while doing so.
            int k = Math.min(delta, workQueue.size());
            while (k-- > 0 && addWorker(null, true)) {
                if (workQueue.isEmpty())
                    break;
            }
        }
    }

    /**
     * Starts a core thread, causing it to idly wait for work. This
     * overrides the default policy of starting core threads only when
     * new tasks are executed. This method will return {@code false}
     * if all core threads have already been started.
     * <p>
     *     启动一个核心线程，使其等待工作。
     *     这将覆盖仅在执行新任务时启动核心线程的默认策略。
     *
     * @return {@code true} if a thread was started
     */
    public boolean prestartCoreThread() {
        // 如果当前工作线程数量小于核心线程数，那么添加一个核心线程
        return workerCountOf(ctl.get()) < corePoolSize &&
            addWorker(null, true);
    }

    /**
     * Same as prestartCoreThread except arranges that at least one
     * thread is started even if corePoolSize is 0.
     * <p>
     *     与 prestartCoreThread 相同，除了即使 corePoolSize 为 0，也会启动至少一个线程。
     */
    void ensurePrestart() {
        // 获取当前的工作线程数量
        int wc = workerCountOf(ctl.get());
        // 如果当前工作线程数量小于核心线程数，那么添加一个核心线程
        if (wc < corePoolSize)
            addWorker(null, true);
            // 否则如果 wc >= corePoolSize && wc == 0，说明 corePoolSize == 0
            // 那么也会启动一个线程
        else if (wc == 0)
            addWorker(null, false);
    }

    /**
     * Starts all core threads, causing them to idly wait for work. This
     * overrides the default policy of starting core threads only when
     * new tasks are executed.
     * <p>
     *     启动所有核心线程，使它们等待工作。
     *     这将覆盖仅在执行新任务时启动核心线程的默认策略。
     *
     * @return the number of threads started
     */
    public int prestartAllCoreThreads() {
        int n = 0;
        // 持续添加 worker 直到核心线程数达到 corePoolSize
        while (addWorker(null, true))
            ++n;
        return n;
    }

    /**
     * Returns true if this pool allows core threads to time out and
     * terminate if no tasks arrive within the keepAlive time, being
     * replaced if needed when new tasks arrive. When true, the same
     * keep-alive policy applying to non-core threads applies also to
     * core threads. When false (the default), core threads are never
     * terminated due to lack of incoming tasks.
     * <p>
     *     如果此池允许核心线程超时并在没有任务到达时终止，那么当新任务到达时需要时将被替换。
     *     当为 true 时，适用于非核心线程的相同保持活动策略也适用于核心线程。
     *     当为 false 时（默认值），核心线程永远不会因为缺少传入任务而终止。
     *
     * @return {@code true} if core threads are allowed to time out,
     *         else {@code false}
     *
     * @since 1.6
     */
    public boolean allowsCoreThreadTimeOut() {
        return allowCoreThreadTimeOut;
    }

    /**
     * Sets the policy governing whether core threads may time out and
     * terminate if no tasks arrive within the keep-alive time, being
     * replaced if needed when new tasks arrive. When false, core
     * threads are never terminated due to lack of incoming
     * tasks. When true, the same keep-alive policy applying to
     * non-core threads applies also to core threads. To avoid
     * continual thread replacement, the keep-alive time must be
     * greater than zero when setting {@code true}. This method
     * should in general be called before the pool is actively used.
     * <p>
     *     设置控制核心线程是否可以超时并在没有任务到达时终止的策略，
     *     在需要时被替换，当新任务到达时。当为false时，核心线程永远不会因为缺少传入任务而终止。
     *     当为 true 时，适用于非核心线程的相同保持活动策略也适用于核心线程。
     *     为了避免持续的线程替换，当设置为 true 时，保持活动时间必须大于零。
     *     通常在池被积极使用之前调用此方法。
     *
     * @param value {@code true} if should time out, else {@code false}
     * @throws IllegalArgumentException if value is {@code true}
     *         and the current keep-alive time is not greater than zero
     *
     * @since 1.6
     */
    public void allowCoreThreadTimeOut(boolean value) {
        // 参数校验
        if (value && keepAliveTime <= 0)
            throw new IllegalArgumentException("Core threads must have nonzero keep alive times");
        // 更新是否允许核心线程超时
        if (value != allowCoreThreadTimeOut) {
            allowCoreThreadTimeOut = value;
            // 如果允许核心线程超时，那么中断所有未执行任务的线程
            if (value)
                interruptIdleWorkers();
        }
    }

    /**
     * Returns the maximum allowed number of threads.
     * <p>
     *     返回允许的最大线程数。
     *
     * @return the maximum allowed number of threads
     * @see #setMaximumPoolSize
     */
    public int getMaximumPoolSize() {
        return maximumPoolSize;
    }

    /**
     * Sets the maximum allowed number of threads. This overrides any
     * value set in the constructor. If the new value is smaller than
     * the current value, excess existing threads will be
     * terminated when they next become idle.
     * <p>
     *     设置允许的最大线程数。这将覆盖构造函数中设置的任何值。
     *     如果新值小于当前值，则在下次空闲时终止多余的现有线程。
     *
     * @param maximumPoolSize the new maximum
     * @throws IllegalArgumentException if the new maximum is
     *         less than or equal to zero, or
     *         less than the {@linkplain #getCorePoolSize core pool size}
     * @see #getMaximumPoolSize
     */
    public void setMaximumPoolSize(int maximumPoolSize) {
        // 参数校验
        if (maximumPoolSize <= 0 || maximumPoolSize < corePoolSize)
            throw new IllegalArgumentException();
        // 更新最大线程数
        this.maximumPoolSize = maximumPoolSize;
        // 如果线程数超过最大线程数，那么中断所有未执行任务的线程
        if (workerCountOf(ctl.get()) > maximumPoolSize)
            interruptIdleWorkers();
    }

    /**
     * Sets the time limit for which threads may remain idle before
     * being terminated.  If there are more than the core number of
     * threads currently in the pool, after waiting this amount of
     * time without processing a task, excess threads will be
     * terminated.  This overrides any value set in the constructor.
     * <p>
     *     设置线程在终止之前可以保持空闲的时间限制。
     *     如果当前池中的线程数超过核心数，那么在等待这段时间而不处理任务后，将终止多余的线程。
     *     这将覆盖构造函数中设置的任何值。
     *
     * @param time the time to wait.  A time value of zero will cause
     *        excess threads to terminate immediately after executing tasks.
     * @param unit the time unit of the {@code time} argument
     * @throws IllegalArgumentException if {@code time} less than zero or
     *         if {@code time} is zero and {@code allowsCoreThreadTimeOut}
     * @see #getKeepAliveTime(TimeUnit)
     */
    public void setKeepAliveTime(long time, TimeUnit unit) {
        // 参数校验
        if (time < 0)
            throw new IllegalArgumentException();
        if (time == 0 && allowsCoreThreadTimeOut())
            throw new IllegalArgumentException("Core threads must have nonzero keep alive times");
        // 计算线程保持活动时间
        long keepAliveTime = unit.toNanos(time);
        long delta = keepAliveTime - this.keepAliveTime;
        // 更新线程保持活动时间
        this.keepAliveTime = keepAliveTime;
        // 如果时间变短了，那么中断所有未执行任务的线程
        if (delta < 0)
            interruptIdleWorkers();
    }

    /**
     * Returns the thread keep-alive time, which is the amount of time
     * that threads in excess of the core pool size may remain
     * idle before being terminated.
     * <p>
     *     返回线程保持活动时间，这是超出核心池大小的线程可能保持空闲状态的时间量，然后被终止。
     *
     * @param unit the desired time unit of the result
     * @return the time limit
     * @see #setKeepAliveTime(long, TimeUnit)
     */
    public long getKeepAliveTime(TimeUnit unit) {
        return unit.convert(keepAliveTime, TimeUnit.NANOSECONDS);
    }

    /* User-level queue utilities */

    /**
     * Returns the task queue used by this executor. Access to the
     * task queue is intended primarily for debugging and monitoring.
     * This queue may be in active use.  Retrieving the task queue
     * does not prevent queued tasks from executing.
     * <p>
     *     返回此执行程序使用的任务队列。
     *     对任务队列的访问主要用于调试和监视。
     *     此队列可能正在使用中。检索任务队列不会阻止排队的任务执行。
     *
     * @return the task queue
     */
    public BlockingQueue<Runnable> getQueue() {
        return workQueue;
    }

    /**
     * Removes this task from the executor's internal queue if it is
     * present, thus causing it not to be run if it has not already
     * started.
     * <p>
     *     如果任务存在于执行程序的内部队列中，则从中删除此任务，从而导致如果尚未启动，则不运行该任务。
     *
     * <p>This method may be useful as one part of a cancellation
     * scheme.  It may fail to remove tasks that have been converted
     * into other forms before being placed on the internal queue. For
     * example, a task entered using {@code submit} might be
     * converted into a form that maintains {@code Future} status.
     * However, in such cases, method {@link #purge} may be used to
     * remove those Futures that have been cancelled.
     * <p>
     *     此方法可能作为取消方案的一部分而有用。
     *     它可能无法删除在放置在内部队列之前已转换为其他形式的任务。
     *     例如，使用 submit 输入的任务可能会转换为维护 Future 状态的形式。
     *     但是，在这种情况下，可以使用方法 purge 删除已取消的那些 Future。
     *
     * @param task the task to remove
     * @return {@code true} if the task was removed
     */
    public boolean remove(Runnable task) {
        // 从任务队列中移除任务
        boolean removed = workQueue.remove(task);
        tryTerminate(); // In case SHUTDOWN and now empty
        return removed;
    }

    /**
     * Tries to remove from the work queue all {@link Future}
     * tasks that have been cancelled. This method can be useful as a
     * storage reclamation operation, that has no other impact on
     * functionality. Cancelled tasks are never executed, but may
     * accumulate in work queues until worker threads can actively
     * remove them. Invoking this method instead tries to remove them now.
     * However, this method may fail to remove tasks in
     * the presence of interference by other threads.
     * <p>
     *     尝试从工作队列中删除所有已取消的 Future 任务。
     *     此方法可以作为存储回收操作，对功能没有其他影响。
     *     已取消的任务永远不会被执行，但可能会在工作队列中累积，直到工作线程可以主动删除它们。
     *     调用此方法而不是尝试立即删除它们。
     */
    public void purge() {
        // 获取任务队列
        final BlockingQueue<Runnable> q = workQueue;
        try {
            // 获取任务队列的迭代器
            Iterator<Runnable> it = q.iterator();
            // 基于迭代器遍历任务队列
            while (it.hasNext()) {
                Runnable r = it.next();
                // 如果任务是 Future 类型，并且任务已经被取消了，就从任务队列中移除
                if (r instanceof Future<?> && ((Future<?>)r).isCancelled())
                    it.remove();
            }

            // 捕获并忽略 ConcurrentModificationException 异常
        } catch (ConcurrentModificationException fallThrough) {
            // Take slow path if we encounter interference during traversal.
            // Make copy for traversal and call remove for cancelled entries.
            // The slow path is more likely to be O(N*N).
            // 如果在遍历期间遇到干扰，请采取慢路径。
            // 为遍历创建副本，并为已取消的条目调用 remove。
            // 慢路径更有可能是 O(N*N)。
            for (Object r : q.toArray())
                if (r instanceof Future<?> && ((Future<?>)r).isCancelled())
                    q.remove(r);
        }

        tryTerminate(); // In case SHUTDOWN and now empty
    }

    /* Statistics */

    /**
     * Returns the current number of threads in the pool.
     * <p>
     *     返回池中当前线程的数量。
     *
     * @return the number of threads
     */
    public int getPoolSize() {
        // 加主锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // Remove rare and surprising possibility of
            // isTerminated() && getPoolSize() > 0
            // 如果 ctl >= TIDYING，则返回 0；否则返回工作线程数
            return runStateAtLeast(ctl.get(), TIDYING) ? 0
                : workers.size();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the approximate number of threads that are actively
     * executing tasks.
     * <p>
     *     返回正在执行任务的线程的近似数量。
     *
     * @return the number of threads
     */
    public int getActiveCount() {
        // 加主锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            int n = 0;
            // 遍历所有的工作线程，如果工作线程被锁住了，说明它正在执行任务，所以活跃线程数要加 1
            for (Worker w : workers)
                if (w.isLocked())
                    ++n;
            return n;
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the largest number of threads that have ever
     * simultaneously been in the pool.
     * <p>
     *     返回曾经同时存在于池中的最大线程数。
     *
     * @return the number of threads
     */
    public int getLargestPoolSize() {
        // 加主锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 返回最大线程数
            return largestPoolSize;
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the approximate total number of tasks that have ever been
     * scheduled for execution. Because the states of tasks and
     * threads may change dynamically during computation, the returned
     * value is only an approximation.
     * <p>
     *     返回曾经被调度执行的任务的近似总数。
     *     因为任务和线程的状态在计算过程中可能会动态变化，所以返回的值只是一个近似值。
     *
     * @return the number of tasks
     */
    public long getTaskCount() {
        // 加主锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 获取已完成任务数
            long n = completedTaskCount;
            // 遍历所有的工作线程，累加各个工作线程的已完成任务数
            for (Worker w : workers) {
                n += w.completedTasks;
                // 如果工作线程被锁住了，说明它正在执行任务，所以已完成任务数要加 1
                if (w.isLocked())
                    ++n;
            }
            // 再增加队列中的任务数
            return n + workQueue.size();
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns the approximate total number of tasks that have
     * completed execution. Because the states of tasks and threads
     * may change dynamically during computation, the returned value
     * is only an approximation, but one that does not ever decrease
     * across successive calls.
     * <p>
     *     返回已完成执行的任务的近似总数。
     *     因为任务和线程的状态在计算过程中可能会动态变化，所以返回的值只是一个近似值，但在连续调用中不会减少。
     *
     * @return the number of tasks
     */
    public long getCompletedTaskCount() {
        // 加主锁
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            // 获取总的已完成任务数
            long n = completedTaskCount;
            // 遍历所有的工作线程，累加各个工作线程的已完成任务数
            for (Worker w : workers)
                n += w.completedTasks;
            return n;
        } finally {
            mainLock.unlock();
        }
    }

    /**
     * Returns a string identifying this pool, as well as its state,
     * including indications of run state and estimated worker and
     * task counts.
     *
     * @return a string identifying this pool, as well as its state
     */
    public String toString() {
        long ncompleted;
        int nworkers, nactive;
        final ReentrantLock mainLock = this.mainLock;
        mainLock.lock();
        try {
            ncompleted = completedTaskCount;
            nactive = 0;
            nworkers = workers.size();
            for (Worker w : workers) {
                ncompleted += w.completedTasks;
                if (w.isLocked())
                    ++nactive;
            }
        } finally {
            mainLock.unlock();
        }
        int c = ctl.get();
        String rs = (runStateLessThan(c, SHUTDOWN) ? "Running" :
                     (runStateAtLeast(c, TERMINATED) ? "Terminated" :
                      "Shutting down"));
        return super.toString() +
            "[" + rs +
            ", pool size = " + nworkers +
            ", active threads = " + nactive +
            ", queued tasks = " + workQueue.size() +
            ", completed tasks = " + ncompleted +
            "]";
    }

    /* Extension hooks */

    /**
     * Method invoked prior to executing the given Runnable in the
     * given thread.  This method is invoked by thread {@code t} that
     * will execute task {@code r}, and may be used to re-initialize
     * ThreadLocals, or to perform logging.
     * <p>
     *     在给定线程中执行给定 Runnable 之前调用的方法。
     *     此方法由将执行任务 r 的线程 t 调用，可用于重新初始化 ThreadLocals 或执行日志记录。
     *
     * <p>This implementation does nothing, but may be customized in
     * subclasses. Note: To properly nest multiple overridings, subclasses
     * should generally invoke {@code super.beforeExecute} at the end of
     * this method.
     * <p>
     *     此实现不执行任何操作，但可以在子类中自定义。
     *     注意：为了正确地嵌套多个重写，子类通常应该在此方法的末尾调用 super.beforeExecute。
     *
     * @param t the thread that will run task {@code r}
     * @param r the task that will be executed
     */
    protected void beforeExecute(Thread t, Runnable r) { }

    /**
     * Method invoked upon completion of execution of the given Runnable.
     * This method is invoked by the thread that executed the task. If
     * non-null, the Throwable is the uncaught {@code RuntimeException}
     * or {@code Error} that caused execution to terminate abruptly.
     * <p>
     *     在完成执行给定的 Runnable 时调用的方法。
     *     此方法由执行任务的线程调用。
     *     如果非空，则 Throwable 是导致执行突然终止的未捕获 RuntimeException 或 Error。
     *
     * <p>This implementation does nothing, but may be customized in
     * subclasses. Note: To properly nest multiple overridings, subclasses
     * should generally invoke {@code super.afterExecute} at the
     * beginning of this method.
     * <p>
     *     此实现不执行任何操作，但可以在子类中自定义。
     *     注意：为了正确地嵌套多个重写，子类通常应该在此方法的开头调用 super.afterExecute。
     *
     * <p><b>Note:</b> When actions are enclosed in tasks (such as
     * {@link FutureTask}) either explicitly or via methods such as
     * {@code submit}, these task objects catch and maintain
     * computational exceptions, and so they do not cause abrupt
     * termination, and the internal exceptions are <em>not</em>
     * passed to this method. If you would like to trap both kinds of
     * failures in this method, you can further probe for such cases,
     * as in this sample subclass that prints either the direct cause
     * or the underlying exception if a task has been aborted:
     *
     *  <pre> {@code
     * class ExtendedExecutor extends ThreadPoolExecutor {
     *   // ...
     *   protected void afterExecute(Runnable r, Throwable t) {
     *     super.afterExecute(r, t);
     *     if (t == null && r instanceof Future<?>) {
     *       try {
     *         Object result = ((Future<?>) r).get();
     *       } catch (CancellationException ce) {
     *           t = ce;
     *       } catch (ExecutionException ee) {
     *           t = ee.getCause();
     *       } catch (InterruptedException ie) {
     *           Thread.currentThread().interrupt(); // ignore/reset
     *       }
     *     }
     *     if (t != null)
     *       System.out.println(t);
     *   }
     * }}</pre>
     *
     * @param r the runnable that has completed
     * @param t the exception that caused termination, or null if
     * execution completed normally
     */
    protected void afterExecute(Runnable r, Throwable t) { }

    /**
     * Method invoked when the Executor has terminated.  Default
     * implementation does nothing. Note: To properly nest multiple
     * overridings, subclasses should generally invoke
     * {@code super.terminated} within this method.
     * <p>
     *     当执行程序终止时调用的方法。
     *     默认实现不执行任何操作。
     *     注意：为了正确地嵌套多个重写，子类通常应该在此方法中调用 super.terminated。
     */
    protected void terminated() { }

    /* Predefined RejectedExecutionHandlers */

    /**
     * A handler for rejected tasks that runs the rejected task
     * directly in the calling thread of the {@code execute} method,
     * unless the executor has been shut down, in which case the task
     * is discarded.
     * <p>
     *     一个处理被拒绝任务的处理程序，它会直接在 execute 方法的调用线程中运行被拒绝的任务，
     *     除非执行程序已关闭，在这种情况下，任务将被丢弃。
     */
    public static class CallerRunsPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code CallerRunsPolicy}.
         */
        public CallerRunsPolicy() { }

        /**
         * Executes task r in the caller's thread, unless the executor
         * has been shut down, in which case the task is discarded.
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            if (!e.isShutdown()) {
                r.run();
            }
        }
    }

    /**
     * A handler for rejected tasks that throws a
     * {@code RejectedExecutionException}.
     * <p>
     *     一个处理被拒绝任务的处理程序，它会抛出一个 RejectedExecutionException。
     */
    public static class AbortPolicy implements RejectedExecutionHandler {
        /**
         * Creates an {@code AbortPolicy}.
         */
        public AbortPolicy() { }

        /**
         * Always throws RejectedExecutionException.
         * <p>
         *     总是抛出 RejectedExecutionException。
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         * @throws RejectedExecutionException always
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            throw new RejectedExecutionException("Task " + r.toString() +
                                                 " rejected from " +
                                                 e.toString());
        }
    }

    /**
     * A handler for rejected tasks that silently discards the
     * rejected task.
     * <p>
     *     一个处理被拒绝任务的处理程序，它会默默地丢弃被拒绝的任务。
     */
    public static class DiscardPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code DiscardPolicy}.
         * <p>
         *     创建一个 DiscardPolicy。
         */
        public DiscardPolicy() { }

        /**
         * Does nothing, which has the effect of discarding task r.
         * <p>
         *     不执行任何操作，这会导致丢弃任务 r。
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
        }
    }

    /**
     * A handler for rejected tasks that discards the oldest unhandled
     * request and then retries {@code execute}, unless the executor
     * is shut down, in which case the task is discarded.
     * <p>
     *     一个处理被拒绝任务的处理程序，它会丢弃最旧的未处理请求，然后重试 execute，
     *     除非执行程序已关闭，在这种情况下，任务将被丢弃。
     */
    public static class DiscardOldestPolicy implements RejectedExecutionHandler {
        /**
         * Creates a {@code DiscardOldestPolicy} for the given executor.
         * <p>
         *     为给定的执行程序创建一个 DiscardOldestPolicy。
         */
        public DiscardOldestPolicy() { }

        /**
         * Obtains and ignores the next task that the executor
         * would otherwise execute, if one is immediately available,
         * and then retries execution of task r, unless the executor
         * is shut down, in which case task r is instead discarded.
         * <p>
         *     获取并忽略执行程序否则将执行的下一个任务（如果立即可用），然后重试任务 r 的执行，
         *     除非执行程序已关闭，在这种情况下，任务 r 将被丢弃。
         *
         * @param r the runnable task requested to be executed
         * @param e the executor attempting to execute this task
         */
        public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
            if (!e.isShutdown()) {
                e.getQueue().poll();
                e.execute(r);
            }
        }
    }
}
