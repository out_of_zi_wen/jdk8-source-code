/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea, Bill Scherer, and Michael Scott with
 * assistance from members of JCP JSR-166 Expert Group and released to
 * the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util.concurrent;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A {@linkplain BlockingQueue blocking queue} in which each insert
 * operation must wait for a corresponding remove operation by another
 * thread, and vice versa.  A synchronous queue does not have any
 * internal capacity, not even a capacity of one.  You cannot
 * {@code peek} at a synchronous queue because an element is only
 * present when you try to remove it; you cannot insert an element
 * (using any method) unless another thread is trying to remove it;
 * you cannot iterate as there is nothing to iterate.  The
 * <em>head</em> of the queue is the element that the first queued
 * inserting thread is trying to add to the queue; if there is no such
 * queued thread then no element is available for removal and
 * {@code poll()} will return {@code null}.  For purposes of other
 * {@code Collection} methods (for example {@code contains}), a
 * {@code SynchronousQueue} acts as an empty collection.  This queue
 * does not permit {@code null} elements.
 * <p>
 *     同步队列是一种阻塞队列，其中每个插入操作必须等待另一个线程的对应删除操作，反之亦然。
 *     同步队列没有任何内部容量，甚至没有一个容量。
 *     您不能查看同步队列，因为只有在尝试删除元素时元素才存在；
 *     除非另一个线程尝试删除元素，否则您不能插入元素（使用任何方法）；
 *     由于没有要迭代的元素，您不能迭代。
 *     队列的头是第一个排队插入线程试图添加到队列的元素；如果没有这样的排队线程，则没有元素可用于删除，poll()将返回null。
 *     对于其他Collection方法（例如contains），SynchronousQueue充当空集合。
 *     此队列不允许null元素。
 *
 * <p>Synchronous queues are similar to rendezvous channels used in
 * CSP and Ada. They are well suited for handoff designs, in which an
 * object running in one thread must sync up with an object running
 * in another thread in order to hand it some information, event, or
 * task.
 * <p>
 *     同步队列类似于 CSP 和 Ada 中使用的会合通道。
 *     它们非常适合于交接设计，其中在一个线程中运行的对象必须与在另一个线程中运行的对象同步，以便将一些信息、事件或任务交给它。
 *
 * <p>This class supports an optional fairness policy for ordering
 * waiting producer and consumer threads.  By default, this ordering
 * is not guaranteed. However, a queue constructed with fairness set
 * to {@code true} grants threads access in FIFO order.
 * <p>
 *     此类支持用于对等待生产者和消费者线程进行排序的可选公平策略。
 *     默认情况下，不保证此排序。
 *     但是，使用设置为true的公平性构造的队列以FIFO顺序授予线程访问权限。
 *
 * <p>This class and its iterator implement all of the
 * <em>optional</em> methods of the {@link Collection} and {@link
 * Iterator} interfaces.
 * <p>
 *     此类及其迭代器实现 Collection 和 Iterator 接口的所有可选方法。
 *
 * <p>This class is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @since 1.5
 * @author Doug Lea and Bill Scherer and Michael Scott
 * @param <E> the type of elements held in this collection
 */
public class SynchronousQueue<E> extends AbstractQueue<E>
    implements BlockingQueue<E>, java.io.Serializable {
    private static final long serialVersionUID = -3223113410248163686L;

    /*
     * This class implements extensions of the dual stack and dual
     * queue algorithms described in "Nonblocking Concurrent Objects
     * with Condition Synchronization", by W. N. Scherer III and
     * M. L. Scott.  18th Annual Conf. on Distributed Computing,
     * Oct. 2004 (see also
     * http://www.cs.rochester.edu/u/scott/synchronization/pseudocode/duals.html).
     * The (Lifo) stack is used for non-fair mode, and the (Fifo)
     * queue for fair mode. The performance of the two is generally
     * similar. Fifo usually supports higher throughput under
     * contention but Lifo maintains higher thread locality in common
     * applications.
     * <p>
     * 此类实现了“具有条件同步的非阻塞并发对象”中描述的双栈和双队列算法的扩展，作者是 W. N. Scherer III 和 M. L. Scott。
     * 2004年10月分布式计算年会（另请参见 http://www.cs.rochester.edu/u/scott/synchronization/pseudocode/duals.html）。
     * （Lifo）栈用于非公平模式，（Fifo）队列用于公平模式。
     * 两者的性能通常相似。
     * 在争用下，Fifo通常支持更高的吞吐量，但Lifo在常见应用程序中保持更高的线程局部性。
     *
     * A dual queue (and similarly stack) is one that at any given
     * time either holds "data" -- items provided by put operations,
     * or "requests" -- slots representing take operations, or is
     * empty. A call to "fulfill" (i.e., a call requesting an item
     * from a queue holding data or vice versa) dequeues a
     * complementary node.  The most interesting feature of these
     * queues is that any operation can figure out which mode the
     * queue is in, and act accordingly without needing locks.
     * <p>
     * 双队列（以及类似的栈）是指在任何给定时间，它们要么持有“数据”——由put操作提供的项目，要么持有“请求”——表示take操作的插槽，要么为空。
     * 对“fulfill”的调用（即，从持有数据的队列请求项目或反之亦然）出队一个补充节点。
     * 这些队列最有趣的特性是，任何操作都可以弄清楚队列处于哪种模式，并相应地行事，而无需锁。
     *
     * Both the queue and stack extend abstract class Transferer
     * defining the single method transfer that does a put or a
     * take. These are unified into a single method because in dual
     * data structures, the put and take operations are symmetrical,
     * so nearly all code can be combined. The resulting transfer
     * methods are on the long side, but are easier to follow than
     * they would be if broken up into nearly-duplicated parts.
     * <p>
     * 队列和栈都扩展了抽象类 Transferer，定义了执行 put 或 take 的单个方法 transfer。
     * 这些方法统一为一个方法，因为在双数据结构中，put 和 take 操作是对称的，因此几乎所有代码都可以合并。
     * 生成的 transfer 方法有点长，但比将其分解为几乎重复的部分更容易遵循。
     *
     * The queue and stack data structures share many conceptual
     * similarities but very few concrete details. For simplicity,
     * they are kept distinct so that they can later evolve
     * separately.
     * <p>
     * 队列和栈数据结构在概念上有许多相似之处，但具体细节很少。
     * 为简单起见，它们保持不同，以便以后可以分别发展。
     *
     * The algorithms here differ from the versions in the above paper
     * in extending them for use in synchronous queues, as well as
     * dealing with cancellation. The main differences include:
     *
     *  1. The original algorithms used bit-marked pointers, but
     *     the ones here use mode bits in nodes, leading to a number
     *     of further adaptations.
     *  2. SynchronousQueues must block threads waiting to become
     *     fulfilled.
     *  3. Support for cancellation via timeout and interrupts,
     *     including cleaning out cancelled nodes/threads
     *     from lists to avoid garbage retention and memory depletion.
     * <p>
     * 此处的算法与上述论文中的版本不同，它们扩展了用于同步队列的使用，并处理了取消。
     * 主要区别包括：
     * 1. 原始算法使用位标记指针，但这里的算法使用节点中的模式位，导致了许多进一步的适应。
     * 2. 同步队列必须阻塞等待变得完成的线程。
     * 3. 通过超时和中断支持取消，包括清除列表中的已取消节点/线程，以避免垃圾保留和内存耗尽。
     *
     * Blocking is mainly accomplished using LockSupport park/unpark,
     * except that nodes that appear to be the next ones to become
     * fulfilled first spin a bit (on multiprocessors only). On very
     * busy synchronous queues, spinning can dramatically improve
     * throughput. And on less busy ones, the amount of spinning is
     * small enough not to be noticeable.
     * <p>
     * 阻塞主要是使用 LockSupport park/unpark 完成的，除了那些似乎是下一个要变得完成的节点会先自旋一点（仅在多处理器上）。
     * 在非常繁忙的同步队列上，自旋可以显著提高吞吐量。
     * 在不太繁忙的队列上，自旋的数量很小，不会引起注意。
     *
     * Cleaning is done in different ways in queues vs stacks.  For
     * queues, we can almost always remove a node immediately in O(1)
     * time (modulo retries for consistency checks) when it is
     * cancelled. But if it may be pinned as the current tail, it must
     * wait until some subsequent cancellation. For stacks, we need a
     * potentially O(n) traversal to be sure that we can remove the
     * node, but this can run concurrently with other threads
     * accessing the stack.
     * <p>
     * 在队列和栈中以不同的方式进行清理。
     * 对于队列，当节点被取消时，我们几乎总是可以在 O(1) 时间内立即删除节点（除了一致性检查的重试）。
     * 但是，如果它可能被固定为当前尾部，则必须等待一些后续取消。
     * 对于栈，我们需要一个潜在的 O(n) 遍历，以确保我们可以删除节点，但这可以与其他访问栈的线程并发运行。
     *
     * While garbage collection takes care of most node reclamation
     * issues that otherwise complicate nonblocking algorithms, care
     * is taken to "forget" references to data, other nodes, and
     * threads that might be held on to long-term by blocked
     * threads. In cases where setting to null would otherwise
     * conflict with main algorithms, this is done by changing a
     * node's link to now point to the node itself. This doesn't arise
     * much for Stack nodes (because blocked threads do not hang on to
     * old head pointers), but references in Queue nodes must be
     * aggressively forgotten to avoid reachability of everything any
     * node has ever referred to since arrival.
     * <p>
     * 虽然垃圾收集处理了大多数节点回收问题，否则会使非阻塞算法复杂化，但是要注意“忘记”数据、其他节点和线程的引用，这些引用可能被阻塞线程长期保留。
     * 在设置为 null 会与主要算法冲突的情况下，通过将节点的链接更改为现在指向节点本身来完成。
     * 这在栈节点中并不经常出现（因为阻塞线程不会保留旧的头指针），但是必须积极忘记队列节点中的引用，以避免自到达以来任何节点曾引用过的所有内容。
     */

    /**
     * Shared internal API for dual stacks and queues.
     * <p>
     *     双栈和队列的共享内部 API。
     */
    abstract static class Transferer<E> {
        /**
         * Performs a put or take.
         * <p>
         *     执行 put 或 take。
         *
         * @param e if non-null, the item to be handed to a consumer;
         *          if null, requests that transfer return an item
         *          offered by producer.
         * @param timed if this operation should timeout
         * @param nanos the timeout, in nanoseconds
         * @return if non-null, the item provided or received; if null,
         *         the operation failed due to timeout or interrupt --
         *         the caller can distinguish which of these occurred
         *         by checking Thread.interrupted.
         */
        abstract E transfer(E e, boolean timed, long nanos);
    }

    /**
     * The number of CPUs, for spin control
     * <p>
     *     CPU 的数量，用于自旋控制
     */
    static final int NCPUS = Runtime.getRuntime().availableProcessors();

    /**
     * The number of times to spin before blocking in timed waits.
     * The value is empirically derived -- it works well across a
     * variety of processors and OSes. Empirically, the best value
     * seems not to vary with number of CPUs (beyond 2) so is just
     * a constant.
     * <p>
     *     在定时等待中阻塞之前自旋的次数。
     *     该值是经验性的——它在各种处理器和操作系统上都表现良好。
     *     从经验上看，最佳值似乎不随 CPU 数量（超过 2）而变化，因此只是一个常数。
     */
    static final int maxTimedSpins = (NCPUS < 2) ? 0 : 32;

    /**
     * The number of times to spin before blocking in untimed waits.
     * This is greater than timed value because untimed waits spin
     * faster since they don't need to check times on each spin.
     * <p>
     *     在不定时等待中阻塞之前自旋的次数。
     *     这个值大于定时值，因为不定时等待自旋得更快，因为它们不需要在每次自旋时检查时间。
     */
    static final int maxUntimedSpins = maxTimedSpins * 16;

    /**
     * The number of nanoseconds for which it is faster to spin
     * rather than to use timed park. A rough estimate suffices.
     * <p>
     *     自旋比使用定时 park 更快的纳秒数。
     *     粗略估计就足够了。
     */
    static final long spinForTimeoutThreshold = 1000L;

    /** Dual stack */
    static final class TransferStack<E> extends Transferer<E> {
        /*
         * This extends Scherer-Scott dual stack algorithm, differing,
         * among other ways, by using "covering" nodes rather than
         * bit-marked pointers: Fulfilling operations push on marker
         * nodes (with FULFILLING bit set in mode) to reserve a spot
         * to match a waiting node.
         * <p>
         * 这扩展了 Scherer-Scott 双栈算法，通过使用“覆盖”节点而不是位标记指针等方式进行区分：
         * 完成操作在标记节点上推送（在模式中设置 FULFILLING 位）以保留一个位置以匹配等待节点。
         */

        /* Modes for SNodes, ORed together in node fields */
        /**
         * Node represents an unfulfilled consumer
         * <p>
         *     表示一个未完成的消费者的结点
         */
        static final int REQUEST    = 0;
        /**
         * Node represents an unfulfilled producer
         * <p>
         *     表示一个未完成的生产者的结点
         */
        static final int DATA       = 1;
        /**
         * Node is fulfilling another unfulfilled DATA or REQUEST
         * <p>
         *     正在完成另一个未完成的 DATA 或 REQUEST 的结点
         */
        static final int FULFILLING = 2;

        /**
         * Returns true if m has fulfilling bit set.
         * <p>
         *     如果 m 设置了完成位，则返回 true。
         */
        static boolean isFulfilling(int m) { return (m & FULFILLING) != 0; }

        /**
         * Node class for TransferStacks.
         * <p>
         *     TransferStacks 的结点类。
         */
        static final class SNode {
            volatile SNode next;        // next node in stack
            volatile SNode match;       // the node matched to this
            volatile Thread waiter;     // to control park/unpark
            Object item;                // data; or null for REQUESTs
            int mode;
            // Note: item and mode fields don't need to be volatile
            // since they are always written before, and read after,
            // other volatile/atomic operations.

            SNode(Object item) {
                this.item = item;
            }

            // cas 更新当前对象的 next 成员
            boolean casNext(SNode cmp, SNode val) {
                return cmp == next &&
                    UNSAFE.compareAndSwapObject(this, nextOffset, cmp, val);
            }

            /**
             * Tries to match node s to this node, if so, waking up thread.
             * Fulfillers call tryMatch to identify their waiters.
             * Waiters block until they have been matched.
             * <p>
             *     尝试将结点 s 与此结点匹配，如果匹配成功，则唤醒线程。
             *     完成者调用 tryMatch 来识别他们的等待者。
             *     等待者会阻塞，直到它们被匹配。
             *
             * @param s the node to match
             * @return true if successfully matched to s
             */
            boolean tryMatch(SNode s) {
                // cas 更新 match 位
                if (match == null &&
                    UNSAFE.compareAndSwapObject(this, matchOffset, null, s)) {
                    // cas 成功之后，唤醒当前 node 结点对应的 Thread 线程
                    Thread w = waiter;
                    if (w != null) {    // waiters need at most one unpark
                        waiter = null;
                        LockSupport.unpark(w);
                    }
                    return true;
                }
                // 判断是否 match 成功
                return match == s;
            }

            /**
             * Tries to cancel a wait by matching node to itself.
             * <p>
             *     通过将结点与自身匹配来尝试取消等待。
             */
            void tryCancel() {
                // 记录 match == this，代表取消 node 对应的任务
                UNSAFE.compareAndSwapObject(this, matchOffset, null, this);
            }

            boolean isCancelled() {
                return match == this;
            }

            // Unsafe mechanics
            private static final sun.misc.Unsafe UNSAFE;
            private static final long matchOffset;
            private static final long nextOffset;

            static {
                try {
                    UNSAFE = sun.misc.Unsafe.getUnsafe();
                    Class<?> k = SNode.class;
                    matchOffset = UNSAFE.objectFieldOffset
                        (k.getDeclaredField("match"));
                    nextOffset = UNSAFE.objectFieldOffset
                        (k.getDeclaredField("next"));
                } catch (Exception e) {
                    throw new Error(e);
                }
            }
        }

        /** The head (top) of the stack */
        volatile SNode head;

        boolean casHead(SNode h, SNode nh) {
            return h == head &&
                UNSAFE.compareAndSwapObject(this, headOffset, h, nh);
        }

        /**
         * Creates or resets fields of a node. Called only from transfer
         * where the node to push on stack is lazily created and
         * reused when possible to help reduce intervals between reads
         * and CASes of head and to avoid surges of garbage when CASes
         * to push nodes fail due to contention.
         * <p>
         *     创建或重置结点的字段。
         *     仅从 transfer 调用，其中要推送到栈上的结点是懒惰创建的，并且在可能的情况下重用，以帮助减少对 head 的读取和 CAS 之间的间隔，
         */
        static SNode snode(SNode s, Object e, SNode next, int mode) {
            if (s == null) s = new SNode(e);
            s.mode = mode;
            s.next = next;
            return s;
        }

        /**
         * Puts or takes an item.
         * <p>
         *     放置或获取一个项目。
         */
        @SuppressWarnings("unchecked")
        E transfer(E e, boolean timed, long nanos) {
            /*
             * Basic algorithm is to loop trying one of three actions:
             *
             * 1. If apparently empty or already containing nodes of same
             *    mode, try to push node on stack and wait for a match,
             *    returning it, or null if cancelled.
             *
             * 2. If apparently containing node of complementary mode,
             *    try to push a fulfilling node on to stack, match
             *    with corresponding waiting node, pop both from
             *    stack, and return matched item. The matching or
             *    unlinking might not actually be necessary because of
             *    other threads performing action 3:
             *
             * 3. If top of stack already holds another fulfilling node,
             *    help it out by doing its match and/or pop
             *    operations, and then continue. The code for helping
             *    is essentially the same as for fulfilling, except
             *    that it doesn't return the item.
             * <p>
             * 基本算法是循环尝试以下三种操作之一：
             * 1. 如果显然为空或已包含相同 mode 的结点，尝试将 node 推送到栈上并等待匹配，然后返回它；如果取消，则返回 null。
             * 2. 如果显然包含 complementary mode 的结点，尝试将 fulfilling node 推送到栈上，与相应的等待结点匹配，从栈中弹出两个结点，并返回匹配的项目。
             *   匹配或取消可能实际上并不是必要的，因为其他线程执行操作 3：
             * 3. 如果栈顶已经包含另一个 fulfilling node，通过执行其匹配和/或弹出操作来帮助它，然后继续。
             *  帮助的代码基本上与 fulfilling 的代码相同，只是它不返回项目。
             */

            SNode s = null; // constructed/reused as needed
            // 根据入参 e == null 来判断本次 transfer 是向 queue 塞入数据还是取出数据
            int mode = (e == null) ? REQUEST : DATA;

            // todo 一个前置思想，如果 SynchronousQueue 的 stack 中存了大量的元素，那么这些元素的 mode 一定都是相同的！
            // 由于所有操作都依赖针对 mode 的 cas，所以不需要同步锁
            // 大致的的操作总结：
            // 1. 如果 put 操作执行时 stack 为空或者 stack 里面已经有很多 put 结点了，那么你也跟着自我阻塞；take 同理
            // 2. 如果 put 操作执行时，stack 非空，且 stack 里面存在 take 结点，那么理论上你可以和它配对
                // 2.1 如果 cas 配对成功了，那么你会同时 cas 插入自己并标记自己为 fulfill 状态；同时唤醒对方，自己也返回
                // 2.2 如果 cas 配对失败了，说明对方已经被别人抢走了，那么你主动弹出这对狗男女，然后重试

            // 一个持续的循环
            for (;;) {
                // 取到头结点
                SNode h = head;
                // 如果头结点为空，或者头结点的 mode 和本次的行为一致
                // STAGE 1：必须自我阻塞的场景（cur.mode == head.mode，eg：全部都是男的）
                if (h == null || h.mode == mode) {  // empty or same-mode
                    // 如果本次操作允许超时，并且超时时间小于等于 0，则直接返回 null
                    if (timed && nanos <= 0) {      // can't wait
                        // 如果此时 head 不正常，那么就 cas 更新 head 为其下一个结点
                        if (h != null && h.isCancelled())
                            casHead(h, h.next);     // pop cancelled node

                            // 否则说明 head 正常且与 cur.mode 一致，那么 cur.mode 必然超时；体现了 apparently 的思想
                        else
                            return null;

                        // 否则，如果不会发生超时，或存在一定的超时时间
                        // 尝试将当前结点推入栈中，并等待匹配
                    } else if (casHead(h, s = snode(s, e, h, mode))) {
                        // 调用 LockSupport.park 来阻塞
                        SNode m = awaitFulfill(s, timed, nanos);
                        // 如果 match 的对象就是自己，说明没匹配上，且超时/中断了
                        if (m == s) {               // wait was cancelled
                            // 清除当前结点，并更新 head 指向一个正确有效的结点
                            clean(s);
                            return null;
                        }
                        // 否则说明匹配成功
                        // 判断是否需要更新 head
                        if ((h = head) != null && h.next == s)
                            casHead(h, s.next);     // help s's fulfiller
                        // 根据本次请求的 mode 返回数据
                        // 如果当前请求是 REQUEST，即取值，那么返回另一个结点的 item
                        // 如果当前请求是 DATA，即放值，那么返回当前结点的 item
                        return (E) ((mode == REQUEST) ? m.item : s.item);
                    }

                    // 否则说明此时已经存在了 node，并且 head.mode != cur.mode
                    // 如果此时 head 尚未实现配对

                    // STAGE 2：理论上存在可以配对的结点(eg：你是男的，stack.head 是女的； 理论上你能遇到对象；)
                    // eg：此时你发现 head 还没对象，那么你尝试匹配 head；
                } else if (!isFulfilling(h.mode)) { // try to fulfill
                    // 判断 head 是否已经取消
                    if (h.isCancelled())            // already cancelled
                        casHead(h, h.next);         // pop and retry
                    // 插入当前结点，并尝试进入配对状态
                    // 此时 cur 成为了 head，且标记了 fulfill 状态
                    else if (casHead(h, s=snode(s, e, h, FULFILLING|mode))) {
                        // 一个持续的循环（eg：尝试与 head 发展对象）
                        for (;;) { // loop until matched or waiters disappear
                            // 获取下一个结点
                            SNode m = s.next;       // m is s's match
                            // 如果不存在下一个结点，说明此时 head 被 cancel 了
                            if (m == null) {        // all waiters are gone
                                casHead(s, null);   // pop fulfill node
                                s = null;           // use new node next time
                                break;              // restart main loop
                            }
                            // 再获取一个下一个结点，即 s->head->mn
                            // 理论上此时 m 一定处于沉睡状态
                            SNode mn = m.next;
                            if (m.tryMatch(s)) {
                                casHead(s, mn);     // pop both s and m
                                return (E) ((mode == REQUEST) ? m.item : s.item);
                            } else                  // lost match
                                s.casNext(m, mn);   // help unlink
                        }
                    }

                    // 否则，说明此时 head 已经被别人抢走了，那么主动弹出两个结点；并继续下一轮循环
                } else {                            // help a fulfiller
                    SNode m = h.next;               // m is h's match
                    // 如果 head.next == null，说明此时实际上 stack 为空，那么 cas 更新 head 为 null
                    // 进而触发下一轮的大循环
                    if (m == null)                  // waiter is gone
                        casHead(h, null);           // pop fulfilling node
                    else {
                        // 取到 m.next mn，即 head->m->mn
                        // 理论上 head 状态与 mn 一致，都与本次结点的 mode 相反，可以匹配
                        SNode mn = m.next;
                        // 尝试 match，Match 成功了的话，则更新 head 为 mn，代表 pop 了两个结点
                        if (m.tryMatch(h))          // help match
                            casHead(h, mn);         // pop both h and m
                        else                        // lost match
                            h.casNext(m, mn);       // help unlink
                    }
                }
            }
        }

        /**
         * Spins/blocks until node s is matched by a fulfill operation.
         *
         * @param s the waiting node
         * @param timed true if timed wait
         * @param nanos timeout value
         * @return matched node, or s if cancelled
         */
        SNode awaitFulfill(SNode s, boolean timed, long nanos) {
            /*
             * When a node/thread is about to block, it sets its waiter
             * field and then rechecks state at least one more time
             * before actually parking, thus covering race vs
             * fulfiller noticing that waiter is non-null so should be
             * woken.
             *
             * When invoked by nodes that appear at the point of call
             * to be at the head of the stack, calls to park are
             * preceded by spins to avoid blocking when producers and
             * consumers are arriving very close in time.  This can
             * happen enough to bother only on multiprocessors.
             *
             * The order of checks for returning out of main loop
             * reflects fact that interrupts have precedence over
             * normal returns, which have precedence over
             * timeouts. (So, on timeout, one last check for match is
             * done before giving up.) Except that calls from untimed
             * SynchronousQueue.{poll/offer} don't check interrupts
             * and don't wait at all, so are trapped in transfer
             * method rather than calling awaitFulfill.
             */
            final long deadline = timed ? System.nanoTime() + nanos : 0L;
            Thread w = Thread.currentThread();
            int spins = (shouldSpin(s) ?
                         (timed ? maxTimedSpins : maxUntimedSpins) : 0);
            for (;;) {
                if (w.isInterrupted())
                    s.tryCancel();
                SNode m = s.match;
                // 持续阻塞/自旋，直到超时/匹配完成
                // m 代表匹配的结果，如果超时，那么当前结点就会 tryCancel 设置 match 为自己，表示失败
                if (m != null)
                    return m;
                if (timed) {
                    nanos = deadline - System.nanoTime();
                    if (nanos <= 0L) {
                        s.tryCancel();
                        continue;
                    }
                }
                if (spins > 0)
                    spins = shouldSpin(s) ? (spins-1) : 0;
                else if (s.waiter == null)
                    s.waiter = w; // establish waiter so can park next iter
                else if (!timed)
                    LockSupport.park(this);
                else if (nanos > spinForTimeoutThreshold)
                    LockSupport.parkNanos(this, nanos);
            }
        }

        /**
         * Returns true if node s is at head or there is an active
         * fulfiller.
         */
        boolean shouldSpin(SNode s) {
            SNode h = head;
            return (h == s || h == null || isFulfilling(h.mode));
        }

        /**
         * Unlinks s from the stack.
         */
        void clean(SNode s) {
            s.item = null;   // forget item
            s.waiter = null; // forget thread

            /*
             * At worst we may need to traverse entire stack to unlink
             * s. If there are multiple concurrent calls to clean, we
             * might not see s if another thread has already removed
             * it. But we can stop when we see any node known to
             * follow s. We use s.next unless it too is cancelled, in
             * which case we try the node one past. We don't check any
             * further because we don't want to doubly traverse just to
             * find sentinel.
             */

            SNode past = s.next;
            if (past != null && past.isCancelled())
                past = past.next;

            // Absorb cancelled nodes at head
            SNode p;
            while ((p = head) != null && p != past && p.isCancelled())
                casHead(p, p.next);

            // Unsplice embedded nodes
            while (p != null && p != past) {
                SNode n = p.next;
                if (n != null && n.isCancelled())
                    p.casNext(n, n.next);
                else
                    p = n;
            }
        }

        // Unsafe mechanics
        private static final sun.misc.Unsafe UNSAFE;
        private static final long headOffset;
        static {
            try {
                UNSAFE = sun.misc.Unsafe.getUnsafe();
                Class<?> k = TransferStack.class;
                headOffset = UNSAFE.objectFieldOffset
                    (k.getDeclaredField("head"));
            } catch (Exception e) {
                throw new Error(e);
            }
        }
    }

    /** Dual Queue */
    static final class TransferQueue<E> extends Transferer<E> {
        /*
         * This extends Scherer-Scott dual queue algorithm, differing,
         * among other ways, by using modes within nodes rather than
         * marked pointers. The algorithm is a little simpler than
         * that for stacks because fulfillers do not need explicit
         * nodes, and matching is done by CAS'ing QNode.item field
         * from non-null to null (for put) or vice versa (for take).
         * <p>
         * 这扩展了 Scherer-Scott 双队列算法，通过在节点中使用模式而不是标记指针等方式进行区分。
         * 该算法比栈的算法简单一些，因为完成者不需要显式节点，并且匹配是通过将 QNode.item 字段从非空 CAS 为 null（对于 put）或反之（对于 take）来完成的。
         */

        /** Node class for TransferQueue. */
        static final class QNode {
            volatile QNode next;          // next node in queue
            volatile Object item;         // CAS'ed to or from null
            volatile Thread waiter;       // to control park/unpark
            final boolean isData;

            QNode(Object item, boolean isData) {
                this.item = item;
                this.isData = isData;
            }

            // 更新 next 指针，cas 保证明确是否实现
            boolean casNext(QNode cmp, QNode val) {
                return next == cmp &&
                    UNSAFE.compareAndSwapObject(this, nextOffset, cmp, val);
            }

            boolean casItem(Object cmp, Object val) {
                return item == cmp &&
                    UNSAFE.compareAndSwapObject(this, itemOffset, cmp, val);
            }

            /**
             * Tries to cancel by CAS'ing ref to this as item.
             * <p>
             *     通过将对此的引用 CAS 为 item 来尝试取消。
             */
            void tryCancel(Object cmp) {
                UNSAFE.compareAndSwapObject(this, itemOffset, cmp, this);
            }

            boolean isCancelled() {
                return item == this;
            }

            /**
             * Returns true if this node is known to be off the queue
             * because its next pointer has been forgotten due to
             * an advanceHead operation.
             * <p>
             *     如果由于 advanceHead 操作而忘记了其 next 指针，因此已知此结点已从队列中移除，则返回 true。
             */
            boolean isOffList() {
                return next == this;
            }

            // Unsafe mechanics
            private static final sun.misc.Unsafe UNSAFE;
            private static final long itemOffset;
            private static final long nextOffset;

            static {
                try {
                    UNSAFE = sun.misc.Unsafe.getUnsafe();
                    Class<?> k = QNode.class;
                    itemOffset = UNSAFE.objectFieldOffset
                        (k.getDeclaredField("item"));
                    nextOffset = UNSAFE.objectFieldOffset
                        (k.getDeclaredField("next"));
                } catch (Exception e) {
                    throw new Error(e);
                }
            }
        }

        /**
         * Head of queue
         * <p>
         *     队列的头结点
         */
        transient volatile QNode head;
        /**
         * Tail of queue
         * <p>
         *     队列的尾结点
         */
        transient volatile QNode tail;
        /**
         * Reference to a cancelled node that might not yet have been
         * unlinked from queue because it was the last inserted node
         * when it was cancelled.
         * <p>
         *     取消的结点的引用，该结点可能尚未从队列中取消链接，因为当它被取消时，它是最后插入的结点。
         */
        transient volatile QNode cleanMe;

        TransferQueue() {
            QNode h = new QNode(null, false); // initialize to dummy node.
            // head 和 tail 同时指向一个 dummy 结点
            head = h;
            tail = h;
        }

        /**
         * Tries to cas nh as new head; if successful, unlink
         * old head's next node to avoid garbage retention.
         * <p>
         *     尝试将 nh 作为新的 head；
         *     如果成功，则取消链接旧 head 的下一个结点，以避免保留垃圾。
         */
        void advanceHead(QNode h, QNode nh) {
            if (h == head &&
                UNSAFE.compareAndSwapObject(this, headOffset, h, nh))
                h.next = h; // forget old next
        }

        /**
         * Tries to cas nt as new tail.
         * <p>
         *     尝试将 nt 作为新的 tail。
         */
        void advanceTail(QNode t, QNode nt) {
            if (tail == t)
                UNSAFE.compareAndSwapObject(this, tailOffset, t, nt);
        }

        /**
         * Tries to CAS cleanMe slot.
         * <p>
         *     尝试 CAS 更新 cleanMe 插槽。
         */
        boolean casCleanMe(QNode cmp, QNode val) {
            return cleanMe == cmp &&
                UNSAFE.compareAndSwapObject(this, cleanMeOffset, cmp, val);
        }

        /**
         * Puts or takes an item.
         * <p>
         *     放置或获取一个项目。
         */
        @SuppressWarnings("unchecked")
        E transfer(E e, boolean timed, long nanos) {
            /* Basic algorithm is to loop trying to take either of
             * two actions:
             *
             * 1. If queue apparently empty or holding same-mode nodes,
             *    try to add node to queue of waiters, wait to be
             *    fulfilled (or cancelled) and return matching item.
             *
             * 2. If queue apparently contains waiting items, and this
             *    call is of complementary mode, try to fulfill by CAS'ing
             *    item field of waiting node and dequeuing it, and then
             *    returning matching item.
             *
             * In each case, along the way, check for and try to help
             * advance head and tail on behalf of other stalled/slow
             * threads.
             *
             * The loop starts off with a null check guarding against
             * seeing uninitialized head or tail values. This never
             * happens in current SynchronousQueue, but could if
             * callers held non-volatile/final ref to the
             * transferer. The check is here anyway because it places
             * null checks at top of loop, which is usually faster
             * than having them implicitly interspersed.
             * <p>
             * 基本算法是循环尝试执行以下两种操作之一：
             * 1. 如果队列显然为空或包含相同模式的结点，尝试将结点添加到等待者队列中，等待被完成（或取消），并返回匹配的项目。
             * 2. 如果队列显然包含等待项目，并且此调用是 complementary mode，则尝试通过 CAS'ing 等待结点的 item 字段来完成，并将其出队，然后返回匹配的项目。
             *
             * 在每种情况下，沿途检查并尝试帮助代表其他停滞/缓慢的线程推进 head 和 tail。
             *
             * 循环从一个空检查开始，防止看到未初始化的 head 或 tail 值。
             * 这在当前的 SynchronousQueue 中永远不会发生，但是如果调用者持有非易失性/最终引用传输器，则可能会发生。
             * 无论如何，这里的检查都是因为它将空检查放在循环顶部，这通常比隐式交错的检查更快。
             */

            QNode s = null; // constructed/reused as needed
            // 根据入参 e 是否为 null，判断本次操作是 put 还是 take
            boolean isData = (e != null);

            // 持续循环
            for (;;) {
                // 获取首尾结点
                QNode t = tail;
                QNode h = head;
                // 如果 tail 和 head 是 null，那么就自旋；
                // 等待 head 和 tail 初始化
                if (t == null || h == null)         // saw uninitialized value
                    continue;                       // spin

                // 如果队列为空，或队尾的 mode 与当前操作的 mode 一致
                // 意味着此时必须将当前结点构造为 node，插入队列，一起阻塞
                if (h == t || t.isData == isData) { // empty or same-mode
                    QNode tn = t.next;
                    // 如果有其他线程插入了结点，那么就触发重试循环
                    if (t != tail)                  // inconsistent read
                        continue;
                    // 如果 tail.next 不为空，说明 tail 此时并没有正确地指向队尾，那么帮助其向后移动一位
                    // 然后自己重试
                    if (tn != null) {               // lagging tail
                        advanceTail(t, tn);
                        continue;
                    }
                    // 如果存在等待时间 0，那么此时必定超时，立即返回 null
                    if (timed && nanos <= 0)        // can't wait
                        return null;
                    // 然后为当前插入的内容构造一个新的结点
                    if (s == null)
                        s = new QNode(e, isData);
                    // 更新 tail 的 next 指针；如果失败了，那么就说明存在其他线程插入，那么就重试
                    if (!t.casNext(null, s))        // failed to link in
                        continue;

                    // cas 成功了，那么就更新 tail 指针
                    advanceTail(t, s);              // swing tail and wait
                    // 等待知道被匹配或被取消
                    Object x = awaitFulfill(s, e, timed, nanos);
                    // x == s 表示被取消了，那么就需要 clean 当前结点，并更新 tail 指针
                    if (x == s) {                   // wait was cancelled
                        clean(t, s);
                        return null;
                    }

                    // 如果 s 此时仍处于队列中，但其实此时它已经被匹配了
                    if (!s.isOffList()) {           // not already unlinked
                        // 如果此时 tail 是 head，那么就更新 head 指针，指向 s
                        advanceHead(t, s);          // unlink if head
                        if (x != null)              // and forget fields
                            s.item = s;
                        s.waiter = null;
                    }
                    return (x != null) ? (E)x : e;

                    // 否则，说明队列不空，且 tail.mode != cur.mode，即此时队列中的元素是可以匹配的
                } else {                            // complementary-mode
                    QNode m = h.next;               // node to fulfill
                    // 判断是否有其他线程提前完成了匹配，如果是的话，那么就触发重新循环
                    if (t != tail || m == null || h != head)
                        continue;                   // inconsistent read

                    // 判断 m 是否已经被别人匹配走了
                    Object x = m.item;
                    if (isData == (x != null) ||    // m already fulfilled
                        x == m ||                   // m cancelled
                        !m.casItem(x, e)) {         // lost CAS
                        // 将 head 指向下一个结点
                        advanceHead(h, m);          // dequeue and retry
                        continue;
                    }

                    // 此时说明匹配成功，上面的 casItem 已经成功了
                    // 那么就唤醒 m 对应的线程
                    advanceHead(h, m);              // successfully fulfilled
                    LockSupport.unpark(m.waiter);
                    return (x != null) ? (E)x : e;
                }
            }
        }

        /**
         * Spins/blocks until node s is fulfilled.
         *
         * @param s the waiting node
         * @param e the comparison value for checking match
         * @param timed true if timed wait
         * @param nanos timeout value
         * @return matched item, or s if cancelled
         */
        Object awaitFulfill(QNode s, E e, boolean timed, long nanos) {
            /* Same idea as TransferStack.awaitFulfill */
            final long deadline = timed ? System.nanoTime() + nanos : 0L;
            Thread w = Thread.currentThread();
            int spins = ((head.next == s) ?
                         (timed ? maxTimedSpins : maxUntimedSpins) : 0);
            for (;;) {
                if (w.isInterrupted())
                    s.tryCancel(e);
                Object x = s.item;
                if (x != e)
                    return x;
                if (timed) {
                    nanos = deadline - System.nanoTime();
                    if (nanos <= 0L) {
                        s.tryCancel(e);
                        continue;
                    }
                }
                if (spins > 0)
                    --spins;
                else if (s.waiter == null)
                    s.waiter = w;
                else if (!timed)
                    LockSupport.park(this);
                else if (nanos > spinForTimeoutThreshold)
                    LockSupport.parkNanos(this, nanos);
            }
        }

        /**
         * Gets rid of cancelled node s with original predecessor pred.
         */
        void clean(QNode pred, QNode s) {
            s.waiter = null; // forget thread
            /*
             * At any given time, exactly one node on list cannot be
             * deleted -- the last inserted node. To accommodate this,
             * if we cannot delete s, we save its predecessor as
             * "cleanMe", deleting the previously saved version
             * first. At least one of node s or the node previously
             * saved can always be deleted, so this always terminates.
             */
            while (pred.next == s) { // Return early if already unlinked
                QNode h = head;
                QNode hn = h.next;   // Absorb cancelled first node as head
                if (hn != null && hn.isCancelled()) {
                    advanceHead(h, hn);
                    continue;
                }
                QNode t = tail;      // Ensure consistent read for tail
                if (t == h)
                    return;
                QNode tn = t.next;
                if (t != tail)
                    continue;
                if (tn != null) {
                    advanceTail(t, tn);
                    continue;
                }
                if (s != t) {        // If not tail, try to unsplice
                    QNode sn = s.next;
                    if (sn == s || pred.casNext(s, sn))
                        return;
                }
                QNode dp = cleanMe;
                if (dp != null) {    // Try unlinking previous cancelled node
                    QNode d = dp.next;
                    QNode dn;
                    if (d == null ||               // d is gone or
                        d == dp ||                 // d is off list or
                        !d.isCancelled() ||        // d not cancelled or
                        (d != t &&                 // d not tail and
                         (dn = d.next) != null &&  //   has successor
                         dn != d &&                //   that is on list
                         dp.casNext(d, dn)))       // d unspliced
                        casCleanMe(dp, null);
                    if (dp == pred)
                        return;      // s is already saved node
                } else if (casCleanMe(null, pred))
                    return;          // Postpone cleaning s
            }
        }

        private static final sun.misc.Unsafe UNSAFE;
        private static final long headOffset;
        private static final long tailOffset;
        private static final long cleanMeOffset;
        static {
            try {
                UNSAFE = sun.misc.Unsafe.getUnsafe();
                Class<?> k = TransferQueue.class;
                headOffset = UNSAFE.objectFieldOffset
                    (k.getDeclaredField("head"));
                tailOffset = UNSAFE.objectFieldOffset
                    (k.getDeclaredField("tail"));
                cleanMeOffset = UNSAFE.objectFieldOffset
                    (k.getDeclaredField("cleanMe"));
            } catch (Exception e) {
                throw new Error(e);
            }
        }
    }

    /**
     * The transferer. Set only in constructor, but cannot be declared
     * as final without further complicating serialization.  Since
     * this is accessed only at most once per public method, there
     * isn't a noticeable performance penalty for using volatile
     * instead of final here.
     * <p>
     *     传输器。
     *     仅在构造函数中设置，但是如果不进一步复杂化序列化，就不能声明为 final。
     *     由于每个公共方法最多只访问一次，因此在这里使用 volatile 而不是 final 不会有明显的性能损失。
     */
    private transient volatile Transferer<E> transferer;

    /**
     * Creates a {@code SynchronousQueue} with nonfair access policy.
     * <p>
     *     使用非公平访问策略创建一个 SynchronousQueue
     */
    public SynchronousQueue() {
        this(false);
    }

    /**
     * Creates a {@code SynchronousQueue} with the specified fairness policy.
     * <p>
     *     使用指定的公平策略创建一个 SynchronousQueue
     *      公平 -> TransferQueue
     *      非公平 -> TransferStack
     *
     * @param fair if true, waiting threads contend in FIFO order for
     *        access; otherwise the order is unspecified.
     */
    public SynchronousQueue(boolean fair) {
        transferer = fair ? new TransferQueue<E>() : new TransferStack<E>();
    }

    /**
     * Adds the specified element to this queue, waiting if necessary for
     * another thread to receive it.
     * <p>
     *     将指定的元素添加到此队列中，必要时等待另一个线程接收它。
     *
     * @throws InterruptedException {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     */
    public void put(E e) throws InterruptedException {
        if (e == null) throw new NullPointerException();
        // 调用 transfer 方法
        if (transferer.transfer(e, false, 0) == null) {
            Thread.interrupted();
            throw new InterruptedException();
        }
    }

    /**
     * Inserts the specified element into this queue, waiting if necessary
     * up to the specified wait time for another thread to receive it.
     * <p>
     *     将指定的元素插入此队列中，必要时等待另一个线程在指定的等待时间内接收它。
     *
     * @return {@code true} if successful, or {@code false} if the
     *         specified waiting time elapses before a consumer appears
     * @throws InterruptedException {@inheritDoc}
     * @throws NullPointerException {@inheritDoc}
     */
    public boolean offer(E e, long timeout, TimeUnit unit)
        throws InterruptedException {
        if (e == null) throw new NullPointerException();
        // 调用 transfer 方法
        if (transferer.transfer(e, true, unit.toNanos(timeout)) != null)
            return true;
        if (!Thread.interrupted())
            return false;
        throw new InterruptedException();
    }

    /**
     * Inserts the specified element into this queue, if another thread is
     * waiting to receive it.
     * <p>
     *     如果另一个线程正在等待接收它，则将指定的元素插入此队列中。
     *
     * @param e the element to add
     * @return {@code true} if the element was added to this queue, else
     *         {@code false}
     * @throws NullPointerException if the specified element is null
     */
    public boolean offer(E e) {
        if (e == null) throw new NullPointerException();
        return transferer.transfer(e, true, 0) != null;
    }

    /**
     * Retrieves and removes the head of this queue, waiting if necessary
     * for another thread to insert it.
     * <p>
     *     检索并删除此队列的头部，必要时等待另一个线程插入它。
     *
     * @return the head of this queue
     * @throws InterruptedException {@inheritDoc}
     */
    public E take() throws InterruptedException {
        E e = transferer.transfer(null, false, 0);
        if (e != null)
            return e;
        Thread.interrupted();
        throw new InterruptedException();
    }

    /**
     * Retrieves and removes the head of this queue, waiting
     * if necessary up to the specified wait time, for another thread
     * to insert it.
     *
     * @return the head of this queue, or {@code null} if the
     *         specified waiting time elapses before an element is present
     * @throws InterruptedException {@inheritDoc}
     */
    public E poll(long timeout, TimeUnit unit) throws InterruptedException {
        E e = transferer.transfer(null, true, unit.toNanos(timeout));
        if (e != null || !Thread.interrupted())
            return e;
        throw new InterruptedException();
    }

    /**
     * Retrieves and removes the head of this queue, if another thread
     * is currently making an element available.
     *
     * @return the head of this queue, or {@code null} if no
     *         element is available
     */
    public E poll() {
        return transferer.transfer(null, true, 0);
    }

    /**
     * Always returns {@code true}.
     * A {@code SynchronousQueue} has no internal capacity.
     * <p>
     *     始终返回 true。
     *     SynchronousQueue 没有内部容量。
     *
     * @return {@code true}
     */
    public boolean isEmpty() {
        return true;
    }

    /**
     * Always returns zero.
     * A {@code SynchronousQueue} has no internal capacity.
     * <p>
     *     始终返回零。
     *     SynchronousQueue 没有内部容量。
     *
     * @return zero
     */
    public int size() {
        return 0;
    }

    /**
     * Always returns zero.
     * A {@code SynchronousQueue} has no internal capacity.
     *
     * @return zero
     */
    public int remainingCapacity() {
        return 0;
    }

    /**
     * Does nothing.
     * A {@code SynchronousQueue} has no internal capacity.
     */
    public void clear() {
    }

    /**
     * Always returns {@code false}.
     * A {@code SynchronousQueue} has no internal capacity.
     *
     * @param o the element
     * @return {@code false}
     */
    public boolean contains(Object o) {
        return false;
    }

    /**
     * Always returns {@code false}.
     * A {@code SynchronousQueue} has no internal capacity.
     *
     * @param o the element to remove
     * @return {@code false}
     */
    public boolean remove(Object o) {
        return false;
    }

    /**
     * Returns {@code false} unless the given collection is empty.
     * A {@code SynchronousQueue} has no internal capacity.
     *
     * @param c the collection
     * @return {@code false} unless given collection is empty
     */
    public boolean containsAll(Collection<?> c) {
        return c.isEmpty();
    }

    /**
     * Always returns {@code false}.
     * A {@code SynchronousQueue} has no internal capacity.
     *
     * @param c the collection
     * @return {@code false}
     */
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    /**
     * Always returns {@code false}.
     * A {@code SynchronousQueue} has no internal capacity.
     *
     * @param c the collection
     * @return {@code false}
     */
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    /**
     * Always returns {@code null}.
     * A {@code SynchronousQueue} does not return elements
     * unless actively waited on.
     *
     * @return {@code null}
     */
    public E peek() {
        return null;
    }

    /**
     * Returns an empty iterator in which {@code hasNext} always returns
     * {@code false}.
     *
     * @return an empty iterator
     */
    public Iterator<E> iterator() {
        return Collections.emptyIterator();
    }

    /**
     * Returns an empty spliterator in which calls to
     * {@link java.util.Spliterator#trySplit()} always return {@code null}.
     *
     * @return an empty spliterator
     * @since 1.8
     */
    public Spliterator<E> spliterator() {
        return Spliterators.emptySpliterator();
    }

    /**
     * Returns a zero-length array.
     * @return a zero-length array
     */
    public Object[] toArray() {
        return new Object[0];
    }

    /**
     * Sets the zeroeth element of the specified array to {@code null}
     * (if the array has non-zero length) and returns it.
     *
     * @param a the array
     * @return the specified array
     * @throws NullPointerException if the specified array is null
     */
    public <T> T[] toArray(T[] a) {
        if (a.length > 0)
            a[0] = null;
        return a;
    }

    /**
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     */
    public int drainTo(Collection<? super E> c) {
        if (c == null)
            throw new NullPointerException();
        if (c == this)
            throw new IllegalArgumentException();
        int n = 0;
        for (E e; (e = poll()) != null;) {
            c.add(e);
            ++n;
        }
        return n;
    }

    /**
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     */
    public int drainTo(Collection<? super E> c, int maxElements) {
        if (c == null)
            throw new NullPointerException();
        if (c == this)
            throw new IllegalArgumentException();
        int n = 0;
        for (E e; n < maxElements && (e = poll()) != null;) {
            c.add(e);
            ++n;
        }
        return n;
    }

    /*
     * To cope with serialization strategy in the 1.5 version of
     * SynchronousQueue, we declare some unused classes and fields
     * that exist solely to enable serializability across versions.
     * These fields are never used, so are initialized only if this
     * object is ever serialized or deserialized.
     * <p>
     * 为了应对 1.5 版本 SynchronousQueue 中的序列化策略，我们声明了一些未使用的类和字段，
     * 这些类和字段仅用于在版本之间实现可序列化。
     */

    @SuppressWarnings("serial")
    static class WaitQueue implements java.io.Serializable { }
    static class LifoWaitQueue extends WaitQueue {
        private static final long serialVersionUID = -3633113410248163686L;
    }
    static class FifoWaitQueue extends WaitQueue {
        private static final long serialVersionUID = -3623113410248163686L;
    }
    private ReentrantLock qlock;
    // 这两个 queue 实际没用，只是为了兼容序列化
    private WaitQueue waitingProducers;
    private WaitQueue waitingConsumers;

    /**
     * Saves this queue to a stream (that is, serializes it).
     * @param s the stream
     * @throws java.io.IOException if an I/O error occurs
     */
    private void writeObject(java.io.ObjectOutputStream s)
        throws java.io.IOException {
        boolean fair = transferer instanceof TransferQueue;
        if (fair) {
            qlock = new ReentrantLock(true);
            waitingProducers = new FifoWaitQueue();
            waitingConsumers = new FifoWaitQueue();
        }
        else {
            qlock = new ReentrantLock();
            waitingProducers = new LifoWaitQueue();
            waitingConsumers = new LifoWaitQueue();
        }
        s.defaultWriteObject();
    }

    /**
     * Reconstitutes this queue from a stream (that is, deserializes it).
     * @param s the stream
     * @throws ClassNotFoundException if the class of a serialized object
     *         could not be found
     * @throws java.io.IOException if an I/O error occurs
     */
    private void readObject(java.io.ObjectInputStream s)
        throws java.io.IOException, ClassNotFoundException {
        s.defaultReadObject();
        if (waitingProducers instanceof FifoWaitQueue)
            transferer = new TransferQueue<E>();
        else
            transferer = new TransferStack<E>();
    }

    // Unsafe mechanics
    static long objectFieldOffset(sun.misc.Unsafe UNSAFE,
                                  String field, Class<?> klazz) {
        try {
            return UNSAFE.objectFieldOffset(klazz.getDeclaredField(field));
        } catch (NoSuchFieldException e) {
            // Convert Exception to corresponding Error
            NoSuchFieldError error = new NoSuchFieldError(field);
            error.initCause(e);
            throw error;
        }
    }

}
