/*
 * Copyright (c) 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.util;

/**
 * {@code StringJoiner} is used to construct a sequence of characters separated
 * by a delimiter and optionally starting with a supplied prefix
 * and ending with a supplied suffix.
 * <p>
 * StringJoiner 用于构造由指定 delimiter 分隔的 char sequence，
 * 可选择指定的 prefix 和 suffix。
 * <p>
 * Prior to adding something to the {@code StringJoiner}, its
 * {@code sj.toString()} method will, by default, return {@code prefix + suffix}.
 * However, if the {@code setEmptyValue} method is called, the {@code emptyValue}
 * supplied will be returned instead. This can be used, for example, when
 * creating a string using set notation to indicate an empty set, i.e.
 * <code>"{}"</code>, where the {@code prefix} is <code>"{"</code>, the
 * {@code suffix} is <code>"}"</code> and nothing has been added to the
 * {@code StringJoiner}.
 * <p>
 * 如果在向 StringJoiner 添加元素之前，调用 toString 方法，默认返回 prefix + suffix。
 * 但是，如果调用 setEmptyValue 方法设定了默认值，将返回指定的 emptyValue。
 * <p>
 * 例如，当创建一个空集合时，可以使用 setEmptyValue 方法，指定 emptyValue 为 "{}"，
 * prefix 为 "{"，suffix 为 "}"，这样当没有元素添加到 StringJoiner 时，toString 方法将返回 "{}"。
 *
 * @apiNote <p>The String {@code "[George:Sally:Fred]"} may be constructed as follows:
 *
 * <pre> {@code
 * StringJoiner sj = new StringJoiner(":", "[", "]");
 * sj.add("George").add("Sally").add("Fred");
 * String desiredString = sj.toString();
 * }</pre>
 * <p>
 * A {@code StringJoiner} may be employed to create formatted output from a
 * {@link java.util.stream.Stream} using
 * {@link java.util.stream.Collectors#joining(CharSequence)}. For example:
 *
 * <pre> {@code
 * List<Integer> numbers = Arrays.asList(1, 2, 3, 4);
 * String commaSeparatedNumbers = numbers.stream()
 *     .map(i -> i.toString())
 *     .collect(Collectors.joining(", "));
 * }</pre>
 * @see java.util.stream.Collectors#joining(CharSequence)
 * @see java.util.stream.Collectors#joining(CharSequence, CharSequence, CharSequence)
 * @since 1.8
 */
public final class StringJoiner {
    // 前缀
    private final String prefix;
    // 分隔符
    private final String delimiter;
    // 后缀
    private final String suffix;

    /*
     * StringBuilder value -- at any time, the characters constructed from the
     * prefix, the added element separated by the delimiter, but without the
     * suffix, so that we can more easily add elements without having to jigger
     * the suffix each time.
     *
     * 任何时候，value 中都包含了 prefix，由 delimiter 分割的 element，但是没有 suffix。
     * 这样每次 add 值的时候可以更加方便
     */
    private StringBuilder value;

    /*
     * By default, the string consisting of prefix+suffix, returned by
     * toString(), or properties of value, when no elements have yet been added,
     * i.e. when it is empty.  This may be overridden by the user to be some
     * other value including the empty String.
     *
     * 默认情况下，emptyValue=prefix+suffix；如果还没有 add element，那么此时调用 toString 方法就会返回该值
     */
    private String emptyValue;

    /**
     * Constructs a {@code StringJoiner} with no characters in it, with no
     * {@code prefix} or {@code suffix}, and a copy of the supplied
     * {@code delimiter}.
     * <p>
     * 构造一个不包含任何 characters 的 StringJoiner，没有 prefix 和 suffix，仅仅指定了 delimiter
     * <p>
     * <p>
     * If no characters are added to the {@code StringJoiner} and methods
     * accessing the value of it are invoked, it will not return a
     * {@code prefix} or {@code suffix} (or properties thereof) in the result,
     * unless {@code setEmptyValue} has first been called.
     *
     * @param delimiter the sequence of characters to be used between each
     *                  element added to the {@code StringJoiner} value
     * @throws NullPointerException if {@code delimiter} is {@code null}
     */
    public StringJoiner(CharSequence delimiter) {
        this(delimiter, "", "");
    }

    /**
     * Constructs a {@code StringJoiner} with no characters in it using copies
     * of the supplied {@code prefix}, {@code delimiter} and {@code suffix}.
     * If no characters are added to the {@code StringJoiner} and methods
     * accessing the string value of it are invoked, it will return the
     * {@code prefix + suffix} (or properties thereof) in the result, unless
     * {@code setEmptyValue} has first been called.
     * <p>
     * 构造一个 StringJoiner，指定了 prefix，delimiter，suffix。
     * 如果没有 add element，那么调用 toString 方法将返回 prefix + suffix，除非调用了 setEmptyValue 方法。
     *
     * @param delimiter the sequence of characters to be used between each
     *                  element added to the {@code StringJoiner}
     * @param prefix    the sequence of characters to be used at the beginning
     * @param suffix    the sequence of characters to be used at the end
     * @throws NullPointerException if {@code prefix}, {@code delimiter}, or
     *                              {@code suffix} is {@code null}
     */
    public StringJoiner(CharSequence delimiter,
                        CharSequence prefix,
                        CharSequence suffix) {
        Objects.requireNonNull(prefix, "The prefix must not be null");
        Objects.requireNonNull(delimiter, "The delimiter must not be null");
        Objects.requireNonNull(suffix, "The suffix must not be null");
        // make defensive copies of arguments
        this.prefix = prefix.toString();
        this.delimiter = delimiter.toString();
        this.suffix = suffix.toString();
        this.emptyValue = this.prefix + this.suffix;
    }

    /**
     * Sets the sequence of characters to be used when determining the string
     * representation of this {@code StringJoiner} and no elements have been
     * added yet, that is, when it is empty.  A copy of the {@code emptyValue}
     * parameter is made for this purpose. Note that once an add method has been
     * called, the {@code StringJoiner} is no longer considered empty, even if
     * the element(s) added correspond to the empty {@code String}.
     * <p>
     * 设置 emptyValue，当 StringJoiner 没有 add element 时，调用 toString 方法将返回该值。
     * 注意，一旦调用了 add 方法，StringJoiner 就不再是 empty，即使 add 的是空字符串。
     *
     * @param emptyValue the characters to return as the value of an empty
     *                   {@code StringJoiner}
     * @return this {@code StringJoiner} itself so the calls may be chained
     * @throws NullPointerException when the {@code emptyValue} parameter is
     *                              {@code null}
     */
    public StringJoiner setEmptyValue(CharSequence emptyValue) {
        this.emptyValue = Objects.requireNonNull(emptyValue,
                "The empty value must not be null").toString();
        return this;
    }

    /**
     * Returns the current value, consisting of the {@code prefix}, the values
     * added so far separated by the {@code delimiter}, and the {@code suffix},
     * unless no elements have been added in which case, the
     * {@code prefix + suffix} or the {@code emptyValue} characters are returned
     * <p>
     * 返回当前的 value，包含 prefix，delimiter 分隔的 element，suffix。
     * 如果没有 add element，那么返回 prefix + suffix 或者 emptyValue。
     *
     * @return the string representation of this {@code StringJoiner}
     */
    @Override
    public String toString() {
        // 如果 value 为 null，说明没有 add element，返回 emptyValue
        if (value == null) {
            return emptyValue;
        } else {
            // 如果没有设置 suffix，那么直接返回 value（value 是一个 StringBuilder，本身就没有存 suffix）
            if (suffix.equals("")) {
                return value.toString();
            } else {
                // 记录 value 的长度，因为后面要 append suffix，所以要先记录长度
                int initialLength = value.length();
                String result = value.append(suffix).toString();
                // reset value to pre-append initialLength
                // 重置 value 的长度，底层其实就是更新 StringBuilder 的 char 数组的有效长度；因为 toString 之后可能还会继续 add，必须复原
                value.setLength(initialLength);
                return result;
            }
        }
    }

    /**
     * Adds a copy of the given {@code CharSequence} value as the next
     * element of the {@code StringJoiner} value. If {@code newElement} is
     * {@code null}, then {@code "null"} is added.
     * <p>
     * 添加一个 CharSequence 类型的 element 到 StringJoiner。
     * 如果 newElement 为 null，那么添加 "null"。
     *
     * @param newElement The element to add
     * @return a reference to this {@code StringJoiner}
     */
    public StringJoiner add(CharSequence newElement) {
        prepareBuilder().append(newElement);
        return this;
    }

    /**
     * Adds the contents of the given {@code StringJoiner} without prefix and
     * suffix as the next element if it is non-empty. If the given {@code
     * StringJoiner} is empty, the call has no effect.
     * <p>
     * 添加一个 StringJoiner 的内容，不包含 prefix 和 suffix。
     * 如果 StringJoiner 为空，那么没有任何效果。
     *
     * <p>A {@code StringJoiner} is empty if {@link #add(CharSequence) add()}
     * has never been called, and if {@code merge()} has never been called
     * with a non-empty {@code StringJoiner} argument.
     * <p>
     * StringJoiner 为空的条件：
     * 1. 没有调用过 add 方法
     * 2. 没有调用过 merge 方法，或者调用过 merge 方法，但是传入的 StringJoiner 为空
     *
     * <p>If the other {@code StringJoiner} is using a different delimiter,
     * then elements from the other {@code StringJoiner} are concatenated with
     * that delimiter and the result is appended to this {@code StringJoiner}
     * as a single element.
     * <p>
     * 如果 other StringJoiner 使用了不同的 delimiter，那么 other StringJoiner 的 element 会使用 other delimiter 进行拼接，
     * 然后作为一个 element 添加到当前 StringJoiner。
     *
     * @param other The {@code StringJoiner} whose contents should be merged
     *              into this one
     * @return This {@code StringJoiner}
     * @throws NullPointerException if the other {@code StringJoiner} is null
     */
    public StringJoiner merge(StringJoiner other) {
        // other 不能为 null
        Objects.requireNonNull(other);
        // 如果 other 是空的，那么直接返回了
        if (other.value != null) {
            // 记录下 other 的长度
            final int length = other.value.length();
            // lock the length so that we can seize the data to be appended
            // before initiate copying to avoid interference, especially when
            // merge 'this'
            // 锁定 other 的长度，以便在开始复制之前获取要附加的数据，以避免干扰，特别是当 merge this 的时候
            StringBuilder builder = prepareBuilder();
            // 将 other 的除 prefix 外的 value 作为一个 element 添加到 this
            builder.append(other.value, other.prefix.length(), length);
        }
        return this;
    }

    private StringBuilder prepareBuilder() {
        if (value != null) {
            // 添加分隔符
            value.append(delimiter);
        } else {
            // 如果 value 为 null，说明还没有 add element，那么先初始化，然后添加 prefix
            value = new StringBuilder().append(prefix);
        }
        return value;
    }

    /**
     * Returns the length of the {@code String} representation
     * of this {@code StringJoiner}. Note that if
     * no add methods have been called, then the length of the {@code String}
     * representation (either {@code prefix + suffix} or {@code emptyValue})
     * will be returned. The value should be equivalent to
     * {@code toString().length()}.
     * <p>
     *     返回 StringJoiner 的长度。
     *     注意，如果没有调用过 add 方法，那么返回的是 prefix + suffix 或者 emptyValue 的长度。
     *     这个值应该等价于 toString().length()。
     *
     * @return the length of the current value of {@code StringJoiner}
     */
    public int length() {
        // Remember that we never actually append the suffix unless we return
        // the full (present) value or some sub-string or length of it, so that
        // we can add on more if we need to.
        // 记住，我们实际上从来没有添加过 suffix，除非我们返回了完整的 value，或者 value 的某个子串，或者 value 的长度。
        return (value != null ? value.length() + suffix.length() :
                emptyValue.length());
    }
}
