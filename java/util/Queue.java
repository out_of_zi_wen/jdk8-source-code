/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 */

package java.util;

/**
 * A collection designed for holding elements prior to processing.
 * Besides basic {@link java.util.Collection Collection} operations,
 * queues provide additional insertion, extraction, and inspection
 * operations.  Each of these methods exists in two forms: one throws
 * an exception if the operation fails, the other returns a special
 * value (either {@code null} or {@code false}, depending on the
 * operation).  The latter form of the insert operation is designed
 * specifically for use with capacity-restricted {@code Queue}
 * implementations; in most implementations, insert operations cannot
 * fail.
 * <p>
 * 用于在处理之前保存元素的集合。
 * 除了基本的 Collection 操作之外，队列还提供了额外的插入、提取和检查操作。
 * 这些方法中的每一个都有两种形式：一种在操作失败时抛出异常，另一种返回一个特殊值（根据操作的不同，可能是 null 或 false）。
 * 插入操作的后一种形式专门用于容量受限的队列实现；在大多数实现中，插入操作不会失败。
 * <p>
 * <table BORDER CELLPADDING=3 CELLSPACING=1>
 * <caption>Summary of Queue methods</caption>
 * <tr>
 * <td></td>
 * <td ALIGN=CENTER><em>Throws exception</em></td>
 * <td ALIGN=CENTER><em>Returns special value</em></td>
 * </tr>
 * <tr>
 * <td><b>Insert</b></td>
 * <td>{@link Queue#add add(e)}</td>
 * <td>{@link Queue#offer offer(e)}</td>
 * </tr>
 * <tr>
 * <td><b>Remove</b></td>
 * <td>{@link Queue#remove remove()}</td>
 * <td>{@link Queue#poll poll()}</td>
 * </tr>
 * <tr>
 * <td><b>Examine</b></td>
 * <td>{@link Queue#element element()}</td>
 * <td>{@link Queue#peek peek()}</td>
 * </tr>
 * </table>
 * <p>
 * 方法摘要 抛出异常  返回特殊值
 * 插入  Queue#add(e)  Queue#offer(e)
 * 删除  Queue#remove()  Queue#poll()
 * 检查  Queue#element()  Queue#peek()
 *
 * <p>Queues typically, but do not necessarily, order elements in a
 * FIFO (first-in-first-out) manner.  Among the exceptions are
 * priority queues, which order elements according to a supplied
 * comparator, or the elements' natural ordering, and LIFO queues (or
 * stacks) which order the elements LIFO (last-in-first-out).
 * Whatever the ordering used, the <em>head</em> of the queue is that
 * element which would be removed by a call to {@link #remove() } or
 * {@link #poll()}.  In a FIFO queue, all new elements are inserted at
 * the <em>tail</em> of the queue. Other kinds of queues may use
 * different placement rules.  Every {@code Queue} implementation
 * must specify its ordering properties.
 * <p>
 * 队列通常（但不一定）以先进先出（FIFO）的方式对元素进行排序。
 * 例外情况包括优先级队列，它根据提供的比较器或元素的自然顺序对元素进行排序，以及 LIFO 队列（或堆栈），它以 LIFO（后进先出）的顺序对元素进行排序。
 * 无论使用什么排序，队列的头部都是通过调用 remove() 或 poll() 方法将被删除的元素。
 * 在 FIFO 队列中，所有新元素都插入到队列的尾部。
 * 其他类型的队列可能使用不同的放置规则。
 *
 * <p>The {@link #offer offer} method inserts an element if possible,
 * otherwise returning {@code false}.  This differs from the {@link
 * java.util.Collection#add Collection.add} method, which can fail to
 * add an element only by throwing an unchecked exception.  The
 * {@code offer} method is designed for use when failure is a normal,
 * rather than exceptional occurrence, for example, in fixed-capacity
 * (or &quot;bounded&quot;) queues.
 * <p>
 * offer 方法在可能的情况下插入一个元素，否则返回 false。
 * 这与 Collection.add 方法不同，后者只能通过抛出未检查的异常来添加元素。
 * offer 方法的使用场景是：当失败是正常的，而不是异常的情况；例如， 针对固定容量（或“有界”）队列。
 *
 * <p>The {@link #remove()} and {@link #poll()} methods remove and
 * return the head of the queue.
 * Exactly which element is removed from the queue is a
 * function of the queue's ordering policy, which differs from
 * implementation to implementation. The {@code remove()} and
 * {@code poll()} methods differ only in their behavior when the
 * queue is empty: the {@code remove()} method throws an exception,
 * while the {@code poll()} method returns {@code null}.
 * <p>
 * remove() 和 poll() 方法删除并返回队列的头部。
 * 从队列中删除的元素是队列排序策略的函数，该函数因实现而异。
 * remove() 和 poll() 方法的区别仅在于队列为空时的行为：remove() 方法抛出异常，而 poll() 方法返回 null。
 *
 * <p>The {@link #element()} and {@link #peek()} methods return, but do
 * not remove, the head of the queue.
 * <p>
 * element() 和 peek() 方法返回但不删除队列的头部。
 *
 * <p>The {@code Queue} interface does not define the <i>blocking queue
 * methods</i>, which are common in concurrent programming.  These methods,
 * which wait for elements to appear or for space to become available, are
 * defined in the {@link java.util.concurrent.BlockingQueue} interface, which
 * extends this interface.
 * <p>
 * Queue 接口没有定义阻塞队列方法，这些方法在并发编程中很常见。
 * 这些方法等待元素出现或空间可用，这些方法在 BlockingQueue 接口中定义，该接口扩展了此接口。
 *
 * <p>{@code Queue} implementations generally do not allow insertion
 * of {@code null} elements, although some implementations, such as
 * {@link LinkedList}, do not prohibit insertion of {@code null}.
 * Even in the implementations that permit it, {@code null} should
 * not be inserted into a {@code Queue}, as {@code null} is also
 * used as a special return value by the {@code poll} method to
 * indicate that the queue contains no elements.
 * <p>
 * Queue 实现通常不允许插入 null 元素，尽管某些实现（如 LinkedList）不禁止插入 null。
 * 即使在允许的实现中，也不应该将 null 插入到 Queue 中，因为 null 也被 poll 方法用作特殊的返回值，以指示队列不包含元素。
 *
 * <p>{@code Queue} implementations generally do not define
 * element-based versions of methods {@code equals} and
 * {@code hashCode} but instead inherit the identity based versions
 * from class {@code Object}, because element-based equality is not
 * always well-defined for queues with the same elements but different
 * ordering properties.
 * <p>
 * Queue 实现通常不定义基于元素的 equals 和 hashCode 方法的版本，而是从 Object 类继承基于标识的版本，
 * 因为对于具有相同元素但具有不同排序属性的队列，基于元素的相等性并不总是定义良好的。
 *
 *
 * <p>This interface is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @param <E> the type of elements held in this collection
 * @author Doug Lea
 * @see java.util.Collection
 * @see LinkedList
 * @see PriorityQueue
 * @see java.util.concurrent.LinkedBlockingQueue
 * @see java.util.concurrent.BlockingQueue
 * @see java.util.concurrent.ArrayBlockingQueue
 * @see java.util.concurrent.LinkedBlockingQueue
 * @see java.util.concurrent.PriorityBlockingQueue
 * @since 1.5
 */
public interface Queue<E> extends Collection<E> {
    /**
     * Inserts the specified element into this queue if it is possible to do so
     * immediately without violating capacity restrictions, returning
     * {@code true} upon success and throwing an {@code IllegalStateException}
     * if no space is currently available.
     * <p>
     * 如果可以立即这样做而不违反容量限制，则将指定的元素插入此队列，如果成功，则返回 true；
     * 如果当前没有空间，则抛出 IllegalStateException。
     *
     * @param e the element to add
     * @return {@code true} (as specified by {@link Collection#add})
     * @throws IllegalStateException    if the element cannot be added at this
     *                                  time due to capacity restrictions
     * @throws ClassCastException       if the class of the specified element
     *                                  prevents it from being added to this queue
     * @throws NullPointerException     if the specified element is null and
     *                                  this queue does not permit null elements
     * @throws IllegalArgumentException if some property of this element
     *                                  prevents it from being added to this queue
     */
    boolean add(E e);

    /**
     * Inserts the specified element into this queue if it is possible to do
     * so immediately without violating capacity restrictions.
     * When using a capacity-restricted queue, this method is generally
     * preferable to {@link #add}, which can fail to insert an element only
     * by throwing an exception.
     * <p>
     * 如果可以立即这样做而不违反容量限制，则将指定的元素插入此队列。
     * 当使用容量受限的队列时，此方法通常优于 add，后者只能通过抛出异常来表示插入元素的失败。
     *
     * @param e the element to add
     * @return {@code true} if the element was added to this queue, else
     * {@code false}
     * @throws ClassCastException       if the class of the specified element
     *                                  prevents it from being added to this queue
     * @throws NullPointerException     if the specified element is null and
     *                                  this queue does not permit null elements
     * @throws IllegalArgumentException if some property of this element
     *                                  prevents it from being added to this queue
     */
    boolean offer(E e);

    /**
     * Retrieves and removes the head of this queue.  This method differs
     * from {@link #poll poll} only in that it throws an exception if this
     * queue is empty.
     * <p>
     * 检索并删除此队列的头部。
     * 此方法与 poll 方法的不同之处在于，如果此队列为空，则抛出异常。
     *
     * @return the head of this queue
     * @throws NoSuchElementException if this queue is empty
     */
    E remove();

    /**
     * Retrieves and removes the head of this queue,
     * or returns {@code null} if this queue is empty.
     * <p>
     * 检索并删除此队列的头部，如果此队列为空，则返回 null。
     *
     * @return the head of this queue, or {@code null} if this queue is empty
     */
    E poll();

    /**
     * Retrieves, but does not remove, the head of this queue.  This method
     * differs from {@link #peek peek} only in that it throws an exception
     * if this queue is empty.
     * <p>
     * 检索但不删除此队列的头部。
     * 此方法与 peek 方法的不同之处在于，如果此队列为空，则抛出异常。
     *
     * @return the head of this queue
     * @throws NoSuchElementException if this queue is empty
     */
    E element();

    /**
     * Retrieves, but does not remove, the head of this queue,
     * or returns {@code null} if this queue is empty.
     * <p>
     * 检索但不删除此队列的头部，如果此队列为空，则返回 null。
     *
     * @return the head of this queue, or {@code null} if this queue is empty
     */
    E peek();
}
