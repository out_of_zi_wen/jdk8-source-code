/*
 * Copyright (c) 1997, 2006, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

/**
 * This class provides a skeletal implementation of the <tt>List</tt>
 * interface to minimize the effort required to implement this interface
 * backed by a "sequential access" data store (such as a linked list).  For
 * random access data (such as an array), <tt>AbstractList</tt> should be used
 * in preference to this class.
 * <p>
 * 这个类提供了一个 List 接口的骨架实现，以最小化实现这个接口所需的工作量，这个接口由“顺序访问”数据存储（如链表）支持。
 * 对于随机访问数据（如数组），应该优先使用 AbstractList 而不是这个类。
 * <p>
 * This class is the opposite of the <tt>AbstractList</tt> class in the sense
 * that it implements the "random access" methods (<tt>get(int index)</tt>,
 * <tt>set(int index, E element)</tt>, <tt>add(int index, E element)</tt> and
 * <tt>remove(int index)</tt>) on top of the list's list iterator, instead of
 * the other way around.
 * <p>
 * 这个类与 AbstractList 类相反，
 * 它实现了“随机访问”方法（get(int index)、set(int index, E element)、add(int index, E element) 和 remove(int index)）， 基于列表的 list iterator，
 * 而不是反过来。
 * <p>
 * To implement a list the programmer needs only to extend this class and
 * provide implementations for the <tt>listIterator</tt> and <tt>size</tt>
 * methods.  For an unmodifiable list, the programmer need only implement the
 * list iterator's <tt>hasNext</tt>, <tt>next</tt>, <tt>hasPrevious</tt>,
 * <tt>previous</tt> and <tt>index</tt> methods.
 * <p>
 * 要实现一个列表，程序员只需要扩展这个类，并为 listIterator 和 size 方法提供实现。
 * 对于不可修改的列表，程序员只需要实现 list iterator 的 hasNext、next、hasPrevious、previous 和 index 方法。
 * <p>
 * For a modifiable list the programmer should additionally implement the list
 * iterator's <tt>set</tt> method.  For a variable-size list the programmer
 * should additionally implement the list iterator's <tt>remove</tt> and
 * <tt>add</tt> methods.
 * <p>
 * 对于可修改的列表，程序员还应该实现 list iterator 的 set 方法。
 * 对于可变大小的列表，程序员还应该实现 list iterator 的 remove 和 add 方法。
 * <p>
 * The programmer should generally provide a void (no argument) and collection
 * constructor, as per the recommendation in the <tt>Collection</tt> interface
 * specification.
 * <p>
 * 程序员通常应该提供一个无参构造函数和一个 collection 构造函数，如 Collection 接口规范中所建议的那样。
 * <p>
 * This class is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @author Josh Bloch
 * @author Neal Gafter
 * @see Collection
 * @see List
 * @see AbstractList
 * @see AbstractCollection
 * @since 1.2
 */

public abstract class AbstractSequentialList<E> extends AbstractList<E> {

    // TODO REMIND 将所有的操作都委托给 listIterator 处理，只要子类实现了 listIterator，就可以实现所有的操作。
    // 为什么在类开头的注释中明确：如果需要 RandomAccess，应该优先使用 AbstractList 而不是这个类？
    // 因为这里都代理给了 listIterator，而 listIterator 是一个迭代器，它并没有保证过任何随机访问的性能。

    /**
     * Sole constructor.  (For invocation by subclass constructors, typically
     * implicit.)
     */
    protected AbstractSequentialList() {
    }

    /**
     * Returns the element at the specified position in this list.
     * <p>
     * 返回列表中指定位置的元素。
     *
     * <p>This implementation first gets a list iterator pointing to the
     * indexed element (with <tt>listIterator(index)</tt>).  Then, it gets
     * the element using <tt>ListIterator.next</tt> and returns it.
     * <p>
     * 这个实现首先得到一个指向索引元素的 list iterator（使用 listIterator(index)）。
     * 然后，它使用 ListIterator.next 获取元素并返回它。
     *
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public E get(int index) {
        try {
            return listIterator(index).next();
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element (optional operation).
     * <p>
     * 用指定的元素（可选操作）替换列表中指定位置的元素。
     *
     * <p>This implementation first gets a list iterator pointing to the
     * indexed element (with <tt>listIterator(index)</tt>).  Then, it gets
     * the current element using <tt>ListIterator.next</tt> and replaces it
     * with <tt>ListIterator.set</tt>.
     * <p>
     * 这个实现首先得到一个指向索引元素的 list iterator（使用 listIterator(index)）。
     * 然后，它使用 ListIterator.next 获取当前元素，并用 ListIterator.set 替换它。
     *
     * <p>Note that this implementation will throw an
     * <tt>UnsupportedOperationException</tt> if the list iterator does not
     * implement the <tt>set</tt> operation.
     * <p>
     * 注意，如果 list iterator 没有实现 set 操作，这个实现将抛出 UnsupportedOperationException。
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public E set(int index, E element) {
        try {
            ListIterator<E> e = listIterator(index);
            E oldVal = e.next();
            e.set(element);
            return oldVal;
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }
    }

    /**
     * Inserts the specified element at the specified position in this list
     * (optional operation).  Shifts the element currently at that position
     * (if any) and any subsequent elements to the right (adds one to their
     * indices).
     * <p>
     * 在列表中指定的位置插入指定的元素（可选操作）。
     * 将当前在该位置的元素（如果有的话）和任何后续元素向右移动（将它们的索引加一）。
     *
     * <p>This implementation first gets a list iterator pointing to the
     * indexed element (with <tt>listIterator(index)</tt>).  Then, it
     * inserts the specified element with <tt>ListIterator.add</tt>.
     * <p>
     * 这个实现首先得到一个指向索引元素的 list iterator（使用 listIterator(index)）。
     * 然后，它使用 ListIterator.add 插入指定的元素。
     *
     * <p>Note that this implementation will throw an
     * <tt>UnsupportedOperationException</tt> if the list iterator does not
     * implement the <tt>add</tt> operation.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public void add(int index, E element) {
        try {
            listIterator(index).add(element);
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }
    }

    /**
     * Removes the element at the specified position in this list (optional
     * operation).  Shifts any subsequent elements to the left (subtracts one
     * from their indices).  Returns the element that was removed from the
     * list.
     * <p>
     * 删除列表中指定位置的元素（可选操作）。
     * 将任何后续元素向左移动（将它们的索引减一）。
     * 返回从列表中删除的元素。
     *
     * <p>This implementation first gets a list iterator pointing to the
     * indexed element (with <tt>listIterator(index)</tt>).  Then, it removes
     * the element with <tt>ListIterator.remove</tt>.
     * <p>
     * 这个实现首先得到一个指向索引元素的 list iterator（使用 listIterator(index)）。
     * 然后，它使用 ListIterator.remove 删除元素。
     *
     * <p>Note that this implementation will throw an
     * <tt>UnsupportedOperationException</tt> if the list iterator does not
     * implement the <tt>remove</tt> operation.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public E remove(int index) {
        try {
            ListIterator<E> e = listIterator(index);
            E outCast = e.next();
            e.remove();
            return outCast;
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }
    }


    // Bulk Operations

    /**
     * Inserts all of the elements in the specified collection into this
     * list at the specified position (optional operation).  Shifts the
     * element currently at that position (if any) and any subsequent
     * elements to the right (increases their indices).  The new elements
     * will appear in this list in the order that they are returned by the
     * specified collection's iterator.  The behavior of this operation is
     * undefined if the specified collection is modified while the
     * operation is in progress.  (Note that this will occur if the specified
     * collection is this list, and it's nonempty.)
     * <p>
     * 将指定集合中的所有元素插入到列表中的指定位置（可选操作）。
     * 将当前在该位置的元素（如果有的话）和任何后续元素向右移动（增加它们的索引）。
     * 新元素将按照它们由指定集合的迭代器返回的顺序出现在这个列表中。
     * 如果在操作正在进行时修改了指定的集合，则此操作的行为是未定义的。
     * （注意，如果指定的集合是这个列表，且它不为空，那么这将发生。）
     *
     * <p>This implementation gets an iterator over the specified collection and
     * a list iterator over this list pointing to the indexed element (with
     * <tt>listIterator(index)</tt>).  Then, it iterates over the specified
     * collection, inserting the elements obtained from the iterator into this
     * list, one at a time, using <tt>ListIterator.add</tt> followed by
     * <tt>ListIterator.next</tt> (to skip over the added element).
     * <p>
     * 这个实现在指定的集合上获取一个迭代器，并在这个列表上获取一个指向索引元素的 list iterator（使用 listIterator(index)）。
     * 然后，它遍历指定的集合，使用 ListIterator.add 将从迭代器获取的元素一个接一个地插入到这个列表中，然后使用 ListIterator.next 跳过添加的元素。
     *
     * <p>Note that this implementation will throw an
     * <tt>UnsupportedOperationException</tt> if the list iterator returned by
     * the <tt>listIterator</tt> method does not implement the <tt>add</tt>
     * operation.
     *
     * @throws UnsupportedOperationException {@inheritDoc}
     * @throws ClassCastException            {@inheritDoc}
     * @throws NullPointerException          {@inheritDoc}
     * @throws IllegalArgumentException      {@inheritDoc}
     * @throws IndexOutOfBoundsException     {@inheritDoc}
     */
    public boolean addAll(int index, Collection<? extends E> c) {
        try {
            boolean modified = false;
            ListIterator<E> e1 = listIterator(index);
            // 至少是 O(N) 的开销，而如果是 ArrayList，那么这个开销就是 O(1) 的，仅需两次 arrayCopy。
            Iterator<? extends E> e2 = c.iterator();
            while (e2.hasNext()) {
                e1.add(e2.next());
                modified = true;
            }
            return modified;
        } catch (NoSuchElementException exc) {
            throw new IndexOutOfBoundsException("Index: " + index);
        }
    }


    // Iterators

    /**
     * Returns an iterator over the elements in this list (in proper
     * sequence).<p>
     * <p>
     * This implementation merely returns a list iterator over the list.
     *
     * @return an iterator over the elements in this list (in proper sequence)
     */
    public Iterator<E> iterator() {
        return listIterator();
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     *
     * @param index index of first element to be returned from the list
     *              iterator (by a call to the <code>next</code> method)
     * @return a list iterator over the elements in this list (in proper
     * sequence)
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    public abstract ListIterator<E> listIterator(int index);
}
