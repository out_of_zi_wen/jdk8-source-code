/*
 * Copyright (c) 1997, 2021, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.util;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import sun.misc.SharedSecrets;

/**
 * Hash table based implementation of the <tt>Map</tt> interface.  This
 * implementation provides all of the optional map operations, and permits
 * <tt>null</tt> values and the <tt>null</tt> key.  (The <tt>HashMap</tt>
 * class is roughly equivalent to <tt>Hashtable</tt>, except that it is
 * unsynchronized and permits nulls.)  This class makes no guarantees as to
 * the order of the map; in particular, it does not guarantee that the order
 * will remain constant over time.
 * <p>
 * 基于哈希表的 Map 接口实现。
 * 此实现提供了所有可选的映射操作，并允许 null 值和 null 键。
 * (HashMap 类大致相当于 Hashtable，除了它是不同步的，并且允许 nulls。)
 * 此类不保证映射的顺序; 特别是，它不保证该顺序随时间的推移保持不变。
 *
 * <p>This implementation provides constant-time performance for the basic
 * operations (<tt>get</tt> and <tt>put</tt>), assuming the hash function
 * disperses the elements properly among the buckets.  Iteration over
 * collection views requires time proportional to the "capacity" of the
 * <tt>HashMap</tt> instance (the number of buckets) plus its size (the number
 * of key-value mappings).  Thus, it's very important not to set the initial
 * capacity too high (or the load factor too low) if iteration performance is
 * important.
 * <p>
 * 假设哈希函数在桶之间正确地分散元素，此实现为基本操作 (get 和 put) 提供了恒定时间性能。
 * 集合视图的迭代需要与 HashMap 实例的“容量”(桶的数量)加上其大小(键值映射的数量)成比例的时间。
 * 因此，如果迭代性能很重要，则不要将初始容量设置得太高(或负载系数太低)。
 *
 * <p>An instance of <tt>HashMap</tt> has two parameters that affect its
 * performance: <i>initial capacity</i> and <i>load factor</i>.  The
 * <i>capacity</i> is the number of buckets in the hash table, and the initial
 * capacity is simply the capacity at the time the hash table is created.  The
 * <i>load factor</i> is a measure of how full the hash table is allowed to
 * get before its capacity is automatically increased.  When the number of
 * entries in the hash table exceeds the product of the load factor and the
 * current capacity, the hash table is <i>rehashed</i> (that is, internal data
 * structures are rebuilt) so that the hash table has approximately twice the
 * number of buckets.
 * <p>
 * HashMap 的实例有两个参数影响其性能: initial capacity 和 load factor。
 * capacity 是哈希表中的桶数，initial capacity 只是在创建哈希表时的容量。
 * load factor 是哈希表在其容量自动增加之前可以达到多满的一种尺度。
 * 当哈希表中的条目数超出了 load factor * capacity 的结果值时，哈希表将被 rehash(即内部数据结构被重建)，哈希表的桶数大约增加一倍。
 *
 * <p>As a general rule, the default load factor (.75) offers a good
 * tradeoff between time and space costs.  Higher values decrease the
 * space overhead but increase the lookup cost (reflected in most of
 * the operations of the <tt>HashMap</tt> class, including
 * <tt>get</tt> and <tt>put</tt>).  The expected number of entries in
 * the map and its load factor should be taken into account when
 * setting its initial capacity, so as to minimize the number of
 * rehash operations.  If the initial capacity is greater than the
 * maximum number of entries divided by the load factor, no rehash
 * operations will ever occur.
 * <p>
 * 通常，默认的 load factor (0.75) 在时间和空间成本之间提供了很好的折衷。
 * 较高的 load factor 降低了空间开销，但增加了查找成本(反映在 HashMap 类的大多数操作中，包括 get 和 put)。
 * 在设置初始容量时应考虑到映射中的条目数及其 load factor，以尽量减少 rehash 操作的数量。
 * 如果初始容量大于最大条目数除以 load factor，则不会发生 rehash 操作。
 *
 * <p>If many mappings are to be stored in a <tt>HashMap</tt>
 * instance, creating it with a sufficiently large capacity will allow
 * the mappings to be stored more efficiently than letting it perform
 * automatic rehashing as needed to grow the table.  Note that using
 * many keys with the same {@code hashCode()} is a sure way to slow
 * down performance of any hash table. To ameliorate impact, when keys
 * are {@link Comparable}, this class may use comparison order among
 * keys to help break ties.
 * <p>
 * 如果要在 HashMap 实例中存储许多映射，则在一开始创建具有足够大容量比让它不断地扩容更加高效。
 * 注意，使用许多具有相同 hashCode() 的键是降低任何哈希表性能的一种可靠方法。
 * 为了减轻影响，当键是可比较的时，此类可以使用键之间的比较顺序来帮助打破平局。
 *
 * <p><strong>Note that this implementation is not synchronized.</strong>
 * If multiple threads access a hash map concurrently, and at least one of
 * the threads modifies the map structurally, it <i>must</i> be
 * synchronized externally.  (A structural modification is any operation
 * that adds or deletes one or more mappings; merely changing the value
 * associated with a key that an instance already contains is not a
 * structural modification.)  This is typically accomplished by
 * synchronizing on some object that naturally encapsulates the map.
 * <p>
 * 注意，此实现不是同步的。
 * 如果多个线程同时访问一个哈希映射，并且至少有一个线程从结构上修改了该映射，则它必须是外部同步的。
 * (结构修改是指添加或删除一个或多个映射的任何操作;仅仅改变与实例已经包含的键相关联的值不是结构修改。)
 * 通常，通过在自然封装映射的某个对象上同步来实现这一点。
 *
 * <p>
 * If no such object exists, the map should be "wrapped" using the
 * {@link Collections#synchronizedMap Collections.synchronizedMap}
 * method.  This is best done at creation time, to prevent accidental
 * unsynchronized access to the map:<pre>
 *   Map m = Collections.synchronizedMap(new HashMap(...));</pre>
 *
 * <p>The iterators returned by all of this class's "collection view methods"
 * are <i>fail-fast</i>: if the map is structurally modified at any time after
 * the iterator is created, in any way except through the iterator's own
 * <tt>remove</tt> method, the iterator will throw a
 * {@link ConcurrentModificationException}.  Thus, in the face of concurrent
 * modification, the iterator fails quickly and cleanly, rather than risking
 * arbitrary, non-deterministic behavior at an undetermined time in the
 * future.
 *
 * <p>Note that the fail-fast behavior of an iterator cannot be guaranteed
 * as it is, generally speaking, impossible to make any hard guarantees in the
 * presence of unsynchronized concurrent modification.  Fail-fast iterators
 * throw <tt>ConcurrentModificationException</tt> on a best-effort basis.
 * Therefore, it would be wrong to write a program that depended on this
 * exception for its correctness: <i>the fail-fast behavior of iterators
 * should be used only to detect bugs.</i>
 *
 * <p>This class is a member of the
 * <a href="{@docRoot}/../technotes/guides/collections/index.html">
 * Java Collections Framework</a>.
 *
 * @param <K> the type of keys maintained by this map
 * @param <V> the type of mapped values
 * @author Doug Lea
 * @author Josh Bloch
 * @author Arthur van Hoff
 * @author Neal Gafter
 * @see Object#hashCode()
 * @see Collection
 * @see Map
 * @see TreeMap
 * @see Hashtable
 * @since 1.2
 */
public class HashMap<K, V> extends AbstractMap<K, V>
        implements Map<K, V>, Cloneable, Serializable {

    private static final long serialVersionUID = 362498820763181265L;

    /*
     * Implementation notes.
     *
     * This map usually acts as a binned (bucketed) hash table, but
     * when bins get too large, they are transformed into bins of
     * TreeNodes, each structured similarly to those in
     * java.util.TreeMap. Most methods try to use normal bins, but
     * relay to TreeNode methods when applicable (simply by checking
     * instanceof a node).  Bins of TreeNodes may be traversed and
     * used like any others, but additionally support faster lookup
     * when overpopulated. However, since the vast majority of bins in
     * normal use are not overpopulated, checking for existence of
     * tree bins may be delayed in the course of table methods.
     * <p>
     * 此 map 通常充当一个基于哈希桶实现的哈希表，但是当哈希桶变得太大时，它们就会转换成类似于 java.util.TreeMap 中的 TreeNodes 的 桶。
     * 大多数方法都试图使用普通的哈希桶，但是在一个合适的场景下转换为到 TreeNode 方法(只需检查 instanceof 节点)。
     * TreeNodes 的桶可以像其他桶一样遍历和使用，但是在过度填充时还支持更快的查找。
     * 然而，由于正常使用中绝大多数桶都没有过度填充，所以在表方法的过程中可能会延迟检查树桶的存在。
     *
     * Tree bins (i.e., bins whose elements are all TreeNodes) are
     * ordered primarily by hashCode, but in the case of ties, if two
     * elements are of the same "class C implements Comparable<C>",
     * type then their compareTo method is used for ordering. (We
     * conservatively check generic types via reflection to validate
     * this -- see method comparableClassFor).  The added complexity
     * of tree bins is worthwhile in providing worst-case O(log n)
     * operations when keys either have distinct hashes or are
     * orderable, Thus, performance degrades gracefully under
     * accidental or malicious usages in which hashCode() methods
     * return values that are poorly distributed, as well as those in
     * which many keys share a hashCode, so long as they are also
     * Comparable. (If neither of these apply, we may waste about a
     * factor of two in time and space compared to taking no
     * precautions. But the only known cases stem from poor user
     * programming practices that are already so slow that this makes
     * little difference.)
     * <p>
     * Tree bins (即所有元素都是 TreeNodes 的桶)主要按 hashCode 排序，但是在关联的情况下，
     * 如果两个元素是相同的“类 C 实现 Comparable<C>”，则使用它们的 compareTo 方法进行排序。
     * (我们通过反射保守地检查通用类型来验证这一点 - 请参阅 comparableClassFor 方法。)
     * 树桶的复杂性是值得的，因为它在提供最坏情况下的 O(log n) 操作时，键具有不同的哈希或可排序。
     *
     * Because TreeNodes are about twice the size of regular nodes, we
     * use them only when bins contain enough nodes to warrant use
     * (see TREEIFY_THRESHOLD). And when they become too small (due to
     * removal or resizing) they are converted back to plain bins.  In
     * usages with well-distributed user hashCodes, tree bins are
     * rarely used.  Ideally, under random hashCodes, the frequency of
     * nodes in bins follows a Poisson distribution
     * (http://en.wikipedia.org/wiki/Poisson_distribution) with a
     * parameter of about 0.5 on average for the default resizing
     * threshold of 0.75, although with a large variance because of
     * resizing granularity. Ignoring variance, the expected
     * occurrences of list size k are (exp(-0.5) * pow(0.5, k) /
     * factorial(k)). The first values are:
     *
     * 0:    0.60653066
     * 1:    0.30326533
     * 2:    0.07581633
     * 3:    0.01263606
     * 4:    0.00157952
     * 5:    0.00015795
     * 6:    0.00001316
     * 7:    0.00000094
     * 8:    0.00000006
     * more: less than 1 in ten million
     * <p>
     * 因为 TreeNodes 的大小大约是常规节点的两倍，所以只有当桶中包含足够多的节点时才使用它们(请参阅 TREEIFY_THRESHOLD)。
     * 当它们变得太小(由于删除或调整大小)时，它们将转换回普通的桶。
     * 在具有分布良好的哈希码的用法中，树桶很少使用。
     * 理想情况下，在随机哈希码下，桶中节点的频率遵循泊松分布 (http://en.wikipedia.org/wiki/Poisson_distribution)，平均为 0.5，
     * 默认调整大小阈值为 0.75，尽管由于调整大小粒度的原因，方差很大。
     * 忽略方差，列表大小 k 的预期出现是 (exp(-0.5) * pow(0.5, k) / factorial(k))。
     * 第一个值是:
     * 0:    0.60653066
     * 1:    0.30326533
     * 2:    0.07581633
     * 3:    0.01263606
     * 4:    0.00157952
     * 5:    0.00015795
     * 6:    0.00001316
     * 7:    0.00000094
     * 8:    0.00000006
     * more:  比一千万分之一还少
     *
     * The root of a tree bin is normally its first node.  However,
     * sometimes (currently only upon Iterator.remove), the root might
     * be elsewhere, but can be recovered following parent links
     * (method TreeNode.root()).
     * <p>
     * 树桶的根通常是它的第一个节点。
     * 然而，有时(目前只有在 Iterator.remove 时)，根可能在其他地方，但是可以通过跟随父链接来恢复 (方法 TreeNode.root())。
     *
     * All applicable internal methods accept a hash code as an
     * argument (as normally supplied from a public method), allowing
     * them to call each other without recomputing user hashCodes.
     * Most internal methods also accept a "tab" argument, that is
     * normally the current table, but may be a new or old one when
     * resizing or converting.
     * <p>
     * 所有适用的内部方法都接受哈希码作为参数(通常从公共方法提供)，允许它们在不重新计算哈希码的情况下相互调用。
     * 大多数内部方法也接受一个“tab”参数，通常是当前表，但在调整大小或转换时可能是代表新表或旧表。
     *
     * When bin lists are treeified, split, or untreeified, we keep
     * them in the same relative access/traversal order (i.e., field
     * Node.next) to better preserve locality, and to slightly
     * simplify handling of splits and traversals that invoke
     * iterator.remove. When using comparators on insertion, to keep a
     * total ordering (or as close as is required here) across
     * rebalancings, we compare classes and identityHashCodes as
     * tie-breakers.
     * <p>
     * 当链表桶被树化、分割或取消树化时，我们保持它们在相同的相对访问/遍历顺序(即，字段 Node.next)，
     * 以更好地保留局部性，并稍微简化调用 iterator.remove 的分割和遍历的处理。
     * 当在插入时使用比较器时，为了在重新平衡时保持完全排序(或尽可能接近此处所需的排序)，我们比较类和 identityHashCodes 作为 tie-breakers。
     *
     * The use and transitions among plain vs tree modes is
     * complicated by the existence of subclass LinkedHashMap. See
     * below for hook methods defined to be invoked upon insertion,
     * removal and access that allow LinkedHashMap internals to
     * otherwise remain independent of these mechanics. (This also
     * requires that a map instance be passed to some utility methods
     * that may create new nodes.)
     * <p>
     * 普通模式和树模式之间的使用和转换由子类 LinkedHashMap 的存在复杂化。
     * 请参阅下面的 hook 方法，这些 hook 方法在插入、删除和访问时被定义为被调用，允许 LinkedHashMap 内部在其他情况下保持独立于这些机制。
     * (这还要求将 map 实例传递给可能创建新节点的一些实用方法。)
     *
     * The concurrent-programming-like SSA-based coding style helps
     * avoid aliasing errors amid all of the twisty pointer operations.
     */

    /**
     * The default initial capacity - MUST be a power of two.
     * <p>
     * 默认初始容量 - 必须是 2 的幂。
     */
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16

    /**
     * The maximum capacity, used if a higher value is implicitly specified
     * by either of the constructors with arguments.
     * MUST be a power of two <= 1<<30.
     * <p>
     * 最大容量，如果由带参数的任一构造函数隐式指定了更高的值，则使用该值。
     * 必须是 2 的幂 <= 1<<30。
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;

    /**
     * The load factor used when none specified in constructor.
     * <p>
     * 构造函数中未指定时使用的负载因子。
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;

    /**
     * The bin count threshold for using a tree rather than list for a
     * bin.  Bins are converted to trees when adding an element to a
     * bin with at least this many nodes. The value must be greater
     * than 2 and should be at least 8 to mesh with assumptions in
     * tree removal about conversion back to plain bins upon
     * shrinkage.
     * <p>
     * 链表桶转为树桶的结点数量阈值。
     * 当向具有至少这么多节点的桶中添加元素时，链表将转为树。
     * 此值必须大于 2，并且应至少为 8，以便与关于在缩小时转换回普通桶的假设相吻合。
     */
    static final int TREEIFY_THRESHOLD = 8;

    /**
     * The bin count threshold for untreeifying a (split) bin during a
     * resize operation. Should be less than TREEIFY_THRESHOLD, and at
     * most 6 to mesh with shrinkage detection under removal.
     * <p>
     * 在调整大小操作期间取消树化(分割)桶的结点数量阈值。
     * 应该小于 TREEIFY_THRESHOLD，并且最多为 6，以便在删除时与收缩检测相吻合。
     */
    static final int UNTREEIFY_THRESHOLD = 6;

    /**
     * The smallest table capacity for which bins may be treeified.
     * (Otherwise the table is resized if too many nodes in a bin.)
     * Should be at least 4 * TREEIFY_THRESHOLD to avoid conflicts
     * between resizing and treeification thresholds.
     * <p>
     * 可以树化的最小表容量。
     * (否则，如果桶中的节点太多，则调整表的大小。)
     * 应至少为 4 * TREEIFY_THRESHOLD，以避免调整大小和树化阈值之间的冲突。
     */
    static final int MIN_TREEIFY_CAPACITY = 64;

    // TODO REMIND 树化的条件有两个 -> 一个桶的元素数量大于 8 && 整个 map 的数量大于 64
    // 如果仅满足了单桶数量大于 8，但是整个 map 的数量小于 64，那么就不会树化，而是会 resize

    /**
     * Basic hash bin node, used for most entries.  (See below for
     * TreeNode subclass, and in LinkedHashMap for its Entry subclass.)
     * <p>
     * 基本的哈希桶结点，用于大多数条目。
     * (请参阅下面的 TreeNode 子类，以及 LinkedHashMap 中的 Entry 子类。)
     */
    static class Node<K, V> implements Map.Entry<K, V> {
        // 缓存记录 key 的 hash 值
        final int hash;
        final K key;
        V value;

        // 指向下一个结点的指针；印象里 jdk7 以前好像也是 final 的
        Node<K, V> next;

        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public final K getKey() {
            return key;
        }

        public final V getValue() {
            return value;
        }

        public final String toString() {
            return key + "=" + value;
        }

        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        // Entry 接口的方法
        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }

        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Map.Entry) {
                Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
                if (Objects.equals(key, e.getKey()) &&
                        Objects.equals(value, e.getValue()))
                    return true;
            }
            return false;
        }
    }

    /* ---------------- Static utilities -------------- */

    /**
     * Computes key.hashCode() and spreads (XORs) higher bits of hash
     * to lower.  Because the table uses power-of-two masking, sets of
     * hashes that vary only in bits above the current mask will
     * always collide. (Among known examples are sets of Float keys
     * holding consecutive whole numbers in small tables.)  So we
     * apply a transform that spreads the impact of higher bits
     * downward. There is a tradeoff between speed, utility, and
     * quality of bit-spreading. Because many common sets of hashes
     * are already reasonably distributed (so don't benefit from
     * spreading), and because we use trees to handle large sets of
     * collisions in bins, we just XOR some shifted bits in the
     * cheapest possible way to reduce systematic lossage, as well as
     * to incorporate impact of the highest bits that would otherwise
     * never be used in index calculations because of table bounds.
     * <p>
     * 计算 key.hashCode() 并将 hash 的高位扩展(XOR)到低位。
     * 因为表使用 2 的幂掩码，所以仅在当前掩码上方的位中变化的哈希集将总是发生冲突。
     * (已知的示例包括在小表中保存连续整数的 Float 键集。)
     * 因此，我们应用了一种变换，将较高位的影响向下传播。
     * 在速度、效用和位扩展质量之间存在权衡。
     * 因为许多常见的哈希集已经合理地分布(因此不会受益于扩展)，并且因为我们使用树来处理桶中大量的冲突，
     * 所以我们只是以最便宜的方式 XOR 一些移位的位，以减少系统损失，以及合并最高位的影响，
     * 否则由于表边界而永远不会在索引计算中使用。
     */
    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    /**
     * Returns x's Class if it is of the form "class C implements
     * Comparable<C>", else null.
     * <p>
     * 如果 x 是“类 C 实现 Comparable<C>”的形式，则返回 x 的类，否则返回 null。
     */
    static Class<?> comparableClassFor(Object x) {
        // 判断对象 x 是否实现了 Comparable 接口
        if (x instanceof Comparable) {
            Class<?> c;
            Type[] ts, as;
            Type t;
            ParameterizedType p;
            // 如果 x 是字符串，就直接返回 String.class
            if ((c = x.getClass()) == String.class) // bypass checks
                return c;
            // 获取带泛型信息的父接口
            if ((ts = c.getGenericInterfaces()) != null) {
                for (int i = 0; i < ts.length; ++i) {
                    if (((t = ts[i]) instanceof ParameterizedType) &&
                            ((p = (ParameterizedType) t).getRawType() ==
                                    Comparable.class) &&
                            (as = p.getActualTypeArguments()) != null &&
                            as.length == 1 && as[0] == c) // type arg is c
                        return c;
                }
            }
        }
        return null;
    }

    /**
     * Returns k.compareTo(x) if x matches kc (k's screened comparable
     * class), else 0.
     * <p>
     * 如果 x 匹配 kc (k 的筛选可比较类)，则返回 k.compareTo(x)，否则返回 0。
     */
    @SuppressWarnings({"rawtypes", "unchecked"}) // for cast to Comparable
    static int compareComparables(Class<?> kc, Object k, Object x) {
        return (x == null || x.getClass() != kc ? 0 :
                ((Comparable) k).compareTo(x));
    }

    /**
     * Returns a power of two size for the given target capacity.
     * <p>
     * 找到大于等于 cap 的最小二次幂
     * 这个方法在 ArrayDeque 里也有
     */
    static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }

    /* ---------------- Fields -------------- */

    /**
     * The table, initialized on first use, and resized as
     * necessary. When allocated, length is always a power of two.
     * (We also tolerate length zero in some operations to allow
     * bootstrapping mechanics that are currently not needed.)
     * <p>
     * 表，第一次使用时初始化，并根据需要调整大小。
     * 分配时，长度总是 2 的幂。
     * (我们还容忍一些操作中的长度为零，以允许当前不需要的引导机制。)
     */
    transient Node<K, V>[] table;

    /**
     * Holds cached entrySet(). Note that AbstractMap fields are used
     * for keySet() and values().
     * <p>
     * 缓存的 entrySet()。
     * 注意，AbstractMap 中的字段用于 keySet() 和 values()。
     */
    transient Set<Map.Entry<K, V>> entrySet;

    /**
     * The number of key-value mappings contained in this map.
     * <p>
     * 此 map 中包含的键值映射的数量。
     */
    transient int size;

    /**
     * The number of times this HashMap has been structurally modified
     * Structural modifications are those that change the number of mappings in
     * the HashMap or otherwise modify its internal structure (e.g.,
     * rehash).  This field is used to make iterators on Collection-views of
     * the HashMap fail-fast.  (See ConcurrentModificationException).
     * <p>
     * 此 HashMap 已结构化修改的次数
     * 结构修改是那些改变 HashMap 中映射数量或以其他方式修改其内部结构(例如，rehash)的修改。
     * 此字段用于使 HashMap 的 Collection-views 上的迭代器快速失败。
     */
    transient int modCount;

    /**
     * The next size value at which to resize (capacity * load factor).
     * <p>
     * 下一次调整大小的阈值 (容量 * 负载因子)。
     *
     * @serial
     */
    // (The javadoc description is true upon serialization.
    // Additionally, if the table array has not been allocated, this
    // field holds the initial array capacity, or zero signifying
    // DEFAULT_INITIAL_CAPACITY.)
    int threshold;

    /**
     * The load factor for the hash table.
     * <p>
     * 哈希表的负载因子。
     *
     * @serial
     */
    final float loadFactor;

    /* ---------------- Public operations -------------- */

    /**
     * Constructs an empty <tt>HashMap</tt> with the specified initial
     * capacity and load factor.
     * <p>
     * 构造一个空的 HashMap，指定初始容量和负载因子。
     *
     * @param initialCapacity the initial capacity
     * @param loadFactor      the load factor
     * @throws IllegalArgumentException if the initial capacity is negative
     *                                  or the load factor is nonpositive
     */
    public HashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: " +
                    initialCapacity);
        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " +
                    loadFactor);
        //  指定初始容量和负载因子
        this.loadFactor = loadFactor;
        // 这里会直接将 capacity 转换为 2 的幂，然后赋值给 threshold
        // 注意：没乘 loadFactor，也就是说，如果我们在构造 map 的时候指定了容量，那么这个容量在转 2 次幂之后就会直接作为 threshold！！！
        this.threshold = tableSizeFor(initialCapacity);
    }

    /**
     * Constructs an empty <tt>HashMap</tt> with the specified initial
     * capacity and the default load factor (0.75).
     *
     * @param initialCapacity the initial capacity.
     * @throws IllegalArgumentException if the initial capacity is negative.
     */
    public HashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    /**
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public HashMap() {
        // 设置了默认的 loadFactor
        this.loadFactor = DEFAULT_LOAD_FACTOR; // all other fields defaulted
    }

    /**
     * Constructs a new <tt>HashMap</tt> with the same mappings as the
     * specified <tt>Map</tt>.  The <tt>HashMap</tt> is created with
     * default load factor (0.75) and an initial capacity sufficient to
     * hold the mappings in the specified <tt>Map</tt>.
     * <p>
     * 构造一个新的 HashMap，其映射与指定的 Map 相同。
     * HashMap 是使用默认负载因子 (0.75) 和足以容纳指定 Map 中的映射的初始容量创建的。
     *
     * @param m the map whose mappings are to be placed in this map
     * @throws NullPointerException if the specified map is null
     */
    public HashMap(Map<? extends K, ? extends V> m) {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        putMapEntries(m, false);
    }

    /**
     * Implements Map.putAll and Map constructor.
     * <p>
     * 实现 Map.putAll 和 Map 构造函数。
     *
     * @param m     the map
     * @param evict false when initially constructing this map, else
     *              true (relayed to method afterNodeInsertion).
     */
    final void putMapEntries(Map<? extends K, ? extends V> m, boolean evict) {
        int s = m.size();
        if (s > 0) {
            // step1 -- 更新容量
            // 如果 table 为空，意味着是第一次 put
            if (table == null) { // pre-size
                // 计算出 m 中元素迁移到 HashMap 之后需要占用的 HashMap 的容量
                float ft = ((float) s / loadFactor) + 1.0F;
                // 转为 int 类型
                int t = ((ft < (float) MAXIMUM_CAPACITY) ?
                        (int) ft : MAXIMUM_CAPACITY);
                // 此时因为 table == null，所以需要比较 t 和 threshold；
                // 必要时更新 threshold，注意此时 t 已经是基于 loadFactor 计算后的结果
                if (t > threshold)
                    threshold = tableSizeFor(t);

                // 如果新增的元素数量就已经大于 threshold 了，那么当前 map 肯定装不下；因此立即扩容
            } else if (s > threshold) {
                resize();
            }


            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
                // 取出 k 和 v
                K key = e.getKey();
                V value = e.getValue();
                // 重新构造 Node 插入
                putVal(hash(key), key, value, false, evict);
            }
        }
    }

    /**
     * Returns the number of key-value mappings in this map.
     * <p>
     * 返回此 map 中键值映射的数量。
     *
     * @return the number of key-value mappings in this map
     */
    public int size() {
        return size;
    }

    /**
     * Returns <tt>true</tt> if this map contains no key-value mappings.
     * <p>
     * 如果此 map 不包含键值映射，则返回 true。
     *
     * @return <tt>true</tt> if this map contains no key-value mappings
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns the value to which the specified key is mapped,
     * or {@code null} if this map contains no mapping for the key.
     * <p>
     * 返回指定 key 映射到的值，如果此 map 不包含 key 的映射，则返回 null。
     *
     * <p>More formally, if this map contains a mapping from a key
     * {@code k} to a value {@code v} such that {@code (key==null ? k==null :
     * key.equals(k))}, then this method returns {@code v}; otherwise
     * it returns {@code null}.  (There can be at most one such mapping.)
     * <p>
     * 更正式地说，如果此 map 包含从 key k 到 value v 的映射，使得 (key==null ? k==null : key.equals(k))，
     * 那么此方法返回 v；否则返回 null。(最多只能有一个这样的映射。)
     *
     * <p>A return value of {@code null} does not <i>necessarily</i>
     * indicate that the map contains no mapping for the key; it's also
     * possible that the map explicitly maps the key to {@code null}.
     * The {@link #containsKey containsKey} operation may be used to
     * distinguish these two cases.
     * <p>
     * null 的返回值不一定表示 map 不包含 key 的映射；map 也可能将 key 显式映射到 null。
     * 可以使用 containsKey 操作来区分这两种情况。
     *
     * @see #put(Object, Object)
     */
    public V get(Object key) {
        Node<K, V> e;
        // 根据 key 计算 hash 值，然后调用 getNode 方法，如果找到了就返回 value，否则返回 null
        return (e = getNode(hash(key), key)) == null ? null : e.value;
    }

    /**
     * Implements Map.get and related methods.
     *
     * @param hash hash for key
     * @param key  the key
     * @return the node, or null if none
     */
    final Node<K, V> getNode(int hash, Object key) {
        // avoid getfield opcode
        Node<K, V>[] tab;
        // first 代表桶中的第一个元素；e 是遍历期间指向的元素
        Node<K, V> first, e;
        // n 是桶的长度；如果 get 期间，其他线程正在 resize，那么 n 也能保证不变
        int n;
        // k 记录 e 结点的 key
        K k;

        // 赋值 tab 和 n，找到桶中的第一个元素；
        if ((tab = table) != null && (n = tab.length) > 0 &&
                (first = tab[(n - 1) & hash]) != null) {
            // 判断第一个元素是否就是要找的元素
            if (first.hash == hash && // always check first node
                    ((k = first.key) == key || (key != null && key.equals(k))))
                return first;

            // 如果第一个元素不是要找的元素，并且后面还有元素，那么就遍历链表或者树
            if ((e = first.next) != null) {
                // 如果已经树化了，那么就调用树的查找方法
                if (first instanceof TreeNode)
                    return ((TreeNode<K, V>) first).getTreeNode(hash, key);
                // 否则就遍历链表
                do {
                    // 判断 e 结点是否就是要找的元素
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k))))
                        return e;
                } while ((e = e.next) != null);
            }
        }
        return null;
    }

    /**
     * Returns <tt>true</tt> if this map contains a mapping for the
     * specified key.
     * <p>
     * 如果此 map 包含指定 key 的映射，则返回 true。
     *
     * @param key The key whose presence in this map is to be tested
     * @return <tt>true</tt> if this map contains a mapping for the specified
     * key.
     */
    public boolean containsKey(Object key) {
        // 调用 getNode 方法，判断是否存在指定 key 相关的 Node
        return getNode(hash(key), key) != null;
    }

    /**
     * Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key, the old
     * value is replaced.
     * <p>
     * 将指定的值与此 map 中的指定 key 关联。
     * 如果 map 先前包含 key 的映射，则替换旧值。
     *
     * @param key   key with which the specified value is to be associated
     * @param value value to be associated with the specified key
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>.
     * (A <tt>null</tt> return can also indicate that the map
     * previously associated <tt>null</tt> with <tt>key</tt>.)
     */
    public V put(K key, V value) {
        return putVal(hash(key), key, value, false, true);
    }

    /**
     * Implements Map.put and related methods.
     *
     * @param hash         hash for key
     * @param key          the key
     * @param value        the value to put
     * @param onlyIfAbsent if true, don't change existing value；如果为 true，则不更改现有值
     * @param evict        if false, the table is in creation mode；如果为 false，则表处于创建模式
     * @return previous value, or null if none
     */
    final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        // tab 代表 table；p 代表桶中的第一个元素；
        Node<K, V>[] tab;
        Node<K, V> p;
        // n 代表桶的长度；i 代表桶的索引
        int n, i;

        // 如果 table 为空，或者 table 的长度为 0，那么就调用 resize 方法，初始化 table
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
        // 计算出桶的索引，如果是空桶，那么就直接插入
        if ((p = tab[i = (n - 1) & hash]) == null) {

            tab[i] = newNode(hash, key, value, null);
        } else {
            // 否则，说明桶中存在元素，就遍历链表或者树
            // e 记录迭代期间 map 中已存在的相同 key 的结点位置
            Node<K, V> e;
            K k;
            // 开始的时候，p 指向桶中的第一个元素；针对桶中的第一个元素进行判断
            if (p.hash == hash &&
                    ((k = p.key) == key || (key != null && key.equals(k)))) {
                e = p;
            }
            // 如果 p 是 TreeNode，那么就调用红黑树的插入方法
            else if (p instanceof TreeNode) {
                e = ((TreeNode<K, V>) p).putTreeVal(this, tab, hash, key, value);
            } else {
                // 否则，说明桶中的第一个元素是链表结点，就遍历链表
                for (int binCount = 0; ; ++binCount) {
                    // 如果遍历到了链表的最后一个结点，那么就将新结点插入到链表的最后
                    if ((e = p.next) == null) {
                        p.next = newNode(hash, key, value, null);
                        // 如果链表长度大于等于 8，那么就将链表转为红黑树（插入前长度 >= 7）
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
                    // 否则，在遍历期间（还没到表尾），如果找到了相同的 key，那么就跳出循环；此时 e 记录了该 Node 的位置
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        break;
                    }
                    // 否则，就继续遍历
                    p = e;
                }
            }

            // 如果 e 不为空，说明 map 中已经存在相同的 key，那么就更新 value
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                // 模版方法，供 LinkedHashMap 使用
                afterNodeAccess(e);
                return oldValue;
            }
        }
        // 累加 modCount
        ++modCount;
        // 累加 map 的元素数量，并且判断是否需要扩容
        if (++size > threshold)
            resize();

        // 模版方法，供 LinkedHashMap 使用
        afterNodeInsertion(evict);
        // 到这里，说明 putVal 是一个 create 操作而不是 update 操作，因此不存在旧值，返回 null
        return null;
    }

    /**
     * Initializes or doubles table size.  If null, allocates in
     * accord with initial capacity target held in field threshold.
     * Otherwise, because we are using power-of-two expansion, the
     * elements from each bin must either stay at same index, or move
     * with a power of two offset in the new table.
     * <p>
     * 初始化或加倍表大小。
     * 如果为 null，则根据字段 threshold 中保存的初始容量目标进行分配。
     * 否则，因为我们使用的是 2 的幂扩展，所以每个桶中的元素必须保持在相同的索引上，或者在新表中以 2 的幂偏移量移动。
     *
     * @return the table
     */
    final Node<K, V>[] resize() {
        // oldTab 记录旧的 table；
        Node<K, V>[] oldTab = table;
        // oldCap 记录旧的 table 的长度，oldThr 记录旧的 threshold
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        int oldThr = threshold;
        int newCap, newThr = 0;
        // 如果旧的 table 的长度大于 0，那么就进行扩容
        if (oldCap > 0) {
            // 如果旧的 table 的长度已经达到了最大值，那么就不再扩容，而是直接返回旧的 table
            if (oldCap >= MAXIMUM_CAPACITY) {
                threshold = Integer.MAX_VALUE;
                return oldTab;

                // 否则，就将旧的 table 扩容为原来的 2 倍
            } else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                    oldCap >= DEFAULT_INITIAL_CAPACITY)
                newThr = oldThr << 1; // double threshold

            // 否则说明 oldCap == 0，如果此时指定了 threshold，那么就直接使用 threshold 作为 newCap；
            // 即本次初始化扩容之后，cap == threshold
        } else if (oldThr > 0) { // initial capacity was placed in threshold
            newCap = oldThr;

            // 否则说明 oldCap == 0 && oldThr == 0，即第一次初始化，使用默认的容量和负载因子
        } else {               // zero initial threshold signifies using defaults
            newCap = DEFAULT_INITIAL_CAPACITY;
            newThr = (int) (DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
        }

        // 如果 newThr == 0， 说明是从 else if (oldThr > 0) 分支进入的，那么就需要计算 newThr
        if (newThr == 0) {
            float ft = (float) newCap * loadFactor;
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float) MAXIMUM_CAPACITY ?
                    (int) ft : Integer.MAX_VALUE);
        }
        // 将新的 threshold 赋值给 threshold
        threshold = newThr;
        @SuppressWarnings({"rawtypes", "unchecked"})
        // 创建新的 table
        Node<K, V>[] newTab = (Node<K, V>[]) new Node[newCap];
        // 将新的 table 赋值给 table；
        // 如果当前线程在 putVal 期间，其他线程正在 resize，那么就会出现 table != oldTab 的情况；
        // 甚至如果当前线程在赋值之后，迁移之前，阻塞了；那么如果其他线程正在 get，那么就会取不到数据；如果正在 put，那么就会出现数据丢失；
        table = newTab;

        if (oldTab != null) {
            // 遍历旧的 table，将旧的 table 中的元素迁移到新的 table 中
            for (int j = 0; j < oldCap; ++j) {
                Node<K, V> e;
                // 取到桶中的第一个元素
                if ((e = oldTab[j]) != null) {
                    // 清空该桶
                    oldTab[j] = null;
                    // 如果该桶中只有一个元素，那么就直接将该元素放到新的 table 中
                    if (e.next == null)
                        newTab[e.hash & (newCap - 1)] = e;

                        // 否则如果这是一个红黑树，那么就调用红黑树的迁移方法
                    else if (e instanceof TreeNode) {
                        ((TreeNode<K, V>) e).split(this, newTab, j, oldCap);

                        // 否则，就是链表，那么就需要将链表拆分为两个链表，分别放到新的 table 中
                    } else { // preserve order
                        Node<K, V> loHead = null, loTail = null;
                        Node<K, V> hiHead = null, hiTail = null;
                        // e 和 next 将组成双指针
                        Node<K, V> next;
                        do {
                            next = e.next;
                            // 判断 e 将放到哪个桶中；
                            // 如果 e.hash & oldCap == 0，原桶；将 e 添加到 lo 链表中
                            if ((e.hash & oldCap) == 0) {
                                if (loTail == null)
                                    loHead = e;
                                else
                                    loTail.next = e;
                                loTail = e;

                                // 否则，新桶；将 e 添加到 hi 链表中
                            } else {
                                if (hiTail == null)
                                    hiHead = e;
                                else
                                    hiTail.next = e;
                                hiTail = e;
                            }
                        } while ((e = next) != null);

                        // 将两个链表放到各自放到新的 table 中
                        if (loTail != null) {
                            // 手动打断一下链表，避免死循环
                            loTail.next = null;
                            newTab[j] = loHead;
                        }
                        if (hiTail != null) {
                            hiTail.next = null;
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
            }
        }
        return newTab;
    }

    /**
     * Replaces all linked nodes in bin at index for given hash unless
     * table is too small, in which case resizes instead.
     * <p>
     * 除非 table 太小，否则替换给定哈希索引处 bin 中的所有链接结点，否则调整大小。
     */
    final void treeifyBin(Node<K, V>[] tab, int hash) {
        // n 记录数组的长度；index 记录桶的索引；
        int n, index;
        // e 记录桶中的第一个元素，即链表的头结点
        Node<K, V> e;
        // 如果 table 为空，或者 table 的长度小于 MIN_TREEIFY_CAPACITY，那么就触发 resize
        // 即优先扩容，而不是树化
        if (tab == null || (n = tab.length) < MIN_TREEIFY_CAPACITY)
            resize();

            // 否则，定位到本次发生变更的桶的位置，(n - 1) & hash 就是为了方便定位哈希桶
        else if ((e = tab[index = (n - 1) & hash]) != null) {
            // hd 代表红黑树的头结点；tl 代表尾结点
            TreeNode<K, V> hd = null, tl = null;
            // 先基于链表结点构造红黑树；并且此时尚未树化，而是先变成了双向链表
            // 即红黑树的结点类型 TreeNode 继承了 Node，它同时也可以实现链表（双向链表）的功能
            do {
                TreeNode<K, V> p = replacementTreeNode(e, null);
                // tl 记录了尾结点的位置；即上一个结点的位置，如果 tl==null，说明不存在上一个结点；即当前结点是头结点
                if (tl == null)
                    hd = p;
                else {
                    // 否则，就将当前结点插入到尾结点的后面
                    // prev 是 TreeNode 的属性；next 是 Node 的属性
                    p.prev = tl;
                    tl.next = p;
                }
                // 更新尾结点指针
                tl = p;
            } while ((e = e.next) != null);

            // 将链表转为红黑树
            if ((tab[index] = hd) != null)
                hd.treeify(tab);
        }
    }

    /**
     * Copies all of the mappings from the specified map to this map.
     * These mappings will replace any mappings that this map had for
     * any of the keys currently in the specified map.
     * <p>
     * 将指定 map 中的所有映射复制到此 map。
     * 这些映射将替换此 map 为指定 map 中当前存在的任何键的映射。
     *
     * @param m mappings to be stored in this map
     * @throws NullPointerException if the specified map is null
     */
    public void putAll(Map<? extends K, ? extends V> m) {
        putMapEntries(m, true);
    }

    /**
     * Removes the mapping for the specified key from this map if present.
     * <p>
     * 如果存在，从此 map 中删除指定 key 的映射。
     *
     * @param key key whose mapping is to be removed from the map
     * @return the previous value associated with <tt>key</tt>, or
     * <tt>null</tt> if there was no mapping for <tt>key</tt>.
     * (A <tt>null</tt> return can also indicate that the map
     * previously associated <tt>null</tt> with <tt>key</tt>.)
     */
    public V remove(Object key) {
        Node<K, V> e;
        return (e = removeNode(hash(key), key, null, false, true)) == null ?
                null : e.value;
    }

    /**
     * Implements Map.remove and related methods.
     * <p>
     * 实现 Map.remove 和相关方法。
     *
     * @param hash       hash for key
     * @param key        the key
     * @param value      the value to match if matchValue, else ignored
     * @param matchValue if true only remove if value is equal
     * @param movable    if false do not move other nodes while removing
     * @return the node, or null if none
     */
    final Node<K, V> removeNode(int hash, Object key, Object value,
                                boolean matchValue, boolean movable) {
        // tab 代表 table；p 代表桶中的第一个元素；
        Node<K, V>[] tab;
        Node<K, V> p;
        // n 代表桶的长度；index 代表桶的索引
        int n, index;
        // 如果 table 不为空，且桶中有元素，那么就开始遍历
        if ((tab = table) != null && (n = tab.length) > 0 &&
                (p = tab[index = (n - 1) & hash]) != null) {
            // node 代表要删除的结点，e 代表遍历期间指向的结点
            Node<K, V> node = null, e;
            // k,v 代表 e 的 key 和 value
            K k;
            V v;
            // 如果桶中第一个元素就是要删除的元素，那么就直接将 node 指向该元素
            if (p.hash == hash &&
                    ((k = p.key) == key || (key != null && key.equals(k)))) {
                node = p;

                // 否则意味着需要遍历链表或者树
            } else if ((e = p.next) != null) {
                // 如果是树，那么就调用树的删除方法
                if (p instanceof TreeNode) {
                    node = ((TreeNode<K, V>) p).getTreeNode(hash, key);
                } else {
                    // 否则就是链表，那么就遍历链表
                    do {
                        if (e.hash == hash &&
                                ((k = e.key) == key ||
                                        (key != null && key.equals(k)))) {
                            node = e;
                            break;
                        }
                        // 更新 p，即如果找到了 node，那么 p 将是 node 的前驱结点
                        p = e;
                    } while ((e = e.next) != null);
                }
            }

            // 如果 node 不为空，并且满足了其他的移除条件，那么就需要移除该 node
            if (node != null && (!matchValue || (v = node.value) == value ||
                    (value != null && value.equals(v)))) {
                // 如果 node 是 TreeNode，那么就调用红黑树的删除方法
                if (node instanceof TreeNode) {
                    ((TreeNode<K, V>) node).removeTreeNode(this, tab, movable);

                    // 如果 node == p，说明 node 就是桶中的第一个元素，那么就直接让桶存放 node.next
                } else if (node == p) {
                    tab[index] = node.next;

                    // 否则，说明 node 是链表的内部结点，不是首元结点；
                    // 此时 p 代表 node 的前驱结点，那么就让 p.next = node.next
                } else {
                    p.next = node.next;
                }

                // 累加 modCount，累减 map 的元素数量
                ++modCount;
                --size;
                // 模版方法，供 LinkedHashMap 使用
                afterNodeRemoval(node);
                return node;
            }
        }
        return null;
    }

    /**
     * Removes all of the mappings from this map.
     * The map will be empty after this call returns.
     * <p>
     * 从此 map 中删除所有映射。
     */
    public void clear() {
        // tab 代表 table
        Node<K, V>[] tab;
        // modCount 代表结构性修改次数，累加该值
        modCount++;
        // 如果 table 不为空，且桶中有元素，那么就开始遍历；将每个桶都置为 null
        if ((tab = table) != null && size > 0) {
            size = 0;
            for (int i = 0; i < tab.length; ++i)
                tab[i] = null;
        }
    }

    /**
     * Returns <tt>true</tt> if this map maps one or more keys to the
     * specified value.
     * <p>
     * 如果此 map 将一个或多个键映射到指定值，则返回 true。
     *
     * @param value value whose presence in this map is to be tested
     * @return <tt>true</tt> if this map maps one or more keys to the
     * specified value
     */
    public boolean containsValue(Object value) {
        Node<K, V>[] tab;
        // v 暂存遍历期间指向的 value
        V v;
        if ((tab = table) != null && size > 0) {
            // 遍历每个桶
            for (int i = 0; i < tab.length; ++i) {
                // 遍历桶中的每个结点，即使是红黑树，也仍旧保留了链表的结构
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    // 判断 value 是否相等
                    if ((v = e.value) == value ||
                            (value != null && value.equals(v)))
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns a {@link Set} view of the keys contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own <tt>remove</tt> operation), the results of
     * the iteration are undefined.  The set supports element removal,
     * which removes the corresponding mapping from the map, via the
     * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>,
     * <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
     * operations.  It does not support the <tt>add</tt> or <tt>addAll</tt>
     * operations.
     * <p>
     * 返回此 map 中包含的键的 set 视图。
     * 该 set 受 map 的支持，因此对 map 的更改会反映在 set 中，反之亦然。
     * 如果在对 set 进行迭代时修改了 map（通过迭代器自己的 remove 操作除外），
     * 则迭代的结果是不确定的。
     * 该 set 支持元素删除，该删除会从 map 中删除相应的映射，
     * 通过 Iterator.remove、Set.remove、removeAll、retainAll 和 clear 操作执行。
     * 它不支持 add 或 addAll 操作。
     *
     * @return a set view of the keys contained in this map
     */
    public Set<K> keySet() {
        // 可以理解为，keySet 是对 map 的一个视图
        // 即 keySet 与 map 共享同一份数据，keySet 本身不存任何东西，它提供了以
        Set<K> ks = keySet;
        // keySet 为空的场景只有 HashMap 对象的新建（new、clone、deserialize等）
        if (ks == null) {
            ks = new KeySet();
            keySet = ks;
        }
        return ks;
    }

    final class KeySet extends AbstractSet<K> {
        public final int size() {
            return size;
        }

        public final void clear() {
            HashMap.this.clear();
        }

        public final Iterator<K> iterator() {
            return new KeyIterator();
        }

        public final boolean contains(Object o) {
            return containsKey(o);
        }

        public final boolean remove(Object key) {
            return removeNode(hash(key), key, null, false, true) != null;
        }

        public final Spliterator<K> spliterator() {
            return new KeySpliterator<>(HashMap.this, 0, -1, 0, 0);
        }

        public final void forEach(Consumer<? super K> action) {
            Node<K, V>[] tab;
            if (action == null)
                throw new NullPointerException();
            if (size > 0 && (tab = table) != null) {
                int mc = modCount;
                for (int i = 0; i < tab.length; ++i) {
                    for (Node<K, V> e = tab[i]; e != null; e = e.next)
                        action.accept(e.key);
                }
                if (modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * Returns a {@link Collection} view of the values contained in this map.
     * The collection is backed by the map, so changes to the map are
     * reflected in the collection, and vice-versa.  If the map is
     * modified while an iteration over the collection is in progress
     * (except through the iterator's own <tt>remove</tt> operation),
     * the results of the iteration are undefined.  The collection
     * supports element removal, which removes the corresponding
     * mapping from the map, via the <tt>Iterator.remove</tt>,
     * <tt>Collection.remove</tt>, <tt>removeAll</tt>,
     * <tt>retainAll</tt> and <tt>clear</tt> operations.  It does not
     * support the <tt>add</tt> or <tt>addAll</tt> operations.
     *
     * @return a view of the values contained in this map
     */
    public Collection<V> values() {
        Collection<V> vs = values;
        if (vs == null) {
            vs = new Values();
            values = vs;
        }
        return vs;
    }

    final class Values extends AbstractCollection<V> {
        public final int size() {
            return size;
        }

        public final void clear() {
            HashMap.this.clear();
        }

        public final Iterator<V> iterator() {
            return new ValueIterator();
        }

        public final boolean contains(Object o) {
            return containsValue(o);
        }

        public final Spliterator<V> spliterator() {
            return new ValueSpliterator<>(HashMap.this, 0, -1, 0, 0);
        }

        public final void forEach(Consumer<? super V> action) {
            Node<K, V>[] tab;
            if (action == null)
                throw new NullPointerException();
            if (size > 0 && (tab = table) != null) {
                int mc = modCount;
                for (int i = 0; i < tab.length; ++i) {
                    for (Node<K, V> e = tab[i]; e != null; e = e.next)
                        action.accept(e.value);
                }
                if (modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }
    }

    /**
     * Returns a {@link Set} view of the mappings contained in this map.
     * The set is backed by the map, so changes to the map are
     * reflected in the set, and vice-versa.  If the map is modified
     * while an iteration over the set is in progress (except through
     * the iterator's own <tt>remove</tt> operation, or through the
     * <tt>setValue</tt> operation on a map entry returned by the
     * iterator) the results of the iteration are undefined.  The set
     * supports element removal, which removes the corresponding
     * mapping from the map, via the <tt>Iterator.remove</tt>,
     * <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt> and
     * <tt>clear</tt> operations.  It does not support the
     * <tt>add</tt> or <tt>addAll</tt> operations.
     *
     * @return a set view of the mappings contained in this map
     */
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> es;
        return (es = entrySet) == null ? (entrySet = new EntrySet()) : es;
    }

    final class EntrySet extends AbstractSet<Map.Entry<K, V>> {
        public final int size() {
            return size;
        }

        public final void clear() {
            HashMap.this.clear();
        }

        public final Iterator<Map.Entry<K, V>> iterator() {
            return new EntryIterator();
        }

        public final boolean contains(Object o) {
            if (!(o instanceof Map.Entry))
                return false;
            Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
            Object key = e.getKey();
            Node<K, V> candidate = getNode(hash(key), key);
            return candidate != null && candidate.equals(e);
        }

        public final boolean remove(Object o) {
            if (o instanceof Map.Entry) {
                Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
                Object key = e.getKey();
                Object value = e.getValue();
                return removeNode(hash(key), key, value, true, true) != null;
            }
            return false;
        }

        public final Spliterator<Map.Entry<K, V>> spliterator() {
            return new EntrySpliterator<>(HashMap.this, 0, -1, 0, 0);
        }

        public final void forEach(Consumer<? super Map.Entry<K, V>> action) {
            Node<K, V>[] tab;
            if (action == null)
                throw new NullPointerException();
            if (size > 0 && (tab = table) != null) {
                int mc = modCount;
                for (int i = 0; i < tab.length; ++i) {
                    for (Node<K, V> e = tab[i]; e != null; e = e.next)
                        action.accept(e);
                }
                if (modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }
    }

    // Overrides of JDK8 Map extension methods

    @Override
    public V getOrDefault(Object key, V defaultValue) {
        Node<K, V> e;
        return (e = getNode(hash(key), key)) == null ? defaultValue : e.value;
    }

    @Override
    public V putIfAbsent(K key, V value) {
        return putVal(hash(key), key, value, true, true);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return removeNode(hash(key), key, value, true, true) != null;
    }

    @Override
    public boolean replace(K key, V oldValue, V newValue) {
        Node<K, V> e;
        V v;
        if ((e = getNode(hash(key), key)) != null &&
                ((v = e.value) == oldValue || (v != null && v.equals(oldValue)))) {
            e.value = newValue;
            afterNodeAccess(e);
            return true;
        }
        return false;
    }

    @Override
    public V replace(K key, V value) {
        Node<K, V> e;
        if ((e = getNode(hash(key), key)) != null) {
            V oldValue = e.value;
            e.value = value;
            afterNodeAccess(e);
            return oldValue;
        }
        return null;
    }

    @Override
    public V computeIfAbsent(K key,
                             Function<? super K, ? extends V> mappingFunction) {
        if (mappingFunction == null)
            throw new NullPointerException();
        int hash = hash(key);
        Node<K, V>[] tab;
        Node<K, V> first;
        int n, i;
        int binCount = 0;
        TreeNode<K, V> t = null;
        Node<K, V> old = null;
        if (size > threshold || (tab = table) == null ||
                (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((first = tab[i = (n - 1) & hash]) != null) {
            if (first instanceof TreeNode)
                old = (t = (TreeNode<K, V>) first).getTreeNode(hash, key);
            else {
                Node<K, V> e = first;
                K k;
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        old = e;
                        break;
                    }
                    ++binCount;
                } while ((e = e.next) != null);
            }
            V oldValue;
            if (old != null && (oldValue = old.value) != null) {
                afterNodeAccess(old);
                return oldValue;
            }
        }
        V v = mappingFunction.apply(key);
        if (v == null) {
            return null;
        } else if (old != null) {
            old.value = v;
            afterNodeAccess(old);
            return v;
        } else if (t != null)
            t.putTreeVal(this, tab, hash, key, v);
        else {
            tab[i] = newNode(hash, key, v, first);
            if (binCount >= TREEIFY_THRESHOLD - 1)
                treeifyBin(tab, hash);
        }
        ++modCount;
        ++size;
        afterNodeInsertion(true);
        return v;
    }

    public V computeIfPresent(K key,
                              BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        if (remappingFunction == null)
            throw new NullPointerException();
        Node<K, V> e;
        V oldValue;
        int hash = hash(key);
        if ((e = getNode(hash, key)) != null &&
                (oldValue = e.value) != null) {
            V v = remappingFunction.apply(key, oldValue);
            if (v != null) {
                e.value = v;
                afterNodeAccess(e);
                return v;
            } else
                removeNode(hash, key, null, false, true);
        }
        return null;
    }

    @Override
    public V compute(K key,
                     BiFunction<? super K, ? super V, ? extends V> remappingFunction) {
        if (remappingFunction == null)
            throw new NullPointerException();
        int hash = hash(key);
        Node<K, V>[] tab;
        Node<K, V> first;
        int n, i;
        int binCount = 0;
        TreeNode<K, V> t = null;
        Node<K, V> old = null;
        if (size > threshold || (tab = table) == null ||
                (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((first = tab[i = (n - 1) & hash]) != null) {
            if (first instanceof TreeNode)
                old = (t = (TreeNode<K, V>) first).getTreeNode(hash, key);
            else {
                Node<K, V> e = first;
                K k;
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        old = e;
                        break;
                    }
                    ++binCount;
                } while ((e = e.next) != null);
            }
        }
        V oldValue = (old == null) ? null : old.value;
        V v = remappingFunction.apply(key, oldValue);
        if (old != null) {
            if (v != null) {
                old.value = v;
                afterNodeAccess(old);
            } else
                removeNode(hash, key, null, false, true);
        } else if (v != null) {
            if (t != null)
                t.putTreeVal(this, tab, hash, key, v);
            else {
                tab[i] = newNode(hash, key, v, first);
                if (binCount >= TREEIFY_THRESHOLD - 1)
                    treeifyBin(tab, hash);
            }
            ++modCount;
            ++size;
            afterNodeInsertion(true);
        }
        return v;
    }

    @Override
    public V merge(K key, V value,
                   BiFunction<? super V, ? super V, ? extends V> remappingFunction) {
        if (value == null)
            throw new NullPointerException();
        if (remappingFunction == null)
            throw new NullPointerException();
        int hash = hash(key);
        Node<K, V>[] tab;
        Node<K, V> first;
        int n, i;
        int binCount = 0;
        TreeNode<K, V> t = null;
        Node<K, V> old = null;
        if (size > threshold || (tab = table) == null ||
                (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((first = tab[i = (n - 1) & hash]) != null) {
            if (first instanceof TreeNode)
                old = (t = (TreeNode<K, V>) first).getTreeNode(hash, key);
            else {
                Node<K, V> e = first;
                K k;
                do {
                    if (e.hash == hash &&
                            ((k = e.key) == key || (key != null && key.equals(k)))) {
                        old = e;
                        break;
                    }
                    ++binCount;
                } while ((e = e.next) != null);
            }
        }
        if (old != null) {
            V v;
            if (old.value != null)
                v = remappingFunction.apply(old.value, value);
            else
                v = value;
            if (v != null) {
                old.value = v;
                afterNodeAccess(old);
            } else
                removeNode(hash, key, null, false, true);
            return v;
        }
        if (value != null) {
            if (t != null)
                t.putTreeVal(this, tab, hash, key, value);
            else {
                tab[i] = newNode(hash, key, value, first);
                if (binCount >= TREEIFY_THRESHOLD - 1)
                    treeifyBin(tab, hash);
            }
            ++modCount;
            ++size;
            afterNodeInsertion(true);
        }
        return value;
    }

    @Override
    public void forEach(BiConsumer<? super K, ? super V> action) {
        Node<K, V>[] tab;
        if (action == null)
            throw new NullPointerException();
        if (size > 0 && (tab = table) != null) {
            int mc = modCount;
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next)
                    action.accept(e.key, e.value);
            }
            if (modCount != mc)
                throw new ConcurrentModificationException();
        }
    }

    @Override
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> function) {
        Node<K, V>[] tab;
        if (function == null)
            throw new NullPointerException();
        if (size > 0 && (tab = table) != null) {
            int mc = modCount;
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    e.value = function.apply(e.key, e.value);
                }
            }
            if (modCount != mc)
                throw new ConcurrentModificationException();
        }
    }

    /* ------------------------------------------------------------ */
    // Cloning and serialization

    /**
     * Returns a shallow copy of this <tt>HashMap</tt> instance: the keys and
     * values themselves are not cloned.
     *
     * @return a shallow copy of this map
     */
    @SuppressWarnings("unchecked")
    @Override
    public Object clone() {
        HashMap<K, V> result;
        try {
            result = (HashMap<K, V>) super.clone();
        } catch (CloneNotSupportedException e) {
            // this shouldn't happen, since we are Cloneable
            throw new InternalError(e);
        }
        result.reinitialize();
        result.putMapEntries(this, false);
        return result;
    }

    // These methods are also used when serializing HashSets
    final float loadFactor() {
        return loadFactor;
    }

    final int capacity() {
        return (table != null) ? table.length :
                (threshold > 0) ? threshold :
                        DEFAULT_INITIAL_CAPACITY;
    }

    /**
     * Save the state of the <tt>HashMap</tt> instance to a stream (i.e.,
     * serialize it).
     *
     * @serialData The <i>capacity</i> of the HashMap (the length of the
     * bucket array) is emitted (int), followed by the
     * <i>size</i> (an int, the number of key-value
     * mappings), followed by the key (Object) and value (Object)
     * for each key-value mapping.  The key-value mappings are
     * emitted in no particular order.
     */
    private void writeObject(java.io.ObjectOutputStream s)
            throws IOException {
        int buckets = capacity();
        // Write out the threshold, loadfactor, and any hidden stuff
        s.defaultWriteObject();
        s.writeInt(buckets);
        s.writeInt(size);
        internalWriteEntries(s);
    }

    /**
     * Reconstitutes this map from a stream (that is, deserializes it).
     *
     * @param s the stream
     * @throws ClassNotFoundException if the class of a serialized object
     *                                could not be found
     * @throws IOException            if an I/O error occurs
     */
    private void readObject(ObjectInputStream s)
            throws IOException, ClassNotFoundException {

        ObjectInputStream.GetField fields = s.readFields();

        // Read loadFactor (ignore threshold)
        float lf = fields.get("loadFactor", 0.75f);
        if (lf <= 0 || Float.isNaN(lf))
            throw new InvalidObjectException("Illegal load factor: " + lf);

        lf = Math.min(Math.max(0.25f, lf), 4.0f);
        HashMap.UnsafeHolder.putLoadFactor(this, lf);

        reinitialize();

        s.readInt();                // Read and ignore number of buckets
        int mappings = s.readInt(); // Read number of mappings (size)
        if (mappings < 0) {
            throw new InvalidObjectException("Illegal mappings count: " + mappings);
        } else if (mappings == 0) {
            // use defaults
        } else if (mappings > 0) {
            float fc = (float) mappings / lf + 1.0f;
            int cap = ((fc < DEFAULT_INITIAL_CAPACITY) ?
                    DEFAULT_INITIAL_CAPACITY :
                    (fc >= MAXIMUM_CAPACITY) ?
                            MAXIMUM_CAPACITY :
                            tableSizeFor((int) fc));
            float ft = (float) cap * lf;
            threshold = ((cap < MAXIMUM_CAPACITY && ft < MAXIMUM_CAPACITY) ?
                    (int) ft : Integer.MAX_VALUE);

            // Check Map.Entry[].class since it's the nearest public type to
            // what we're actually creating.
            SharedSecrets.getJavaOISAccess().checkArray(s, Map.Entry[].class, cap);
            @SuppressWarnings({"rawtypes", "unchecked"})
            Node<K, V>[] tab = (Node<K, V>[]) new Node[cap];
            table = tab;

            // Read the keys and values, and put the mappings in the HashMap
            for (int i = 0; i < mappings; i++) {
                @SuppressWarnings("unchecked")
                K key = (K) s.readObject();
                @SuppressWarnings("unchecked")
                V value = (V) s.readObject();
                putVal(hash(key), key, value, false, false);
            }
        }
    }

    // Support for resetting final field during deserializing
    private static final class UnsafeHolder {
        private UnsafeHolder() {
            throw new InternalError();
        }

        private static final sun.misc.Unsafe unsafe
                = sun.misc.Unsafe.getUnsafe();
        private static final long LF_OFFSET;

        static {
            try {
                LF_OFFSET = unsafe.objectFieldOffset(HashMap.class.getDeclaredField("loadFactor"));
            } catch (NoSuchFieldException e) {
                throw new InternalError();
            }
        }

        static void putLoadFactor(HashMap<?, ?> map, float lf) {
            unsafe.putFloat(map, LF_OFFSET, lf);
        }
    }

    /* ------------------------------------------------------------ */
    // iterators

    abstract class HashIterator {
        Node<K, V> next;        // next entry to return
        Node<K, V> current;     // current entry
        int expectedModCount;  // for fast-fail
        int index;             // current slot

        HashIterator() {
            expectedModCount = modCount;
            Node<K, V>[] t = table;
            current = next = null;
            index = 0;
            if (t != null && size > 0) { // advance to first entry
                do {
                } while (index < t.length && (next = t[index++]) == null);
            }
        }

        public final boolean hasNext() {
            return next != null;
        }

        final Node<K, V> nextNode() {
            Node<K, V>[] t;
            Node<K, V> e = next;
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
            if (e == null)
                throw new NoSuchElementException();
            if ((next = (current = e).next) == null && (t = table) != null) {
                do {
                } while (index < t.length && (next = t[index++]) == null);
            }
            return e;
        }

        public final void remove() {
            Node<K, V> p = current;
            if (p == null)
                throw new IllegalStateException();
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
            current = null;
            K key = p.key;
            removeNode(hash(key), key, null, false, false);
            expectedModCount = modCount;
        }
    }

    final class KeyIterator extends HashIterator
            implements Iterator<K> {
        public final K next() {
            return nextNode().key;
        }
    }

    final class ValueIterator extends HashIterator
            implements Iterator<V> {
        public final V next() {
            return nextNode().value;
        }
    }

    final class EntryIterator extends HashIterator
            implements Iterator<Map.Entry<K, V>> {
        public final Map.Entry<K, V> next() {
            return nextNode();
        }
    }

    /* ------------------------------------------------------------ */
    // spliterators

    static class HashMapSpliterator<K, V> {
        final HashMap<K, V> map;
        Node<K, V> current;          // current node
        int index;                  // current index, modified on advance/split
        int fence;                  // one past last index
        int est;                    // size estimate
        int expectedModCount;       // for comodification checks

        HashMapSpliterator(HashMap<K, V> m, int origin,
                           int fence, int est,
                           int expectedModCount) {
            this.map = m;
            this.index = origin;
            this.fence = fence;
            this.est = est;
            this.expectedModCount = expectedModCount;
        }

        final int getFence() { // initialize fence and size on first use
            int hi;
            if ((hi = fence) < 0) {
                HashMap<K, V> m = map;
                est = m.size;
                expectedModCount = m.modCount;
                Node<K, V>[] tab = m.table;
                hi = fence = (tab == null) ? 0 : tab.length;
            }
            return hi;
        }

        public final long estimateSize() {
            getFence(); // force init
            return (long) est;
        }
    }

    static final class KeySpliterator<K, V>
            extends HashMapSpliterator<K, V>
            implements Spliterator<K> {
        KeySpliterator(HashMap<K, V> m, int origin, int fence, int est,
                       int expectedModCount) {
            super(m, origin, fence, est, expectedModCount);
        }

        public KeySpliterator<K, V> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid || current != null) ? null :
                    new KeySpliterator<>(map, lo, index = mid, est >>>= 1,
                            expectedModCount);
        }

        public void forEachRemaining(Consumer<? super K> action) {
            int i, hi, mc;
            if (action == null)
                throw new NullPointerException();
            HashMap<K, V> m = map;
            Node<K, V>[] tab = m.table;
            if ((hi = fence) < 0) {
                mc = expectedModCount = m.modCount;
                hi = fence = (tab == null) ? 0 : tab.length;
            } else
                mc = expectedModCount;
            if (tab != null && tab.length >= hi &&
                    (i = index) >= 0 && (i < (index = hi) || current != null)) {
                Node<K, V> p = current;
                current = null;
                do {
                    if (p == null)
                        p = tab[i++];
                    else {
                        action.accept(p.key);
                        p = p.next;
                    }
                } while (p != null || i < hi);
                if (m.modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }

        public boolean tryAdvance(Consumer<? super K> action) {
            int hi;
            if (action == null)
                throw new NullPointerException();
            Node<K, V>[] tab = map.table;
            if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                while (current != null || index < hi) {
                    if (current == null)
                        current = tab[index++];
                    else {
                        K k = current.key;
                        current = current.next;
                        action.accept(k);
                        if (map.modCount != expectedModCount)
                            throw new ConcurrentModificationException();
                        return true;
                    }
                }
            }
            return false;
        }

        public int characteristics() {
            return (fence < 0 || est == map.size ? Spliterator.SIZED : 0) |
                    Spliterator.DISTINCT;
        }
    }

    static final class ValueSpliterator<K, V>
            extends HashMapSpliterator<K, V>
            implements Spliterator<V> {
        ValueSpliterator(HashMap<K, V> m, int origin, int fence, int est,
                         int expectedModCount) {
            super(m, origin, fence, est, expectedModCount);
        }

        public ValueSpliterator<K, V> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid || current != null) ? null :
                    new ValueSpliterator<>(map, lo, index = mid, est >>>= 1,
                            expectedModCount);
        }

        public void forEachRemaining(Consumer<? super V> action) {
            int i, hi, mc;
            if (action == null)
                throw new NullPointerException();
            HashMap<K, V> m = map;
            Node<K, V>[] tab = m.table;
            if ((hi = fence) < 0) {
                mc = expectedModCount = m.modCount;
                hi = fence = (tab == null) ? 0 : tab.length;
            } else
                mc = expectedModCount;
            if (tab != null && tab.length >= hi &&
                    (i = index) >= 0 && (i < (index = hi) || current != null)) {
                Node<K, V> p = current;
                current = null;
                do {
                    if (p == null)
                        p = tab[i++];
                    else {
                        action.accept(p.value);
                        p = p.next;
                    }
                } while (p != null || i < hi);
                if (m.modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }

        public boolean tryAdvance(Consumer<? super V> action) {
            int hi;
            if (action == null)
                throw new NullPointerException();
            Node<K, V>[] tab = map.table;
            if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                while (current != null || index < hi) {
                    if (current == null)
                        current = tab[index++];
                    else {
                        V v = current.value;
                        current = current.next;
                        action.accept(v);
                        if (map.modCount != expectedModCount)
                            throw new ConcurrentModificationException();
                        return true;
                    }
                }
            }
            return false;
        }

        public int characteristics() {
            return (fence < 0 || est == map.size ? Spliterator.SIZED : 0);
        }
    }

    static final class EntrySpliterator<K, V>
            extends HashMapSpliterator<K, V>
            implements Spliterator<Map.Entry<K, V>> {
        EntrySpliterator(HashMap<K, V> m, int origin, int fence, int est,
                         int expectedModCount) {
            super(m, origin, fence, est, expectedModCount);
        }

        public EntrySpliterator<K, V> trySplit() {
            int hi = getFence(), lo = index, mid = (lo + hi) >>> 1;
            return (lo >= mid || current != null) ? null :
                    new EntrySpliterator<>(map, lo, index = mid, est >>>= 1,
                            expectedModCount);
        }

        public void forEachRemaining(Consumer<? super Map.Entry<K, V>> action) {
            int i, hi, mc;
            if (action == null)
                throw new NullPointerException();
            HashMap<K, V> m = map;
            Node<K, V>[] tab = m.table;
            if ((hi = fence) < 0) {
                mc = expectedModCount = m.modCount;
                hi = fence = (tab == null) ? 0 : tab.length;
            } else
                mc = expectedModCount;
            if (tab != null && tab.length >= hi &&
                    (i = index) >= 0 && (i < (index = hi) || current != null)) {
                Node<K, V> p = current;
                current = null;
                do {
                    if (p == null)
                        p = tab[i++];
                    else {
                        action.accept(p);
                        p = p.next;
                    }
                } while (p != null || i < hi);
                if (m.modCount != mc)
                    throw new ConcurrentModificationException();
            }
        }

        public boolean tryAdvance(Consumer<? super Map.Entry<K, V>> action) {
            int hi;
            if (action == null)
                throw new NullPointerException();
            Node<K, V>[] tab = map.table;
            if (tab != null && tab.length >= (hi = getFence()) && index >= 0) {
                while (current != null || index < hi) {
                    if (current == null)
                        current = tab[index++];
                    else {
                        Node<K, V> e = current;
                        current = current.next;
                        action.accept(e);
                        if (map.modCount != expectedModCount)
                            throw new ConcurrentModificationException();
                        return true;
                    }
                }
            }
            return false;
        }

        public int characteristics() {
            return (fence < 0 || est == map.size ? Spliterator.SIZED : 0) |
                    Spliterator.DISTINCT;
        }
    }

    /* ------------------------------------------------------------ */
    // LinkedHashMap support


    /*
     * The following package-protected methods are designed to be
     * overridden by LinkedHashMap, but not by any other subclass.
     * Nearly all other internal methods are also package-protected
     * but are declared final, so can be used by LinkedHashMap, view
     * classes, and HashSet.
     */

    // Create a regular (non-tree) node
    Node<K, V> newNode(int hash, K key, V value, Node<K, V> next) {
        return new Node<>(hash, key, value, next);
    }

    // For conversion from TreeNodes to plain nodes
    Node<K, V> replacementNode(Node<K, V> p, Node<K, V> next) {
        return new Node<>(p.hash, p.key, p.value, next);
    }

    // Create a tree bin node
    TreeNode<K, V> newTreeNode(int hash, K key, V value, Node<K, V> next) {
        return new TreeNode<>(hash, key, value, next);
    }

    // For treeifyBin
    TreeNode<K, V> replacementTreeNode(Node<K, V> p, Node<K, V> next) {
        return new TreeNode<>(p.hash, p.key, p.value, next);
    }

    /**
     * Reset to initial default state.  Called by clone and readObject.
     */
    void reinitialize() {
        table = null;
        entrySet = null;
        keySet = null;
        values = null;
        modCount = 0;
        threshold = 0;
        size = 0;
    }

    // Callbacks to allow LinkedHashMap post-actions
    void afterNodeAccess(Node<K, V> p) {
    }

    void afterNodeInsertion(boolean evict) {
    }

    void afterNodeRemoval(Node<K, V> p) {
    }

    // Called only from writeObject, to ensure compatible ordering.
    void internalWriteEntries(java.io.ObjectOutputStream s) throws IOException {
        Node<K, V>[] tab;
        if (size > 0 && (tab = table) != null) {
            for (int i = 0; i < tab.length; ++i) {
                for (Node<K, V> e = tab[i]; e != null; e = e.next) {
                    s.writeObject(e.key);
                    s.writeObject(e.value);
                }
            }
        }
    }

    /* ------------------------------------------------------------ */
    // Tree bins

    /**
     * Entry for Tree bins. Extends LinkedHashMap.Entry (which in turn
     * extends Node) so can be used as extension of either regular or
     * linked node.
     */
    static final class TreeNode<K, V> extends LinkedHashMap.Entry<K, V> {
        /**
         * 记录红黑树中当前结点的父结点
         */
        TreeNode<K, V> parent;  // red-black tree links

        /**
         * 记录红黑树中当前结点的左子结点
         */
        TreeNode<K, V> left;

        /**
         * 记录红黑树中当前结点的右子结点
         */
        TreeNode<K, V> right;

        /**
         * 记录作为链表时的前驱结点；
         * 正常情况下，我们删除红黑树中的一个结点时，只需要知道 parent、left、right 即可；
         * 但是，在 HashMap 的实现中，由于链表转化为红黑树的过程是渐进的，也就是说即使转化为红黑树后，链表结构依然保留；
         * 因此在删除节点时，也需要考虑到其链表的前驱节点（prev），以便在删除后将链表的前后节点正确地链接起来
         * <p>
         * 可以理解为 prev 用来保持双向链表结点，以便于操作
         */
        TreeNode<K, V> prev;    // needed to unlink next upon deletion

        /**
         * 标记结点的颜色，为 true 时表示结点为红色，为 false 时表示结点为黑色
         */
        boolean red;

        TreeNode(int hash, K key, V val, Node<K, V> next) {
            super(hash, key, val, next);
        }

        /**
         * Returns root of tree containing this node.
         * <p>
         * 返回当前结点所在的红黑树的根结点
         */
        final TreeNode<K, V> root() {
            for (TreeNode<K, V> r = this, p; ; ) {
                if ((p = r.parent) == null)
                    return r;
                r = p;
            }
        }

        /**
         * Ensures that the given root is the first node of its bin.
         * <p>
         * 确保给定的 root 结点是其所在桶的第一个结点
         * 更新 TreeNode 中的 prev 和 Node 中的 next，使得 root 结点成为 tab 中对应桶内的第一个元素；即链表的首元结点
         */
        static <K, V> void moveRootToFront(Node<K, V>[] tab, TreeNode<K, V> root) {
            // n 记录了桶的数量
            int n;
            if (root != null && tab != null && (n = tab.length) > 0) {
                // 定位到桶的位置
                int index = (n - 1) & root.hash;
                // 取出此时直接存在桶中的元素
                TreeNode<K, V> first = (TreeNode<K, V>) tab[index];
                // 如果它不是 root 结点，那么就将 root 结点放到桶中
                if (root != first) {
                    // rn 记录了 root 结点的后继结点（在链表中的后继结点）
                    Node<K, V> rn;
                    // 将 root 结点放到桶中，替换掉原来的结点
                    tab[index] = root;
                    // rp 记录 root 结点的前驱结点（在链表中的前驱结点）
                    TreeNode<K, V> rp = root.prev;

                    // 在链表中先移除 root
                    if ((rn = root.next) != null)
                        ((TreeNode<K, V>) rn).prev = rp;
                    if (rp != null)
                        rp.next = rn;

                    // 将 root 放到链表头部的位置，即 first 的前面
                    if (first != null)
                        first.prev = root;
                    root.next = first;
                    root.prev = null;
                }
                assert checkInvariants(root);
            }
        }

        /**
         * Finds the node starting at root p with the given hash and key.
         * The kc argument caches comparableClassFor(key) upon first use
         * comparing keys.
         * <p>
         * 从 root 结点开始查找，找到哈希值和 key 都相等的结点
         * kc 用于缓存 key 的 ComparableClass 对象，以便在比较 key 时使用
         *
         * @param h  hash
         * @param k  key
         * @param kc comparableClassFor(key)
         */
        final TreeNode<K, V> find(int h, Object k, Class<?> kc) {
            // 从 this 结点开始查找；一般 find 方法的调用方会传入 root 结点
            TreeNode<K, V> p = this;
            do {
                /*
                定义临时变量
                 */
                // ph 记录当前结点的哈希值，dir 记录查找方向（即记录比较的结果）
                int ph, dir;
                // pk 记录当前结点的 key
                K pk;
                // pl 记录当前结点的左子结点，pr 记录当前结点的右子结点
                // q 作为临时变量，记录递归调用的结果
                TreeNode<K, V> pl = p.left, pr = p.right, q;

                /*
                开始操作
                 */
                // 如果当前结点的哈希值大于目标哈希值，则向左查找
                if ((ph = p.hash) > h)
                    p = pl;
                    // 如果当前结点的哈希值小于目标哈希值，则向右查找
                else if (ph < h)
                    p = pr;

                    // 如果当前结点的哈希值等于目标哈希值，则比较当前结点的 key 和目标 key；
                    // 如果 key 相等，则返回当前结点
                else if ((pk = p.key) == k || (k != null && k.equals(pk)))
                    return p;

                    // 如果 hash 值一样，那么就判断左右子节点是否有一侧是 null，如果有一侧是 null，那么就向另一侧查找
                else if (pl == null)
                    p = pr;
                else if (pr == null)
                    p = pl;

                    // 如果两侧都存在结点，那么就判断当前结点是否实现了 Comparable 接口，如果实现了，那么就调用 compareTo 方法比较 key 的大小
                    // 随后根据 compareTo 的结果决定向左还是向右查找
                else if ((kc != null ||
                        (kc = comparableClassFor(k)) != null) &&
                        (dir = compareComparables(kc, k, pk)) != 0)
                    p = (dir < 0) ? pl : pr;

                    // 否则向右递归寻找，如果在右侧找到了值，那么直接返回
                else if ((q = pr.find(h, k, kc)) != null)
                    return q;
                else
                    // 否则向左查找，且不递归
                    p = pl;
            } while (p != null);

            // 如果到了叶子结点，那么说明没找到，返回 null
            return null;
        }

        /**
         * Calls find for root node.
         * <p>
         * 从 root 结点开始查找，找到哈希值和 key 都相等的结点
         *
         * @param h hash
         * @param k key
         */
        final TreeNode<K, V> getTreeNode(int h, Object k) {
            // parent 指当前节点的父节点
            // ((parent != null) ? root() : this)  先找到 root 结点
            // 然后再从 root 结点开始查找
            return ((parent != null) ? root() : this).find(h, k, null);
        }

        /**
         * Tie-breaking utility for ordering insertions when equal
         * hashCodes and non-comparable. We don't require a total
         * order, just a consistent insertion rule to maintain
         * equivalence across rebalancings. Tie-breaking further than
         * necessary simplifies testing a bit.
         * <p>
         * 当哈希值相等并且没有实现 Comparable 接口时，用于比较两个 key 的大小。
         * 我们并不强调一个完整的顺序，只需要一个一致的插入规则来维护重新平衡时的一致性。
         * 这能简化测试。
         */
        static int tieBreakOrder(Object a, Object b) {
            int d;
            if (a == null
                    || b == null
                    || (d = a.getClass().getName().compareTo(b.getClass().getName())) == 0)
                d = (System.identityHashCode(a) <= System.identityHashCode(b) ?
                        -1 : 1);
            return d;
        }

        /**
         * Forms tree of the nodes linked from this node.
         * <p>
         * 将链表转化为红黑树，此时 root 结点及其关联的所有结点已经是 TreeNode 了；
         * 所谓的转化是指，将 TreeNode 节点中的 left、right、parent 等属性设置正确；同时在链表维度将 root 放到链表的首元结点（修改 prev 和 next）
         */
        final void treeify(Node<K, V>[] tab) {
            // 声明 root 结点，即红黑树的根结点
            TreeNode<K, V> root = null;
            // 从当前结点 this 开始，将 this 及 this.next 之后的所有结点都添加到 root 上
            for (TreeNode<K, V> x = this, next; x != null; x = next) {
                // x 代表本次正在遍历的结点
                // next = x.next，代表下一个要遍历的结点
                next = (TreeNode<K, V>) x.next;
                // 初始化 left 和 right 为 null
                x.left = x.right = null;
                // 如果 root 为 null，说明当前结点是第一个要添加到红黑树中的结点，因此将其赋值给 root
                if (root == null) {
                    x.parent = null;
                    x.red = false;
                    root = x;

                    // 否则需要将当前结点以红黑树的方式添加到红黑树中
                } else {
                    // 记录当前结点相关的信息
                    K k = x.key;
                    int h = x.hash;
                    Class<?> kc = null;
                    // 从 root 开始迭代，定位到当前结点应该插入的位置
                    for (TreeNode<K, V> p = root; ; ) {
                        // ph 记录当前结点的哈希值，dir 记录查找方向（即记录比较的结果）
                        int dir, ph;
                        // pk 记录当前结点的 key
                        K pk = p.key;
                        // 如果当前结点的 hash 大于待插入结点的 hash，那么就向左查找
                        if ((ph = p.hash) > h) {
                            dir = -1;
                            // 如果当前结点的 hash 小于待插入结点的 hash，那么就向右查找
                        } else if (ph < h) {
                            dir = 1;

                            // 否则说明哈希值相等，此时尝试比较 key 的大小
                        } else if ((kc == null &&
                                (kc = comparableClassFor(k)) == null) ||
                                (dir = compareComparables(kc, k, pk)) == 0) {

                            dir = tieBreakOrder(k, pk);
                        }

                        // xp 可能作为 x 的 parent
                        TreeNode<K, V> xp = p;
                        // p 将向左或右迭代一位，如果迭代后 p == null，说明已经达到了叶子结点的位置（红黑树中叶子节点是 Nil）
                        if ((p = (dir <= 0) ? p.left : p.right) == null) {
                            // 插入 x 结点
                            x.parent = xp;
                            if (dir <= 0)
                                xp.left = x;
                            else
                                xp.right = x;
                            // 完成红黑树的平衡操作，即每插一个元素就会平衡一次
                            root = balanceInsertion(root, x);
                            break;
                        }
                    }
                }
            }
            // 此时 this 结点仍然占据着桶中第一个元素的位置；但是它可能并不是红黑树中的 root 结点
            moveRootToFront(tab, root);
        }

        /**
         * Returns a list of non-TreeNodes replacing those linked from
         * this node.
         * <p>
         * 将红黑树转化为链表
         */
        final Node<K, V> untreeify(HashMap<K, V> map) {
            Node<K, V> hd = null, tl = null;
            // 以链表的方式遍历 TreeNode
            for (Node<K, V> q = this; q != null; q = q.next) {
                // 基于 TreeNode 中的信息重新构造 Node
                Node<K, V> p = map.replacementNode(q, null);
                if (tl == null)
                    hd = p;
                else
                    tl.next = p;
                tl = p;
            }
            return hd;
        }

        /**
         * Tree version of putVal.
         * <p>
         * 向红黑树中插入一个结点
         */
        final TreeNode<K, V> putTreeVal(HashMap<K, V> map, Node<K, V>[] tab,
                                        int h, K k, V v) {
            Class<?> kc = null;
            // 标记有没有找到相同 key 的结点
            boolean searched = false;
            // 找 root
            TreeNode<K, V> root = (parent != null) ? root() : this;
            // 从 root 开始查找
            for (TreeNode<K, V> p = root; ; ) {
                // dir 记录查找方向（即记录比较的结果），ph 记录当前结点的哈希值
                int dir, ph;
                // pk 记录当前结点的 key
                K pk;
                // 在 hash 值不一样的场景下，根据 hash 值的大小决定向左还是向右查找
                if ((ph = p.hash) > h) {
                    dir = -1;
                } else if (ph < h) {
                    dir = 1;
                }

                // 否则，说明 hash 值一样，那么就需要比较 equals；如果遇到 equal 的 key 就直接 return；
                else if ((pk = p.key) == k || (k != null && k.equals(pk)))
                    return p;

                // 否则，说明目前还没有找到相同 key 的结点，那么就继续向左或向右查找（通过 compare 查找）
                else if ((kc == null &&
                        (kc = comparableClassFor(k)) == null) ||
                        (dir = compareComparables(kc, k, pk)) == 0) {
                    if (!searched) {
                        TreeNode<K, V> q, ch;
                        searched = true;
                        if (((ch = p.left) != null &&
                                (q = ch.find(h, k, kc)) != null) ||
                                ((ch = p.right) != null &&
                                        (q = ch.find(h, k, kc)) != null))
                            return q;
                    }
                    dir = tieBreakOrder(k, pk);
                }

                TreeNode<K, V> xp = p;
                if ((p = (dir <= 0) ? p.left : p.right) == null) {
                    Node<K, V> xpn = xp.next;
                    TreeNode<K, V> x = map.newTreeNode(h, k, v, xpn);
                    if (dir <= 0)
                        xp.left = x;
                    else
                        xp.right = x;
                    xp.next = x;
                    x.parent = x.prev = xp;
                    if (xpn != null)
                        ((TreeNode<K, V>) xpn).prev = x;
                    moveRootToFront(tab, balanceInsertion(root, x));
                    return null;
                }
            }
        }

        /**
         * Removes the given node, that must be present before this call.
         * This is messier than typical red-black deletion code because we
         * cannot swap the contents of an interior node with a leaf
         * successor that is pinned by "next" pointers that are accessible
         * independently during traversal. So instead we swap the tree
         * linkages. If the current tree appears to have too few nodes,
         * the bin is converted back to a plain bin. (The test triggers
         * somewhere between 2 and 6 nodes, depending on tree structure).
         */
        final void removeTreeNode(HashMap<K, V> map, Node<K, V>[] tab,
                                  boolean movable) {
            int n;
            if (tab == null || (n = tab.length) == 0)
                return;
            int index = (n - 1) & hash;
            TreeNode<K, V> first = (TreeNode<K, V>) tab[index], root = first, rl;
            TreeNode<K, V> succ = (TreeNode<K, V>) next, pred = prev;
            if (pred == null)
                tab[index] = first = succ;
            else
                pred.next = succ;
            if (succ != null)
                succ.prev = pred;
            if (first == null)
                return;
            if (root.parent != null)
                root = root.root();
            if (root == null
                    || (movable
                    && (root.right == null
                    || (rl = root.left) == null
                    || rl.left == null))) {
                tab[index] = first.untreeify(map);  // too small
                return;
            }
            TreeNode<K, V> p = this, pl = left, pr = right, replacement;
            if (pl != null && pr != null) {
                TreeNode<K, V> s = pr, sl;
                while ((sl = s.left) != null) // find successor
                    s = sl;
                boolean c = s.red;
                s.red = p.red;
                p.red = c; // swap colors
                TreeNode<K, V> sr = s.right;
                TreeNode<K, V> pp = p.parent;
                if (s == pr) { // p was s's direct parent
                    p.parent = s;
                    s.right = p;
                } else {
                    TreeNode<K, V> sp = s.parent;
                    if ((p.parent = sp) != null) {
                        if (s == sp.left)
                            sp.left = p;
                        else
                            sp.right = p;
                    }
                    if ((s.right = pr) != null)
                        pr.parent = s;
                }
                p.left = null;
                if ((p.right = sr) != null)
                    sr.parent = p;
                if ((s.left = pl) != null)
                    pl.parent = s;
                if ((s.parent = pp) == null)
                    root = s;
                else if (p == pp.left)
                    pp.left = s;
                else
                    pp.right = s;
                if (sr != null)
                    replacement = sr;
                else
                    replacement = p;
            } else if (pl != null)
                replacement = pl;
            else if (pr != null)
                replacement = pr;
            else
                replacement = p;
            if (replacement != p) {
                TreeNode<K, V> pp = replacement.parent = p.parent;
                if (pp == null)
                    root = replacement;
                else if (p == pp.left)
                    pp.left = replacement;
                else
                    pp.right = replacement;
                p.left = p.right = p.parent = null;
            }

            TreeNode<K, V> r = p.red ? root : balanceDeletion(root, replacement);

            if (replacement == p) {  // detach
                TreeNode<K, V> pp = p.parent;
                p.parent = null;
                if (pp != null) {
                    if (p == pp.left)
                        pp.left = null;
                    else if (p == pp.right)
                        pp.right = null;
                }
            }
            if (movable)
                moveRootToFront(tab, r);
        }

        /**
         * Splits nodes in a tree bin into lower and upper tree bins,
         * or untreeifies if now too small. Called only from resize;
         * see above discussion about split bits and indices.
         *
         * @param map   the map
         * @param tab   the table for recording bin heads
         * @param index the index of the table being split
         * @param bit   the bit of hash to split on
         */
        final void split(HashMap<K, V> map, Node<K, V>[] tab, int index, int bit) {
            TreeNode<K, V> b = this;
            // Relink into lo and hi lists, preserving order
            TreeNode<K, V> loHead = null, loTail = null;
            TreeNode<K, V> hiHead = null, hiTail = null;
            int lc = 0, hc = 0;
            for (TreeNode<K, V> e = b, next; e != null; e = next) {
                next = (TreeNode<K, V>) e.next;
                e.next = null;
                if ((e.hash & bit) == 0) {
                    if ((e.prev = loTail) == null)
                        loHead = e;
                    else
                        loTail.next = e;
                    loTail = e;
                    ++lc;
                } else {
                    if ((e.prev = hiTail) == null)
                        hiHead = e;
                    else
                        hiTail.next = e;
                    hiTail = e;
                    ++hc;
                }
            }

            if (loHead != null) {
                if (lc <= UNTREEIFY_THRESHOLD)
                    tab[index] = loHead.untreeify(map);
                else {
                    tab[index] = loHead;
                    if (hiHead != null) // (else is already treeified)
                        loHead.treeify(tab);
                }
            }
            if (hiHead != null) {
                if (hc <= UNTREEIFY_THRESHOLD)
                    tab[index + bit] = hiHead.untreeify(map);
                else {
                    tab[index + bit] = hiHead;
                    if (loHead != null)
                        hiHead.treeify(tab);
                }
            }
        }

        /* ------------------------------------------------------------ */
        // Red-black tree methods, all adapted from CLR

        static <K, V> TreeNode<K, V> rotateLeft(TreeNode<K, V> root,
                                                TreeNode<K, V> p) {
            TreeNode<K, V> r, pp, rl;
            if (p != null && (r = p.right) != null) {
                if ((rl = p.right = r.left) != null)
                    rl.parent = p;
                if ((pp = r.parent = p.parent) == null)
                    (root = r).red = false;
                else if (pp.left == p)
                    pp.left = r;
                else
                    pp.right = r;
                r.left = p;
                p.parent = r;
            }
            return root;
        }

        static <K, V> TreeNode<K, V> rotateRight(TreeNode<K, V> root,
                                                 TreeNode<K, V> p) {
            TreeNode<K, V> l, pp, lr;
            if (p != null && (l = p.left) != null) {
                if ((lr = p.left = l.right) != null)
                    lr.parent = p;
                if ((pp = l.parent = p.parent) == null)
                    (root = l).red = false;
                else if (pp.right == p)
                    pp.right = l;
                else
                    pp.left = l;
                l.right = p;
                p.parent = l;
            }
            return root;
        }

        /**
         * 在向红黑树中插入新的结点后，进行必要的调整，以保持红黑树的性质
         *
         * @param root
         * @param x
         * @param <K>
         * @param <V>
         * @return
         */
        static <K, V> TreeNode<K, V> balanceInsertion(TreeNode<K, V> root,
                                                      TreeNode<K, V> x) {
            // root 代表根节点，x 代表刚加入到红黑树中的结点
            x.red = true;
            // xp 代表 x 的父节点，xpp 代表 x 的祖父节点，xppl 代表祖父节点的左子节点，xppr 代表祖父节点的右子节点
            for (TreeNode<K, V> xp, xpp, xppl, xppr; ; ) {
                // 如果 x 的父节点为 null，说明 x 就是根节点，此时将 x 的颜色置为黑色，然后返回
                if ((xp = x.parent) == null) {
                    x.red = false;
                    return x;

                    // 如果 x 的父节点为黑色，或者 x 的祖父节点为 null，即父结点为根节点，那么就不需要调整，直接返回
                } else if (!xp.red || (xpp = xp.parent) == null) {
                    return root;

                    // 否则说明 x 不是根节点，且 x 的父节点为红色，并且父结点不为根节点
                } else {

                    // 如果 x 的父节点是祖父节点的左子节点
                    if (xp == (xppl = xpp.left)) {
                        // 如果 x 的祖父节点的右子节点（父结点的兄弟结点）存在，且也为红色；则反转颜色
                        if ((xppr = xpp.right) != null && xppr.red) {
                            //  将祖父结点的左右孩子结点（x 的父结点和父结点的兄弟结点）的颜色都置为黑色
                            xppr.red = false;
                            xp.red = false;
                            // 将祖父结点的颜色置为红色
                            xpp.red = true;
                            // 将 x 指向祖父结点，继续向上调整；即移动了两层
                            x = xpp;

                            // 否则，如果 x 的祖父结点的右子节点（父结点的兄弟结点）不存在，或者为黑色；则需要进行旋转操作
                        } else {
                            // 如果 x 是父结点的右子节点，那么就需要先进行左旋操作，将 x 转化为左子节点
                            if (x == xp.right) {
                                root = rotateLeft(root, x = xp);
                                // 旋转之后，x 的父结点和祖父结点将会变化，因此需要重新赋值
                                xpp = (xp = x.parent) == null ? null : xp.parent;
                            }
                            if (xp != null) {
                                xp.red = false;
                                if (xpp != null) {
                                    xpp.red = true;
                                    root = rotateRight(root, xpp);
                                }
                            }
                        }

                        // 如果 x 的父节点是祖父节点的右子节点
                    } else {
                        // 如果 x 的祖父节点的左子节点（父结点的兄弟结点）存在，且也为红色；则反转颜色
                        if (xppl != null && xppl.red) {
                            // 将祖父结点的左右孩子结点（x 的父结点和父结点的兄弟结点）的颜色都置为黑色
                            xppl.red = false;
                            xp.red = false;
                            // 将祖父结点的颜色置为红色
                            xpp.red = true;
                            // 将 x 指向祖父结点，继续向上调整；即移动了两层
                            x = xpp;
                        } else {
                            // 否则，如果 x 的祖父结点的左子节点（父结点的兄弟结点）不存在，或者为黑色；则需要进行旋转操作
                            if (x == xp.left) {
                                root = rotateRight(root, x = xp);
                                xpp = (xp = x.parent) == null ? null : xp.parent;
                            }
                            if (xp != null) {
                                xp.red = false;
                                if (xpp != null) {
                                    xpp.red = true;
                                    root = rotateLeft(root, xpp);
                                }
                            }
                        }
                    }
                }

            }

            // 完成循环之后，即可保证树中任何一个节点到其每一个叶子节点包含相同数量的黑色结点；且没有任何一条路径会遇到两个连续的红色结点
        }

        static <K, V> TreeNode<K, V> balanceDeletion(TreeNode<K, V> root,
                                                     TreeNode<K, V> x) {
            for (TreeNode<K, V> xp, xpl, xpr; ; ) {
                if (x == null || x == root)
                    return root;
                else if ((xp = x.parent) == null) {
                    x.red = false;
                    return x;
                } else if (x.red) {
                    x.red = false;
                    return root;
                } else if ((xpl = xp.left) == x) {
                    if ((xpr = xp.right) != null && xpr.red) {
                        xpr.red = false;
                        xp.red = true;
                        root = rotateLeft(root, xp);
                        xpr = (xp = x.parent) == null ? null : xp.right;
                    }
                    if (xpr == null)
                        x = xp;
                    else {
                        TreeNode<K, V> sl = xpr.left, sr = xpr.right;
                        if ((sr == null || !sr.red) &&
                                (sl == null || !sl.red)) {
                            xpr.red = true;
                            x = xp;
                        } else {
                            if (sr == null || !sr.red) {
                                if (sl != null)
                                    sl.red = false;
                                xpr.red = true;
                                root = rotateRight(root, xpr);
                                xpr = (xp = x.parent) == null ?
                                        null : xp.right;
                            }
                            if (xpr != null) {
                                xpr.red = (xp == null) ? false : xp.red;
                                if ((sr = xpr.right) != null)
                                    sr.red = false;
                            }
                            if (xp != null) {
                                xp.red = false;
                                root = rotateLeft(root, xp);
                            }
                            x = root;
                        }
                    }
                } else { // symmetric
                    if (xpl != null && xpl.red) {
                        xpl.red = false;
                        xp.red = true;
                        root = rotateRight(root, xp);
                        xpl = (xp = x.parent) == null ? null : xp.left;
                    }
                    if (xpl == null)
                        x = xp;
                    else {
                        TreeNode<K, V> sl = xpl.left, sr = xpl.right;
                        if ((sl == null || !sl.red) &&
                                (sr == null || !sr.red)) {
                            xpl.red = true;
                            x = xp;
                        } else {
                            if (sl == null || !sl.red) {
                                if (sr != null)
                                    sr.red = false;
                                xpl.red = true;
                                root = rotateLeft(root, xpl);
                                xpl = (xp = x.parent) == null ?
                                        null : xp.left;
                            }
                            if (xpl != null) {
                                xpl.red = (xp == null) ? false : xp.red;
                                if ((sl = xpl.left) != null)
                                    sl.red = false;
                            }
                            if (xp != null) {
                                xp.red = false;
                                root = rotateRight(root, xp);
                            }
                            x = root;
                        }
                    }
                }
            }
        }

        /**
         * Recursive invariant check
         */
        static <K, V> boolean checkInvariants(TreeNode<K, V> t) {
            TreeNode<K, V> tp = t.parent, tl = t.left, tr = t.right,
                    tb = t.prev, tn = (TreeNode<K, V>) t.next;
            if (tb != null && tb.next != t)
                return false;
            if (tn != null && tn.prev != t)
                return false;
            if (tp != null && t != tp.left && t != tp.right)
                return false;
            if (tl != null && (tl.parent != t || tl.hash > t.hash))
                return false;
            if (tr != null && (tr.parent != t || tr.hash < t.hash))
                return false;
            if (t.red && tl != null && tl.red && tr != null && tr.red)
                return false;
            if (tl != null && !checkInvariants(tl))
                return false;
            if (tr != null && !checkInvariants(tr))
                return false;
            return true;
        }
    }

}
