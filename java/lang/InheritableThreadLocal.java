/*
 * Copyright (c) 1998, 2012, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * This class extends <tt>ThreadLocal</tt> to provide inheritance of values
 * from parent thread to child thread: when a child thread is created, the
 * child receives initial values for all inheritable thread-local variables
 * for which the parent has values.  Normally the child's values will be
 * identical to the parent's; however, the child's value can be made an
 * arbitrary function of the parent's by overriding the <tt>childValue</tt>
 * method in this class.
 * <p>
 * 此类扩展了ThreadLocal，以提供从父线程到子线程的值的继承：当创建子线程时，子线程接收父线程具有值的所有可继承线程本地变量的初始值。
 * 通常，子值将与父值相同；但是，通过在此类中覆盖 childValue 方法，可以使子值成为父值的任意函数。
 *
 * <p>Inheritable thread-local variables are used in preference to
 * ordinary thread-local variables when the per-thread-attribute being
 * maintained in the variable (e.g., User ID, Transaction ID) must be
 * automatically transmitted to any child threads that are created.
 * <p>
 * 针对那些每个线程都需要存储的信息，并且必须自动传给子线程时（例如，用户ID，事务ID）时，
 * 优先使用 Inheritable thread-local 而不是 ordinary thread-local。
 *
 * @author Josh Bloch and Doug Lea
 * @see ThreadLocal
 * @since 1.2
 */

public class InheritableThreadLocal<T> extends ThreadLocal<T> {
    /**
     * Computes the child's initial value for this inheritable thread-local
     * variable as a function of the parent's value at the time the child
     * thread is created.  This method is called from within the parent
     * thread before the child is started.
     * <p>
     * 计算子线程的初始值，作为父线程创建子线程时父线程值的函数。
     * 此方法在子线程启动之前从父线程内部调用。-> Thread.init 的时候就会调用
     *
     * <p>
     * This method merely returns its input argument, and should be overridden
     * if a different behavior is desired.
     *
     * @param parentValue the parent thread's value
     * @return the child thread's initial value
     */
    protected T childValue(T parentValue) {
        return parentValue;
    }

    /**
     * Get the map associated with a ThreadLocal.
     * <p>
     * 获取与 ThreadLocal 关联的 map
     * InheritableThreadLocal 存在 inheritableThreadLocals 中；
     * ThreadLocal 存在 threadLocals 中；
     *
     * @param t the current thread
     */
    ThreadLocalMap getMap(Thread t) {
        return t.inheritableThreadLocals;
    }

    /**
     * Create the map associated with a ThreadLocal.
     *
     * @param t          the current thread
     * @param firstValue value for the initial entry of the table.
     */
    void createMap(Thread t, T firstValue) {
        t.inheritableThreadLocals = new ThreadLocalMap(this, firstValue);
    }
}
