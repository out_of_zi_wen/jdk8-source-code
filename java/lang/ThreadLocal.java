/*
 * Copyright (c) 1997, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import jdk.internal.misc.TerminatingThreadLocal;

import java.lang.ref.*;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * This class provides thread-local variables.  These variables differ from
 * their normal counterparts in that each thread that accesses one (via its
 * {@code get} or {@code set} method) has its own, independently initialized
 * copy of the variable.  {@code ThreadLocal} instances are typically private
 * static fields in classes that wish to associate state with a thread (e.g.,
 * a user ID or Transaction ID).
 * <p>
 * 这个类提供了线程本地变量。
 * 每个访问一个的线程（通过它的 get 或 set 方法）都有它自己的，独立初始化的变量的副本。
 * ThreadLocal 实例通常是类中希望将状态与线程关联的私有静态字段（例如，用户 ID 或事务 ID）。
 *
 * <p>For example, the class below generates unique identifiers local to each
 * thread.
 * A thread's id is assigned the first time it invokes {@code ThreadId.get()}
 * and remains unchanged on subsequent calls.
 * <pre>
 * import java.util.concurrent.atomic.AtomicInteger;
 *
 * public class ThreadId {
 *     // Atomic integer containing the next thread ID to be assigned
 *     private static final AtomicInteger nextId = new AtomicInteger(0);
 *
 *     // Thread local variable containing each thread's ID
 *     private static final ThreadLocal&lt;Integer&gt; threadId =
 *         new ThreadLocal&lt;Integer&gt;() {
 *             &#64;Override protected Integer initialValue() {
 *                 return nextId.getAndIncrement();
 *         }
 *     };
 *
 *     // Returns the current thread's unique ID, assigning it if necessary
 *     public static int get() {
 *         return threadId.get();
 *     }
 * }
 * </pre>
 * <p>Each thread holds an implicit reference to its copy of a thread-local
 * variable as long as the thread is alive and the {@code ThreadLocal}
 * instance is accessible; after a thread goes away, all of its copies of
 * thread-local instances are subject to garbage collection (unless other
 * references to these copies exist).
 *
 * @author Josh Bloch and Doug Lea
 * @since 1.2
 */
public class ThreadLocal<T> {

    // TODO 【REMIND】 以前经常不理解，其实 ThreadLocal 的隔离其实就是将那些值放到 Thread 类（ThreadLocalMap）。
    // 然后将 ThreadLocal 作为 Key，具体存储的值作为 Value，这样就实现了隔离。


    /**
     * ThreadLocals rely on per-thread linear-probe hash maps attached
     * to each thread (Thread.threadLocals and
     * inheritableThreadLocals).  The ThreadLocal objects act as keys,
     * searched via threadLocalHashCode.  This is a custom hash code
     * (useful only within ThreadLocalMaps) that eliminates collisions
     * in the common case where consecutively constructed ThreadLocals
     * are used by the same threads, while remaining well-behaved in
     * less common cases.
     * <p>
     * ThreadLocals 依赖于每个线程（Thread.threadLocals 和 inheritableThreadLocals）附加的线性探测哈希映射。
     * ThreadLocal 对象充当键，通过 threadLocalHashCode 进行搜索。
     * 这是一个自定义哈希码（仅在 ThreadLocalMaps 中有用），它消除了在同一线程中使用连续构造的 ThreadLocals 的常见情况下的冲突，
     * 同时在不太常见的情况下保持良好的行为。
     */
    private final int threadLocalHashCode = nextHashCode();

    /**
     * The next hash code to be given out. Updated atomically. Starts at
     * zero.
     */
    private static AtomicInteger nextHashCode =
            new AtomicInteger();

    /**
     * The difference between successively generated hash codes - turns
     * implicit sequential thread-local IDs into near-optimally spread
     * multiplicative hash values for power-of-two-sized tables.
     * <p>
     * 连续生成的哈希码之间的差异 - 将隐式顺序线程本地 ID 转换为用于二次幂大小表的近乎最优的乘法哈希值。
     */
    private static final int HASH_INCREMENT = 0x61c88647;

    /**
     * Returns the next hash code.
     */
    private static int nextHashCode() {
        return nextHashCode.getAndAdd(HASH_INCREMENT);
    }

    /**
     * Returns the current thread's "initial value" for this
     * thread-local variable.  This method will be invoked the first
     * time a thread accesses the variable with the {@link #get}
     * method, unless the thread previously invoked the {@link #set}
     * method, in which case the {@code initialValue} method will not
     * be invoked for the thread.  Normally, this method is invoked at
     * most once per thread, but it may be invoked again in case of
     * subsequent invocations of {@link #remove} followed by {@link #get}.
     * <p>
     * 返回此线程本地变量的当前线程的“初始值”。
     * 除非线程先前调用了 set 方法，否则此方法将在线程首次使用 get 方法访问变量时调用。
     * 通常，此方法每个线程最多调用一次，但是如果随后调用 remove，然后调用 get，则可能再次调用它。
     *
     * <p>This implementation simply returns {@code null}; if the
     * programmer desires thread-local variables to have an initial
     * value other than {@code null}, {@code ThreadLocal} must be
     * subclassed, and this method overridden.  Typically, an
     * anonymous inner class will be used.
     * <p>
     * 此实现只返回 null；
     * 如果程序员希望线程本地变量具有除 null 以外的初始值，则必须对 ThreadLocal 进行继承，并覆盖此方法。
     * 通常，将使用匿名内部类。
     *
     * @return the initial value for this thread-local
     */
    protected T initialValue() {
        return null;
    }

    /**
     * Creates a thread local variable. The initial value of the variable is
     * determined by invoking the {@code get} method on the {@code Supplier}.
     * <p>
     * 创建线程本地变量。
     * 变量的 initial value 是通过调用 Supplier 上的 get 方法来确定的。
     *
     * @param <S>      the type of the thread local's value
     * @param supplier the supplier to be used to determine the initial value
     * @return a new thread local variable
     * @throws NullPointerException if the specified supplier is null
     * @since 1.8
     */
    public static <S> ThreadLocal<S> withInitial(Supplier<? extends S> supplier) {
        return new SuppliedThreadLocal<>(supplier);
    }

    /**
     * Creates a thread local variable.
     *
     * @see #withInitial(java.util.function.Supplier)
     */
    public ThreadLocal() {
    }

    /**
     * Returns the value in the current thread's copy of this
     * thread-local variable.  If the variable has no value for the
     * current thread, it is first initialized to the value returned
     * by an invocation of the {@link #initialValue} method.
     * <p>
     * 返回此线程本地变量的当前线程副本中的值。
     * 如果变量对当前线程没有值，则首先将其初始化为调用 initialValue 方法返回的值。
     * <p>
     * 最多只会清理一截连续的已回收的无效 entry
     *
     * @return the current thread's value of this thread-local
     */
    public T get() {
        // 获取当前线程
        Thread t = Thread.currentThread();
        // 获取当前线程的 ThreadLocalMap，直接就存储在 Thread 类的成员变量中
        ThreadLocalMap map = getMap(t);
        if (map != null) {
            ThreadLocalMap.Entry e = map.getEntry(this);
            if (e != null) {
                @SuppressWarnings("unchecked")
                T result = (T) e.value;
                return result;
            }
        }
        // 初始化值/map
        return setInitialValue();
    }

    /**
     * Returns {@code true} if there is a value in the current thread's copy of
     * this thread-local variable, even if that values is {@code null}.
     * <p>
     * 如果当前线程的副本中存在此线程本地变量的值（即使该值为 null），则返回 true。
     *
     * @return {@code true} if current thread has associated value in this
     * thread-local variable; {@code false} if not
     */
    boolean isPresent() {
        // 确认当前线程
        Thread t = Thread.currentThread();
        // 获取当前线程的 ThreadLocalMap
        ThreadLocalMap map = getMap(t);
        // 如果能根据当前 ThreadLocal 对象作为 key 找到对应的 Entry，
        // 说明当前线程的 ThreadLocalMap 中存在此 ThreadLocal 对象（不管是否为 initial 值）
        return map != null && map.getEntry(this) != null;
    }

    /**
     * Variant of set() to establish initialValue. Used instead
     * of set() in case user has overridden the set() method.
     *
     * @return the initial value
     */
    private T setInitialValue() {
        // 取到 initial 值
        T value = initialValue();
        Thread t = Thread.currentThread();
        ThreadLocalMap map = getMap(t);
        if (map != null) {
            // map 已存在
            // 设置 map 中的值
            map.set(this, value);
        } else {
            // map 尚未创建
            // 创建 map && 将 this 存入到 map 中，因为 map 刚创建，所以肯定不存在哈希冲突
            createMap(t, value);
        }
        if (this instanceof TerminatingThreadLocal) {
            TerminatingThreadLocal.register((TerminatingThreadLocal<?>) this);
        }
        return value;
    }

    /**
     * Sets the current thread's copy of this thread-local variable
     * to the specified value.  Most subclasses will have no need to
     * override this method, relying solely on the {@link #initialValue}
     * method to set the values of thread-locals.
     *
     * @param value the value to be stored in the current thread's copy of
     *              this thread-local.
     */
    public void set(T value) {
        Thread t = Thread.currentThread();
        ThreadLocalMap map = getMap(t);
        if (map != null) {
            map.set(this, value);
        } else {
            createMap(t, value);
        }
    }

    /**
     * Removes the current thread's value for this thread-local
     * variable.  If this thread-local variable is subsequently
     * {@linkplain #get read} by the current thread, its value will be
     * reinitialized by invoking its {@link #initialValue} method,
     * unless its value is {@linkplain #set set} by the current thread
     * in the interim.  This may result in multiple invocations of the
     * {@code initialValue} method in the current thread.
     * <p>
     * 删除当前 thread 的 threadMap 中存储的此 threadLocal 的值。
     * 如果随后由当前线程读取此线程本地变量，则将通过调用其 initialValue 方法重新初始化其值，
     * 除非其值在此期间由当前线程设置。
     *
     * @since 1.5
     */
    public void remove() {
        ThreadLocalMap m = getMap(Thread.currentThread());
        if (m != null) {
            m.remove(this);
        }
    }

    /**
     * Get the map associated with a ThreadLocal. Overridden in
     * InheritableThreadLocal.
     *
     * @param t the current thread
     * @return the map
     */
    ThreadLocalMap getMap(Thread t) {
        return t.threadLocals;
    }

    /**
     * Create the map associated with a ThreadLocal. Overridden in
     * InheritableThreadLocal.
     *
     * @param t          the current thread
     * @param firstValue value for the initial entry of the map
     */
    void createMap(Thread t, T firstValue) {
        t.threadLocals = new ThreadLocalMap(this, firstValue);
    }

    /**
     * Factory method to create map of inherited thread locals.
     * Designed to be called only from Thread constructor.
     * <p>
     * 创建继承的 thread locals 的 map 的工厂方法。
     * 仅设计从 Thread 构造函数调用。
     *
     * @param parentMap the map associated with parent thread
     * @return a map containing the parent's inheritable bindings
     */
    static ThreadLocalMap createInheritedMap(ThreadLocalMap parentMap) {
        return new ThreadLocalMap(parentMap);
    }

    /**
     * Method childValue is visibly defined in subclass
     * InheritableThreadLocal, but is internally defined here for the
     * sake of providing createInheritedMap factory method without
     * needing to subclass the map class in InheritableThreadLocal.
     * This technique is preferable to the alternative of embedding
     * instanceof tests in methods.
     * <p>
     * childValue 方法在子类 InheritableThreadLocal 中可见定义，但在此处内部定义，
     * 以便提供 createInheritedMap 工厂方法，而无需在 InheritableThreadLocal 中子类化 map 类。
     * 这种技术优于在方法中嵌入 instanceof 测试的替代方法。
     */
    T childValue(T parentValue) {
        throw new UnsupportedOperationException();
    }

    /**
     * An extension of ThreadLocal that obtains its initial value from
     * the specified {@code Supplier}.
     */
    static final class SuppliedThreadLocal<T> extends ThreadLocal<T> {

        private final Supplier<? extends T> supplier;

        SuppliedThreadLocal(Supplier<? extends T> supplier) {
            this.supplier = Objects.requireNonNull(supplier);
        }

        @Override
        protected T initialValue() {
            return supplier.get();
        }
    }

    /**
     * ThreadLocalMap is a customized hash map suitable only for
     * maintaining thread local values. No operations are exported
     * outside of the ThreadLocal class. The class is package private to
     * allow declaration of fields in class Thread.  To help deal with
     * very large and long-lived usages, the hash table entries use
     * WeakReferences for keys. However, since reference queues are not
     * used, stale entries are guaranteed to be removed only when
     * the table starts running out of space.
     * <p>
     * ThreadLocalMap 是一个定制的哈希映射，仅适用于维护线程本地值。
     * 没有在 ThreadLocal 类之外导出任何操作。
     * 该类是包私有的，以允许在 Thread 类中声明字段。
     * 为了帮助处理非常大且长期存在的用法，哈希表条目使用 WeakReferences 作为键。
     */
    static class ThreadLocalMap {

        /**
         * The entries in this hash map extend WeakReference, using
         * its main ref field as the key (which is always a
         * ThreadLocal object).  Note that null keys (i.e. entry.get()
         * == null) mean that the key is no longer referenced, so the
         * entry can be expunged from table.  Such entries are referred to
         * as "stale entries" in the code that follows.
         * <p>
         * 此哈希映射中的条目扩展 WeakReference，使用其主引用字段作为键（始终为 ThreadLocal 对象）。
         * 请注意，null 键（即 entry.get() == null）意味着不再引用键，因此可以从表中删除条目。
         * 此类条目在随后的代码中称为“陈旧条目”。
         */
        static class Entry extends WeakReference<ThreadLocal<?>> {
            /**
             * The value associated with this ThreadLocal.
             */
            Object value;

            Entry(ThreadLocal<?> k, Object v) {
                super(k);
                value = v;
            }
        }

        /**
         * The initial capacity -- MUST be a power of two.
         */
        private static final int INITIAL_CAPACITY = 16;

        /**
         * The table, resized as necessary.
         * table.length MUST always be a power of two.
         */
        private Entry[] table;

        /**
         * The number of entries in the table.
         */
        private int size = 0;

        /**
         * The next size value at which to resize.
         */
        private int threshold; // Default to 0

        /**
         * Set the resize threshold to maintain at worst a 2/3 load factor.
         * <p>
         * 设置 resize 阈值，以保持最坏的 2/3 负载因子。
         */
        private void setThreshold(int len) {
            threshold = len * 2 / 3;
        }

        /**
         * Increment i modulo len.
         */
        private static int nextIndex(int i, int len) {
            return ((i + 1 < len) ? i + 1 : 0);
        }

        /**
         * Decrement i modulo len.
         */
        private static int prevIndex(int i, int len) {
            return ((i - 1 >= 0) ? i - 1 : len - 1);
        }

        /**
         * Construct a new map initially containing (firstKey, firstValue).
         * ThreadLocalMaps are constructed lazily, so we only create
         * one when we have at least one entry to put in it.
         */
        ThreadLocalMap(ThreadLocal<?> firstKey, Object firstValue) {
            table = new Entry[INITIAL_CAPACITY];
            int i = firstKey.threadLocalHashCode & (INITIAL_CAPACITY - 1);
            table[i] = new Entry(firstKey, firstValue);
            size = 1;
            setThreshold(INITIAL_CAPACITY);
        }

        /**
         * Construct a new map including all Inheritable ThreadLocals
         * from given parent map. Called only by createInheritedMap.
         *
         * @param parentMap the map associated with parent thread.
         */
        private ThreadLocalMap(ThreadLocalMap parentMap) {
            Entry[] parentTable = parentMap.table;
            int len = parentTable.length;
            setThreshold(len);
            table = new Entry[len];

            for (int j = 0; j < len; j++) {
                Entry e = parentTable[j];
                if (e != null) {
                    @SuppressWarnings("unchecked")
                    ThreadLocal<Object> key = (ThreadLocal<Object>) e.get();
                    if (key != null) {
                        Object value = key.childValue(e.value);
                        Entry c = new Entry(key, value);
                        int h = key.threadLocalHashCode & (len - 1);
                        while (table[h] != null)
                            h = nextIndex(h, len);
                        table[h] = c;
                        size++;
                    }
                }
            }
        }

        /**
         * Get the entry associated with key.  This method
         * itself handles only the fast path: a direct hit of existing
         * key. It otherwise relays to getEntryAfterMiss.  This is
         * designed to maximize performance for direct hits, in part
         * by making this method readily inlinable.
         * <p>
         * 获取与 key 关联的 entry。
         * 此方法本身仅处理快速路径：直接命中现有 key。
         * 否则，它会转发到 getEntryAfterMiss 方法来向后寻找 key。
         * 通过使此方法容易内联，这旨在最大限度地提高直接命中的性能。
         *
         * @param key the thread local object
         * @return the entry associated with key, or null if no such
         */
        private Entry getEntry(ThreadLocal<?> key) {
            // 确定 key 对应哪一个哈希桶
            int i = key.threadLocalHashCode & (table.length - 1);
            Entry e = table[i];
            // 如果桶中元素存在，且 key 相等，则直接返回
            if (e != null && e.get() == key)
                return e;
            else
                // 否则，需要向后遍历，直到找到 key 对应的元素或遇到空桶
                return getEntryAfterMiss(key, i, e);
        }

        /**
         * Version of getEntry method for use when key is not found in
         * its direct hash slot.
         * <p>
         * 当 key 在其直接哈希槽中找不到时使用的 getEntry 方法的版本。
         *
         * @param key the thread local object
         * @param i   the table index for key's hash code
         * @param e   the entry at table[i]
         * @return the entry associated with key, or null if no such
         */
        private Entry getEntryAfterMiss(ThreadLocal<?> key, int i, Entry e) {
            // avoid getfield，因为本身就是 ThreadLocal  的，因此不可能出现多线程的修改
            Entry[] tab = table;
            int len = tab.length;

            while (e != null) {
                ThreadLocal<?> k = e.get();
                if (k == key)
                    return e;
                // e != null, e.get() == null! 说明 weakReference 已经被回收了；此时会从 i 开始向后整理元素（只保证整理连续的一节）
                if (k == null)
                    expungeStaleEntry(i);
                else
                    i = nextIndex(i, len);
                e = tab[i];
            }
            return null;
        }

        /**
         * Set the value associated with key.
         *
         * @param key   the thread local object
         * @param value the value to be set
         */
        private void set(ThreadLocal<?> key, Object value) {

            // We don't use a fast path as with get() because it is at
            // least as common to use set() to create new entries as
            // it is to replace existing ones, in which case, a fast
            // path would fail more often than not.
            // 与 get() 一样，我们不使用 fast path，
            // 因为使用 set() 来 create 和 update 一样常见，
            // 在这种情况下，fast path 通常会失败。

            Entry[] tab = table;
            int len = tab.length;
            // 定位到初始哈希桶的位置
            int i = key.threadLocalHashCode & (len - 1);

            // 向后找到一个空桶
            for (Entry e = tab[i]; //  初始位置
                 e != null; // 桶中有元素，说明发生了哈希冲突
                 e = tab[i = nextIndex(i, len)]) { // i++

                // 定位桶中元素
                ThreadLocal<?> k = e.get();
                // key 相等，则 update
                if (k == key) {
                    e.value = value;
                    return;
                }

                // e != null, e.get() == null
                // 说明 e 是无效 entry，需要执行清理操作；清理的同时会赋值（因为清理的同时也在向后找）
                if (k == null) {
                    replaceStaleEntry(key, value, i);
                    return;
                }
            }

            // 存到一个空桶中
            tab[i] = new Entry(key, value);
            int sz = ++size;
            // 因为 i 存入了一个新的 Entry，所以 i 一定有效
            // 会从 i 开始向后清理，log2(sz) 次
            // 如果没有清理掉无效 Entry && 容量已经达到了 threshold，那么就会触发扩容
            if (!cleanSomeSlots(i, sz) && sz >= threshold)
                rehash();
        }

        /**
         * Remove the entry for key.
         */
        private void remove(ThreadLocal<?> key) {
            Entry[] tab = table;
            int len = tab.length;
            // 定位到初始的哈希桶
            int i = key.threadLocalHashCode & (len - 1);
            // 向后寻找，直到找到一个空桶
            for (Entry e = tab[i];
                 e != null;
                 e = tab[i = nextIndex(i, len)]) {
                // 清空 entry
                if (e.get() == key) {
                    e.clear(); // 将导致 e.get() == null
                    expungeStaleEntry(i); // i 此时一定是无效 Entry，从 i 开始发起一截连续的清理
                    return;
                }
            }
        }

        /**
         * Replace a stale entry encountered during a set operation
         * with an entry for the specified key.  The value passed in
         * the value parameter is stored in the entry, whether or not
         * an entry already exists for the specified key.
         *
         * <p>
         * As a side effect, this method expunges all stale entries in the
         * "run" containing the stale entry.  (A run is a sequence of entries
         * between two null slots.)
         *
         * @param key       the key
         * @param value     the value to be associated with key
         * @param staleSlot index of the first stale entry encountered while
         *                  searching for key.
         */
        private void replaceStaleEntry(ThreadLocal<?> key, Object value,
                                       int staleSlot) {
            Entry[] tab = table;
            int len = tab.length;
            Entry e;

            // Back up to check for prior stale entry in current run.
            // We clean out whole runs at a time to avoid continual
            // incremental rehashing due to garbage collector freeing
            // up refs in bunches (i.e., whenever the collector runs).

            // 记录开始清理的位置（因为会向前找，所以 slotToExpunge <= staleSlot）
            int slotToExpunge = staleSlot;
            for (int i = prevIndex(staleSlot, len); // 从 staleSlot 向前找
                 (e = tab[i]) != null;
                 i = prevIndex(i, len))
                // 找到最开始的无效 entry
                if (e.get() == null)
                    slotToExpunge = i;

            // Find either the key or trailing null slot of run, whichever
            // occurs first
            for (int i = nextIndex(staleSlot, len); // 从 staleSlot 向后找
                 (e = tab[i]) != null;
                 i = nextIndex(i, len)) {
                ThreadLocal<?> k = e.get();

                // If we find key, then we need to swap it
                // with the stale entry to maintain hash table order.
                // The newly stale slot, or any other stale slot
                // encountered above it, can then be sent to expungeStaleEntry
                // to remove or rehash all of the other entries in run.

                // 找到了相同 key 的 Entry（此时 Entry 肯定是有效的）
                if (k == key) {
                    // 更新 Entry 的值
                    e.value = value;

                    // 将 slot 和 i 进行交换，即将 i 换到第一个无效 entry 的位置（staleSlot 一定是无效 Entry）
                    tab[i] = tab[staleSlot];
                    tab[staleSlot] = e;

                    // Start expunge at preceding stale entry if it exists
                    // 如果 slotToExpunge == staleSlot，说明 staleSlot 之前没有无效的 entry，而此时位置 i 因为发生了交换，因此后续就从 i 开始清理
                    if (slotToExpunge == staleSlot)
                        slotToExpunge = i;
                    // 从 i 开始清理连续的一截，expungeStaleEntry 返回下一个空哈希桶的位置
                    // 然后会再从「下一个空哈希桶的位置」开始向后做清理，一共清理 log2(len) 次
                    cleanSomeSlots(expungeStaleEntry(slotToExpunge), len);
                    return;
                }

                // If we didn't find stale entry on backward scan, the
                // first stale entry seen while scanning for key is the
                // first still present in the run.

                // 如果执行到了这里，说明在 k 之前没找到可 update 的 Entry（不保证一定没有，只是 k 之前没有）
                // 如果 k == null，说明 e 是无效的 Entry && staleSlot 之前没有无效 Entry && k 之前没有相同 key 的 Entry
                if (k == null && slotToExpunge == staleSlot)
                    // 更新 slotToExpunge，但是如果有多个无效的 i，是不是会更新多次啊？会不会有啥影响？-> 应该不会，因为 len 固定，所以清理的次数固定
                    slotToExpunge = i;
            }

            // If key not found, put new entry in stale slot
            // 如果一圈下来没找到相同 key 的 Entry，说明需要新增一个 Entry（无法 update，只能 insert）
            tab[staleSlot].value = null;
            tab[staleSlot] = new Entry(key, value);

            // If there are any other stale entries in run, expunge them
            // 因为此时 staleSlot 已经被覆盖了，所以 staleSlot 已经不是无效 Entry 了；
            // 此时如果 slotToExpunge == staleSlot，staleSlot 前后都没有无效 Entry（null 间隔区间内），那么就不需要 clean
            if (slotToExpunge != staleSlot)
                cleanSomeSlots(expungeStaleEntry(slotToExpunge), len);
        }

        /**
         * Expunge a stale entry by rehashing any possibly colliding entries
         * lying between staleSlot and the next null slot.  This also expunges
         * any other stale entries encountered before the trailing null.  See
         * Knuth, Section 6.4
         * <p>
         * 通过重新哈希可能位于 staleSlot 和下一个空槽之间的任何可能冲突的条目来清除陈旧的条目。
         * 这也会清除尾随 null 之前遇到的任何其他陈旧条目。
         *
         * @param staleSlot index of slot known to have null key
         * @return the index of the next null slot after staleSlot
         * (all between staleSlot and this slot will have been checked
         * for expunging).
         */
        private int expungeStaleEntry(int staleSlot) {
            Entry[] tab = table;
            int len = tab.length;

            // expunge entry at staleSlot
            // 既然调用了这个方法，并且传递了 staleSlot，那么说明这个 staleSlot 对应的元素已经被回收了
            tab[staleSlot].value = null;
            tab[staleSlot] = null;
            size--;

            // Rehash until we encounter null
            Entry e;
            int i;
            for (i = nextIndex(staleSlot, len); // 初始条件：i = staleSlot + 1
                 (e = tab[i]) != null; // 结束条件：e == null（e = tab[i]）-> 仅仅只是清理连续的一截而已
                 i = nextIndex(i, len)) { // 迭代条件：i = i + 1

                // 获取 e 对应的 ThreadLocal
                ThreadLocal<?> k = e.get();
                // e != null, e.get() == null
                // 说明 e 对应的 ThreadLocal 已经被回收了，需要清理
                if (k == null) {
                    e.value = null;
                    tab[i] = null;
                    size--;
                } else {
                    // 说明 e 对应的 ThreadLocal 还没有被回收，需要重新计算哈希值，然后放到合适的位置（之前被放到当前位置可能是发生了哈希冲突）
                    // 重新定位哈希桶的位置
                    int h = k.threadLocalHashCode & (len - 1);
                    // h != j，说明之前放置的时候确实发生过哈希冲突
                    if (h != i) {
                        // 将原位置的元素置为 null
                        tab[i] = null;

                        // Unlike Knuth 6.4 Algorithm R, we must scan until
                        // null because multiple entries could have been stale.
                        // 找到一个新的位置，将元素放置到新的位置(新的放置可能也会发生哈希冲突，因此需要 while 向后找)
                        while (tab[h] != null)
                            h = nextIndex(h, len);
                        tab[h] = e;
                    }
                }
            }
            // 返回 i，说明当前方法能保障 staleSlot 到 i 之间的数据是有效的
            return i;
        }

        /**
         * Heuristically scan some cells looking for stale entries.
         * This is invoked when either a new element is added, or
         * another stale one has been expunged. It performs a
         * logarithmic number of scans, as a balance between no
         * scanning (fast but retains garbage) and a number of scans
         * proportional to number of elements, that would find all
         * garbage but would cause some insertions to take O(n) time.
         * <p>
         * 启发式地扫描一些单元格以查找陈旧的条目。
         * 当添加新元素或另一个陈旧元素被清除时，将调用此方法。
         * 它执行对数数量的扫描，以在不扫描（快速但保留垃圾）和扫描数量之间取得平衡，
         * 与元素数量成比例，这将找到所有垃圾，但会导致某些插入花费 O(n) 时间。
         *
         * @param i a position known NOT to hold a stale entry. The
         *          scan starts at the element after i.
         * @param n scan control: {@code log2(n)} cells are scanned,
         *          unless a stale entry is found, in which case
         *          {@code log2(table.length)-1} additional cells are scanned.
         *          When called from insertions, this parameter is the number
         *          of elements, but when from replaceStaleEntry, it is the
         *          table length. (Note: all this could be changed to be either
         *          more or less aggressive by weighting n instead of just
         *          using straight log n. But this version is simple, fast, and
         *          seems to work well.)
         * @return true if any stale entries have been removed.
         */
        private boolean cleanSomeSlots(int i, int n) {
            // flag 用于标记是否清理了无效的 entry
            boolean removed = false;
            // avoid getfield Opcode
            Entry[] tab = table;
            int len = tab.length;
            do {
                // 初始时，i 一定不是无效的 entry，可能是 null 或者普通的值；因此可以直接 nextIndex
                i = nextIndex(i, len);
                Entry e = tab[i];
                // 如果 e 是一个无效的 Entry
                if (e != null && e.get() == null) {
                    n = len;
                    removed = true;
                    // 因为 e 是无效的 Entry，所以 i 一定也是，因此会从 i 开始向后执行一截清理操作
                    i = expungeStaleEntry(i);
                }
            } while ((n >>>= 1) != 0); // 除以 2，直到 n == 0，n 控制 scan 的次数，一般 n == len；所以 cleanSomeSlots 方法并不能保障一定清理干净（出于性能考虑）
            return removed;
        }

        /**
         * Re-pack and/or re-size the table. First scan the entire
         * table removing stale entries. If this doesn't sufficiently
         * shrink the size of the table, double the table size.
         */
        private void rehash() {
            // 全量的清理一次
            expungeStaleEntries();

            // Use lower threshold for doubling to avoid hysteresis
            // 如果容量达到了四分之三的 threshold，那么就扩容
            if (size >= threshold - threshold / 4)
                resize();
        }

        /**
         * Double the capacity of the table.
         */
        private void resize() {
            Entry[] oldTab = table;
            int oldLen = oldTab.length;
            // 扩容成原来的两倍
            int newLen = oldLen * 2;
            Entry[] newTab = new Entry[newLen];
            int count = 0;

            // 将旧 map 中的元素重新添加到新的 map 中
            for (int j = 0; j < oldLen; ++j) {
                Entry e = oldTab[j];
                if (e != null) {
                    ThreadLocal<?> k = e.get();
                    if (k == null) {
                        e.value = null; // Help the GC
                    } else {
                        int h = k.threadLocalHashCode & (newLen - 1);
                        while (newTab[h] != null)
                            h = nextIndex(h, newLen);
                        newTab[h] = e;
                        count++;
                    }
                }
            }

            // 重新计算 threshold
            setThreshold(newLen);

            size = count;
            table = newTab;
        }

        /**
         * Expunge all stale entries in the table.
         */
        private void expungeStaleEntries() {
            Entry[] tab = table;
            int len = tab.length;
            for (int j = 0; j < len; j++) {
                Entry e = tab[j];
                // 如果 j 是无效 Entry，那么就从 j 开始向后清理一截；
                if (e != null && e.get() == null)
                    expungeStaleEntry(j);
            }
        }
    }
}
