/*
 * Copyright (c) 1994, 2016, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.security.AccessController;
import java.security.AccessControlContext;
import java.security.PrivilegedAction;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.LockSupport;

import jdk.internal.misc.TerminatingThreadLocal;
import sun.nio.ch.Interruptible;
import sun.reflect.CallerSensitive;
import sun.reflect.Reflection;
import sun.security.util.SecurityConstants;


/**
 * A <i>thread</i> is a thread of execution in a program. The Java
 * Virtual Machine allows an application to have multiple threads of
 * execution running concurrently.
 * <p>
 * 一个 thread 是程序中的执行线程。
 * Java 虚拟机允许应用程序同时运行多个执行线程。
 *
 * <p>
 * Every thread has a priority. Threads with higher priority are
 * executed in preference to threads with lower priority. Each thread
 * may or may not also be marked as a daemon. When code running in
 * some thread creates a new <code>Thread</code> object, the new
 * thread has its priority initially set equal to the priority of the
 * creating thread, and is a daemon thread if and only if the
 * creating thread is a daemon.
 * <p>
 * 每个 thread 都有一个优先级。
 * 优先级高的 thread 会优先执行。
 * thread  可以是一个 daemon thread。
 * 当 main thread 中的代码创建一个新的 Thread 对象时，新的 thread 的优先级初始值等于 main thread 的优先级。
 * 当且仅当 main thread 是一个 daemon thread 时，新的 thread 才是一个 daemon thread。
 *
 * <p>
 * When a Java Virtual Machine starts up, there is usually a single
 * non-daemon thread (which typically calls the method named
 * <code>main</code> of some designated class). The Java Virtual
 * Machine continues to execute threads until either of the following
 * occurs:
 * <ul>
 * <li>The <code>exit</code> method of class <code>Runtime</code> has been
 *     called and the security manager has permitted the exit operation
 *     to take place.
 * <li>All threads that are not daemon threads have died, either by
 *     returning from the call to the <code>run</code> method or by
 *     throwing an exception that propagates beyond the <code>run</code>
 *     method.
 * </ul>
 * <p>
 *     当 Java 虚拟机启动时，通常有一个非 daemon thread（通常是调用某个指定类的 main 方法的 thread）。
 *     Java 虚拟机会继续执行 thread，直到发生以下情况之一：
 *     1. Runtime 类的 exit 方法被调用，并且 security manager 允许 exit 操作。
 *     2. 所有非 daemon thread 都已经死亡，要么是从 run 方法返回，要么是抛出一个超出 run 方法的异常。
 *
 * <p>
 * There are two ways to create a new thread of execution. One is to
 * declare a class to be a subclass of <code>Thread</code>. This
 * subclass should override the <code>run</code> method of class
 * <code>Thread</code>. An instance of the subclass can then be
 * allocated and started. For example, a thread that computes primes
 * larger than a stated value could be written as follows:
 * <hr><blockquote><pre>
 *     class PrimeThread extends Thread {
 *         long minPrime;
 *         PrimeThread(long minPrime) {
 *             this.minPrime = minPrime;
 *         }
 *
 *         public void run() {
 *             // compute primes larger than minPrime
 *             &nbsp;.&nbsp;.&nbsp;.
 *         }
 *     }
 * </pre></blockquote><hr>
 * <p>
 * The following code would then create a thread and start it running:
 * <blockquote><pre>
 *     PrimeThread p = new PrimeThread(143);
 *     p.start();
 * </pre></blockquote>
 * <p>
 * The other way to create a thread is to declare a class that
 * implements the <code>Runnable</code> interface. That class then
 * implements the <code>run</code> method. An instance of the class can
 * then be allocated, passed as an argument when creating
 * <code>Thread</code>, and started. The same example in this other
 * style looks like the following:
 * <hr><blockquote><pre>
 *     class PrimeRun implements Runnable {
 *         long minPrime;
 *         PrimeRun(long minPrime) {
 *             this.minPrime = minPrime;
 *         }
 *
 *         public void run() {
 *             // compute primes larger than minPrime
 *             &nbsp;.&nbsp;.&nbsp;.
 *         }
 *     }
 * </pre></blockquote><hr>
 * <p>
 * The following code would then create a thread and start it running:
 * <blockquote><pre>
 *     PrimeRun p = new PrimeRun(143);
 *     new Thread(p).start();
 * </pre></blockquote>
 * <p>
 * Every thread has a name for identification purposes. More than
 * one thread may have the same name. If a name is not specified when
 * a thread is created, a new name is generated for it.
 * <p>
 * Unless otherwise noted, passing a {@code null} argument to a constructor
 * or method in this class will cause a {@link NullPointerException} to be
 * thrown.
 *
 * @author unascribed
 * @see Runnable
 * @see Runtime#exit(int)
 * @see #run()
 * @see #stop()
 * @since JDK1.0
 */
public
class Thread implements Runnable {
    /* Make sure registerNatives is the first thing <clinit> does. */

    // TODO 【REMIND】Thread 类有一个很大的特点，就是有时候 this != currentThread！
    private static native void registerNatives();

    static {
        // 静态代码块保证，registerNatives 方法在类初始化时就被执行
        // Object 类中也有
        registerNatives();
    }

    private volatile String name;
    private int priority;
    private Thread threadQ;
    private long eetop;

    /* Whether or not to single_step this thread. */
    private boolean single_step;

    /* Whether or not the thread is a daemon thread. */
    private boolean daemon = false;

    /* JVM state */
    private boolean stillborn = false;

    /* What will be run. */
    private Runnable target;

    /* The group of this thread */
    private ThreadGroup group;

    /* The context ClassLoader for this thread */
    private ClassLoader contextClassLoader;

    /* The inherited AccessControlContext of this thread */
    private AccessControlContext inheritedAccessControlContext;

    /* For autonumbering anonymous threads. */
    private static int threadInitNumber;

    private static synchronized int nextThreadNum() {
        return threadInitNumber++;
    }

    /**
     * 一个线程有两种 ThreadLocal：
     * 1. ThreadLocal：线程自己的
     * 2. InheritableThreadLocal：可以被继承的
     */

    /* ThreadLocal values pertaining to this thread. This map is maintained
     * by the ThreadLocal class.
     *
     * 与当前 thread 相关的 ThreadLocal 值。
     * 这个 map 由 ThreadLocal 类维护。
     */
    ThreadLocal.ThreadLocalMap threadLocals = null;

    /*
     * InheritableThreadLocal values pertaining to this thread. This map is
     * maintained by the InheritableThreadLocal class.
     *
     * 与当前 thread 相关的 InheritableThreadLocal 值。
     * 这个 map 由 InheritableThreadLocal 类维护。
     */
    ThreadLocal.ThreadLocalMap inheritableThreadLocals = null;

    /*
     * The requested stack size for this thread, or 0 if the creator did
     * not specify a stack size.  It is up to the VM to do whatever it
     * likes with this number; some VMs will ignore it.
     *
     * 当前 thread 的 stack size。
     * 如果创建 thread 时没有指定 stack size，那么 stackSize 的值为 0。
     * VM 可以自由决定如何使用这个值，有些 VM 会忽略它。
     */
    private long stackSize;

    /*
     * JVM-private state that persists after native thread termination.
     *
     * JVM 私有状态，native thread 终止后仍然存在。
     */
    private long nativeParkEventPointer;

    /*
     * Thread ID
     */
    private long tid;

    /* For generating thread ID */
    private static long threadSeqNumber;

    /* Java thread status for tools,
     * initialized to indicate thread 'not yet started'
     */

    private volatile int threadStatus = 0;


    private static synchronized long nextThreadID() {
        return ++threadSeqNumber;
    }

    /**
     * The argument supplied to the current call to
     * java.util.concurrent.locks.LockSupport.park.
     * Set by (private) java.util.concurrent.locks.LockSupport.setBlocker
     * Accessed using java.util.concurrent.locks.LockSupport.getBlocker
     * <p>
     * 当前调用 java.util.concurrent.locks.LockSupport.park 的参数。
     * 由 (private) java.util.concurrent.locks.LockSupport.setBlocker 设置。
     * <p>
     * LockSupport 通过 UNSAFE 类访问当前变量
     */
    volatile Object parkBlocker;

    /* The object in which this thread is blocked in an interruptible I/O
     * operation, if any.  The blocker's interrupt method should be invoked
     * after setting this thread's interrupt status.
     *
     * 当前 thread 在可中断 I/O 操作中被阻塞的对象，如果有的话。
     * 应该在设置当前 thread 的 interrupt 状态后调用 blocker 的 interrupt 方法。
     */
    private volatile Interruptible blocker;
    private final Object blockerLock = new Object();

    /* Set the blocker field; invoked via sun.misc.SharedSecrets from java.nio code
     *
     * 设置 blocker 字段；由 sun.misc.SharedSecrets 从 java.nio 代码中调用
     */
    void blockedOn(Interruptible b) {
        synchronized (blockerLock) {
            blocker = b;
        }
    }

    /**
     * The minimum priority that a thread can have.
     */
    public final static int MIN_PRIORITY = 1;

    /**
     * The default priority that is assigned to a thread.
     */
    public final static int NORM_PRIORITY = 5;

    /**
     * The maximum priority that a thread can have.
     */
    public final static int MAX_PRIORITY = 10;

    /**
     * Returns a reference to the currently executing thread object.
     * <p>
     * 返回当前正在执行的 thread 对象的引用。
     *
     * @return the currently executing thread.
     */
    public static native Thread currentThread();

    /**
     * A hint to the scheduler that the current thread is willing to yield
     * its current use of a processor. The scheduler is free to ignore this
     * hint.
     * <p>
     * 提示 scheduler 当前 thread 愿意放弃当前对 processor 的使用。
     * scheduler 可以忽略这个提示。
     *
     * <p> Yield is a heuristic attempt to improve relative progression
     * between threads that would otherwise over-utilise a CPU. Its use
     * should be combined with detailed profiling and benchmarking to
     * ensure that it actually has the desired effect.
     * <p>
     * Yield 是一种启发式尝试，旨在改善 thread 之间的相对进度，否则 thread 会过度使用 CPU。
     * 应该将其与详细的 profiling 和 benchmarking 结合使用，以确保它实际上具有预期的效果。
     *
     * <p> It is rarely appropriate to use this method. It may be useful
     * for debugging or testing purposes, where it may help to reproduce
     * bugs due to race conditions. It may also be useful when designing
     * concurrency control constructs such as the ones in the
     * {@link java.util.concurrent.locks} package.
     * <p>
     * 很少有合适的场景使用这个方法。
     * 在 debug 或 test 时可能会有用，因为 yield 方法能帮助找到一些罕见的 bug。
     * 在设计 concurrency control constructs 时也可能有用，比如 java.util.concurrent.locks 包中的 constructs。
     */
    public static native void yield();

    /**
     * Causes the currently executing thread to sleep (temporarily cease
     * execution) for the specified number of milliseconds, subject to
     * the precision and accuracy of system timers and schedulers. The thread
     * does not lose ownership of any monitors.
     * <p>
     * 使当前 thread 休眠（暂时停止执行）指定的毫秒数，取决于系统计时器和调度器的精度和准确性。
     * thread 不会失去任何 monitor 的所有权。 即不会释放任何持有的锁。
     *
     * @param millis the length of time to sleep in milliseconds
     * @throws IllegalArgumentException if the value of {@code millis} is negative
     * @throws InterruptedException     if any thread has interrupted the current thread. The
     *                                  <i>interrupted status</i> of the current thread is
     *                                  cleared when this exception is thrown.
     */
    public static native void sleep(long millis) throws InterruptedException;

    /**
     * Causes the currently executing thread to sleep (temporarily cease
     * execution) for the specified number of milliseconds plus the specified
     * number of nanoseconds, subject to the precision and accuracy of system
     * timers and schedulers. The thread does not lose ownership of any
     * monitors.
     *
     * @param millis the length of time to sleep in milliseconds
     * @param nanos  {@code 0-999999} additional nanoseconds to sleep
     * @throws IllegalArgumentException if the value of {@code millis} is negative, or the value of
     *                                  {@code nanos} is not in the range {@code 0-999999}
     * @throws InterruptedException     if any thread has interrupted the current thread. The
     *                                  <i>interrupted status</i> of the current thread is
     *                                  cleared when this exception is thrown.
     */
    public static void sleep(long millis, int nanos)
            throws InterruptedException {
        if (millis < 0) {
            throw new IllegalArgumentException("timeout value is negative");
        }

        if (nanos < 0 || nanos > 999999) {
            throw new IllegalArgumentException(
                    "nanosecond timeout value out of range");
        }

        if (nanos >= 500000 || (nanos != 0 && millis == 0)) {
            millis++;
        }

        sleep(millis);
    }

    /**
     * Initializes a Thread with the current AccessControlContext.
     *
     * @see #init(ThreadGroup, Runnable, String, long, AccessControlContext, boolean)
     */
    private void init(ThreadGroup g, Runnable target, String name,
                      long stackSize) {
        init(g, target, name, stackSize, null, true);
    }

    /**
     * Initializes a Thread.
     * <p>
     * 初始化一个 thread。
     *
     * @param g                   the Thread group
     * @param target              the object whose run() method gets called
     * @param name                the name of the new Thread
     * @param stackSize           the desired stack size for the new thread, or
     *                            zero to indicate that this parameter is to be ignored.
     * @param acc                 the AccessControlContext to inherit, or
     *                            AccessController.getContext() if null
     * @param inheritThreadLocals if {@code true}, inherit initial values for
     *                            inheritable thread-locals from the constructing thread
     */
    private void init(ThreadGroup g, Runnable target, String name,
                      long stackSize, AccessControlContext acc,
                      boolean inheritThreadLocals) {
        // init name
        if (name == null) {
            throw new NullPointerException("name cannot be null");
        }

        this.name = name;

        Thread parent = currentThread();
        SecurityManager security = System.getSecurityManager();
        // 如果没有指定 thread group，那么先取自 security manager 的 thread group，如果没有，那么取 parent 的 thread group
        // 默认的 security manager 的 thread group 其实就是 parent 的 thread group
        if (g == null) {
            /* Determine if it's an applet or not */

            /* If there is a security manager, ask the security manager
               what to do. */
            if (security != null) {
                g = security.getThreadGroup();
            }

            /* If the security doesn't have a strong opinion of the matter
               use the parent thread group. */
            if (g == null) {
                g = parent.getThreadGroup();
            }
        }

        /* checkAccess regardless of whether or not threadgroup is
           explicitly passed in. */
        // 其实只会在 g == root group 时才会真的 checkAccess（默认实现）
        g.checkAccess();

        /*
         * Do we have the required permissions?
         */
        if (security != null) {
            if (isCCLOverridden(getClass())) {
                security.checkPermission(SUBCLASS_IMPLEMENTATION_PERMISSION);
            }
        }

        g.addUnstarted();

        this.group = g;
        this.daemon = parent.isDaemon();
        this.priority = parent.getPriority();
        if (security == null || isCCLOverridden(parent.getClass()))
            this.contextClassLoader = parent.getContextClassLoader();
        else
            this.contextClassLoader = parent.contextClassLoader;
        this.inheritedAccessControlContext =
                acc != null ? acc : AccessController.getContext();
        this.target = target;
        setPriority(priority);
        // 如果允许继承 thread locals，那么就继承 parent 的 inheritableThreadLocals
        if (inheritThreadLocals && parent.inheritableThreadLocals != null)
            this.inheritableThreadLocals =
                    ThreadLocal.createInheritedMap(parent.inheritableThreadLocals);
        /* Stash the specified stack size in case the VM cares */
        this.stackSize = stackSize;

        /* Set thread ID */
        tid = nextThreadID();
    }

    /**
     * Throws CloneNotSupportedException as a Thread can not be meaningfully
     * cloned. Construct a new Thread instead.
     * <p>
     * 抛出 CloneNotSupportedException，因为 thread 不能被有意义地克隆。
     *
     * @throws CloneNotSupportedException always
     */
    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Allocates a new {@code Thread} object. This constructor has the same
     * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
     * {@code (null, null, gname)}, where {@code gname} is a newly generated
     * name. Automatically generated names are of the form
     * {@code "Thread-"+}<i>n</i>, where <i>n</i> is an integer.
     */
    public Thread() {
        init(null, null, "Thread-" + nextThreadNum(), 0);
    }

    /**
     * Allocates a new {@code Thread} object. This constructor has the same
     * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
     * {@code (null, target, gname)}, where {@code gname} is a newly generated
     * name. Automatically generated names are of the form
     * {@code "Thread-"+}<i>n</i>, where <i>n</i> is an integer.
     *
     * @param target the object whose {@code run} method is invoked when this thread
     *               is started. If {@code null}, this classes {@code run} method does
     *               nothing.
     */
    public Thread(Runnable target) {
        init(null, target, "Thread-" + nextThreadNum(), 0);
    }

    /**
     * Creates a new Thread that inherits the given AccessControlContext.
     * This is not a public constructor.
     */
    Thread(Runnable target, AccessControlContext acc) {
        init(null, target, "Thread-" + nextThreadNum(), 0, acc, false);
    }

    /**
     * Allocates a new {@code Thread} object. This constructor has the same
     * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
     * {@code (group, target, gname)} ,where {@code gname} is a newly generated
     * name. Automatically generated names are of the form
     * {@code "Thread-"+}<i>n</i>, where <i>n</i> is an integer.
     *
     * @param group  the thread group. If {@code null} and there is a security
     *               manager, the group is determined by {@linkplain
     *               SecurityManager#getThreadGroup SecurityManager.getThreadGroup()}.
     *               If there is not a security manager or {@code
     *               SecurityManager.getThreadGroup()} returns {@code null}, the group
     *               is set to the current thread's thread group.
     * @param target the object whose {@code run} method is invoked when this thread
     *               is started. If {@code null}, this thread's run method is invoked.
     * @throws SecurityException if the current thread cannot create a thread in the specified
     *                           thread group
     */
    public Thread(ThreadGroup group, Runnable target) {
        init(group, target, "Thread-" + nextThreadNum(), 0);
    }

    /**
     * Allocates a new {@code Thread} object. This constructor has the same
     * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
     * {@code (null, null, name)}.
     *
     * @param name the name of the new thread
     */
    public Thread(String name) {
        init(null, null, name, 0);
    }

    /**
     * Allocates a new {@code Thread} object. This constructor has the same
     * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
     * {@code (group, null, name)}.
     *
     * @param group the thread group. If {@code null} and there is a security
     *              manager, the group is determined by {@linkplain
     *              SecurityManager#getThreadGroup SecurityManager.getThreadGroup()}.
     *              If there is not a security manager or {@code
     *              SecurityManager.getThreadGroup()} returns {@code null}, the group
     *              is set to the current thread's thread group.
     * @param name  the name of the new thread
     * @throws SecurityException if the current thread cannot create a thread in the specified
     *                           thread group
     */
    public Thread(ThreadGroup group, String name) {
        init(group, null, name, 0);
    }

    /**
     * Allocates a new {@code Thread} object. This constructor has the same
     * effect as {@linkplain #Thread(ThreadGroup, Runnable, String) Thread}
     * {@code (null, target, name)}.
     *
     * @param target the object whose {@code run} method is invoked when this thread
     *               is started. If {@code null}, this thread's run method is invoked.
     * @param name   the name of the new thread
     */
    public Thread(Runnable target, String name) {
        init(null, target, name, 0);
    }

    /**
     * Allocates a new {@code Thread} object so that it has {@code target}
     * as its run object, has the specified {@code name} as its name,
     * and belongs to the thread group referred to by {@code group}.
     *
     * <p>If there is a security manager, its
     * {@link SecurityManager#checkAccess(ThreadGroup) checkAccess}
     * method is invoked with the ThreadGroup as its argument.
     *
     * <p>In addition, its {@code checkPermission} method is invoked with
     * the {@code RuntimePermission("enableContextClassLoaderOverride")}
     * permission when invoked directly or indirectly by the constructor
     * of a subclass which overrides the {@code getContextClassLoader}
     * or {@code setContextClassLoader} methods.
     *
     * <p>The priority of the newly created thread is set equal to the
     * priority of the thread creating it, that is, the currently running
     * thread. The method {@linkplain #setPriority setPriority} may be
     * used to change the priority to a new value.
     *
     * <p>The newly created thread is initially marked as being a daemon
     * thread if and only if the thread creating it is currently marked
     * as a daemon thread. The method {@linkplain #setDaemon setDaemon}
     * may be used to change whether or not a thread is a daemon.
     *
     * @param group  the thread group. If {@code null} and there is a security
     *               manager, the group is determined by {@linkplain
     *               SecurityManager#getThreadGroup SecurityManager.getThreadGroup()}.
     *               If there is not a security manager or {@code
     *               SecurityManager.getThreadGroup()} returns {@code null}, the group
     *               is set to the current thread's thread group.
     * @param target the object whose {@code run} method is invoked when this thread
     *               is started. If {@code null}, this thread's run method is invoked.
     * @param name   the name of the new thread
     * @throws SecurityException if the current thread cannot create a thread in the specified
     *                           thread group or cannot override the context class loader methods.
     */
    public Thread(ThreadGroup group, Runnable target, String name) {
        init(group, target, name, 0);
    }

    /**
     * Allocates a new {@code Thread} object so that it has {@code target}
     * as its run object, has the specified {@code name} as its name,
     * and belongs to the thread group referred to by {@code group}, and has
     * the specified <i>stack size</i>.
     *
     * <p>This constructor is identical to {@link
     * #Thread(ThreadGroup, Runnable, String)} with the exception of the fact
     * that it allows the thread stack size to be specified.  The stack size
     * is the approximate number of bytes of address space that the virtual
     * machine is to allocate for this thread's stack.  <b>The effect of the
     * {@code stackSize} parameter, if any, is highly platform dependent.</b>
     *
     * <p>On some platforms, specifying a higher value for the
     * {@code stackSize} parameter may allow a thread to achieve greater
     * recursion depth before throwing a {@link StackOverflowError}.
     * Similarly, specifying a lower value may allow a greater number of
     * threads to exist concurrently without throwing an {@link
     * OutOfMemoryError} (or other internal error).  The details of
     * the relationship between the value of the <tt>stackSize</tt> parameter
     * and the maximum recursion depth and concurrency level are
     * platform-dependent.  <b>On some platforms, the value of the
     * {@code stackSize} parameter may have no effect whatsoever.</b>
     *
     * <p>The virtual machine is free to treat the {@code stackSize}
     * parameter as a suggestion.  If the specified value is unreasonably low
     * for the platform, the virtual machine may instead use some
     * platform-specific minimum value; if the specified value is unreasonably
     * high, the virtual machine may instead use some platform-specific
     * maximum.  Likewise, the virtual machine is free to round the specified
     * value up or down as it sees fit (or to ignore it completely).
     *
     * <p>Specifying a value of zero for the {@code stackSize} parameter will
     * cause this constructor to behave exactly like the
     * {@code Thread(ThreadGroup, Runnable, String)} constructor.
     *
     * <p><i>Due to the platform-dependent nature of the behavior of this
     * constructor, extreme care should be exercised in its use.
     * The thread stack size necessary to perform a given computation will
     * likely vary from one JRE implementation to another.  In light of this
     * variation, careful tuning of the stack size parameter may be required,
     * and the tuning may need to be repeated for each JRE implementation on
     * which an application is to run.</i>
     *
     * <p>Implementation note: Java platform implementers are encouraged to
     * document their implementation's behavior with respect to the
     * {@code stackSize} parameter.
     *
     * @param group     the thread group. If {@code null} and there is a security
     *                  manager, the group is determined by {@linkplain
     *                  SecurityManager#getThreadGroup SecurityManager.getThreadGroup()}.
     *                  If there is not a security manager or {@code
     *                  SecurityManager.getThreadGroup()} returns {@code null}, the group
     *                  is set to the current thread's thread group.
     * @param target    the object whose {@code run} method is invoked when this thread
     *                  is started. If {@code null}, this thread's run method is invoked.
     * @param name      the name of the new thread
     * @param stackSize the desired stack size for the new thread, or zero to indicate
     *                  that this parameter is to be ignored.
     * @throws SecurityException if the current thread cannot create a thread in the specified
     *                           thread group
     * @since 1.4
     */
    public Thread(ThreadGroup group, Runnable target, String name,
                  long stackSize) {
        init(group, target, name, stackSize);
    }

    /**
     * Causes this thread to begin execution; the Java Virtual Machine
     * calls the <code>run</code> method of this thread.
     * <p>
     * 使当前 thread 开始执行；Java 虚拟机调用当前 thread 的 run 方法。
     *
     * <p>
     * The result is that two threads are running concurrently: the
     * current thread (which returns from the call to the
     * <code>start</code> method) and the other thread (which executes its
     * <code>run</code> method).
     * <p>
     * 结果是两个 thread 并发运行：当前 thread（从调用 start 方法返回）和另一个 thread（执行它的 run 方法）。
     *
     * <p>
     * It is never legal to start a thread more than once.
     * In particular, a thread may not be restarted once it has completed
     * execution.
     * <p>
     * 一个 thread 只能被 start 一次。
     * 特别地，一个 thread 在执行完成后不能被重新 start。
     *
     * @throws IllegalThreadStateException if the thread was already
     *                                     started.
     * @see #run()
     * @see #stop()
     */
    public synchronized void start() {
        /**
         * This method is not invoked for the main method thread or "system"
         * group threads created/set up by the VM. Any new functionality added
         * to this method in the future may have to also be added to the VM.
         * <p>
         *     这个方法不会被 main 方法 thread 或者 VM 创建/设置的 "system" group thread 调用。
         *     未来添加到这个方法的任何新功能也可能需要添加到 VM 中。
         *
         * A zero status value corresponds to state "NEW".
         * <p>
         *     0 状态值对应于 "NEW" 状态。
         */
        if (threadStatus != 0)
            throw new IllegalThreadStateException();

        /* Notify the group that this thread is about to be started
         * so that it can be added to the group's list of threads
         * and the group's unstarted count can be decremented.
         *
         * 通知 group，当前 thread 即将被启动，
         * 以便将其添加到 group 的 thread 列表中，
         * 并将 group 的 unstarted count 减 1。
         */
        group.add(this);

        boolean started = false;
        try {
            start0();
            started = true;
        } finally {
            try {
                if (!started) {
                    group.threadStartFailed(this);
                }
            } catch (Throwable ignore) {
                /* do nothing. If start0 threw a Throwable then
                  it will be passed up the call stack */
            }
        }
    }

    private native void start0();

    /**
     * If this thread was constructed using a separate
     * <code>Runnable</code> run object, then that
     * <code>Runnable</code> object's <code>run</code> method is called;
     * otherwise, this method does nothing and returns.
     * <p>
     * 如果当前 thread 是使用单独的 Runnable run 对象构造的，
     * 那么就会调用该 Runnable 对象的 run 方法；
     *
     * <p>
     * Subclasses of <code>Thread</code> should override this method.
     *
     * @see #start()
     * @see #stop()
     * @see #Thread(ThreadGroup, Runnable, String)
     */
    @Override
    public void run() {
        if (target != null) {
            target.run();
        }
    }

    /**
     * This method is called by the system to give a Thread
     * a chance to clean up before it actually exits.
     * <p>
     * 这个方法由系统调用，给 thread 一个机会在退出之前清理。
     */
    private void exit() {
        if (threadLocals != null && TerminatingThreadLocal.REGISTRY.isPresent()) {
            TerminatingThreadLocal.threadTerminated();
        }
        if (group != null) {
            group.threadTerminated(this);
            group = null;
        }
        /* Aggressively null out all reference fields: see bug 4006245 */
        target = null;
        /* Speed the release of some of these resources */
        threadLocals = null;
        inheritableThreadLocals = null;
        inheritedAccessControlContext = null;
        blocker = null;
        uncaughtExceptionHandler = null;
    }

    /**
     * Forces the thread to stop executing.
     * <p>
     * 强制 thread 停止执行。
     *
     * <p>
     * If there is a security manager installed, its <code>checkAccess</code>
     * method is called with <code>this</code>
     * as its argument. This may result in a
     * <code>SecurityException</code> being raised (in the current thread).
     * <p>
     * 如果安装了 security manager，那么它的 checkAccess 方法会以当前 thread 作为参数调用。
     * 这可能会导致抛出 SecurityException（在当前 thread 中）。
     *
     * <p>
     * If this thread is different from the current thread (that is, the current
     * thread is trying to stop a thread other than itself), the
     * security manager's <code>checkPermission</code> method (with a
     * <code>RuntimePermission("stopThread")</code> argument) is called in
     * addition.
     * Again, this may result in throwing a
     * <code>SecurityException</code> (in the current thread).
     * <p>
     * 如果当前 thread 与当前 thread 不同（即，当前 thread 正在尝试停止除自身之外的 thread），
     * 那么除了调用 security manager 的 checkAccess 方法外，还会调用 security manager 的
     * checkPermission 方法（使用 RuntimePermission("stopThread") 作为参数）。
     * 同样，这可能会导致抛出 SecurityException（在当前 thread 中）。
     *
     * <p>
     * The thread represented by this thread is forced to stop whatever
     * it is doing abnormally and to throw a newly created
     * <code>ThreadDeath</code> object as an exception.
     * <p>
     * 由当前 thread 表示的线程被强制停止它正在做的任何操作，
     * 并抛出一个新创建的 ThreadDeath 对象作为异常。
     *
     * <p>
     * It is permitted to stop a thread that has not yet been started.
     * If the thread is eventually started, it immediately terminates.
     * <p>
     * 允许停止尚未启动的 thread。
     * 如果 thread 最终启动，它将立即终止。
     *
     * <p>
     * An application should not normally try to catch
     * <code>ThreadDeath</code> unless it must do some extraordinary
     * cleanup operation (note that the throwing of
     * <code>ThreadDeath</code> causes <code>finally</code> clauses of
     * <code>try</code> statements to be executed before the thread
     * officially dies).  If a <code>catch</code> clause catches a
     * <code>ThreadDeath</code> object, it is important to rethrow the
     * object so that the thread actually dies.
     * <p>
     * 除非必须执行一些特殊的清理操作，否则应用程序通常不应该尝试捕获 ThreadDeath。
     * 请注意，抛出 ThreadDeath 会导致 try 语句的 finally 子句在 thread 正式死亡之前被执行。
     * 如果 catch 子句捕获 ThreadDeath 对象，重要的是重新抛出该对象，以便 thread 实际上死亡。
     *
     * <p>
     * The top-level error handler that reacts to otherwise uncaught
     * exceptions does not print out a message or otherwise notify the
     * application if the uncaught exception is an instance of
     * <code>ThreadDeath</code>.
     * <p>
     * 最上层的 exception handler（用于处理未捕获的异常）如果未捕获的异常是 ThreadDeath 的实例，
     * 则不会打印消息或以其他方式通知应用程序。
     *
     * @throws SecurityException if the current thread cannot
     *                           modify this thread.
     * @see #interrupt()
     * @see #checkAccess()
     * @see #run()
     * @see #start()
     * @see ThreadDeath
     * @see ThreadGroup#uncaughtException(Thread, Throwable)
     * @see SecurityManager#checkAccess(Thread)
     * @see SecurityManager#checkPermission
     * @deprecated This method is inherently unsafe.  Stopping a thread with
     * Thread.stop causes it to unlock all of the monitors that it
     * has locked (as a natural consequence of the unchecked
     * <code>ThreadDeath</code> exception propagating up the stack).  If
     * any of the objects previously protected by these monitors were in
     * an inconsistent state, the damaged objects become visible to
     * other threads, potentially resulting in arbitrary behavior.  Many
     * uses of <code>stop</code> should be replaced by code that simply
     * modifies some variable to indicate that the target thread should
     * stop running.  The target thread should check this variable
     * regularly, and return from its run method in an orderly fashion
     * if the variable indicates that it is to stop running.  If the
     * target thread waits for long periods (on a condition variable,
     * for example), the <code>interrupt</code> method should be used to
     * interrupt the wait.
     * For more information, see
     * <a href="{@docRoot}/../technotes/guides/concurrency/threadPrimitiveDeprecation.html">Why
     * are Thread.stop, Thread.suspend and Thread.resume Deprecated?</a>.
     * <p>
     * 这个方法本质上是不安全的。
     * 使用 Thread.stop 停止 thread 会导致它释放它已获取的所有 monitor（作为 ThreadDeath 异常传播到堆栈的结果）。
     * 如果这些 monitor 之前保护的任何对象处于不一致状态，则这些不一致的内容将对其他 thread 可见，可能导致任意行为。
     * 许多使用 stop 的地方应该被替换为简单地修改某些变量，以指示目标 thread 应该停止运行。
     * 目标 thread 应该定期检查此变量，并在变量指示它应该停止运行时从其 run 方法中有序地返回。
     * 如果目标 thread 等待很长时间（例如，在条件变量上），则应使用 interrupt 方法来中断等待。
     * 有关详细信息，请参阅 Why are Thread.stop, Thread.suspend and Thread.resume Deprecated?。
     */
    @Deprecated
    public final void stop() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            checkAccess();
            if (this != Thread.currentThread()) {
                security.checkPermission(SecurityConstants.STOP_THREAD_PERMISSION);
            }
        }
        // A zero status value corresponds to "NEW", it can't change to
        // not-NEW because we hold the lock.
        if (threadStatus != 0) {
            resume(); // Wake up thread if it was suspended; no-op otherwise
        }

        // The VM can handle all thread states
        stop0(new ThreadDeath());
    }

    /**
     * Throws {@code UnsupportedOperationException}.
     *
     * @param obj ignored
     * @deprecated This method was originally designed to force a thread to stop
     * and throw a given {@code Throwable} as an exception. It was
     * inherently unsafe (see {@link #stop()} for details), and furthermore
     * could be used to generate exceptions that the target thread was
     * not prepared to handle.
     * For more information, see
     * <a href="{@docRoot}/../technotes/guides/concurrency/threadPrimitiveDeprecation.html">Why
     * are Thread.stop, Thread.suspend and Thread.resume Deprecated?</a>.
     */
    @Deprecated
    public final synchronized void stop(Throwable obj) {
        throw new UnsupportedOperationException();
    }

    /**
     * Interrupts this thread.
     * <p>
     * 中断当前 thread。
     *
     * <p> Unless the current thread is interrupting itself, which is
     * always permitted, the {@link #checkAccess() checkAccess} method
     * of this thread is invoked, which may cause a {@link
     * SecurityException} to be thrown.
     * <p>
     * 除非当前 thread 是中断自身，否则将调用当前 thread 的 checkAccess 方法，这可能会导致抛出 SecurityException。
     *
     * <p> If this thread is blocked in an invocation of the {@link
     * Object#wait() wait()}, {@link Object#wait(long) wait(long)}, or {@link
     * Object#wait(long, int) wait(long, int)} methods of the {@link Object}
     * class, or of the {@link #join()}, {@link #join(long)}, {@link
     * #join(long, int)}, {@link #sleep(long)}, or {@link #sleep(long, int)},
     * methods of this class, then its interrupt status will be cleared and it
     * will receive an {@link InterruptedException}.
     * <p>
     * 如果当前 thread 被阻塞在 Object 类的 wait()、wait(long) 或 wait(long, int) 方法，
     * 或者当前 thread 被阻塞在当前类的 join()、join(long)、join(long, int)、sleep(long) 或 sleep(long, int) 方法，
     * 那么它的中断状态将被清除，并且它将接收到 InterruptedException。
     *
     * <p> If this thread is blocked in an I/O operation upon an {@link
     * java.nio.channels.InterruptibleChannel InterruptibleChannel}
     * then the channel will be closed, the thread's interrupt
     * status will be set, and the thread will receive a {@link
     * java.nio.channels.ClosedByInterruptException}.
     * <p>
     * 如果当前 thread 被阻塞在 java.nio.channels.InterruptibleChannel 上的 I/O 操作中，
     * 那么 channel 将被关闭，thread 的中断状态将被设置，并且 thread 将接收到 ClosedByInterruptException。
     *
     * <p> If this thread is blocked in a {@link java.nio.channels.Selector}
     * then the thread's interrupt status will be set and it will return
     * immediately from the selection operation, possibly with a non-zero
     * value, just as if the selector's {@link
     * java.nio.channels.Selector#wakeup wakeup} method were invoked.
     * <p>
     * 如果当前 thread 被阻塞在 java.nio.channels.Selector 上，
     * 那么 thread 的中断状态将被设置，并且它将立即从选择操作中返回，可能会返回非零值，就像调用了 selector 的 wakeup 方法一样。
     * （Selector 的 wakeup 方法会立即返回，不会阻塞）
     *
     * <p> If none of the previous conditions hold then this thread's interrupt
     * status will be set. </p>
     *
     * <p>
     * 如果没有满足上述条件，那么 thread 的中断状态将被设置。
     *
     * <p> Interrupting a thread that is not alive need not have any effect.
     * <p>
     * 中断一个不是 alive 的 thread 不一定会有任何效果。
     *
     * @throws SecurityException if the current thread cannot modify this thread
     * @revised 6.0
     * @spec JSR-51
     */
    public void interrupt() {
        if (this != Thread.currentThread())
            checkAccess();

        synchronized (blockerLock) {
            Interruptible b = blocker;
            // 如果设置了 blocker，那么就调用 blocker.interrupt(this)
            // 一般来说，IO 相关的会设置
            if (b != null) {
                interrupt0();           // Just to set the interrupt flag
                b.interrupt(this);
                return;
            }
        }
        interrupt0();
    }

    /**
     * Tests whether the current thread has been interrupted.  The
     * <i>interrupted status</i> of the thread is cleared by this method.  In
     * other words, if this method were to be called twice in succession, the
     * second call would return false (unless the current thread were
     * interrupted again, after the first call had cleared its interrupted
     * status and before the second call had examined it).
     * <p>
     * 测试当前 thread 是否已被中断。
     * 该方法会清除 thread 的 interrupted status。
     * 换句话说，如果连续两次调用该方法，第二次调用将返回 false
     * （除非在第一次调用清除其 interrupted status 之后，并且在第二次调用检查它之前，当前 thread 再次被中断）。
     *
     * <p>A thread interruption ignored because a thread was not alive
     * at the time of the interrupt will be reflected by this method
     * returning false.
     * <p>
     * 由于 thread 在中断时不是 alive 的，所以忽略 thread 中断将导致该方法返回 false。
     *
     * @return <code>true</code> if the current thread has been interrupted;
     * <code>false</code> otherwise.
     * @revised 6.0
     * @see #isInterrupted()
     */
    public static boolean interrupted() {
        return currentThread().isInterrupted(true);
    }

    /**
     * Tests whether this thread has been interrupted.  The <i>interrupted
     * status</i> of the thread is unaffected by this method.
     * <p>
     * 测试当前 thread 是否已被中断。
     * 该方法不会影响 thread 的 interrupted status。
     *
     * <p>A thread interruption ignored because a thread was not alive
     * at the time of the interrupt will be reflected by this method
     * returning false.
     * <p>
     * 由于 thread 在中断时不是 alive 的，所以忽略 thread 中断将导致该方法返回 false。
     *
     * @return <code>true</code> if this thread has been interrupted;
     * <code>false</code> otherwise.
     * @revised 6.0
     * @see #interrupted()
     */
    public boolean isInterrupted() {
        return isInterrupted(false);
    }

    /**
     * Tests if some Thread has been interrupted.  The interrupted state
     * is reset or not based on the value of ClearInterrupted that is
     * passed.
     * <p>
     * 测试某个 thread 是否已被中断。
     * 根据传递的 ClearInterrupted 的值，中断状态将被重置或不重置。
     */
    private native boolean isInterrupted(boolean ClearInterrupted);

    /**
     * Throws {@link NoSuchMethodError}.
     *
     * @throws NoSuchMethodError always
     * @deprecated This method was originally designed to destroy this
     * thread without any cleanup. Any monitors it held would have
     * remained locked. However, the method was never implemented.
     * If if were to be implemented, it would be deadlock-prone in
     * much the manner of {@link #suspend}. If the target thread held
     * a lock protecting a critical system resource when it was
     * destroyed, no thread could ever access this resource again.
     * If another thread ever attempted to lock this resource, deadlock
     * would result. Such deadlocks typically manifest themselves as
     * "frozen" processes. For more information, see
     * <a href="{@docRoot}/../technotes/guides/concurrency/threadPrimitiveDeprecation.html">
     * Why are Thread.stop, Thread.suspend and Thread.resume Deprecated?</a>.
     * <p>
     * 这个方法最初是设计用来销毁当前 thread，而不进行任何清理。
     * 它持有的任何 monitor 都将保持锁定。
     * 但是，该方法从未被实现。
     * 如果它被实现，它将以与 suspend 类似的方式容易死锁。
     * 如果目标 thread 在被销毁时持有保护关键系统资源的锁，那么没有 thread 可以再次访问此资源。
     * 如果另一个 thread 尝试锁定此资源，将导致死锁。
     * 这种死锁通常表现为 "frozen" 进程。
     */
    @Deprecated
    public void destroy() {
        throw new NoSuchMethodError();
    }

    /**
     * Tests if this thread is alive. A thread is alive if it has
     * been started and has not yet died.
     * <p>
     * 测试当前 thread 是否 alive。
     * 如果 thread 已启动但尚未死亡，则 thread 是 alive 的。
     *
     * @return <code>true</code> if this thread is alive;
     * <code>false</code> otherwise.
     */
    public final native boolean isAlive();

    /**
     * Suspends this thread.
     * <p>
     * First, the <code>checkAccess</code> method of this thread is called
     * with no arguments. This may result in throwing a
     * <code>SecurityException </code>(in the current thread).
     * <p>
     * If the thread is alive, it is suspended and makes no further
     * progress unless and until it is resumed.
     *
     * @throws SecurityException if the current thread cannot modify
     *                           this thread.
     * @see #checkAccess
     * @deprecated This method has been deprecated, as it is
     * inherently deadlock-prone.  If the target thread holds a lock on the
     * monitor protecting a critical system resource when it is suspended, no
     * thread can access this resource until the target thread is resumed. If
     * the thread that would resume the target thread attempts to lock this
     * monitor prior to calling <code>resume</code>, deadlock results.  Such
     * deadlocks typically manifest themselves as "frozen" processes.
     * For more information, see
     * <a href="{@docRoot}/../technotes/guides/concurrency/threadPrimitiveDeprecation.html">Why
     * are Thread.stop, Thread.suspend and Thread.resume Deprecated?</a>.
     * <p>
     * 这个方法已被弃用，因为它本质上是死锁的。
     * 如果目标 thread 在被 suspend 时持有保护关键系统资源的 monitor 的锁，
     * 那么在目标 thread 被 resume 之前，没有 thread 可以访问此资源。
     * 如果尝试在调用 resume 之前锁定此 monitor 的 thread 尝试锁定此 monitor，则会导致死锁。
     * 这种死锁通常表现为 "frozen" 进程。
     */
    @Deprecated
    public final void suspend() {
        checkAccess();
        suspend0();
    }

    /**
     * Resumes a suspended thread.
     * <p>
     * First, the <code>checkAccess</code> method of this thread is called
     * with no arguments. This may result in throwing a
     * <code>SecurityException</code> (in the current thread).
     * <p>
     * If the thread is alive but suspended, it is resumed and is
     * permitted to make progress in its execution.
     *
     * @throws SecurityException if the current thread cannot modify this
     *                           thread.
     * @see #checkAccess
     * @see #suspend()
     * @deprecated This method exists solely for use with {@link #suspend},
     * which has been deprecated because it is deadlock-prone.
     * For more information, see
     * <a href="{@docRoot}/../technotes/guides/concurrency/threadPrimitiveDeprecation.html">Why
     * are Thread.stop, Thread.suspend and Thread.resume Deprecated?</a>.
     */
    @Deprecated
    public final void resume() {
        checkAccess();
        resume0();
    }

    /**
     * Changes the priority of this thread.
     * <p>
     * First the <code>checkAccess</code> method of this thread is called
     * with no arguments. This may result in throwing a
     * <code>SecurityException</code>.
     * <p>
     * Otherwise, the priority of this thread is set to the smaller of
     * the specified <code>newPriority</code> and the maximum permitted
     * priority of the thread's thread group.
     *
     * @param newPriority priority to set this thread to
     * @throws IllegalArgumentException If the priority is not in the
     *                                  range <code>MIN_PRIORITY</code> to
     *                                  <code>MAX_PRIORITY</code>.
     * @throws SecurityException        if the current thread cannot modify
     *                                  this thread.
     * @see #getPriority
     * @see #checkAccess()
     * @see #getThreadGroup()
     * @see #MAX_PRIORITY
     * @see #MIN_PRIORITY
     * @see ThreadGroup#getMaxPriority()
     */
    public final void setPriority(int newPriority) {
        ThreadGroup g;
        checkAccess();
        if (newPriority > MAX_PRIORITY || newPriority < MIN_PRIORITY) {
            throw new IllegalArgumentException();
        }
        if ((g = getThreadGroup()) != null) {
            if (newPriority > g.getMaxPriority()) {
                newPriority = g.getMaxPriority();
            }
            setPriority0(priority = newPriority);
        }
    }

    /**
     * Returns this thread's priority.
     *
     * @return this thread's priority.
     * @see #setPriority
     */
    public final int getPriority() {
        return priority;
    }

    /**
     * Changes the name of this thread to be equal to the argument
     * <code>name</code>.
     * <p>
     * First the <code>checkAccess</code> method of this thread is called
     * with no arguments. This may result in throwing a
     * <code>SecurityException</code>.
     *
     * @param name the new name for this thread.
     * @throws SecurityException if the current thread cannot modify this
     *                           thread.
     * @see #getName
     * @see #checkAccess()
     */
    public final synchronized void setName(String name) {
        checkAccess();
        if (name == null) {
            throw new NullPointerException("name cannot be null");
        }

        this.name = name;
        if (threadStatus != 0) {
            setNativeName(name);
        }
    }

    /**
     * Returns this thread's name.
     *
     * @return this thread's name.
     * @see #setName(String)
     */
    public final String getName() {
        return name;
    }

    /**
     * Returns the thread group to which this thread belongs.
     * This method returns null if this thread has died
     * (been stopped).
     *
     * @return this thread's thread group.
     */
    public final ThreadGroup getThreadGroup() {
        return group;
    }

    /**
     * Returns an estimate of the number of active threads in the current
     * thread's {@linkplain java.lang.ThreadGroup thread group} and its
     * subgroups. Recursively iterates over all subgroups in the current
     * thread's thread group.
     * <p>
     * 返回当前 thread 的 thread group 及其子 group 中活动 thread 的数量的估计值。
     * 递归迭代当前 thread 的 thread group 中的所有子 group。
     *
     * <p> The value returned is only an estimate because the number of
     * threads may change dynamically while this method traverses internal
     * data structures, and might be affected by the presence of certain
     * system threads. This method is intended primarily for debugging
     * and monitoring purposes.
     * <p>
     * 返回的值只是一个估计值，因为 thread 的数量可能会在此方法遍历内部数据结构时动态更改，
     * 并且可能会受到某些系统 thread 的影响。该方法主要用于调试和监视目的。
     *
     * @return an estimate of the number of active threads in the current
     * thread's thread group and in any other thread group that
     * has the current thread's thread group as an ancestor
     */
    public static int activeCount() {
        return currentThread().getThreadGroup().activeCount();
    }

    /**
     * Copies into the specified array every active thread in the current
     * thread's thread group and its subgroups. This method simply
     * invokes the {@link java.lang.ThreadGroup#enumerate(Thread[])}
     * method of the current thread's thread group.
     * <p>
     * 将当前 thread 的 thread group 及其子 group 中的每个活动 thread 复制到指定的数组中。
     * 该方法只是调用当前 thread 的 thread group 的 enumerate(Thread[]) 方法。
     *
     * <p> An application might use the {@linkplain #activeCount activeCount}
     * method to get an estimate of how big the array should be, however
     * <i>if the array is too short to hold all the threads, the extra threads
     * are silently ignored.</i>  If it is critical to obtain every active
     * thread in the current thread's thread group and its subgroups, the
     * invoker should verify that the returned int value is strictly less
     * than the length of {@code tarray}.
     * <p>
     * 应用程序可能会使用 activeCount 方法来估计数组的大小，
     * 但是，如果数组太短而无法容纳所有 thread，则会静默地忽略额外的 thread。
     * 如果必须获取当前 thread 的 thread group 及其子 group 中的每个活动 thread，
     * 那么调用者应该验证返回的 int 值是否严格小于 tarray 的长度。(入参 array 不能太短)
     *
     * <p> Due to the inherent race condition in this method, it is recommended
     * that the method only be used for debugging and monitoring purposes.
     * <p>
     * 由于此方法中固有的竞争条件，建议该方法仅用于调试和监视目的。
     *
     * @param tarray an array into which to put the list of threads
     * @return the number of threads put into the array
     * @throws SecurityException if {@link java.lang.ThreadGroup#checkAccess} determines that
     *                           the current thread cannot access its thread group
     */
    public static int enumerate(Thread tarray[]) {
        return currentThread().getThreadGroup().enumerate(tarray);
    }

    /**
     * Counts the number of stack frames in this thread. The thread must
     * be suspended.
     * <p>
     * 计算当前 thread 的 stack frame 数量。thread 必须是 suspended 的。
     *
     * @return the number of stack frames in this thread.
     * @throws IllegalThreadStateException if this thread is not
     *                                     suspended.
     * @deprecated The definition of this call depends on {@link #suspend},
     * which is deprecated.  Further, the results of this call
     * were never well-defined.
     */
    @Deprecated
    public native int countStackFrames();

    /**
     * Waits at most {@code millis} milliseconds for this thread to
     * die. A timeout of {@code 0} means to wait forever.
     * <p>
     * 等待 millis 毫秒，直到目标 thread 死亡
     * （目标线程不是当前线程，main 线程执行了 t1.join() 之后，main 会陷入等待直到 t1 死亡后发出 notify，如果被提前 notify 了也会查询 t1 的状态而再次 wait）。
     * timeout 为 0 表示永远等待。
     *
     * <p> This implementation uses a loop of {@code this.wait} calls
     * conditioned on {@code this.isAlive}. As a thread terminates the
     * {@code this.notifyAll} method is invoked. It is recommended that
     * applications not use {@code wait}, {@code notify}, or
     * {@code notifyAll} on {@code Thread} instances.
     * <p>
     * 该方法通过循环调用 this.wait（取决于 this.isAlive）。
     * 当 thread 终止时，将调用 this.notifyAll 方法。
     *
     * @param millis the time to wait in milliseconds
     * @throws IllegalArgumentException if the value of {@code millis} is negative
     * @throws InterruptedException     if any thread has interrupted the current thread. The
     *                                  <i>interrupted status</i> of the current thread is
     *                                  cleared when this exception is thrown.
     */
    public final synchronized void join(long millis)
            throws InterruptedException {
        long base = System.currentTimeMillis();
        long now = 0;

        if (millis < 0) {
            throw new IllegalArgumentException("timeout value is negative");
        }

        if (millis == 0) {
            // 如果当前线程被意外的 notify 了，那么会校验目标 thread 是否存活，如果尚未死亡，当前线程就会再次 wait
            while (isAlive()) {
                // Object 的 wait；会导致当前线程等待！
                wait(0);
            }
        } else {
            while (isAlive()) {
                long delay = millis - now;
                if (delay <= 0) {
                    break;
                }
                wait(delay);
                now = System.currentTimeMillis() - base;
            }
        }
    }

    /**
     * Waits at most {@code millis} milliseconds plus
     * {@code nanos} nanoseconds for this thread to die.
     *
     * <p> This implementation uses a loop of {@code this.wait} calls
     * conditioned on {@code this.isAlive}. As a thread terminates the
     * {@code this.notifyAll} method is invoked. It is recommended that
     * applications not use {@code wait}, {@code notify}, or
     * {@code notifyAll} on {@code Thread} instances.
     *
     * @param millis the time to wait in milliseconds
     * @param nanos  {@code 0-999999} additional nanoseconds to wait
     * @throws IllegalArgumentException if the value of {@code millis} is negative, or the value
     *                                  of {@code nanos} is not in the range {@code 0-999999}
     * @throws InterruptedException     if any thread has interrupted the current thread. The
     *                                  <i>interrupted status</i> of the current thread is
     *                                  cleared when this exception is thrown.
     */
    public final synchronized void join(long millis, int nanos)
            throws InterruptedException {

        if (millis < 0) {
            throw new IllegalArgumentException("timeout value is negative");
        }

        if (nanos < 0 || nanos > 999999) {
            throw new IllegalArgumentException(
                    "nanosecond timeout value out of range");
        }

        if (nanos >= 500000 || (nanos != 0 && millis == 0)) {
            millis++;
        }

        join(millis);
    }

    /**
     * Waits for this thread to die.
     *
     * <p> An invocation of this method behaves in exactly the same
     * way as the invocation
     *
     * <blockquote>
     * {@linkplain #join(long) join}{@code (0)}
     * </blockquote>
     *
     * @throws InterruptedException if any thread has interrupted the current thread. The
     *                              <i>interrupted status</i> of the current thread is
     *                              cleared when this exception is thrown.
     */
    public final void join() throws InterruptedException {
        join(0);
    }

    /**
     * Prints a stack trace of the current thread to the standard error stream.
     * This method is used only for debugging.
     * <p>
     * 打印当前 thread 的 stack trace 到标准错误流。
     *
     * @see Throwable#printStackTrace()
     */
    public static void dumpStack() {
        new Exception("Stack trace").printStackTrace();
    }

    /**
     * Marks this thread as either a {@linkplain #isDaemon daemon} thread
     * or a user thread. The Java Virtual Machine exits when the only
     * threads running are all daemon threads.
     * <p>
     * 将当前 thread 标记为 daemon thread 或 user thread。
     * 当只有 daemon thread 运行时，Java 虚拟机将退出。
     *
     * <p> This method must be invoked before the thread is started.
     *
     * @param on if {@code true}, marks this thread as a daemon thread
     * @throws IllegalThreadStateException if this thread is {@linkplain #isAlive alive}
     * @throws SecurityException           if {@link #checkAccess} determines that the current
     *                                     thread cannot modify this thread
     */
    public final void setDaemon(boolean on) {
        checkAccess();
        // 只有在线程启动之前才能设置，或许线程死亡后也能设置？
        if (isAlive()) {
            throw new IllegalThreadStateException();
        }
        daemon = on;
    }

    /**
     * Tests if this thread is a daemon thread.
     *
     * @return <code>true</code> if this thread is a daemon thread;
     * <code>false</code> otherwise.
     * @see #setDaemon(boolean)
     */
    public final boolean isDaemon() {
        return daemon;
    }

    /**
     * Determines if the currently running thread has permission to
     * modify this thread.
     * <p>
     * If there is a security manager, its <code>checkAccess</code> method
     * is called with this thread as its argument. This may result in
     * throwing a <code>SecurityException</code>.
     *
     * @throws SecurityException if the current thread is not allowed to
     *                           access this thread.
     * @see SecurityManager#checkAccess(Thread)
     */
    public final void checkAccess() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkAccess(this);
        }
    }

    /**
     * Returns a string representation of this thread, including the
     * thread's name, priority, and thread group.
     *
     * @return a string representation of this thread.
     */
    public String toString() {
        ThreadGroup group = getThreadGroup();
        if (group != null) {
            return "Thread[" + getName() + "," + getPriority() + "," +
                    group.getName() + "]";
        } else {
            // 如果 thread group 为 null，那么说明就处于 rootGroup 中
            return "Thread[" + getName() + "," + getPriority() + "," +
                    "" + "]";
        }
    }

    /**
     * Returns the context ClassLoader for this Thread. The context
     * ClassLoader is provided by the creator of the thread for use
     * by code running in this thread when loading classes and resources.
     * If not {@linkplain #setContextClassLoader set}, the default is the
     * ClassLoader context of the parent Thread. The context ClassLoader of the
     * primordial thread is typically set to the class loader used to load the
     * application.
     * <p>
     * 返回当前 thread 的 context ClassLoader。
     * context ClassLoader 是由 thread 的创建者提供的，用于在此 thread 中运行的代码在加载类和资源时使用。
     * 如果没有设置，那么默认值是父 thread 的 ClassLoader context。
     * 原始 thread 的 context ClassLoader 通常设置为用于加载应用程序的类加载器。
     *
     * <p>If a security manager is present, and the invoker's class loader is not
     * {@code null} and is not the same as or an ancestor of the context class
     * loader, then this method invokes the security manager's {@link
     * SecurityManager#checkPermission(java.security.Permission) checkPermission}
     * method with a {@link RuntimePermission RuntimePermission}{@code
     * ("getClassLoader")} permission to verify that retrieval of the context
     * class loader is permitted.
     * <p>
     * 如果存在 security manager，并且调用者的类加载器不为 null，并且不与 context class loader 相同或不是其祖先加载器，
     * 那么该方法将使用 RuntimePermission("getClassLoader") 权限调用安全管理器的 checkPermission 方法，
     * 以验证是否允许检索 context class loader。
     * （如果调用者的类加载器为 null，那么不会调用安全管理器的 checkPermission 方法）
     *
     * @return the context ClassLoader for this Thread, or {@code null}
     * indicating the system class loader (or, failing that, the
     * bootstrap class loader)
     * @throws SecurityException if the current thread cannot get the context ClassLoader
     * @since 1.2
     */
    @CallerSensitive
    public ClassLoader getContextClassLoader() {
        if (contextClassLoader == null)
            return null;
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            ClassLoader.checkClassLoaderPermission(contextClassLoader,
                    Reflection.getCallerClass());
        }
        return contextClassLoader;
    }

    /**
     * Sets the context ClassLoader for this Thread. The context
     * ClassLoader can be set when a thread is created, and allows
     * the creator of the thread to provide the appropriate class loader,
     * through {@code getContextClassLoader}, to code running in the thread
     * when loading classes and resources.
     *
     * <p>If a security manager is present, its {@link
     * SecurityManager#checkPermission(java.security.Permission) checkPermission}
     * method is invoked with a {@link RuntimePermission RuntimePermission}{@code
     * ("setContextClassLoader")} permission to see if setting the context
     * ClassLoader is permitted.
     *
     * @param cl the context ClassLoader for this Thread, or null  indicating the
     *           system class loader (or, failing that, the bootstrap class loader)
     * @throws SecurityException if the current thread cannot set the context ClassLoader
     * @since 1.2
     */
    public void setContextClassLoader(ClassLoader cl) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("setContextClassLoader"));
        }
        contextClassLoader = cl;
    }

    /**
     * Returns <tt>true</tt> if and only if the current thread holds the
     * monitor lock on the specified object.
     * <p>
     * 当且仅当当前 thread 持有指定对象的 monitor lock 时，才返回 true。
     *
     * <p>This method is designed to allow a program to assert that
     * the current thread already holds a specified lock:
     * <pre>
     *     assert Thread.holdsLock(obj);
     * </pre>
     *
     * @param obj the object on which to test lock ownership
     * @return <tt>true</tt> if the current thread holds the monitor lock on
     * the specified object.
     * @throws NullPointerException if obj is <tt>null</tt>
     * @since 1.4
     */
    public static native boolean holdsLock(Object obj);

    private static final StackTraceElement[] EMPTY_STACK_TRACE
            = new StackTraceElement[0];

    /**
     * Returns an array of stack trace elements representing the stack dump
     * of this thread.  This method will return a zero-length array if
     * this thread has not started, has started but has not yet been
     * scheduled to run by the system, or has terminated.
     * If the returned array is of non-zero length then the first element of
     * the array represents the top of the stack, which is the most recent
     * method invocation in the sequence.  The last element of the array
     * represents the bottom of the stack, which is the least recent method
     * invocation in the sequence.
     * <p>
     * 返回表示此 thread 的 stack dump 的 stack trace element 数组。
     * 如果 thread 尚未启动，已启动但尚未被系统调度运行，或已终止，则此方法将返回长度为零的数组。
     * 如果返回的数组长度为非零，则数组的第一个元素表示 stack 的顶部，即序列中最近的方法调用。
     * 数组的最后一个元素表示 stack 的底部，即序列中最开始的方法调用。
     *
     * <p>If there is a security manager, and this thread is not
     * the current thread, then the security manager's
     * <tt>checkPermission</tt> method is called with a
     * <tt>RuntimePermission("getStackTrace")</tt> permission
     * to see if it's ok to get the stack trace.
     *
     * <p>Some virtual machines may, under some circumstances, omit one
     * or more stack frames from the stack trace.  In the extreme case,
     * a virtual machine that has no stack trace information concerning
     * this thread is permitted to return a zero-length array from this
     * method.
     * <p>
     * 在某些情况下，某些虚拟机可能会从 stack trace 中省略一个或多个 stack frame。
     * 在极端情况下，允许没有关于此 thread 的 stack trace 信息的虚拟机从此方法返回长度为零的数组。
     *
     * @return an array of <tt>StackTraceElement</tt>,
     * each represents one stack frame.
     * @throws SecurityException if a security manager exists and its
     *                           <tt>checkPermission</tt> method doesn't allow
     *                           getting the stack trace of thread.
     * @see SecurityManager#checkPermission
     * @see RuntimePermission
     * @see Throwable#getStackTrace
     * @since 1.5
     */
    public StackTraceElement[] getStackTrace() {
        if (this != Thread.currentThread()) {
            // 如果是在当前线程，尝试获取其他线程的 stack trace
            // check for getStackTrace permission
            SecurityManager security = System.getSecurityManager();
            if (security != null) {
                security.checkPermission(
                        SecurityConstants.GET_STACK_TRACE_PERMISSION);
            }
            // optimization so we do not call into the vm for threads that
            // have not yet started or have terminated
            if (!isAlive()) {
                return EMPTY_STACK_TRACE;
            }
            // 通过 native 方法获取目标线程的 stack trace
            StackTraceElement[][] stackTraceArray = dumpThreads(new Thread[]{this});
            StackTraceElement[] stackTrace = stackTraceArray[0];
            // a thread that was alive during the previous isAlive call may have
            // since terminated, therefore not having a stacktrace.
            if (stackTrace == null) {
                stackTrace = EMPTY_STACK_TRACE;
            }
            return stackTrace;
        } else {
            // Don't need JVM help for current thread
            // 如果是当前线程，那么直接调用 Throwable 的 getStackTrace 方法
            return (new Exception()).getStackTrace();
        }
    }

    /**
     * Returns a map of stack traces for all live threads.
     * The map keys are threads and each map value is an array of
     * <tt>StackTraceElement</tt> that represents the stack dump
     * of the corresponding <tt>Thread</tt>.
     * The returned stack traces are in the format specified for
     * the {@link #getStackTrace getStackTrace} method.
     * <p>
     * 返回所有活动线程的 stack trace 的 map。
     * map 的 key 是 thread，每个 map 的 value 是一个 StackTraceElement 数组，
     * 该数组表示相应 Thread 的 stack dump。
     * 返回的 stack trace 的格式与 getStackTrace 方法指定的格式相同。
     *
     * <p>The threads may be executing while this method is called.
     * The stack trace of each thread only represents a snapshot and
     * each stack trace may be obtained at different time.  A zero-length
     * array will be returned in the map value if the virtual machine has
     * no stack trace information about a thread.
     * <p>
     * 在调用此方法时，线程可能正在执行。
     * 每个 thread 的 stack trace 只表示一个快照，并且每个 stack trace 可能在不同的时间获得。
     * 如果虚拟机没有关于 thread 的 stack trace 信息，则在 map value 中返回长度为零的数组。
     *
     * <p>If there is a security manager, then the security manager's
     * <tt>checkPermission</tt> method is called with a
     * <tt>RuntimePermission("getStackTrace")</tt> permission as well as
     * <tt>RuntimePermission("modifyThreadGroup")</tt> permission
     * to see if it is ok to get the stack trace of all threads.
     * <p>
     * 如果存在 security manager，则会调用 security manager 的 checkPermission 方法，
     *
     * @return a <tt>Map</tt> from <tt>Thread</tt> to an array of
     * <tt>StackTraceElement</tt> that represents the stack trace of
     * the corresponding thread.
     * @throws SecurityException if a security manager exists and its
     *                           <tt>checkPermission</tt> method doesn't allow
     *                           getting the stack trace of thread.
     * @see #getStackTrace
     * @see SecurityManager#checkPermission
     * @see RuntimePermission
     * @see Throwable#getStackTrace
     * @since 1.5
     */
    public static Map<Thread, StackTraceElement[]> getAllStackTraces() {
        // check for getStackTrace permission
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkPermission(
                    SecurityConstants.GET_STACK_TRACE_PERMISSION);
            security.checkPermission(
                    SecurityConstants.MODIFY_THREADGROUP_PERMISSION);
        }

        // Get a snapshot of the list of all threads
        // 获取所有线程，因为线程本身就在即时变化，所以只能代表获取时的快照
        Thread[] threads = getThreads();
        StackTraceElement[][] traces = dumpThreads(threads);
        Map<Thread, StackTraceElement[]> m = new HashMap<>(threads.length);
        for (int i = 0; i < threads.length; i++) {
            StackTraceElement[] stackTrace = traces[i];
            if (stackTrace != null) {
                m.put(threads[i], stackTrace);
            }
            // else terminated so we don't put it in the map
        }
        return m;
    }


    private static final RuntimePermission SUBCLASS_IMPLEMENTATION_PERMISSION =
            new RuntimePermission("enableContextClassLoaderOverride");

    /**
     * cache of subclass security audit results
     * <p>
     * 子类安全审计结果的缓存
     */
    /* Replace with ConcurrentReferenceHashMap when/if it appears in a future
     * release */
    private static class Caches {
        /**
         * cache of subclass security audit results
         */
        static final ConcurrentMap<WeakClassKey, Boolean> subclassAudits =
                new ConcurrentHashMap<>();

        /**
         * queue for WeakReferences to audited subclasses
         */
        static final ReferenceQueue<Class<?>> subclassAuditsQueue =
                new ReferenceQueue<>();
    }

    /**
     * Verifies that this (possibly subclass) instance can be constructed
     * without violating security constraints: the subclass must not override
     * security-sensitive non-final methods, or else the
     * "enableContextClassLoaderOverride" RuntimePermission is checked.
     * <p>
     * 验证此实例（可能是子类）是否可以在不违反安全约束的情况下构造：
     * 子类不能覆盖安全敏感的非 final 方法，否则将检查 "enableContextClassLoaderOverride" RuntimePermission。
     */
    private static boolean isCCLOverridden(Class<?> cl) {
        if (cl == Thread.class)
            return false;

        processQueue(Caches.subclassAuditsQueue, Caches.subclassAudits);
        WeakClassKey key = new WeakClassKey(cl, Caches.subclassAuditsQueue);
        Boolean result = Caches.subclassAudits.get(key);
        if (result == null) {
            // 如果没有缓存，那么就进行安全审计
            result = Boolean.valueOf(auditSubclass(cl));
            Caches.subclassAudits.putIfAbsent(key, result);
        }

        return result.booleanValue();
    }

    /**
     * Performs reflective checks on given subclass to verify that it doesn't
     * override security-sensitive non-final methods.  Returns true if the
     * subclass overrides any of the methods, false otherwise.
     * <p>
     * 对给定的子类执行反射检查，以验证它是否覆盖了安全敏感的非 final 方法。
     */
    private static boolean auditSubclass(final Class<?> subcl) {
        Boolean result = AccessController.doPrivileged(
                new PrivilegedAction<Boolean>() {
                    public Boolean run() {
                        for (Class<?> cl = subcl;
                             cl != Thread.class;
                             cl = cl.getSuperclass()) {
                            // 一旦发现子类覆盖了父类的下述两个方法，那么就返回 true
                            try {
                                cl.getDeclaredMethod("getContextClassLoader", new Class<?>[0]);
                                return Boolean.TRUE;
                            } catch (NoSuchMethodException ex) {
                            }
                            try {
                                Class<?>[] params = {ClassLoader.class};
                                cl.getDeclaredMethod("setContextClassLoader", params);
                                return Boolean.TRUE;
                            } catch (NoSuchMethodException ex) {
                            }
                        }
                        return Boolean.FALSE;
                    }
                }
        );
        return result.booleanValue();
    }

    private native static StackTraceElement[][] dumpThreads(Thread[] threads);

    private native static Thread[] getThreads();

    /**
     * Returns the identifier of this Thread.  The thread ID is a positive
     * <tt>long</tt> number generated when this thread was created.
     * The thread ID is unique and remains unchanged during its lifetime.
     * When a thread is terminated, this thread ID may be reused.
     *
     * @return this thread's ID.
     * @since 1.5
     */
    public long getId() {
        return tid;
    }

    /**
     * A thread state.  A thread can be in one of the following states:
     * <ul>
     * <li>{@link #NEW}<br>
     *     A thread that has not yet started is in this state.
     *     </li>
     * <li>{@link #RUNNABLE}<br>
     *     A thread executing in the Java virtual machine is in this state.
     *     </li>
     * <li>{@link #BLOCKED}<br>
     *     A thread that is blocked waiting for a monitor lock
     *     is in this state.
     *     </li>
     * <li>{@link #WAITING}<br>
     *     A thread that is waiting indefinitely for another thread to
     *     perform a particular action is in this state.
     *     </li>
     * <li>{@link #TIMED_WAITING}<br>
     *     A thread that is waiting for another thread to perform an action
     *     for up to a specified waiting time is in this state.
     *     </li>
     * <li>{@link #TERMINATED}<br>
     *     A thread that has exited is in this state.
     *     </li>
     * </ul>
     *
     * <p>
     * A thread can be in only one state at a given point in time.
     * These states are virtual machine states which do not reflect
     * any operating system thread states.
     *
     * @see #getState
     * @since 1.5
     */
    public enum State {
        /**
         * Thread state for a thread which has not yet started.
         * <p>
         * 尚未启动的 thread 的状态。
         */
        NEW,

        /**
         * Thread state for a runnable thread.  A thread in the runnable
         * state is executing in the Java virtual machine but it may
         * be waiting for other resources from the operating system
         * such as processor.
         * <p>
         * 可运行 thread 的状态。
         * 处于 runnable 状态的 thread 正在 Java 虚拟机中执行，但它可能正在等待来自操作系统的其他资源，例如处理器。
         */
        RUNNABLE,

        /**
         * Thread state for a thread blocked waiting for a monitor lock.
         * A thread in the blocked state is waiting for a monitor lock
         * to enter a synchronized block/method or
         * reenter a synchronized block/method after calling
         * {@link Object#wait() Object.wait}.
         * <p>
         * 线程阻塞等待监视器锁的状态。
         * 处于 blocked 状态的 thread 正在等待监视器锁以进入 synchronized block/method，
         * 或在调用 Object.wait 之后重新进入 synchronized block/method。
         */
        BLOCKED,

        /**
         * Thread state for a waiting thread.
         * A thread is in the waiting state due to calling one of the
         * following methods:
         * <ul>
         *   <li>{@link Object#wait() Object.wait} with no timeout</li>
         *   <li>{@link #join() Thread.join} with no timeout</li>
         *   <li>{@link LockSupport#park() LockSupport.park}</li>
         * </ul>
         *
         * <p>A thread in the waiting state is waiting for another thread to
         * perform a particular action.
         * <p>
         * For example, a thread that has called <tt>Object.wait()</tt>
         * on an object is waiting for another thread to call
         * <tt>Object.notify()</tt> or <tt>Object.notifyAll()</tt> on
         * that object. A thread that has called <tt>Thread.join()</tt>
         * is waiting for a specified thread to terminate.
         * <p>
         *     等待 thread 的状态。
         *     由于调用以下方法之一，线程处于等待状态：
         *     - Object.wait()，没有 timeout
         *     - Thread.join()，没有 timeout
         *     - LockSupport.park()
         * <p>
         *     处于等待状态的 thread 正在等待另一个 thread 执行特定操作。
         *     例如，调用 Object.wait() 的 thread 正在等待另一个 thread 调用 Object.notify() 或 Object.notifyAll()。
         *     调用 Thread.join() 的 thread 正在等待指定的 thread 终止。
         */
        WAITING,

        /**
         * Thread state for a waiting thread with a specified waiting time.
         * A thread is in the timed waiting state due to calling one of
         * the following methods with a specified positive waiting time:
         * <ul>
         *   <li>{@link #sleep Thread.sleep}</li>
         *   <li>{@link Object#wait(long) Object.wait} with timeout</li>
         *   <li>{@link #join(long) Thread.join} with timeout</li>
         *   <li>{@link LockSupport#parkNanos LockSupport.parkNanos}</li>
         *   <li>{@link LockSupport#parkUntil LockSupport.parkUntil}</li>
         * </ul>
         * <p>
         *     带有指定等待时间的等待 thread 的状态。
         */
        TIMED_WAITING,

        /**
         * Thread state for a terminated thread.
         * The thread has completed execution.
         * <p>
         * 终止 thread 的状态。
         */
        TERMINATED;
    }

    /**
     * Returns the state of this thread.
     * This method is designed for use in monitoring of the system state,
     * not for synchronization control.
     *
     * @return this thread's state.
     * @since 1.5
     */
    public State getState() {
        // get current thread state
        return sun.misc.VM.toThreadState(threadStatus);
    }

    // Added in JSR-166

    /**
     * Interface for handlers invoked when a <tt>Thread</tt> abruptly
     * terminates due to an uncaught exception.
     * <p>When a thread is about to terminate due to an uncaught exception
     * the Java Virtual Machine will query the thread for its
     * <tt>UncaughtExceptionHandler</tt> using
     * {@link #getUncaughtExceptionHandler} and will invoke the handler's
     * <tt>uncaughtException</tt> method, passing the thread and the
     * exception as arguments.
     * If a thread has not had its <tt>UncaughtExceptionHandler</tt>
     * explicitly set, then its <tt>ThreadGroup</tt> object acts as its
     * <tt>UncaughtExceptionHandler</tt>. If the <tt>ThreadGroup</tt> object
     * has no
     * special requirements for dealing with the exception, it can forward
     * the invocation to the {@linkplain #getDefaultUncaughtExceptionHandler
     * default uncaught exception handler}.
     * <p>
     *     当 thread 由于未捕获的异常而突然终止时，调用程序将查询 thread 的 UncaughtExceptionHandler，
     *     并将调用处理程序的 uncaughtException 方法，传递 thread 和异常作为参数。
     *     如果 thread 没有显式设置其 UncaughtExceptionHandler，则其 ThreadGroup 对象充当其 UncaughtExceptionHandler。
     *     如果 ThreadGroup 对象对处理异常没有特殊要求，则可以将调用转发给默认的 uncaught exception handler。
     *
     * @see #setDefaultUncaughtExceptionHandler
     * @see #setUncaughtExceptionHandler
     * @see ThreadGroup#uncaughtException
     * @since 1.5
     */
    @FunctionalInterface
    public interface UncaughtExceptionHandler {
        /**
         * Method invoked when the given thread terminates due to the
         * given uncaught exception.
         * <p>Any exception thrown by this method will be ignored by the
         * Java Virtual Machine.
         *
         * @param t the thread
         * @param e the exception
         */
        void uncaughtException(Thread t, Throwable e);
    }

    // null unless explicitly set
    private volatile UncaughtExceptionHandler uncaughtExceptionHandler;

    // null unless explicitly set
    private static volatile UncaughtExceptionHandler defaultUncaughtExceptionHandler;

    /**
     * Set the default handler invoked when a thread abruptly terminates
     * due to an uncaught exception, and no other handler has been defined
     * for that thread.
     *
     * <p>Uncaught exception handling is controlled first by the thread, then
     * by the thread's {@link ThreadGroup} object and finally by the default
     * uncaught exception handler. If the thread does not have an explicit
     * uncaught exception handler set, and the thread's thread group
     * (including parent thread groups)  does not specialize its
     * <tt>uncaughtException</tt> method, then the default handler's
     * <tt>uncaughtException</tt> method will be invoked.
     * <p>By setting the default uncaught exception handler, an application
     * can change the way in which uncaught exceptions are handled (such as
     * logging to a specific device, or file) for those threads that would
     * already accept whatever &quot;default&quot; behavior the system
     * provided.
     *
     * <p>Note that the default uncaught exception handler should not usually
     * defer to the thread's <tt>ThreadGroup</tt> object, as that could cause
     * infinite recursion.
     *
     * @param eh the object to use as the default uncaught exception handler.
     *           If <tt>null</tt> then there is no default handler.
     * @throws SecurityException if a security manager is present and it
     *                           denies <tt>{@link RuntimePermission}
     *                           (&quot;setDefaultUncaughtExceptionHandler&quot;)</tt>
     * @see #setUncaughtExceptionHandler
     * @see #getUncaughtExceptionHandler
     * @see ThreadGroup#uncaughtException
     * @since 1.5
     */
    public static void setDefaultUncaughtExceptionHandler(UncaughtExceptionHandler eh) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(
                    new RuntimePermission("setDefaultUncaughtExceptionHandler")
            );
        }

        defaultUncaughtExceptionHandler = eh;
    }

    /**
     * Returns the default handler invoked when a thread abruptly terminates
     * due to an uncaught exception. If the returned value is <tt>null</tt>,
     * there is no default.
     *
     * @return the default uncaught exception handler for all threads
     * @see #setDefaultUncaughtExceptionHandler
     * @since 1.5
     */
    public static UncaughtExceptionHandler getDefaultUncaughtExceptionHandler() {
        return defaultUncaughtExceptionHandler;
    }

    /**
     * Returns the handler invoked when this thread abruptly terminates
     * due to an uncaught exception. If this thread has not had an
     * uncaught exception handler explicitly set then this thread's
     * <tt>ThreadGroup</tt> object is returned, unless this thread
     * has terminated, in which case <tt>null</tt> is returned.
     *
     * @return the uncaught exception handler for this thread
     * @since 1.5
     */
    public UncaughtExceptionHandler getUncaughtExceptionHandler() {
        return uncaughtExceptionHandler != null ?
                uncaughtExceptionHandler : group;
    }

    /**
     * Set the handler invoked when this thread abruptly terminates
     * due to an uncaught exception.
     * <p>A thread can take full control of how it responds to uncaught
     * exceptions by having its uncaught exception handler explicitly set.
     * If no such handler is set then the thread's <tt>ThreadGroup</tt>
     * object acts as its handler.
     *
     * @param eh the object to use as this thread's uncaught exception
     *           handler. If <tt>null</tt> then this thread has no explicit handler.
     * @throws SecurityException if the current thread is not allowed to
     *                           modify this thread.
     * @see #setDefaultUncaughtExceptionHandler
     * @see ThreadGroup#uncaughtException
     * @since 1.5
     */
    public void setUncaughtExceptionHandler(UncaughtExceptionHandler eh) {
        checkAccess();
        uncaughtExceptionHandler = eh;
    }

    /**
     * Dispatch an uncaught exception to the handler. This method is
     * intended to be called only by the JVM.
     */
    private void dispatchUncaughtException(Throwable e) {
        getUncaughtExceptionHandler().uncaughtException(this, e);
    }

    /**
     * Removes from the specified map any keys that have been enqueued
     * on the specified reference queue.
     */
    static void processQueue(ReferenceQueue<Class<?>> queue,
                             ConcurrentMap<? extends
                                     WeakReference<Class<?>>, ?> map) {
        Reference<? extends Class<?>> ref;
        while ((ref = queue.poll()) != null) {
            map.remove(ref);
        }
    }

    /**
     * Weak key for Class objects.
     **/
    static class WeakClassKey extends WeakReference<Class<?>> {
        /**
         * saved value of the referent's identity hash code, to maintain
         * a consistent hash code after the referent has been cleared
         */
        private final int hash;

        /**
         * Create a new WeakClassKey to the given object, registered
         * with a queue.
         */
        WeakClassKey(Class<?> cl, ReferenceQueue<Class<?>> refQueue) {
            super(cl, refQueue);
            hash = System.identityHashCode(cl);
        }

        /**
         * Returns the identity hash code of the original referent.
         */
        @Override
        public int hashCode() {
            return hash;
        }

        /**
         * Returns true if the given object is this identical
         * WeakClassKey instance, or, if this object's referent has not
         * been cleared, if the given object is another WeakClassKey
         * instance with the identical non-null referent as this one.
         */
        @Override
        public boolean equals(Object obj) {
            if (obj == this)
                return true;

            if (obj instanceof WeakClassKey) {
                Object referent = get();
                return (referent != null) &&
                        (referent == ((WeakClassKey) obj).get());
            } else {
                return false;
            }
        }
    }


    // The following three initially uninitialized fields are exclusively
    // managed by class java.util.concurrent.ThreadLocalRandom. These
    // fields are used to build the high-performance PRNGs in the
    // concurrent code, and we can not risk accidental false sharing.
    // Hence, the fields are isolated with @Contended.

    /**
     * The current seed for a ThreadLocalRandom
     */
    @sun.misc.Contended("tlr")
    long threadLocalRandomSeed;

    /**
     * Probe hash value; nonzero if threadLocalRandomSeed initialized
     */
    @sun.misc.Contended("tlr")
    int threadLocalRandomProbe;

    /**
     * Secondary seed isolated from public ThreadLocalRandom sequence
     */
    @sun.misc.Contended("tlr")
    int threadLocalRandomSecondarySeed;

    /* Some private helper methods */
    private native void setPriority0(int newPriority);

    private native void stop0(Object o);

    private native void suspend0();

    private native void resume0();

    private native void interrupt0();

    private native void setNativeName(String name);
}
