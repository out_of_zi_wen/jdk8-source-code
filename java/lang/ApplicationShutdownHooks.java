/*
 * Copyright (c) 2005, 2010, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

import java.util.*;

/*
 * Class to track and run user level shutdown hooks registered through
 * <tt>{@link Runtime#addShutdownHook Runtime.addShutdownHook}</tt>.
 * <p>
 * 用于跟踪和运行通过 Runtime.addShutdownHook 方式注册的用户级别的 shutdown hook 的类。
 * 即当程序正常退出时，所有通过 Runtime.addShutdownHook 方式注册的 shutdown hook 都会被执行。
 *
 * @see java.lang.Runtime#addShutdownHook
 * @see java.lang.Runtime#removeShutdownHook
 */

class ApplicationShutdownHooks {
    /* The set of registered hooks */
    // 已注册的 hooks
    private static IdentityHashMap<Thread, Thread> hooks;

    static {
        try {
            // 将实际的执行 hook 的逻辑注册到 JVM 的 shutdown 类中
            Shutdown.add(1 /* shutdown hook invocation order */, // 1 表示最高优先级
                    false /* not registered if shutdown in progress */, // 如果 shutdown 过程中，不会注册
                    new Runnable() {
                        public void run() {
                            runHooks();
                        }
                    }
            );
            hooks = new IdentityHashMap<>(); // 初始化 hooks，IdentityHashMap 类似于 HashMap，但是在比较 key 时，使用的是 == 而不是 equals
        } catch (IllegalStateException e) {
            // application shutdown hooks cannot be added if
            // shutdown is in progress.
            hooks = null;
        }
    }


    private ApplicationShutdownHooks() {
    }

    /* Add a new shutdown hook.  Checks the shutdown state and the hook itself,
     * but does not do any security checks.
     * <p>
     * 添加一个新的 shutdown hook。检查 shutdown 状态和 hook 本身，但是不做任何安全检查。
     */
    static synchronized void add(Thread hook) {
        // 简单的检查 hook
        if (hooks == null)
            throw new IllegalStateException("Shutdown in progress");

        if (hook.isAlive())
            throw new IllegalArgumentException("Hook already running");

        if (hooks.containsKey(hook))
            throw new IllegalArgumentException("Hook previously registered");

        hooks.put(hook, hook);
    }

    /* Remove a previously-registered hook.  Like the add method, this method
     * does not do any security checks.
     * <p>
     * 移除一个之前注册的 hook。和 add 方法一样，这个方法也不做任何安全检查。
     */
    static synchronized boolean remove(Thread hook) {
        if (hooks == null)
            throw new IllegalStateException("Shutdown in progress");

        if (hook == null)
            throw new NullPointerException();

        return hooks.remove(hook) != null;
    }

    /* Iterates over all application hooks creating a new thread for each
     * to run in. Hooks are run concurrently and this method waits for
     * them to finish.
     * <p>
     * 遍历所有的 application hooks，为每个 hook 创建一个新的线程来运行。
     * Hooks 是并发运行的，这个方法会等待它们完成。
     */
    static void runHooks() {
        Collection<Thread> threads;
        synchronized (ApplicationShutdownHooks.class) {
            threads = hooks.keySet();
            hooks = null;
        }

        for (Thread hook : threads) {
            hook.start();
        }
        for (Thread hook : threads) {
            while (true) {
                try {
                    hook.join();
                    break;
                } catch (InterruptedException ignored) {
                }
            }
        }
    }
}
