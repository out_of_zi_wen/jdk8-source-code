/*
 * Copyright (c) 1996, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * The {@code Byte} class wraps a value of primitive type {@code byte}
 * in an object.  An object of type {@code Byte} contains a single
 * field whose type is {@code byte}.
 * <p>
 * Byte 类将 byte 类型的原始值包装在对象中。
 * Byte 类型的对象包含一个 byte 类型的字段。
 *
 * <p>In addition, this class provides several methods for converting
 * a {@code byte} to a {@code String} and a {@code String} to a {@code
 * byte}, as well as other constants and methods useful when dealing
 * with a {@code byte}.
 * <p>
 * 此外，该类还提供了几种方法，用于将 byte 转换为 String，将 String 转换为 byte，
 * 以及在处理 byte 时有用的其他常量和方法。
 *
 * @author Nakul Saraiya
 * @author Joseph D. Darcy
 * @see java.lang.Number
 * @since JDK1.1
 */
public final class Byte extends Number implements Comparable<Byte> {

    /**
     * A constant holding the minimum value a {@code byte} can
     * have, -2<sup>7</sup>.
     * <p>
     * byte 类型的最小值，-2^7
     */
    public static final byte MIN_VALUE = -128;

    /**
     * A constant holding the maximum value a {@code byte} can
     * have, 2<sup>7</sup>-1.
     * <p>
     * byte 类型的最大值，2^7-1
     */
    public static final byte MAX_VALUE = 127;

    /**
     * The {@code Class} instance representing the primitive type
     * {@code byte}.
     * <p>
     * 表示 byte 类型的 Class 实例
     */
    @SuppressWarnings("unchecked")
    public static final Class<Byte> TYPE = (Class<Byte>) Class.getPrimitiveClass("byte");

    /**
     * Returns a new {@code String} object representing the
     * specified {@code byte}. The radix is assumed to be 10.
     * <p>
     * 返回一个表示指定 byte 的新 String 对象。假定基数为 10。
     *
     * @param b the {@code byte} to be converted
     * @return the string representation of the specified {@code byte}
     * @see java.lang.Integer#toString(int)
     */
    public static String toString(byte b) {
        return Integer.toString((int) b, 10);
    }

    private static class ByteCache {
        private ByteCache() {
        }

        // 定义一个 byte 的缓存数组，长度为 256；缓存了每一个 byte 值
        static final Byte cache[] = new Byte[-(-128) + 127 + 1];

        static {
            for (int i = 0; i < cache.length; i++)
                cache[i] = new Byte((byte) (i - 128));
        }
    }

    /**
     * Returns a {@code Byte} instance representing the specified
     * {@code byte} value.
     * <p>
     * 返回一个表示指定 byte 值的 Byte 实例。
     * <p>
     * If a new {@code Byte} instance is not required, this method
     * should generally be used in preference to the constructor
     * {@link #Byte(byte)}, as this method is likely to yield
     * significantly better space and time performance since
     * all byte values are cached.
     * <p>
     * 如果不需要新的 Byte 实例，通常应该优先使用此方法而不是构造函数 Byte(byte)，
     * 因为此方法可能会产生更好的空间和时间性能，因为所有 byte 值都被缓存。
     *
     * @param b a byte value.
     * @return a {@code Byte} instance representing {@code b}.
     * @since 1.5
     */
    public static Byte valueOf(byte b) {
        final int offset = 128;
        return ByteCache.cache[(int) b + offset];
    }

    /**
     * Parses the string argument as a signed {@code byte} in the
     * radix specified by the second argument. The characters in the
     * string must all be digits, of the specified radix (as
     * determined by whether {@link java.lang.Character#digit(char,
     * int)} returns a nonnegative value) except that the first
     * character may be an ASCII minus sign {@code '-'}
     * ({@code '\u005Cu002D'}) to indicate a negative value or an
     * ASCII plus sign {@code '+'} ({@code '\u005Cu002B'}) to
     * indicate a positive value.  The resulting {@code byte} value is
     * returned.
     * <p>
     * 将字符串参数解析为 指定进制的有符号 byte。
     * 字符串中的字符必须都是指定进制的数字（由 Character.digit(char, int) 是否返回非负值确定），
     * 但第一个字符可以是 ASCII 减号 '-'（'\u005Cu002D'）表示负值，
     * 或 ASCII 加号 '+'（'\u005Cu002B'）表示正值。
     *
     * <p>An exception of type {@code NumberFormatException} is
     * thrown if any of the following situations occurs:
     * <ul>
     * <li> The first argument is {@code null} or is a string of
     * length zero.
     *
     * <li> The radix is either smaller than {@link
     * java.lang.Character#MIN_RADIX} or larger than {@link
     * java.lang.Character#MAX_RADIX}.
     *
     * <li> Any character of the string is not a digit of the
     * specified radix, except that the first character may be a minus
     * sign {@code '-'} ({@code '\u005Cu002D'}) or plus sign
     * {@code '+'} ({@code '\u005Cu002B'}) provided that the
     * string is longer than length 1.
     *
     * <li> The value represented by the string is not a value of type
     * {@code byte}.
     * </ul>
     * <p>
     *     如果发生以下任何情况，则抛出 NumberFormatException 异常：
     *     第一个参数为 null 或长度为零的字符串。
     *     进制小于 Character.MIN_RADIX 或大于 Character.MAX_RADIX。
     *     字符串的任何字符都不是指定进制的数字，但第一个字符可以是减号 '-'（'\u005Cu002D'）或加号 '+'（'\u005Cu002B'），前提是字符串的长度大于 1。
     *
     * @param s     the {@code String} containing the
     *              {@code byte}
     *              representation to be parsed
     * @param radix the radix to be used while parsing {@code s}
     * @return the {@code byte} value represented by the string
     * argument in the specified radix
     * @throws NumberFormatException If the string does
     *                               not contain a parsable {@code byte}.
     */
    public static byte parseByte(String s, int radix)
            throws NumberFormatException {
        // 将字符串转换为 int
        int i = Integer.parseInt(s, radix);
        // 如果 int 超出 byte 的范围，抛出异常
        if (i < MIN_VALUE || i > MAX_VALUE)
            throw new NumberFormatException(
                    "Value out of range. Value:\"" + s + "\" Radix:" + radix);
        return (byte) i;
    }

    /**
     * Parses the string argument as a signed decimal {@code
     * byte}. The characters in the string must all be decimal digits,
     * except that the first character may be an ASCII minus sign
     * {@code '-'} ({@code '\u005Cu002D'}) to indicate a negative
     * value or an ASCII plus sign {@code '+'}
     * ({@code '\u005Cu002B'}) to indicate a positive value. The
     * resulting {@code byte} value is returned, exactly as if the
     * argument and the radix 10 were given as arguments to the {@link
     * #parseByte(java.lang.String, int)} method.
     *
     * @param s a {@code String} containing the
     *          {@code byte} representation to be parsed
     * @return the {@code byte} value represented by the
     * argument in decimal
     * @throws NumberFormatException if the string does not
     *                               contain a parsable {@code byte}.
     */
    public static byte parseByte(String s) throws NumberFormatException {
        return parseByte(s, 10);
    }

    /**
     * Returns a {@code Byte} object holding the value
     * extracted from the specified {@code String} when parsed
     * with the radix given by the second argument. The first argument
     * is interpreted as representing a signed {@code byte} in
     * the radix specified by the second argument, exactly as if the
     * argument were given to the {@link #parseByte(java.lang.String,
     * int)} method. The result is a {@code Byte} object that
     * represents the {@code byte} value specified by the string.
     * <p>
     * 返回一个 Byte 对象，该对象包含使用 radix 作为进制，从 s 解析得到的值。
     * 第一个参数 s 被认为是使用第二个参数 radix 进制表示的有符号 byte，就像将该参数传递给 parseByte(String, int) 方法一样。
     *
     * <p> In other words, this method returns a {@code Byte} object
     * equal to the value of:
     *
     * <blockquote>
     * {@code new Byte(Byte.parseByte(s, radix))}
     * </blockquote>
     *
     * @param s     the string to be parsed
     * @param radix the radix to be used in interpreting {@code s}
     * @return a {@code Byte} object holding the value
     * represented by the string argument in the
     * specified radix.
     * @throws NumberFormatException If the {@code String} does
     *                               not contain a parsable {@code byte}.
     */
    public static Byte valueOf(String s, int radix)
            throws NumberFormatException {
        return valueOf(parseByte(s, radix));
    }

    /**
     * Returns a {@code Byte} object holding the value
     * given by the specified {@code String}. The argument is
     * interpreted as representing a signed decimal {@code byte},
     * exactly as if the argument were given to the {@link
     * #parseByte(java.lang.String)} method. The result is a
     * {@code Byte} object that represents the {@code byte}
     * value specified by the string.
     *
     * <p> In other words, this method returns a {@code Byte} object
     * equal to the value of:
     *
     * <blockquote>
     * {@code new Byte(Byte.parseByte(s))}
     * </blockquote>
     *
     * @param s the string to be parsed
     * @return a {@code Byte} object holding the value
     * represented by the string argument
     * @throws NumberFormatException If the {@code String} does
     *                               not contain a parsable {@code byte}.
     */
    public static Byte valueOf(String s) throws NumberFormatException {
        return valueOf(s, 10);
    }

    /**
     * Decodes a {@code String} into a {@code Byte}.
     * Accepts decimal, hexadecimal, and octal numbers given by
     * the following grammar:
     *
     * <blockquote>
     * <dl>
     * <dt><i>DecodableString:</i>
     * <dd><i>Sign<sub>opt</sub> DecimalNumeral</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0x} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0X} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code #} <i>HexDigits</i>
     * <dd><i>Sign<sub>opt</sub></i> {@code 0} <i>OctalDigits</i>
     *
     * <dt><i>Sign:</i>
     * <dd>{@code -}
     * <dd>{@code +}
     * </dl>
     * </blockquote>
     * <p>
     *     将 String 解码为 Byte。
     *     接受以下语法给出的十进制、十六进制和八进制数：
     *     Sign<sub>opt</sub> DecimalNumeral
     *     Sign<sub>opt</sub> 0x HexDigits
     *     Sign<sub>opt</sub> 0X HexDigits
     *     Sign<sub>opt</sub> # HexDigits
     *     Sign<sub>opt</sub> 0 OctalDigits
     *
     * <i>DecimalNumeral</i>, <i>HexDigits</i>, and <i>OctalDigits</i>
     * are as defined in section 3.10.1 of
     * <cite>The Java&trade; Language Specification</cite>,
     * except that underscores are not accepted between digits.
     * <p>
     *     DecimalNumeral、HexDigits 和 OctalDigits 的定义如 Java 语言规范 3.10.1 节所述，
     *
     * <p>The sequence of characters following an optional
     * sign and/or radix specifier ("{@code 0x}", "{@code 0X}",
     * "{@code #}", or leading zero) is parsed as by the {@code
     * Byte.parseByte} method with the indicated radix (10, 16, or 8).
     * This sequence of characters must represent a positive value or
     * a {@link NumberFormatException} will be thrown.  The result is
     * negated if first character of the specified {@code String} is
     * the minus sign.  No whitespace characters are permitted in the
     * {@code String}.
     * <p>
     *     可选的符号和/或基数指示符（"0x"、"0X"、"#" 或前导零）后面的字符序列将按照指定的基数（10、16 或 8）由 Byte.parseByte 方法解析。
     *     这个字符序列必须表示一个正值，否则将抛出 NumberFormatException 异常。
     *     如果指定 String 的第一个字符是减号，则结果为负。
     *     String 中不允许有空格字符。
     *
     * @param nm the {@code String} to decode.
     * @return a {@code Byte} object holding the {@code byte}
     * value represented by {@code nm}
     * @throws NumberFormatException if the {@code String} does not
     *                               contain a parsable {@code byte}.
     * @see java.lang.Byte#parseByte(java.lang.String, int)
     */
    public static Byte decode(String nm) throws NumberFormatException {
        int i = Integer.decode(nm);
        if (i < MIN_VALUE || i > MAX_VALUE)
            throw new NumberFormatException(
                    "Value " + i + " out of range from input " + nm);
        return valueOf((byte) i);
    }

    /**
     * The value of the {@code Byte}.
     * <p>
     * Byte 的值。
     *
     * @serial
     */
    private final byte value;

    /**
     * Constructs a newly allocated {@code Byte} object that
     * represents the specified {@code byte} value.
     * <p>
     * 构造一个新分配的 Byte 对象，该对象表示指定的 byte 值。
     *
     * @param value the value to be represented by the
     *              {@code Byte}.
     */
    public Byte(byte value) {
        this.value = value;
    }

    /**
     * Constructs a newly allocated {@code Byte} object that
     * represents the {@code byte} value indicated by the
     * {@code String} parameter. The string is converted to a
     * {@code byte} value in exactly the manner used by the
     * {@code parseByte} method for radix 10.
     *
     * @param s the {@code String} to be converted to a
     *          {@code Byte}
     * @throws NumberFormatException If the {@code String}
     *                               does not contain a parsable {@code byte}.
     * @see java.lang.Byte#parseByte(java.lang.String, int)
     */
    public Byte(String s) throws NumberFormatException {
        this.value = parseByte(s, 10);
    }

    /**
     * Returns the value of this {@code Byte} as a
     * {@code byte}.
     */
    public byte byteValue() {
        return value;
    }

    /**
     * Returns the value of this {@code Byte} as a {@code short} after
     * a widening primitive conversion.
     *
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public short shortValue() {
        return (short) value;
    }

    /**
     * Returns the value of this {@code Byte} as an {@code int} after
     * a widening primitive conversion.
     *
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public int intValue() {
        return (int) value;
    }

    /**
     * Returns the value of this {@code Byte} as a {@code long} after
     * a widening primitive conversion.
     *
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public long longValue() {
        return (long) value;
    }

    /**
     * Returns the value of this {@code Byte} as a {@code float} after
     * a widening primitive conversion.
     *
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public float floatValue() {
        return (float) value;
    }

    /**
     * Returns the value of this {@code Byte} as a {@code double}
     * after a widening primitive conversion.
     *
     * @jls 5.1.2 Widening Primitive Conversions
     */
    public double doubleValue() {
        return (double) value;
    }

    /**
     * Returns a {@code String} object representing this
     * {@code Byte}'s value.  The value is converted to signed
     * decimal representation and returned as a string, exactly as if
     * the {@code byte} value were given as an argument to the
     * {@link java.lang.Byte#toString(byte)} method.
     * <p>
     * 返回一个表示此 Byte 值的 String 对象。
     * 该值被转换为有符号的十进制表示形式，并作为字符串返回，就像将 byte 值作为参数传递给 Byte.toString(byte) 方法一样。
     *
     * @return a string representation of the value of this object in
     * base&nbsp;10.
     */
    public String toString() {
        return Integer.toString((int) value);
    }

    /**
     * Returns a hash code for this {@code Byte}; equal to the result
     * of invoking {@code intValue()}.
     *
     * @return a hash code value for this {@code Byte}
     */
    @Override
    public int hashCode() {
        return Byte.hashCode(value);
    }

    /**
     * Returns a hash code for a {@code byte} value; compatible with
     * {@code Byte.hashCode()}.
     *
     * @param value the value to hash
     * @return a hash code value for a {@code byte} value.
     * @since 1.8
     */
    public static int hashCode(byte value) {
        return (int) value;
    }

    /**
     * Compares this object to the specified object.  The result is
     * {@code true} if and only if the argument is not
     * {@code null} and is a {@code Byte} object that
     * contains the same {@code byte} value as this object.
     * <p>
     * 将此对象与指定对象进行比较。
     * 当且仅当参数不为 null 且为包含与此对象相同 byte 值的 Byte 对象时，结果为 true。
     *
     * @param obj the object to compare with
     * @return {@code true} if the objects are the same;
     * {@code false} otherwise.
     */
    public boolean equals(Object obj) {
        if (obj instanceof Byte) {
            return value == ((Byte) obj).byteValue();
        }
        return false;
    }

    /**
     * Compares two {@code Byte} objects numerically.
     *
     * @param anotherByte the {@code Byte} to be compared.
     * @return the value {@code 0} if this {@code Byte} is
     * equal to the argument {@code Byte}; a value less than
     * {@code 0} if this {@code Byte} is numerically less
     * than the argument {@code Byte}; and a value greater than
     * {@code 0} if this {@code Byte} is numerically
     * greater than the argument {@code Byte} (signed
     * comparison).
     * @since 1.2
     */
    public int compareTo(Byte anotherByte) {
        return compare(this.value, anotherByte.value);
    }

    /**
     * Compares two {@code byte} values numerically.
     * The value returned is identical to what would be returned by:
     * <pre>
     *    Byte.valueOf(x).compareTo(Byte.valueOf(y))
     * </pre>
     *
     * @param x the first {@code byte} to compare
     * @param y the second {@code byte} to compare
     * @return the value {@code 0} if {@code x == y};
     * a value less than {@code 0} if {@code x < y}; and
     * a value greater than {@code 0} if {@code x > y}
     * @since 1.7
     */
    public static int compare(byte x, byte y) {
        return x - y;
    }

    /**
     * Converts the argument to an {@code int} by an unsigned
     * conversion.  In an unsigned conversion to an {@code int}, the
     * high-order 24 bits of the {@code int} are zero and the
     * low-order 8 bits are equal to the bits of the {@code byte} argument.
     * <p>
     * 将参数转换为 int 类型的无符号值。
     * 在将 byte 转换为 int 时，int 的高 24 位为零，低 8 位等于 byte 参数的位。
     * <p>
     * Consequently, zero and positive {@code byte} values are mapped
     * to a numerically equal {@code int} value and negative {@code
     * byte} values are mapped to an {@code int} value equal to the
     * input plus 2<sup>8</sup>.
     * <p>
     * 因此，零和正 byte 值被映射为数值相等的 int 值，负 byte 值被映射为等于输入加 2^8 的 int 值。
     *
     * @param x the value to convert to an unsigned {@code int}
     * @return the argument converted to {@code int} by an unsigned
     * conversion
     * @since 1.8
     */
    public static int toUnsignedInt(byte x) {
        return ((int) x) & 0xff;
    }

    /**
     * Converts the argument to a {@code long} by an unsigned
     * conversion.  In an unsigned conversion to a {@code long}, the
     * high-order 56 bits of the {@code long} are zero and the
     * low-order 8 bits are equal to the bits of the {@code byte} argument.
     * <p>
     * Consequently, zero and positive {@code byte} values are mapped
     * to a numerically equal {@code long} value and negative {@code
     * byte} values are mapped to a {@code long} value equal to the
     * input plus 2<sup>8</sup>.
     *
     * @param x the value to convert to an unsigned {@code long}
     * @return the argument converted to {@code long} by an unsigned
     * conversion
     * @since 1.8
     */
    public static long toUnsignedLong(byte x) {
        return ((long) x) & 0xffL;
    }


    /**
     * The number of bits used to represent a {@code byte} value in two's
     * complement binary form.
     * <p>
     * 用二进制补码形式表示 byte 值所用的位数。
     *
     * @since 1.5
     */
    public static final int SIZE = 8;

    /**
     * The number of bytes used to represent a {@code byte} value in two's
     * complement binary form.
     *
     * @since 1.8
     */
    public static final int BYTES = SIZE / Byte.SIZE;

    /**
     * use serialVersionUID from JDK 1.1. for interoperability
     */
    private static final long serialVersionUID = -7183698231559129828L;
}
