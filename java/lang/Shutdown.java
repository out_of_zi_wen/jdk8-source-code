/*
 * Copyright (c) 1999, 2022, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

/**
 * Package-private utility class containing data structures and logic
 * governing the virtual-machine shutdown sequence.
 * <p>
 * Package-private 的工具类，包含了虚拟机 shutdown 时的数据结构和逻辑。
 *
 * @author Mark Reinhold
 * @see java.io.Console
 * @see ApplicationShutdownHooks
 * @see java.io.DeleteOnExitHook
 * @since 1.3
 */

class Shutdown {

    // The system shutdown hooks are registered with a predefined slot.
    // The list of shutdown hooks is as follows:
    // (0) Console restore hook
    // (1) ApplicationShutdownHooks that invokes all registered application
    //     shutdown hooks and waits until they finish
    // (2) DeleteOnExit hook
    /*
     * 系统 shutdown hook 注册在一个预定义的 slot 中。
     * shutdown hook 的列表如下：
     * (0) Console restore hook
     * (1) ApplicationShutdownHooks that invokes all registered application
     * (2) DeleteOnExit hook
     */
    private static final int MAX_SYSTEM_HOOKS = 10;
    private static final Runnable[] hooks = new Runnable[MAX_SYSTEM_HOOKS];

    // the index of the currently running shutdown hook to the hooks array
    // 当前正在执行的 shutdown hook 的 slot 位置
    private static int currentRunningHook = -1;

    // track whether we have already (commenced) shutdown
    // 是否完成了 shutdown 流程（调用完了 hook）
    private static boolean isShutdown;

    /* The preceding static fields are protected by this lock */
    // 用于保护上面的静态字段，也就是说，上面的静态字段都是线程安全的，每一次变更都需要加锁
    private static class Lock {
    }

    ;
    private static Object lock = new Lock();

    /* Lock object for the native halt method */
    // 用于保护 native halt 方法的锁
    private static Object haltLock = new Lock();

    /**
     * Add a new system shutdown hook.  Checks the shutdown state and
     * the hook itself, but does not do any security checks.
     * <p>
     * 添加一个新的系统 shutdown hook。检查 shutdown 状态和 hook 本身，但是不做任何安全检查。
     * <p>
     * <p>
     * The registerShutdownInProgress parameter should be false except
     * registering the DeleteOnExitHook since the first file may
     * be added to the delete on exit list by the application shutdown
     * hooks.
     * <p>
     * registerShutdownInProgress 参数应该为 false，
     * 除非注册 DeleteOnExitHook，因为第一个 slot 可能会被 application shutdown hook 添加到 delete on exit 列表中。
     *
     * @param slot                       the slot in the shutdown hook array, whose element
     *                                   will be invoked in order during shutdown
     * @param registerShutdownInProgress true to allow the hook
     *                                   to be registered even if the shutdown is in progress.
     * @param hook                       the hook to be registered
     * @throws IllegalStateException if registerShutdownInProgress is false and shutdown is in progress; or
     *                               if registerShutdownInProgress is true and the shutdown process
     *                               already passes the given slot
     */
    static void add(int slot, boolean registerShutdownInProgress, Runnable hook) {
        // 验证 slot 的合法性，即必须能够在 hook 数组中找到对应的位置
        if (slot < 0 || slot >= MAX_SYSTEM_HOOKS) {
            throw new IllegalArgumentException("Invalid slot: " + slot);
        }
        // 加锁，保证线程安全
        synchronized (lock) {
            // 如果待加入的位置处，已经有 hook 了，抛出异常
            if (hooks[slot] != null)
                throw new InternalError("Shutdown hook at slot " + slot + " already registered");

            // 如果不允许在 shutdown 过程中注册 hook，那么只要开始调用 hook 了，就不允许再注册 hook
            if (!registerShutdownInProgress) {
                if (currentRunningHook >= 0)
                    throw new IllegalStateException("Shutdown in progress");
            } else {
                // 如果允许在 shutdown 过程中注册 hook，那么待加入的位置必须大于当前正在执行的 hook 的位置，即必须保证它能被执行到
                if (isShutdown || slot <= currentRunningHook)
                    throw new IllegalStateException("Shutdown in progress");
            }

            // 加入 hook
            hooks[slot] = hook;
        }
    }

    /* Run all system shutdown hooks.
     * <p>
     *     执行所有的系统 shutdown hook。
     *
     * The system shutdown hooks are run in the thread synchronized on
     * Shutdown.class.  Other threads calling Runtime::exit, Runtime::halt
     * or JNI DestroyJavaVM will block indefinitely.
     * <p>
     *     系统 shutdown hook 在 Shutdown.class 上同步执行。
     *    其他线程调用 Runtime::exit, Runtime::halt 或者 JNI DestroyJavaVM 会无限期的阻塞。
     *
     * ApplicationShutdownHooks is registered as one single hook that starts
     * all application shutdown hooks and waits until they finish.
     * <p>
     *     ApplicationShutdownHooks 被注册为一个单一的 hook，它会启动所有的 application shutdown hook，并且等待它们完成。
     */
    private static void runHooks() {
        synchronized (lock) {
            /* Guard against the possibility of a daemon thread invoking exit
             * after DestroyJavaVM initiates the shutdown sequence
             */
            // 防止一个 daemon 线程在 DestroyJavaVM 开始 shutdown 流程之后调用 exit
            if (isShutdown) return;
        }

        for (int i = 0; i < MAX_SYSTEM_HOOKS; i++) {
            try {
                Runnable hook;
                synchronized (lock) {
                    // acquire the lock to make sure the hook registered during
                    // shutdown is visible here.
                    currentRunningHook = i;
                    hook = hooks[i];
                }
                //  这里是调用了 run 方法，而不是 start 方法，所以是同步执行的
                if (hook != null) hook.run();
            } catch (Throwable t) {
                if (t instanceof ThreadDeath) {
                    ThreadDeath td = (ThreadDeath) t;
                    throw td;
                }
            }
        }

        // set shutdown state
        // Synchronization is for visibility; only one thread
        // can ever get here.
        // 设置 shutdown 状态
        // 同步是为了可见性；只有一个线程能够到达这里
        synchronized (lock) {
            isShutdown = true;
        }
    }

    /* Notify the VM that it's time to halt. */
    static native void beforeHalt();

    /* The halt method is synchronized on the halt lock
     * to avoid corruption of the delete-on-shutdown file list.
     * It invokes the true native halt method.
     * <p>
     *     halt 方法在 halt lock 上同步，以避免 delete-on-shutdown 文件列表的损坏。
     * 其实就是终止 VM。
     */
    static void halt(int status) {
        synchronized (haltLock) {
            halt0(status);
        }
    }

    static native void halt0(int status);

    /* Invoked by Runtime.exit, which does all the security checks.
     * Also invoked by handlers for system-provided termination events,
     * which should pass a nonzero status code.
     * <p>
     *     Runtime.exit 调用，它做了所有的安全检查。
     *    也被系统提供的终止事件的处理程序调用，它应该传递一个非零的状态码。
     */
    static void exit(int status) {
        synchronized (lock) {
            // 如果已经开始 shutdown 或者状态异常，那么直接终止
            if (status != 0 && isShutdown) {
                /* Halt immediately on nonzero status */
                halt(status);
            }
        }
        // 能执行 hook 的前提是 status 为 0，并且是第一次 shutdown
        synchronized (Shutdown.class) {
            /* Synchronize on the class object, causing any other thread
             * that attempts to initiate shutdown to stall indefinitely
             */
            beforeHalt();
            runHooks();
            halt(status);
        }
    }


    /* Invoked by the JNI DestroyJavaVM procedure when the last non-daemon
     * thread has finished.  Unlike the exit method, this method does not
     * actually halt the VM.
     * <p>
     *     当最后一个非 daemon 线程结束时，JNI DestroyJavaVM 过程调用。
     *   与 exit 方法不同，这个方法不会真正的 halt VM。
     * 为什么这里不判断 isShutdown，而是直接调用 runHooks 呢？可能是 runHooks 里面会判断 isShutdown，所以这里不用判断了
     */
    static void shutdown() {
        synchronized (Shutdown.class) {
            runHooks();
        }
    }

}
