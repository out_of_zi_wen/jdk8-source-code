/*
 * Copyright (c) 1994, 2020, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.io.ObjectStreamField;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Formatter;
import java.util.Locale;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * The {@code String} class represents character strings. All
 * string literals in Java programs, such as {@code "abc"}, are
 * implemented as instances of this class.
 * <p>
 * String 类代表了字符串。Java 程序中的所有字符串字面量，如 {@code "abc"}，都是该类的实例。
 * <p>
 * Strings are constant; their values cannot be changed after they
 * are created. String buffers support mutable strings.
 * <p>
 * 字符串是常量；它们的值在创建后不能被更改。StringBuffer 等类支持可变的字符串。
 * <p>
 * Because String objects are immutable they can be shared. For example:
 * <p>
 * 因为字符串是不可变的，所以它们可以被共享。例如：
 * <blockquote><pre>
 *     String str = "abc";
 * </pre></blockquote><p>
 * is equivalent to:
 * <blockquote><pre>
 *     char data[] = {'a', 'b', 'c'};
 *     String str = new String(data);
 * </pre></blockquote><p>
 * <p>
 * Here are some more examples of how strings can be used:
 * <blockquote><pre>
 *     System.out.println("abc");
 *     String cde = "cde";
 *     System.out.println("abc" + cde);
 *     String c = "abc".substring(2,3);
 *     String d = cde.substring(1, 2);
 * </pre></blockquote>
 * <p>
 * The class {@code String} includes methods for examining
 * individual characters of the sequence, for comparing strings, for
 * searching strings, for extracting substrings, and for creating a
 * copy of a string with all characters translated to uppercase or to
 * lowercase. Case mapping is based on the Unicode Standard version
 * specified by the {@link java.lang.Character Character} class.
 * <p>
 * The Java language provides special support for the string
 * concatenation operator (&nbsp;+&nbsp;), and for conversion of
 * other objects to strings. String concatenation is implemented
 * through the {@code StringBuilder}(or {@code StringBuffer})
 * class and its {@code append} method.
 * <p>
 * Java针对字符串连接操作符（+）和其他对象自动转换为字符串提供了特殊的支持。
 * 字符串连接通过 StringBuilder（或 StringBuffer）类及其 append 方法实现。
 * <p>
 * String conversions are implemented through the method
 * {@code toString}, defined by {@code Object} and
 * inherited by all classes in Java. For additional information on
 * string concatenation and conversion, see Gosling, Joy, and Steele,
 * <i>The Java Language Specification</i>.
 * <p>
 * 字符串转换通过 toString 方法实现，该方法由 Object 类定义，并由 Java 中的所有类继承。
 *
 * <p> Unless otherwise noted, passing a <tt>null</tt> argument to a constructor
 * or method in this class will cause a {@link NullPointerException} to be
 * thrown.
 * <p>
 * 除非另有说明，否则将 null 参数传递给此类中的构造函数或方法将导致抛出 NullPointerException。
 *
 * <p>A {@code String} represents a string in the UTF-16 format
 * in which <em>supplementary characters</em> are represented by <em>surrogate
 * pairs</em> (see the section <a href="Character.html#unicode">Unicode
 * Character Representations</a> in the {@code Character} class for
 * more information).
 * <p>
 * String 表示 UTF-16 格式的字符串，其中补充字符（supplementary characters）由 surrogate pairs表示（有关更多信息，请参见 Character 类中的 Unicode 字符表示部分）。
 * <p>
 * Index values refer to {@code char} code units, so a supplementary
 * character uses two positions in a {@code String}.
 * <p>
 * 索引值指的是 char 代码单元，因此补充字符在 String 中使用两个位置。
 * <p>The {@code String} class provides methods for dealing with
 * Unicode code points (i.e., characters), in addition to those for
 * dealing with Unicode code units (i.e., {@code char} values).
 * <p>
 * String 类提供了处理 Unicode 代码点（即字符）的方法，除了处理 Unicode 代码单元（即 char 值）的方法。
 * String 提供了处理 char 值和码点的两套方法
 * <p>
 *
 * @author Lee Boynton
 * @author Arthur van Hoff
 * @author Martin Buchholz
 * @author Ulf Zibis
 * @see java.lang.Object#toString()
 * @see java.lang.StringBuffer
 * @see java.lang.StringBuilder
 * @see java.nio.charset.Charset
 * @since JDK1.0
 */

public final class String
        implements java.io.Serializable, Comparable<String>, CharSequence {
    /**
     * The value is used for character storage.
     * <p>
     * char 类型的 value 数组是用于字符存储的；
     * 同时一个 char 值已经能代表一个 Unicode 字符了，所以也能用来存取 Unicode 码点
     */
    private final char value[];

    /**
     * Cache the hash code for the string
     * <p>
     * 缓存字符串的 hash 值；能够在尝试获取字符串的 hash 值时，直接返回该值，而不需要重新计算；
     * 由于字符串是不可变的，所以这个值也是不可变的，可以直接缓存；
     * 一旦 string 值发生了变更（eg：append 了一个另一个字符串），那么就会创建出一个新的对象，这个新对象的 hash 值也会被重新计算
     */
    private int hash; // Default to 0

    /**
     * use serialVersionUID from JDK 1.0.2 for interoperability
     * <p>
     * 用于兼容 JDK 1.0.2 的序列化版本号
     */
    private static final long serialVersionUID = -6849794470754667710L;

    /**
     * Class String is special cased within the Serialization Stream Protocol.
     * <p>
     * A String instance is written into an ObjectOutputStream according to
     * <a href="{@docRoot}/../platform/serialization/spec/output.html">
     * Object Serialization Specification, Section 6.2, "Stream Elements"</a>
     * <p>
     * String 类在序列化流协议中是特殊的；
     * 这里定义了一个 ObjectStreamField 数组，ObjectStreamField 是 jdk 内置的一个类，用于描述一个类的序列化字段；
     * 在jdk的string类中，可能不需要使用到 serialPersistentFields，因此提供一个空数组作为默认值。
     * <p>
     * 具体来说，serialPersistentFields数组是在对象流（Object Stream）的序列化和反序列化过程中使用的。
     * 当对象需要被序列化时，它会提供关于该对象的字段信息，包括字段名和字段类型等。而在反序列化时，这些字段信息会被用来恢复对象的状态。
     * <p>
     * 但是，如果对象的字段信息发生了变化，那么反序列化过程中就会出现问题，因为反序列化时使用的字段信息和序列化时使用的字段信息不一致。
     * 为了解决这个问题，可以使用serialPersistentFields数组来显式地指定需要序列化的字段，这样，即使对象的字段信息发生了变化，也不会影响反序列化过程。
     * <p>
     * 也就是说，它的作用和 transient 关键字类似，都是用来指定不需要序列化的字段。或者说和 @JsonIgnore 一样，用来指定不需要序列化的字段。
     */
    private static final ObjectStreamField[] serialPersistentFields =
            new ObjectStreamField[0];

    /**
     * Initializes a newly created {@code String} object so that it represents
     * an empty character sequence.  Note that use of this constructor is
     * unnecessary since Strings are immutable.
     * <p>
     * 初始化一个新创建的 String 对象，使其表示一个空字符序列。
     * 这里的 "".value 是一个静态导入，它导入了 String 类中的 value 属性。
     * 静态导入用于直接导入类中的静态成员，这样就无需在使用该成员时指定类名。这种方式可以使代码更简洁、更易读。
     * 此时没有给 hash 赋值，因此默认就是 0。
     */
    public String() {
        this.value = "".value;
    }

    /**
     * Initializes a newly created {@code String} object so that it represents
     * the same sequence of characters as the argument; in other words, the
     * newly created string is a copy of the argument string. Unless an
     * explicit copy of {@code original} is needed, use of this constructor is
     * unnecessary since Strings are immutable.
     * <p>
     * 初始化一个新创建的 String 对象，使其表示与参数相同的字符序列；
     * 换句话说，新创建的字符串是参数字符串的副本。
     * 除非需要显式地复制原始字符串，否则无需使用此构造函数，因为字符串是不可变的。
     * <p>
     * 一般我们创建拷贝的原因是希望能够将副本和原对象分开修改，彼此不覆盖，或者溯源回去。
     * 这里因为 string 本身就是不可变的，所以很多时候并不需要创建一个副本，直接使用原对象即可。
     * <p>
     * String s1 = "abc";
     * String s2 = s1;
     * s1 = "def";
     * s2 == "abc"; // true
     *
     * @param original A {@code String}
     */
    public String(String original) {
        this.value = original.value;
        this.hash = original.hash;
    }

    /**
     * Allocates a new {@code String} so that it represents the sequence of
     * characters currently contained in the character array argument. The
     * contents of the character array are copied; subsequent modification of
     * the character array does not affect the newly created string.
     * <p>
     * 分配一个新的 String，它将代表当前包含在字符数组参数中的字符序列。
     * 字符数组的内容被复制；因此，对原入参字符数组的后续修改不会影响新创建的字符串。
     *
     * @param value The initial value of the string
     */
    public String(char value[]) {
        this.value = Arrays.copyOf(value, value.length);
    }

    /**
     * Allocates a new {@code String} that contains characters from a subarray
     * of the character array argument. The {@code offset} argument is the
     * index of the first character of the subarray and the {@code count}
     * argument specifies the length of the subarray. The contents of the
     * subarray are copied; subsequent modification of the character array does
     * not affect the newly created string.
     * <p>
     * 分配一个新的 String，它包含入参字符数组参数的子数组中的字符。
     * offset 参数是子数组的将被拷贝的第一个字符的索引，count 参数指定从该第一个字符开始需要往后拷贝多少字符。
     * 子数组的内容将被拷贝；因此，对原入参字符数组的后续修改不会影响新创建的字符串。
     *
     * @param value  Array that is the source of characters
     * @param offset The initial offset
     * @param count  The length
     * @throws IndexOutOfBoundsException If the {@code offset} and {@code count} arguments index
     *                                   characters outside the bounds of the {@code value} array
     */
    public String(char value[], int offset, int count) {
        // 检查入参 offset 和 count 是否合法
        if (offset < 0) {
            throw new StringIndexOutOfBoundsException(offset);
        }
        if (count <= 0) {
            if (count < 0) {
                throw new StringIndexOutOfBoundsException(count);
            }
            // 这里的前提是 count == 0，因此得到的 value 一定是空字符串；
            // 但是这里的 offset 也可能是大于 value.length 的，因此需要再次检查
            if (offset <= value.length) {
                this.value = "".value;
                return;
            }
        }
        // Note: offset or count might be near -1>>>1.
        // 这里的前提是 offset 和 count 都是合法的
        // 但是 offset + count 可能会溢出，因此采用的比较方式是 offset > value.length - count
        if (offset > value.length - count) {
            throw new StringIndexOutOfBoundsException(offset + count);
        }
        this.value = Arrays.copyOfRange(value, offset, offset + count);
    }

    /**
     * Allocates a new {@code String} that contains characters from a subarray
     * of the <a href="Character.html#unicode">Unicode code point</a> array
     * argument.  The {@code offset} argument is the index of the first code
     * point of the subarray and the {@code count} argument specifies the
     * length of the subarray.  The contents of the subarray are converted to
     * {@code char}s; subsequent modification of the {@code int} array does not
     * affect the newly created string.
     * <p>
     * 分配一个新的 String，它包含来自 Unicode 代码点数组入参的子数组的字符。
     * offset 参数是子数组的第一个代码点的索引，count 参数指定子数组的长度。
     * 子数组的内容被转换为 char；因此，对 int 数组的后续修改不会影响新创建的字符串。
     *
     * @param codePoints Array that is the source of Unicode code points
     * @param offset     The initial offset
     * @param count      The length
     * @throws IllegalArgumentException  If any invalid Unicode code point is found in {@code
     *                                   codePoints}
     * @throws IndexOutOfBoundsException If the {@code offset} and {@code count} arguments index
     *                                   characters outside the bounds of the {@code codePoints} array
     * @since 1.5
     */
    public String(int[] codePoints, int offset, int count) {
        if (offset < 0) {
            throw new StringIndexOutOfBoundsException(offset);
        }
        if (count <= 0) {
            if (count < 0) {
                throw new StringIndexOutOfBoundsException(count);
            }
            // 这里的前提是 count == 0
            if (offset <= codePoints.length) {
                this.value = "".value;
                return;
            }
        }
        // Note: offset or count might be near -1>>>1.
        // 避免 offset + count 溢出
        if (offset > codePoints.length - count) {
            throw new StringIndexOutOfBoundsException(offset + count);
        }

        final int end = offset + count;

        // Pass 1: Compute precise size of char[]
        int n = count;
        for (int i = offset; i < end; i++) {
            int c = codePoints[i];
            if (Character.isBmpCodePoint(c))
                // BMP（基本字符集） 码点只需要一个 char 来表示，因此这里不需要再加 1
                continue;
            else if (Character.isValidCodePoint(c))
                // 原先已经设置 n = count， 而一般一个码点需要两个 char （surrogates pair）来表示，因此这里需要再加 1
                n++;
            else throw new IllegalArgumentException(Integer.toString(c));
        }

        // Pass 2: Allocate and fill in char[]
        final char[] v = new char[n];

        for (int i = offset, j = 0; i < end; i++, j++) {
            int c = codePoints[i];
            if (Character.isBmpCodePoint(c))
                // BMP 码点只需要一个 char 来表示
                v[j] = (char) c;
            else
                // 非 BMP 码点需要两个 char 来表示
                Character.toSurrogates(c, v, j++);
        }

        this.value = v;
    }

    /**
     * Allocates a new {@code String} constructed from a subarray of an array
     * of 8-bit integer values.
     *
     * <p> The {@code offset} argument is the index of the first byte of the
     * subarray, and the {@code count} argument specifies the length of the
     * subarray.
     *
     * <p> Each {@code byte} in the subarray is converted to a {@code char} as
     * specified in the method above.
     *
     * @param ascii  The bytes to be converted to characters
     * @param hibyte The top 8 bits of each 16-bit Unicode code unit
     * @param offset The initial offset
     * @param count  The length
     * @throws IndexOutOfBoundsException If the {@code offset} or {@code count} argument is invalid
     * @see #String(byte[], int)
     * @see #String(byte[], int, int, java.lang.String)
     * @see #String(byte[], int, int, java.nio.charset.Charset)
     * @see #String(byte[], int, int)
     * @see #String(byte[], java.lang.String)
     * @see #String(byte[], java.nio.charset.Charset)
     * @see #String(byte[])
     * @deprecated This method does not properly convert bytes into characters.
     * As of JDK&nbsp;1.1, the preferred way to do this is via the
     * {@code String} constructors that take a {@link
     * java.nio.charset.Charset}, charset name, or that use the platform's
     * default charset.
     */
    @Deprecated
    public String(byte ascii[], int hibyte, int offset, int count) {
        checkBounds(ascii, offset, count);
        char value[] = new char[count];

        if (hibyte == 0) {
            for (int i = count; i-- > 0; ) {
                value[i] = (char) (ascii[i + offset] & 0xff);
            }
        } else {
            hibyte <<= 8;
            for (int i = count; i-- > 0; ) {
                value[i] = (char) (hibyte | (ascii[i + offset] & 0xff));
            }
        }
        this.value = value;
    }

    /**
     * Allocates a new {@code String} containing characters constructed from
     * an array of 8-bit integer values. Each character <i>c</i>in the
     * resulting string is constructed from the corresponding component
     * <i>b</i> in the byte array such that:
     *
     * <blockquote><pre>
     *     <b><i>c</i></b> == (char)(((hibyte &amp; 0xff) &lt;&lt; 8)
     *                         | (<b><i>b</i></b> &amp; 0xff))
     * </pre></blockquote>
     *
     * @param ascii  The bytes to be converted to characters
     * @param hibyte The top 8 bits of each 16-bit Unicode code unit
     * @see #String(byte[], int, int, java.lang.String)
     * @see #String(byte[], int, int, java.nio.charset.Charset)
     * @see #String(byte[], int, int)
     * @see #String(byte[], java.lang.String)
     * @see #String(byte[], java.nio.charset.Charset)
     * @see #String(byte[])
     * @deprecated This method does not properly convert bytes into
     * characters.  As of JDK&nbsp;1.1, the preferred way to do this is via the
     * {@code String} constructors that take a {@link
     * java.nio.charset.Charset}, charset name, or that use the platform's
     * default charset.
     */
    @Deprecated
    public String(byte ascii[], int hibyte) {
        this(ascii, hibyte, 0, ascii.length);
    }

    /* Common private utility method used to bounds check the byte array
     * and requested offset & length values used by the String(byte[],..)
     * constructors.
     */
    private static void checkBounds(byte[] bytes, int offset, int length) {
        if (length < 0)
            throw new StringIndexOutOfBoundsException(length);
        if (offset < 0)
            throw new StringIndexOutOfBoundsException(offset);
        if (offset > bytes.length - length)
            throw new StringIndexOutOfBoundsException(offset + length);
    }

    /**
     * Constructs a new {@code String} by decoding the specified subarray of
     * bytes using the specified charset.  The length of the new {@code String}
     * is a function of the charset, and hence may not be equal to the length
     * of the subarray.
     * <p>
     * 通过使用指定的字符集解码指定的字节数组的子数组来构造新的 String。
     * 新 String 的长度是字符集的函数，因此可能不等于子数组的长度。
     *
     * <p> The behavior of this constructor when the given bytes are not valid
     * in the given charset is unspecified.  The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     * <p>
     * 当给定的字节在给定的字符集中无效时(实际编码格式和给定的格式不一致)，此构造函数的行为是不确定的。
     * 因此如果希望更好的控制 decode 行为，应使用 CharsetDecoder 类。
     *
     * @param bytes       The bytes to be decoded into characters
     * @param offset      The index of the first byte to decode
     * @param length      The number of bytes to decode
     * @param charsetName The name of a supported {@linkplain java.nio.charset.Charset
     *                    charset}
     * @throws UnsupportedEncodingException If the named charset is not supported
     * @throws IndexOutOfBoundsException    If the {@code offset} and {@code length} arguments index
     *                                      characters outside the bounds of the {@code bytes} array
     * @since JDK1.1
     */
    public String(byte bytes[], int offset, int length, String charsetName)
            throws UnsupportedEncodingException {
        if (charsetName == null)
            throw new NullPointerException("charsetName");
        checkBounds(bytes, offset, length);
        // StringCoding.decode 方法较为低效
        this.value = StringCoding.decode(charsetName, bytes, offset, length);
    }

    /**
     * Constructs a new {@code String} by decoding the specified subarray of
     * bytes using the specified {@linkplain java.nio.charset.Charset charset}.
     * The length of the new {@code String} is a function of the charset, and
     * hence may not be equal to the length of the subarray.
     *
     * <p> This method always replaces malformed-input and unmappable-character
     * sequences with this charset's default replacement string.  The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     *
     * @param bytes   The bytes to be decoded into characters
     * @param offset  The index of the first byte to decode
     * @param length  The number of bytes to decode
     * @param charset The {@linkplain java.nio.charset.Charset charset} to be used to
     *                decode the {@code bytes}
     * @throws IndexOutOfBoundsException If the {@code offset} and {@code length} arguments index
     *                                   characters outside the bounds of the {@code bytes} array
     * @since 1.6
     */
    public String(byte bytes[], int offset, int length, Charset charset) {
        if (charset == null)
            throw new NullPointerException("charset");
        checkBounds(bytes, offset, length);
        this.value = StringCoding.decode(charset, bytes, offset, length);
    }

    /**
     * Constructs a new {@code String} by decoding the specified array of bytes
     * using the specified {@linkplain java.nio.charset.Charset charset}.  The
     * length of the new {@code String} is a function of the charset, and hence
     * may not be equal to the length of the byte array.
     *
     * <p> The behavior of this constructor when the given bytes are not valid
     * in the given charset is unspecified.  The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     *
     * @param bytes       The bytes to be decoded into characters
     * @param charsetName The name of a supported {@linkplain java.nio.charset.Charset
     *                    charset}
     * @throws UnsupportedEncodingException If the named charset is not supported
     * @since JDK1.1
     */
    public String(byte bytes[], String charsetName)
            throws UnsupportedEncodingException {
        this(bytes, 0, bytes.length, charsetName);
    }

    /**
     * Constructs a new {@code String} by decoding the specified array of
     * bytes using the specified {@linkplain java.nio.charset.Charset charset}.
     * The length of the new {@code String} is a function of the charset, and
     * hence may not be equal to the length of the byte array.
     *
     * <p> This method always replaces malformed-input and unmappable-character
     * sequences with this charset's default replacement string.  The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     *
     * @param bytes   The bytes to be decoded into characters
     * @param charset The {@linkplain java.nio.charset.Charset charset} to be used to
     *                decode the {@code bytes}
     * @since 1.6
     */
    public String(byte bytes[], Charset charset) {
        this(bytes, 0, bytes.length, charset);
    }

    /**
     * Constructs a new {@code String} by decoding the specified subarray of
     * bytes using the platform's default charset.  The length of the new
     * {@code String} is a function of the charset, and hence may not be equal
     * to the length of the subarray.
     *
     * <p> The behavior of this constructor when the given bytes are not valid
     * in the default charset is unspecified.  The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     *
     * @param bytes  The bytes to be decoded into characters
     * @param offset The index of the first byte to decode
     * @param length The number of bytes to decode
     * @throws IndexOutOfBoundsException If the {@code offset} and the {@code length} arguments index
     *                                   characters outside the bounds of the {@code bytes} array
     * @since JDK1.1
     */
    public String(byte bytes[], int offset, int length) {
        checkBounds(bytes, offset, length);
        this.value = StringCoding.decode(bytes, offset, length);
    }

    /**
     * Constructs a new {@code String} by decoding the specified array of bytes
     * using the platform's default charset.  The length of the new {@code
     * String} is a function of the charset, and hence may not be equal to the
     * length of the byte array.
     *
     * <p> The behavior of this constructor when the given bytes are not valid
     * in the default charset is unspecified.  The {@link
     * java.nio.charset.CharsetDecoder} class should be used when more control
     * over the decoding process is required.
     *
     * @param bytes The bytes to be decoded into characters
     * @since JDK1.1
     */
    public String(byte bytes[]) {
        this(bytes, 0, bytes.length);
    }

    /**
     * Allocates a new string that contains the sequence of characters
     * currently contained in the string buffer argument. The contents of the
     * string buffer are copied; subsequent modification of the string buffer
     * does not affect the newly created string.
     * <p>
     * 分配一个新的字符串，它包含入参 string buffer 中的字符序列。
     * string buffer 中的字符序列将被拷贝；因此，对原 string buffer 的后续修改不会影响新创建的字符串。
     *
     * @param buffer A {@code StringBuffer}
     */
    public String(StringBuffer buffer) {
        // 因为 StringBuffer 是线程安全的，所以这里也需要遵守线程安全的规则，需要加 synchronized
        synchronized (buffer) {
            this.value = Arrays.copyOf(buffer.getValue(), buffer.length());
        }
    }

    /**
     * Allocates a new string that contains the sequence of characters
     * currently contained in the string builder argument. The contents of the
     * string builder are copied; subsequent modification of the string builder
     * does not affect the newly created string.
     * <p>
     * 分配一个新的字符串，它包含入参 string builder 中的字符序列。
     * string builder 中的字符序列将被拷贝；因此，对原 string builder 的后续修改不会影响新创建的字符串。
     *
     * <p> This constructor is provided to ease migration to {@code
     * StringBuilder}. Obtaining a string from a string builder via the {@code
     * toString} method is likely to run faster and is generally preferred.
     *
     * <p>
     * 这个构造函数是为了方便迁移到 StringBuilder 而提供的。
     * 通过 toString 方法从 string builder 中获取字符串可能会运行得更快，并且通常更受欢迎。
     *
     * @param builder A {@code StringBuilder}
     * @since 1.5
     */
    public String(StringBuilder builder) {
        this.value = Arrays.copyOf(builder.getValue(), builder.length());
    }

    /*
     * Package private constructor which shares value array for speed.
     * this constructor is always expected to be called with share==true.
     * a separate constructor is needed because we already have a public
     * String(char[]) constructor that makes a copy of the given char[].
     * <p>
     * 用于提高速度的共享 value 数组的包私有构造函数。
     * 此构造函数始终带有 share==true 调用。
     * 需要单独的构造函数，因为我们已经有了一个公共的 String(char[]) 构造函数，它会复制给定的 char[]。
     */
    String(char[] value, boolean share) {
        // assert share : "unshared not supported";
        this.value = value;
    }

    /**
     * Returns the length of this string.
     * The length is equal to the number of <a href="Character.html#unicode">Unicode
     * code units</a> in the string.
     * <p>
     * 返回此字符串的长度。
     * 长度等于字符串中的 Unicode 代码单元数。即码点数量。（这是由构造函数保证的）
     *
     * @return the length of the sequence of characters represented by this
     * object.
     */
    public int length() {
        return value.length;
    }

    /**
     * Returns {@code true} if, and only if, {@link #length()} is {@code 0}.
     * <p>
     * 当且仅当 length() == 0，返回 true，否则返回 false
     *
     * @return {@code true} if {@link #length()} is {@code 0}, otherwise
     * {@code false}
     * @since 1.6
     */
    public boolean isEmpty() {
        return value.length == 0;
    }

    /**
     * Returns the {@code char} value at the
     * specified index. An index ranges from {@code 0} to
     * {@code length() - 1}. The first {@code char} value of the sequence
     * is at index {@code 0}, the next at index {@code 1},
     * and so on, as for array indexing.
     * <p>
     * 返回指定索引处的 char 值。
     * 索引范围从 0 到 length() - 1。
     * 序列的第一个 char 值位于索引 0 处，下一个位于索引 1 处，依此类推，就像数组索引一样。
     *
     * <p>If the {@code char} value specified by the index is a
     * <a href="Character.html#unicode">surrogate</a>, the surrogate
     * value is returned.
     * <p>
     * 如果索引指定的 char 值是一个 surrogate pair，那么返回该 surrogate value。
     *
     * @param index the index of the {@code char} value.
     * @return the {@code char} value at the specified index of this string.
     * The first {@code char} value is at index {@code 0}.
     * @throws IndexOutOfBoundsException if the {@code index}
     *                                   argument is negative or not less than the length of this
     *                                   string.
     */
    public char charAt(int index) {
        // 这里的 index < 0 || index >= value.length 是为了避免 index 溢出
        if ((index < 0) || (index >= value.length)) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return value[index];
    }

    /**
     * Returns the character (Unicode code point) at the specified
     * index. The index refers to {@code char} values
     * (Unicode code units) and ranges from {@code 0} to
     * {@link #length()}{@code  - 1}.
     * <p>
     * 返回指定索引处的字符（Unicode 码点）。
     * 索引是指 char 值（Unicode 代码单元），范围从 0 到 length() - 1。
     *
     * <p> If the {@code char} value specified at the given index
     * is in the high-surrogate range, the following index is less
     * than the length of this {@code String}, and the
     * {@code char} value at the following index is in the
     * low-surrogate range, then the supplementary code point
     * corresponding to this surrogate pair is returned. Otherwise,
     * the {@code char} value at the given index is returned.
     * <p>
     * 如果给定索引处指定的 char 值在 high-surrogate range，那么以下索引小于此 String 的长度，
     * 同样的，如果给定索引处的 char 值在 low-surrogate range，则返回与此代理项对应的补充代码点。
     * 否则，返回给定索引处的 char 值。
     * 也就是说可能上下浮动
     *
     * @param index the index to the {@code char} values
     * @return the code point value of the character at the
     * {@code index}
     * @throws IndexOutOfBoundsException if the {@code index}
     *                                   argument is negative or not less than the length of this
     *                                   string.
     * @since 1.5
     */
    public int codePointAt(int index) {
        if ((index < 0) || (index >= value.length)) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return Character.codePointAtImpl(value, index, value.length);
    }

    /**
     * Returns the character (Unicode code point) before the specified
     * index. The index refers to {@code char} values
     * (Unicode code units) and ranges from {@code 1} to {@link
     * CharSequence#length() length}.
     * <p>
     * 返回指定索引之前的字符（Unicode 码点）。
     * index 是指 char 值（Unicode 代码单元）的下标，范围从 1 到 length()。
     *
     * <p> If the {@code char} value at {@code (index - 1)}
     * is in the low-surrogate range, {@code (index - 2)} is not
     * negative, and the {@code char} value at {@code (index -
     * 2)} is in the high-surrogate range, then the
     * supplementary code point value of the surrogate pair is
     * returned. If the {@code char} value at {@code index -
     * 1} is an unpaired low-surrogate or a high-surrogate, the
     * surrogate value is returned.
     * <p>
     * 如果 index - 1 处的 char 值在 low-surrogate range， 并且 index - 2 不是负数，
     * 并且 index - 2 处的 char 值在 high-surrogate range，那么返回代理项对的补充代码点值。
     *
     * @param index the index following the code point that should be returned
     * @return the Unicode code point value before the given index.
     * @throws IndexOutOfBoundsException if the {@code index}
     *                                   argument is less than 1 or greater than the length
     *                                   of this string.
     * @since 1.5
     */
    public int codePointBefore(int index) {
        int i = index - 1;
        if ((i < 0) || (i >= value.length)) {
            throw new StringIndexOutOfBoundsException(index);
        }
        return Character.codePointBeforeImpl(value, index, 0);
    }

    /**
     * Returns the number of Unicode code points in the specified text
     * range of this {@code String}. The text range begins at the
     * specified {@code beginIndex} and extends to the
     * {@code char} at index {@code endIndex - 1}. Thus the
     * length (in {@code char}s) of the text range is
     * {@code endIndex-beginIndex}. Unpaired surrogates within
     * the text range count as one code point each.
     * <p>
     * 返回此 String 的指定索引范围内的 Unicode 代码点数。
     * 文本范围从指定的 beginIndex 开始，到索引 endIndex - 1 处的 char 结束。前闭后开
     * 因此，文本范围的长度（以 char 为单位）为 endIndex-beginIndex。
     * 文本范围内的不成对的 surrogates 每个都计为一个代码点。
     *
     * @param beginIndex the index to the first {@code char} of
     *                   the text range.
     * @param endIndex   the index after the last {@code char} of
     *                   the text range.
     * @return the number of Unicode code points in the specified text
     * range
     * @throws IndexOutOfBoundsException if the
     *                                   {@code beginIndex} is negative, or {@code endIndex}
     *                                   is larger than the length of this {@code String}, or
     *                                   {@code beginIndex} is larger than {@code endIndex}.
     * @since 1.5
     */
    public int codePointCount(int beginIndex, int endIndex) {
        if (beginIndex < 0 || endIndex > value.length || beginIndex > endIndex) {
            throw new IndexOutOfBoundsException();
        }
        return Character.codePointCountImpl(value, beginIndex, endIndex - beginIndex);
    }

    /**
     * Returns the index within this {@code String} that is
     * offset from the given {@code index} by
     * {@code codePointOffset} code points. Unpaired surrogates
     * within the text range given by {@code index} and
     * {@code codePointOffset} count as one code point each.
     * <p>
     * 返回此 String 中的索引，该索引与给定的 index 偏移 codePointOffset 个代码点。
     * 目标位置处，不成对的 surrogates 每个都计为一个代码点。
     *
     * @param index           the index to be offset
     * @param codePointOffset the offset in code points
     * @return the index within this {@code String}
     * @throws IndexOutOfBoundsException if {@code index}
     *                                   is negative or larger then the length of this
     *                                   {@code String}, or if {@code codePointOffset} is positive
     *                                   and the substring starting with {@code index} has fewer
     *                                   than {@code codePointOffset} code points,
     *                                   or if {@code codePointOffset} is negative and the substring
     *                                   before {@code index} has fewer than the absolute value
     *                                   of {@code codePointOffset} code points.
     * @since 1.5
     */
    public int offsetByCodePoints(int index, int codePointOffset) {
        if (index < 0 || index > value.length) {
            throw new IndexOutOfBoundsException();
        }
        return Character.offsetByCodePointsImpl(value, 0, value.length,
                index, codePointOffset);
    }

    /**
     * Copy characters from this string into dst starting at dstBegin.
     * This method doesn't perform any range checking.
     * <p>
     * 将此字符串中的字符复制到 dst，从 dstBegin 开始。
     * 此方法不执行任何范围检查。
     */
    void getChars(char dst[], int dstBegin) {
        System.arraycopy(value, 0, dst, dstBegin, value.length);
    }

    /**
     * Copies characters from this string into the destination character
     * array.
     * <p>
     * 将此字符串中的字符复制到目标字符数组。
     * <p>
     * The first character to be copied is at index {@code srcBegin};
     * the last character to be copied is at index {@code srcEnd-1}
     * (thus the total number of characters to be copied is
     * {@code srcEnd-srcBegin}). The characters are copied into the
     * subarray of {@code dst} starting at index {@code dstBegin}
     * and ending at index:
     * <blockquote><pre>
     *     dstBegin + (srcEnd-srcBegin) - 1
     * </pre></blockquote>
     * <p>
     * 要复制的第一个字符位于索引 srcBegin 处；要复制的最后一个字符位于索引 srcEnd-1 处，前闭后开
     * （因此，要复制的字符总数为 srcEnd-srcBegin）。
     *
     * @param srcBegin index of the first character in the string
     *                 to copy.
     * @param srcEnd   index after the last character in the string
     *                 to copy.
     * @param dst      the destination array.
     * @param dstBegin the start offset in the destination array.
     * @throws IndexOutOfBoundsException If any of the following
     *                                   is true:
     *                                   <ul><li>{@code srcBegin} is negative.
     *                                   <li>{@code srcBegin} is greater than {@code srcEnd}
     *                                   <li>{@code srcEnd} is greater than the length of this
     *                                       string
     *                                   <li>{@code dstBegin} is negative
     *                                   <li>{@code dstBegin+(srcEnd-srcBegin)} is larger than
     *                                       {@code dst.length}</ul>
     */
    public void getChars(int srcBegin, int srcEnd, char dst[], int dstBegin) {
        // 检查 srcBegin 和 srcEnd 是否合法
        if (srcBegin < 0) {
            throw new StringIndexOutOfBoundsException(srcBegin);
        }
        if (srcEnd > value.length) {
            throw new StringIndexOutOfBoundsException(srcEnd);
        }
        if (srcBegin > srcEnd) {
            throw new StringIndexOutOfBoundsException(srcEnd - srcBegin);
        }
        // 拷贝数组
        System.arraycopy(value, srcBegin, dst, dstBegin, srcEnd - srcBegin);
    }

    /**
     * Copies characters from this string into the destination byte array. Each
     * byte receives the 8 low-order bits of the corresponding character. The
     * eight high-order bits of each character are not copied and do not
     * participate in the transfer in any way.
     * <p>
     * 将此字符串中的字符复制到目标字节数组。
     * 每个字节接收相应字符的 8 个低位。
     * 每个字符的 8 个高位不会被复制，也不会以任何方式参与传输。
     *
     * <p> The first character to be copied is at index {@code srcBegin}; the
     * last character to be copied is at index {@code srcEnd-1}.  The total
     * number of characters to be copied is {@code srcEnd-srcBegin}. The
     * characters, converted to bytes, are copied into the subarray of {@code
     * dst} starting at index {@code dstBegin} and ending at index:
     *
     * <blockquote><pre>
     *     dstBegin + (srcEnd-srcBegin) - 1
     * </pre></blockquote>
     * <p>
     * 要复制的第一个字符位于索引 srcBegin 处；要复制的最后一个字符位于索引 srcEnd-1 处，前闭后开
     * （因此，要复制的字符总数为 srcEnd-srcBegin）。将字符转换为字节后，将其复制到 dst 的子数组中，
     *
     * @param srcBegin Index of the first character in the string to copy
     * @param srcEnd   Index after the last character in the string to copy
     * @param dst      The destination array
     * @param dstBegin The start offset in the destination array
     * @throws IndexOutOfBoundsException If any of the following is true:
     *                                   <ul>
     *                                     <li> {@code srcBegin} is negative
     *                                     <li> {@code srcBegin} is greater than {@code srcEnd}
     *                                     <li> {@code srcEnd} is greater than the length of this String
     *                                     <li> {@code dstBegin} is negative
     *                                     <li> {@code dstBegin+(srcEnd-srcBegin)} is larger than {@code
     *                                          dst.length}
     *                                   </ul>
     * @deprecated This method does not properly convert characters into
     * bytes.  As of JDK&nbsp;1.1, the preferred way to do this is via the
     * {@link #getBytes()} method, which uses the platform's default charset.
     */
    @Deprecated
    public void getBytes(int srcBegin, int srcEnd, byte dst[], int dstBegin) {
        // 检查 srcBegin 和 srcEnd 是否合法
        if (srcBegin < 0) {
            throw new StringIndexOutOfBoundsException(srcBegin);
        }
        if (srcEnd > value.length) {
            throw new StringIndexOutOfBoundsException(srcEnd);
        }
        if (srcBegin > srcEnd) {
            throw new StringIndexOutOfBoundsException(srcEnd - srcBegin);
        }
        Objects.requireNonNull(dst);

        // 拷贝数组
        int j = dstBegin;
        int n = srcEnd;
        int i = srcBegin;
        char[] val = value;   /* avoid getfield opcode */

        while (i < n) {
            dst[j++] = (byte) val[i++];
        }
    }

    /**
     * Encodes this {@code String} into a sequence of bytes using the named
     * charset, storing the result into a new byte array.
     * <p>
     * 使用指定的字符集将此 String 编码为字节序列，将结果存储到新的字节数组中。
     *
     * <p> The behavior of this method when this string cannot be encoded in
     * the given charset is unspecified.  The {@link
     * java.nio.charset.CharsetEncoder} class should be used when more control
     * over the encoding process is required.
     * <p>
     * 当此字符串无法在给定的字符集中编码时，此方法的行为是不确定的。
     * 因此如果希望更好的控制 encode 行为，应使用 CharsetEncoder 类。
     *
     * @param charsetName The name of a supported {@linkplain java.nio.charset.Charset
     *                    charset}
     * @return The resultant byte array
     * @throws UnsupportedEncodingException If the named charset is not supported
     * @since JDK1.1
     */
    public byte[] getBytes(String charsetName)
            throws UnsupportedEncodingException {
        if (charsetName == null) throw new NullPointerException();
        // 底层同样基于 StringEncoder，性能不高
        return StringCoding.encode(charsetName, value, 0, value.length);
    }

    /**
     * Encodes this {@code String} into a sequence of bytes using the given
     * {@linkplain java.nio.charset.Charset charset}, storing the result into a
     * new byte array.
     *
     * <p> This method always replaces malformed-input and unmappable-character
     * sequences with this charset's default replacement byte array.  The
     * {@link java.nio.charset.CharsetEncoder} class should be used when more
     * control over the encoding process is required.
     *
     * @param charset The {@linkplain java.nio.charset.Charset} to be used to encode
     *                the {@code String}
     * @return The resultant byte array
     * @since 1.6
     */
    public byte[] getBytes(Charset charset) {
        if (charset == null) throw new NullPointerException();
        return StringCoding.encode(charset, value, 0, value.length);
    }

    /**
     * Encodes this {@code String} into a sequence of bytes using the
     * platform's default charset, storing the result into a new byte array.
     * <p>
     * 使用平台默认的字符集将此 String 编码为字节序列，将结果存储到新的字节数组中。
     *
     * <p> The behavior of this method when this string cannot be encoded in
     * the default charset is unspecified.  The {@link
     * java.nio.charset.CharsetEncoder} class should be used when more control
     * over the encoding process is required.
     *
     * @return The resultant byte array
     * @since JDK1.1
     */
    public byte[] getBytes() {
        return StringCoding.encode(value, 0, value.length);
    }

    /**
     * Compares this string to the specified object.  The result is {@code
     * true} if and only if the argument is not {@code null} and is a {@code
     * String} object that represents the same sequence of characters as this
     * object.
     * <p>
     * 将此字符串与指定对象进行比较。
     * 当且仅当参数不为 null，并且是一个 String 对象，它表示与此对象相同的字符序列时，结果为 true。
     *
     * @param anObject The object to compare this {@code String} against
     * @return {@code true} if the given object represents a {@code String}
     * equivalent to this string, {@code false} otherwise
     * @see #compareTo(String)
     * @see #equalsIgnoreCase(String)
     */
    public boolean equals(Object anObject) {
        // 如果是同一个对象，直接返回 true（比较地址值）
        if (this == anObject) {
            return true;
        }
        // 判断目标对象是否为 String 类型
        if (anObject instanceof String) {
            String anotherString = (String) anObject;
            int n = value.length;
            // 判断长度是否相等
            if (n == anotherString.value.length) {
                // 新建两个指针
                char v1[] = value;
                char v2[] = anotherString.value;
                int i = 0;
                // 逐个比较字符
                while (n-- != 0) {
                    if (v1[i] != v2[i])
                        return false;
                    i++;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Compares this string to the specified {@code StringBuffer}.  The result
     * is {@code true} if and only if this {@code String} represents the same
     * sequence of characters as the specified {@code StringBuffer}. This method
     * synchronizes on the {@code StringBuffer}.
     * <p>
     * 将此字符串与指定的 StringBuffer 进行比较。
     * 当且仅当此 String 表示与指定的 StringBuffer 相同的字符序列时，结果为 true。
     * 此方法在 StringBuffer 上同步。
     *
     * @param sb The {@code StringBuffer} to compare this {@code String} against
     * @return {@code true} if this {@code String} represents the same
     * sequence of characters as the specified {@code StringBuffer},
     * {@code false} otherwise
     * @since 1.4
     */
    public boolean contentEquals(StringBuffer sb) {
        return contentEquals((CharSequence) sb);
    }

    /**
     * 以一种不加同步的方式当前对象所代表的字符串序列与指定的 AbstractStringBuilder 进行比较。
     * 这里的不加同步是指，不在当前方法上加同步；但是会在外层给 StringBuffer 加同步。
     *
     * @param sb
     * @return
     */
    private boolean nonSyncContentEquals(AbstractStringBuilder sb) {
        char v1[] = value;
        char v2[] = sb.getValue();
        int n = v1.length;
        // 判断长度是否相等
        if (n != sb.length()) {
            return false;
        }
        // 逐个比较字符
        for (int i = 0; i < n; i++) {
            if (v1[i] != v2[i]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compares this string to the specified {@code CharSequence}.  The
     * result is {@code true} if and only if this {@code String} represents the
     * same sequence of char values as the specified sequence. Note that if the
     * {@code CharSequence} is a {@code StringBuffer} then the method
     * synchronizes on it.
     * <p>
     * 将此字符串与指定的 CharSequence 进行比较(CharSequence 是一个接口，String 实现了该接口；StringBuilder 等也实现了该接口)
     * 当且仅当此 String 表示与指定的 CharSequence 相同的 char 值序列时，结果为 true。
     * 注意，如果 CharSequence 是一个 StringBuffer，则该方法会针对该 StringBuffer 对象加锁。
     *
     * @param cs The sequence to compare this {@code String} against
     * @return {@code true} if this {@code String} represents the same
     * sequence of char values as the specified sequence, {@code
     * false} otherwise
     * @since 1.5
     */
    public boolean contentEquals(CharSequence cs) {
        // Argument is a StringBuffer, StringBuilder
        // 如果是 StringBuffer 或者 StringBuilder
        if (cs instanceof AbstractStringBuilder) {
            if (cs instanceof StringBuffer) {
                synchronized (cs) {
                    return nonSyncContentEquals((AbstractStringBuilder) cs);
                }
            } else {
                return nonSyncContentEquals((AbstractStringBuilder) cs);
            }
        }
        // 如果是 String
        // Argument is a String
        if (cs instanceof String) {
            return equals(cs);
        }
        // Argument is a generic CharSequence
        // 如果不在上述之列，仅仅只是实现了 CharSequence 接口，那么后续的逐个比较只能通过 CharSequence 定义的方法来实现
        char v1[] = value;
        int n = v1.length;
        if (n != cs.length()) {
            return false;
        }
        for (int i = 0; i < n; i++) {
            if (v1[i] != cs.charAt(i)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Compares this {@code String} to another {@code String}, ignoring case
     * considerations.  Two strings are considered equal ignoring case if they
     * are of the same length and corresponding characters in the two strings
     * are equal ignoring case.
     * <p>
     * 将此 String 与另一个 String 进行比较，忽略大小写。
     * 如果两个字符串的长度相同，并且两个字符串中相应的字符（忽略大小写）相等，则认为它们是相等的。
     *
     * <p> Two characters {@code c1} and {@code c2} are considered the same
     * ignoring case if at least one of the following is true:
     * <ul>
     *   <li> The two characters are the same (as compared by the
     *        {@code ==} operator)
     *   <li> Applying the method {@link
     *        java.lang.Character#toUpperCase(char)} to each character
     *        produces the same result
     *   <li> Applying the method {@link
     *        java.lang.Character#toLowerCase(char)} to each character
     *        produces the same result
     * </ul>
     *
     * @param anotherString The {@code String} to compare this {@code String} against
     * @return {@code true} if the argument is not {@code null} and it
     * represents an equivalent {@code String} ignoring case; {@code
     * false} otherwise
     * @see #equals(Object)
     */
    public boolean equalsIgnoreCase(String anotherString) {
        // 如果是同一个对象，直接返回 true（比较地址值）
        if (this == anotherString) {
            return true;
        }
        // 如果入参为 null 或长度不一致，直接返回 false
        if (anotherString == null || anotherString.value.length != value.length) {
            return false;

        }
        // 进行更加复杂的比较
        return regionMatches(true, 0, anotherString, 0, value.length);
    }

    /**
     * Compares two strings lexicographically.
     * The comparison is based on the Unicode value of each character in
     * the strings. The character sequence represented by this
     * {@code String} object is compared lexicographically to the
     * character sequence represented by the argument string. The result is
     * a negative integer if this {@code String} object
     * lexicographically precedes the argument string. The result is a
     * positive integer if this {@code String} object lexicographically
     * follows the argument string. The result is zero if the strings
     * are equal; {@code compareTo} returns {@code 0} exactly when
     * the {@link #equals(Object)} method would return {@code true}.
     * <p>
     * 按字典顺序比较两个字符串。
     * 此比较基于字符串中每个字符的 Unicode 值。
     * 由此 String 对象表示的字符序列按字典顺序与参数字符串表示的字符序列进行比较。
     * 如果此 String 对象按字典顺序在参数字符串之前，则结果为负整数。即认为当前对象更小；
     * 如果此 String 对象按字典顺序在参数字符串之后，则结果为正整数。即认为当前对象更大；
     * 如果字符串相等，则结果为零；当且仅当 equals(Object) 方法返回 true 时，compareTo 返回 0。
     * <p>
     * This is the definition of lexicographic ordering. If two strings are
     * different, then either they have different characters at some index
     * that is a valid index for both strings, or their lengths are different,
     * or both. If they have different characters at one or more index
     * positions, let <i>k</i> be the smallest such index; then the string
     * whose character at position <i>k</i> has the smaller value, as
     * determined by using the &lt; operator, lexicographically precedes the
     * other string. In this case, {@code compareTo} returns the
     * difference of the two character values at position {@code k} in
     * the two string -- that is, the value:
     * <blockquote><pre>
     * this.charAt(k)-anotherString.charAt(k)
     * </pre></blockquote>
     * If there is no index position at which they differ, then the shorter
     * string lexicographically precedes the longer string. In this case,
     * {@code compareTo} returns the difference of the lengths of the
     * strings -- that is, the value:
     * <blockquote><pre>
     * this.length()-anotherString.length()
     * </pre></blockquote>
     * <p>
     * 这是字典顺序的定义。
     * 如果两个字符串不同，则它们在某个有效索引处具有不同的字符；或者它们的长度不同，或者两者都是。
     * 如果它们在一个或多个索引位置具有不同的字符，假设 k 是最早出现不一致的索引下标；
     * <p>
     * 然后在位置 k 处的字符具有较小值的字符串（由使用 < 运算符确定）按字典顺序在另一个字符串之前。
     * 在这种情况下，compareTo 返回两个字符串中位置 k 处的两个字符值之间的差异，
     * 即：this.charAt(k)-anotherString.charAt(k)
     * <p>
     * 如果没有索引位置不同，则较短的字符串按字典顺序在较长的字符串之前。
     * 在这种情况下，compareTo 返回字符串的长度之差，
     * 即：this.length()-anotherString.length()
     *
     * @param anotherString the {@code String} to be compared.
     * @return the value {@code 0} if the argument string is equal to
     * this string; a value less than {@code 0} if this string
     * is lexicographically less than the string argument; and a
     * value greater than {@code 0} if this string is
     * lexicographically greater than the string argument.
     */
    public int compareTo(String anotherString) {
        // 获取两个字符串的长度，如果入参为 null 就会报错
        int len1 = value.length;
        int len2 = anotherString.value.length;
        int lim = Math.min(len1, len2);
        // 新建两个指针，这里使用局部变量能方便编译器优化
        char v1[] = value;
        char v2[] = anotherString.value;

        // 逐个比较字符
        int k = 0;
        while (k < lim) {
            char c1 = v1[k];
            char c2 = v2[k];
            if (c1 != c2) {
                return c1 - c2;
            }
            k++;
        }
        // 如果前面的字符都相等，那么长度更长的字符串更大
        return len1 - len2;
    }

    /**
     * A Comparator that orders {@code String} objects as by
     * {@code compareToIgnoreCase}. This comparator is serializable.
     * <p>
     * 一个比较器，按照 compareToIgnoreCase 对 String 对象进行排序。
     * <p>
     * Note that this Comparator does <em>not</em> take locale into account,
     * and will result in an unsatisfactory ordering for certain locales.
     * The java.text package provides <em>Collators</em> to allow
     * locale-sensitive ordering.
     * <p>
     * 注意，此比较器不考虑 locale，并且将导致某些 locale 的排序不令人满意。
     * java.text 包提供了 Collators 类来允许 locale 敏感的排序。
     *
     * @see java.text.Collator#compare(String, String)
     * @since 1.2
     */
    public static final Comparator<String> CASE_INSENSITIVE_ORDER
            = new CaseInsensitiveComparator();

    private static class CaseInsensitiveComparator
            implements Comparator<String>, java.io.Serializable {
        // use serialVersionUID from JDK 1.2.2 for interoperability
        private static final long serialVersionUID = 8575799808933029326L;

        public int compare(String s1, String s2) {
            // 确定两个字符串的长度
            int n1 = s1.length();
            int n2 = s2.length();
            int min = Math.min(n1, n2);
            // 逐个比较字符
            for (int i = 0; i < min; i++) {
                char c1 = s1.charAt(i);
                char c2 = s2.charAt(i);
                // 如果字符不相等，那么全部转换为大写或小写后来进行比较
                if (c1 != c2) {
                    c1 = Character.toUpperCase(c1);
                    c2 = Character.toUpperCase(c2);
                    if (c1 != c2) {
                        c1 = Character.toLowerCase(c1);
                        c2 = Character.toLowerCase(c2);
                        // 如果还不相等，那么直接返回差值
                        if (c1 != c2) {
                            // No overflow because of numeric promotion
                            return c1 - c2;
                        }
                    }
                }
            }
            // 如果前面的字符都相等，那么长度更长的字符串更大
            return n1 - n2;
        }

        /**
         * Replaces the de-serialized object.
         */
        private Object readResolve() {
            return CASE_INSENSITIVE_ORDER;
        }
    }

    /**
     * Compares two strings lexicographically, ignoring case
     * differences. This method returns an integer whose sign is that of
     * calling {@code compareTo} with normalized versions of the strings
     * where case differences have been eliminated by calling
     * {@code Character.toLowerCase(Character.toUpperCase(character))} on
     * each character.
     * <p>
     * 按字典顺序比较两个字符串，忽略大小写差异。
     * 此方法返回一个整数，其符号与调用 compareTo 具有规范化版本的字符串的符号相同，
     * 其中通过调用 Character.toLowerCase(Character.toUpperCase(character)) 来消除了每个字符的大小写差异。
     * <p>
     * Note that this method does <em>not</em> take locale into account,
     * and will result in an unsatisfactory ordering for certain locales.
     * The java.text package provides <em>collators</em> to allow
     * locale-sensitive ordering.
     * <p>
     * 注意，此方法不考虑 locale，并且将导致某些 locale 的排序不令人满意。
     * java.text 包提供了 collators 来允许 locale 敏感的排序。
     *
     * @param str the {@code String} to be compared.
     * @return a negative integer, zero, or a positive integer as the
     * specified String is greater than, equal to, or less
     * than this String, ignoring case considerations.
     * @see java.text.Collator#compare(String, String)
     * @since 1.2
     */
    public int compareToIgnoreCase(String str) {
        return CASE_INSENSITIVE_ORDER.compare(this, str);
    }

    /**
     * Tests if two string regions are equal.
     * <p>
     * A substring of this {@code String} object is compared to a substring
     * of the argument other. The result is true if these substrings
     * represent identical character sequences. The substring of this
     * {@code String} object to be compared begins at index {@code toffset}
     * and has length {@code len}. The substring of other to be compared
     * begins at index {@code ooffset} and has length {@code len}. The
     * result is {@code false} if and only if at least one of the following
     * is true:
     * <ul><li>{@code toffset} is negative.
     * <li>{@code ooffset} is negative.
     * <li>{@code toffset+len} is greater than the length of this
     * {@code String} object.
     * <li>{@code ooffset+len} is greater than the length of the other
     * argument.
     * <li>There is some nonnegative integer <i>k</i> less than {@code len}
     * such that:
     * {@code this.charAt(toffset + }<i>k</i>{@code ) != other.charAt(ooffset + }
     * <i>k</i>{@code )}
     * </ul>
     *
     * @param toffset the starting offset of the subregion in this string.
     * @param other   the string argument.
     * @param ooffset the starting offset of the subregion in the string
     *                argument.
     * @param len     the number of characters to compare.
     * @return {@code true} if the specified subregion of this string
     * exactly matches the specified subregion of the string argument;
     * {@code false} otherwise.
     */
    public boolean regionMatches(int toffset, String other, int ooffset,
                                 int len) {
        char ta[] = value;
        int to = toffset;
        char pa[] = other.value;
        int po = ooffset;
        // Note: toffset, ooffset, or len might be near -1>>>1.
        if ((ooffset < 0) || (toffset < 0)
                || (toffset > (long) value.length - len)
                || (ooffset > (long) other.value.length - len)) {
            return false;
        }
        while (len-- > 0) {
            if (ta[to++] != pa[po++]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tests if two string regions are equal.
     * <p>
     * A substring of this {@code String} object is compared to a substring
     * of the argument {@code other}. The result is {@code true} if these
     * substrings represent character sequences that are the same, ignoring
     * case if and only if {@code ignoreCase} is true.
     * <p>
     * 将此 String 对象的子字符串与参数 other 的子字符串进行比较。
     * 当且仅当 ignoreCase 为 true 时，如果这些子字符串表示相同的字符序列，则结果为 true。
     *
     * <p>The substring of this {@code String} object to be compared begins at index
     * {@code toffset} and has length {@code len}. The substring of
     * {@code other} to be compared begins at index {@code ooffset} and
     * has length {@code len}. The result is {@code false} if and only if
     * at least one of the following is true:
     * <ul><li>{@code toffset} is negative.
     * <li>{@code ooffset} is negative.
     * <li>{@code toffset+len} is greater than the length of this
     * {@code String} object.
     * <li>{@code ooffset+len} is greater than the length of the other
     * argument.
     * <li>{@code ignoreCase} is {@code false} and there is some nonnegative
     * integer <i>k</i> less than {@code len} such that:
     * <blockquote><pre>
     * this.charAt(toffset+k) != other.charAt(ooffset+k)
     * </pre></blockquote>
     * <li>{@code ignoreCase} is {@code true} and there is some nonnegative
     * integer <i>k</i> less than {@code len} such that:
     * <blockquote><pre>
     * Character.toLowerCase(this.charAt(toffset+k)) !=
     * Character.toLowerCase(other.charAt(ooffset+k))
     * </pre></blockquote>
     * and:
     * <blockquote><pre>
     * Character.toUpperCase(this.charAt(toffset+k)) !=
     *         Character.toUpperCase(other.charAt(ooffset+k))
     * </pre></blockquote>
     * </ul>
     *
     * @param ignoreCase if {@code true}, ignore case when comparing
     *                   characters.
     * @param toffset    the starting offset of the subregion in this
     *                   string.
     * @param other      the string argument.
     * @param ooffset    the starting offset of the subregion in the string
     *                   argument.
     * @param len        the number of characters to compare.
     * @return {@code true} if the specified subregion of this string
     * matches the specified subregion of the string argument;
     * {@code false} otherwise. Whether the matching is exact
     * or case insensitive depends on the {@code ignoreCase}
     * argument.
     */
    public boolean regionMatches(boolean ignoreCase, int toffset,
                                 String other, int ooffset, int len) {
        // 定义好两个指针和引用
        char ta[] = value;
        int to = toffset;
        char pa[] = other.value;
        int po = ooffset;
        // Note: toffset, ooffset, or len might be near -1>>>1.
        // 检查参数是否合法
        if ((ooffset < 0) || (toffset < 0)
                || (toffset > (long) value.length - len)
                || (ooffset > (long) other.value.length - len)) {
            return false;
        }
        // 逐个比较字符
        while (len-- > 0) {
            char c1 = ta[to++];
            char c2 = pa[po++];
            // 如果字符相等，继续比较下一个字符
            if (c1 == c2) {
                continue;
            }
            // 如果字符不相等，但是可以忽略大小写
            if (ignoreCase) {
                // If characters don't match but case may be ignored,
                // try converting both characters to uppercase.
                // If the results match, then the comparison scan should
                // continue.
                // 将两个字符都转换为大写，如果相等，继续比较下一个字符
                char u1 = Character.toUpperCase(c1);
                char u2 = Character.toUpperCase(c2);
                if (u1 == u2) {
                    continue;
                }
                // Unfortunately, conversion to uppercase does not work properly
                // for the Georgian alphabet, which has strange rules about case
                // conversion.  So we need to make one last check before
                // exiting.
                // 将两个字符都转换为小写，如果相等，继续比较下一个字符
                if (Character.toLowerCase(u1) == Character.toLowerCase(u2)) {
                    continue;
                }
            }
            // 如果经过上面的比较，两个字符都不相等，直接返回 false
            return false;
        }
        return true;
    }

    /**
     * Tests if the substring of this string beginning at the
     * specified index starts with the specified prefix.
     * <p>
     * 验证当前字符串从指定下标开始，是否以指定的前缀开头。
     * 本质上其实是比较：当前字符串从 toffset 开始的后续 prefix.length 长度的子串与 prefix 是否一致
     *
     * @param prefix  the prefix.
     * @param toffset where to begin looking in this string.
     * @return {@code true} if the character sequence represented by the
     * argument is a prefix of the substring of this object starting
     * at index {@code toffset}; {@code false} otherwise.
     * The result is {@code false} if {@code toffset} is
     * negative or greater than the length of this
     * {@code String} object; otherwise the result is the same
     * as the result of the expression
     * <pre>
     *          this.substring(toffset).startsWith(prefix)
     *          </pre>
     */
    public boolean startsWith(String prefix, int toffset) {
        // 定义局部变量
        char ta[] = value;
        int to = toffset;
        char pa[] = prefix.value;
        int po = 0;
        int pc = prefix.value.length;
        // Note: toffset might be near -1>>>1.
        // 检查参数是否合法
        if ((toffset < 0) || (toffset > value.length - pc)) {
            return false;
        }
        // 逐个比较字符
        while (--pc >= 0) {
            if (ta[to++] != pa[po++]) {
                return false;
            }
        }
        return true;
    }

    /**
     * Tests if this string starts with the specified prefix.
     * <p>
     * 验证当前字符串是否以指定的前缀开头。
     *
     * @param prefix the prefix.
     * @return {@code true} if the character sequence represented by the
     * argument is a prefix of the character sequence represented by
     * this string; {@code false} otherwise.
     * Note also that {@code true} will be returned if the
     * argument is an empty string or is equal to this
     * {@code String} object as determined by the
     * {@link #equals(Object)} method.
     * @since 1. 0
     */
    public boolean startsWith(String prefix) {
        return startsWith(prefix, 0);
    }

    /**
     * Tests if this string ends with the specified suffix.
     * <p>
     * 验证当前字符串是否以指定的后缀结尾。
     *
     * @param suffix the suffix.
     * @return {@code true} if the character sequence represented by the
     * argument is a suffix of the character sequence represented by
     * this object; {@code false} otherwise. Note that the
     * result will be {@code true} if the argument is the
     * empty string or is equal to this {@code String} object
     * as determined by the {@link #equals(Object)} method.
     */
    public boolean endsWith(String suffix) {
        return startsWith(suffix, value.length - suffix.value.length);
    }

    /**
     * Returns a hash code for this string. The hash code for a
     * {@code String} object is computed as
     * <blockquote><pre>
     * s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
     * </pre></blockquote>
     * using {@code int} arithmetic, where {@code s[i]} is the
     * <i>i</i>th character of the string, {@code n} is the length of
     * the string, and {@code ^} indicates exponentiation.
     * (The hash value of the empty string is zero.)
     * <p>
     * 返回此字符串的哈希码。
     * String 对象的哈希码根据以下公式计算：
     * s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
     * 其中：s[i] 是字符串的第 i 个字符，n 是字符串的长度，^ 表示指数运算。
     * （空字符串的哈希值为零。）
     *
     * @return a hash code value for this object.
     */
    public int hashCode() {
        // 当前仅当 hash 值未被计算时，才会计算 hash 值
        int h = hash;
        if (h == 0 && value.length > 0) {
            char val[] = value;

            // 如果字符串非常长，hash 值会溢出，但是即使溢出也没有太大的关系
            for (int i = 0; i < value.length; i++) {
                h = 31 * h + val[i];
            }
            hash = h;
        }
        return h;
    }

    /**
     * Returns the index within this string of the first occurrence of
     * the specified character. If a character with value
     * {@code ch} occurs in the character sequence represented by
     * this {@code String} object, then the index (in Unicode
     * code units) of the first such occurrence is returned. For
     * values of {@code ch} in the range from 0 to 0xFFFF
     * (inclusive), this is the smallest value <i>k</i> such that:
     * <blockquote><pre>
     * this.charAt(<i>k</i>) == ch
     * </pre></blockquote>
     * is true. For other values of {@code ch}, it is the
     * smallest value <i>k</i> such that:
     * <blockquote><pre>
     * this.codePointAt(<i>k</i>) == ch
     * </pre></blockquote>
     * is true. In either case, if no such character occurs in this
     * string, then {@code -1} is returned.
     * <p>
     * 返回指定字符在当前字符串中第一次出现处的索引。
     * 如果 ch 值在此 String 对象表示的字符序列中，则返回第一次出现的索引（以 Unicode 代码单元表示）。
     * <p>
     * 对于 ch 值在 0 到 0xFFFF（包括）之间的值，这是最小的值 k，使得：
     * this.charAt(k) == ch 为 true。
     * <p>
     * 对于 ch 值在其他值之间，这是最小的值 k，使得：
     * this.codePointAt(k) == ch 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * @param ch a character (Unicode code point).
     * @return the index of the first occurrence of the character in the
     * character sequence represented by this object, or
     * {@code -1} if the character does not occur.
     */
    public int indexOf(int ch) {
        return indexOf(ch, 0);
    }

    /**
     * Returns the index within this string of the first occurrence of the
     * specified character, starting the search at the specified index.
     * <p>
     * 返回指定字符在当前字符串中第一次出现处的索引，从指定的索引开始搜索。
     * <p>
     * If a character with value {@code ch} occurs in the
     * character sequence represented by this {@code String}
     * object at an index no smaller than {@code fromIndex}, then
     * the index of the first such occurrence is returned. For values
     * of {@code ch} in the range from 0 to 0xFFFF (inclusive),
     * this is the smallest value <i>k</i> such that:
     * <blockquote><pre>
     * (this.charAt(<i>k</i>) == ch) {@code &&} (<i>k</i> &gt;= fromIndex)
     * </pre></blockquote>
     * is true. For other values of {@code ch}, it is the
     * smallest value <i>k</i> such that:
     * <blockquote><pre>
     * (this.codePointAt(<i>k</i>) == ch) {@code &&} (<i>k</i> &gt;= fromIndex)
     * </pre></blockquote>
     * is true. In either case, if no such character occurs in this
     * string at or after position {@code fromIndex}, then
     * {@code -1} is returned.
     * <p>
     * 如果当前字符串从 fromIndex 往后，存在字符值 ch，则返回第一次出现的索引。
     * <p>
     * 对于 ch 值在 0 到 0xFFFF（包括）之间的值，这是最小的值 k，使得：
     * (this.charAt(k) == ch) && (k >= fromIndex) 为 true。
     * <p>
     * 对于 ch 值在其他值之间，这是最小的值 k，使得：
     * (this.codePointAt(k) == ch) && (k >= fromIndex) 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * <p>
     * There is no restriction on the value of {@code fromIndex}. If it
     * is negative, it has the same effect as if it were zero: this entire
     * string may be searched. If it is greater than the length of this
     * string, it has the same effect as if it were equal to the length of
     * this string: {@code -1} is returned.
     * <p>
     * fromIndex 的值没有限制。如果它为负，则与它为零的效果相同：可以搜索整个字符串。
     * 如果它大于此字符串的长度，则与它等于此字符串的长度的效果相同：返回 -1。
     *
     * <p>All indices are specified in {@code char} values
     * (Unicode code units).
     * <p>
     * 所有索引都是以 char 值（Unicode 代码单元）指定的。
     *
     * @param ch        a character (Unicode code point).
     * @param fromIndex the index to start the search from.
     * @return the index of the first occurrence of the character in the
     * character sequence represented by this object that is greater
     * than or equal to {@code fromIndex}, or {@code -1}
     * if the character does not occur.
     */
    public int indexOf(int ch, int fromIndex) {
        final int max = value.length;
        // 规范化 fromIndex
        if (fromIndex < 0) {
            fromIndex = 0;
        } else if (fromIndex >= max) {
            // Note: fromIndex might be near -1>>>1.
            return -1;
        }

        // 如果 ch 是 BMP 字符，直接使用 charAt 进行比较
        if (ch < Character.MIN_SUPPLEMENTARY_CODE_POINT) {
            // handle most cases here (ch is a BMP code point or a
            // negative value (invalid code point))
            final char[] value = this.value;
            for (int i = fromIndex; i < max; i++) {
                if (value[i] == ch) {
                    return i;
                }
            }
            return -1;
        } else {
            // 否则说明 ch 的存储是依赖两个码点的，需要使用 codePointAt 进行比较
            return indexOfSupplementary(ch, fromIndex);
        }
    }

    /**
     * Handles (rare) calls of indexOf with a supplementary character.
     */
    private int indexOfSupplementary(int ch, int fromIndex) {
        if (Character.isValidCodePoint(ch)) {
            final char[] value = this.value;
            // 获取 ch 的高低位
            final char hi = Character.highSurrogate(ch);
            final char lo = Character.lowSurrogate(ch);
            final int max = value.length - 1;
            // 从 fromIndex 开始，逐个比较字符
            for (int i = fromIndex; i < max; i++) {
                if (value[i] == hi && value[i + 1] == lo) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the index within this string of the last occurrence of
     * the specified character. For values of {@code ch} in the
     * range from 0 to 0xFFFF (inclusive), the index (in Unicode code
     * units) returned is the largest value <i>k</i> such that:
     * <blockquote><pre>
     * this.charAt(<i>k</i>) == ch
     * </pre></blockquote>
     * is true. For other values of {@code ch}, it is the
     * largest value <i>k</i> such that:
     * <blockquote><pre>
     * this.codePointAt(<i>k</i>) == ch
     * </pre></blockquote>
     * is true.  In either case, if no such character occurs in this
     * string, then {@code -1} is returned.  The
     * {@code String} is searched backwards starting at the last
     * character.
     * <p>
     * 返回指定字符在当前字符串中最后一次出现处的索引。
     * <p>
     * 对于 ch 值在 0 到 0xFFFF（包括）之间的值，返回的索引（以 Unicode 代码单元表示）是最大的值 k，使得：
     * this.charAt(k) == ch 为 true。
     * <p>
     * 对于 ch 值在其他值之间，返回的索引是最大的值 k，使得：
     * this.codePointAt(k) == ch 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * @param ch a character (Unicode code point).
     * @return the index of the last occurrence of the character in the
     * character sequence represented by this object, or
     * {@code -1} if the character does not occur.
     */
    public int lastIndexOf(int ch) {
        return lastIndexOf(ch, value.length - 1);
    }

    /**
     * Returns the index within this string of the last occurrence of
     * the specified character, searching backward starting at the
     * specified index. For values of {@code ch} in the range
     * from 0 to 0xFFFF (inclusive), the index returned is the largest
     * value <i>k</i> such that:
     * <blockquote><pre>
     * (this.charAt(<i>k</i>) == ch) {@code &&} (<i>k</i> &lt;= fromIndex)
     * </pre></blockquote>
     * is true. For other values of {@code ch}, it is the
     * largest value <i>k</i> such that:
     * <blockquote><pre>
     * (this.codePointAt(<i>k</i>) == ch) {@code &&} (<i>k</i> &lt;= fromIndex)
     * </pre></blockquote>
     * is true. In either case, if no such character occurs in this
     * string at or before position {@code fromIndex}, then
     * {@code -1} is returned.
     * <p>
     * 返回指定字符在当前字符串中最后一次出现处的索引，从指定的索引开始向前搜索。
     * <p>
     * 对于 ch 值在 0 到 0xFFFF（包括）之间的值，返回的索引是最大的值 k，使得：
     * (this.charAt(k) == ch) && (k <= fromIndex) 为 true。
     * <p>
     * 对于 ch 值在其他值之间，返回的索引是最大的值 k，使得：
     * (this.codePointAt(k) == ch) && (k <= fromIndex) 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * <p>All indices are specified in {@code char} values
     * (Unicode code units).
     * <p>
     * 所有索引都是以 char 值（Unicode 代码单元）指定的。
     *
     * @param ch        a character (Unicode code point).
     * @param fromIndex the index to start the search from. There is no
     *                  restriction on the value of {@code fromIndex}. If it is
     *                  greater than or equal to the length of this string, it has
     *                  the same effect as if it were equal to one less than the
     *                  length of this string: this entire string may be searched.
     *                  If it is negative, it has the same effect as if it were -1:
     *                  -1 is returned.
     * @return the index of the last occurrence of the character in the
     * character sequence represented by this object that is less
     * than or equal to {@code fromIndex}, or {@code -1}
     * if the character does not occur before that point.
     */
    public int lastIndexOf(int ch, int fromIndex) {
        // 如果 ch 是 BMP 字符，可以直接针对 value 数组在进行比较
        // 大部分场景下，ch 都是 BMP 字符
        if (ch < Character.MIN_SUPPLEMENTARY_CODE_POINT) {
            // handle most cases here (ch is a BMP code point or a
            // negative value (invalid code point))
            final char[] value = this.value;
            int i = Math.min(fromIndex, value.length - 1);
            // 从 i 开始向前逐个比较字符
            for (; i >= 0; i--) {
                if (value[i] == ch) {
                    return i;
                }
            }
            return -1;
        } else {
            // 否则说明 ch 的存储是依赖两个码点的，需要使用 codePointAt 进行比较
            return lastIndexOfSupplementary(ch, fromIndex);
        }
    }

    /**
     * Handles (rare) calls of lastIndexOf with a supplementary character.
     */
    private int lastIndexOfSupplementary(int ch, int fromIndex) {
        if (Character.isValidCodePoint(ch)) {
            final char[] value = this.value;
            // 获取 ch 的高低位
            char hi = Character.highSurrogate(ch);
            char lo = Character.lowSurrogate(ch);
            int i = Math.min(fromIndex, value.length - 2);
            // 从 i 开始向前逐个比较字符
            for (; i >= 0; i--) {
                if (value[i] == hi && value[i + 1] == lo) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the index within this string of the first occurrence of the
     * specified substring.
     * <p>
     * 返回指定子字符串在当前字符串中第一次出现处的索引。
     *
     * <p>The returned index is the smallest value <i>k</i> for which:
     * <blockquote><pre>
     * this.startsWith(str, <i>k</i>)
     * </pre></blockquote>
     * If no such value of <i>k</i> exists, then {@code -1} is returned.
     * <p>
     * 返回的索引是最小的值 k，使得：
     * this.startsWith(str, k) 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * @param str the substring to search for.
     * @return the index of the first occurrence of the specified substring,
     * or {@code -1} if there is no such occurrence.
     */
    public int indexOf(String str) {
        return indexOf(str, 0);
    }

    /**
     * Returns the index within this string of the first occurrence of the
     * specified substring, starting at the specified index.
     * <p>
     * 返回指定子字符串在当前字符串中第一次出现处的索引，从指定的索引开始向后搜索。
     *
     * <p>The returned index is the smallest value <i>k</i> for which:
     * <blockquote><pre>
     * <i>k</i> &gt;= fromIndex {@code &&} this.startsWith(str, <i>k</i>)
     * </pre></blockquote>
     * If no such value of <i>k</i> exists, then {@code -1} is returned.
     * <p>
     * 返回的索引是最小的值 k，使得：
     * (k >= fromIndex) && this.startsWith(str, k) 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * @param str       the substring to search for.
     * @param fromIndex the index from which to start the search.
     * @return the index of the first occurrence of the specified substring,
     * starting at the specified index,
     * or {@code -1} if there is no such occurrence.
     */
    public int indexOf(String str, int fromIndex) {
        return indexOf(value, 0, value.length,
                str.value, 0, str.value.length, fromIndex);
    }

    /**
     * Code shared by String and AbstractStringBuilder to do searches. The
     * source is the character array being searched, and the target
     * is the string being searched for.
     * <p>
     * String 和 AbstractStringBuilder 共享的代码来执行搜索。
     * source 是被搜索的字符数组，target 是要目标子串。
     *
     * @param source       the characters being searched.
     * @param sourceOffset offset of the source string.
     * @param sourceCount  count of the source string.
     * @param target       the characters being searched for.
     * @param fromIndex    the index to begin searching from.
     */
    static int indexOf(char[] source, int sourceOffset, int sourceCount,
                       String target, int fromIndex) {
        return indexOf(source, sourceOffset, sourceCount,
                target.value, 0, target.value.length,
                fromIndex);
    }

    /**
     * Code shared by String and StringBuffer to do searches. The
     * source is the character array being searched, and the target
     * is the string being searched for.
     * <p>
     * String 和 StringBuffer 共享的代码来执行搜索。
     * source 是被搜索的字符数组，target 是要目标子串。
     * <p>
     * jdk 使用的是朴素的匹配算法，O(m*n)，没有使用 KMP 算法
     *
     * @param source       the characters being searched.
     * @param sourceOffset offset of the source string.
     * @param sourceCount  count of the source string.
     * @param target       the characters being searched for.
     * @param targetOffset offset of the target string.
     * @param targetCount  count of the target string.
     * @param fromIndex    the index to begin searching from.
     */
    static int indexOf(char[] source, int sourceOffset, int sourceCount,
                       char[] target, int targetOffset, int targetCount,
                       int fromIndex) {
        // 如果 fromIndex 大于等于 sourceCount，说明不需要寻找，直接返回 -1
        if (fromIndex >= sourceCount) {
            return (targetCount == 0 ? sourceCount : -1);
        }
        // 先规整 fromIndex
        if (fromIndex < 0) {
            fromIndex = 0;
        }
        // 如果 targetCount 为 0，意味着目标字符串为空 ""，那么直接返回 fromIndex
        if (targetCount == 0) {
            return fromIndex;
        }

        // 目标子串的第一个字符
        char first = target[targetOffset];
        // sourceOffset+sourceCount 代表 source 字符串参与匹配的长度；
        // - targetCount 代表参与遍历时的位数
        // 之所以先计算减法，是为了防止溢出
        int max = sourceOffset + (sourceCount - targetCount);

        // 此时一开始 i 定位到 source 的第一个参与匹配的字符
        for (int i = sourceOffset + fromIndex; i <= max; i++) {
            /* Look for first character. */
            // 先匹配第一个字符
            if (source[i] != first) {
                while (++i <= max && source[i] != first) ;
            }

            /* Found first character, now look at the rest of v2 */
            // i <= max 意味着已经完成了第一个字符的匹配；之后会匹配后续的其他字符
            if (i <= max) {
                int j = i + 1;
                int end = j + targetCount - 1; // end = i + targetCount
                // 比较 source 和 target 的后续字符，相等才会继续循环
                for (int k = targetOffset + 1; j < end && source[j]
                        == target[k]; j++, k++)
                    ;

                // 如果最终 j == end，说明整个 target 字符串都匹配上了，那么直接返回 i - sourceOffset
                if (j == end) {
                    /* Found whole string. */
                    return i - sourceOffset;
                }
            }
        }
        return -1;
    }

    /**
     * Returns the index within this string of the last occurrence of the
     * specified substring.  The last occurrence of the empty string ""
     * is considered to occur at the index value {@code this.length()}.
     * <p>
     * 返回指定子字符串在当前字符串中最后一次出现处的索引。
     * 空字符串 "" 的最后一次出现被认为是在索引值 this.length() 处。
     *
     * <p>The returned index is the largest value <i>k</i> for which:
     * <blockquote><pre>
     * this.startsWith(str, <i>k</i>)
     * </pre></blockquote>
     * If no such value of <i>k</i> exists, then {@code -1} is returned.
     * <p>
     * 返回的索引是最大的值 k，使得：
     * this.startsWith(str, k) 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * @param str the substring to search for.
     * @return the index of the last occurrence of the specified substring,
     * or {@code -1} if there is no such occurrence.
     */
    public int lastIndexOf(String str) {
        return lastIndexOf(str, value.length);
    }

    /**
     * Returns the index within this string of the last occurrence of the
     * specified substring, searching backward starting at the specified index.
     * <p>
     * 返回指定子字符串在当前字符串中最后一次出现处的索引，从指定的索引开始向前搜索。
     *
     * <p>The returned index is the largest value <i>k</i> for which:
     * <blockquote><pre>
     * <i>k</i> {@code <=} fromIndex {@code &&} this.startsWith(str, <i>k</i>)
     * </pre></blockquote>
     * If no such value of <i>k</i> exists, then {@code -1} is returned.
     * <p>
     * 返回的索引是最大的值 k，使得：
     * (k <= fromIndex) && this.startsWith(str, k) 为 true。
     * <p>
     * 在任一情况下，如果此字符串中没有这样的字符，则返回 -1。
     *
     * @param str       the substring to search for.
     * @param fromIndex the index to start the search from.
     * @return the index of the last occurrence of the specified substring,
     * searching backward from the specified index,
     * or {@code -1} if there is no such occurrence.
     */
    public int lastIndexOf(String str, int fromIndex) {
        return lastIndexOf(value, 0, value.length,
                str.value, 0, str.value.length, fromIndex);
    }

    /**
     * Code shared by String and AbstractStringBuilder to do searches. The
     * source is the character array being searched, and the target
     * is the string being searched for.
     * <p>
     * String 和 AbstractStringBuilder 共享的代码来执行搜索。
     * source 是被搜索的字符数组，target 是要目标子串。
     *
     * @param source       the characters being searched.
     * @param sourceOffset offset of the source string.
     * @param sourceCount  count of the source string.
     * @param target       the characters being searched for.
     * @param fromIndex    the index to begin searching from.
     */
    static int lastIndexOf(char[] source, int sourceOffset, int sourceCount,
                           String target, int fromIndex) {
        return lastIndexOf(source, sourceOffset, sourceCount,
                target.value, 0, target.value.length,
                fromIndex);
    }

    /**
     * Code shared by String and StringBuffer to do searches. The
     * source is the character array being searched, and the target
     * is the string being searched for.
     * <p>
     * String 和 StringBuffer 共享的代码来执行搜索。
     * source 是被搜索的字符数组，target 是要目标子串。
     *
     * @param source       the characters being searched.
     * @param sourceOffset offset of the source string.
     * @param sourceCount  count of the source string.
     * @param target       the characters being searched for.
     * @param targetOffset offset of the target string.
     * @param targetCount  count of the target string.
     * @param fromIndex    the index to begin searching from.
     */
    static int lastIndexOf(char[] source, int sourceOffset, int sourceCount,
                           char[] target, int targetOffset, int targetCount,
                           int fromIndex) {
        /*
         * Check arguments; return immediately where possible. For
         * consistency, don't check for null str.
         */
        // 定位到最右边开始识别的位置
        int rightIndex = sourceCount - targetCount;
        if (fromIndex < 0) {
            return -1;
        }
        if (fromIndex > rightIndex) {
            fromIndex = rightIndex;
        }
        /* Empty string always matches. */
        if (targetCount == 0) {
            return fromIndex;
        }

        // 目标子串的最后一个字符
        int strLastIndex = targetOffset + targetCount - 1;
        char strLastChar = target[strLastIndex];
        // source 字符串识别的最后位置（因为是从后往前识别的，所以其实是字符串数组最前面的位置）
        int min = sourceOffset + targetCount - 1;
        // source 字符串识别的开始位置（因为是从后往前识别的，所以其实是字符串数组最后面的位置）
        int i = min + fromIndex;

        // 定义一个 label，用于跳出多层循环
        startSearchForLastChar:
        while (true) {
            // 先尝试判断第一个字符；从后往前识别，如果 source[i] != strLastChar，那么 i--，继续识别
            while (i >= min && source[i] != strLastChar) {
                i--;
            }
            // 如果 i < min，说明 source 中没有 strLastChar，直接返回 -1
            if (i < min) {
                return -1;
            }
            int j = i - 1;
            int start = j - (targetCount - 1);
            int k = strLastIndex - 1;

            while (j > start) {
                if (source[j--] != target[k--]) {
                    i--;
                    continue startSearchForLastChar;
                }
            }
            return start - sourceOffset + 1;
        }
    }

    /**
     * Returns a string that is a substring of this string. The
     * substring begins with the character at the specified index and
     * extends to the end of this string. <p>
     * Examples:
     * <blockquote><pre>
     * "unhappy".substring(2) returns "happy"
     * "Harbison".substring(3) returns "bison"
     * "emptiness".substring(9) returns "" (an empty string)
     * </pre></blockquote>
     * <p>
     * 返回当前字符串的子串。
     * 子串从指定索引处的字符开始，并扩展到此字符串的末尾。
     * <p>
     * 例如：
     * "unhappy".substring(2) 返回 "happy"
     * "Harbison".substring(3) 返回 "bison"
     * "emptiness".substring(9) 返回 ""（一个空字符串）
     *
     * @param beginIndex the beginning index, inclusive.
     * @return the specified substring.
     * @throws IndexOutOfBoundsException if
     *                                   {@code beginIndex} is negative or larger than the
     *                                   length of this {@code String} object.
     */
    public String substring(int beginIndex) {
        // 如果 beginIndex < 0，抛出异常
        if (beginIndex < 0) {
            throw new StringIndexOutOfBoundsException(beginIndex);
        }
        // 计算子串的长度
        int subLen = value.length - beginIndex;
        if (subLen < 0) {
            throw new StringIndexOutOfBoundsException(subLen);
        }
        // 如果 beginIndex == 0，直接返回当前字符串；否则返回一个新的字符串对象
        return (beginIndex == 0) ? this : new String(value, beginIndex, subLen);
    }

    /**
     * Returns a string that is a substring of this string. The
     * substring begins at the specified {@code beginIndex} and
     * extends to the character at index {@code endIndex - 1}.
     * Thus the length of the substring is {@code endIndex-beginIndex}.
     * <p>
     * Examples:
     * <blockquote><pre>
     * "hamburger".substring(4, 8) returns "urge"
     * "smiles".substring(1, 5) returns "mile"
     * </pre></blockquote>
     * <p>
     * 返回当前字符串的子串。
     * 子串从指定索引处的字符开始，并扩展到索引 endIndex - 1 处的字符。前闭后开。
     * 因此，子字符串的长度为 endIndex-beginIndex。
     * <p>
     * 例如：
     * "hamburger".substring(4, 8) 返回 "urge"
     * "smiles".substring(1, 5) 返回 "mile"
     *
     * @param beginIndex the beginning index, inclusive.
     * @param endIndex   the ending index, exclusive.
     * @return the specified substring.
     * @throws IndexOutOfBoundsException if the
     *                                   {@code beginIndex} is negative, or
     *                                   {@code endIndex} is larger than the length of
     *                                   this {@code String} object, or
     *                                   {@code beginIndex} is larger than
     *                                   {@code endIndex}.
     */
    public String substring(int beginIndex, int endIndex) {
        // 如果 beginIndex < 0，抛出异常
        if (beginIndex < 0) {
            throw new StringIndexOutOfBoundsException(beginIndex);
        }
        // 如果 endIndex > value.length，抛出异常
        if (endIndex > value.length) {
            throw new StringIndexOutOfBoundsException(endIndex);
        }
        // 计算子串的长度
        int subLen = endIndex - beginIndex;
        if (subLen < 0) {
            throw new StringIndexOutOfBoundsException(subLen);
        }
        // 如果 beginIndex == 0 && endIndex == value.length，直接返回当前字符串；
        // 否则返回一个新的字符串对象
        return ((beginIndex == 0) && (endIndex == value.length)) ? this
                : new String(value, beginIndex, subLen);
    }

    /**
     * Returns a character sequence that is a subsequence of this sequence.
     * <p>
     * 返回当前字符串的子序列。
     *
     * <p> An invocation of this method of the form
     *
     * <blockquote><pre>
     * str.subSequence(begin,&nbsp;end)</pre></blockquote>
     * <p>
     * behaves in exactly the same way as the invocation
     *
     * <blockquote><pre>
     * str.substring(begin,&nbsp;end)</pre></blockquote>
     * <p>
     * 本方法的行为与 str.substring(begin, end) 完全相同。
     *
     * @param beginIndex the begin index, inclusive.
     * @param endIndex   the end index, exclusive.
     * @return the specified subsequence.
     * @throws IndexOutOfBoundsException if {@code beginIndex} or {@code endIndex} is negative,
     *                                   if {@code endIndex} is greater than {@code length()},
     *                                   or if {@code beginIndex} is greater than {@code endIndex}
     * @apiNote This method is defined so that the {@code String} class can implement
     * the {@link CharSequence} interface.
     * @spec JSR-51
     * @since 1.4
     */
    public CharSequence subSequence(int beginIndex, int endIndex) {
        return this.substring(beginIndex, endIndex);
    }

    /**
     * Concatenates the specified string to the end of this string.
     * <p>
     * 将指定字符串连接到此字符串的结尾。
     * <p>
     * If the length of the argument string is {@code 0}, then this
     * {@code String} object is returned. Otherwise, a
     * {@code String} object is returned that represents a character
     * sequence that is the concatenation of the character sequence
     * represented by this {@code String} object and the character
     * sequence represented by the argument string.<p>
     * Examples:
     * <blockquote><pre>
     * "cares".concat("s") returns "caress"
     * "to".concat("get").concat("her") returns "together"
     * </pre></blockquote>
     * <p>
     * 如果入参字符串的长度为 0，则返回此 String 对象。
     * 否则，返回一个 String 对象，该对象表示一个字符序列，该字符序列是由此 String 对象表示的字符序列和参数字符串表示的字符序列连接而成。
     * <p>
     * 例如：
     * "cares".concat("s") 返回 "caress"
     * "to".concat("get").concat("her") 返回 "together"
     *
     * @param str the {@code String} that is concatenated to the end
     *            of this {@code String}.
     * @return a string that represents the concatenation of this object's
     * characters followed by the string argument's characters.
     */
    public String concat(String str) {
        // 如果入参字符串的长度为 0，直接返回当前字符串
        if (str.isEmpty()) {
            return this;
        }
        // 计算新字符串的长度，并重新分配内存
        int len = value.length;
        int otherLen = str.length();
        // 填充当前字符串的内容
        char buf[] = Arrays.copyOf(value, len + otherLen);
        // 填充入参字符串的内容
        str.getChars(buf, len);
        // share=true 意味着该构造函数不会 copy 字符数组，而是会直接引用它
        return new String(buf, true);
    }

    /**
     * Returns a string resulting from replacing all occurrences of
     * {@code oldChar} in this string with {@code newChar}.
     * <p>
     * 返回一个字符串，该字符串是通过用 newChar 替换此字符串中出现的所有 oldChar 得到的。
     * <p>
     * If the character {@code oldChar} does not occur in the
     * character sequence represented by this {@code String} object,
     * then a reference to this {@code String} object is returned.
     * Otherwise, a {@code String} object is returned that
     * represents a character sequence identical to the character sequence
     * represented by this {@code String} object, except that every
     * occurrence of {@code oldChar} is replaced by an occurrence
     * of {@code newChar}.
     * <p>
     * Examples:
     * <blockquote><pre>
     * "mesquite in your cellar".replace('e', 'o')
     *         returns "mosquito in your collar"
     * "the war of baronets".replace('r', 'y')
     *         returns "the way of bayonets"
     * "sparring with a purple porpoise".replace('p', 't')
     *         returns "starring with a turtle tortoise"
     * "JonL".replace('q', 'x') returns "JonL" (no change)
     * </pre></blockquote>
     * <p>
     * 如果 oldChar 不在当前 String 对象表示的字符序列中，则返回当前 String 对象的引用。
     * 否则，返回一个 String 对象，该对象表示一个字符序列，该字符序列与此 String 对象表示的字符序列相同，除了每个 oldChar 的出现都替换为 newChar。
     * <p>
     * 例如：
     * "mesquite in your cellar".replace('e', 'o') 返回 "mosquito in your collar"
     * "the war of baronets".replace('r', 'y') 返回 "the way of bayonets"
     * "sparring with a purple porpoise".replace('p', 't') 返回 "starring with a turtle tortoise"
     * "JonL".replace('q', 'x') 返回 "JonL"（没有变化）
     *
     * @param oldChar the old character.
     * @param newChar the new character.
     * @return a string derived from this string by replacing every
     * occurrence of {@code oldChar} with {@code newChar}.
     */
    public String replace(char oldChar, char newChar) {
        if (oldChar != newChar) {
            int len = value.length;
            int i = -1;
            // 这里的 avoid getfield opcode 是指：
            // 如果不使用局部变量 val，而是直接使用 this.value，那么每次取值都会使用 getfield 指令；如果在循环中，会导致性能下降
            // 而如果使用局部变量 val，那么只会使用一次 getfield 指令，之后都是使用局部变量 val
            char[] val = value; /* avoid getfield opcode */

            // 找到第一个 oldChar 的位置
            while (++i < len) {
                if (val[i] == oldChar) {
                    break;
                }
            }
            // 如果 i < len，说明找到了 oldChar，需要进行替换
            if (i < len) {
                // 创建一个新的字符数组，用于存储替换后的字符串
                char buf[] = new char[len];
                // 从 0 到 i，将 val 中的内容复制到 buf 中
                // 为什么这里使用循环赋值，而不是使用 System.arraycopy？
                // 可能的原因是：JNI 作为本地方法，性能不稳定；预计 i 值较小；前面的 while 中访问过了 val 数组，可能会被 JIT 编译器优化
                for (int j = 0; j < i; j++) {
                    buf[j] = val[j];
                }
                // 继续从 i 开始，向后探索，此时因为已经声明了一个新的字符数组，因此后面的 char 都需要填充到新的字符数组中
                while (i < len) {
                    char c = val[i];
                    buf[i] = (c == oldChar) ? newChar : c;
                    i++;
                }
                // share=true 意味着该构造函数不会 copy 字符数组，而是会直接引用它
                return new String(buf, true);
            }
        }
        // 如果 oldChar == newChar，直接返回当前字符串
        return this;
    }

    /**
     * Tells whether or not this string matches the given <a
     * href="../util/regex/Pattern.html#sum">regular expression</a>.
     * <p>
     * 返回此字符串是否匹配给定的正则表达式。
     *
     * <p> An invocation of this method of the form
     * <i>str</i>{@code .matches(}<i>regex</i>{@code )} yields exactly the
     * same result as the expression
     *
     * <blockquote>
     * {@link java.util.regex.Pattern}.{@link java.util.regex.Pattern#matches(String, CharSequence)
     * matches(<i>regex</i>, <i>str</i>)}
     * </blockquote>
     * <p>
     * 本方法的行为与 Pattern.matches(regex, str) 完全相同。
     *
     * @param regex the regular expression to which this string is to be matched
     * @return {@code true} if, and only if, this string matches the
     * given regular expression
     * @throws PatternSyntaxException if the regular expression's syntax is invalid
     * @spec JSR-51
     * @see java.util.regex.Pattern
     * @since 1.4
     */
    public boolean matches(String regex) {
        // Pattern.matches(regex, this) 会调用 Pattern.compile(regex).matcher(this).matches();
        // 核心逻辑在 Pattern，这里跳过
        return Pattern.matches(regex, this);
    }

    /**
     * Returns true if and only if this string contains the specified
     * sequence of char values.
     * <p>
     * 当且仅当此字符串包含指定的 char 值序列时，返回 true。
     *
     * @param s the sequence to search for
     * @return true if this string contains {@code s}, false otherwise
     * @since 1.5
     */
    public boolean contains(CharSequence s) {
        // 查找 s 在当前字符串中第一次出现的位置，如果 > -1 说明当前字符串中包含 s
        return indexOf(s.toString()) > -1;
    }

    /**
     * Replaces the first substring of this string that matches the given <a
     * href="../util/regex/Pattern.html#sum">regular expression</a> with the
     * given replacement.
     * <p>
     * 使用给定的 替换字符串 替换 当前字符串 匹配 给定的正则表达式 的第一个子字符串。
     *
     * <p> An invocation of this method of the form
     * <i>str</i>{@code .replaceFirst(}<i>regex</i>{@code ,} <i>repl</i>{@code )}
     * yields exactly the same result as the expression
     *
     * <blockquote>
     * <code>
     * {@link java.util.regex.Pattern}.{@link
     * java.util.regex.Pattern#compile compile}(<i>regex</i>).{@link
     * java.util.regex.Pattern#matcher(java.lang.CharSequence) matcher}(<i>str</i>).{@link
     * java.util.regex.Matcher#replaceFirst replaceFirst}(<i>repl</i>)
     * </code>
     * </blockquote>
     * <p>
     * 本方法的行为与 Pattern.compile(regex).matcher(this).replaceFirst(repl) 完全相同。
     *
     * <p>
     * Note that backslashes ({@code \}) and dollar signs ({@code $}) in the
     * replacement string may cause the results to be different than if it were
     * being treated as a literal replacement string; see
     * {@link java.util.regex.Matcher#replaceFirst}.
     * Use {@link java.util.regex.Matcher#quoteReplacement} to suppress the special
     * meaning of these characters, if desired.
     * <p>
     * 注意，替换字符串中的反斜杠 (\) 和美元符号 ($) 可能会导致结果与将其视为字面替换字符串时的结果不同。
     * 请参阅 Matcher.replaceFirst。
     * 如果需要，可以使用 Matcher.quoteReplacement 来取消这些字符的特殊含义。
     *
     * @param regex       the regular expression to which this string is to be matched
     * @param replacement the string to be substituted for the first match
     * @return The resulting {@code String}
     * @throws PatternSyntaxException if the regular expression's syntax is invalid
     * @spec JSR-51
     * @see java.util.regex.Pattern
     * @since 1.4
     */
    public String replaceFirst(String regex, String replacement) {
        return Pattern.compile(regex).matcher(this).replaceFirst(replacement);
    }

    /**
     * Replaces each substring of this string that matches the given <a
     * href="../util/regex/Pattern.html#sum">regular expression</a> with the
     * given replacement.
     * <p>
     * 使用给定的 替换字符串 替换 当前字符串 匹配 给定的正则表达式 的所有子字符串。
     *
     * <p> An invocation of this method of the form
     * <i>str</i>{@code .replaceAll(}<i>regex</i>{@code ,} <i>repl</i>{@code )}
     * yields exactly the same result as the expression
     *
     * <blockquote>
     * <code>
     * {@link java.util.regex.Pattern}.{@link
     * java.util.regex.Pattern#compile compile}(<i>regex</i>).{@link
     * java.util.regex.Pattern#matcher(java.lang.CharSequence) matcher}(<i>str</i>).{@link
     * java.util.regex.Matcher#replaceAll replaceAll}(<i>repl</i>)
     * </code>
     * </blockquote>
     * <p>
     * 本方法的行为与 Pattern.compile(regex).matcher(this).replaceAll(repl) 完全相同。
     *
     * <p>
     * Note that backslashes ({@code \}) and dollar signs ({@code $}) in the
     * replacement string may cause the results to be different than if it were
     * being treated as a literal replacement string; see
     * {@link java.util.regex.Matcher#replaceAll Matcher.replaceAll}.
     * Use {@link java.util.regex.Matcher#quoteReplacement} to suppress the special
     * meaning of these characters, if desired.
     * <p>
     * 注意，替换字符串中的反斜杠 (\) 和美元符号 ($) 可能会导致结果与将其视为字面替换字符串时的结果不同。
     * 请参阅 Matcher.replaceAll。
     * 如果需要，可以使用 Matcher.quoteReplacement 来取消这些字符的特殊含义。
     *
     * @param regex       the regular expression to which this string is to be matched
     * @param replacement the string to be substituted for each match
     * @return The resulting {@code String}
     * @throws PatternSyntaxException if the regular expression's syntax is invalid
     * @spec JSR-51
     * @see java.util.regex.Pattern
     * @since 1.4
     */
    public String replaceAll(String regex, String replacement) {
        return Pattern.compile(regex).matcher(this).replaceAll(replacement);
    }

    /**
     * Replaces each substring of this string that matches the literal target
     * sequence with the specified literal replacement sequence. The
     * replacement proceeds from the beginning of the string to the end, for
     * example, replacing "aa" with "b" in the string "aaa" will result in
     * "ba" rather than "ab".
     * <p>
     * 使用指定的字面替换字符串替换此字符串中与字面目标序列匹配的每个子字符串。
     * 替换从字符串的开头到结尾进行，例如，在字符串 "aaa" 中用 "b" 替换 "aa" 将导致 "ba" 而不是 "ab"。
     *
     * @param target      The sequence of char values to be replaced
     * @param replacement The replacement sequence of char values
     * @return The resulting string
     * @since 1.5
     */
    public String replace(CharSequence target, CharSequence replacement) {
        return Pattern.compile(target.toString(), Pattern.LITERAL).matcher(
                this).replaceAll(Matcher.quoteReplacement(replacement.toString()));
    }

    /**
     * Splits this string around matches of the given
     * <a href="../util/regex/Pattern.html#sum">regular expression</a>.
     * <p>
     * 使用给定的正则表达式拆分此字符串。
     * <p> The array returned by this method contains each substring of this
     * string that is terminated by another substring that matches the given
     * expression or is terminated by the end of the string.  The substrings in
     * the array are in the order in which they occur in this string.  If the
     * expression does not match any part of the input then the resulting array
     * has just one element, namely this string.
     * <p>
     * 本方法返回的数组包含此字符串的每个子字符串，这些子字符串由另一个与给定表达式匹配的子字符串终止，或者由字符串的结尾终止。
     * 数组中的子字符串按照它们在此字符串中出现的顺序排列。
     * 如果表达式不匹配输入的任何部分，则结果数组只有一个元素，即此字符串。
     *
     * <p> When there is a positive-width match at the beginning of this
     * string then an empty leading substring is included at the beginning
     * of the resulting array. A zero-width match at the beginning however
     * never produces such empty leading substring.
     * <p>
     * 当此字符串开头有一个 positive-width match 时，结果数组的前几个元素将包含一个空的 leading substring。
     * 但是，开始的 zero-width match 永远不会产生这样的空的 leading substring。
     *
     * <p> The {@code limit} parameter controls the number of times the
     * pattern is applied and therefore affects the length of the resulting
     * array.  If the limit <i>n</i> is greater than zero then the pattern
     * will be applied at most <i>n</i>&nbsp;-&nbsp;1 times, the array's
     * length will be no greater than <i>n</i>, and the array's last entry
     * will contain all input beyond the last matched delimiter.  If <i>n</i>
     * is non-positive then the pattern will be applied as many times as
     * possible and the array can have any length.  If <i>n</i> is zero then
     * the pattern will be applied as many times as possible, the array can
     * have any length, and trailing empty strings will be discarded.
     * <p>
     * limit 参数控制 pattern 的应用次数，因此影响结果数组的长度。
     * 如果 limit > 0，则 pattern 最多应用 n - 1 次，数组的长度不大于 n，数组的最后一个元素将包含所有输入的最后一个匹配定界符之后的内容。
     * 如果 n <= 0，则 pattern 将尽可能多地应用，数组的长度可以是任意长度。
     *
     * <p> The string {@code "boo:and:foo"}, for example, yields the
     * following results with these parameters:
     *
     * <blockquote><table cellpadding=1 cellspacing=0 summary="Split example showing regex, limit, and result">
     * <tr>
     * <th>Regex</th>
     * <th>Limit</th>
     * <th>Result</th>
     * </tr>
     * <tr><td align=center>:</td>
     * <td align=center>2</td>
     * <td>{@code { "boo", "and:foo" }}</td></tr>
     * <tr><td align=center>:</td>
     * <td align=center>5</td>
     * <td>{@code { "boo", "and", "foo" }}</td></tr>
     * <tr><td align=center>:</td>
     * <td align=center>-2</td>
     * <td>{@code { "boo", "and", "foo" }}</td></tr>
     * <tr><td align=center>o</td>
     * <td align=center>5</td>
     * <td>{@code { "b", "", ":and:f", "", "" }}</td></tr>
     * <tr><td align=center>o</td>
     * <td align=center>-2</td>
     * <td>{@code { "b", "", ":and:f", "", "" }}</td></tr>
     * <tr><td align=center>o</td>
     * <td align=center>0</td>
     * <td>{@code { "b", "", ":and:f" }}</td></tr>
     * </table></blockquote>
     * <p>
     * 例如，字符串 "boo:and:foo" 使用这些参数将产生以下结果：
     * Regex	Limit	Result
     * :	2	{ "boo", "and:foo" }
     * :	5	{ "boo", "and", "foo" }
     * :	-2	{ "boo", "and", "foo" }
     * o	5	{ "b", "", ":and:f", "", "" }
     * o	-2	{ "b", "", ":and:f", "", "" }
     * o	0	{ "b", "", ":and:f" }
     *
     * <p> An invocation of this method of the form
     * <i>str.</i>{@code split(}<i>regex</i>{@code ,}&nbsp;<i>n</i>{@code )}
     * yields the same result as the expression
     *
     * <blockquote>
     * <code>
     * {@link java.util.regex.Pattern}.{@link
     * java.util.regex.Pattern#compile compile}(<i>regex</i>).{@link
     * java.util.regex.Pattern#split(java.lang.CharSequence, int) split}(<i>str</i>,&nbsp;<i>n</i>)
     * </code>
     * </blockquote>
     * <p>
     * 本方法的行为与 Pattern.compile(regex).split(this, n) 完全相同。
     *
     * @param regex the delimiting regular expression
     * @param limit the result threshold, as described above
     * @return the array of strings computed by splitting this string
     * around matches of the given regular expression
     * @throws PatternSyntaxException if the regular expression's syntax is invalid
     * @spec JSR-51
     * @see java.util.regex.Pattern
     * @since 1.4
     */
    public String[] split(String regex, int limit) {
        /* fastpath if the regex is a
         (1)one-char String and this character is not one of the
            RegEx's meta characters ".$|()[{^?*+\\", or
         (2)two-char String and the first char is the backslash and
            the second is not the ascii digit or ascii letter.
         */
        // 如果 regex 是一个字符，并且这个字符不是正则表达式的元字符 ".$|()[{^?*+\\"
        // 或者 regex 是两个字符，并且第一个字符是反斜杠，第二个字符不是数字或字母或码点对
        // 那么直接调用 indexOf 方法进行分割
        char ch = 0;
        if (((regex.value.length == 1 &&
                ".$|()[{^?*+\\".indexOf(ch = regex.charAt(0)) == -1) ||
                (regex.length() == 2 &&
                        regex.charAt(0) == '\\' &&
                        (((ch = regex.charAt(1)) - '0') | ('9' - ch)) < 0 &&
                        ((ch - 'a') | ('z' - ch)) < 0 &&
                        ((ch - 'A') | ('Z' - ch)) < 0)) &&
                (ch < Character.MIN_HIGH_SURROGATE ||
                        ch > Character.MAX_LOW_SURROGATE)) {
            int off = 0;
            int next = 0;
            boolean limited = limit > 0;
            ArrayList<String> list = new ArrayList<>();
            // 从 off 开始向后寻找 ch 字符下标
            while ((next = indexOf(ch, off)) != -1) {
                // 如果不存在限制，或者未达到限制，就向后截取数据
                if (!limited || list.size() < limit - 1) {
                    list.add(substring(off, next));
                    off = next + 1;
                } else {    // last one
                    //assert (list.size() == limit - 1);
                    // 触发了限制，就将剩余的字符串全部添加到 list 中
                    list.add(substring(off, value.length));
                    off = value.length;
                    break;
                }
            }
            // If no match was found, return this
            // 如果 off=0，就意味着没有找到匹配的字符串，直接返回当前字符串
            if (off == 0)
                return new String[]{this};

            // Add remaining segment
            // 如果不存在限制，或者未达到限制，就将剩余的字符串添加到 list 中；可能会多添加一个空字符串
            if (!limited || list.size() < limit)
                list.add(substring(off, value.length));

            // Construct result
            // 将 list 转换为数组，如果 limit=0，意味着没有指定限制，那么需要将 list 中的空字符串去掉
            int resultSize = list.size();
            if (limit == 0) {
                while (resultSize > 0 && list.get(resultSize - 1).isEmpty()) {
                    resultSize--;
                }
            }
            String[] result = new String[resultSize];
            return list.subList(0, resultSize).toArray(result);
        }
        return Pattern.compile(regex).split(this, limit);
    }

    /**
     * Splits this string around matches of the given <a
     * href="../util/regex/Pattern.html#sum">regular expression</a>.
     * <p>
     * 使用给定的正则表达式拆分此字符串。
     *
     * <p> This method works as if by invoking the two-argument {@link
     * #split(String, int) split} method with the given expression and a limit
     * argument of zero.  Trailing empty strings are therefore not included in
     * the resulting array.
     * <p>
     * 本方法的行为与 split(regex, 0) 完全相同。
     * 因此，结果数组中不包含尾随空字符串。
     *
     * <p> The string {@code "boo:and:foo"}, for example, yields the following
     * results with these expressions:
     *
     * <blockquote><table cellpadding=1 cellspacing=0 summary="Split examples showing regex and result">
     * <tr>
     * <th>Regex</th>
     * <th>Result</th>
     * </tr>
     * <tr><td align=center>:</td>
     * <td>{@code { "boo", "and", "foo" }}</td></tr>
     * <tr><td align=center>o</td>
     * <td>{@code { "b", "", ":and:f" }}</td></tr>
     * </table></blockquote>
     *
     * @param regex the delimiting regular expression
     * @return the array of strings computed by splitting this string
     * around matches of the given regular expression
     * @throws PatternSyntaxException if the regular expression's syntax is invalid
     * @spec JSR-51
     * @see java.util.regex.Pattern
     * @since 1.4
     */
    public String[] split(String regex) {
        return split(regex, 0);
    }

    /**
     * Returns a new String composed of copies of the
     * {@code CharSequence elements} joined together with a copy of
     * the specified {@code delimiter}.
     *
     * <blockquote>For example,
     * <pre>{@code
     *     String message = String.join("-", "Java", "is", "cool");
     *     // message returned is: "Java-is-cool"
     * }</pre></blockquote>
     * <p>
     * 返回一个由指定的 CharSequence 元素组成的新 String，这些元素使用指定的分隔符连接。
     * 例如，String message = String.join("-", "Java", "is", "cool"); 返回 "Java-is-cool"。
     * <p>
     * Note that if an element is null, then {@code "null"} is added.
     * <p>
     * 注意，如果元素为 null，则添加 "null"。
     *
     * @param delimiter the delimiter that separates each element
     * @param elements  the elements to join together.
     * @return a new {@code String} that is composed of the {@code elements}
     * separated by the {@code delimiter}
     * @throws NullPointerException If {@code delimiter} or {@code elements}
     *                              is {@code null}
     * @see java.util.StringJoiner
     * @since 1.8
     */
    public static String join(CharSequence delimiter, CharSequence... elements) {
        // 校验入参
        Objects.requireNonNull(delimiter);
        Objects.requireNonNull(elements);
        // Number of elements not likely worth Arrays.stream overhead.
        // 构造 StringJoiner 对象；内部维护好了一个 StringBuilder 对象
        StringJoiner joiner = new StringJoiner(delimiter);
        for (CharSequence cs : elements) {
            joiner.add(cs);
        }
        return joiner.toString();
    }

    /**
     * Returns a new {@code String} composed of copies of the
     * {@code CharSequence elements} joined together with a copy of the
     * specified {@code delimiter}.
     * <p>
     * 返回一个由指定的 CharSequence 元素组成的新 String，这些元素使用指定的分隔符连接。
     *
     * <blockquote>For example,
     * <pre>{@code
     *     List<String> strings = new LinkedList<>();
     *     strings.add("Java");strings.add("is");
     *     strings.add("cool");
     *     String message = String.join(" ", strings);
     *     //message returned is: "Java is cool"
     *
     *     Set<String> strings = new LinkedHashSet<>();
     *     strings.add("Java"); strings.add("is");
     *     strings.add("very"); strings.add("cool");
     *     String message = String.join("-", strings);
     *     //message returned is: "Java-is-very-cool"
     * }</pre></blockquote>
     * <p>
     * 例如，List<String> strings = new LinkedList<>();
     * strings.add("Java");strings.add("is");
     * strings.add("cool");
     * String message = String.join(" ", strings);
     * 返回 "Java is cool"。
     * <p>
     * Note that if an individual element is {@code null}, then {@code "null"} is added.
     * <p>
     * 注意，如果元素为 null，则添加 "null"。
     *
     * @param delimiter a sequence of characters that is used to separate each
     *                  of the {@code elements} in the resulting {@code String}
     * @param elements  an {@code Iterable} that will have its {@code elements}
     *                  joined together.
     * @return a new {@code String} that is composed from the {@code elements}
     * argument
     * @throws NullPointerException If {@code delimiter} or {@code elements}
     *                              is {@code null}
     * @see #join(CharSequence, CharSequence...)
     * @see java.util.StringJoiner
     * @since 1.8
     */
    public static String join(CharSequence delimiter,
                              Iterable<? extends CharSequence> elements) {
        // 校验入参
        Objects.requireNonNull(delimiter);
        Objects.requireNonNull(elements);
        // 构建 StringJoiner 对象
        StringJoiner joiner = new StringJoiner(delimiter);
        for (CharSequence cs : elements) {
            joiner.add(cs);
        }
        return joiner.toString();
    }

    /**
     * Converts all of the characters in this {@code String} to lower
     * case using the rules of the given {@code Locale}.  Case mapping is based
     * on the Unicode Standard version specified by the {@link java.lang.Character Character}
     * class. Since case mappings are not always 1:1 char mappings, the resulting
     * {@code String} may be a different length than the original {@code String}.
     * <p>
     * 使用给定 Locale 的规则将此 String 中的所有字符转换为小写。
     * Case mapping 基于 Character 类指定的 Unicode 标准版本。
     * 由于 case mappings 并不总是 1:1 char mappings，因此生成的 String 可能与原始 String 长度不同。
     * <p>
     * Examples of lowercase  mappings are in the following table:
     * <table border="1" summary="Lowercase mapping examples showing language code of locale, upper case, lower case, and description">
     * <tr>
     *   <th>Language Code of Locale</th>
     *   <th>Upper Case</th>
     *   <th>Lower Case</th>
     *   <th>Description</th>
     * </tr>
     * <tr>
     *   <td>tr (Turkish)</td>
     *   <td>&#92;u0130</td>
     *   <td>&#92;u0069</td>
     *   <td>capital letter I with dot above -&gt; small letter i</td>
     * </tr>
     * <tr>
     *   <td>tr (Turkish)</td>
     *   <td>&#92;u0049</td>
     *   <td>&#92;u0131</td>
     *   <td>capital letter I -&gt; small letter dotless i </td>
     * </tr>
     * <tr>
     *   <td>(all)</td>
     *   <td>French Fries</td>
     *   <td>french fries</td>
     *   <td>lowercased all chars in String</td>
     * </tr>
     * <tr>
     *   <td>(all)</td>
     *   <td><img src="doc-files/capiota.gif" alt="capiota"><img src="doc-files/capchi.gif" alt="capchi">
     *       <img src="doc-files/captheta.gif" alt="captheta"><img src="doc-files/capupsil.gif" alt="capupsil">
     *       <img src="doc-files/capsigma.gif" alt="capsigma"></td>
     *   <td><img src="doc-files/iota.gif" alt="iota"><img src="doc-files/chi.gif" alt="chi">
     *       <img src="doc-files/theta.gif" alt="theta"><img src="doc-files/upsilon.gif" alt="upsilon">
     *       <img src="doc-files/sigma1.gif" alt="sigma"></td>
     *   <td>lowercased all chars in String</td>
     * </tr>
     * </table>
     *
     * @param locale use the case transformation rules for this locale
     * @return the {@code String}, converted to lowercase.
     * @see java.lang.String#toLowerCase()
     * @see java.lang.String#toUpperCase()
     * @see java.lang.String#toUpperCase(Locale)
     * @since 1.1
     */
    public String toLowerCase(Locale locale) {
        // locale 为 null，抛出异常
        if (locale == null) {
            throw new NullPointerException();
        }

        int firstUpper;
        final int len = value.length;

        /* Now check if there are any characters that need to be changed. */
        // 检查是否有任何需要更改的字符
        scan:
        {
            for (firstUpper = 0; firstUpper < len; ) {
                char c = value[firstUpper];
                // 如果 c 是 Unicode 码点对之一
                if ((c >= Character.MIN_HIGH_SURROGATE)
                        && (c <= Character.MAX_HIGH_SURROGATE)) {
                    // 获取码点对
                    int supplChar = codePointAt(firstUpper);
                    // 如果 supplChar 不是小写字符，就跳出循环
                    if (supplChar != Character.toLowerCase(supplChar)) {
                        break scan;
                    }
                    firstUpper += Character.charCount(supplChar);
                } else {
                    if (c != Character.toLowerCase(c)) {
                        break scan;
                    }
                    firstUpper++;
                }
            }
            // 如果走到了这里，意味着 break scan 没有执行过；即不存在小写字符；那么直接返回即可
            return this;
        }

        // 如果走到了这里，意味着存在小写字符
        char[] result = new char[len];
        // result 数组可能变大，这里记录变大的偏移量
        int resultOffset = 0;  /* result may grow, so i+resultOffset
         * is the write location in result */

        /* Just copy the first few lowerCase characters. */
        // 复制第一个大写字符前面的所有小写字符
        System.arraycopy(value, 0, result, 0, firstUpper);

        String lang = locale.getLanguage();
        boolean localeDependent =
                (lang == "tr" || lang == "az" || lang == "lt");
        char[] lowerCharArray;
        int lowerChar;
        int srcChar;
        int srcCount;
        // 从第一个大写字符开始遍历
        for (int i = firstUpper; i < len; i += srcCount) {
            srcChar = (int) value[i];
            // 确定 srcChar  到底在数组中占了几个位置
            if ((char) srcChar >= Character.MIN_HIGH_SURROGATE
                    && (char) srcChar <= Character.MAX_HIGH_SURROGATE) {
                srcChar = codePointAt(i);
                srcCount = Character.charCount(srcChar);
            } else {
                srcCount = 1;
            }
            // 将 srcChar 转换为小写字符（不考虑它现在是不是小写的）
            if (localeDependent ||
                    srcChar == '\u03A3' || // GREEK CAPITAL LETTER SIGMA
                    srcChar == '\u0130') { // LATIN CAPITAL LETTER I WITH DOT ABOVE
                lowerChar = ConditionalSpecialCasing.toLowerCaseEx(this, i, locale);
            } else {
                lowerChar = Character.toLowerCase(srcChar);
            }
            if ((lowerChar == Character.ERROR)
                    || (lowerChar >= Character.MIN_SUPPLEMENTARY_CODE_POINT)) {
                // 将 char 转换为 char[]
                if (lowerChar == Character.ERROR) {
                    lowerCharArray =
                            ConditionalSpecialCasing.toLowerCaseCharArray(this, i, locale);
                } else if (srcCount == 2) {
                    resultOffset += Character.toChars(lowerChar, result, i + resultOffset) - srcCount;
                    continue;
                } else {
                    lowerCharArray = Character.toChars(lowerChar);
                }

                /* Grow result if needed */
                // 如果 result 数组不够大，就扩容
                int mapLen = lowerCharArray.length;
                // if 成立的条件：toLowerCase 之前的字符数量 < toLowerCase 之后的字符数量；因此 result 数组需要扩容
                if (mapLen > srcCount) {
                    // 现在是在 for 循环中，这种 reallocate 的行为可能多次发生，因此需要考虑性能
                    char[] result2 = new char[result.length + mapLen - srcCount];
                    System.arraycopy(result, 0, result2, 0, i + resultOffset);
                    result = result2;
                }
                // 将 lowerCharArray 中的字符复制到 result 数组中
                for (int x = 0; x < mapLen; ++x) {
                    result[i + resultOffset + x] = lowerCharArray[x];
                }
                // 更新 resultOffset，即记录本次增加的偏移量
                resultOffset += (mapLen - srcCount);
            } else {
                result[i + resultOffset] = (char) lowerChar;
            }
        }
        return new String(result, 0, len + resultOffset);
    }

    /**
     * Converts all of the characters in this {@code String} to lower
     * case using the rules of the default locale. This is equivalent to calling
     * {@code toLowerCase(Locale.getDefault())}.
     * <p>
     * 使用默认语言环境的规则将此 String 中的所有字符转换为小写。
     * 这等效于调用 toLowerCase(Locale.getDefault())。
     * <p>
     * <b>Note:</b> This method is locale sensitive, and may produce unexpected
     * results if used for strings that are intended to be interpreted locale
     * independently.
     * <p>
     * 注意：本方法是区域敏感的，如果用于希望独立于区域设置进行解释的字符串，可能会产生意外的结果。
     * Examples are programming language identifiers, protocol keys, and HTML
     * tags.
     * For instance, {@code "TITLE".toLowerCase()} in a Turkish locale
     * returns {@code "t\u005Cu0131tle"}, where '\u005Cu0131' is the
     * LATIN SMALL LETTER DOTLESS I character.
     * To obtain correct results for locale insensitive strings, use
     * {@code toLowerCase(Locale.ROOT)}.
     * <p>
     *
     * @return the {@code String}, converted to lowercase.
     * @see java.lang.String#toLowerCase(Locale)
     */
    public String toLowerCase() {
        return toLowerCase(Locale.getDefault());
    }

    /**
     * Converts all of the characters in this {@code String} to upper
     * case using the rules of the given {@code Locale}. Case mapping is based
     * on the Unicode Standard version specified by the {@link java.lang.Character Character}
     * class. Since case mappings are not always 1:1 char mappings, the resulting
     * {@code String} may be a different length than the original {@code String}.
     * <p>
     * 使用给定 Locale 的规则将此 String 中的所有字符转换为大写。
     * Case mapping 基于 Character 类指定的 Unicode 标准版本。
     * 由于 case mappings 并不总是 1:1 char mappings，因此生成的 String 可能与原始 String 长度不同。
     * <p>
     * Examples of locale-sensitive and 1:M case mappings are in the following table.
     *
     * <table border="1" summary="Examples of locale-sensitive and 1:M case mappings. Shows Language code of locale, lower case, upper case, and description.">
     * <tr>
     *   <th>Language Code of Locale</th>
     *   <th>Lower Case</th>
     *   <th>Upper Case</th>
     *   <th>Description</th>
     * </tr>
     * <tr>
     *   <td>tr (Turkish)</td>
     *   <td>&#92;u0069</td>
     *   <td>&#92;u0130</td>
     *   <td>small letter i -&gt; capital letter I with dot above</td>
     * </tr>
     * <tr>
     *   <td>tr (Turkish)</td>
     *   <td>&#92;u0131</td>
     *   <td>&#92;u0049</td>
     *   <td>small letter dotless i -&gt; capital letter I</td>
     * </tr>
     * <tr>
     *   <td>(all)</td>
     *   <td>&#92;u00df</td>
     *   <td>&#92;u0053 &#92;u0053</td>
     *   <td>small letter sharp s -&gt; two letters: SS</td>
     * </tr>
     * <tr>
     *   <td>(all)</td>
     *   <td>Fahrvergn&uuml;gen</td>
     *   <td>FAHRVERGN&Uuml;GEN</td>
     *   <td></td>
     * </tr>
     * </table>
     *
     * @param locale use the case transformation rules for this locale
     * @return the {@code String}, converted to uppercase.
     * @see java.lang.String#toUpperCase()
     * @see java.lang.String#toLowerCase()
     * @see java.lang.String#toLowerCase(Locale)
     * @since 1.1
     */
    public String toUpperCase(Locale locale) {
        if (locale == null) {
            throw new NullPointerException();
        }

        int firstLower;
        final int len = value.length;

        /* Now check if there are any characters that need to be changed. */
        scan:
        {
            for (firstLower = 0; firstLower < len; ) {
                int c = (int) value[firstLower];
                int srcCount;
                if ((c >= Character.MIN_HIGH_SURROGATE)
                        && (c <= Character.MAX_HIGH_SURROGATE)) {
                    c = codePointAt(firstLower);
                    srcCount = Character.charCount(c);
                } else {
                    srcCount = 1;
                }
                int upperCaseChar = Character.toUpperCaseEx(c);
                if ((upperCaseChar == Character.ERROR)
                        || (c != upperCaseChar)) {
                    break scan;
                }
                firstLower += srcCount;
            }
            return this;
        }

        /* result may grow, so i+resultOffset is the write location in result */
        int resultOffset = 0;
        char[] result = new char[len]; /* may grow */

        /* Just copy the first few upperCase characters. */
        System.arraycopy(value, 0, result, 0, firstLower);

        String lang = locale.getLanguage();
        boolean localeDependent =
                (lang == "tr" || lang == "az" || lang == "lt");
        char[] upperCharArray;
        int upperChar;
        int srcChar;
        int srcCount;
        for (int i = firstLower; i < len; i += srcCount) {
            srcChar = (int) value[i];
            if ((char) srcChar >= Character.MIN_HIGH_SURROGATE &&
                    (char) srcChar <= Character.MAX_HIGH_SURROGATE) {
                srcChar = codePointAt(i);
                srcCount = Character.charCount(srcChar);
            } else {
                srcCount = 1;
            }
            if (localeDependent) {
                upperChar = ConditionalSpecialCasing.toUpperCaseEx(this, i, locale);
            } else {
                upperChar = Character.toUpperCaseEx(srcChar);
            }
            if ((upperChar == Character.ERROR)
                    || (upperChar >= Character.MIN_SUPPLEMENTARY_CODE_POINT)) {
                if (upperChar == Character.ERROR) {
                    if (localeDependent) {
                        upperCharArray =
                                ConditionalSpecialCasing.toUpperCaseCharArray(this, i, locale);
                    } else {
                        upperCharArray = Character.toUpperCaseCharArray(srcChar);
                    }
                } else if (srcCount == 2) {
                    resultOffset += Character.toChars(upperChar, result, i + resultOffset) - srcCount;
                    continue;
                } else {
                    upperCharArray = Character.toChars(upperChar);
                }

                /* Grow result if needed */
                int mapLen = upperCharArray.length;
                if (mapLen > srcCount) {
                    char[] result2 = new char[result.length + mapLen - srcCount];
                    System.arraycopy(result, 0, result2, 0, i + resultOffset);
                    result = result2;
                }
                for (int x = 0; x < mapLen; ++x) {
                    result[i + resultOffset + x] = upperCharArray[x];
                }
                resultOffset += (mapLen - srcCount);
            } else {
                result[i + resultOffset] = (char) upperChar;
            }
        }
        return new String(result, 0, len + resultOffset);
    }

    /**
     * Converts all of the characters in this {@code String} to upper
     * case using the rules of the default locale. This method is equivalent to
     * {@code toUpperCase(Locale.getDefault())}.
     * <p>
     * 使用默认语言环境的规则将此 String 中的所有字符转换为大写。
     * 这等效于调用 toUpperCase(Locale.getDefault())。
     * <p>
     * <b>Note:</b> This method is locale sensitive, and may produce unexpected
     * results if used for strings that are intended to be interpreted locale
     * independently.
     * <p>
     * 注意：本方法是区域敏感的，如果用于希望独立于区域设置进行解释的字符串，可能会产生意外的结果。
     * Examples are programming language identifiers, protocol keys, and HTML
     * tags.
     * For instance, {@code "title".toUpperCase()} in a Turkish locale
     * returns {@code "T\u005Cu0130TLE"}, where '\u005Cu0130' is the
     * LATIN CAPITAL LETTER I WITH DOT ABOVE character.
     * To obtain correct results for locale insensitive strings, use
     * {@code toUpperCase(Locale.ROOT)}.
     * <p>
     *
     * @return the {@code String}, converted to uppercase.
     * @see java.lang.String#toUpperCase(Locale)
     */
    public String toUpperCase() {
        return toUpperCase(Locale.getDefault());
    }

    /**
     * Returns a string whose value is this string, with any leading and trailing
     * whitespace removed.
     * <p>
     * 返回一个字符串，其值为此字符串，其中移除了任何前导和尾随空格。
     * <p>
     * If this {@code String} object represents an empty character
     * sequence, or the first and last characters of character sequence
     * represented by this {@code String} object both have codes
     * greater than {@code '\u005Cu0020'} (the space character), then a
     * reference to this {@code String} object is returned.
     * <p>
     * 如果此 String 对象表示一个空字符序列，或者此 String 对象表示的字符序列的第一个和最后一个字符的代码都大于 '\u005Cu0020'（空格字符），
     * 则返回对此 String 对象的引用。
     * <p>
     * Otherwise, if there is no character with a code greater than
     * {@code '\u005Cu0020'} in the string, then a
     * {@code String} object representing an empty string is
     * returned.
     * <p>
     * 否则，如果字符串中没有代码大于 '\u005Cu0020' 的字符，则返回表示空字符串的 String 对象。
     * （这里的代码大于 '\u005Cu0020' 的字符，指的是空格字符之外的字符）
     * <p>
     * Otherwise, let <i>k</i> be the index of the first character in the
     * string whose code is greater than {@code '\u005Cu0020'}, and let
     * <i>m</i> be the index of the last character in the string whose code
     * is greater than {@code '\u005Cu0020'}. A {@code String}
     * object is returned, representing the substring of this string that
     * begins with the character at index <i>k</i> and ends with the
     * character at index <i>m</i>-that is, the result of
     * {@code this.substring(k, m + 1)}.
     * <p>
     * 否则，让 k 为字符串中第一个代码大于 '\u005Cu0020' 的字符的索引，让 m 为字符串中最后一个代码大于 '\u005Cu0020' 的字符的索引。
     * 返回一个 String 对象，表示此字符串的子字符串，该子字符串从索引 k 处的字符开始，到索引 m 处的字符结束。
     * （即 this.substring(k, m + 1) 的结果）
     * <p>
     * This method may be used to trim whitespace (as defined above) from
     * the beginning and end of a string.
     * <p>
     * 本方法可用于从字符串的开头和结尾修剪空格（如上所述）。
     *
     * @return A string whose value is this string, with any leading and trailing white
     * space removed, or this string if it has no leading or
     * trailing white space.
     */
    public String trim() {
        int len = value.length;
        int st = 0;
        // 避免获取成员变量时大量的 getfield 指令
        char[] val = value;    /* avoid getfield opcode */

        // 过滤掉前面的空格
        while ((st < len) && (val[st] <= ' ')) {
            st++;
        }
        // 过滤掉后面的空格
        while ((st < len) && (val[len - 1] <= ' ')) {
            len--;
        }
        // 如果前面没有空格，后面也没有空格，就直接返回当前字符串；否则返回过滤后的字符串
        return ((st > 0) || (len < value.length)) ? substring(st, len) : this;
    }

    /**
     * This object (which is already a string!) is itself returned.
     *
     * @return the string itself.
     */
    public String toString() {
        return this;
    }

    /**
     * Converts this string to a new character array.
     * <p>
     * 将此字符串转换为一个新的字符数组。
     *
     * @return a newly allocated character array whose length is the length
     * of this string and whose contents are initialized to contain
     * the character sequence represented by this string.
     */
    public char[] toCharArray() {
        // Cannot use Arrays.copyOf because of class initialization order issues
        // 不能使用 Arrays.copyOf，因为存在类初始化顺序问题
        // 定义一个新的数组，并将 value 数组中的元素复制到新数组中
        char result[] = new char[value.length];
        System.arraycopy(value, 0, result, 0, value.length);
        return result;
    }

    /**
     * Returns a formatted string using the specified format string and
     * arguments.
     * <p>
     * 使用指定的格式字符串和参数返回一个格式化的字符串。
     *
     * <p> The locale always used is the one returned by {@link
     * java.util.Locale#getDefault() Locale.getDefault()}.
     * <p>
     * 始终使用的语言环境是由 Locale.getDefault() 返回的语言环境。
     *
     * @param format A <a href="../util/Formatter.html#syntax">format string</a>
     * @param args   Arguments referenced by the format specifiers in the format
     *               string.  If there are more arguments than format specifiers, the
     *               extra arguments are ignored.  The number of arguments is
     *               variable and may be zero.  The maximum number of arguments is
     *               limited by the maximum dimension of a Java array as defined by
     *               <cite>The Java&trade; Virtual Machine Specification</cite>.
     *               The behaviour on a
     *               {@code null} argument depends on the <a
     *               href="../util/Formatter.html#syntax">conversion</a>.
     * @return A formatted string
     * @throws java.util.IllegalFormatException If a format string contains an illegal syntax, a format
     *                                          specifier that is incompatible with the given arguments,
     *                                          insufficient arguments given the format string, or other
     *                                          illegal conditions.  For specification of all possible
     *                                          formatting errors, see the <a
     *                                          href="../util/Formatter.html#detail">Details</a> section of the
     *                                          formatter class specification.
     * @see java.util.Formatter
     * @since 1.5
     */
    public static String format(String format, Object... args) {
        return new Formatter().format(format, args).toString();
    }

    /**
     * Returns a formatted string using the specified locale, format string,
     * and arguments.
     * <p>
     * 使用指定的语言环境、格式字符串和参数返回一个格式化的字符串。
     *
     * @param l      The {@linkplain java.util.Locale locale} to apply during
     *               formatting.  If {@code l} is {@code null} then no localization
     *               is applied.
     * @param format A <a href="../util/Formatter.html#syntax">format string</a>
     * @param args   Arguments referenced by the format specifiers in the format
     *               string.  If there are more arguments than format specifiers, the
     *               extra arguments are ignored.  The number of arguments is
     *               variable and may be zero.  The maximum number of arguments is
     *               limited by the maximum dimension of a Java array as defined by
     *               <cite>The Java&trade; Virtual Machine Specification</cite>.
     *               The behaviour on a
     *               {@code null} argument depends on the
     *               <a href="../util/Formatter.html#syntax">conversion</a>.
     * @return A formatted string
     * @throws java.util.IllegalFormatException If a format string contains an illegal syntax, a format
     *                                          specifier that is incompatible with the given arguments,
     *                                          insufficient arguments given the format string, or other
     *                                          illegal conditions.  For specification of all possible
     *                                          formatting errors, see the <a
     *                                          href="../util/Formatter.html#detail">Details</a> section of the
     *                                          formatter class specification
     * @see java.util.Formatter
     * @since 1.5
     */
    public static String format(Locale l, String format, Object... args) {
        return new Formatter(l).format(format, args).toString();
    }

    /**
     * Returns the string representation of the {@code Object} argument.
     * <p>
     * 返回 Object 参数的字符串表示形式。
     *
     * @param obj an {@code Object}.
     * @return if the argument is {@code null}, then a string equal to
     * {@code "null"}; otherwise, the value of
     * {@code obj.toString()} is returned.
     * @see java.lang.Object#toString()
     */
    public static String valueOf(Object obj) {
        return (obj == null) ? "null" : obj.toString();
    }

    /**
     * Returns the string representation of the {@code char} array
     * argument. The contents of the character array are copied; subsequent
     * modification of the character array does not affect the returned
     * string.
     * <p>
     * 返回 char 数组参数的字符串表示形式。
     * 字符数组的内容被复制；字符数组的后续修改不会影响返回的字符串。
     *
     * @param data the character array.
     * @return a {@code String} that contains the characters of the
     * character array.
     */
    public static String valueOf(char data[]) {
        return new String(data);
    }

    /**
     * Returns the string representation of a specific subarray of the
     * {@code char} array argument.
     * <p>
     * 返回 char 数组参数的特定子数组的字符串表示形式。
     * <p>
     * The {@code offset} argument is the index of the first
     * character of the subarray. The {@code count} argument
     * specifies the length of the subarray. The contents of the subarray
     * are copied; subsequent modification of the character array does not
     * affect the returned string.
     * <p>
     * offset 参数是子数组的第一个字符的索引。
     * count 参数指定子数组的长度。
     * 子数组的内容被复制；字符数组的后续修改不会影响返回的字符串。
     *
     * @param data   the character array.
     * @param offset initial offset of the subarray.
     * @param count  length of the subarray.
     * @return a {@code String} that contains the characters of the
     * specified subarray of the character array.
     * @throws IndexOutOfBoundsException if {@code offset} is
     *                                   negative, or {@code count} is negative, or
     *                                   {@code offset+count} is larger than
     *                                   {@code data.length}.
     */
    public static String valueOf(char data[], int offset, int count) {
        return new String(data, offset, count);
    }

    /**
     * Equivalent to {@link #valueOf(char[], int, int)}.
     * <p>
     * 等效于 valueOf(char[], int, int)。
     *
     * @param data   the character array.
     * @param offset initial offset of the subarray.
     * @param count  length of the subarray.
     * @return a {@code String} that contains the characters of the
     * specified subarray of the character array.
     * @throws IndexOutOfBoundsException if {@code offset} is
     *                                   negative, or {@code count} is negative, or
     *                                   {@code offset+count} is larger than
     *                                   {@code data.length}.
     */
    public static String copyValueOf(char data[], int offset, int count) {
        return new String(data, offset, count);
    }

    /**
     * Equivalent to {@link #valueOf(char[])}.
     * <p>
     * 等效于 valueOf(char[])。
     *
     * @param data the character array.
     * @return a {@code String} that contains the characters of the
     * character array.
     */
    public static String copyValueOf(char data[]) {
        return new String(data);
    }

    /**
     * Returns the string representation of the {@code boolean} argument.
     * <p>
     * 返回 boolean 参数的字符串表示形式。
     *
     * @param b a {@code boolean}.
     * @return if the argument is {@code true}, a string equal to
     * {@code "true"} is returned; otherwise, a string equal to
     * {@code "false"} is returned.
     */
    public static String valueOf(boolean b) {
        return b ? "true" : "false";
    }

    /**
     * Returns the string representation of the {@code char}
     * argument.
     * <p>
     * 返回 char 参数的字符串表示形式。
     *
     * @param c a {@code char}.
     * @return a string of length {@code 1} containing
     * as its single character the argument {@code c}.
     */
    public static String valueOf(char c) {
        char data[] = {c};
        return new String(data, true);
    }

    /**
     * Returns the string representation of the {@code int} argument.
     * <p>
     * 返回 int 参数的字符串表示形式。
     * <p>
     * The representation is exactly the one returned by the
     * {@code Integer.toString} method of one argument.
     * <p>
     * 表示形式与 Integer.toString(int) 方法的返回值完全相同。
     *
     * @param i an {@code int}.
     * @return a string representation of the {@code int} argument.
     * @see java.lang.Integer#toString(int, int)
     */
    public static String valueOf(int i) {
        return Integer.toString(i);
    }

    /**
     * Returns the string representation of the {@code long} argument.
     * <p>
     * The representation is exactly the one returned by the
     * {@code Long.toString} method of one argument.
     *
     * @param l a {@code long}.
     * @return a string representation of the {@code long} argument.
     * @see java.lang.Long#toString(long)
     */
    public static String valueOf(long l) {
        return Long.toString(l);
    }

    /**
     * Returns the string representation of the {@code float} argument.
     * <p>
     *     返回 float 参数的字符串表示形式。
     * <p>
     * The representation is exactly the one returned by the
     * {@code Float.toString} method of one argument.
     *
     * @param f a {@code float}.
     * @return a string representation of the {@code float} argument.
     * @see java.lang.Float#toString(float)
     */
    public static String valueOf(float f) {
        return Float.toString(f);
    }

    /**
     * Returns the string representation of the {@code double} argument.
     * <p>
     * The representation is exactly the one returned by the
     * {@code Double.toString} method of one argument.
     *
     * @param d a {@code double}.
     * @return a  string representation of the {@code double} argument.
     * @see java.lang.Double#toString(double)
     */
    public static String valueOf(double d) {
        return Double.toString(d);
    }

    /**
     * Returns a canonical representation for the string object.
     * <p>
     * 返回字符串对象的规范表示形式。
     * <p>
     * A pool of strings, initially empty, is maintained privately by the
     * class {@code String}.
     * <p>
     * 一个字符串池，最初为空，由 String 类私有地维护。
     * <p>
     * When the intern method is invoked, if the pool already contains a
     * string equal to this {@code String} object as determined by
     * the {@link #equals(Object)} method, then the string from the pool is
     * returned. Otherwise, this {@code String} object is added to the
     * pool and a reference to this {@code String} object is returned.
     * <p>
     * 当 intern 方法被调用时，如果池中已经包含一个与此 String 对象相等的字符串（由 equals(Object) 方法确定），则返回池中的字符串。
     * 否则，将此 String 对象添加到池中，并返回对此 String 对象的引用。
     * <p>
     * It follows that for any two strings {@code s} and {@code t},
     * {@code s.intern() == t.intern()} is {@code true}
     * if and only if {@code s.equals(t)} is {@code true}.
     * <p>
     * 因此，对于任意两个字符串 s 和 t，当且仅当 s.equals(t) 为 true 时，s.intern() == t.intern() 才为 true。
     * <p>
     * All literal strings and string-valued constant expressions are
     * interned. String literals are defined in section 3.10.5 of the
     * <cite>The Java&trade; Language Specification</cite>.
     * <p>
     * 所有字面字符串和字符串值常量表达式都是 interned。
     * 字符串字面量定义在 Java 语言规范的 3.10.5 节中。
     *
     * @return a string that has the same contents as this string, but is
     * guaranteed to be from a pool of unique strings.
     */
    public native String intern();
    // 它会在运行时常量池中查找是否存在一份等值的字符串；可以帮助我们节省内存，特别是在大量重复字符串的场景中
    // 但是，使用 intern() 方法会消耗一定的时间，特别是在处理大量字符串的时候
    // 不同版本的 jdk 的实现可能不同
    // 过度使用 intern() 方法有可能导致字符串常量池溢出
}
