/*
 * Copyright (c) 2003, 2013, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

import java.util.Iterator;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

/**
 * Implementing this interface allows an object to be the target of
 * the "for-each loop" statement. See
 * <strong>
 * <a href="{@docRoot}/../technotes/guides/language/foreach.html">For-each Loop</a>
 * </strong>
 *
 * <p>
 * 实现此接口允许对象成为“for-each循环”语句的目标。
 *
 * @param <T> the type of elements returned by the iterator
 * @jls 14.14.2 The enhanced for statement
 * @since 1.5
 */
public interface Iterable<T> {
    /**
     * Returns an iterator over elements of type {@code T}.
     * <p>
     * 返回一个迭代器，用于迭代元素
     *
     * @return an Iterator.
     */
    Iterator<T> iterator();

    /**
     * Performs the given action for each element of the {@code Iterable}
     * until all elements have been processed or the action throws an
     * exception.  Unless otherwise specified by the implementing class,
     * actions are performed in the order of iteration (if an iteration order
     * is specified).  Exceptions thrown by the action are relayed to the
     * caller.
     * <p>
     *     对于每个元素执行给定的操作，直到所有元素都被处理或操作抛出异常。
     *     除非实现类另有规定，否则操作按迭代顺序执行（如果指定了迭代顺序）。
     *     操作抛出的异常被中继给调用者。
     *
     * @param action The action to be performed for each element
     * @throws NullPointerException if the specified action is null
     * @implSpec <p>The default implementation behaves as if:
     * <pre>{@code
     *     for (T t : this)
     *         action.accept(t);
     * }</pre>
     * @since 1.8
     */
    default void forEach(Consumer<? super T> action) {
        Objects.requireNonNull(action);
        for (T t : this) {
            action.accept(t);
        }
    }

    /**
     * Creates a {@link Spliterator} over the elements described by this
     * {@code Iterable}.
     * <p>
     *     在此 Iterable 描述的元素上创建 Spliterator。
     *
     *     Spliterator 初始时和 iterator 差不多，都能迭代元素，但是它还能对迭代器进行 split。
     *     eg：
     *     // 将 list 转化为一个 Spliterator
     *     Spliterator spliterator = list.spliterator();
     *     // 将 spliterator 分割成两个，至于具体怎么分割，取决于具体的实现类
     *     Spliterator spliterator1 = spliterator.trySplit();
     *
     * @return a {@code Spliterator} over the elements described by this
     * {@code Iterable}.
     * @implSpec The default implementation creates an
     * <em><a href="Spliterator.html#binding">early-binding</a></em>
     * spliterator from the iterable's {@code Iterator}.  The spliterator
     * inherits the <em>fail-fast</em> properties of the iterable's iterator.
     * @implNote The default implementation should usually be overridden.  The
     * spliterator returned by the default implementation has poor splitting
     * capabilities, is unsized, and does not report any spliterator
     * characteristics. Implementing classes can nearly always provide a
     * better implementation.
     * @since 1.8
     */
    default Spliterator<T> spliterator() {
        return Spliterators.spliteratorUnknownSize(iterator(), 0);
    }
}
