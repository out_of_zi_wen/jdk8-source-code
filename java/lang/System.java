/*
 * Copyright (c) 1994, 2022, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
package java.lang;

import sun.nio.ch.Interruptible;
import sun.reflect.CallerSensitive;
import sun.reflect.Reflection;
import sun.reflect.annotation.AnnotationType;
import sun.security.util.SecurityConstants;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Console;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Executable;
import java.nio.channels.Channel;
import java.nio.channels.spi.SelectorProvider;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Map;
import java.util.Properties;
import java.util.PropertyPermission;

/**
 * The <code>System</code> class contains several useful class fields
 * and methods. It cannot be instantiated.
 * <p>
 * System 类包含了一些有用的类字段和方法，不能被实例化。
 *
 * <p>Among the facilities provided by the <code>System</code> class
 * are standard input, standard output, and error output streams;
 * access to externally defined properties and environment
 * variables; a means of loading files and libraries; and a utility
 * method for quickly copying a portion of an array.
 * <p>
 * System 类提供的功能包括
 * - 标准输入、标准输出、标准错误输出流；
 * - 访问外部定义的属性和环境变量；
 * - 加载文件和库的方法；
 * - 快速复制数组的工具方法。
 *
 * @author unascribed
 * @since JDK1.0
 */
public final class System {

    /* register the natives via the static initializer.
     *
     * VM will invoke the initializeSystemClass method to complete
     * the initialization for this class separated from clinit.
     * Note that to use properties set by the VM, see the constraints
     * described in the initializeSystemClass method.
     *
     * 通过 静态初始化器 注册本地方法。
     * VM 将调用 initializeSystemClass 方法来完成这个类的初始化，与 clinit 分开。
     * 注意，要使用 VM 设置的属性，请参阅 initializeSystemClass 方法中描述的约束。
     */
    private static native void registerNatives();

    static {
        // 静态代码块，注册本地方法
        registerNatives();
    }

    /**
     * Don't let anyone instantiate this class
     */
    private System() {
    }

    /**
     * The "standard" input stream. This stream is already
     * open and ready to supply input data. Typically this stream
     * corresponds to keyboard input or another input source specified by
     * the host environment or user.
     * <p>
     * 标准输入流，已经打开并准备好提供输入数据。
     * 通常，这个流对应于键盘输入或者其他由宿主环境或用户指定的输入源。
     */
    public final static InputStream in = null;

    /**
     * The "standard" output stream. This stream is already
     * open and ready to accept output data. Typically this stream
     * corresponds to display output or another output destination
     * specified by the host environment or user.
     * <p>
     * 标准输出流，已经打开并准备好接受输出数据。
     * 通常，这个流对应于显示输出或者其他由宿主环境或用户指定的输出目标。
     * <p>
     * For simple stand-alone Java applications, a typical way to write
     * a line of output data is:
     * <blockquote><pre>
     *     System.out.println(data)
     * </pre></blockquote>
     * <p>
     * 对于简单的独立 Java 应用程序，输出一行数据的典型方式是：
     * System.out.println(data)
     * See the <code>println</code> methods in class <code>PrintStream</code>.
     *
     * @see java.io.PrintStream#println()
     * @see java.io.PrintStream#println(boolean)
     * @see java.io.PrintStream#println(char)
     * @see java.io.PrintStream#println(char[])
     * @see java.io.PrintStream#println(double)
     * @see java.io.PrintStream#println(float)
     * @see java.io.PrintStream#println(int)
     * @see java.io.PrintStream#println(long)
     * @see java.io.PrintStream#println(java.lang.Object)
     * @see java.io.PrintStream#println(java.lang.String)
     */
    public final static PrintStream out = null;

    /**
     * The "standard" error output stream. This stream is already
     * open and ready to accept output data.
     * <p>
     * 标准错误输出流，已经打开并准备好接受输出数据。
     * <p>
     * Typically this stream corresponds to display output or another
     * output destination specified by the host environment or user. By
     * convention, this output stream is used to display error messages
     * or other information that should come to the immediate attention
     * of a user even if the principal output stream, the value of the
     * variable <code>out</code>, has been redirected to a file or other
     * destination that is typically not continuously monitored.
     * <p>
     * 通常，这个流对应于显示输出或者其他由宿主环境或用户指定的输出目标。
     * 按照惯例，这个输出流用于显示错误消息或者其他信息，即使主输出流（变量 out 的值）已经被重定向到一个文件或者其他目标(通常不会持续监视)。
     */
    public final static PrintStream err = null;

    /* The security manager for the system.
     */
    private static volatile SecurityManager security = null;

    /**
     * Reassigns the "standard" input stream.
     * <p>
     * 重新分配标准输入流。
     *
     * <p>First, if there is a security manager, its <code>checkPermission</code>
     * method is called with a <code>RuntimePermission("setIO")</code> permission
     * to see if it's ok to reassign the "standard" input stream.
     * <p>
     * 首先，如果有安全管理器，那么它的 checkPermission 方法会被调用，传入 RuntimePermission("setIO") 权限，
     * 来判断是否可以重新分配标准输入流。
     *
     * @param in the new standard input stream.
     * @throws SecurityException if a security manager exists and its
     *                           <code>checkPermission</code> method doesn't allow
     *                           reassigning of the standard input stream.
     * @see SecurityManager#checkPermission
     * @see java.lang.RuntimePermission
     * @since JDK1.1
     */
    public static void setIn(InputStream in) {
        checkIO();
        setIn0(in);
    }

    /**
     * Reassigns the "standard" output stream.
     * <p>
     * 重新设置标准输出流。
     *
     * <p>First, if there is a security manager, its <code>checkPermission</code>
     * method is called with a <code>RuntimePermission("setIO")</code> permission
     * to see if it's ok to reassign the "standard" output stream.
     * <p>
     * 首先，如果有安全管理器，那么它的 checkPermission 方法会被调用，传入 RuntimePermission("setIO") 权限，
     * 来判断是否可以重新分配标准输出流。
     *
     * @param out the new standard output stream
     * @throws SecurityException if a security manager exists and its
     *                           <code>checkPermission</code> method doesn't allow
     *                           reassigning of the standard output stream.
     * @see SecurityManager#checkPermission
     * @see java.lang.RuntimePermission
     * @since JDK1.1
     */
    public static void setOut(PrintStream out) {
        checkIO();
        setOut0(out);
    }

    /**
     * Reassigns the "standard" error output stream.
     * <p>
     * 重新设置标准错误输出流。
     *
     * <p>First, if there is a security manager, its <code>checkPermission</code>
     * method is called with a <code>RuntimePermission("setIO")</code> permission
     * to see if it's ok to reassign the "standard" error output stream.
     *
     * @param err the new standard error output stream.
     * @throws SecurityException if a security manager exists and its
     *                           <code>checkPermission</code> method doesn't allow
     *                           reassigning of the standard error output stream.
     * @see SecurityManager#checkPermission
     * @see java.lang.RuntimePermission
     * @since JDK1.1
     */
    public static void setErr(PrintStream err) {
        checkIO();
        setErr0(err);
    }

    private static volatile Console cons = null;

    /**
     * Returns the unique {@link java.io.Console Console} object associated
     * with the current Java virtual machine, if any.
     * <p>
     * 返回与当前 Java 虚拟机关联的唯一 Console 对象（如果有）。
     *
     * @return The system console, if any, otherwise <tt>null</tt>.
     * @since 1.6
     */
    public static Console console() {
        if (cons == null) {
            synchronized (System.class) {
                cons = sun.misc.SharedSecrets.getJavaIOAccess().console();
            }
        }
        return cons;
    }

    /**
     * Returns the channel inherited from the entity that created this
     * Java virtual machine.
     * <p>
     * 返回从创建此 Java 虚拟机的实体继承的 channel。
     *
     * <p> This method returns the channel obtained by invoking the
     * {@link java.nio.channels.spi.SelectorProvider#inheritedChannel
     * inheritedChannel} method of the system-wide default
     * {@link java.nio.channels.spi.SelectorProvider} object. </p>
     * <p>
     * 该方法返回通过调用系统范围内默认的 SelectorProvider 对象的 inheritedChannel 方法获得的 channel。
     *
     * <p> In addition to the network-oriented channels described in
     * {@link java.nio.channels.spi.SelectorProvider#inheritedChannel
     * inheritedChannel}, this method may return other kinds of
     * channels in the future.
     * <p>
     * 除了 inheritedChannel 中描述的面向网络的 channel，该方法将来可能会返回其他类型的 channel。
     *
     * @return The inherited channel, if any, otherwise <tt>null</tt>.
     * @throws IOException       If an I/O error occurs
     * @throws SecurityException If a security manager is present and it does not
     *                           permit access to the channel.
     * @since 1.5
     */
    public static Channel inheritedChannel() throws IOException {
        return SelectorProvider.provider().inheritedChannel();
    }

    private static void checkIO() {
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("setIO"));
        }
    }

    private static native void setIn0(InputStream in);

    private static native void setOut0(PrintStream out);

    private static native void setErr0(PrintStream err);

    /**
     * Sets the System security.
     * <p>
     * 设置系统安全。
     *
     * <p> If there is a security manager already installed, this method first
     * calls the security manager's <code>checkPermission</code> method
     * with a <code>RuntimePermission("setSecurityManager")</code>
     * permission to ensure it's ok to replace the existing
     * security manager.
     * This may result in throwing a <code>SecurityException</code>.
     * <p>
     * 如果已经设置了security manager，那么该方法首先调用安全管理器的 checkPermission 方法，
     * 传入 RuntimePermission("setSecurityManager") 权限，来确保可以替换现有的安全管理器。
     * 这可能会导致抛出 SecurityException。
     *
     * <p> Otherwise, the argument is established as the current
     * security manager. If the argument is <code>null</code> and no
     * security manager has been established, then no action is taken and
     * the method simply returns.
     * <p>
     * 否则，参数被设置为当前的安全管理器。
     * 如果参数为 null 并且没有设置安全管理器，那么不会执行任何操作，该方法直接返回。
     * <p>
     * 实际上，System 只是提供一个查询和设置的途径，并且缓存一个 security manager 的引用。
     * 真实的设置实在 AccessController 中完成的（set 和 check 都是），System 仅仅是 AccessController 的一个封装
     *
     * @param s the security manager.
     * @throws SecurityException if the security manager has already
     *                           been set and its <code>checkPermission</code> method
     *                           doesn't allow it to be replaced.
     * @see #getSecurityManager
     * @see SecurityManager#checkPermission
     * @see java.lang.RuntimePermission
     */
    public static void setSecurityManager(final SecurityManager s) {
        try {
            s.checkPackageAccess("java.lang");
        } catch (Exception e) {
            // no-op
        }
        setSecurityManager0(s);
    }

    private static synchronized void setSecurityManager0(final SecurityManager s) {
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            // ask the currently installed security manager if we
            // can replace it.
            // 询问当前设置的 security manager 是否可以替换它。
            sm.checkPermission(new RuntimePermission
                    ("setSecurityManager"));
        }

        if ((s != null) && (s.getClass().getClassLoader() != null)) {
            // New security manager class is not on bootstrap classpath.
            // Cause policy to get initialized before we install the new
            // security manager, in order to prevent infinite loops when
            // trying to initialize the policy (which usually involves
            // accessing some security and/or system properties, which in turn
            // calls the installed security manager's checkPermission method
            // which will loop infinitely if there is a non-system class
            // (in this case: the new security manager class) on the stack).
            AccessController.doPrivileged(new PrivilegedAction<Object>() {
                public Object run() {
                    s.getClass().getProtectionDomain().implies
                            (SecurityConstants.ALL_PERMISSION);
                    return null;
                }
            });
        }

        security = s;
    }

    /**
     * Gets the system security interface.
     * <p>
     * 获取已经被设置了的 security manager。
     *
     * @return if a security manager has already been established for the
     * current application, then that security manager is returned;
     * otherwise, <code>null</code> is returned.
     * @see #setSecurityManager
     */
    public static SecurityManager getSecurityManager() {
        return security;
    }

    /**
     * Returns the current time in milliseconds.  Note that
     * while the unit of time of the return value is a millisecond,
     * the granularity of the value depends on the underlying
     * operating system and may be larger.  For example, many
     * operating systems measure time in units of tens of
     * milliseconds.
     * <p>
     * 返回当前时间的毫秒数。
     * 注意，虽然返回值的时间单位是毫秒，但是值的粒度取决于底层操作系统，可能更大。
     * 例如，许多操作系统以十毫秒为单位测量时间。
     *
     * <p> See the description of the class <code>Date</code> for
     * a discussion of slight discrepancies that may arise between
     * "computer time" and coordinated universal time (UTC).
     * <p>
     * 请参阅类 Date 的描述，讨论“计算机时间”和协调世界时（UTC）之间可能出现的轻微差异。
     *
     * @return the difference, measured in milliseconds, between
     * the current time and midnight, January 1, 1970 UTC.
     * @see java.util.Date
     */
    public static native long currentTimeMillis();

    /**
     * Returns the current value of the running Java Virtual Machine's
     * high-resolution time source, in nanoseconds.
     * <p>
     * 返回正在运行的 Java 虚拟机的高分辨率时间源的当前值（以纳秒为单位）。
     *
     * <p>This method can only be used to measure elapsed time and is
     * not related to any other notion of system or wall-clock time.
     * The value returned represents nanoseconds since some fixed but
     * arbitrary <i>origin</i> time (perhaps in the future, so values
     * may be negative).  The same origin is used by all invocations of
     * this method in an instance of a Java virtual machine; other
     * virtual machine instances are likely to use a different origin.
     * <p>
     * 该方法只能用于计算时间差时使用，它与任何其他系统或挂钟时间的概念无关。
     * 返回的值表示自某个固定但任意的起源时间（可能在未来，因此值可能为负）以来的纳秒数。
     * Java 虚拟机实例中该方法的所有调用都会使用同一起源时间；其他而不同的虚拟机实例可能使用不同的起源时间。
     *
     * <p>This method provides nanosecond precision, but not necessarily
     * nanosecond resolution (that is, how frequently the value changes)
     * - no guarantees are made except that the resolution is at least as
     * good as that of {@link #currentTimeMillis()}.
     * <p>
     * 该方法提供纳秒精度，但不一定提供纳秒的变更频率（即值更改的频率）——除了变更频率至少与 currentTimeMillis() 一样好之外，不提供任何保证。
     *
     * <p>Differences in successive calls that span greater than
     * approximately 292 years (2<sup>63</sup> nanoseconds) will not
     * correctly compute elapsed time due to numerical overflow.
     * <p>
     * 跨越大约 292 年（2^63 纳秒）的连续调用之间的差异将无法正确计算经过的时间，因为会发生数值溢出。
     *
     * <p>The values returned by this method become meaningful only when
     * the difference between two such values, obtained within the same
     * instance of a Java virtual machine, is computed.
     * <p>
     * 该方法返回的值只有在计算在同一 Java 虚拟机实例中获得的两个这样的值之间的差异时才有意义。
     *
     * <p> For example, to measure how long some code takes to execute:
     * <pre> {@code
     * long startTime = System.nanoTime();
     * // ... the code being measured ...
     * long estimatedTime = System.nanoTime() - startTime;}</pre>
     *
     * <p>To compare two nanoTime values
     * <pre> {@code
     * long t0 = System.nanoTime();
     * ...
     * long t1 = System.nanoTime();}</pre>
     * <p>
     * one should use {@code t1 - t0 < 0}, not {@code t1 < t0},
     * because of the possibility of numerical overflow.
     *
     * @return the current value of the running Java Virtual Machine's
     * high-resolution time source, in nanoseconds
     * @since 1.5
     */
    public static native long nanoTime();

    /**
     * Copies an array from the specified source array, beginning at the
     * specified position, to the specified position of the destination array.
     * A subsequence of array components are copied from the source
     * array referenced by <code>src</code> to the destination array
     * referenced by <code>dest</code>. The number of components copied is
     * equal to the <code>length</code> argument. The components at
     * positions <code>srcPos</code> through
     * <code>srcPos+length-1</code> in the source array are copied into
     * positions <code>destPos</code> through
     * <code>destPos+length-1</code>, respectively, of the destination
     * array.
     * <p>
     * 从指定的源数组复制一个数组，从指定的位置开始，到目标数组的指定位置。
     * 从源数组（src）引用的数组元素的子序列被复制到目标数组（dest）引用的数组中。
     * 被复制的元素数量等于 length 参数。
     * 源数组中位置 srcPos 到 srcPos+length-1 的元素分别被复制到目标数组中位置 destPos 到 destPos+length-1。
     * <p>
     * If the <code>src</code> and <code>dest</code> arguments refer to the
     * same array object, then the copying is performed as if the
     * components at positions <code>srcPos</code> through
     * <code>srcPos+length-1</code> were first copied to a temporary
     * array with <code>length</code> components and then the contents of
     * the temporary array were copied into positions
     * <code>destPos</code> through <code>destPos+length-1</code> of the
     * destination array.
     * <p>
     * 如果 src 和 dest 参数引用同一个数组对象，
     * 那么复制的过程就好像首先将源数组中位置 srcPos 到 srcPos+length-1 的元素复制到一个具有 length 个元素的临时数组中，
     * 然后将临时数组的内容复制到目标数组中位置 destPos 到 destPos + length - 1。
     * <p>
     * If <code>dest</code> is <code>null</code>, then a
     * <code>NullPointerException</code> is thrown.
     * <p>
     * 如果 dest 是 null，那么抛出 NullPointerException。
     * <p>
     * If <code>src</code> is <code>null</code>, then a
     * <code>NullPointerException</code> is thrown and the destination
     * array is not modified.
     * <p>
     * 如果 src 是 null，那么抛出 NullPointerException，目标数组不会被修改。
     * <p>
     * Otherwise, if any of the following is true, an
     * <code>ArrayStoreException</code> is thrown and the destination is
     * not modified:
     * <ul>
     * <li>The <code>src</code> argument refers to an object that is not an
     *     array.
     * <li>The <code>dest</code> argument refers to an object that is not an
     *     array.
     * <li>The <code>src</code> argument and <code>dest</code> argument refer
     *     to arrays whose component types are different primitive types.
     * <li>The <code>src</code> argument refers to an array with a primitive
     *    component type and the <code>dest</code> argument refers to an array
     *     with a reference component type.
     * <li>The <code>src</code> argument refers to an array with a reference
     *    component type and the <code>dest</code> argument refers to an array
     *     with a primitive component type.
     * </ul>
     * <p>
     *     否则，如果满足以下任何条件，抛出 ArrayStoreException，目标数组不会被修改：
     *     - src 参数引用的对象不是数组。
     *     - dest 参数引用的对象不是数组。
     *     - src 参数和 dest 参数引用的数组的元素类型是不同的原始类型。
     *     - src 参数引用的数组具有原始元素类型，dest 参数引用的数组具有引用元素类型。
     *     - src 参数引用的数组具有引用元素类型，dest 参数引用的数组具有原始元素类型。
     * <p>
     * Otherwise, if any of the following is true, an
     * <code>IndexOutOfBoundsException</code> is
     * thrown and the destination is not modified:
     * <ul>
     * <li>The <code>srcPos</code> argument is negative.
     * <li>The <code>destPos</code> argument is negative.
     * <li>The <code>length</code> argument is negative.
     * <li><code>srcPos+length</code> is greater than
     *     <code>src.length</code>, the length of the source array.
     * <li><code>destPos+length</code> is greater than
     *     <code>dest.length</code>, the length of the destination array.
     * </ul>
     * <p>
     *     否则，如果满足以下任何条件，抛出 IndexOutOfBoundsException，目标数组不会被修改：
     *     - srcPos 参数是负数。
     *     - destPos 参数是负数。
     *     - length 参数是负数。
     *     - srcPos+length 大于 src.length，源数组的长度。
     *     - destPos+length 大于 dest.length，目标数组的长度。
     * <p>
     * Otherwise, if any actual component of the source array from
     * position <code>srcPos</code> through
     * <code>srcPos+length-1</code> cannot be converted to the component
     * type of the destination array by assignment conversion, an
     * <code>ArrayStoreException</code> is thrown. In this case, let
     * <b><i>k</i></b> be the smallest nonnegative integer less than
     * length such that <code>src[srcPos+</code><i>k</i><code>]</code>
     * cannot be converted to the component type of the destination
     * array; when the exception is thrown, source array components from
     * positions <code>srcPos</code> through
     * <code>srcPos+</code><i>k</i><code>-1</code>
     * will already have been copied to destination array positions
     * <code>destPos</code> through
     * <code>destPos+</code><i>k</I><code>-1</code> and no other
     * positions of the destination array will have been modified.
     * (Because of the restrictions already itemized, this
     * paragraph effectively applies only to the situation where both
     * arrays have component types that are reference types.)
     * <p>
     *     否则，如果源数组从 srcPos 到 srcPos+length-1 的任何实际元素都不能通过赋值转换转换为目标数组的元素类型，那么抛出 ArrayStoreException。
     *     在这种情况下，让 k 是小于 length 的最小非负整数，使得 src[srcPos+k] 不能转换为目标数组的元素类型；
     *     当抛出异常时，源数组从 srcPos 到 srcPos+k-1 的元素已经被复制到目标数组的位置 destPos 到 destPos+k-1，并且目标数组的其他位置都没有被修改。
     *     （由于已经列出的限制，该段落实际上仅适用于两个数组都具有引用类型的组件类型的情况。） 也就是说，该方法在抛出异常时，不会回滚已经复制的元素。
     *
     * @param src     the source array.
     * @param srcPos  starting position in the source array.
     * @param dest    the destination array.
     * @param destPos starting position in the destination data.
     * @param length  the number of array elements to be copied.
     * @throws IndexOutOfBoundsException if copying would cause
     *                                   access of data outside array bounds.
     * @throws ArrayStoreException       if an element in the <code>src</code>
     *                                   array could not be stored into the <code>dest</code> array
     *                                   because of a type mismatch.
     * @throws NullPointerException      if either <code>src</code> or
     *                                   <code>dest</code> is <code>null</code>.
     */
    public static native void arraycopy(Object src, int srcPos,
                                        Object dest, int destPos,
                                        int length);

    /**
     * Returns the same hash code for the given object as
     * would be returned by the default method hashCode(),
     * whether or not the given object's class overrides
     * hashCode().
     * The hash code for the null reference is zero.
     * <p>
     * 返回与给定对象相同的哈希码，该哈希码与默认 Object 中的方法 hashCode() 返回的哈希码相同，
     * 无论给定对象的类是否重写了 hashCode()。
     *
     * @param x object for which the hashCode is to be calculated
     * @return the hashCode
     * @since JDK1.1
     */
    public static native int identityHashCode(Object x);

    /**
     * System properties. The following properties are guaranteed to be defined:
     * <dl>
     * <dt>java.version         <dd>Java version number
     * <dt>java.vendor          <dd>Java vendor specific string
     * <dt>java.vendor.url      <dd>Java vendor URL
     * <dt>java.home            <dd>Java installation directory
     * <dt>java.class.version   <dd>Java class version number
     * <dt>java.class.path      <dd>Java classpath
     * <dt>os.name              <dd>Operating System Name
     * <dt>os.arch              <dd>Operating System Architecture
     * <dt>os.version           <dd>Operating System Version
     * <dt>file.separator       <dd>File separator ("/" on Unix)
     * <dt>path.separator       <dd>Path separator (":" on Unix)
     * <dt>line.separator       <dd>Line separator ("\n" on Unix)
     * <dt>user.name            <dd>User account name
     * <dt>user.home            <dd>User home directory
     * <dt>user.dir             <dd>User's current working directory
     * </dl>
     */

    private static Properties props;

    private static native Properties initProperties(Properties props);

    /**
     * Determines the current system properties.
     * <p>
     * 确定当前系统属性。
     * <p>
     * First, if there is a security manager, its
     * <code>checkPropertiesAccess</code> method is called with no
     * arguments. This may result in a security exception.
     * <p>
     * 首先，如果有 security manager，那么它的 checkPropertiesAccess 方法会被调用，不传入任何参数。
     * <p>
     * The current set of system properties for use by the
     * {@link #getProperty(String)} method is returned as a
     * <code>Properties</code> object. If there is no current set of
     * system properties, a set of system properties is first created and
     * initialized. This set of system properties always includes values
     * for the following keys:
     * <table summary="Shows property keys and associated values">
     * <tr><th>Key</th>
     *     <th>Description of Associated Value</th></tr>
     * <tr><td><code>java.version</code></td>
     *     <td>Java Runtime Environment version</td></tr>
     * <tr><td><code>java.vendor</code></td>
     *     <td>Java Runtime Environment vendor</td></tr>
     * <tr><td><code>java.vendor.url</code></td>
     *     <td>Java vendor URL</td></tr>
     * <tr><td><code>java.home</code></td>
     *     <td>Java installation directory</td></tr>
     * <tr><td><code>java.vm.specification.version</code></td>
     *     <td>Java Virtual Machine specification version</td></tr>
     * <tr><td><code>java.specification.maintenance.version</code></td>
     *     <td>Java Runtime Environment specification maintenance
     *     version, may be interpreted as a positive integer
     *     <em>(optional, see below)</em></td></tr>
     * <tr><td><code>java.vm.specification.vendor</code></td>
     *     <td>Java Virtual Machine specification vendor</td></tr>
     * <tr><td><code>java.vm.specification.name</code></td>
     *     <td>Java Virtual Machine specification name</td></tr>
     * <tr><td><code>java.vm.version</code></td>
     *     <td>Java Virtual Machine implementation version</td></tr>
     * <tr><td><code>java.vm.vendor</code></td>
     *     <td>Java Virtual Machine implementation vendor</td></tr>
     * <tr><td><code>java.vm.name</code></td>
     *     <td>Java Virtual Machine implementation name</td></tr>
     * <tr><td><code>java.specification.version</code></td>
     *     <td>Java Runtime Environment specification  version</td></tr>
     * <tr><td><code>java.specification.vendor</code></td>
     *     <td>Java Runtime Environment specification  vendor</td></tr>
     * <tr><td><code>java.specification.name</code></td>
     *     <td>Java Runtime Environment specification  name</td></tr>
     * <tr><td><code>java.class.version</code></td>
     *     <td>Java class format version number</td></tr>
     * <tr><td><code>java.class.path</code></td>
     *     <td>Java class path</td></tr>
     * <tr><td><code>java.library.path</code></td>
     *     <td>List of paths to search when loading libraries</td></tr>
     * <tr><td><code>java.io.tmpdir</code></td>
     *     <td>Default temp file path</td></tr>
     * <tr><td><code>java.compiler</code></td>
     *     <td>Name of JIT compiler to use</td></tr>
     * <tr><td><code>java.ext.dirs</code></td>
     *     <td>Path of extension directory or directories
     *         <b>Deprecated.</b> <i>This property, and the mechanism
     *            which implements it, may be removed in a future
     *            release.</i> </td></tr>
     * <tr><td><code>os.name</code></td>
     *     <td>Operating system name</td></tr>
     * <tr><td><code>os.arch</code></td>
     *     <td>Operating system architecture</td></tr>
     * <tr><td><code>os.version</code></td>
     *     <td>Operating system version</td></tr>
     * <tr><td><code>file.separator</code></td>
     *     <td>File separator ("/" on UNIX)</td></tr>
     * <tr><td><code>path.separator</code></td>
     *     <td>Path separator (":" on UNIX)</td></tr>
     * <tr><td><code>line.separator</code></td>
     *     <td>Line separator ("\n" on UNIX)</td></tr>
     * <tr><td><code>user.name</code></td>
     *     <td>User's account name</td></tr>
     * <tr><td><code>user.home</code></td>
     *     <td>User's home directory</td></tr>
     * <tr><td><code>user.dir</code></td>
     *     <td>User's current working directory</td></tr>
     * </table>
     * <p>
     * The {@code java.specification.maintenance.version} property is
     * defined if the specification implemented by this runtime at the
     * time of its construction had undergone a <a
     * href="https://jcp.org/en/procedures/jcp2#3.6.4">maintenance
     * release</a>. When defined, its value identifies that
     * maintenance release. To indicate the first maintenance release
     * this property will have the value {@code "1"}, to indicate the
     * second maintenance release this property will have the value
     * {@code "2"}, and so on.
     * <p>
     * Multiple paths in a system property value are separated by the path
     * separator character of the platform.
     * <p>
     * Note that even if the security manager does not permit the
     * <code>getProperties</code> operation, it may choose to permit the
     * {@link #getProperty(String)} operation.
     *
     * @return the system properties
     * @throws SecurityException if a security manager exists and its
     *                           <code>checkPropertiesAccess</code> method doesn't allow access
     *                           to the system properties.
     * @see #setProperties
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkPropertiesAccess()
     * @see java.util.Properties
     */
    public static Properties getProperties() {
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPropertiesAccess();
        }

        return props;
    }

    /**
     * Returns the system-dependent line separator string.  It always
     * returns the same value - the initial value of the {@linkplain
     * #getProperty(String) system property} {@code line.separator}.
     * <p>
     * 返回系统相关的行分隔符字符串。
     * 它总是返回相同的值——系统属性 line.separator 的初始值。
     *
     * <p>On UNIX systems, it returns {@code "\n"}; on Microsoft
     * Windows systems it returns {@code "\r\n"}.
     * <p>
     * 在 UNIX 系统上，它返回 "\n"；
     * 在 Microsoft Windows 系统上，它返回 "\r\n"。
     *
     * @return the system-dependent line separator string
     * @since 1.7
     */
    public static String lineSeparator() {
        return lineSeparator;
    }

    private static String lineSeparator;

    /**
     * Sets the system properties to the <code>Properties</code>
     * argument.
     * <p>
     * 将系统属性设置为 Properties 参数。
     * <p>
     * First, if there is a security manager, its
     * <code>checkPropertiesAccess</code> method is called with no
     * arguments. This may result in a security exception.
     * <p>
     * The argument becomes the current set of system properties for use
     * by the {@link #getProperty(String)} method. If the argument is
     * <code>null</code>, then the current set of system properties is
     * forgotten.
     * <p>
     * 该参数成为 getProperty(String) 方法使用的当前系统属性集。是一种覆盖的 set 方式
     * 如果参数为 null，则忘记当前系统属性集。
     *
     * @param props the new system properties.
     * @throws SecurityException if a security manager exists and its
     *                           <code>checkPropertiesAccess</code> method doesn't allow access
     *                           to the system properties.
     * @see #getProperties
     * @see java.util.Properties
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkPropertiesAccess()
     */
    public static void setProperties(Properties props) {
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPropertiesAccess();
        }
        if (props == null) {
            props = new Properties();
            initProperties(props);
        }
        System.props = props;
    }

    /**
     * Gets the system property indicated by the specified key.
     * <p>
     * 获取指定键指示的系统属性。
     * <p>
     * First, if there is a security manager, its
     * <code>checkPropertyAccess</code> method is called with the key as
     * its argument. This may result in a SecurityException.
     * <p>
     * If there is no current set of system properties, a set of system
     * properties is first created and initialized in the same manner as
     * for the <code>getProperties</code> method.
     * <p>
     * 如果没有当前系统属性集，那么首先创建一个系统属性集，并以与 getProperties 方法相同的方式初始化。
     *
     * @param key the name of the system property.
     * @return the string value of the system property,
     * or <code>null</code> if there is no property with that key.
     * @throws SecurityException        if a security manager exists and its
     *                                  <code>checkPropertyAccess</code> method doesn't allow
     *                                  access to the specified system property.
     * @throws NullPointerException     if <code>key</code> is
     *                                  <code>null</code>.
     * @throws IllegalArgumentException if <code>key</code> is empty.
     * @see #setProperty
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkPropertyAccess(java.lang.String)
     * @see java.lang.System#getProperties()
     */
    public static String getProperty(String key) {
        checkKey(key);
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPropertyAccess(key);
        }

        return props.getProperty(key);
    }

    /**
     * Gets the system property indicated by the specified key.
     * <p>
     * First, if there is a security manager, its
     * <code>checkPropertyAccess</code> method is called with the
     * <code>key</code> as its argument.
     * <p>
     * If there is no current set of system properties, a set of system
     * properties is first created and initialized in the same manner as
     * for the <code>getProperties</code> method.
     *
     * @param key the name of the system property.
     * @param def a default value.
     * @return the string value of the system property,
     * or the default value if there is no property with that key.
     * @throws SecurityException        if a security manager exists and its
     *                                  <code>checkPropertyAccess</code> method doesn't allow
     *                                  access to the specified system property.
     * @throws NullPointerException     if <code>key</code> is
     *                                  <code>null</code>.
     * @throws IllegalArgumentException if <code>key</code> is empty.
     * @see #setProperty
     * @see java.lang.SecurityManager#checkPropertyAccess(java.lang.String)
     * @see java.lang.System#getProperties()
     */
    public static String getProperty(String key, String def) {
        checkKey(key);
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPropertyAccess(key);
        }

        return props.getProperty(key, def);
    }

    /**
     * Sets the system property indicated by the specified key.
     * <p>
     * First, if a security manager exists, its
     * <code>SecurityManager.checkPermission</code> method
     * is called with a <code>PropertyPermission(key, "write")</code>
     * permission. This may result in a SecurityException being thrown.
     * If no exception is thrown, the specified property is set to the given
     * value.
     * <p>
     *
     * @param key   the name of the system property.
     * @param value the value of the system property.
     * @return the previous value of the system property,
     * or <code>null</code> if it did not have one.
     * @throws SecurityException        if a security manager exists and its
     *                                  <code>checkPermission</code> method doesn't allow
     *                                  setting of the specified property.
     * @throws NullPointerException     if <code>key</code> or
     *                                  <code>value</code> is <code>null</code>.
     * @throws IllegalArgumentException if <code>key</code> is empty.
     * @see #getProperty
     * @see java.lang.System#getProperty(java.lang.String)
     * @see java.lang.System#getProperty(java.lang.String, java.lang.String)
     * @see java.util.PropertyPermission
     * @see SecurityManager#checkPermission
     * @since 1.2
     */
    public static String setProperty(String key, String value) {
        checkKey(key);
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new PropertyPermission(key,
                    SecurityConstants.PROPERTY_WRITE_ACTION));
        }

        return (String) props.setProperty(key, value);
    }

    /**
     * Removes the system property indicated by the specified key.
     * <p>
     * First, if a security manager exists, its
     * <code>SecurityManager.checkPermission</code> method
     * is called with a <code>PropertyPermission(key, "write")</code>
     * permission. This may result in a SecurityException being thrown.
     * If no exception is thrown, the specified property is removed.
     * <p>
     *
     * @param key the name of the system property to be removed.
     * @return the previous string value of the system property,
     * or <code>null</code> if there was no property with that key.
     * @throws SecurityException        if a security manager exists and its
     *                                  <code>checkPropertyAccess</code> method doesn't allow
     *                                  access to the specified system property.
     * @throws NullPointerException     if <code>key</code> is
     *                                  <code>null</code>.
     * @throws IllegalArgumentException if <code>key</code> is empty.
     * @see #getProperty
     * @see #setProperty
     * @see java.util.Properties
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkPropertiesAccess()
     * @since 1.5
     */
    public static String clearProperty(String key) {
        checkKey(key);
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new PropertyPermission(key, "write"));
        }

        return (String) props.remove(key);
    }

    private static void checkKey(String key) {
        if (key == null) {
            throw new NullPointerException("key can't be null");
        }
        if (key.equals("")) {
            throw new IllegalArgumentException("key can't be empty");
        }
    }

    /**
     * Gets the value of the specified environment variable. An
     * environment variable is a system-dependent external named
     * value.
     * <p>
     * 获取指定环境变量的值。环境变量是一个系统相关的外部命名值。
     *
     * <p>If a security manager exists, its
     * {@link SecurityManager#checkPermission checkPermission}
     * method is called with a
     * <code>{@link RuntimePermission}("getenv."+name)</code>
     * permission.  This may result in a {@link SecurityException}
     * being thrown.  If no exception is thrown the value of the
     * variable <code>name</code> is returned.
     *
     *
     * <p><a name="EnvironmentVSSystemProperties"><i>System
     * properties</i> and <i>environment variables</i></a> are both
     * conceptually mappings between names and values.  Both
     * mechanisms can be used to pass user-defined information to a
     * Java process.  Environment variables have a more global effect,
     * because they are visible to all descendants of the process
     * which defines them, not just the immediate Java subprocess.
     * They can have subtly different semantics, such as case
     * insensitivity, on different operating systems.  For these
     * reasons, environment variables are more likely to have
     * unintended side effects.  It is best to use system properties
     * where possible.  Environment variables should be used when a
     * global effect is desired, or when an external system interface
     * requires an environment variable (such as <code>PATH</code>).
     * <p>
     * 系统属性 和 环境变量都是名称和值之间的概念映射。
     * 这两种机制都可以用于将用户定义的信息传递给 Java 进程。
     * 环境变量具有更广泛的影响，因为它们对定义它们的进程的所有后代都是可见的，而不仅仅是直接的 Java 子进程。
     * 它们在不同的操作系统上可能具有不同的语义，例如大小写不敏感。
     * 出于这些原因，环境变量更可能产生意外的副作用。
     * 最好优先使用系统属性。
     * 环境变量应该在需要全局效果时使用，或者当外部系统接口需要环境变量（例如 PATH）时使用。
     *
     *
     * <p>On UNIX systems the alphabetic case of <code>name</code> is
     * typically significant, while on Microsoft Windows systems it is
     * typically not.  For example, the expression
     * <code>System.getenv("FOO").equals(System.getenv("foo"))</code>
     * is likely to be true on Microsoft Windows.
     * <p>
     * 在 UNIX 系统上，name 的字母大小写通常是重要的，而在 Microsoft Windows 系统上通常不是。
     * 例如，表达式 System.getenv("FOO").equals(System.getenv("foo")) 在 Microsoft Windows 上可能为 true。
     *
     * @param name the name of the environment variable
     * @return the string value of the variable, or <code>null</code>
     * if the variable is not defined in the system environment
     * @throws NullPointerException if <code>name</code> is <code>null</code>
     * @throws SecurityException    if a security manager exists and its
     *                              {@link SecurityManager#checkPermission checkPermission}
     *                              method doesn't allow access to the environment variable
     *                              <code>name</code>
     * @see #getenv()
     * @see ProcessBuilder#environment()
     */
    public static String getenv(String name) {
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("getenv." + name));
        }

        return ProcessEnvironment.getenv(name);
    }


    /**
     * Returns an unmodifiable string map view of the current system environment.
     * The environment is a system-dependent mapping from names to
     * values which is passed from parent to child processes.
     *
     * <p>If the system does not support environment variables, an
     * empty map is returned.
     *
     * <p>The returned map will never contain null keys or values.
     * Attempting to query the presence of a null key or value will
     * throw a {@link NullPointerException}.  Attempting to query
     * the presence of a key or value which is not of type
     * {@link String} will throw a {@link ClassCastException}.
     *
     * <p>The returned map and its collection views may not obey the
     * general contract of the {@link Object#equals} and
     * {@link Object#hashCode} methods.
     *
     * <p>The returned map is typically case-sensitive on all platforms.
     *
     * <p>If a security manager exists, its
     * {@link SecurityManager#checkPermission checkPermission}
     * method is called with a
     * <code>{@link RuntimePermission}("getenv.*")</code>
     * permission.  This may result in a {@link SecurityException} being
     * thrown.
     *
     * <p>When passing information to a Java subprocess,
     * <a href=#EnvironmentVSSystemProperties>system properties</a>
     * are generally preferred over environment variables.
     *
     * @return the environment as a map of variable names to values
     * @throws SecurityException if a security manager exists and its
     *                           {@link SecurityManager#checkPermission checkPermission}
     *                           method doesn't allow access to the process environment
     * @see #getenv(String)
     * @see ProcessBuilder#environment()
     * @since 1.5
     */
    public static java.util.Map<String, String> getenv() {
        SecurityManager sm = getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("getenv.*"));
        }

        return ProcessEnvironment.getenv();
    }

    /**
     * Terminates the currently running Java Virtual Machine. The
     * argument serves as a status code; by convention, a nonzero status
     * code indicates abnormal termination.
     * <p>
     * 终止当前正在运行的 Java 虚拟机。该参数用作状态码；按照惯例，非零状态码表示异常终止。
     *
     * <p>
     * This method calls the <code>exit</code> method in class
     * <code>Runtime</code>. This method never returns normally.
     * <p>
     * The call <code>System.exit(n)</code> is effectively equivalent to
     * the call:
     * <blockquote><pre>
     * Runtime.getRuntime().exit(n)
     * </pre></blockquote>
     * <p>
     * 该方法调用 Runtime 类中的 exit 方法。该方法永远不会正常返回。
     * 调用 System.exit(n) 等效于调用 Runtime.getRuntime().exit(n)
     *
     * @param status exit status.
     * @throws SecurityException if a security manager exists and its <code>checkExit</code>
     *                           method doesn't allow exit with the specified status.
     * @see java.lang.Runtime#exit(int)
     */
    public static void exit(int status) {
        Runtime.getRuntime().exit(status);
    }

    /**
     * Runs the garbage collector.
     * <p>
     * 运行垃圾回收器。
     *
     * <p>
     * Calling the <code>gc</code> method suggests that the Java Virtual
     * Machine expend effort toward recycling unused objects in order to
     * make the memory they currently occupy available for quick reuse.
     * When control returns from the method call, the Java Virtual
     * Machine has made a best effort to reclaim space from all discarded
     * objects.
     * <p>
     * 调用 gc 方法建议 Java 虚拟机努力回收未使用的对象，以便使它们当前占用的内存可用于快速重用。
     * 当从方法调用返回时，Java 虚拟机已经尽最大努力从所有丢弃的对象中回收空间。
     *
     * <p>
     * The call <code>System.gc()</code> is effectively equivalent to the
     * call:
     * <blockquote><pre>
     * Runtime.getRuntime().gc()
     * </pre></blockquote>
     *
     * @see java.lang.Runtime#gc()
     */
    public static void gc() {
        Runtime.getRuntime().gc();
    }

    /**
     * Runs the finalization methods of any objects pending finalization.
     * <p>
     * 运行任何待完成的对象的 finalize 方法。
     *
     * <p>
     * Calling this method suggests that the Java Virtual Machine expend
     * effort toward running the <code>finalize</code> methods of objects
     * that have been found to be discarded but whose <code>finalize</code>
     * methods have not yet been run. When control returns from the
     * method call, the Java Virtual Machine has made a best effort to
     * complete all outstanding finalizations.
     * <p>
     * 调用此方法建议 Java 虚拟机努力运行已被认为将要丢弃但其 finalize 方法尚未运行的对象的 finalize 方法。
     * 当从方法调用返回时，Java 虚拟机已经尽最大努力完成所有未完成的终结。
     * <p>
     * The call <code>System.runFinalization()</code> is effectively
     * equivalent to the call:
     * <blockquote><pre>
     * Runtime.getRuntime().runFinalization()
     * </pre></blockquote>
     *
     * @see java.lang.Runtime#runFinalization()
     */
    public static void runFinalization() {
        Runtime.getRuntime().runFinalization();
    }

    /**
     * Throws {@code UnsupportedOperationException}.
     *
     * <p>The call {@code System.runFinalizersOnExit()} is effectively
     * equivalent to the call:
     * <blockquote><pre>
     * Runtime.runFinalizersOnExit()
     * </pre></blockquote>
     *
     * @param value ignored
     * @see java.lang.Runtime#runFinalizersOnExit(boolean)
     * @since JDK1.1
     * @deprecated This method was originally designed to enable or disable
     * running finalizers on exit. Running finalizers on exit was disabled
     * by default. If enabled, then the finalizers of all objects whose
     * finalizers had not yet been automatically invoked were to be run before
     * the Java runtime exits. That behavior is inherently unsafe. It may
     * result in finalizers being called on live objects while other threads
     * are concurrently manipulating those objects, resulting in erratic
     * behavior or deadlock.
     */
    @Deprecated
    public static void runFinalizersOnExit(boolean value) {
        Runtime.runFinalizersOnExit(value);
    }

    /**
     * Loads the native library specified by the filename argument.  The filename
     * argument must be an absolute path name.
     * <p>
     * If the filename argument, when stripped of any platform-specific library
     * prefix, path, and file extension, indicates a library whose name is,
     * for example, L, and a native library called L is statically linked
     * with the VM, then the JNI_OnLoad_L function exported by the library
     * is invoked rather than attempting to load a dynamic library.
     * A filename matching the argument does not have to exist in the
     * file system.
     * See the JNI Specification for more details.
     * <p>
     * Otherwise, the filename argument is mapped to a native library image in
     * an implementation-dependent manner.
     *
     * <p>
     * The call <code>System.load(name)</code> is effectively equivalent
     * to the call:
     * <blockquote><pre>
     * Runtime.getRuntime().load(name)
     * </pre></blockquote>
     *
     * @param filename the file to load.
     * @throws SecurityException    if a security manager exists and its
     *                              <code>checkLink</code> method doesn't allow
     *                              loading of the specified dynamic library
     * @throws UnsatisfiedLinkError if either the filename is not an
     *                              absolute path name, the native library is not statically
     *                              linked with the VM, or the library cannot be mapped to
     *                              a native library image by the host system.
     * @throws NullPointerException if <code>filename</code> is
     *                              <code>null</code>
     * @see java.lang.Runtime#load(java.lang.String)
     * @see java.lang.SecurityManager#checkLink(java.lang.String)
     */
    @CallerSensitive
    public static void load(String filename) {
        Runtime.getRuntime().load0(Reflection.getCallerClass(), filename);
    }

    /**
     * Loads the native library specified by the <code>libname</code>
     * argument.  The <code>libname</code> argument must not contain any platform
     * specific prefix, file extension or path. If a native library
     * called <code>libname</code> is statically linked with the VM, then the
     * JNI_OnLoad_<code>libname</code> function exported by the library is invoked.
     * See the JNI Specification for more details.
     * <p>
     * Otherwise, the libname argument is loaded from a system library
     * location and mapped to a native library image in an implementation-
     * dependent manner.
     * <p>
     * The call <code>System.loadLibrary(name)</code> is effectively
     * equivalent to the call
     * <blockquote><pre>
     * Runtime.getRuntime().loadLibrary(name)
     * </pre></blockquote>
     *
     * @param libname the name of the library.
     * @throws SecurityException    if a security manager exists and its
     *                              <code>checkLink</code> method doesn't allow
     *                              loading of the specified dynamic library
     * @throws UnsatisfiedLinkError if either the libname argument
     *                              contains a file path, the native library is not statically
     *                              linked with the VM,  or the library cannot be mapped to a
     *                              native library image by the host system.
     * @throws NullPointerException if <code>libname</code> is
     *                              <code>null</code>
     * @see java.lang.Runtime#loadLibrary(java.lang.String)
     * @see java.lang.SecurityManager#checkLink(java.lang.String)
     */
    @CallerSensitive
    public static void loadLibrary(String libname) {
        Runtime.getRuntime().loadLibrary0(Reflection.getCallerClass(), libname);
    }

    /**
     * Maps a library name into a platform-specific string representing
     * a native library.
     *
     * @param libname the name of the library.
     * @return a platform-dependent native library name.
     * @throws NullPointerException if <code>libname</code> is
     *                              <code>null</code>
     * @see java.lang.System#loadLibrary(java.lang.String)
     * @see java.lang.ClassLoader#findLibrary(java.lang.String)
     * @since 1.2
     */
    public static native String mapLibraryName(String libname);

    /**
     * Create PrintStream for stdout/err based on encoding.
     */
    private static PrintStream newPrintStream(FileOutputStream fos, String enc) {
        if (enc != null) {
            try {
                return new PrintStream(new BufferedOutputStream(fos, 128), true, enc);
            } catch (UnsupportedEncodingException uee) {
            }
        }
        return new PrintStream(new BufferedOutputStream(fos, 128), true);
    }


    /**
     * Initialize the system class.  Called after thread initialization.
     */
    private static void initializeSystemClass() {

        // VM might invoke JNU_NewStringPlatform() to set those encoding
        // sensitive properties (user.home, user.name, boot.class.path, etc.)
        // during "props" initialization, in which it may need access, via
        // System.getProperty(), to the related system encoding property that
        // have been initialized (put into "props") at early stage of the
        // initialization. So make sure the "props" is available at the
        // very beginning of the initialization and all system properties to
        // be put into it directly.
        // 虚拟机可能会调用 JNU_NewStringPlatform() 来设置那些编码敏感的属性（user.home、user.name、boot.class.path 等），
        // 在这些属性初始化期间，它可能需要通过 System.getProperty() 访问已经初始化（放入 "props"）的相关系统编码属性。
        // 因此，确保 "props" 在初始化的最开始就可用，并且所有系统属性都直接放入其中。
        props = new Properties();
        initProperties(props);  // initialized by the VM

        // There are certain system configurations that may be controlled by
        // VM options such as the maximum amount of direct memory and
        // Integer cache size used to support the object identity semantics
        // of autoboxing.  Typically, the library will obtain these values
        // from the properties set by the VM.  If the properties are for
        // internal implementation use only, these properties should be
        // removed from the system properties.
        //
        // 有些系统配置可能由 VM 选项控制，例如用于支持自动装箱的对象标识语义的 direct memory 的最大量和 Integer 缓存大小。
        // 通常，相应的代码配置将从 VM 设置的属性中获取这些值。如果这些属性仅供内部实现使用，则应从系统属性中删除这些属性。
        //
        // See java.lang.Integer.IntegerCache and the
        // sun.misc.VM.saveAndRemoveProperties method for example.
        //
        // Save a private copy of the system properties object that
        // can only be accessed by the internal implementation.  Remove
        // certain system properties that are not intended for public access.
        // 保存系统属性对象的私有副本，只能由内部实现访问。删除某些不打算公开访问的系统属性。
        // 例如：java.lang.Integer.IntegerCache 和 sun.misc.VM.saveAndRemoveProperties 方法。
        sun.misc.VM.saveAndRemoveProperties(props);


        lineSeparator = props.getProperty("line.separator");
        sun.misc.Version.init();

        FileInputStream fdIn = new FileInputStream(FileDescriptor.in);
        FileOutputStream fdOut = new FileOutputStream(FileDescriptor.out);
        FileOutputStream fdErr = new FileOutputStream(FileDescriptor.err);
        setIn0(new BufferedInputStream(fdIn));
        setOut0(newPrintStream(fdOut, props.getProperty("sun.stdout.encoding")));
        setErr0(newPrintStream(fdErr, props.getProperty("sun.stderr.encoding")));

        // Load the zip library now in order to keep java.util.zip.ZipFile
        // from trying to use itself to load this library later.
        // 现在加载 zip 库，以便保持 java.util.zip.ZipFile 以后不会尝试使用自身来加载此库。
        loadLibrary("zip");

        // Setup Java signal handlers for HUP, TERM, and INT (where available).
        // 设置 HUP、TERM 和 INT 的 Java 信号处理程序（如果可用）。
        Terminator.setup();

        // Initialize any miscellenous operating system settings that need to be
        // set for the class libraries. Currently this is no-op everywhere except
        // for Windows where the process-wide error mode is set before the java.io
        // classes are used.
        // 初始化需要为类库设置的任何其他操作系统设置。
        // 目前，这在除 Windows 之外的所有地方都是无操作，其中在使用 java.io 类之前设置了进程范围的错误模式。
        sun.misc.VM.initializeOSEnvironment();

        // The main thread is not added to its thread group in the same
        // way as other threads; we must do it ourselves here.
        // 主线程不会像其他线程一样添加到其线程组中；我们必须在这里自己做。
        Thread current = Thread.currentThread();
        current.getThreadGroup().add(current);

        // register shared secrets
        // 设置一些访问权限
        setJavaLangAccess();

        // Subsystems that are invoked during initialization can invoke
        // sun.misc.VM.isBooted() in order to avoid doing things that should
        // wait until the application class loader has been set up.
        // IMPORTANT: Ensure that this remains the last initialization action!
        // 在初始化期间调用的子系统可以调用 sun.misc.VM.isBooted()，以避免做应该等到应用程序类加载器设置完成的事情。
        // 重要：确保这仍然是最后一个初始化操作！
        sun.misc.VM.booted();
    }

    private static void setJavaLangAccess() {
        // Allow privileged classes outside of java.lang
        sun.misc.SharedSecrets.setJavaLangAccess(new sun.misc.JavaLangAccess() {
            public sun.reflect.ConstantPool getConstantPool(Class<?> klass) {
                return klass.getConstantPool();
            }

            public boolean casAnnotationType(Class<?> klass, AnnotationType oldType, AnnotationType newType) {
                return klass.casAnnotationType(oldType, newType);
            }

            public AnnotationType getAnnotationType(Class<?> klass) {
                return klass.getAnnotationType();
            }

            public Map<Class<? extends Annotation>, Annotation> getDeclaredAnnotationMap(Class<?> klass) {
                return klass.getDeclaredAnnotationMap();
            }

            public byte[] getRawClassAnnotations(Class<?> klass) {
                return klass.getRawAnnotations();
            }

            public byte[] getRawClassTypeAnnotations(Class<?> klass) {
                return klass.getRawTypeAnnotations();
            }

            public byte[] getRawExecutableTypeAnnotations(Executable executable) {
                return Class.getExecutableTypeAnnotationBytes(executable);
            }

            public <E extends Enum<E>>
            E[] getEnumConstantsShared(Class<E> klass) {
                return klass.getEnumConstantsShared();
            }

            public void blockedOn(Thread t, Interruptible b) {
                t.blockedOn(b);
            }

            public void registerShutdownHook(int slot, boolean registerShutdownInProgress, Runnable hook) {
                Shutdown.add(slot, registerShutdownInProgress, hook);
            }

            public int getStackTraceDepth(Throwable t) {
                return t.getStackTraceDepth();
            }

            public StackTraceElement getStackTraceElement(Throwable t, int i) {
                return t.getStackTraceElement(i);
            }

            public String newStringUnsafe(char[] chars) {
                return new String(chars, true);
            }

            public Thread newThreadWithAcc(Runnable target, AccessControlContext acc) {
                return new Thread(target, acc);
            }

            public void invokeFinalize(Object o) throws Throwable {
                o.finalize();
            }
        });
    }
}
