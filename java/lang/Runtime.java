/*
 * Copyright (c) 1995, 2022, Oracle and/or its affiliates. All rights reserved.
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package java.lang;

import java.io.*;
import java.util.StringTokenizer;

import sun.reflect.CallerSensitive;
import sun.reflect.Reflection;

/**
 * Every Java application has a single instance of class
 * <code>Runtime</code> that allows the application to interface with
 * the environment in which the application is running. The current
 * runtime can be obtained from the <code>getRuntime</code> method.
 * <p>
 * 每个 Java 应用程序都有唯一的一个 Runtime 类的实例，允许应用程序与其外部运行的环境进行交互。
 * 当前运行时可以从 getRuntime 方法中获得。
 * <p>
 * An application cannot create its own instance of this class.
 * <p>
 * 应用程序不能创建自己的 Runtime 类实例。
 *
 * @author unascribed
 * @see java.lang.Runtime#getRuntime()
 * @since JDK1.0
 */

public class Runtime {
    // 饿汉式的单例
    private static Runtime currentRuntime = new Runtime();

    /**
     * Returns the runtime object associated with the current Java application.
     * Most of the methods of class <code>Runtime</code> are instance
     * methods and must be invoked with respect to the current runtime object.
     * <p>
     * 返回与当前 Java 应用程序关联的运行时对象。
     * Runtime 类的大多数方法都是实例方法，必须 基于当前运行时对象调用。
     *
     * @return the <code>Runtime</code> object associated with the current
     * Java application.
     */
    public static Runtime getRuntime() {
        return currentRuntime;
    }

    /**
     * Don't let anyone else instantiate this class
     * <p>
     * 单例模式，私有化构造方法
     */
    private Runtime() {
    }

    /**
     * Terminates the currently running Java virtual machine by initiating its
     * shutdown sequence.  This method never returns normally.  The argument
     * serves as a status code; by convention, a nonzero status code indicates
     * abnormal termination.
     * <p>
     * 通过启动 shutdown sequence 来终止当前正在运行的 Java 虚拟机。
     * 该方法永远不会正常返回。
     * 参数作为状态码；按照惯例，非零状态码表示异常终止。
     *
     * <p> All registered {@linkplain #addShutdownHook shutdown hooks}, if any,
     * are started in some unspecified order and allowed to run concurrently
     * until they finish.  Once this is done the virtual machine
     * {@linkplain #halt halts}.
     * <p>
     * 所有已注册的 shutdown hooks（如果有）将以某种未指定的顺序启动，并允许并发运行，直到它们完成。
     * 一旦完成，虚拟机将停止。
     *
     * <p> If this method is invoked after all shutdown hooks have already
     * been run and the status is nonzero then this method halts the
     * virtual machine with the given status code. Otherwise, this method
     * blocks indefinitely.
     * <p>
     * 如果在所有 shutdown hooks 已经运行并且状态为非零的情况下调用此方法，则此方法使用给定的状态代码停止虚拟机。
     * 否则，此方法将无限期地阻塞。
     *
     * <p> The {@link System#exit(int) System.exit} method is the
     * conventional and convenient means of invoking this method.
     * <p>
     * System.exit 方法是调用此方法的常规和方便的方法。
     *
     * @param status Termination status.  By convention, a nonzero status code
     *               indicates abnormal termination.
     * @throws SecurityException If a security manager is present and its
     *                           {@link SecurityManager#checkExit checkExit} method does not permit
     *                           exiting with the specified status
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkExit(int)
     * @see #addShutdownHook
     * @see #removeShutdownHook
     * @see #halt(int)
     */
    public void exit(int status) {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkExit(status);
        }
        Shutdown.exit(status);
    }

    /**
     * Registers a new virtual-machine shutdown hook.
     * <p>
     * 注册一个新的 vm shutdown hook。
     *
     * <p> The Java virtual machine <i>shuts down</i> in response to two kinds
     * of events:
     *
     *   <ul>
     *
     *   <li> The program <i>exits</i> normally, when the last non-daemon
     *   thread exits or when the {@link #exit exit} (equivalently,
     *   {@link System#exit(int) System.exit}) method is invoked, or
     *
     *   <li> The virtual machine is <i>terminated</i> in response to a
     *   user interrupt, such as typing {@code ^C}, or a system-wide event,
     *   such as user logoff or system shutdown.
     *
     *   </ul>
     *
     * <p>
     *     Java 虚拟机响应两种类型的事件而关闭：
     *     1. 程序正常退出，当最后一个非 daemon 线程退出或调用 exit 方法时
     *     2. Java 虚拟机被终止，响应用户中断，例如键入 ^C，或系统范围的事件，例如用户注销或系统关闭。
     *          即使是这种异常终止，jvm 也会尽量执行所有的 shutdown hooks。
     *
     * <p> A <i>shutdown hook</i> is simply an initialized but unstarted
     * thread.  When the virtual machine begins its shutdown sequence it will
     * start all registered shutdown hooks in some unspecified order and let
     * them run concurrently.  When all the hooks have finished it will then
     * halt. Note that daemon threads will continue to run during the shutdown
     * sequence, as will non-daemon threads if shutdown was initiated by
     * invoking the {@link #exit exit} method.
     * <p>
     *     shutdown hook 只是一个初始化但未启动的线程。
     *     当虚拟机开始其 shutdown 流程时，它将以某种未指定的顺序启动所有已注册的 shutdown hooks 并让它们并发运行。
     *     当所有的 hooks 都完成后，它将停止。
     *     注意，在 shutdown 流程期间，daemon 线程将继续运行，如果是通过调用 exit 方法启动的，则非 daemon 线程也将继续运行。
     *
     * <p> Once the shutdown sequence has begun it can be stopped only by
     * invoking the {@link #halt halt} method, which forcibly
     * terminates the virtual machine.
     * <p>
     *    一旦 shutdown 流程开始，只能通过调用 halt 方法来停止它，halt 方法强制终止虚拟机。
     *
     * <p> Once the shutdown sequence has begun it is impossible to register a
     * new shutdown hook or de-register a previously-registered hook.
     * Attempting either of these operations will cause an
     * {@link IllegalStateException} to be thrown.
     * <p>
     *     一旦 shutdown 流程开始，就不可能注册一个新的 shutdown hook 或取消注册一个先前注册的 hook。
     *     尝试执行这两个操作将导致抛出 IllegalStateException。（其实也是可以的，只是必须调用更底层的 Shutdown.add 方法，并指定 inProgress 参数）
     *
     * <p> Shutdown hooks run at a delicate time in the life cycle of a virtual
     * machine and should therefore be coded defensively.  They should, in
     * particular, be written to be thread-safe and to avoid deadlocks insofar
     * as possible.  They should also not rely blindly upon services that may
     * have registered their own shutdown hooks and therefore may themselves in
     * the process of shutting down.  Attempts to use other thread-based
     * services such as the AWT event-dispatch thread, for example, may lead to
     * deadlocks.
     * <p>
     *     Shutdown hooks 在虚拟机的生命周期中运行在一个脆弱的时刻，因此应该进行防御性编码。
     *     特别是，它们应该编写为线程安全的，并尽可能避免死锁。
     *     它们也不应该盲目依赖可能已经注册了自己的 shutdown hooks 的服务，因此它们自己可能正在关闭过程中。
     *     尝试使用其他基于线程的服务，例如 AWT 事件分派线程，可能会导致死锁。
     *
     * <p> Shutdown hooks should also finish their work quickly.  When a
     * program invokes {@link #exit exit} the expectation is
     * that the virtual machine will promptly shut down and exit.  When the
     * virtual machine is terminated due to user logoff or system shutdown the
     * underlying operating system may only allow a fixed amount of time in
     * which to shut down and exit.  It is therefore inadvisable to attempt any
     * user interaction or to perform a long-running computation in a shutdown
     * hook.
     * <p>
     *     Shutdown hooks 也应该快速完成它们的工作。
     *     当程序调用 exit 时，期望是虚拟机将会迅速关闭并退出。
     *     当虚拟机由于用户注销或系统关闭而终止时，底层操作系统可能只允许在其中关闭和退出的固定时间。
     *     因此，尝试在 shutdown hook 中进行任何用户交互或执行长时间运行的计算是不可取的。
     *
     * <p> Uncaught exceptions are handled in shutdown hooks just as in any
     * other thread, by invoking the
     * {@link ThreadGroup#uncaughtException uncaughtException} method of the
     * thread's {@link ThreadGroup} object. The default implementation of this
     * method prints the exception's stack trace to {@link System#err} and
     * terminates the thread; it does not cause the virtual machine to exit or
     * halt.
     * <p>
     *     未捕获的异常在 shutdown hooks 中的处理方式与任何其他线程中的处理方式相同，
     *     即通过调用线程的 ThreadGroup 对象的 uncaughtException 方法。
     *     此方法的默认实现将异常的堆栈跟踪打印到 System.err 并终止线程；
     *     它不会导致虚拟机退出或停止。
     *
     * <p> In rare circumstances the virtual machine may <i>abort</i>, that is,
     * stop running without shutting down cleanly.  This occurs when the
     * virtual machine is terminated externally, for example with the
     * {@code SIGKILL} signal on Unix or the {@code TerminateProcess} call on
     * Microsoft Windows.  The virtual machine may also abort if a native
     * method goes awry by, for example, corrupting internal data structures or
     * attempting to access nonexistent memory.  If the virtual machine aborts
     * then no guarantee can be made about whether or not any shutdown hooks
     * will be run.
     * <p>
     *     在极少数情况下，虚拟机可能会中止，即在不彻底地关闭的情况下停止运行。
     *     当虚拟机在外部终止时，例如在 Unix 上使用 SIGKILL 信号或在 Microsoft Windows 上使用 TerminateProcess 调用时，就会发生这种情况。
     *     如果本地方法出现问题，例如损坏内部数据结构或尝试访问不存在的内存，则虚拟机也可能会中止。
     *     如果虚拟机中止，则不能保证是否会运行任何 shutdown hooks。
     *
     * @param hook An initialized but unstarted {@link Thread} object
     * @throws IllegalArgumentException If the specified hook has already been registered,
     *                                  or if it can be determined that the hook is already running or
     *                                  has already been run
     * @throws IllegalStateException    If the virtual machine is already in the process
     *                                  of shutting down
     * @throws SecurityException        If a security manager is present and it denies
     *                                  {@link RuntimePermission}{@code ("shutdownHooks")}
     * @see #removeShutdownHook
     * @see #halt(int)
     * @see #exit(int)
     * @since 1.3
     */
    public void addShutdownHook(Thread hook) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("shutdownHooks"));
        }
        ApplicationShutdownHooks.add(hook);
    }

    /**
     * De-registers a previously-registered virtual-machine shutdown hook. <p>
     * 注销先前注册的虚拟机 shutdown hook。
     *
     * @param hook the hook to remove
     * @return <tt>true</tt> if the specified hook had previously been
     * registered and was successfully de-registered, <tt>false</tt>
     * otherwise.
     * @throws IllegalStateException If the virtual machine is already in the process of shutting
     *                               down
     * @throws SecurityException     If a security manager is present and it denies
     *                               <tt>{@link RuntimePermission}("shutdownHooks")</tt>
     * @see #addShutdownHook
     * @see #exit(int)
     * @since 1.3
     */
    public boolean removeShutdownHook(Thread hook) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkPermission(new RuntimePermission("shutdownHooks"));
        }
        return ApplicationShutdownHooks.remove(hook);
    }

    /**
     * Forcibly terminates the currently running Java virtual machine.  This
     * method never returns normally.
     * <p>
     * 强制终止当前正在运行的 Java 虚拟机。该方法永远不会正常返回。
     *
     * <p> This method should be used with extreme caution.  Unlike the
     * {@link #exit exit} method, this method does not cause shutdown
     * hooks to be started.  If the shutdown sequence has already been
     * initiated then this method does not wait for any running
     * shutdown hooks to finish their work.
     * <p>
     * 此方法应谨慎使用。
     * 与 exit 方法不同，此方法不会导致启动 shutdown hooks。
     * 如果 shutdown sequence 已经启动，则此方法不会等待任何正在运行的 shutdown hooks 完成其工作。
     *
     * @param status Termination status. By convention, a nonzero status code
     *               indicates abnormal termination. If the {@link Runtime#exit exit}
     *               (equivalently, {@link System#exit(int) System.exit}) method
     *               has already been invoked then this status code
     *               will override the status code passed to that method.
     * @throws SecurityException If a security manager is present and its
     *                           {@link SecurityManager#checkExit checkExit} method
     *                           does not permit an exit with the specified status
     * @see #exit
     * @see #addShutdownHook
     * @see #removeShutdownHook
     * @since 1.3
     */
    public void halt(int status) {
        SecurityManager sm = System.getSecurityManager();
        if (sm != null) {
            sm.checkExit(status);
        }
        Shutdown.beforeHalt();
        Shutdown.halt(status);
    }

    /**
     * Throws {@code UnsupportedOperationException}.
     *
     * @param value ignored
     * @since JDK1.1
     * @deprecated This method was originally designed to enable or disable
     * running finalizers on exit. Running finalizers on exit was disabled by default.
     * If enabled, then the finalizers of all objects whose finalizers had not
     * yet been automatically invoked were to be run before the Java runtime exits.
     * That behavior is inherently unsafe. It may result in finalizers being called
     * on live objects while other threads are concurrently manipulating those objects,
     * resulting in erratic behavior or deadlock.
     */
    @Deprecated
    public static void runFinalizersOnExit(boolean value) {
        throw new UnsupportedOperationException();
    }

    /**
     * Executes the specified string command in a separate process.
     * <p>
     * 在一个独立的进程中执行指定的字符串命令。
     *
     * <p>This is a convenience method.  An invocation of the form
     * <tt>exec(command)</tt>
     * behaves in exactly the same way as the invocation
     * <tt>{@link #exec(String, String[], File) exec}(command, null, null)</tt>.
     * <p>
     * 这是一个很便利方法。
     * exec(command) 的调用方式与 exec(command, null, null) 的调用方式完全相同。
     *
     * @param command a specified system command.
     * @return A new {@link Process} object for managing the subprocess
     * @throws SecurityException        If a security manager exists and its
     *                                  {@link SecurityManager#checkExec checkExec}
     *                                  method doesn't allow creation of the subprocess
     * @throws IOException              If an I/O error occurs
     * @throws NullPointerException     If <code>command</code> is <code>null</code>
     * @throws IllegalArgumentException If <code>command</code> is empty
     * @see #exec(String[], String[], File)
     * @see ProcessBuilder
     */
    public Process exec(String command) throws IOException {
        return exec(command, null, null);
    }

    /**
     * Executes the specified string command in a separate process with the
     * specified environment.
     *
     * <p>This is a convenience method.  An invocation of the form
     * <tt>exec(command, envp)</tt>
     * behaves in exactly the same way as the invocation
     * <tt>{@link #exec(String, String[], File) exec}(command, envp, null)</tt>.
     *
     * @param command a specified system command.
     * @param envp    array of strings, each element of which
     *                has environment variable settings in the format
     *                <i>name</i>=<i>value</i>, or
     *                <tt>null</tt> if the subprocess should inherit
     *                the environment of the current process.
     * @return A new {@link Process} object for managing the subprocess
     * @throws SecurityException        If a security manager exists and its
     *                                  {@link SecurityManager#checkExec checkExec}
     *                                  method doesn't allow creation of the subprocess
     * @throws IOException              If an I/O error occurs
     * @throws NullPointerException     If <code>command</code> is <code>null</code>,
     *                                  or one of the elements of <code>envp</code> is <code>null</code>
     * @throws IllegalArgumentException If <code>command</code> is empty
     * @see #exec(String[], String[], File)
     * @see ProcessBuilder
     */
    public Process exec(String command, String[] envp) throws IOException {
        return exec(command, envp, null);
    }

    /**
     * Executes the specified string command in a separate process with the
     * specified environment and working directory.
     * <p>
     * 在指定的环境和工作目录中，在一个独立的进程中执行指定的字符串命令。
     *
     * <p>This is a convenience method.  An invocation of the form
     * <tt>exec(command, envp, dir)</tt>
     * behaves in exactly the same way as the invocation
     * <tt>{@link #exec(String[], String[], File) exec}(cmdarray, envp, dir)</tt>,
     * where <code>cmdarray</code> is an array of all the tokens in
     * <code>command</code>.
     *
     * <p>More precisely, the <code>command</code> string is broken
     * into tokens using a {@link StringTokenizer} created by the call
     * <code>new {@link StringTokenizer}(command)</code> with no
     * further modification of the character categories.  The tokens
     * produced by the tokenizer are then placed in the new string
     * array <code>cmdarray</code>, in the same order.
     * <p>
     * 更加准确地说，command 字符串通过 new StringTokenizer(command) 被分解成 tokens，
     * 然后 tokens 被放入新的字符串数组 cmdarray 中，顺序不变。
     *
     * @param command a specified system command.
     * @param envp    array of strings, each element of which
     *                has environment variable settings in the format
     *                <i>name</i>=<i>value</i>, or
     *                <tt>null</tt> if the subprocess should inherit
     *                the environment of the current process.
     * @param dir     the working directory of the subprocess, or
     *                <tt>null</tt> if the subprocess should inherit
     *                the working directory of the current process.
     * @return A new {@link Process} object for managing the subprocess
     * @throws SecurityException        If a security manager exists and its
     *                                  {@link SecurityManager#checkExec checkExec}
     *                                  method doesn't allow creation of the subprocess
     * @throws IOException              If an I/O error occurs
     * @throws NullPointerException     If <code>command</code> is <code>null</code>,
     *                                  or one of the elements of <code>envp</code> is <code>null</code>
     * @throws IllegalArgumentException If <code>command</code> is empty
     * @see ProcessBuilder
     * @since 1.3
     */
    public Process exec(String command, String[] envp, File dir)
            throws IOException {
        if (command.isEmpty())
            throw new IllegalArgumentException("Empty command");

        // 构造 tokenizer，方便 split
        StringTokenizer st = new StringTokenizer(command);
        String[] cmdarray = new String[st.countTokens()];
        // 类似 iterator 的进行迭代
        for (int i = 0; st.hasMoreTokens(); i++)
            cmdarray[i] = st.nextToken();
        return exec(cmdarray, envp, dir);
    }

    /**
     * Executes the specified command and arguments in a separate process.
     *
     * <p>This is a convenience method.  An invocation of the form
     * <tt>exec(cmdarray)</tt>
     * behaves in exactly the same way as the invocation
     * <tt>{@link #exec(String[], String[], File) exec}(cmdarray, null, null)</tt>.
     *
     * @param cmdarray array containing the command to call and
     *                 its arguments.
     * @return A new {@link Process} object for managing the subprocess
     * @throws SecurityException         If a security manager exists and its
     *                                   {@link SecurityManager#checkExec checkExec}
     *                                   method doesn't allow creation of the subprocess
     * @throws IOException               If an I/O error occurs
     * @throws NullPointerException      If <code>cmdarray</code> is <code>null</code>,
     *                                   or one of the elements of <code>cmdarray</code> is <code>null</code>
     * @throws IndexOutOfBoundsException If <code>cmdarray</code> is an empty array
     *                                   (has length <code>0</code>)
     * @see ProcessBuilder
     */
    public Process exec(String cmdarray[]) throws IOException {
        return exec(cmdarray, null, null);
    }

    /**
     * Executes the specified command and arguments in a separate process
     * with the specified environment.
     *
     * <p>This is a convenience method.  An invocation of the form
     * <tt>exec(cmdarray, envp)</tt>
     * behaves in exactly the same way as the invocation
     * <tt>{@link #exec(String[], String[], File) exec}(cmdarray, envp, null)</tt>.
     *
     * @param cmdarray array containing the command to call and
     *                 its arguments.
     * @param envp     array of strings, each element of which
     *                 has environment variable settings in the format
     *                 <i>name</i>=<i>value</i>, or
     *                 <tt>null</tt> if the subprocess should inherit
     *                 the environment of the current process.
     * @return A new {@link Process} object for managing the subprocess
     * @throws SecurityException         If a security manager exists and its
     *                                   {@link SecurityManager#checkExec checkExec}
     *                                   method doesn't allow creation of the subprocess
     * @throws IOException               If an I/O error occurs
     * @throws NullPointerException      If <code>cmdarray</code> is <code>null</code>,
     *                                   or one of the elements of <code>cmdarray</code> is <code>null</code>,
     *                                   or one of the elements of <code>envp</code> is <code>null</code>
     * @throws IndexOutOfBoundsException If <code>cmdarray</code> is an empty array
     *                                   (has length <code>0</code>)
     * @see ProcessBuilder
     */
    public Process exec(String[] cmdarray, String[] envp) throws IOException {
        return exec(cmdarray, envp, null);
    }


    /**
     * Executes the specified command and arguments in a separate process with
     * the specified environment and working directory.
     *
     * <p>Given an array of strings <code>cmdarray</code>, representing the
     * tokens of a command line, and an array of strings <code>envp</code>,
     * representing "environment" variable settings, this method creates
     * a new process in which to execute the specified command.
     *
     * <p>This method checks that <code>cmdarray</code> is a valid operating
     * system command.  Which commands are valid is system-dependent,
     * but at the very least the command must be a non-empty list of
     * non-null strings.
     *
     * <p>If <tt>envp</tt> is <tt>null</tt>, the subprocess inherits the
     * environment settings of the current process.
     *
     * <p>A minimal set of system dependent environment variables may
     * be required to start a process on some operating systems.
     * As a result, the subprocess may inherit additional environment variable
     * settings beyond those in the specified environment.
     *
     * <p>{@link ProcessBuilder#start()} is now the preferred way to
     * start a process with a modified environment.
     *
     * <p>The working directory of the new subprocess is specified by <tt>dir</tt>.
     * If <tt>dir</tt> is <tt>null</tt>, the subprocess inherits the
     * current working directory of the current process.
     *
     * <p>If a security manager exists, its
     * {@link SecurityManager#checkExec checkExec}
     * method is invoked with the first component of the array
     * <code>cmdarray</code> as its argument. This may result in a
     * {@link SecurityException} being thrown.
     *
     * <p>Starting an operating system process is highly system-dependent.
     * Among the many things that can go wrong are:
     * <ul>
     * <li>The operating system program file was not found.
     * <li>Access to the program file was denied.
     * <li>The working directory does not exist.
     * </ul>
     *
     * <p>In such cases an exception will be thrown.  The exact nature
     * of the exception is system-dependent, but it will always be a
     * subclass of {@link IOException}.
     *
     * @param cmdarray array containing the command to call and
     *                 its arguments.
     * @param envp     array of strings, each element of which
     *                 has environment variable settings in the format
     *                 <i>name</i>=<i>value</i>, or
     *                 <tt>null</tt> if the subprocess should inherit
     *                 the environment of the current process.
     * @param dir      the working directory of the subprocess, or
     *                 <tt>null</tt> if the subprocess should inherit
     *                 the working directory of the current process.
     * @return A new {@link Process} object for managing the subprocess
     * @throws SecurityException         If a security manager exists and its
     *                                   {@link SecurityManager#checkExec checkExec}
     *                                   method doesn't allow creation of the subprocess
     * @throws IOException               If an I/O error occurs
     * @throws NullPointerException      If <code>cmdarray</code> is <code>null</code>,
     *                                   or one of the elements of <code>cmdarray</code> is <code>null</code>,
     *                                   or one of the elements of <code>envp</code> is <code>null</code>
     * @throws IndexOutOfBoundsException If <code>cmdarray</code> is an empty array
     *                                   (has length <code>0</code>)
     * @see ProcessBuilder
     * @since 1.3
     */
    public Process exec(String[] cmdarray, String[] envp, File dir)
            throws IOException {
        return new ProcessBuilder(cmdarray)
                .environment(envp)
                .directory(dir)
                .start();
    }

    /**
     * Returns the number of processors available to the Java virtual machine.
     * <p>
     * 返回 Java 虚拟机可用的处理器数量。
     *
     * <p> This value may change during a particular invocation of the virtual
     * machine.  Applications that are sensitive to the number of available
     * processors should therefore occasionally poll this property and adjust
     * their resource usage appropriately. </p>
     * <p>
     * 此值可能在虚拟机的特定调用期间更改。
     * 因此，对该信息敏感的应用程序应该定时轮询此属性，并相应地调整其资源使用情况。
     *
     * @return the maximum number of processors available to the virtual
     * machine; never smaller than one
     * @since 1.4
     */
    public native int availableProcessors();

    /**
     * Returns the amount of free memory in the Java Virtual Machine.
     * Calling the
     * <code>gc</code> method may result in increasing the value returned
     * by <code>freeMemory.</code>
     * <p>
     * 返回 Java 虚拟机中的空闲内存量。
     * 调用 gc 方法可能会导致增加 freeMemory 返回的值。
     *
     * @return an approximation to the total amount of memory currently
     * available for future allocated objects, measured in bytes.
     */
    public native long freeMemory();

    /**
     * Returns the total amount of memory in the Java virtual machine.
     * The value returned by this method may vary over time, depending on
     * the host environment.
     * <p>
     * 返回 Java 虚拟机中的内存总量。
     * 此方法返回的值可能随时间而变化，具体取决于主机环境。
     * <p>
     * Note that the amount of memory required to hold an object of any
     * given type may be implementation-dependent.
     * <p>
     * 请注意，保存任何给定类型的对象所需的内存量可能取决于实现。
     *
     * @return the total amount of memory currently available for current
     * and future objects, measured in bytes.
     */
    public native long totalMemory();

    /**
     * Returns the maximum amount of memory that the Java virtual machine will
     * attempt to use.  If there is no inherent limit then the value {@link
     * java.lang.Long#MAX_VALUE} will be returned.
     * <p>
     * 返回 Java 虚拟机将尝试使用的最大内存量。
     * 如果没有固有的限制，则返回值为 java.lang.Long.MAX_VALUE。
     *
     * @return the maximum amount of memory that the virtual machine will
     * attempt to use, measured in bytes
     * @since 1.4
     */
    public native long maxMemory();

    /**
     * Runs the garbage collector.
     * Calling this method suggests that the Java virtual machine expend
     * effort toward recycling unused objects in order to make the memory
     * they currently occupy available for quick reuse. When control
     * returns from the method call, the virtual machine has made
     * its best effort to recycle all discarded objects.
     * <p>
     * 运行垃圾回收器。
     * 调用此方法建议 Java 虚拟机花费精力回收未使用的对象，以便使它们当前占用的内存可用于快速重用。
     * 当从该方法调用返回时，虚拟机已尽其所能回收所有可丢弃的对象。
     * <p>
     * The name <code>gc</code> stands for "garbage
     * collector". The virtual machine performs this recycling
     * process automatically as needed, in a separate thread, even if the
     * <code>gc</code> method is not invoked explicitly.
     * <p>
     * 名称 gc 代表 "garbage collector"（垃圾回收器）。
     * 虚拟机会根据需要自动执行此回收过程，即使未显式调用 gc 方法，也会在单独的线程中执行。
     * <p>
     * The method {@link System#gc()} is the conventional and convenient
     * means of invoking this method.
     * <p>
     * 方法 System.gc() 是调用此方法的常规和便捷方法。
     */
    public native void gc();

    /* Wormhole for calling java.lang.ref.Finalizer.runFinalization */
    // 调用 java.lang.ref.Finalizer.runFinalization 的虫洞
    private static native void runFinalization0();

    /**
     * Runs the finalization methods of any objects pending finalization.
     * Calling this method suggests that the Java virtual machine expend
     * effort toward running the <code>finalize</code> methods of objects
     * that have been found to be discarded but whose <code>finalize</code>
     * methods have not yet been run. When control returns from the
     * method call, the virtual machine has made a best effort to
     * complete all outstanding finalizations.
     * <p>
     * 运行任何待完成的对象的 finalize 方法。
     * 调用此方法建议 Java 虚拟机花费精力运行已被发现丢弃但其 finalize 方法尚未运行的对象的 finalize 方法。
     * 当从该方法调用返回时，虚拟机已尽其所能完成所有未完成的 finalize 方法。
     * （这里的“尽其所能”是指，虚拟机会尽力去完成，但是不保证一定能完成）
     * <p>
     * The virtual machine performs the finalization process
     * automatically as needed, in a separate thread, if the
     * <code>runFinalization</code> method is not invoked explicitly.
     * <p>
     * 即使未显式调用 runFinalization 方法，则虚拟机也会根据需要自动在单独的线程中执行 finalize 过程。
     * <p>
     * The method {@link System#runFinalization()} is the conventional
     * and convenient means of invoking this method.
     *
     * @see java.lang.Object#finalize()
     */
    public void runFinalization() {
        runFinalization0();
    }

    /**
     * Enables/Disables tracing of instructions.
     * If the <code>boolean</code> argument is <code>true</code>, this
     * method suggests that the Java virtual machine emit debugging
     * information for each instruction in the virtual machine as it
     * is executed. The format of this information, and the file or other
     * output stream to which it is emitted, depends on the host environment.
     * The virtual machine may ignore this request if it does not support
     * this feature. The destination of the trace output is system
     * dependent.
     * <p>
     * 启用/禁用指令跟踪。
     * 如果 boolean 参数为 true，则此方法建议 Java 虚拟机在执行每条指令时发出虚拟机的调试信息。
     * 此信息的格式以及发出的文件或其他输出流取决于主机环境。
     * 如果虚拟机不支持此功能，则可能会忽略此请求。
     * <p>
     * If the <code>boolean</code> argument is <code>false</code>, this
     * method causes the virtual machine to stop performing the
     * detailed instruction trace it is performing.
     * <p>
     * 如果 boolean 参数为 false，则此方法会导致虚拟机停止执行其正在执行的详细指令跟踪。
     *
     * @param on <code>true</code> to enable instruction tracing;
     *           <code>false</code> to disable this feature.
     */
    public native void traceInstructions(boolean on);

    /**
     * Enables/Disables tracing of method calls.
     * If the <code>boolean</code> argument is <code>true</code>, this
     * method suggests that the Java virtual machine emit debugging
     * information for each method in the virtual machine as it is
     * called. The format of this information, and the file or other output
     * stream to which it is emitted, depends on the host environment. The
     * virtual machine may ignore this request if it does not support
     * this feature.
     * <p>
     * 启用/禁用方法调用跟踪。
     * 如果 boolean 参数为 true，则此方法建议 Java 虚拟机在调用每个方法时发出虚拟机的调试信息。
     * 此信息的格式以及发出的文件或其他输出流取决于主机环境。
     * 如果虚拟机不支持此功能，则可能会忽略此请求。
     * <p>
     * Calling this method with argument false suggests that the
     * virtual machine cease emitting per-call debugging information.
     * <p>
     * 使用参数 false 调用此方法会建议虚拟机停止发出每次调用的调试信息。
     *
     * @param on <code>true</code> to enable instruction tracing;
     *           <code>false</code> to disable this feature.
     */
    public native void traceMethodCalls(boolean on);

    /**
     * Loads the native library specified by the filename argument.  The filename
     * argument must be an absolute path name.
     * (for example
     * <code>Runtime.getRuntime().load("/home/avh/lib/libX11.so");</code>).
     * <p>
     * 加载由 filename 参数指定的 native 库。
     * filename 参数必须是绝对路径名。
     * （例如 Runtime.getRuntime().load("/home/avh/lib/libX11.so");）
     * <p>
     * If the filename argument, when stripped of any platform-specific library
     * prefix, path, and file extension, indicates a library whose name is,
     * for example, L, and a native library called L is statically linked
     * with the VM, then the JNI_OnLoad_L function exported by the library
     * is invoked rather than attempting to load a dynamic library.
     * A filename matching the argument does not have to exist in the file
     * system. See the JNI Specification for more details.
     * <p>
     * 如果 filename 参数在去除任何特定于平台的库前缀、路径和文件扩展名后，
     * 指示的库的名称为 L，且名为 L 的 native 库与 VM 静态链接，
     * 则会调用库导出的 JNI_OnLoad_L 函数，而不是尝试加载动态库。
     * 参数匹配的文件名不必存在于文件系统中。
     * 有关详细信息，请参阅 JNI 规范。
     * <p>
     * Otherwise, the filename argument is mapped to a native library image in
     * an implementation-dependent manner.
     * <p>
     * 否则，filename 参数将以实现相关的方式映射到本机库映像。
     * 也就是说，优先从静态库中加载，如果没有静态库，再从动态库中加载。
     * <p>
     * First, if there is a security manager, its <code>checkLink</code>
     * method is called with the <code>filename</code> as its argument.
     * This may result in a security exception.
     * <p>
     * 首先，如果有 SecurityManager 对象，则会将 filename 作为参数调用其 checkLink 方法。
     * 这可能会抛出 security exception。
     * <p>
     * This is similar to the method {@link #loadLibrary(String)}, but it
     * accepts a general file name as an argument rather than just a library
     * name, allowing any file of native code to be loaded.
     * <p>
     * 这类似于方法 loadLibrary(String)，但它接受一般的文件名作为参数，而不仅仅是库名，允许加载任何本机代码文件。
     * <p>
     * The method {@link System#load(String)} is the conventional and
     * convenient means of invoking this method.
     *
     * @param filename the file to load.
     * @throws SecurityException    if a security manager exists and its
     *                              <code>checkLink</code> method doesn't allow
     *                              loading of the specified dynamic library
     * @throws UnsatisfiedLinkError if either the filename is not an
     *                              absolute path name, the native library is not statically
     *                              linked with the VM, or the library cannot be mapped to
     *                              a native library image by the host system.
     * @throws NullPointerException if <code>filename</code> is
     *                              <code>null</code>
     * @see java.lang.Runtime#getRuntime()
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkLink(java.lang.String)
     */
    @CallerSensitive
    public void load(String filename) {
        load0(Reflection.getCallerClass(), filename);
    }

    // 这里锁的是当前 Runtime 对象；但由于 Runtime 是单例的，所以等价于锁的是 Runtime 类对象
    synchronized void load0(Class<?> fromClass, String filename) {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkLink(filename);
        }
        if (!(new File(filename).isAbsolute())) {
            throw new UnsatisfiedLinkError(
                    "Expecting an absolute path of the library: " + filename);
        }
        ClassLoader.loadLibrary(fromClass, filename, true);
    }

    /**
     * Loads the native library specified by the <code>libname</code>
     * argument.  The <code>libname</code> argument must not contain any platform
     * specific prefix, file extension or path. If a native library
     * called <code>libname</code> is statically linked with the VM, then the
     * JNI_OnLoad_<code>libname</code> function exported by the library is invoked.
     * See the JNI Specification for more details.
     * <p>
     * 加载由 libname 参数指定的 native 库。
     * libname 参数不得包含任何特定于平台的前缀、文件扩展名或路径；即 libname 必须是相对路径（单纯的库名）
     * 如果名为 libname 的 native 库与 VM 静态链接，则会调用库导出的 JNI_OnLoad_libname 函数。
     * <p>
     * Otherwise, the libname argument is loaded from a system library
     * location and mapped to a native library image in an implementation-
     * dependent manner.
     * <p>
     * First, if there is a security manager, its <code>checkLink</code>
     * method is called with the <code>libname</code> as its argument.
     * This may result in a security exception.
     * <p>
     * The method {@link System#loadLibrary(String)} is the conventional
     * and convenient means of invoking this method. If native
     * methods are to be used in the implementation of a class, a standard
     * strategy is to put the native code in a library file (call it
     * <code>LibFile</code>) and then to put a static initializer:
     * <blockquote><pre>
     * static { System.loadLibrary("LibFile"); }
     * </pre></blockquote>
     * within the class declaration. When the class is loaded and
     * initialized, the necessary native code implementation for the native
     * methods will then be loaded as well.
     * <p>
     * If this method is called more than once with the same library
     * name, the second and subsequent calls are ignored.
     *
     * @param libname the name of the library.
     * @throws SecurityException    if a security manager exists and its
     *                              <code>checkLink</code> method doesn't allow
     *                              loading of the specified dynamic library
     * @throws UnsatisfiedLinkError if either the libname argument
     *                              contains a file path, the native library is not statically
     *                              linked with the VM,  or the library cannot be mapped to a
     *                              native library image by the host system.
     * @throws NullPointerException if <code>libname</code> is
     *                              <code>null</code>
     * @see java.lang.SecurityException
     * @see java.lang.SecurityManager#checkLink(java.lang.String)
     */
    @CallerSensitive
    public void loadLibrary(String libname) {
        loadLibrary0(Reflection.getCallerClass(), libname);
    }

    synchronized void loadLibrary0(Class<?> fromClass, String libname) {
        SecurityManager security = System.getSecurityManager();
        if (security != null) {
            security.checkLink(libname);
        }
        if (libname.indexOf((int) File.separatorChar) != -1) {
            throw new UnsatisfiedLinkError(
                    "Directory separator should not appear in library name: " + libname);
        }
        ClassLoader.loadLibrary(fromClass, libname, false);
    }

    /**
     * Creates a localized version of an input stream. This method takes
     * an <code>InputStream</code> and returns an <code>InputStream</code>
     * equivalent to the argument in all respects except that it is
     * localized: as characters in the local character set are read from
     * the stream, they are automatically converted from the local
     * character set to Unicode.
     * <p>
     * 创建本地化了的 InputStream。
     * 此方法接受 InputStream 并返回与参数在所有方面都相同的 InputStream，
     * 除了它是本地化的：当从流中读取本地字符集中的字符时，它们会自动从本地字符集转换为 Unicode。
     * <p>
     * If the argument is already a localized stream, it may be returned
     * as the result.
     * <p>
     * 如果参数已经是本地化的流，则可能会将其作为结果返回。
     *
     * @param in InputStream to localize
     * @return a localized input stream
     * @see java.io.InputStream
     * @see java.io.BufferedReader#BufferedReader(java.io.Reader)
     * @see java.io.InputStreamReader#InputStreamReader(java.io.InputStream)
     * @deprecated As of JDK&nbsp;1.1, the preferred way to translate a byte
     * stream in the local encoding into a character stream in Unicode is via
     * the <code>InputStreamReader</code> and <code>BufferedReader</code>
     * classes.
     */
    @Deprecated
    public InputStream getLocalizedInputStream(InputStream in) {
        return in;
    }

    /**
     * Creates a localized version of an output stream. This method
     * takes an <code>OutputStream</code> and returns an
     * <code>OutputStream</code> equivalent to the argument in all respects
     * except that it is localized: as Unicode characters are written to
     * the stream, they are automatically converted to the local
     * character set.
     * <p>
     * 创建本地化了的 OutputStream。
     * 此方法接受 OutputStream 并返回与参数在所有方面都相同的 OutputStream，
     * 除了它是本地化的：当 Unicode 字符写入流时，它们会自动转换为本地字符集。
     * <p>
     * If the argument is already a localized stream, it may be returned
     * as the result.
     * <p>
     * 如果参数已经是本地化的流，则可能会将其作为结果返回。
     *
     * @param out OutputStream to localize
     * @return a localized output stream
     * @see java.io.OutputStream
     * @see java.io.BufferedWriter#BufferedWriter(java.io.Writer)
     * @see java.io.OutputStreamWriter#OutputStreamWriter(java.io.OutputStream)
     * @see java.io.PrintWriter#PrintWriter(java.io.OutputStream)
     * @deprecated As of JDK&nbsp;1.1, the preferred way to translate a
     * Unicode character stream into a byte stream in the local encoding is via
     * the <code>OutputStreamWriter</code>, <code>BufferedWriter</code>, and
     * <code>PrintWriter</code> classes.
     */
    @Deprecated
    public OutputStream getLocalizedOutputStream(OutputStream out) {
        return out;
    }

}
